<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Canada</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>US - Central</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>US - East</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>US - West</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>US - East,US - Central,US - West</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Canada,US - East,US - Central,US - West</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>United Kingdom</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Italy</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Argentina,Australia,Austria,Belgium,Brazil,Chile,China,Colombia,Czech Rep,Denmark,Dubai,Finland,France,Germany,Hong Kong,Hungary,India,Indonesia,Ireland,Italy,Japan,Korea,Malaysia,Mexico,Netherlands,New Zealand,Norway,Peru,Philippines,Poland,Portugal,Puerto Rico,Russia and CIS,Saudi Arabia,Singapore,Spain,Sweden,Switzerland,Taiwan,Thailand,Turkey,United Kingdom,Venezuela</values>
        </dashboardFilterOptions>
        <name>Opportunity Zone*</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Communication Online Tools</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Employee Communications,Employee Value Proposition,Change Communication</values>
        </dashboardFilterOptions>
        <name>Cluster</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Won Opportunities - CYR</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 2019 YTD Wins - CYR</metricLabel>
            <report>North_American_Talent/X1_Comm_Closed_Won_CY_by_Product</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 2018 YTD Wins - CYR</metricLabel>
            <report>North_American_Talent/Comm_Closed_Won_PY_by_Product_km</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Millions</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity.Opportunity_Sub_Market__c</groupingColumn>
            <groupingColumn>BucketField_30633785</groupingColumn>
            <header>Prior and Current MTD Won Opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>North_American_Talent/X3_Comm_Closed_Won_by_Prod_PM_CMTD</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total MTD Wins - CYR</metricLabel>
            <report>North_American_Talent/X3_Comm_Closed_Won_by_Prod_PM_CMTD</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total MTD Wins - TOR</metricLabel>
            <report>North_American_Talent/X3_Comm_Closed_Won_by_Prod_PM_CMTD</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity.Opportunity_Sub_Market__c</groupingColumn>
            <groupingColumn>BucketField_32301634</groupingColumn>
            <header>Open Pipeline Closing in Next 6 Months</header>
            <legendPosition>Bottom</legendPosition>
            <report>North_American_Talent/Comm_Open_by_Prod</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>TOR By Market</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total Open Pipeline Closing in Next 6 Months (TOR)</metricLabel>
            <report>North_American_Talent/Comm_Open_by_Prod</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Opportunities Closing This Month &amp; Next</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>North_American_Talent/X13_Comm_Opps_Closing_2_months</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 10 Communication Technology Opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>North_American_Talent/X12_Comm_Top_20_Open_Technology</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click here</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Won Opportunities - TOR</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 2019 YTD Wins - TOR</metricLabel>
            <report>North_American_Talent/X2_Comm_Closed_Won_TOR_by_Product</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total 2018 YTD Wins - TOR</metricLabel>
            <report>North_American_Talent/Comm_Closed_won_PY_TOR_by_product_KM</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>The shape of the funnel is an indicator of pipeline health.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline - TOR Funnel by Stage</header>
            <legendPosition>Bottom</legendPosition>
            <report>North_American_Talent/X6_Comm_Funnel_by_Stage</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>Total opportunity revenue for all opportunities in the pipeline.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total Pipeline</metricLabel>
            <report>North_American_Talent/X6_Comm_Funnel_by_Stage</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Past Due Open Opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>North_American_Talent/X14_Comm_Past_Due_Open_Opps</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>STAGE_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 15 Open Communication Consulting Opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>15</maxValuesDisplayed>
            <report>North_American_Talent/X11_Comm_Top_20_Open_Consulting</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top 10 Lost Opportunities - Closed this Month and Last</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>North_American_Talent/Comm_Lost_Consulting_Technology_Opp</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <groupingColumn>CLOSE_DATE2</groupingColumn>
            <header>Monthly Sales Trend - CYR Sales</header>
            <legendPosition>Bottom</legendPosition>
            <report>North_American_Talent/X2013_2015_Comm_Sales_Closed_by_Month1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>EXP_PRODUCT_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>The shape of the funnel is an indicator of pipeline health.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Win Probability Pipeline - TOR Forecasted Funnel by Stage</header>
            <legendPosition>Bottom</legendPosition>
            <report>North_American_Talent/X10_Comm_Win_Prob_Pipeline</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>EXP_PRODUCT_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>Total weighted opportunity revenue for all opportunities in the pipeline.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Forecasted Pipeline</metricLabel>
            <report>North_American_Talent/X10_Comm_Win_Prob_Pipeline</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_Sub_Market__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Product2.Cluster__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>PRODUCT_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Top Talent Open Opportunities Excluding Communication (TOR)</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>50</maxValuesDisplayed>
            <report>North_American_Talent/X16_Source_open_excl_Comm</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here</title>
        </components>
    </rightSection>
    <runningUser>kathryn.mccabe@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>Communication Practice Leadership Pipeline</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
