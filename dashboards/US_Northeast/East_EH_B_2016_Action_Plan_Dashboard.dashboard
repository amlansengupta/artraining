<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Atlanta - Lenox,WFH - Georgia</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Baltimore - South St,&quot;Washington,DC - Conn Ave NW&quot;,WFH - Delaware,WFH - District of Columbia,WFH - Maryland</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Charleston - Meeting,Charlotte - North Tryon,Greenville - South Main,WFH - North Carolina,WFH - South Carolina</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Hartford - Church,Norwalk - Merritt,WFH - Connecticut</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Maitland - Lake Lucien,Miami - South Biscayne,Sunrise - Sawgrass,Tampa - N. Rocky Point,WFH - Florida</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Louisville - West Market,WFH - Kentucky</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Boston - High St,Norwood - Investors Way,Portland - Monument,Rochester - Linden Oaks,WFH - Maine,WFH - Massachusetts,WFH - New Hampshire,WFH - Rhode Island,WFH - Vermont</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Morristown - South,Princeton - University,WFH - New Jersey</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Hoboken - River,Melville - South Service,New York - 1166,WFH - New York</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Philadelphia - Arch,WFH - Pennsylvania,Wilmington - Market</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Memphis - Ridgeway Loop,Nashville - West End,WFH - Tennessee</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Norfolk - Main,Richmond - East Cary,WFH - Virginia,WFH - West Virginia</values>
        </dashboardFilterOptions>
        <name>East Growth Team</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>Please note, reports on this dashboard are all pulling Opportunity Market = US East. Created date (or above the funnel end date) of opportunities is greater than or equal to June 30, 2016 with close date in 2016.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>EXP_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_48341659</groupingColumn>
            <header>Jim Gallic - Opp Tracking</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/X2016_East_EH_B_Opps_to_Close</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>2016 Opps to Close</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>*Looking to produce meaningful 2016 CY revenue; many are projects that can be presented as one-off cycle recommendations. Opportunities in this report all have close dates in 2016.</footer>
            <groupingColumn>BucketField_35257957</groupingColumn>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <header>EH&amp;B 2016 Targeted Quick Hits - One Time Projects</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/East_EH_B_Open_Pipe_75k_CY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Open Opps &lt;$75k Created Post-6/30</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>*Quick Hits Goal: 13 - 25 projects ($25k - $75k each)</footer>
            <groupingColumn>BucketField_35257957</groupingColumn>
            <groupingColumn>CLOSE_MONTH</groupingColumn>
            <header>EH&amp;B 2016 Targeted Quick Hits - One Time Projects</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/East_EHB_Closed_Pipe_75k_quick_hits</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Closed/Won Opps &lt;$75k Created Post-6/30</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Click on Growth Team name to review filtered report. To see full report click on tile heading.</footer>
            <groupingColumn>BucketField_30834644</groupingColumn>
            <header>EH&amp;B Current Month Created MQLs</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/New_Open_MQLs_EH_B_Requested_Consult</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Requested Consultation</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_23812480</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>BucketField_26243606</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_Year_Revenue__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Total_Opportunity_Revenue__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>EH&amp;B 2016 Book of Business Reviews</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EH_B_BoB_enter_pipe_last_7_days</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>2016 Open Opps - ENTER PIPE LAST 7 DAYS</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Excludes above the funnel. Close date in 2016.</footer>
            <header>EH&amp;B 2016 New Logo / New LOB Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_New_Logo_Pipeline_ALL_Open</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Open Opportunities (Created All Time)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Close date in 2016.</footer>
            <header>EH&amp;B 2016 New Logo / New LOB</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_New_Logo_LOB_Closed_Won_Full_Yr</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>2016 Closed / Won (Created All Time)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Excludes above the funnel. Close date in 2016.</footer>
            <header>EH&amp;B 2016 BOR Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_BOR_Pipeline_Open_All_Time</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Open Opportunities (Created All Time)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Close date in 2016.</footer>
            <header>EH&amp;B 2016 BOR</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_BOR_2016_Closed_Won_full_yr</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>2016 Closed/Won (Created All Time)</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Excludes Above the Funnel. Close date in 2016.</footer>
            <header>EH&amp;B 2016 New Logo / New LOB Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_New_Logo_Pipeline</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Open Opps Entering Pipeline Post-6/30</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>EH&amp;B 2016 New Logo / New LOB</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_New_Logo_New_LOB_Closed_Won</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>2016 Closed/Won (entered pipe post-6/30)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Excludes Above the Funnel</footer>
            <header>EH&amp;B 2016 BOR Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/EHB_BOR_Pipeline_open</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Open Opps Entering Pipeline Post-6/30</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>BucketField_35257957</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>EH&amp;B 2016 BOR</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>US_Northeast/East_EHB_BOR_Pipeline_closed_won</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>2016 Closed/Won (entered pipe post-6/30)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Following 2016 Book of Business Reviews, client planning proposed solutions were loaded into MercerForce as Above the Funnel opportunities.</footer>
            <groupingColumn>BucketField_26243606</groupingColumn>
            <header>EH&amp;B 2016 Book of Business Reviews</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/East_EHB_Book_of_Business_Reviews_ATF</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>2016 Above the Funnel</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Total_Opportunity_Revenue__c.CONVERT</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>*Excludes Above the Funnel</footer>
            <groupingColumn>BucketField_23812480</groupingColumn>
            <header>EH&amp;B 2016 Book of Business Reviews</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/East_EHB_Book_of_Business_Reviews</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>2016 Open Opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Total_Opportunity_Revenue__c.CONVERT</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Office__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>BucketField_26243606</groupingColumn>
            <header>EH&amp;B 2016 Book of Business Reviews</header>
            <legendPosition>Bottom</legendPosition>
            <report>US_Northeast/East_EHB_Book_of_Business_closed_won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>2016 Closed/Won Opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>eveline.case@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>East EH&amp;B 2016 Action Plan Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
