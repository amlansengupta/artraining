<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>% of Opportunities with Updated Sales Step Per Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_Opps_with</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Review Downward Trends</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Same Day Wins</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_TOR_Same_D</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of TOR that is Same Day:  Target: &lt;15%</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Won during the last 30 days.</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/ADP_SameDayWinsLast30Days_Nordics_Easter</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Same Day Wins - By LOB</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>VisualforcePage</componentType>
            <page>ystick__YardstickTrackDashboard</page>
            <pageHeightInPixels>300</pageHeightInPixels>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Based on when the opp entered the funnel, not when record was created.</footer>
            <header># of Newly Opened Opportunities by Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_Opps_Creat</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Excludes Above the Funnel records.</footer>
            <header>Past Due Open Opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_TOR_Past_D</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of TOR Past Due by Week:  Target: &lt;1%</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Excludes Above the Funnel records.</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/ADP_2_Past_Due_2_Nordics_Eastern_Europe</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Past Due - By LOB</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Tasks created by individual users. Includes Above the Funnel tasks.</footer>
            <groupingColumn>Usage_Data__c.Data_Date__c</groupingColumn>
            <header>Tasks  (includes MercerForce Lite users)</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_Avg_of_Tasks</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Avg. Tasks Created Per Week:  Target: 2</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_Tasks_Comp</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title># of Tasks Completed by Week</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header># of Past Due Tasks - Due in Current Year</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel># of Past Due Tasks - Due This Year</metricLabel>
            <report>Leadership_Nordics_Eastern_Europe/Nordics_Eastern_Europe_of_Tasks_Past</report>
            <showRange>false</showRange>
        </components>
    </rightSection>
    <runningUser>informaticaadmin@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>Nordics/Eastern Europe Adoption - Sales Data Statistics</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
