<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>247.0</gaugeMax>
            <gaugeMin>1.0</gaugeMin>
            <header>LATAM Standard Licenses Allocation</header>
            <indicatorBreakpoint2>215.0</indicatorBreakpoint2>
            <indicatorHighColor>#C25454</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#C25454</indicatorMiddleColor>
            <report>Latin_America_Validation/North_Latin_America_Licence_Holders</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>STD License Allocation LA</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>BarGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Inactive Users must have their licenses removed as soon as possible;</footer>
            <header>Inactive Licenses Holders</header>
            <legendPosition>Bottom</legendPosition>
            <report>LATAM_Growth/LATAM_MercerForce_License_Inactive</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Inactive License Holders by Country</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>The total count of licenses allocated by country</footer>
            <groupingColumn>ADDRESS_COUNTRY</groupingColumn>
            <header># Licenses allocated by country</header>
            <legendPosition>Bottom</legendPosition>
            <report>LATAM_Growth/LATAM_MF_Licenses_by_Country_Profile</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title># Total License Holders by Country</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>License Holders With Goals</header>
            <legendPosition>Bottom</legendPosition>
            <report>LATAM_Growth/DASH_LATAM_MF_Users_With_Goals</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>License Holders With Goals</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>BarGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>Types of Licenses (Bucket)</header>
            <legendPosition>Bottom</legendPosition>
            <report>LATAM_Growth/LATAM_MF_Licenses_by_Profile_Bucket</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title># STD &amp; LITE Licenses by Country</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>License Holders Without Goals</header>
            <legendPosition>Bottom</legendPosition>
            <report>LATAM_Growth/DASH_LATAM_MF_License_USER_NO_GOAL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>License Holders Without Goals</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>elton.brandao@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>LATAM MercerForce Licenses by Country</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
