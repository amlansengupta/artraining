<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Tracy Roseman</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Steve Messer</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Mark Allen</values>
        </dashboardFilterOptions>
        <name>Opportunity Owner</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>NA MMx Small Pipeline (Stages 1-4) - TOR</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_Mmx_Pipeline_Stage_2_to_4</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Click here for full report</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>4.0E7</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>NA MMx Small Pipeline (Stages 2-4) - CY</header>
            <indicatorBreakpoint1>2.96476E7</indicatorBreakpoint1>
            <indicatorBreakpoint2>3.7059501E7</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>NA_Small_MMx_Market/NA_Mmx_Pipeline_Stage_2_to_4</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <header>NA MMx Small Pipeline (Stages 2-4) - AR</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_Mmx_Pipeline_Stage_2_to_4</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineGroupedCumulative</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Please note: Sales for future months appear as plateaued. These numbers will update automatically as the year progresses.</footer>
            <header>Year over Year Sales - TOR</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Year_Over_Year_Sales</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>FY = Fiscal Year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Sales_Price_USD__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Newly Opened Opportunities in Last 4 Week &gt;$100K TOR</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Opps_in_past_4_wks_100k</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL report</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>1000000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>NA MMx Small MTD Wins - TOR</header>
            <indicatorBreakpoint1>614341.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>722755.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>NA_Small_MMx_Market/NA_MMx_Small_MTD_Won</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>3.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>NA MMx Small MTD Wins - CY</header>
            <indicatorBreakpoint1>1.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>2.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>NA_Small_MMx_Market/NA_MMx_Small_MTD_Won</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Millions</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>NA MMx Small MTD Wins - AR</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_MMx_Small_MTD_Won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineGroupedCumulative</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Please note: Sales for future months appear as plateaued. These numbers will update automatically as the year progresses.</footer>
            <header>Year over Year Sales - CY</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Year_Over_Year_Sales</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>FY = Fiscal Year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Sales_Price_USD__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Opps appearing less than $100K are single products under a larger opportunity.</footer>
            <header>Opportunities &gt;$100K TOR Expecting to Close THIS Month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Opps_Expecting_to_close_MTD</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Past Due Opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Past_Due_Opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL report</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>1.9E8</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>NA MMx Small YTD Wins - TOR</header>
            <indicatorBreakpoint1>1.44551044E8</indicatorBreakpoint1>
            <indicatorBreakpoint2>1.80688805E8</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>NA_Small_MMx_Market/NA_MMx_Small_YTD_Won</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>4.0E7</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>NA MMx Small YTD Wins - CY</header>
            <indicatorBreakpoint1>2.96476E7</indicatorBreakpoint1>
            <indicatorBreakpoint2>3.7059501E7</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>NA_Small_MMx_Market/NA_MMx_Small_YTD_Won</report>
            <showPercentage>false</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>NA MMx Small YTD Wins - AR</header>
            <legendPosition>Right</legendPosition>
            <report>NA_Small_MMx_Market/NA_MMx_Small_YTD_Won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Click here for full report</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineGroupedCumulative</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Please note: Sales for future months appear as plateaued. These numbers will update automatically as the year progresses.</footer>
            <header>Year over Year Sales - AR</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Year_Over_Year_Sales</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>FY = Fiscal Year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Sales_Price_USD__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>*Opps appearing less than $100K are single products under a larger opportunity.</footer>
            <header>Opps &gt;$100K TOR Closing THIS Quarter (Stages 1-4)</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Opps_closing_this_qtr_100K</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Sales_Price_USD__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Opps appearing less than $100K are single products under a larger opportunity.</footer>
            <header>Opps &gt;$100K TOR Closing NEXT Quarter (Stages 1-4)</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Opps_closing_Next_qtr_100K</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL Report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.CurrentYearRevenue_edit__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Sales_Price_USD__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Opps appearing less than $100K are single products under a larger opportunity.</footer>
            <header>Opps &gt;$100K TOR Closing REMAINDER (Stages 1-4)</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>25</maxValuesDisplayed>
            <report>NA_Small_MMx_Market/NA_MMx_Small_Opps_closing_REMAINDER_100K</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Click Here for FULL Report</title>
        </components>
    </rightSection>
    <runningUser>deborah.lassen@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>MMx</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
