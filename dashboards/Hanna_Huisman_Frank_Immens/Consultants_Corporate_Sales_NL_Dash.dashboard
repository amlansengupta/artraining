<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#99FFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_USD__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_CY_USD__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Individual_Sales_Goal__c$Owner</groupingColumn>
            <header>Previous Year - Actual Sales Credit</header>
            <legendPosition>Bottom</legendPosition>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Last_Year_Sales_Goal_met</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c$Sales_Goals__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c$Sales_Goal_CY__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Individual_Sales_Goal__c$Owner</groupingColumn>
            <header>Current Year - Sales Targets</header>
            <legendPosition>Bottom</legendPosition>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Current_Year_Sales_Targets</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FK_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Sales_Credit__c.Sales_Credit__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Sales_Credit__c.Sales_Credit_CY__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Current Pipeline - Shared Optys</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Shared_Optys</report>
            <showPicturesOnTables>true</showPicturesOnTables>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <header>Current Pipeline - # Shared Optys</header>
            <legendPosition>Bottom</legendPosition>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Shared_Optys1</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_CY_USD__c</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>461538.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>% of sales credits - CY target</header>
            <indicatorBreakpoint1>153846.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>307692.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Sales</report>
            <showPercentage>true</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_USD__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_CY_USD__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Individual_Sales_Goal__c$Owner</groupingColumn>
            <header>Current Year - Sales Credit YTD</header>
            <legendPosition>Bottom</legendPosition>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Sales</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FK_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_Year_Revenue__c.CONVERT</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Current Pipeline - Sales Credit</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Credit</report>
            <showPicturesOnTables>true</showPicturesOnTables>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <groupingColumn>FK_NAME</groupingColumn>
            <header>Current Pipeline - Sales Credit - # Optys</header>
            <legendPosition>Bottom</legendPosition>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Credit1</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Individual_Sales_Goal__c.Sales_Credit_Line_Items__r$Sales_Credit_USD__c</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <gaugeMax>913846.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>% of sales credits - TOR target</header>
            <indicatorBreakpoint1>304615.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>609231.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Hanna_Huisman_Frank_Immens/Cons_Corp_Sales</report>
            <showPercentage>true</showPercentage>
            <showRange>false</showRange>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
    </rightSection>
    <runningUser>hanna.huisman@mercermarshbenefits.com</runningUser>
    <textColor>#000000</textColor>
    <title>NL Corporate Giants</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
