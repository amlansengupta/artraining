<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <name>RM Country</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Please note totals may include multiple risks per one code</footer>
            <header># of Clients at Risk by RM Country</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/of_Clients_at_Risk_by_RM_Country</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title># of Clients at Risk by RM Country</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>Sensitve_At_Risk__c.Responsibility__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Account.Total_CY_Revenue__c</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Top 20 Clients At Risk by YTD Revenue</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Leadership_Growth_Markets/Top_20_Clients_At_Risk_by_YTD_Revenue</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here for full report</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Clients At Risk: # of Multiple At Risk Reasons</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Leadership_Growth_Markets/X08_GMR_CAR_DB</report>
            <showPicturesOnTables>false</showPicturesOnTables>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Please note totals may include multiple risks per one code</footer>
            <header># of Clients at Risk by At Risk Reason</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/of_Clients_at_Risk_by_At_Risk_Reason</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title># of Clients at Risk by At Risk Reason</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ACCOUNT.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Account.Total_CY_Revenue__c</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <header>Top 20 Clients At Risk by YTD Revenue - No Exec Sponsor</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Leadership_Growth_Markets/X5_GMR_Clients_at_Risk_DB</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Click here for full report</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Account$Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>CEM Status</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/CEM_Status</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Click here for full report</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Clients At Risk Totals</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total # of Clients At Risk (Export Full Listing)</metricLabel>
            <report>Leadership_Growth_Markets/X06_a_GMR_CAR_DB</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total CY$ of Clients At Risk (Export Full Listing)</metricLabel>
            <report>Leadership_Growth_Markets/X06_b_GMR_CAR_DB</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total # of Clients with LOBs (Export Full Listing)</metricLabel>
            <report>Leadership_Growth_Markets/X06_c_GMR_CAR_DB</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>Please note totals may include multiple risks per one code</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Last Modified Date Report (Export Full Listing)</metricLabel>
            <report>Leadership_Growth_Markets/X06_d_GMR_CAR_DB</report>
            <showRange>false</showRange>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Country__c</column>
            </dashboardFilterColumns>
            <displayUnits>Billions</displayUnits>
            <footer>YTD Revenue will be based on any filter selections</footer>
            <gaugeMax>1.14522E8</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Clients At Risk</header>
            <indicatorBreakpoint1>1.14522E7</indicatorBreakpoint1>
            <indicatorBreakpoint2>2.29044E7</indicatorBreakpoint2>
            <indicatorHighColor>#C25454</indicatorHighColor>
            <indicatorLowColor>#C2C254</indicatorLowColor>
            <indicatorMiddleColor>#C28B54</indicatorMiddleColor>
            <report>Leadership_Growth_Markets/Clients_At_Risk8</report>
            <showPercentage>true</showPercentage>
            <showRange>false</showRange>
            <showTotal>false</showTotal>
            <showValues>false</showValues>
            <title>% YTD Revenue at Risk vs EP Reg Revenue</title>
        </components>
    </rightSection>
    <runningUser>harpreet.hundal@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>Growth Markets - Clients at Risk</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
