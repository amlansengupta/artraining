<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>IF YOU APPLY A FILTER TO THIS DASHBOARD, THE TILES COMPARING MERCER/COMPETITORS AVERAGE SCORES WILL NO LONGER SHOW RESULTS. TO APPLY A FILTER PARAMETER TO THOSE TILES, CLICK THE GRAPH TO VIEW THE UNDERLYING REPORT.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>BarStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <header>Assigned CEMs - Status Breakdown</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/CEM_Progress_Completing_Assigned_GRO</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target of 233</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>BarStacked100</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/CEM_Progress_Completing_Assigned_GRO</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>%</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>C-Suite Readiness - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_C_Suite_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Partnership - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Partnership_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Quality - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Quality_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>10.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Avg Overall Rating for Completed CEMs</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/PYCEM_Average_Overall_Rating_GMR</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Diversity &amp; Inclusion - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Diversity_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Planning &amp; Proj Mgmt - Mercer/Competitor Avg Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Planning_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Service &amp; Comm - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_ServiceComm_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>10.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Monthly Trend - CEM Average Overall Rating</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/CEM_Average_Score_Trend_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Innovative - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Innovative_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Proactive - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Proactive_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Value - Mercer/Competitor Average Scores</header>
            <legendPosition>Bottom</legendPosition>
            <report>Leadership_Growth_Markets/Competitor_Comparison_Value_GRO</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Out of 10</title>
            <useReportChart>true</useReportChart>
        </components>
    </rightSection>
    <runningUser>harpreet.hundal@mercer.com</runningUser>
    <textColor>#000000</textColor>
    <title>Growth Markets - CEM Prior Year</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
