<?xml version="1.0" encoding="UTF-8"?>
<EmailTemplate xmlns="http://soap.sforce.com/2006/04/metadata">
    <available>true</available>
    <encodingKey>UTF-8</encodingKey>
    <name>Pac_IC_Defined Contribution_Alert or Notification_201407_MST Monthly Portfolio S</name>
    <style>none</style>
    <subject>Portfolio Management Snapshot</subject>
    <textOnly>Mercer Super Trust Portfolio Management Snapshot
This update from the Mercer Investments Portfolio Management team looks back at developments over the month and quarter to 30 June 2014, as well as the financial year 2013-14.

IN THE 2013-14 FINANCIAL YEAR……


 - Mercer Growth* returned 14.3% in the financial year to 30 June 2014, outperforming both its composite benchmark by 0.8% and CPI+3% objective by 8.3%.  





 - In peer relative terms, Mercer Growth had a good financial year, outperforming (after fees and taxes) both the median balanced master trust by 0.8% and slightly outperforming the broader median.



 - Since launch, our new MySuper default, Mercer SmartPath, grew to approximately $1.7 billion of funds under management (FUM) by the end of June and continues to grow.





 - Mercer SmartPath 1969-73* is our largest SmartPath option. Over the quarter to 30 June 2014, its underlying portfolio returned 1.8%, with returns marginally negative over the 30 days of June.



WHAT DID WE DO?


 - Added IFM Investors to the Australian Small Cap Equity manager line up.  Download the manager list &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf&gt; as at 30 June 2014.




 - We finalised our Dynamic Asset Allocation (DAA) views for the first quarter of the new financial year (i.e. Q3 2014).  Download our DAA Dashboard &lt;http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf&gt;.




 - No changes were made to the asset allocation in the month
IN THIS ISSUE


ECONOMY &amp; MARKETS &lt;#1&gt;



INVESTMENT PERFORMANCE &lt;#2&gt;



ASSET ALLOCATION UPDATE &lt;#3&gt;



MANAGER UPDATE &lt;#4&gt;


 &lt;http://www.linkedin.com/groups/Mercer-Investments-Asia-Pacific-4442861/about?trk=anet_ug_grppro&gt;


Click Here to Forward Article


 &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf&gt;


 &lt;http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf&gt;
ECONOMY &amp; MARKETS




The financial year ended with volatility at, or close to, historically low levels.  All major asset classes delivered positive returns with many in double digits.  As we enter the 2014-15 financial year, economic growth is looking stronger than a year ago, particularly in the US.  Meanwhile, the Australian economy has held up better than many had expected. 

However, it wasn’t smooth sailing all year. Financial markets kicked off the 2013-14 financial year on a slightly jittery note, as markets pondered when the US Federal Reserve would begin to “taper” or decrease the size of its asset purchase program and what the impact of this would be.  Bond yields rose (prices 
down), while emerging markets (particularly emerging markets debt) see-sawed, and the Aussie dollar sold off. Global developed market equities rallied however, despite other asset markets being more cautious. Nevertheless, many investors speculated that tapering would instigate a downturn in the market. While the delay in the commencement of tapering (from September to December), was welcomed by the markets, nothing significant actually happened when the Fed started tapering, despite all the noise beforehand. It is fair to say that concerns about emerging markets, particularly the so-called “fragile five” (Brazil, India, Indonesia, South Africa and Turkey), persisted after the commencement of tapering. These countries’ current high account deficits and structural issues affected emerging markets, which performed very poorly in January 2014. 

Come February, it seemed that we suddenly entered a different environment altogether. While geopolitical issues kept investors on their toes, it wasn’t enough to stop emerging markets and the Australian dollar bouncing back, while global equities and sovereign bonds (ex-Europe) traded within a relatively narrow range for the remainder of the calendar year’s first quarter. Meanwhile, the US economy experienced significant weather-induced setbacks, prompting some to question if slower activity was actually weather induced, or a sign of a loss of momentum in the US economy. We know now that, fortunately, it was the former. 

Back home, Australian economic growth actually surprised throughout Q1 2014, with better-than-expected labour, consumer and capex data, before easing toward the end of the financial year. Through the final three months of the 2013-14 financial year, “Goldilocks” (i.e. just right) conditions prevailed globally.  Equities continued to rally, volatility fell to ultra-low levels, and virtually all asset classes delivered positive returns. The strong equity performance can be explained by a positive macro backdrop and earnings growth, particularly in developed markets, and relatively cheap emerging market equity valuations. However, other developments are less easy to explain. The Australian dollar remains high, possibly supported by low volatility, despite softer commodity prices and tighter interest rate spreads.  Global bond yields have fallen to what seem extraordinarily low levels given the prevailing macro conditions. Only time will tell whether these lower yields and low volatility are structural or cyclical phenomena.  
Our DAA Dashboard &lt;http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf&gt; 
summarises our current views for the first three months of the new financial year (Q3 2014).  Well worth a look. &lt;#top&gt;









INVESTMENT PERFORMANCE





Mercer Growth* performance was flat in June, taking financial year returns to 14.3%, a pleasing result, in a challenging and competitive year. Over the year, Mercer Growth outperformed its composite benchmark by 0.8%, with significant contributions from our underlying Australian Equity and Unlisted Infrastructure managers in particular. Our asset allocation decisions were less supportive in the second half of the financial year as the Australian dollar has strengthened, and some sovereign bond markets unexpectedly advanced. Mercer Growth also outperformed its CPI+3% target over the financial year by 8.3%.

Mercer Growth has also performed more pleasingly on a peer relative basis, outperforming the Median SuperRatings Balanced Master Trust by 0.8% and finishing the year slightly ahead of the Broader Balanced Median (after fees and taxes), marking a good reversal from the previous financial year’s position.  It is also worth noting that Mercer Growth’s performance is ahead of the Master Trust Median over all times periods from one month to ten years in the June SuperRatings survey.    


Inflows to Mercer SmartPath® &lt;http://page.mercer.com/FeoEv00bk0D0H2wh000VbFu&gt; investment options continue to grow. By the end of June, funds under management (FUM) in Mercer SmartPath reached $1.7 billion. As FUM has reached critical mass, a number of new asset class exposures required by the introduction of Mercer SmartPath have gradually been added over the course of the year, so we are now in a position to provide updates on Mercer SmartPath performance going forward.  

Due to the large number of Mercer SmartPath options, we will report performance for the Mercer SmartPath 1969-73 option only here because this is the largest option by FUM and is the most representative of underlying 
investors.  This option typically has a higher weighting to growth assets (around 85%) relative to Mercer Growth (around 70%), so we would expect its growth assets exposure to outperform in positive months for risk assets, such as equities, and underperform in weaker risk 
markets.    

Because Mercer SmartPath launched partway through the financial year, we are not yet able to report on its full financial year performance.  That will have to wait until this time next year.  In the month of June, the Mercer SmartPath 1969-73 option returns were marginally negative in absolute returns, but 1.8% over the June quarter.
The June Monthly Report provides more details on the performance of Mercer Growth and Mercer SmartPath 1969-73, along with that of other investment options.
 &lt;#top&gt;









ASSET ALLOCATION UPDATE





No changes were made to the asset allocation in June. We remain overweight growth assets in particular overseas shares, and retain underweight exposures to Global Sovereign Bonds and the Australian dollar.
 &lt;#top&quot;&gt;&lt;img title=&gt;










MANAGER UPDATE





IFM Investors was added to the Australian Small Cap Equity line-up in June. Australian Small Caps typically offer significant potential for active manager outperformance. However, many good Small Cap managers are closed, limiting our ability to invest as much as we would like in this sector. In this context, adding IFM to the line-up is a welcome development. IFM’s strategy uses fundamental research to identify mispriced stocks. Download the 30 June 2014 manager lists for the Mercer Super Trust &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf&gt; for more information on the line-up of our underlying managers at financial year end.
 &lt;#top&quot;&gt;&lt;img title=&gt;
If you want to remove yourself from this distribution list, request to be removed here &lt;mailto:Suzanne.Whiting@mercer.com?subject=Remove from MST Portfolio Snapshot distribution list&quot;&gt;&lt;span style=&gt;.  

If you no longer wish to receive any Mercer e-mails, unsubscribe here &lt;http://info.mercer.com/UnsubscribePage.html&gt;.











SuperRatings has awarded the Mercer Super Trust Corporate Superannuation Division its highest rating of ‘Platinum Rating’ for 2014. ‘SuperRatings’ does not issue, sell, guarantee or underwrite this product. Chant West has awarded the Mercer Super Trust the highest rating of ‘5 Apples’ for the Superannuation Division for 2014. The Chant West ratings logo is a trademark of Chant West Pty Limited and used under licence. For further information about the methodology used by Chant West, please see www.chantwest.com.au &lt;http://www.chantwest.com.au/&gt;. The Heron Partnership (Heron) has awarded Mercer’s Corporate Products (including the Mercer Super Trust’s Corporate Super and SmartSuper divisions) its highest ‘5 Star’ Quality rating for 2014.






 &lt;#2&gt;*NOTE &lt;#2&gt;: &lt;#2&gt; References to the ‘Mercer Growth’ are to the 
Mercer Super Trust’s Mercer Growth investment option and returns quoted are “pre-tax, pre-fees,” with the exception of performance numbers that have been sourced from the SuperRatings Fund Crediting Rate Survey, which are after tax and fees.  This will also apply to references to Mercer SmartPath returns, which relate to the largest Mercer SmartPath investment option in the Mercer Super Trust – Mercer SmartPath 1969-73.  








This email has been prepared by Mercer Superannuation (Australia) Limited (MSAL) ABN 79 004 717 533, Australian Financial Services Licence #235906. Address: Collins Square, 727 Collins Street, Melbourne VIC 3008. 
Tel: 03 9623 5555. MSAL is the trustee of the Mercer Super Trust, ABN 19 905 422 981. Any advice contained in this document is of a general nature only, and does not take into account the personal needs and circumstances of any particular individual. Prior to acting on any information contained in this document, you need to take into account your own financial circumstances, consider the Product Disclosure Statement for any product you are considering, and seek professional advice from a licensed, or appropriately authorised, financial 
adviser if you are unsure of what action to take. Past performance should not be relied upon as an indicator of future performance. &apos;MERCER&apos; is a registered trademark of Mercer (Australia) Pty Ltd ABN 32 005 315 917.

Copyright 2014 Mercer LLC. All rights reserved.﻿﻿

Find a Mercer office &lt;http://www.mercer.com/about-us/locations.html&gt;</textOnly>
    <type>custom</type>
    <uiType>Aloha</uiType>
</EmailTemplate>
