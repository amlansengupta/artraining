<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Marsh &amp; McLennan Companies - Your 2012 Annual Enrollment Materials Will Arrive Soon</title>

<style type="text/css">
img {border:0px}
body {
        margin: 0px;
        padding: 0px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000000;
}
a:link {color:#000000;}      /* unvisited link */
a:visited {color:#000000;}  /* visited link */
a:hover {color:#000000;
             text-decoration:underline;}  /* mouse over link */
a:active {color:#000000;}

table {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        line-height: 17px;
        color: #000000;
}
.head {
        color: #118b3f;
        font-size: 13px;
        font-weight: bold;
        padding-bottom: 2px;
}
li {
        padding-bottom: 5px;
}
.head2 {
        padding-bottom: 5px;
        color: #73bf44;
        font-weight: bold;
        font-size: 12px;
        text-transform: uppercase;
        padding-top: 10px;
}
    .bodyHead {
        padding-bottom: 5px;
        color: #43276D;
        font-weight: bold;
        font-size: 12px;
}
    .green {
        color: #118b3f;
        font-weight: bold;
        font-size: 12px;
}
.greenLt {
        color: #73bf44;
        font-weight: bold;
        font-size: 12px;
}
            .headSide {
        padding-bottom: 8px;
        color: #01592e;
        font-weight: bold;
        font-size: 12px;
        text-transform: uppercase;
}

.footnote {
        font-size: 10px;
        line-height: normal;
        padding-bottom: 5px;
        color: #666666;
}
    .grayBorder { 
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
        border-bottom: 1px solid #c2cad2; 
}
    .grayBordertop { 
        border-top: 1px solid #c2cad2; 
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
}
    .pRowTen {
        padding-bottom: 10px;
                padding-right:
        }
.sidebar {
        background-color: #bddda3;
}
.small{
        font-size: 10px;
        color: #999999;
        padding-left: 45px;
        }
    .pRowTen1 {padding-bottom: 5px;
}
</style>
</head>
<body ><div style="font:14px tahoma; width:100%; " class="mktEditable" id="edit_text" ><div id="edit_text" class="mktEditable" style="font:14px tahoma; width:100%; " ><table border="0" cellspacing="0" cellpadding="0" width="615" align="center">
<tbody>
<tr>
<td class="grayBordertop" style="padding-top: 20px;" width="615" align="center" valign="middle"><img src="http://www.mercerhrs.com/email/mmc/272861/header.gif" alt="Your 2013 Annual Enrollment Materials Will Arrive Soon" width="615" height="215"></td>
</tr>
<tr>
<td class="grayBorder" width="615" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="615">
<tbody>
<tr>
<td width="43" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="43" height="1"></td>
<td width="355" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="355">
<tbody>
<tr>
<td class="pRowTen" align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Dear Colleague,</td>
</tr>
<tr>
<td class="pRowTen" style="padding-right: 20px;" align="left" valign="top">At Marsh &amp; McLennan Companies, we want you to take an active role in preparing for and building your financial future. As part of our commitment to your total wellbeing, we invite you to participate in the <span class="green">Marsh &amp; McLennan Companies Supplemental Savings &amp; Investment Plan</span> (Supplemental Savings &amp; Investment Plan) for the 2014 plan year as part of your long-term savings strategy.</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">As an eligible participant for the Supplemental Savings &amp; Investment Plan for the 2014 plan year, your opportunity to enroll is coming soon.</td>
</tr>
<tr>
<td class="head" align="left" valign="top">ENROLLMENT DATES</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">If you'd like to participate in the Supplemental Savings &amp; Investment Plan for the 2014 plan year, you must enroll during the Annual Enrollment period, <strong>beginning Monday, November 25, 2013, at 9 a.m. Eastern time,</strong> and <strong>ending Friday, December 13, 2013, at 4 p.m. <span style="white-space: nowrap;">Eastern time.</span></strong></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Even if you currently participate in the Supplemental Savings &amp; Investment Plan, you <strong>must</strong> actively enroll <span style="white-space: nowrap;">for 2014</span> if you want to continue participating<span style="white-space: nowrap;">.</span></td>
</tr>
<tr>
<td class="head" align="left" valign="top">WHAT'S NEW FOR 2014</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">This year, there are several changes that may affect your participation in the Supplemental Savings &amp; <span style="white-space: nowrap;">Investment Plan.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td align="left" valign="top"><span class="greenLt">&bull;</span></td>
<td class="pRowTen1" style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" align="left" valign="top"><span class="greenLt">Some Internal Revenue Service (IRS) Limits Are Changing for the Marsh &amp; McLennan Companies 401(k) Savings &amp; Investment Plan (MMC 401(k) Plan)</span> The IRS has increased the limit on compensation eligible for consideration under the MMC 401(k) Plan to $260,000. The IRS limit on the amount a participant can contribute (sum of before-tax and/or Roth 401(k) contributions) to the <span class="pRowTen1" style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;">MMC</span> 401(k) Plan has remained $17,500 for 2014 ($23,000 if you will be age 50 or older by December 31, 2014, and therefore eligible to make $5,500 in catch-up contributions).</td>
</tr>
<tr>
<td align="left" valign="top"><span class="greenLt">&bull;</span></td>
<td class="pRowTen1" style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" align="left" valign="top"><span class="greenLt">Expense and Investment Notices Now Available</span><br>
Effective June 29, 2013, legally required annual Expense and Investment Notices detailing information about the investments, fees, and expenses associated with participating in the MMC 401(k) Plan and/or Mercer/HR Services Plan are now available.</td>
</tr>
<tr>
<td width="3%" align="left" valign="top"></td>
<td class="pRowTen1" style="font-family: Verdana, Geneva, sans-serif; font-size: 11px;" width="97%" align="left" valign="top"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td></td>
</tr>
<tr>
<td class="head" align="left" valign="top">ADDITIONAL FINANCIAL EDUCATION</td>
</tr>
<tr>
<td class="pRowTen" style="padding-right: 10px;" align="left" valign="top">We realize that participating in the Supplemental Savings &amp; Investment Plan can be a complex topic. Before making any financial decisions, you may wish to consult with a tax or financial <span style="white-space: nowrap;">planning advisor.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">During Annual Enrollment, financial planners from <strong>The Ayco Company, L.P.</strong> &mdash; a leading provider of financial education &mdash; will be available to assist you. To speak to an Ayco financial planner, please call the Employee Service Center at +1 866 374 2662. These services are provided by the Company at no cost <span style="white-space: nowrap;">to you.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">The Ayco Company, L.P. is not affiliated with Mercer, LLC or Marsh &amp; McLennan Companies, Inc. (and its direct and indirect subsidiaries), which do not provide <span style="white-space: nowrap;">investment advice.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Sincerely,</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/268135/signature.gif" alt="Alexander P. Voitovich" width="153" height="32"></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Alexander P. Voitovich<br>
Executive Director, Global Benefits</td>
</tr>
</tbody>
</table>
</td>
<td width="13" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="1"></td>
<td width="191" align="center" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="172">
<tbody>
<tr>
<td width="192" align="left" valign="top">
<table class="sidebar" border="0" cellspacing="0" cellpadding="0" width="190">
<tbody>
<tr>
<td class="pRowTen" style="background-color: #fff;" align="right" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="1"></td>
<td><a class="mktNoTrack" href="http://www.mercerhrs.com/flipbooks/mmc/MMC_273355/" target="_blank"><img src="http://www.mercerhrs.com/email/mmc/272861/sidebar_img2.gif" border="0" alt="Enrollment Guide" width="143" height="154"></a></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="14" height="1"></td>
</tr>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8"></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8"></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a class="mktNoTrk" href="http://www.mercerhrs.com/flipbooks/mmc/MMC_273355/" target="_blank"><img src="http://www.mercerhrs.com/email/mmc/272861/view_guide.gif" border="0" alt="View Guide" width="143" height="40"></a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="headSide">2014 ENROLLMENT<br>
INFORMATION</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="pRowTen" style="padding-right: 10px;">You will find complete details on the Supplemental Savings &amp; Investment Plan<br>
&mdash; including specific enrollment instructions &mdash; in a package that is being mailed to your address on file. It contains a personalized letter with your current contribution rates for the MMC 401(k) Plan and deferral rates for the Supplemental Savings &amp; Investment Plan, as applicable. If you do not receive this package by December 1, 2013, please contact the Employee Service Center at<br>
+1 866 374 2662, any business day, from 8 a.m. to 8 p.m. <span style="white-space: nowrap;">Eastern time.</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>
<p>You can also find<br>
detailed information<br>
about the Supplemental Savings &amp; Investment Plan in the Benefits Handbook.</p>
<p>Sign in to PeopleLink (<a href="https://www.mmcpeoplelink.com" target="_new">www.mmcpeoplelink.com</a>), click <strong>MMC Supplemental Savings Plan</strong> via the Finances tab, and under Related Sites in the right navigation bar, select <span style="white-space: nowrap;"><strong>Benefits Handbook</strong>.</span></p>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<div align="center"><span class="footnote"><img style="margin-top: 30px;" src="http://www.mercerhrs.com/email/mmc/272861/wellbeingatwork_logo.gif" alt="" width="170" height="75"></span></div>
</td>
<td width="13" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1"></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td colspan="3" align="left" valign="bottom">&nbsp;</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td style="font-size: 11px; line-height: 14px;" colspan="3" align="left" valign="bottom">
<p><em>Investors should carefully consider the investment objectives, risks, charges, and expenses of an investment option or fund before investing. For a prospectus and, if available, a summary prospectus or offering statement, if applicable for the fund or investment option, or for a fund fact sheet containing this and other information about any investment option or fund in the plan, call +1 866 374 2662 or sign in to PeopleLink (<a href="https://www.mmcpeoplelink.com" target="_new">www.mmcpeoplelink.com</a>). Click MMC Supplemental Savings Plan via the Finances tab, and from the right navigation bar, select MMC Supplemental Savings Plan under Plan/Fund Communications. Read the prospectus and, if available, a summary prospectus or offering statement, if applicable for a fund or investment option, and a fund fact sheet carefully before making any <span style="white-space: nowrap;">investment decisions.</span></em></p>
<p><em>You assume the responsibility for the notional investment choices you make for your account with this type of plan. For further information on notional investments, you may wish to consult an independent investment advisor. Investing involves risk, including the risk <span style="white-space: nowrap;">of loss.</span></em></p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td colspan="4" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td class="small" align="left" valign="top"><br>
M272861 11/13</td>
<td style="font-size: 11px; line-height: 14px;" align="left" valign="bottom"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="spacer" width="10" height="20"></td>
</tr>
</tbody>
</table>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td style="font-size: 11px; line-height: 14px;" colspan="3" align="left" valign="bottom">&nbsp;</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div>
<table width="78%" align="center">
<tbody>
<tr>
<td style="padding-left: 0px; font-size: 9px; padding-bottom: 5px; color: #666666; line-height: 17px; font-family: Arial,Helvetica,sans-serif;" width="50%" align="left" valign="top">
<p>This email was sent by:&nbsp; Mercer<br>
1 Investors Way Norwood, MA 02062 USA</p>
<p>We respect your right to privacy - <a href="http://www.mercer.com/privacy.htm" target="_blank">view our policy</a></p>
</td>
<td style="padding-right: 10px; font-size: 9px;" align="right"><img src="http://info.mercer.com/rs/mercer/images/MMC_horizontal_4c.png" alt="MMC_horizontal_4c.png" width="153" height="21"><br></td>
</tr>
<tr>
<td colspan="2" align="center">
<p><span style="font-family: Verdana; font-size: xx-small;"><br>
If you no longer wish to receive these emails, click on the following link: <a href="http://info.mercer.com/UnsubscribePage.html">Unsubscribe</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="font:14px tahoma; width:100%; " class="mktEditable" ></div>
</body>
</html>