<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
img {
        border:none;    
}
</style>
<title></title>
</head>
<body ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td valign="top"><a href="#"><img src="http://info.mercer.com/rs/mercer/images/cemailLogo.png" width="601" height="63" alt="Mercer" border="0" style="display:block"></a></td>
</tr>
<tr ><td ><div class="mktEditable" id="header" ><IMG alt="" src="http://info.mercer.com/rs/mercer/images/11711A-HC_MarketTo_ExtEmail_Banner_v1.jpg" width=600 height=210><A href="#"></A></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
<tr ><td width="32">&nbsp;</td>
<td valign="top" width="365" ><div class="mktEditable" id="content" ><table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;">
<p><span style="color: #002b79; font-size: large;">EXECUTIVE PAY REGULATION: THE POTENTIAL IMPACTS OF PROPOSED EUROPEAN REFORMS</span></p>
<p>The political impetus to regulate executive pay has accelerated in Europe. Recent regulatory developments that will give shareholders greater oversight of executive pay and cap bonuses in the financial services sector, reflect a hardening of attitudes among European politicians and the public. In an era of low or non-existent economic growth, consumer price inflation, and falling average real wages, executive remuneration will continue to be a sensitive issue.</p>
<p>This is particularly true in the banking sector, where the continued payment of bonuses, in the face of taxpayer-funded bailouts and revelations such as the Libor fixing scandal in London, has sparked outrage. But with other countries and regions taking a less prescriptive approach, an unlevel playing field is emerging and may result in executives leaving the EU for less regulated markets.</p>
<p>IN THIS ISSUE, ANSWERS TO:</p>
<ul>
<li>How are differences in regulations creating an unlevel global playing field for talent?</li>
<li>What unintended consequences could result from additional regulation of executive pay?</li>
<li>What are the potential consequences of a binding say-on-pay vote?</li>
<li>What should companies do to ensure successful say-on-pay votes?</li>
</ul>
<p>DOWNLOAD FULL <a href="http://info.mercer.com/rs/mercer/images/ER%26PE Perspective_Special Issue_April 2013.pdf">PERSPECTIVE</a></p>
<p>__________________________________________________________________</p>
<p><strong>Free webcast:<br /></strong> <br /><span style="color: #002c77;">EXECUTIVE PAY REGULATORY DEVELOPMENTS IN EUROPE: IMPLICATIONS FOR ORGANIZATIONS AROUND THE WORLD</span><br /><br />Join one of our two webcasts on Thursday, May 2 where our executive rewards consultants from London and New York will provide an update on the current state of governance reform and walk through the variety of implications and unintended consequences. With these insights in mind, you and your organization can better plan for contingencies and capitalize on the opportunities.<br /><br />May 2, 2013 at 9:00&nbsp;- 10:00 am EDT<br /><a href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?t=a&amp;d=297098357">Register</a><br /><br />May 2, 2013 at 12:00 noon&nbsp;- 1:00 pm EDT<br /><a href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=298924304">Register<br /></a><br />&nbsp;</p>
</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table></div>
</td>
<td width="20">&nbsp;</td>
<td width="20" style="border-left:1px solid #d3f0f6">&nbsp;</td>
<td width="133" valign="top" ><div class="mktEditable" id="content2" ><table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;">
<p><span style="color: #00a7c7; font-size: 12px;">SPECIAL ISSUE </span><br /><span style="font-size: 12px;"><span style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; color: #00a7c7;">april 2013</span></span></p>
<p><span style="font-size: 12px;"><span style="color: #000000;"><img src="http://info.mercer.com/rs/mercer/images/ER_April_Special.gif" alt="" width="93" height="102" /><br /><a href="http://info.mercer.com/rs/mercer/images/ER%26PE Perspective_Special Issue_April 2013.pdf">DOWNLOAD</a><br /><br /><br />PERSPECTIVE<br />AUTHORS:<br /><br />Mark Hoble<br /><a href="mailto:mark.hoble@mercer.com">mark.hoble@mercer.com</a><br />+44 20 7178 5725<br /><br />Patricia Bradley<br /><a href="mailto:patricia.bradley@mercer.com">patricia.bradley@mercer.com</a><br />+44 20 7178 3135<br /><br />Vicki Elliott<br /><a href="mailto:vicki.elliott@mercer.com">vicki.elliott@mercer.com</a><br />+1 212 345 7663</span></span></p>
<p><span style="color: #000000; font-size: 12px;">Visit our <a href="http://www.mercer.com/referencecontent.htm?idContent=1372545">Executive Rewards &amp; Performance Effectiveness</a> page to read our latest thought pieces!</span><br /><br /><span style="font-size: 12px; color: #00a7c7;">Share:</span>&nbsp;<a href="#"><img src="http://info.mercer.com/rs/mercer/images/twit.png" border="0" alt="Twitter" width="17" height="17" /></a>&nbsp;<a href="#"><img src="http://info.mercer.com/rs/mercer/images/lin.png" border="0" alt="linkedin" width="17" height="17" /></a>&nbsp;<a href="#"><img src="http://info.mercer.com/rs/mercer/images/fb.png" border="0" alt="Facebook" width="17" height="17" /></a>&nbsp;<a href="#"><img src="http://info.mercer.com/rs/mercer/images/blog.png" border="0" alt="Blog" width="17" height="17" /></a></p>
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table></div>
</td>
<td width="32">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div id="footer">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:center;">
<tr>
<td valign="top"><a href="#"><img src="http://info.mercer.com/rs/mercer/images/footer.png" width="568" height="45" alt="Mercer" border="0" style="display:block"></a></td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" >
<hr size="1" />
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
            yourself from this  distribution list, <a class="blue" href="mailto:marketing.reply@mercer.com?subject=Remove from distribution list"> please inform us.</a><br /> If you no longer wish to receive any Mercer e-mails, <a class="blue" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
           <br /> <br /> We welcome your thoughts and input.
                 If you would like to contact us with your comments, please <a class="blue" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
                 We encourage you to forward this e-mail, and to link to content on our site.
           <br /> <br /> <img src="http://image.exct.net/lib/fef91d70766d05/m/1/icon_more10x10bluewhite.gif" border="0" alt="More" width="10" height="10" />&nbsp;Find the <a class="blue" href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</span></span> <span 
style="font-size: xx-small;"><br /> </span></div>
<hr size="1" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a class="blue" href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a class="blue" href="http://www.mercer.com/privacy.jhtml"> Privacy</a> <br /> &copy;2013 Mercer LLC, All    
           Rights   Reserve</span></span></div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>