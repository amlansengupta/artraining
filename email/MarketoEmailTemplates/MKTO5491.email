<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Mercer BenefitsMonitor</title>
</head>
<body ><div align="center" class="mktEditable" id="body" ><table width="600" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF; border:#BFBFBF; border-style:solid; border-width:1px; border-collapse:collapse">
<tr>
<td>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="http://info.mercer.com/rs/mercer/images/WBEG_banner.jpg" alt="Banner" width="600" height="169" border="0"></td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="30">
<tr>
<td align="left" valign="top">
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 14pt; font-weight:400; margin-top: 0px; text-align: left; color: #4BACC6; text-transform:uppercase">Definitive Guidance on Benefits in 64 Countries</p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">Whether you operate in two countries or 20, staying competitive through the creation of effective, attractive, and statutorily correct benefit plans remains a challenge.<br>
<br>
Mercer's <strong>Worldwide Benefit &amp; Employment Guidelines</strong> (WBEG) helps you meet these challenges by providing access to statutory employee benefits, typical employer benefits, and overall employment conditions information in 64 countries. Click on the links below to learn more.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#4BACC6; font-weight:normal; text-transform:uppercase">
<tr>
<td align="left" valign="top"><a style="color:#4BACC6;font-weight:normal" href="#_COMPREHENSIVE_COVERAGE"><strong>Comprehensive Coverage</strong></a></td>
<td align="left" valign="top"><a style="color:#4BACC6;font-weight:normal" href="#_THE_WBEG-ONLINE_ADVANTAGE"><strong>The WBEG-Online Advantage</strong></a></td>
</tr>
<tr>
<td align="left" valign="top"><a style="color:#4BACC6;font-weight:normal" href="#_FORMATS_AND_PRICING"><strong>Formats and Pricing</strong></a></td>
<td align="left" valign="top"><a style="color:#4BACC6;font-weight:normal" href="#_HOW_TO_ORDER"><strong>How to Order</strong></a></td>
</tr>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight:400; margin-top: 30px; margin-bottom:0px; text-align: left; color: #4BACC6; text-transform:uppercase"><a name="_COMPREHENSIVE_COVERAGE" id="_COMPREHENSIVE_COVERAGE"></a><strong>Comprehensive coverage</strong></p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">Our global network of country experts gather and interpret current information, offering you in-depth data of unparalleled quality spanning over 500 subcategories.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse:collapse; font-size:10pt; font-family:Arial, Helvetica, sans-serif; color:#404040;">
<tr>
<td align="left" valign="middle" style="border:solid 1pt #4BACC6; border-left:none"><strong>Country overview</strong></td>
<td align="left" valign="top" style="border:solid 1pt #4BACC6; border-right:none; border-left:none">
<ul type="circle">
<li>Summary of contributions</li>
<li>Legislative updates</li>
<li>Trends</li>
</ul>
</td>
</tr>
<tr>
<td align="left" valign="middle" style="border-right:solid 1pt #4BACC6; border-bottom:solid 1pt #4BACC6"><strong>Statutory benefits</strong></td>
<td align="left" valign="top" style="border-bottom:solid 1pt #4BACC6;">
<ul type="circle">
<li>Social security</li>
<li>Retirement</li>
<li>Death</li>
<li>Disability (short- and long-term)</li>
<li>Medical and dental</li>
<li>Maternity/paternity</li>
<li>Unemployment</li>
<li>Social allowances</li>
</ul>
</td>
</tr>
<tr>
<td align="left" valign="middle" style="border-right:solid 1pt #4BACC6; border-bottom:solid 1pt #4BACC6"><strong>Typical benefits practices</strong></td>
<td align="left" valign="top" style="border-bottom:solid 1pt #4BACC6;">
<ul type="circle">
<li>Retirement<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Defined benefit plans<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Defined contribution plans</li>
<li>Death, plus:<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Accidental death and dismemberment<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Business travel</li>
<li>Disability (short- and long-term)</li>
<li>Medical and dental</li>
<li>Flexible benefit programs</li>
<li>Perquisites and allowances</li>
</ul>
</td>
</tr>
<tr>
<td align="left" valign="middle" style="border-right:solid 1pt #4BACC6; border-bottom:solid 1pt #4BACC6"><strong>Employment Conditions</strong></td>
<td align="left" valign="top" style="border-bottom:solid 1pt #4BACC6;">
<ul type="circle">
<li>Severance and termination indemnities<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Individual termination<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Collective dismissal</li>
<li>Working time
<ul type="circle">
<li>Working hours</li>
<li>Overtime</li>
<li>Night work</li>
<li>Rest periods</li>
</ul>
</li>
<li>Annual vacation and other leave</li>
<li>Entry and residence rules<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Conditions of entry<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Employing expatriates</li>
<li>Employment contracts<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Types of contracts<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Invalid contracts</li>
<li>Rights and duties</li>
<li>Occupational health and safety<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Measuring health and safety<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Rules and implementation</li>
<li>Industrial relations<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Social partners<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Collective agreements<br>
&nbsp;&ndash;&nbsp;&nbsp;&nbsp;Industrial action</li>
</ul>
</td>
</tr>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight:400; margin-top: 30px; text-align: left; color: #4BACC6; text-transform:uppercase"><a name="_FORMATS_AND_PRICING" id="_FORMATS_AND_PRICING"></a><strong>Formats and Pricing</strong></p>
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">
<tr>
<td width="20%" align="left" valign="top"><img src="http://info.mercer.com/rs/mercer/images/WBEG_cover.jpg" alt="WBEG cover" width="100" height="123"></td>
<td width="80%" align="left" valign="top"><br>
We present WBEG in five regional volumes (the Americas, Asia Pacific, the Middle East and Africa, Central and Eastern Europe, and Western Europe), as individual countries, and through an annual, online subscription (see <a style="color:#4BACC6;font-weight:normal" href="#_SPOTLIGHT_ON:_WBEG-ONLINE"><strong>The WBEG-Online Advantage</strong></a> below).</td>
</tr>
<tr>
<td width="20%">&nbsp;</td>
<td width="80%" align="left" valign="top">
<table width="95%" border="0" cellspacing="0" cellpadding="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">
<tr>
<td width="64%" align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&nbsp;</td>
<td width="18%" align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; color:#4BACC6;"><strong>US$</strong></td>
<td width="18%" align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; color:#4BACC6;"><strong>&euro;</strong></td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Americas</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,400</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,030</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Asia Pacific</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,870</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,385</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Central and Eastern Europe</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,870</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,385</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Middle East and Africa</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,400</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,030</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Western Europe</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,870</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">1,385</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">Individual Country</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">370</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">275</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">All Regions (Full set of PDFs)</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">7,620</td>
<td align="right" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">5,640</td>
</tr>
<tr>
<td align="left" valign="top">WBEG-Online (Web)</td>
<td align="right" valign="top">9,900</td>
<td align="right" valign="top">7,330</td>
</tr>
<tr>
<td colspan="3" align="left" valign="top" style="border:none; border-bottom:solid 2px #4BACC6; font-size:10px; line-height:12px">WBEG-Online Includes access to the complete set of WBEG for up to five users within an organization. Each additional user (beyond the five allotted): US$500/&euro;375.</td>
</tr>
</table>
</td>
</tr>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight:400; margin-top: 30px; margin-bottom:0px; text-align: left; color: #4BACC6; text-transform:uppercase"><a name="_SPOTLIGHT_ON:_WBEG-ONLINE" id="_SPOTLIGHT_ON:_WBEG-ONLINE"></a><a name="_THE_WBEG-ONLINE_ADVANTAGE" id="_THE_WBEG-ONLINE_ADVANTAGE"></a><strong>The WBEG-Online Advantage</strong></p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">The online version of WBEG gives clients access to the most current information and provides a significant value compared to purchase of the global PDF. See all of the tool's great features by launching our <a style="color:#4BACC6;font-weight:normal" href="http://www.imercer.com/uploads/videofiles/dmi_wbegdemo_640x360.mp4"><strong>new demo</strong></a>.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;"><strong>Global PDF Edition</strong></td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;"><strong>Global Online Tool</strong></td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">June update of 64 countries.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&#x2714</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Ongoing legislative updates.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Ongoing social security contribution and ceiling updates.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Ongoing statutory benefits and employment conditions updates.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Compare statutory benefits and typical benefits.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&#x2714</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Search content across all countries.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Receive immediate access to new countries as soon as they come available.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Download information by country.</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">&nbsp;</td>
<td align="center" valign="top" style="border:none; border-bottom:solid 1px #4BACC6;">&#x2714</td>
</tr>
<tr>
<td align="left" valign="middle" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;">Annual price<br>
(Up to five online subscribers in an organization - same legal entity).</td>
<td align="center" valign="middle" style="border:none; border-bottom:solid 1px #4BACC6; border-right:solid 1px #4BACC6;"><strong>US$7,620<br>
&euro;5,640</strong></td>
<td align="center" valign="middle" style="border:none; border-bottom:solid 1px #4BACC6;"><strong>US$9,900<br>
&euro;7,330</strong></td>
</tr>
<tr>
<td align="left" valign="middle" style="border:none; border-bottom:solid 2px #4BACC6; border-right:solid 1px #4BACC6;">Additional online user (beyond the five allotted).</td>
<td align="center" valign="middle" style="border:none; border-bottom:solid 2px #4BACC6; border-right:solid 1px #4BACC6;"><strong>&ndash;</strong></td>
<td align="center" valign="middle" style="border:none; border-bottom:solid 2px #4BACC6;"><strong>US$500<br>
&euro;375</strong></td>
</tr>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight:400; margin-top: 30px; margin-bottom:0px; text-align: left; color: #4BACC6; text-transform:uppercase"><a name="_HOW_TO_ORDER" id="_HOW_TO_ORDER"></a><strong>How to Order</strong></p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">To order your copy today or to sign up for WBEG-Online visit the <a style="color:#4BACC6;font-weight:normal" href="http://www.imercer.com/products/wbeg.aspx"><strong>WBEG product page</strong></a>.</p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight:400; margin-top: 30px; margin-bottom:0px; text-align: left; color: #4BACC6; text-transform:uppercase"><strong>Promotion!</strong></p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color: #404040;">Purchase WBEG with any one of our other publications <a style="color:#4BACC6;font-weight:normal" href="http://www.imercer.com/uploads/Asia/pdfs/2014-DMIC-Brochure.pdf"><strong>here</strong></a> and get a 20% discount!<br>
<br>
Please <a style="color:#4BACC6;font-weight:normal" href="mailto:hrsolutions.ap@mercer.com"><strong>contact us</strong></a> if you have any queries.</p>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:30px; font-family:Arial, Helvetica, sans-serif; font-size: 8pt; color:#999; text-align:left;">&nbsp;</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" width="216" height="52" alt="Marsh & McClennan Companies"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" ><table style="table-layout: fixed;" border="0" cellspacing="0" cellpadding="0" width="601" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong>You are receiving this e-mail because you have registered at imercer.com and opted in to receive our communications or because our consultants have requested that this be sent to you.</strong> To view your registration details, <a style="color: #00a7c7; text-decoration: none;" href="https://winsts.mercer.com/iMercerSTS/Login.aspx?ReturnUrl=/iMercerSTS/Default.aspx?wa=wsignin1.0&amp;wtrealm=https%3a%2f%2fwww.imercer.com%2fSTSResponse.aspx&amp;languageId=6&amp;languageId=6">log in here</a>, or if you were forwarded this e-mail and would like to subscribe, please <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=regmember">register at imercer.com</a>. Registration is free, provides you with access to premium content, and allows you to tailor the site to your own interests.<br>
<br>
If you no longer wish to receive any Mercer e-mails, <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="https://imercer.az1.qualtrics.com/SE/?SID=SV_1BxbQDMkVU13Rbu">e-mail your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10">&nbsp;Find the <a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/aboutmercerlocation.htm">Mercer office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=term">Terms of use</a> | <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=privacy">Privacy</a><br>
&copy;2014 Mercer LLC. All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;">This email was sent to: {{lead.Email Address:Default=edit me}}<br>
This email was sent by:<br>
Mercer (Global Headquarters)<br>
1166 Avenue of the Americas New York New York 10036 USA<br>
<br>
Mercer (Singapore) Pte Ltd<br>
8 Marina View #09-08, Asia Square Tower 1, Singapore 018960<br>
Company Registration No. 197802499E</span></td>
</tr>
</tbody>
</table>
</div>
</body>
</html>