<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Marsh &amp; McLennan Companies - Your 2012 Annual Enrollment Materials Will Arrive Soon</title>

<style type="text/css">
img {border:0px}
body {
        margin: 0px;
        padding: 0px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000000;
}
a:link {color:#000000;}      /* unvisited link */
a:visited {color:#000000;}  /* visited link */
a:hover {color:#000000;
             text-decoration:underline;}  /* mouse over link */
a:active {color:#000000;}

table {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        line-height: 17px;
        color: #000000;
}
.head {
        color: #118b3f;
        font-size: 13px;
        font-weight: bold;
        padding-bottom: 2px;
                padding-right:4px;
}
li {
        padding-bottom: 5px;
}
.head2 {
        padding-bottom: 5px;
        color: #73bf44;
        font-weight: bold;
        font-size: 12px;
        text-transform: uppercase;
        padding-top: 10px;
}
    .bodyHead {
        padding-bottom: 5px;
        color: #43276D;
        font-weight: bold;
        font-size: 12px;
}
    .green {
        color: #118b3f;
        font-weight: bold;
        font-size: 12px;
}
.greenLt {
        color: #73bf44;
        font-weight: bold;
        font-size: 12px;
}
            .headSide {
        padding-bottom: 8px;
        color: #01592e;
        font-weight: bold;
        font-size: 12px;
        text-transform: uppercase;
}

.footnote {
        font-size: 10px;
        line-height: normal;
        padding-bottom: 5px;
        color: #666666;
}
    .grayBorder { 
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
        border-bottom: 1px solid #c2cad2; 
}
    .grayBordertop { 
        border-top: 1px solid #c2cad2; 
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
}
    .pRowTen {
        padding-bottom: 10px;
        }
.sidebar {
        background-color: #bddda3;
}
.small{
        font-size: 10px;
        color: #999999;
        padding-left: 45px;
        }
.pRowTen1 {padding-bottom: 5px;
}
.callout {      background-color: #bddda3;
        padding: 10px;
}
.headSide1 {    padding-bottom: 0px;
        color: #01592e;
        font-weight: bold;
        font-size: 12px;
        text-transform: uppercase;
}
.headquestion { color: #01592E;
        font-size: 14px;
        padding-bottom: 2px;
}
.imgleft {      padding-right: 10px;
}
.style3 {font-size: 14px; font-weight: bold; }
</style>
</head>
<body ><div style="font:14px tahoma; width:100%; " class="mktEditable" id="edit_text" ><div id="edit_text" class="mktEditable" style="font:14px tahoma; width:100%; " ><table border="0" cellspacing="0" cellpadding="0" width="615" align="center">
<tbody>
<tr>
<td class="grayBordertop" style="padding-top: 20px;" width="615" align="center" valign="middle"><img src="http://www.mercerhrs.com/email/mmc/268136/header.gif" alt="Annual Enrollment Begins Today" width="622" height="218" /></td>
</tr>
<tr>
<td class="grayBorder" width="615" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="613">
<tbody>
<tr>
<td width="43" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="43" height="1" /></td>
<td width="375" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="370">
<tbody>
<tr>
<td class="pRowTen" align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Dear Colleague,</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">At Marsh &amp; McLennan Companies, we want you to take an active role in preparing for and building your financial future. As part of our commitment to your total wellbeing, we invite you to participate in the <span class="green">Marsh &amp; McLennan Companies Supplemental Savings &amp; Investment Plan</span> (Supplemental Savings &amp; Investment Plan) for the 2014 plan year as part of your long-term savings strategy.</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Your opportunity to enroll in the Supplemental Savings &amp; Investment Plan is now here. <strong>The Annual Enrollment period begins today, and ends on Friday, December 13, 2013, at 4 p.m. Eastern time. If you'd like to participate in 2014, you must enroll during<br /> this period.</strong></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Even if you are currently participating in the Supplemental Savings &amp; Investment Plan, you <strong>must</strong> actively enroll to continue to participate <span style="white-space: nowrap;">in 2014.</span></td>
</tr>
<tr>
<td class="pRowTen" style="padding-right: 5px;" align="left" valign="top"><span class="head">Your Supplemental Savings &amp; Investment Plan Deferral and Marsh &amp; McLennan Companies 401(k) Savings &amp; Investment Plan (MMC 401(k) Plan) Contribution Rate&nbsp;Options</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">If you participate in the Supplemental Savings &amp; Investment Plan, you will not be permitted to make traditional after-tax contributions to the MMC 401(k) Plan. Also, once you enroll in the Supplemental Savings &amp; Investment Plan, you will not be able to change the following elections <span style="white-space: nowrap;">for 2014:</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="20" align="left" valign="top"><span class="style3">&bull;</span><br /> <img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="10" height="1" /></td>
<td style="padding-bottom: 10px;" width="354" valign="top">Your Supplemental Savings &amp; Investment Plan deferral rate.</td>
</tr>
<tr>
<td width="20" align="left" valign="top"><span class="style3">&bull;</span><br /> <img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="10" height="1" /></td>
<td style="padding-bottom: 10px;" width="354">Your MMC 401(k) Plan before-tax contribution rate, Roth 401(k) contribution rate, and catch-up and/or Roth catch-up contributions, <span style="white-space: nowrap;">if eligible.</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Your next opportunity to make changes to these elections will be during the Annual Enrollment period for the 2015 plan year, if you remain eligible. We realize that the Supplemental Savings &amp; Investment Plan can be a complex topic. Before making any financial decisions, you may wish to consult with a tax or financial <span style="white-space: nowrap;">planning advisor.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top"><span class="head">Ready to Enroll?</span><br /> If you're ready to enroll, sign in to PeopleLink <strong>(<a href="https://www.mmcpeoplelink.com" target="_new">www.mmcpeoplelink.com</a>)</strong>. Click <strong>MMC Supplemental Savings Plan</strong> via the Finances tab, and under Take Action in the right navigation bar, select <strong>Enroll, view, change benefits</strong>. From the myBenefits Overview homepage, click <strong>Marsh &amp; McLennan Supplemental Savings &amp; Investment Plan</strong> in the myWealth section. You can also call the Employee Service Center at +1 866 374 2662, any business day, from 8 a.m. to 8 p.m. Eastern time <span style="white-space: nowrap;">to enroll.</span></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Sincerely,</td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/268136/signature.gif" alt="Alexander P. Voitovich" width="153" height="32" /></td>
</tr>
<tr>
<td class="pRowTen" align="left" valign="top">Alexander P. Voitovich<br /> Executive Director, Global Benefits</td>
</tr>
<tr>
<td class="callout" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td><img class="imgleft" src="http://www.mercerhrs.com/email/mmc/question.gif" alt="question mark" width="27" height="27" align="absmiddle" /><span class="headquestion">SPEAK TO AN AYCO FINANCIAL PLANNER</span></td>
</tr>
<tr>
<td style="padding-top: 10px;">
<p>During Annual Enrollment, representatives of <strong>The Ayco Company, L.P.</strong> &mdash; a leading provider of financial education &mdash;<br /> will be available to counsel you. To speak to an Ayco financial planner, please call the Employee Service Center at<br /> +1 866 374 2662. These services are provided by the<br /> Company at no cost <span style="white-space: nowrap;">to you.</span></p>
<p>The Ayco Company, L.P. is not affiliated with Mercer, LLC or Marsh &amp; McLennan Companies, Inc. (and its direct and indirect subsidiaries), which do not provide <span style="white-space: nowrap;">investment advice.</span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding-top: 10px;" align="left" valign="top"></td>
</tr>
</tbody>
</table>
</td>
<td width="13" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="1" /></td>
<td width="170" align="center" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="170">
<tbody>
<tr>
<td align="left" valign="top">
<table class="sidebar" border="0" cellspacing="0" cellpadding="0" width="170">
<tbody>
<tr>
<td class="pRowTen" align="right" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="1" /></td>
<td><a class="mktNoTrack" style="border: none;" href="http://www.mercerhrs.com/flipbooks/mmc/M273355" target="_blank"><img src="http://www.mercerhrs.com/email/mmc/272861/sidebar_img2.gif" border="0" alt="Enrollment Guide" width="143" height="154" /></a><a href="http://www.mercerhrs.com/flipbooks/mmc/" target="_blank"></a></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="14" height="1" /></td>
</tr>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8" /></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8" /></td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="13" height="8" /></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><a class="mktNoTrack" style="border: none;" href="http://www.mercerhrs.com/flipbooks/mmc/M273355" target="_blank"><img src="http://www.mercerhrs.com/email/mmc/272861/view_guide.gif" border="0" alt="View Guide" width="143" height="40" /></a><a href="http://www.mercerhrs.com/flipbooks/mmc/" target="_blank"></a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="headSide1">ENROLLMENT RESOURCES FOR YOU</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="pRowTen">For detailed information about the Supplemental Savings &amp; Investment Plan &mdash; including enrollment instructions &mdash; please refer to the enrollment package that was recently mailed to your address on file. It contains a personalized letter with your current contribution rates for<br /> the MMC 401(k) Plan and<br /> deferral rates for the Supplemental Savings<br /> &amp; Investment Plan, as applicable.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="pRowTen">You can also find detailed information about the Supplemental Savings<br /> &amp; Investment Plan in the Benefits Handbook.<br /> Sign in to PeopleLink <strong>(<a class="mktNoTrack" href="https://www.mmcpeoplelink.com/">www.mmcpeoplelink.com</a>)</strong>, click <strong>MMC Supplemental Savings Plan</strong> via the Finances tab, and under Related Sites in the right navigation bar, select <strong><span style="white-space: nowrap;">Benefits Handbook.</span></strong></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="headSide1">Complete and<br /> Return Your Beneficiary Form</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="pRowTen">You should designate the beneficiary(ies) of your Supplemental Savings &amp; Investment Plan account<br /> by completing the<br /> <a href="http://www.mercerhrs.com/email/mmc/268136/MARSH_CV651215-011.pdf" target="_new">Supplemental Savings &amp; Investment Plan Beneficiary <span style="white-space: nowrap;">Designation Form</span></a>.<br /> <br /> For more information about beneficiary elections, please<br /> contact the Employee Service Center at<br /> <span style="white-space: nowrap;">+1 866 374 2662,</span> any business day, from 8 a.m. to 8 p.m. <span style="white-space: nowrap;">Eastern time.</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<div align="center"><span class="footnote"><img style="margin-top: 30px;" src="http://www.mercerhrs.com/email/mmc/272861/wellbeingatwork_logo.gif" alt="" width="170" height="75" /></span></div>
</td>
<td width="12" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1" /></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td colspan="3" align="left" valign="bottom">&nbsp;</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="615" align="center">
<tbody>
<tr>
<td class="small" align="left" valign="top"><br /> M272862&nbsp; 11/13</td>
<td align="right" valign="top">&nbsp;</td>
</tr>
</tbody>
</table></div>
<div>
<table width="78%" align="center">
<tbody>
<tr>
<td style="padding-left: 0px; font-size: 9px; padding-bottom: 5px; color: #666666; line-height: 17px; font-family: Arial,Helvetica,sans-serif;" width="50%" align="left" valign="top">
<p>This email was sent by:&nbsp; Mercer<br>
1 Investors Way Norwood, MA 02062 USA</p>
<p>We respect your right to privacy - <a href="http://www.mercer.com/privacy.htm" target="_blank">view our policy</a></p>
</td>
<td style="padding-right: 10px; font-size: 9px;" align="right"><img src="http://info.mercer.com/rs/mercer/images/MMC_horizontal_4c.png" alt="MMC_horizontal_4c.png" width="153" height="21"><br></td>
</tr>
<tr>
<td colspan="2" align="center">
<p><span style="font-family: Verdana; font-size: xx-small;"><br>
If you no longer wish to receive these emails, click on the following link: <a href="http://info.mercer.com/UnsubscribePage.html">Unsubscribe</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="font:14px tahoma; width:100%; " class="mktEditable" ></div>
</body>
</html>