<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>

<style type="text/css">
img {
        border:none;    
}
body {
        background-color: #eeeeee;
}
a:link {
        color: #FFFFFF;
        text-decoration: none;
}
a:visited {
        color: #FFFFFF;
        text-decoration: none;
}
a:hover {
        color: #FFFFFF;
         text-decoration: underline
}
a:active {
        color: #FFFFFF;
        text-decoration: underline;
}
</style>
<title>Mercer's Spotlight on the 2014 Australian Federal Budget</title>

<style type="text/css">
.ReadMsgBody { width: 100%; }
.ExternalClass { width: 100%; }
</style>
</head>
<body ><div style="font:14px tahoma; width:100%; " class="mktEditable" id="edit_text" ><table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="color: #777777; padding-top: 10px; padding-bottom: 10px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;" align="center" bgcolor="#f5f5f5"><img src="http://info.mercer.com/rs/mercer/images/spacer.png" alt="" width="1" height="1" /></td>
</tr>
<tr>
<td align="center" bgcolor="#f5f5f5">
<table border="0" cellspacing="0" cellpadding="0" width="650" bgcolor="#ffffff">
<tbody>
<tr>
<td width="30" align="left" valign="top"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/spacer.png" border="0" alt="" width="30" height="95" /></td>
<td width="590" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="590">
<tbody>
<tr>
<td width="180" align="left" valign="top"><a href="http://www.mercer.com.au/"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/header-logo.gif" border="0" alt="Mercer" width="180" height="95" /></a></td>
<td width="159" align="left" valign="top"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/spacer.png" border="0" alt="" width="159" height="95" /></td>
<td width="50" align="left" valign="top"><a href="http://www.mercer.com.au/mercerservices.htm?siteLanguage=1012" target="_blank"><img style="display: block;" title="Talent" src="http://info.mercer.com/rs/mercer/images/header-talent.gif" border="0" alt="Talent" width="50" height="95" /></a></td>
<td width="52" align="left" valign="top"><a href="http://www.mercer.com.au/mercerservices.htm?siteLanguage=1012" target="_blank"><img style="display: block;" title="Health" src="http://info.mercer.com/rs/mercer/images/header-health.gif" border="0" alt="Health" width="52" height="95" /></a></td>
<td width="75" align="left" valign="top"><a href="http://www.mercer.com.au/mercerservices.htm?siteLanguage=1012" target="_blank"><img style="display: block;" title="Retirement" src="http://info.mercer.com/rs/mercer/images/header-retirement.gif" border="0" alt="Retirement" width="75" height="95" /></a></td>
<td width="74" align="left" valign="top"><a href="http://www.mercer.com.au/mercerservices.htm?siteLanguage=1012" target="_blank"><img style="display: block;" title="Investments" src="http://info.mercer.com/rs/mercer/images/header-investments.gif" border="0" alt="Investments" width="74" height="95" /></a></td>
</tr>
</tbody>
</table>
</td>
<td width="30" align="left" valign="top"><img src="http://info.mercer.com/rs/mercer/images/spacer.png" alt="" width="30" height="95" /></td>
</tr>
<tr>
<td colspan="3" align="left" valign="top"><img style="display: block;" title="Spotlight on the 2014 Australian Federal Budget" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-header-banner.jpg" border="0" alt="Spotlight on the 2014 Australian Federal Budget" width="650" height="203" /></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="left" valign="top"><strong style="color: #0080b4; font-size: 16px;"><br /> Dear {{lead.First Name:default=client}},</strong>
<p>Last night&rsquo;s 2014 Federal Budget was the first budget of a new government, and it sent a very clear message that Australia must shift to a more self-reliant society.</p>
<p>In our annual review of the Federal Budget, Mercer puts a spotlight on what this shift means for Australia, Australian businesses and investments, highlighting a range of challenges and opportunities.</p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="left" valign="top"><br /> <a href="#" target="_blank"><img title="2014 Federal Budget - Read our Report" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-spotlight-image-left.jpg" border="0" alt="2014 Federal Budget - Read our Report" width="425" height="246" /></a><a href="http://www.mercer.com.au/referencecontent.htm?idContent=1600850" target="_blank"><img title="Watch our Video" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-spotlight-image-right.jpg" border="0" alt="Watch our Video" width="225" height="246" /></a><br /> <br /></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="left" valign="top">Our assessment includes a view that:  
<ul>
<li>the Investment in innovation, the increase in the pension eligibility age to 70 and the reduction in some welfare programs will reshape our workforce;&nbsp;</li>
<li>changes to our retirement savings system will require people to reassess their work and retirement savings plans; and</li>
<li>the $50 billion investment in infrastructure spending over the next 6 years will expand the pipeline of assets available for private investment.</li>
</ul>
<p>My team has put together the following information to help you navigate the challenges and opportunities your businesses and your customers now face.</p>
<p>Please don&rsquo;t hesitate to provide us with your feedback or comment via <a href="http://www.linkedin.com/company/mercer"><span style="color: #006d9e;">our Linked In page</span></a>.</p>
</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="left" valign="top"><a href="http://stg.mercer.com.au/referencecontent.htm?idContent=1600850#infographic" target="_blank"><img title="Beyond the Budget - View our Infographic" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-infographic-cta1.jpg" border="0" alt="Beyond the Budget - View our Infographic" /></a></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="left" valign="top">Regards,<br /> <strong>David Anderson<br /> Managing Director &amp; Market Leader, Pacific</strong><br /></td>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="left" valign="top"><a href="http://www.superthinking.com"><img style="display: block;" title="SuperThinking" src="http://info.mercer.com/rs/mercer/images/ST-footer-budget2014.png" border="0" alt="SuperThinking" width="650" height="50" /></a></td>
</tr>
<tr>
<td align="left" valign="top" bgcolor="#006098">&nbsp;</td>
<td style="color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="left" valign="top" bgcolor="#006098">
<table border="0" cellspacing="0" cellpadding="0" width="590">
<tbody>
<tr>
<td align="left" valign="top"></td>
<td><a href="{{system.forwardToFriendLink}}"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-footer-forward.gif" border="0" alt="Forward to a friend" width="47" height="55" /></a></td>
<td><span style="font-family: arial,helvetica,sans-serif;"><a style="color: #ffffff; text-decoration: none;" href="{{system.forwardToFriendLink}}"><span style="font-size: 12px;">FORWARD EMAIL<br /> to a friend</span></a></span></td>
<td align="left" valign="top"></td>
<td><a href="mailto:contact.australia@mercer.com" target="_blank"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-footer-email.gif" border="0" alt="Email us at: ausfederalbudget@mercer.com" width="47" height="55" /></a></td>
<td><span style="font-family: arial,helvetica,sans-serif;"><a style="color: #ffffff; text-decoration: none;" href="mailto:contact.australia@mercer.com"><span style="font-size: 12px;">EMAIL US AT<br /> contact.australia@mercer.com</span></a></span></td>
<td align="left" valign="top"></td>
<td><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-footer-twitter.gif" border="0" alt="Follow us on Twitter @MercerAU" width="47" height="55" /></td>
<td><span style="color: #ffffff; font-family: arial,helvetica,sans-serif; font-size: 12px;">FOLLOW US ON<br /> <a style="color: #ffffff; text-decoration: none;" href="https://twitter.com/MercerAu" target="_blank"><img title="Twitter @mercerau" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-twitter_icon.jpg" border="0" alt="Twitter @mercerau" width="12" height="11" />&nbsp;Twitter</a> &nbsp;| <a style="color: #ffffff; text-decoration: none;" href="http://www.linkedin.com/company/mercer" target="_blank"><img title="LinkedIn - Mercer" src="http://info.mercer.com/rs/mercer/images/Fed-Budget-2014-linkedin_icon.jpg" border="0" alt="LinkedIn - Mercer" width="12" height="11" />&nbsp;LinkedIn</a></span></td>
</tr>
</tbody>
</table>
</td>
<td align="left" valign="top" bgcolor="#006098">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="left" valign="top" bgcolor="#f5f5f5"><img style="display: block;" src="http://info.mercer.com/rs/mercer/images/spacer.png" border="0" alt="" width="650" height="10" /></td>
</tr>
<tr>
<td style="color: #777777; font-family: Arial, Helvetica, sans-serif; font-size: 11px;" colspan="3" align="left" valign="top" bgcolor="#f5f5f5">
<p><span style="color: #808080; font-size: 11px;"><span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;" lang="EN-CA">This email has been prepared by Mercer Consulting (Australia) Pty Ltd (MCAPL) ABN 55 153 168 140, Australian Financial Services</span> <span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">Licence</span> <span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;" lang="EN-CA">#411770.</span> <span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;" lang="EN-CA">&lsquo;MERCER&rsquo; is a registered trademark of Mercer (Australia) Pty Ltd ABN 32 005 315 917.</span></span></p>
<p><span style="color: #808080; font-family: Arial,Helvetica,sans-serif; font-size: 11px; font-style: normal;"><span style="font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; font-size: 11px;" lang="EN-CA">Copyright 2014 Mercer LLC. All rights reserved.</span> </span></p>
<p><span style="color: #808080; font-family: Arial,Helvetica,sans-serif; font-size: 11px; font-style: normal;">If you want to remove yourself from this distribution list, <a href="mailto:contact.australia@mercer.com?subject=Remove%20from%20distribution%20list"><span style="color: #006d9e;">please inform us</span></a>.<br /> If you no longer wish to receive any Mercer e-mails, <a href="http://info.mercer.com/UnsubscribePage.html"><span style="color: #006d9e;">unsubscribe&nbsp;here</span></a>.</span></p>
<p><span style="color: #888888;"><span style="font-family: Arial; font-size: 8pt;">We</span> <span style="font-family: Arial; font-size: 8pt;">respect your privacy. Any information we collect from you will be handled in accordance with our <a href="http://www.mercer.com.au/privacy.htm"><span style="color: #006d9e;">Privacy Policy</span></a></span></span><span style="color: #888888; font-family: Arial; font-size: 8pt;">.</span></p>
<p><span style="color: #808080; font-family: Arial,Helvetica,sans-serif; font-size: 11px; font-style: normal;"><img src="http://image.exct.net/lib/fefc1d70716204/m/1/icon_more10x10bluewhite.gif" alt="image" width="10" height="10" />&nbsp; Find the <a href="http://www.mercer.com/aboutmercerlocation.jhtml"><span style="color: #006d9e;">Mercer&nbsp;office</span></a> near you.</span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</body>
</html>