<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Role Analysis and Job Evaluation (IPE) Workshop</title>
</head>
<body ><div ><div class="mktEditable" ><div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="715">
<tbody>
<tr>
<td>
<table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td><img src="http://info.mercer.com/rs/mercer/images/ROLE-ANALYSIS-AND-JOB-EVALUATION-(IPE)-Banner.jpg" border="0" alt="Role Analysis and Job Evaluation (IPE) Workshop" width="713" height="267" /></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding-left: 32px; padding-right: 10px; border-right: solid 1px #e6eaef;" width="420" align="left" valign="top">
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; text-align: left; margin-top: 0px;">Dear Client,</p>
<p style="font: 11pt/14pt Arial, Helvetica, sans-serif, bold; color: #002c77; text-align: center; margin-top: 20px; margin-bottom: 20px;"><strong>Do you often need to compare job positions and<br /> benchmark compensation across regions?</strong></p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; text-align: left; margin-top: 0px;">If so, our <strong>Role Analysis and Job Evaluation (IPE) workshop</strong> in November will be useful for you and your colleagues.<br /> <br /> At our 2-day session, learn to properly administer and implement position evaluations across job families, within any type of organization, anywhere in the world.</p>
<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight: 400; margin-top: 30px; text-align: left; color: #002c77; text-transform: uppercase;"><strong>More about the IPE workshop:</strong></h2>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002c77"><strong style="color: #ffffff;">Workshop details</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb"><strong>Date:</strong> 11 - 12 Nov 2014 (2 days)</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb"><strong>Time:</strong> 9:00 am to 5:00 pm<br /> (registration at 8:30 am)</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb"><strong>Venue:</strong> The Gardens Hotel, Kuala Lumpur, Malaysia</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px;" align="left" valign="top" bgcolor="#fbfbfb">Refreshments at lunch and tea breaks will be provided</td>
</tr>
</tbody>
</table>
<p style="margin-top: 0px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002c77"><strong style="color: #ffffff;">The workshop will cover:</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb">What job evaluation is</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb">Introduction to the job framework and methods used for job evaluation</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb">How job evaluation results can be used to make strategic HR decision, and how this impacts pay outcomes</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb">Mercer's International Position Evaluation (IPE&trade;) methodology</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#fbfbfb">Why and how to conduct role analysis</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="padding-top: 8px;" align="left" valign="top" bgcolor="#fbfbfb">Alternative approaches to documenting roles</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#fbfbfb">&bull;</td>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px;" align="left" valign="top" bgcolor="#fbfbfb">Introduction to creating job descriptions</td>
</tr>
</tbody>
</table>
<p style="margin-top: 0px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002c77"><strong style="color: #ffffff;">Registration is simple</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#fbfbfb">Complete the order form in the <a style="color: #006d9e;" href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf">brochure</a> and</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="11%" align="right" valign="top" bgcolor="#fbfbfb"><strong>1.</strong></td>
<td style="padding-top: 8px;" width="89%" align="left" valign="top" bgcolor="#fbfbfb"><strong>Scan and email to <a style="color: #006d9e;" href="mailto:juliana.philip@mercer.com">juliana.philip@mercer.com</a></strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" align="right" valign="top" bgcolor="#fbfbfb"><strong>2.</strong></td>
<td style="padding-top: 8px;" width="89%" align="left" valign="top" bgcolor="#fbfbfb"><strong>Or, mail the form to<br /></strong> B-9-1, Level 9, Tower B, Menara UOA Bangsar, No.5, Jalan Bangsar Utama 1 59000 Kuala Lumpur</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#fbfbfb">Our consultants will be in touch to confirm details of your registration.</td>
</tr>
</tbody>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a; margin-left: 5px; margin-top: 40px;">For any queries on the workshop, please contact <a style="color: #006d9e;" href="mailto:juliana.philip@mercer.com">Juliana Philip</a>.<br /> <br /> Thank you.</p>
</td>
<td style="padding-left: 10px; padding-right: 22px;" width="222" align="left" valign="top">
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#e6eaef"><strong style="color: #002c77; text-transform: uppercase;">Who should attend</strong></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/12pt Arial, Helvetica, sans-serif, bold; color: #37424a; text-align: left; margin-top: 20px; margin-bottom: 20px;">All HR leaders and practitioners:</p>
<table style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="100%">
<tbody>
<tr>
<td style="font-size: 18pt; padding-bottom: 10px;" width="9%"><strong>&middot;</strong></td>
<td style="padding-bottom: 10px;" width="91%">Who would like to learn more about role analysis and job evaluation</td>
</tr>
<tr>
<td style="font-size: 18pt;"><strong>&middot;</strong></td>
<td>Whose organizations are undergoing role analysis / job evaluation exercises</td>
</tr>
</tbody>
</table>
<p style="margin: 0; margin-bottom: 20px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#e6eaef"><strong style="color: #002c77; text-transform: uppercase;">Download the IPE Brochure<br /> and order form</strong></td>
</tr>
</tbody>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 14pt; margin-top: 30px; text-align: center; color: #37424a; margin-bottom: 40px;"><a href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf"><img src="http://info.mercer.com/rs/mercer/images/2014-Role-Analysis-and-Job-Evaluation_cover.jpg" border="0" alt="2014 IPE Flyer Cover" width="86" height="122" /></a></p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#e6eaef"><strong style="color: #002c77; text-transform: uppercase;">Workshop fees for 2 days</strong></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/12pt Arial, Helvetica, sans-serif, bold; color: #37424a; text-align: left; margin-top: 20px; margin-bottom: 40px;"><strong>Public participant:</strong><br /> USD 1,300<br /> <br /> <strong>Mercer member &amp; client:</strong><br /> USD 1,100<br /> <br /> <strong>Sign up for 3 or more persons at only USD 900 per participant!</strong><br /> <br /> <em>*Prices exclude 6% GST</em><br /> <br /> <strong style="color: #e36c0a;">Early bird package!</strong><br /> Sign up by 12 October to enjoy a further 10% off the listed prices above!</p>
<table border="0" cellspacing="0" cellpadding="8" width="60%" align="center">
<tbody>
<tr>
<td style="padding: 5pt; alignment-adjust: middle;" align="center" valign="middle" bgcolor="#e36c0a"><span style="color: #ffffff;"><span style="font-size: 13px; text-transform: uppercase; font-family: Arial, Helvetica, sans-serif;"><strong><a style="color: #ffffff;" href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf">Register</a> now!<br /> <span style="text-transform: none;">Seats are limited</span></strong></span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">&nbsp;</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div></div>
</div>
</body>
</html>