<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
</head>
<body ><div ><div class="mktEditable" id="edit_text_1" >
<div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="602">
<tbody>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="600">
<tbody>
<tr>
<td><a href="http://www.imercer.com/content/global-mobility-overview.aspx"><img src="http://www.imercer.com/content/GM/email/mmu/mobility-update-banner-1405.jpg" border="0" alt="News for Global Mobility Clients" width="600" height="169" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="30" valign="top">
<p>&nbsp;</p>
</td>
<td width="350" align="center" valign="top">
<h1 style="font: 400 14pt Arial, Helvetica, sans-serif; color: #6f83c1; margin-top: 0; margin-bottom: 20px; text-align: left;">Creating an Optimal Experience for Repatriating Assignees</h1>
<p style="font: 10pt Arial, Helvetica, sans-serif; color: #6f83c1; margin-bottom: 20px; text-align: left;">By Kerry Jantzen</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Despite the attention given to the expatriates' situation at the end of international assignments, returning assignees and their families are not the only ones facing difficulties.
 Employers have concerns, too, although the focus is quite different.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The employers' efforts generally revolve around policy issues, the potential need for negotiating a deal, assigning responsibility for the repatriation process, and finding the employee 
another position back home or elsewhere within the organization.
 The expatriate, on the other hand, worries about both immediate and long-term career issues, and resettling the family in the home country.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #6f83c1; margin-top: 0px; text-align: left;">▶ <a style="color: #595997;" href="http://www.imercer.com/content/create-optimal-experience-repatriating-assignees.aspx">Continue reading this article on imercer.com</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="location" name="location"></a>Location News</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Argentina</strong> &ndash;  Political and economic stability in <strong>Buenos Aires</strong> have been deteriorating for several months.
 A series of protests and strikes have occurred due to rising crime levels and inflation rates that exceed 30%.
 To resolve the foreign-currency shortage that Argentina has been experiencing during recent months, the peso was devalued in January.
 Government price controls were expanded in April and are now causing shortages of some goods.
 A wave of crime, including incidents of lynching, prompted the city's authorities to declare a 12-month state of emergency in April.
   Mercer is closely monitoring the evolving situation.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Bosnia-Herzegovina, Croatia, and Serbia</strong> &ndash;  Much of Bosnia-Herzegovina, as well as Serbia and parts of eastern Croatia, have been struck by 
severe floods causing loss of life, major damage to road and energy infrastructure, and damage or destruction of many buildings.
 News reports indicate that at least 35 people have died regionally since the flooding began in mid-May.
 Scores of residents have either been evacuated or have fled their homes.
 Outer suburbs of both <strong>Sarajevo</strong> and <strong>Belgrade</strong> experienced major flooding; more central areas of the cities were not as badly affected.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Nigeria</strong> &ndash;  The Nigerian government has introduced a new combined expatriate residence and aliens permit (CERPAC) effective 30 April with 
improved security features, according to published reports.
 Long-term expatriate residents should have applied for the new card by the same date, or risked deportation, while expatriate workers who already have a CERPAC which extends beyond 30 April will receive the new CERPAC upon expiry of their current permit.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">United Kingdom </strong>&ndash; Effective 6 August 2014, the UK Home Office has introduced automatic renewal of Certificate of Sponsorship (CoS) allocations 
and eliminated the annual renewal process licensed sponsor employers had to undergo.
 Employers issuing unrestricted CoS for Tier 2 skilled workers and Tier 5 temporary workers will be affected by the automation, although no action is required on their part.
 The change assumes a constant number of annual renewals, so employers should plan for CoS applications when additional staffing is needed.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Vietnam </strong>&ndash;  A circular from Vietnam's Ministry of Labour &ndash;  Invalids and Social Affairs maps out employer responsibilities when recruiting 
and hiring foreign workers.
 Under a decree that took effect 1 November 2013, foreigners qualifying for an employment license must be in a management role or have special skills in short supply locally.
 The new circular requires employers to submit a report to the local or provincial labor department 30 days in advance of recruiting foreign workers &ndash; the report must outline the demand for foreign labor, including work positions, quantity, professional qualifications, experience, salary, 
and work hours.
 The circular, which took effect 10 March, also describes the timing and process for work permit requests, and periodic reports employers must submit to their provincial labor department.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Tonga</strong> &ndash;  In April 2014, Tonga experienced an outbreak of the mosquito-borne chikungunya virus.
 Some reports indicate more than 10,000 people were affected across the country (almost 10% of the population), including the capital, <strong>Nuku'alofa</strong>.
 It is Tonga's first outbreak of this disease.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #6f83c1;">Yemen</strong> &ndash;  In <strong>Sana'a</strong>, violence against Western interests, including embassy personnel, has increased.
 On 4 May, gunmen killed a private French security agent working for the EU delegation and wounded two others &ndash; one a Frenchman and one a Yemeni.
 On 28 April, a German diplomat was injured during an apparent kidnapping attempt.
 The escalating number of drone strikes that inflict casualties on Yemeni bystanders and the presence of Islamist groups are likely to produce continuing high levels of political violence that may involve expatriates located here.</p>
<p style="font: italic 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more HR-related news from  around the world, subscribe to <a style="color: #595997;" href="http://select.mercer.com/">Mercer Select</a>, a daily email with insights and analysis 
on a broad spectrum of benefit, compensation, and HR issues.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="col" name="col"></a>Cost of Living and Housing</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The March editions of the Cost-of-Living reports are available on Global HRMonitor&reg; as of 5 May.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The Cost-of-Living and Housing overview,  which will cover the latest currency and price trends based on the March survey results, will be released on 2 June on Global HRMonitor.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The following locations have been added to Mercer standard coverage as of the March 2014 survey edition:</p>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Africa and Middle East</h3>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Burundi   &ndash; Bujumbura</li>
<li>Iraq  &ndash; Erbil*</li>
<li>Israel  &ndash; Jerusalem</li>
<li>Saudi Arabia  &ndash; Al Khobar/Damman</li>
<li> Saudi Arabia Medium city average</li>
</ul>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Americas</h3>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Brazil  &ndash; Manaus</li>
<li>Mexico &ndash; Ciudad del Carmen</li>
<li>United States  &ndash; Charlotte</li>
</ul>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Asia</h3>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li> Afghanistan  &ndash; Kabul</li>
<li>India &ndash; Hyderabad</li>
<li>Laos    &ndash; Vientiane</li>
<li> Nepal &ndash; Kathmandu</li>
</ul>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Europe</h3>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li> Croatia &ndash; Split</li>
<li>Cyprus &ndash; Nicosia</li>
<li>Czech Republic medium city average</li>
<li>Denmark &ndash; Aarhus</li>
<li> Germany &ndash; Mannheim</li>
<li>Montenegro &ndash; Podgorica</li>
<li>Poland  &ndash; Wroclaw</li>
<li> Spain &ndash; Seville</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">* One edition only</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; margin-bottom: 0px; text-align: left;"><a id="events" name="events"></a>Upcoming Events</h2>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #43276d; margin-top: 36px; text-align: left;"><em>Next in Our Webcast Series<br /> </em>Measuring the Value of International Assignments</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">This webcast will explore the value of measuring an organization's investment in mobility.
 We'll focus on key measures as well as pitfalls and important considerations for more in-depth mobility measurement.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia, Australia, and <br /> New Zealand</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">25 June<br /> 11 am Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsap.webex.com/mercereventsap/onstage/g.php?t=a&amp;d=967325303">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">25 June<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?t=a&amp;d=661290312">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">25 June<br /> 12 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=667109599">Register</a></td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">25 June<br /> 2 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=669805934">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #43276d; margin-top: 36px; text-align: left;">Expatriate Management Training</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer offers training courses for international assignment  specialists in various locations.
 Please visit <a style="color: #595997;" href="http://www.imercer.com/content/global-mobility-events.aspx">imercer.com/gmevents</a> to learn more  about training opportunities in your region.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="160">London (Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">10-11 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="160">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="160">Stockholm (Fundamentals)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">11-12 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="160">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="160">Zaragoza (Fundamentals)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">17 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="160">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="160">New York (Principles <br /> and Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">13-14 August</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/content/NA-expat-seminars.aspx">Register</a></td>
</tr>
</tbody>
</table>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="localplus" name="localplus"></a>Local Plus Surveys</h2>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #595997;">Do You Have Expatriates on Local Plus Packages?</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Take our <a style="color: #595997;" href="http://www.imercer.com/products/2014/local-plus-surveys.aspx">Local Plus Surveys</a> and get access to powerful data and resources to help improve 
your assignment policies and practices.
 Mercer&rsquo;s  Local Plus Surveys cover     compensation package elements and the application of Local Plus policies  among various types of globally mobile employees.
 These popular locations are covered in the surveys:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Australia</li>
<li>Brazil</li>
<li>Hong Kong</li>
<li>Mexico</li>
<li>Singapore</li>
<li>Switzerland</li>
<li>United Kingdom</li>
<li>United States</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Act soon to take advantage of special offers.
 Complete all eight location surveys and get a complimentary calculation from Mercer's Compensation Localizer, our upcoming Local Plus package calculator.
  The surveys  close on <strong>30 June</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/products/2014/local-plus-surveys.aspx">Participate 
Now</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="programinsights" name="programinsights"></a>Global Mobility Program Insights Report</h2>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #595997;">Do You Know How Your Mobility Program Compares to Those of Your Peers?</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="color: #595997;" href="http://www.imercer.com/products/2014/mobility-program-insights.aspx">Global Mobility  Program Insights Report</a>, our new diagnostic analysis, gives you an overview  of how 
your mobile talent distribution measures up to peer groups and  whether your policy segmentation is aligned with what your peers are doing.
 The report draws on Mercer's extensive benchmarking database to examine the  following assignment types:</p>
<ol style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Long-term</li>
<li>Short-term</li>
<li>Developmental/training</li>
<li>Local Plus</li>
<li>Intra-regional</li>
<li>Global nomad </li>
</ol>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #6f83c1; padding: 4px; color: #595997; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/products/2014/mobility-program-insights.aspx">Learn 
More</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="promo6" name="promo6"></a>Global Car Policies</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The 2014 <a style="color: #595997;" href="http://www.imercer.com/products/2014/global-car-policies.aspx">Global Car Policies</a> report keeps HR leaders  abreast of current policy trends and 
competitive market data to support their  corporate car planning and budget allocations in 90 markets.
 The report:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">
<li>Enables organisations to review company car policies against  local markets.
 </li>
<li>Highlights differences in car policy practices across key  markets.
 </li>
<li>Identifies cost-saving measures.</li>
</ul>
</td>
<td style="border-right: solid 1px #bfbfbf;" width="15" valign="top">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td valign="top">
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-right: 0px; margin-bottom: 12px; margin-left: 0px; text-align: left;">May 2014 &bull; Highlights</h6>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#">Creating an Optimal Experience for Repatriating Assignees</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#location">Location News</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#col">Cost of Living and Housing</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#events">Webcasts, Conferences, and Training</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#localplus">Local Plus Surveys</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#programinsights">Global Mobility Program Insights Report</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #595997; text-decoration: none;" href="#promo6">Global Car Policies</a></td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Doing Business in&hellip;<br /> <span style="color: #595997;">China</span></h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><em>In this new section we feature tips for understanding cultural differences while working and living abroad,  from the <a style="color: #595997;" 
href="http://www.imercer.com/content/cultural-training-passport.aspx">CulturalTrainingPassport&trade;</a> online training tool.</em></p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">1.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">In order to get to know you, the Chinese may ask questions that seem too personal.
  If you're not comfortable answering,  try a humorous reply  or subtle change of subject.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">2.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">In business meetings, greet the oldest or most senior person first.
 A gentle handshake is the most common  greeting.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #6f83c1;">3.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Leave a little food on your plate to show the host you have been  fed well.</td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Exchange Rates and Inflation</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Exchange rates on Global HRMonitor&reg; were updated on 5 May.
 The next update will be 5 June.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Tax and Spendable Income</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The Tax Reports and the tax data used by the Hypothetical Tax Calculator, the Cost-of-Living Allowance Calculator, and the Expatriate Compensation Calculator, as well as the Spendable Income 
data have been updated for the following countries:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding: 0px; list-style: none; text-align: left;">
<li>Austria</li>
<li>Chile</li>
<li> Latvia</li>
<li> Malaysia</li>
<li> South Korea &amp; Expatriate</li>
<li> Switzerland (Basel &amp; Expatriate) </li>
<li>Switzerland (Bern &amp; Expatriate)</li>
<li> Switzerland (Geneva &amp; Expatriate)</li>
<li> Switzerland (Vaud &amp; Expatriate)</li>
<li> Switzerland (Zug &amp; Expatriate) </li>
<li>Switzerland (Zurich &amp; Expatriate)</li>
</ul>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">MercerPassport&reg;</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The following locations  have been updated on MercerPassport&reg;:</p>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Employee Mobility Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Austria (Vienna)</li>
<li>China &ndash; Central (Chengdu, Wuhan, Xi'ian)</li>
<li> China &ndash; East (Jinan, Nanjing, Shanghai, Suzhou, Wuxi)</li>
<li>China &ndash; North (Beijing, Dalian, Qingdao, Shenyang, Tianjin)</li>
<li>China &ndash; South (Guangzhou, Shenzhen)</li>
<li>Czech Republic (Prague)</li>
<li>Mexico (Mexico City, Monterrey, Poza Rica, Reynosa, Villahermosa)</li>
<li>Netherlands (Amsterdam, The Hague, Rotterdam)</li>
<li>Philippines (Manila)</li>
<li> Poland (Warsaw)</li>
<li>Slovakia (Bratislava)</li>
<li> United Kingdom &ndash; Central (Birmingham, Macclesfield) </li>
<li>United Kingdom &ndash; North (Belfast, Bradford, Manchester, Liverpool)</li>
<li> United Kingdom &ndash; Scotland (Aberdeen, Glasgow, Paisley) </li>
<li>United Kingdom &ndash; South (London, Oxford)</li>
<li>USA &ndash; Central (Chicago, Cincinnati, Detroit, Houston, Minneapolis)</li>
<li>USA &ndash; East (Atlanta, Boston, Miami, New York, Philadelphia, Washington DC)</li>
<li>USA &ndash; West (Los Angeles, Portland, San Diego, San Francisco, Seattle) </li>
</ul>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Start Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Algeria (Algiers, Oran) </li>
<li>Bangladesh (Dhaka)</li>
<li> Benin* (Cotonou)</li>
<li>Botswana* (Gaborone)</li>
<li> Dem.
 Rep.
 of Congo (Kinshasa)</li>
<li>Dominican Republic* (Santo Domingo)</li>
<li>Gabon* (Libreville)</li>
<li>Ghana (Accra)</li>
<li>Guinea (Conakry)</li>
<li>Mongolia (Ulaanbaatar)</li>
<li>Nicaragua* (Managua)</li>
<li>Sudan (Khartoum) </li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">* New location for 2014</p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Start Guides are &lsquo;lite&rsquo; versions of the Employee Mobility  Guides; these country reports outline procedures and requirements for visiting  or relocating to up-and-coming 
locations.</p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more information about MercerPassport,  visit <a style="color: #595997;" href="http://www.imercer.com/content/mercer-passport-home.aspx">imercer.com</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Get the Latest Mobility Tips</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Follow  us on Twitter <a style="color: #595997;" href="http://www.twitter.com/mercermobility">@MercerMobility</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #6f83c1; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Words.
 Pictures.
 Insights.</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><a style="color: #595997;" href="http://mthink.mercer.com/global-car-policies-a-key-talent-driver/">Global Car Policies: A Key Talent Driver</a></p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><a href="http://mthink.mercer.com/global-car-policies-a-key-talent-driver/"><img src="http://www.imercer.com/content/GM/email/mmu/140416-mercer-129-carpolicies-thumb.png" border="0" 
alt="MercerThink Infographic" width="160" height="724" /></a></p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">MercerThink  is a weekly infographic of HR-related trends around the world.
 Sign up at <a style="color: #595997;" href="http://mercerthink.mercer.com/">mercerthink.mercer.com</a>.</p>
<p style="font-size: 8pt; line-height: 10pt;">&nbsp;</p>
</td>
<td width="30" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">5160</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div id="column_text" class="mktEditable" align="center">
<table border="0" cellpadding="0" width="608">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
                 yourself from this  distribution list, <a style="color: #595997;" href="mailto:marketing.reply@mercer.com?subject=Remove from distribution list"> please inform us.</a><br /> If you no longer wish to receive any Mercer e-mails, <a style="color: #595997;" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
                <br /> <br /> We welcome your thoughts and input.
                      If you would like to contact us with your comments, please <a style="color: #595997;" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
                      We encourage you to forward this e-mail, and to link to content on our site.
                <br /> <br />Find the <a style="color: #595997;" href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</span></span> <span style="font-size: xx-small;"><br /> </span></div>
<hr style="color: #bfbfbf;" size="1" noshade="noshade" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a style="color: #595997;" href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a style="color: #595997;" href="http://www.mercer.com/privacy.jhtml"> Privacy</a> <br /> 
&copy;2014 Mercer LLC, All                    Rights   Reserved</span></span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</body>
</html>