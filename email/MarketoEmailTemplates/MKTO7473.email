<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
img {
        border:none;    
}
</style>
<title></title>
</head>
<body ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td valign="top" ><div class="mktEditable" id="pillars" ><a href="#"><img src="http://info.mercer.com/rs/mercer/images/cemailLogo.png" width="601" height="63" alt="Mercer" border="0" style="display:block"></a></div>
</td>
</tr>
<tr ><td ><div class="mktEditable" id="header" ><img src="https://info.mercer.com/rs/mercer/images/M000241-601x205.jpg" alt="" height="205" width="601"></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
<tr ><td width="32">&nbsp;</td>
<td valign="top" width="365" ><div class="mktEditable" id="content" ><table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;">
<p><span style="font-size: 18px;"><span style="color: #002b79; font-family: Arial,Helvetica,sans-serif; text-transform: uppercase;">PREPARE FOR UK PENSION CHANGES IN 2015<br /></span></span></p>
<div>As a multinational employer you may be aware UK occupational pensions are being radically changed. In March 2014 the UK Budget announcement included proposals allowing members of defined contribution (DC) plans complete flexibility over how they take their benefits at retirement. These changes to pensions are the biggest we have seen in the UK for decades.</div>
<br />
<div><br /><strong>Government consultation</strong><br /><br />It has been confirmed that transfers from Defined Benefit (DB) to Defined Contribution (DC) pension plans can continue (with the exception of transfers from unfunded public sector schemes).&nbsp; This can only increase demand from members of DB plans for the same flexibility as members of DC plans will enjoy from April 2015 - including the ability to use their retirement funds however they choose, even taking them all as cash subject to tax</div>
<div></div>
<p><strong>What do you need to consider following the consultation?</strong></p>
<p>All UK employers with pension plans are now busily evaluating how  these changes impact their own pension plans, and ensuring that they are  ready for April 2015.        Have you started preparing your UK pension  plan for 2015?</p>
<p>The Budget and the government&rsquo;s consultation response raise a number of strategic questions for the company around benefit provision and DB pensions and here are a few examples to consider:</p>
<ul>
<li>What is the right level of pension provision (if any) given the continued blurring of barriers between DB, DC and other forms of savings &amp; investment, such as Individual Savings Accounts (ISAs)?</li>
<li>What place should retirement provision have within the overall employer value proposition and what would best support the employee behaviour that the Company wishes to influence?</li>
<li>Should the approach differ between categories of employee? For example should a degree of financial advice/guidance, tax planning support and other services be part of the package for senior executives?</li>
<li>The Budget introduces the possibility of additional flexibility for members of DB plans.&nbsp; What opportunities does this create for managing the plan&rsquo;s cost and risk?</li>
</ul>
<p><strong>What are your peers considering?</strong><br />To give you an idea of the areas that our UK clients are considering addressing prior to April 2015 you can view the<a href="http://uk.mercer.com/insights/point/2014/budget-2014-infographic.html"> infographic</a> from our latest client survey here.        <br /><br /><strong>Where can I get more information?</strong><br />Read more detail on the topic and the key points of action on Mercer&acute;s<a href="http://uk.mercer.com/insights/focus/mercer-budget-2014-views-insights-and-guidance.html"> website.</a><br /><br /></p>
<p><strong>Best Regards</strong><br />Eric Huitzing and Alex Bateman<br />Retirement &amp; International Consulting<br />Mercer Nordics<br /><br /></p>
</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table></div>
</td>
<td width="20">&nbsp;</td>
<td width="20" style="border-left:1px solid #d3f0f6">&nbsp;</td>
<td width="133" valign="top" ><div class="mktEditable" id="content2" >
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;">
<div><span style="color: #00a7c7; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;">HEAR OUR EXPERTS' VIEW ON THE UK PENSION CHANGES</span> <span style="font-size: 11px; color: #000000;">&nbsp;</span></div>
<div><span style="font-size: 11px; color: #000000;"><br /></span></div>
<div><span style="font-size: 11px;"><span style="color: #000000;">During the 2nd quarter of 2014 the government consulted on the details of the UK Budget announcement and issued its response in July.
     Fiona Dunsire, Mercer's CEO in the UK and a team of Mercer experts then discussed the wide reaching implications of this response and also explored some of the potential issues.
            Download the<a href="http://www.mercer.com/content/dam/mercer/attachments/global/webcasts/140725_WB_Freedom_of_choice_in_pensions.pdf"> slides </a>and listen to the recording of this <a 
href="http://www.mercer.com/events/webcasts/freedom-and-choice-in-pensions-budget-webcast.html">webinar.</a></span></span><br /> <br /> <span style="color: #00a7c7; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;">MERCER CONTACT FOR YOUR NEXT STEP</span><br 
/><br /> <span style="font-size: 11px;"><span style="color: #000000;">Reach out to your local Mercer consultant in the UK or to <strong>Alex Bateman</strong> who is based in Mercer Sweden on email</span> <a href="mailto:alex.bateman@mercer.com">alex.bateman@mercer.com</a> <span style="color: 
#000000;">or +46 8 505 308 69 to  consider the implications for your company and identify any      areas  that may be    worth exploring further.</span></span></div>
<div><span style="color: #000000; font-size: 11px;"><br style="color: #000000; font-size: 11px;" /></span></div>
<div><span style="color: #000000; font-size: 11px;"> </span><br /> <br /><br /> <br /> <span style="font-size: 12px; color: #00a7c7;">&nbsp;</span></div>
</td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
</td>
<td width="32">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div id="footer">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:center;">
<tr>
<td valign="top"><a href="#"><img src="http://info.mercer.com/rs/mercer/images/footer.png" width="568" height="45" alt="Mercer" border="0" style="display:block"></a></td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" >
<hr size="1" />
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
                yourself from this  distribution list, <a href="mailto:info.dk@mercer.com"><span class="blue"> </span><span class="blue">please inform us</span></a>.<a class="blue" href="mailto:info.no@mercer.com?subject=Remove from   distribution list"></a><br /> If you no longer wish to receive  
 any Mercer e-mails, <a class="blue" href="http://info.mercer.com/UnsubscribePage.html">unsubscribe      here</a>.
               <br /> <br /> We welcome your thoughts and input.
                     If you would like to contact us with your comments, please <span class="blue"> </span><a href="mailto:info.dk@mercer.com"><span class="blue">email your feedback</span></a>.
                     We encourage you to forward this e-mail, and to link to content on our site.
               <br /> <br /> <img src="http://image.exct.net/lib/fef91d70766d05/m/1/icon_more10x10bluewhite.gif" border="0" alt="More" width="10" height="10" />&nbsp;Find the <a class="blue" href="http://www.mercer.com/about-us/locations.html">Mercer office</a> near you.</span></span> <span 
style="font-size: xx-small;"><br /> </span></div>
<hr size="1" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a class="blue" href="http://www.mercer.com/terms.html">Terms of use</a> | <a class="blue" href="http://www.mercer.com/privacy.html"> Privacy</a> <br /> &copy;2014 Mercer LLC, All           
        Rights   Reserve</span></span></div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>