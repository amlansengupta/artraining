<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>

<style type="text/css">

body {
        margin: 0px;
        padding: 0px;
        font-family:Verdana, Geneva, sans-serif;
        font-size: 11px;
        line-height: 17px;
        color: #000000;
}
a:link {
        color: #00afd2; 
        font-weight: bold;  
        text-decoration: none;
}    
a:visited {
        color: #00afd2; 
        font-weight: bold;  
        text-decoration: none;
} 
a:hover {
        color: #d56b37; 
        font-weight: bold;  
        text-decoration: none;
}  
a:active {
        color: #d56b37; 
        font-weight: bold; 
        text-decoration: none;
} 
table {
        font-size: 11px;
        line-height: 17px;
        color: #000000;
}
.grayBorder { 
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
}
.head {
        font-style: italic;
        color: #43276D;
}
.head2 {
        text-transform: uppercase;
        padding-bottom: 5px;
        color: #43276D;
        font-weight: bold;
}
.sidebar {
        background-color: #C4CAE6;
}
.pRowTen {
        padding-bottom: 10px;
        }
.pRowFive {
        padding-bottom: 5px;
        }
.pRowThree {
        padding-bottom: 5px;
        color: #666;
}
.pRowFiveBullet {
        padding-top: 5px; 
        color: #0056a8; 
        font-family:"Wingdings 3"
        }
.nopad {
        margin: 0px;
        padding: 0px;
}
.small{
        font-size: 10px;
        line-height: 14px;
        color: #c2cad2;
        padding-left: 45px;
        }
.bottomBorder {
        border-left: 1px solid #c2cad2; 
        border-right: 1px solid #c2cad2; 
        border-bottom: 1px solid #c2cad2;
}
    .lowercase {
        text-transform: lowercase;
}
</style>
</head>
<body ><br>
<table width="615" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td width="615" align="center" valign="middle"><img src="http://www.mercerhrs.com/email/mmc/300995/header.gif" alt="Annual Enrollment Starts Monday - Marsh &amp; McLennal Companies LTD Bonus Income Plan" width="615" height="202"></td>
</tr>
<tr>
<td class="grayBorder">&nbsp;</td>
</tr>
<tr>
<td width="615" align="left" valign="top" class="grayBorder">
<table width="613" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="43" align="left" valign="top">&nbsp;</td>
<td width="374" align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="347" align="left" valign="top" class="pRowTen">Based on your bonus history, you are eligible to participate in the Marsh &amp; McLennan Companies Long Term Disability (LTD) Bonus Income Plan for the Plan year that begins July 1, 2014, and ends June 30, 2015.</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">Annual enrollment for the upcoming Plan year begins today, June 2, 2014, and ends Friday, June 13, 2014, at 11:59 p.m. Eastern time (see sidebar for instructions).</td>
</tr>
<tr>
<td align="left" valign="top" class="head2">The Marsh &amp; M<span class="lowercase">c</span>Lennan Companies LTD<br>
Bonus Income Plan</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">The Marsh & McLennan Companies LTD Bonus Income Plan offers long term disability protection on eligible bonus awards of $5,000 or greater. You may elect to cover your entire bonus up to a maximum of $300,000. If your bonus is $50,000 or more, you can cover either 100% of your bonus or 50% of your bonus (but no less than $50,000).</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">This coverage, if elected, will provide you a monthly tax-free benefit that is based on 60% of your covered amount beginning with the seventh month of an approved disability. For example, if your covered benefit amount is $30,000, your monthly benefit would equal $1,500 ($30,000 times 60%, divided by 12). The Plan pays bonus income long term disability benefits, up to a maximum of $15,000 per month, regardless of any other disability income you may receive.</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">Generally, for Plan purposes, your "eligible bonus" is the higher of the eligible bonus that you are awarded in the current calendar year or the average of your eligible bonuses awarded by the Company for the current calendar year and the prior two calendar years (if you were eligible for the Plan and eligible for an award in each of those years).</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen"><strong>For detailed information about the Plan, including the definition of bonuses eligible for coverage, refer to the Benefits Handbook, accessible via PeopleLink <span style="color: #1ba5e1;">(www.mmcpeoplelink.com).</span></strong></td>
</tr>
<tr>
<td align="left" valign="top" class="head2">Your 2014 Election</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" width="18" height="1"><br>
<img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td width="571" valign="top">If you are a newly eligible employee and you wish to participate in the Plan on July 1, 2014, without providing Evidence of Insurability, you must enroll online (see the instructions in the "Online Enrollment" section) before 11:59 p.m. Eastern time on Friday, June 13, 2014. <strong><br></strong></td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td valign="top" class="pRowThree"><em><strong>Note:</strong> You must elect benefit coverage under the Marsh &amp; McLennan Companies Optional Long Term Disability (LTD) Insurance Plan to participate in the Marsh & McLennan Companies LTD Bonus <span style="white-space:nowrap;">Income Plan.</span></em></td>
</tr>
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td width="571" valign="top" class="pRowFive">If you are currently participating in the Marsh &amp; McLennan Companies LTD Bonus Income Plan and you wish to continue participating at your current coverage percentage, no action is required. However, you should review your eligible bonus amount and 2014 Plan-year costs online at PeopleLink (see the instructions in the "Online Enrollment" section). Your participation will automatically carry over on July 1, 2014, at the 2014 <span style="white-space:nowrap;">Plan-year costs</span>.</td>
</tr>
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td valign="top">If you have previously waived coverage and you wish to begin participating in the Marsh & McLennan Companies LTD Bonus Income Plan, you must enroll online (see the instructions in the "Online Enrollment" section) and provide Evidence <span style="white-space:nowrap;">of Insurability</span>.</td>
</tr>
<tr>
<td valign="top">&nbsp;</td>
<td valign="top" class="pRowThree"><strong><em>Note:</em></strong> <em>You must be enrolled in the Marsh & McLennan Companies Optional Long Term Disability (LTD) Insurance Plan before your coverage under the Marsh & McLennan Companies LTD Bonus Income Plan will be approved. For details, refer to the Benefits Handbook, accessible <span style="white-space:nowrap;">via PeopleLink</span>.</em></td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" valign="top" class="head2">Questions?</td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">If you have any questions about the Marsh &amp; McLennan Companies LTD Bonus Income Plan, please contact the Employee Service Center at +1 866 374 2662, any business day, from <span style="white-space:nowrap;">8 a.m.</span> to <span style="white-space:nowrap;">8 p.m.</span> Eastern time.</td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
</table>
</td>
<td width="13" align="left" valign="top">&nbsp;</td>
<td width="170" align="left" valign="top">
<table width="170" border="0" cellpadding="0" cellspacing="0" class="sidebar">
<tr>
<td width="170" height="35" align="left" valign="top" class="pRowTen"><img src="http://www.mercerhrs.com/email/mmc/images/262574/sidebar.jpg" alt="" width="170" height="132"></td>
</tr>
<tr>
<td align="left" valign="top" class="pRowTen">
<table width="170" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" width="10" height="1"></td>
<td align="left" valign="top" class="head2">HOW TO ENROLL</td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" width="10" height="1"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="left" valign="top" class="pRowTen">To enroll, view your 2014<br>
Plan-year costs and<br>
eligible bonus amount, or<br>
decline participation in the<br>
Plan (if you currently<br>
participate and wish to<br>
end your participation):</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="left" valign="top" class="pRowTen">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="10" height="1"><br>
<img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td width="158" valign="top" class="pRowFive">Sign in to PeopleLink <a href="https://www.mmcpeoplelink.com" style="color: #43276D;">(www.mmcpeoplelink<br>
.com)</a> with your employee ID <span style="white-space:nowrap;">and password.</span></td>
</tr>
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td valign="top" class="pRowFive">In the right navigation bar, go to "My Benefits" and select "Health & <span style="white-space:nowrap;">Benefits Enrollment.&quot;</span></td>
</tr>
<tr>
<td width="18" valign="top" class="pRowFiveBullet"><img src="http://www.mercerhrs.com/email/mmc/images/262206/bullet.gif" alt="" width="5" height="5"></td>
<td valign="top" class="pRowFive">Click "MyHealth" and then select "Complete Your LTD Bonus <span style="white-space:nowrap;">Annual Enrollment.&quot;</span></td>
</tr>
</table>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="left" valign="top" class="pRowTen">Remember, if you are newly eligible and you wish to enroll in the Marsh &amp; McLennan Companies LTD Bonus Income Plan without providing Evidence of Insurability, you must take action by Friday, <span style="white-space:nowrap">June 13, 2014</span>, at <span style="white-space:nowrap">11:59 p.m.</span> Eastern time.</td>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div align="center"><img src="http://www.mercerhrs.com/email/mmc/300995/sidebar_wellbeing_at_work.jpg" style="margin-top:30px;"></div>
</td>
<td width="12" align="left" valign="top">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="31" align="left" valign="top" class="bottomBorder"><span class="small">300995 5/14</span><br>
<br></td>
</tr>
</table>
<br>
<div style="font:14px tahoma; width:100%; " class="mktEditable" ></div>
<div>
<table width="78%" align="center">
<tbody>
<tr>
<td style="padding-left: 0px; font-size: 9px; padding-bottom: 5px; color: #666666; line-height: 17px; font-family: Arial,Helvetica,sans-serif;" width="50%" align="left" valign="top">
<p>This email was sent by:&nbsp; Mercer<br>
1 Investors Way Norwood, MA 02062 USA</p>
<p>We respect your right to privacy - <a href="http://www.mercer.com/privacy.htm" target="_blank">view our policy</a></p>
</td>
<td style="padding-right: 10px; font-size: 9px;" align="right"><img src="http://info.mercer.com/rs/mercer/images/MMC_horizontal_4c.png" alt="MMC_horizontal_4c.png" width="153" height="21"><br></td>
</tr>
<tr>
<td colspan="2" align="center">
<p><span style="font-family: Verdana; font-size: xx-small;"><br>
If you no longer wish to receive these emails, click on the following link: <a href="http://info.mercer.com/UnsubscribePage.html">Unsubscribe</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</body>
</html>