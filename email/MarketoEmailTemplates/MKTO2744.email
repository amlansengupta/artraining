<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body ><div align="center" ><div id="Section 1" class="mktEditable" ><table width="600" align="center">
<tr>
<td height="24" align="left"><a href="{{system.forwardToFriendLink:default=Forward%20to%20a%20colleague}}"><font color="#777777" size="1" face="Verdana,Arial, sans-serif"><font color="#00A8C8">Forward to a friend</font></font></a></td>
<td align="right"><font color="#777777" size="1" face="verdana">follow us:</font> <a class="SocialIconsLink" href="https://www.facebook.com/MercerInsights" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/facebookemail.gif"></a><a class="SocialIconsLink" href="https://twitter.com/MercerInsights" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/twitteremail.gif"></a><a class="SocialIconsLink" href="http://www.imercer.com/content/mercer-rss-feeds.aspx" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/rssemail.gif"></a><a class="SocialIconsLink" href="http://www.youtube.com/user/mercervideo" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/youtubeemail.gif"></a></td>
</tr>
</table>
</div>
<div id="Section 2" class="mktEditable" ><div align="center">
<table width="602" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF; border:#BFBFBF; border-style:solid; border-width:1px;">
<tr>
<td>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><a href="http://www.imercer.com/content/mercer-belong-global-mobility.aspx"><img src="http://www.imercer.com/content/GM/email/belong-global-mobility-banner.jpg" alt="Mercer Belong Global Mobility" width="600" height="169" border="0"></a></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="30" valign="top">
<p>&nbsp;</p>
</td>
<td width="350" align="center" valign="top">
<h1 style="font: 14pt/18pt Arial, Helvetica, sans-serif; text-align: left; color: #00a8c8;"><strong>Powering HR. Empowering Assignees.</strong></h1>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Running a global mobility programme takes resources, and keeping consistent communication channels with assignees and HR partners is time-consuming and difficult to do. That’s why we developed a simple, integrated solution, <strong>Mercer Belong&reg; Global Mobility</strong>, to streamline how you connect with your globally mobile workforce&#8212;and help them stay connected to you.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong>Mercer Belong Global Mobility</strong>, our easy-to-use solution, brings together all your mobility products and content, delivering them straight to those who need them most. It enables assignees, managers, and others in your company to find the tools and information they need quickly and to stay involved. The tool is part of the Mercer Belong&reg; suite of products, which help you connect to your employees and equip them to make informed, positive action.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Learn more by joining our one-hour <strong>webcast on January 23</strong>. We’ll hold <strong>a live demo</strong> of this new tool and discuss how it can help enhance your mobility programme. <a href="https://mmc.webex.com/mw0401l/mywebex/default.do?nomenu=true&siteurl=mmc&service=6&rnd=0.9179102548103891&main_url=https%3A%2F%2Fmmc.webex.com%2Fec0701l%2Feventcenter%2Fevent%2FeventAction.do%3FtheAction%3Ddetail%26confViewID%3D1466804990%26%26%26%26siteurl%3Dmmc" style="color: #00a8c8;">Register here</a>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong>Mercer Belong Global Mobility</strong> can help you connect more directly to employees wherever they are in the mobility lifecycle:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Employees curious about a global assignment as part of their career planning</li>
<li>Candidates considering or preparing for an international assignment</li>
<li>Employees on assignment</li>
<li>Assignees getting ready to repatriate or start another assignment</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">This intuitive tool also provides HR and line managers&#8212;whether in their home countries or in host countries&#8212;access to self-service applications, helping them to stay in touch with assignees and to better understand their critical role in the global mobility lifecycle.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong>Mercer Belong Global Mobility</strong> is compatible with your enterprise network and with mobile devices, and you can have it up and running in as little as 12 weeks.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #00a8c8;"><strong>Want to learn more?</strong></p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Join us for the January 23 webcast (see sidebar) or email <a href="mailto:mobility@mercer.com" style="color:#00a8c8;">mobility@mercer.com</a> for details and a live demo.</p>
</td>
<td width="15" valign="top" style="border-right: solid 1px #bfbfbf;">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td valign="top">
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;">Mobility Communications Webcast</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #00a8c7;"><strong>See a live demo of Mercer Belong Global Mobility</strong><br></p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a href="https://mmc.webex.com/mw0401l/mywebex/default.do?nomenu=true&siteurl=mmc&service=6&rnd=0.9179102548103891&main_url=https%3A%2F%2Fmmc.webex.com%2Fec0701l%2Feventcenter%2Fevent%2FeventAction.do%3FtheAction%3Ddetail%26confViewID%3D1466804990%26%26%26%26siteurl%3Dmmc" style="border:solid 1px #00a8c8; padding:4px; color:#00a8c8; text-decoration:none;">REGISTER</a></p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;"><strong>January 23</strong><br>
Time 2 p.m. UK</p>
</td>
<td width="30" valign="top">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:30px; font-family:Arial, Helvetica, sans-serif; font-size: 8pt; color:#808080; text-align:left;"></td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" width="216" height="52" alt="Marsh &amp; McClennan Companies"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<div class="mktEditable" id="column_text" align="center" ><table width="608" border="0" cellpadding="0">
<tr>
<td>&nbsp;</td>
</tr>
</table>
</div>
</div>
<div class="mktEditable" ><table width="600" align="center">
<tr>
<td align="left">
<hr size="1" noshade="noshade" color="#C4CAE6">
<font style="font-family:Arial, Helvetica, sans-serif; font-size:10px">For details, contact your local Mercer representative, or contact us at:<br>
<br>
<a href="mailto:hrsolutions.ap@mercer.com">Asia</a>: +65 6398 2926<br>
<a href="mailto:customerservice@mercer.com">Australia/NZ</a>: 1800 645 186 (Sydney) +61 2 8864 6800 (International) / 0508 645 186 (Auckland) +64 9 984 3500 (International)<br>
<a href="mailto:info.services@mercer.com">Canada</a>: 1-800-333-3070<br>
<a href="http://www.imercer.com/CC/tabs/home.aspx">China</a> +86 10 6533 4300<br>
<a href="mailto:client.services.europe@mercer.com">Europe</a>: 48 22 434 5383<br>
<a href="mailto:client.services.la@mercer.com">Latin America</a>: 54 11 4000 4081<br>
<a href="mailto:surveys@mercer.com">United States</a>: 1-800-333-3070 or 1-502-560-8290</font></td>
</tr>
</table>
</div>
<div class="mktEditable" id="Dynamic_Content_3" ><table width="600" align="center">
<tr>
<td>
<hr size="1">
<div align="left"><font style="font-family:Arial, Helvetica, sans-serif; font-size:10px">You are receiving this e-mail because our consultants have requested that this be sent to you.<br>
<br>
If you want to remove yourself from this distribution list, <a href="mailto:client.services.europe@mercer.com?subject=Remove%20from%20distribution%20list">please inform us.</a><br>
If you no longer wish to receive any Mercer e-mails, <a href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a href="mailto:client.services.europe@mercer.com?subject=Feedback%20on%20Mercer%20e-mail">email your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://image.exct.net/lib/fef91d70766d05/m/1/icon_more10x10bluewhite.gif" border="0" alt="More" width="10" height="10">&nbsp;Find the <a href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</font></div>
<hr size="1">
<div align="right"><font style="font-family:Arial, Helvetica, sans-serif; font-size:10px"><a href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a href="http://www.mercer.com/privacy.jhtml">Privacy</a><br>
&copy;2013 Mercer LLC, All Rights Reserved</font></div>
</td>
</tr>
</table>
</div>
</div>
</body>
</html>