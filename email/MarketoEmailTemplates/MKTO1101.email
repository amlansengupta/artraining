<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body ><div style="font:14px tahoma; width:100%; " class="mktEditable" id="edit_text" ><table border="0" cellspacing="0" cellpadding="0" width="615" align="center">
<tbody>
<tr>
<td style="border-left: 1px solid #c2cad2; border-right: 1px solid #c2cad2; border-top: 1px solid #c2cad2; padding-top: 10px;" width="100%" align="center" valign="middle"><img src="http://www.mercerhrs.com/email/mmc/271672/header.jpg" alt="top banner" width="615" height="220"></td>
</tr>
<tr>
<td style="border-left: 1px solid #c2cad2; border-right: 1px solid #c2cad2;">&nbsp;</td>
</tr>
<tr>
<td style="border-left: 1px solid #c2cad2; border-right: 1px solid #c2cad2;" width="615" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="45" align="left" valign="top">&nbsp;</td>
<td width="343" align="left" valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="340">
<tbody>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 10px; padding-top: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 17px;" width="335" align="left" valign="top">As part of Marsh & McLennan Companies' commitment to provide you with resources that can help you enhance your financial wellbeing, the Company will be offering services to help you improve your retirement outlook. In September, you’ll have access to Financial Engines' Online Advice service and Professional Management program, designed to help you better manage your Company defined contribution plan account(s). You might be asking yourself: <em>Can these resources really make a difference? Or am I better off making my own investment decisions?</em></td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 3px; padding-top: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 12px; line-height: 17px; font-weight: bold; text-transform:uppercase;" align="left" valign="top">Good news</td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 10px; padding-top: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 17px;" width="335" align="left" valign="top">People who get help are more confident and better prepared for retirement, according to research from the latest Mercer Workplace Survey&trade;, a nationally representative study of employee attitudes and behaviors regarding their benefits.</td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 15px; padding-top: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 17px;" width="335" align="left" valign="top">In fact, <strong>69%</strong> of employees who use a financial advisor felt confident in their retirement readiness, compared with 53% of employees overall. And <strong>66%</strong> of employees who use advice said they increased their plan contributions, compared with only 34% without an advice relationship <span style="white-space:nowrap">who did.*</span></td>
</tr>
<tr>
<td style="padding: 10px; font-family: Verdana,Geneva,sans-serif; font-size: 12px; line-height: 17px; font-weight: bold;" align="left" valign="top" bgcolor="#BDDDA3">STAY TUNED<br>
You’ll receive more information about Financial Engines at work and at your address on file. You can also attend a webinar in August, September, or October and enter in a drawing to win an iPad mini!** &ndash; <a href="http://www.mmc.financialengines.com" target="_blank">register online now</a>.<br>
<br>
<span style="font-size: 10px; color: #666;">**Promotion <a href="http://connections.financialengines.com/open/rules/mmc/" target="_blank" style="font-size: 10px;">terms and conditions</a> apply.</span></td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 3px; padding-top: 15px; font-family: Verdana, Geneva, sans-serif; font-size: 12px; line-height: 17px; font-weight: bold; text-transform:uppercase;" align="left" valign="top">GETTING HELP matters</td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-bottom: 10px; padding-top: 0px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 17px;" align="left" valign="top">With Social Security only expected to provide 37% of the average retiree’s income, it’s important to make the most of your personal investments. Help provided through Financial Engines can support your journey toward a rewarding retirement.</td>
</tr>
<tr>
<td style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 10px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; line-height: 17px;" align="left" valign="top">Mercer's study revealed that employees who take advantage of advice services through their 401(k) defined contribution plan had more positive expectations on many important indicators of retirement security, including:</td>
</tr>
<tr valign="top">
<td align="left" valign="top">
<table style="border: 0px;" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding-left: 10px; padding-top:0px; color: #0056a8;" width="13" valign="top"><img style="display: block;" src="http://www.mercerhrs.com/email/images/bullet.jpg" alt="" width="9" height="18"></td>
<td style="padding-bottom: 5px; padding-left:2px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; color: #000;" width="327" valign="top">Having enough money to pay for health care.</td>
</tr>
<tr>
<td style="padding-left: 10px; color: #0056a8;" width="13" valign="top"><img style="display: block;" src="http://www.mercerhrs.com/email/images/bullet.jpg" alt="" width="9" height="18"></td>
<td style="padding-bottom: 5px; padding-left:2px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; color: #000;" valign="top">Living as well or better as when they were working.</td>
</tr>
<tr>
<td style="padding-left: 10px; color: #0056a8;" width="13" valign="top"><img style="display: block;" src="http://www.mercerhrs.com/email/images/bullet.jpg" alt="" width="9" height="18"></td>
<td style="padding-bottom: 5px; padding-left:2px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; color: #000;" valign="top">Being able to travel extensively.</td>
</tr>
<tr>
<td style="padding-left: 10px; color: #0056a8;" width="13" valign="top"><img style="display: block;" src="http://www.mercerhrs.com/email/images/bullet.jpg" alt="" width="9" height="18"></td>
<td style="padding-bottom: 0px; padding-left:2px; font-family: Verdana, Geneva, sans-serif; font-size: 11px; color: #000;" valign="top">Not running out of money.</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding-top: 20px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; color: #666; line-height: 17px;" colspan="2"><em>*2012 Mercer Workplace Survey</em></td>
</tr>
<tr>
<td style="padding-top: 10px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; color: #666; line-height: 17px;" colspan="2">This email provides general information about Financial Engines and is not intended to provide any personalized advice, including investment and/or financial planning advice. For more information, you may wish to consult with your own personal investment or financial planning advisor.</td>
</tr>
<tr>
<td style="padding-top: 10px; font-family: Verdana, Geneva, sans-serif; font-size: 10px; color: #666; line-height: 17px;" colspan="2">&copy; 2013. All rights reserved. Financial Engines<sup><span style="font-size: 9px;">&copy;</span></sup> and Retirement Help for Life<sup><span style="font-size: 9px;">&copy;</span></sup> are registered trademarks or service marks of Financial Engines, Inc. All advisory services provided by Financial Engines Advisors L.L.C., a federally registered investment advisor and wholly-owned subsidiary of Financial Engines, Inc. Future results are not guaranteed by Financial Engines or any other party.</td>
</tr>
</tbody>
</table>
</td>
<td width="5" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="5" height="1"></td>
<td width="200" align="center" valign="top"><img src="http://www.mercerhrs.com/email/mmc/271642/fe_logo.png" alt="" width="170" height="81">
<table border="0" cellspacing="0" cellpadding="0" width="200" bgcolor="#BDDDA3">
<tbody>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1"></td>
<td>&nbsp;</td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="padding-bottom: 3px; color: #01592e; font-weight: bold; font-size: 12px; text-transform: uppercase; text-align: left; font-family: Verdana, Geneva, sans-serif;" width="180" align="left">WATCH THE TALE OF<br>
TWO INVESTORS</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="font-size: 11px; padding-bottom: 10px; padding-top: 0px; text-align: left; font-family: Verdana, Geneva, sans-serif; line-height: 17px;">The Financial Engines video "Why Getting 401(k) Help Matters" shows you how professional investment help can make a difference &mdash; and why.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="font-size: 11px; padding-bottom: 15px; padding-top: 0px; text-align: left; font-family: Verdana, Geneva, sans-serif; line-height: 17px;" valign="top">See two hypothetical investors bring the research to life in <a title="Financial Engines Site" href="http://corp.financialengines.com/helping_you/why_help_matters.html" target="_blank">this brief video</a>.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="padding-bottom: 3px; color: #01592e; font-weight: bold; font-size: 12px; text-transform: uppercase; text-align: left; font-family: Verdana, Geneva, sans-serif;" width="180" align="left">Not enrolled in <span style="white-space: nowrap;">a plan?</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="font-size: 11px; padding-bottom:10px; text-align: left; font-family: Verdana, Geneva, sans-serif; line-height: 17px;" valign="top">As a reminder, Financial Engines services are available only to eligible plan participants with an account balance in a Marsh & McLennan Companies defined contribution plan. If you’re eligible, but not participating in any of the Marsh & McLennan Companies defined contribution plans, start now!</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="font-size: 11px; padding-bottom:15px; text-align: left; font-family: Verdana, Geneva, sans-serif; line-height: 17px;" valign="top">Log on to <a title="PeopleLink" href="https://www.mmcpeoplelink.com">PeopleLink</a>. Go to the <strong>Finances</strong> tab, select the appropriate plan under the Finances Menu, and under Take Action, click <strong>Enroll, view, change benefits.</strong></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="padding-bottom: 3px; color: #01592e; font-weight: bold; font-size: 12px; text-transform: uppercase; text-align: left; font-family: Verdana, Geneva, sans-serif;" width="180" align="left"><span style="white-space: nowrap;">HAVE A QUESTION?</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="font-size: 11px; text-align: left; font-family: Verdana, Geneva, sans-serif; line-height: 17px;" valign="top">To learn more about Financial Engines, visit <a href="http://www.mmc.financialengines.com" target="_blank">www.mmc.financialengines.com</a> where you can watch informational videos, sign up<br>
to attend an educational webinar, get answers to frequently asked questions, and download the most<br>
recent communications.</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1"></td>
<td>&nbsp;</td>
<td><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="12" height="1"></td>
</tr>
</tbody>
</table>
<div align="center">
<p><img src="http://www.mercerhrs.com/email/mmc/271642/Wellbeing@Work-logo-4c.jpg" alt="" width="166" height="76"></p>
</div>
</td>
<td width="5" align="left" valign="top"><img src="http://www.mercerhrs.com/email/mmc/images/262210/spacer.gif" alt="" width="5" height="1"></td>
</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
<td colspan="3" align="left" valign="bottom">&nbsp;</td>
<td align="left" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table style="border-top: 1px solid #c2cad2;" border="0" cellspacing="0" cellpadding="0" width="615" height="36" align="center">
<tbody>
<tr>
<td style="font-size: 10px; color: #999999; padding-left: 45px;" align="left" valign="top"><br>
<p>M271672 8/13</p>
</td>
<td align="right" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<div style="font:14px tahoma; width:100%; " class="mktEditable" ></div>
<div>
<table width="78%" align="center">
<tbody>
<tr>
<td style="padding-left: 0px; font-size: 9px; padding-bottom: 5px; color: #666666; line-height: 17px; font-family: Arial,Helvetica,sans-serif;" width="50%" align="left" valign="top">
<p>This email was sent by:&nbsp; Mercer<br>
1 Investors Way Norwood, MA 02062 USA</p>
<p>We respect your right to privacy - <a href="http://www.mercer.com/privacy.htm" target="_blank">view our policy</a></p>
</td>
<td style="padding-right: 10px; font-size: 9px;" align="right"><img src="http://info.mercer.com/rs/mercer/images/MMC_horizontal_4c.png" alt="MMC_horizontal_4c.png" width="153" height="21"><br></td>
</tr>
<tr>
<td colspan="2" align="center">
<p><span style="font-family: Verdana; font-size: xx-small;"><br>
If you no longer wish to receive these emails, click on the following link: <a href="http://info.mercer.com/UnsubscribePage.html">Unsubscribe</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</body>
</html>