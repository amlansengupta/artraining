<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
img {
        border:none;    
}
body {
        background-color: #eeeeee;
}
a:link {
        color: #00a8c8;
        text-decoration: underline;
}
a:visited {
        color: #00a8c8;
        text-decoration: underline;
}
a:hover {
        color: #00a8c8;
}
a:active {
        color: #00a8c8;
        text-decoration: underline;
}
</style>
<title></title>
</head>
<body ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td valign="top" ><div class="mktEditable" id="header" ><img src="http://info.mercer.com/rs/mercer/images/Banner_MMB.gif" width="601" height="63" alt="Mercer" border="0" style="display:block"></div>
</td>
</tr>
<tr ><td ><div class="mktEditable" id="banner" ><img style="DISPLAY: block" border="0" alt="Mercer" src="http://info.mercer.com/rs/mercer/images/11563-HB_Banner.jpg"></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr ><td width="32">&nbsp;</td>
<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><div class="mktEditable" id="content" ><p><span style="color: #002c77; font-size: 16px;">BENEFITS: THE UNDERUTILIZED REWARD</span><br>
<span style="font-size: 16px;">EFFECTIVE BENEFIT PROGRAMS FOR A POSITIVE EMPLOYEE EXPERIENCE</span></p>
<p>Dear {{lead.First Name:default=Valued Client}}</p>
<p>An increasing number of employees are considering leaving their jobs. Employees are less satisfied with pay and benefits, and engagement is declining.<br>
<br>
<span style="color: #006d9e;"><strong>Are you well-equipped to differentiate your benefits against those of your competitors or peers? Do your benefits programs meet your employees&#39; needs? And do you have the necessary information to communicate to employees the value of their benefits?</strong></span><br>
<br>
Benefits make up an increasingly significant component of the overall compensation package, with at least 35% of employers in the region spending 6% or more of payroll on their health benefits. Many companies, however, have not taken steps to incorporate benefits into the strategic part of their overall workforce plans.<br>
<br>
In this white paper, <a style="color: #00a8c8; text-decoration: none;" href="mailto:liana.attard@mercer.com">Liana Attard</a>, Mercer Marsh Benefits Regional Benefits Management Leader, Asia, and <a style="color: #00a8c8; text-decoration: none;" href="mailto:godelieve.kroonenberg@mercer.com">Godelieve Kroonenberg</a>, Mercer&#39;s Regional Benefits Products Leader, Asia, discuss how to leverage existing benefit investments to create a benefits program that can serve as a key competitive differentiator.</p>
<table style="border: #002c77 1px solid;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td>
<table style="font-family: Arial, Helvetica, sans-serif; color: #333333; font-size: 12px;" border="0" cellspacing="0" cellpadding="0" width="100%" align="right">
<tbody>
<tr>
<td style="padding-bottom: 4px; padding-top: 5px;" width="40%" align="center" valign="middle" bgcolor="#A6E2EF"><span style="line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #002c77; font-size: 16px;">DOWNLOAD THE<br>
WHITEPAPER</span><br>
<img src="http://info.mercer.com/rs/mercer/images/11563-HB_POV.gif" border="0" alt="Download the whitepaper" width="175" height="163"><br>
<img src="http://info.mercer.com/rs/mercer/images/11563-HB_Arrow_bright_sapphire.jpg" alt="Arrow" width="5" height="10">&nbsp;<a style="color: #00a8c8; text-decoration: none;" href="http://www.mercer-marsh-benefits.com/articles/creating-an-effective-benefits-program"><strong><u>Download now</u></strong></a></td>
<td width="8%" align="center" valign="middle"><img src="http://info.mercer.com/rs/mercer/images/11563-HB_Divider.gif" alt="Divider" width="43" height="237"></td>
<td style="line-height: 17px; padding-left: 10px; padding-right: 10px; font-family: Arial, Helvetica, sans-serif; font-size: 13px;" width="52%" align="left" valign="middle">
<p><span style="color: #000000;"><span style="color: #002c77;">APPLY A THREE-STEP PROCESS TO SETTING UP AN EFFECTIVE BENEFIT PROGRAM:</span></span></p>
<ol>
<li><span style="color: #000000;">Create a market-competitive benefit package via qualitative benchmarking.</span></li>
<li><span style="color: #000000;">Identify benefit plan cost drivers and utilization trends via a deep-dive analysis of claims.</span></li>
<li><span style="color: #000000;">Drive value and appreciation of benefits via a benefits valuation assessment.</span></li>
</ol>
</td>
</tr>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; color: #333333; font-size: 12px; padding: 10px;" colspan="3" align="left" valign="top" bgcolor="#002C77"><span style="color: #ffffff;">Visit <a href="http://www.mercer-marsh-benefits.com/benefits-effectiveness"><span style="color: #ffffff;"><u>Maximizing Benefits Effectiveness</u></span></a> to learn more about structuring your rewards program to drive greater value from your investment or contact a <a href="http://www.mercer-marsh-benefits.com/registerEvent.htm?idContent=1491650"><span style="color: #ffffff;"><u>Mercer Marsh Benefits consultant</u></span></a> today.</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p>Regards,</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
<tbody>
<tr>
<td width="40%" align="left" valign="top"><strong>Vicky B. Ongchangco</strong><br>
Vice President<br>
Mercer Marsh Benefits<br>
<a style="color: #00a8c8; text-decoration: none;" href="mailto:vicky.ongchangco@mercer.com">vicky.ongchangco@mercer.com</a></td>
<td width="60%" align="left" valign="top"><strong>Lito G. Ortiz</strong><br>
Assistant Vice President<br>
Mercer Marsh Benefits<br>
<a style="color: #00a8c8; text-decoration: none;" href="mailto:marcelito.ortiz@mercer.com">marcelito.ortiz@mercer.com</a></td>
</tr>
</tbody>
</table>
</div>
</td>
<td width="32">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="mktEditable" id="footer" ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:center;">
<tr>
<td valign="top"><img src="http://info.mercer.com/rs/mercer/images/footer.png" width="568" height="45" alt="Marsh &amp; McLennan Companies" border="0" style="display:block"></td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" ><table style="table-layout: fixed; background-color: #eeeeee;" border="0" cellspacing="0" cellpadding="0" width="601" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;">IMPORTANT NOTICE: This document does not constitute or form part of any offer or solicitation or invitation to sell by either Marsh or Mercer to provide any regulated services or products in any country in which either Marsh or Mercer has not been authorized or licensed to provide such regulated services or products. You accept this document on the understanding that it does not form the basis of any contract.<br>
<br>
The availability, nature and provider of any services or products, as described herein, and applicable terms and conditions may therefore vary in certain countries as a result of applicable legal and regulatory restrictions and requirements.<br>
<br>
Please consult your Marsh or Mercer consultants regarding any restrictions that may be applicable to the ability of Marsh or Mercer to provide regulated services or products to you in your country.</span>
<hr size="1">
<span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong>You are receiving this e-mail because you have registered at mercer.com and opted in to receive our communications or because our consultants have requested that this be sent to you.</strong> To view your registration details, <a style="color: #00a7c7; text-decoration: none;" href="https://secure.mercer.com/login.jhtml">log in here</a>, or if you were forwarded this e-mail and would like to subscribe, please <a style="color: #00a7c7; text-decoration: none;" href="https://secure.mercer.com/registration.sec">register at mercer.com</a>. Registration is free, provides you with access to premium content, and allows you to tailor the site to your own interests.<br>
<br>
If you want to remove yourself from this distribution list, <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Remove%20from%20distribution%20list%20-%20Asia">please inform us</a>.<br>
If you no longer wish to receive any Mercer e-mails, <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Feedback%20on%20Mercer%20e-mail%20-%20Asia">e-mail your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10">&nbsp;Find the <a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/privacy.jhtml">Privacy</a><br>
©2013 Mercer LLC. All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;">This email was sent to: {{lead.Email Address:Default=edit me}}<br>
This email was sent by:<br>
Mercer (Global Headquarters)<br>
1166 Avenue of the Americas New York New York 10036 USA<br>
<br>
Mercer (Singapore) Pte Ltd<br>
8 Marina View #09-08, Asia Square Tower 1, Singapore 018960</span></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>