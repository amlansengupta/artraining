<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
img {
        border:none;    
}
body {
        background-color: #eeeeee;
}
a:link {
        color: #00a8c8;
        text-decoration: underline;
}
a:visited {
        color: #00a8c8;
        text-decoration: underline;
}
a:hover {
        color: #00a8c8;
}
a:active {
        color: #00a8c8;
        text-decoration: underline;
}
</style>
<title></title>
</head>
<body ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td valign="top" ><div class="mktEditable" id="header" ><img src="http://info.mercer.com/rs/mercer/images/cemailLogo.png" width="601" height="63" alt="Mercer" border="0" style="display:block"></div>
</td>
</tr>
<tr ><td ><div class="mktEditable" id="banner" ><img style="display: block;" border="0" alt="Mercer" src="http://info.mercer.com/rs/mercer/images/BNR_Large_322_Sapphire.jpg" width="601" height="205"></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr ><td width="32">&nbsp;</td>
<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><div class="mktEditable" id="content" ><p><span style="color: #002c77; font-size: 16px;">MERCER 2013 GREATER CHINA RETIREMENT SAVINGS SURVEY REPORT&nbsp;&mdash; HONG KONG</span><br /> <br /> Dear {{lead.First Name:default=valued client}},</p>
<p>Thank you for participating in the Mercer 2013 Greater China Retirement Savings Survey. Following our breakfast briefing session in September&nbsp;2013, we are pleased to share our final report from the Hong Kong survey.</p>
<p>We would welcome the opportunity to meet with you to discuss the results and share further insight into developments in the Hong Kong retirement market. Although we have seen that the structure and design of retirement benefits have not typically changed significantly over recent years, the survey highlights an increasing demand for improved member communication, tools, and support, along with a need for increased governance.</p>
<p>A summary of the highlights and key findings is set out below, but for more details on the survey results, please <a href="http://info.mercer.com/rs/mercer/images/2013_GCRRFSavingsSuvey_HKReport_FIN.pdf" target="_blank">click here</a> to download the free survey report.&nbsp;<br />&nbsp;<br /> <span style="color: #002c77;">REPORT SUMMARY</span></p>
<ul>
<li><strong>Seventy-three percent</strong> of respondents <strong>provide more than the statutory minimum level of employer MPF contributions</strong> (5% of Relevant Income). There is also a wide range of contribution structures, with 49% employing a flat contribution structure and 41% contributing amounts that relate to the length of service (for companies providing more than the statutory minimum level of MPF contributions), with contributions ranging from 5% to 15% of salary. </li>
</ul>
<ul>
<li>The survey data show that almost <strong>two-thirds of companies</strong> either <strong>have no default investment option or the default is cash/money market funds</strong>. This raises a number of questions about the guidance given to members who need help to make appropriate investment decisions within a long-term savings vehicle. </li>
</ul>
<ul>
<li> Almost 80% of the respondents that use MPF as their primary retirement plan stated that they are satisfied or very satisfied with their current MPF provider &mdash; however, there were a number of areas in which <strong>satisfaction was relatively low</strong>, including <strong>investment performance, fees, and the quality of service and communications to employees</strong>.</li>
</ul>
<ul>
<li>Close to <strong>80% of survey participants have not changed their MPF provider since inception</strong>. However, with significant differences in service offerings, fund ranges, and fees in the market, shopping around can add a good deal of value.</li>
</ul>
<ul>
<li> The Mercer 2011 Inside Employees&rsquo; Minds survey indicated that retirement plans are ranked as the <strong>fourth most important element of the employee value proposition</strong>, after base pay, bonus/other incentives and career advancement. Given the importance of retirement plans to employees and the significant cost to employers of providing such benefits, a number of companies are looking to improve their return on investment by using different communication approaches to increase employee understanding and engagement. </li>
</ul>
<ul>
<li>Services such as member online access, online transactions, fund factsheets, and dedicated hotlines are common. However, many companies are <strong>looking for increased personalization</strong> for their employees through such methods as <strong>interactive tools, personalized benefits projection statements, and one-on-one meetings</strong>. New technology administration platforms are increasing the options available for personalization of the message.</li>
</ul>
<ul>
<li>The <strong>majority</strong> of participants are <strong>planning to review their DC schemes within the next three years</strong>. The main areas of focus are employee education and engagement, investment options, and scheme benefits design. </li>
</ul>
<p>If you have any questions or wish to talk to our Mercer consultants regarding the survey results, please feel free to <a href="mailto:GC-Retirement@mercer.com" target="_blank">contact us</a>. We would welcome the chance to meet with you to discuss all of this further.</p>
<p>Yours sincerely,</p>
<p><strong>Simon Ferry</strong> <br />Hong Kong Retirement Business Leader<br />Mercer</p>
<ul>
</ul></div>
</td>
<td width="32">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="mktEditable" id="footer" ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:center;">
<tr>
<td valign="top"><img src="http://info.mercer.com/rs/mercer/images/footer.png" width="568" height="45" alt="Marsh &amp; McLennan Companies" border="0" style="display:block"></td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" >
<table style="table-layout: fixed; background-color: #eeeeee;" border="0" cellspacing="0" cellpadding="0" width="601" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong> You are receiving this e-mail because you have registered at  			mercer.com and opted in to receive our communications or because our  			consultants 
                have requested that this be sent to you.
 </strong>To  			view your registration details, <a style="color: #00a7c7; text-decoration: none;" href="https://secure.mercer.com/login.jhtml"> log in here</a>, or if you were forwarded this e-mail and would like  			to subscribe, please <a style="color: #00a7c7; text-decoration: none;" 
href="https://secure.mercer.com/registration.sec"> register at mercer.com</a>.
                 Registration is free, provides you with  			access to premium content, and allows you to tailor the site to your  			own interests.
                 <br /> <br /> If you want to remove yourself from this distribution list, <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Remove from distribution list - Asia"> please inform us</a>.<br /> If you no longer wish to receive any Mercer     
e-mails,    <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html"> unsubscribe here</a>.
                 <br /> <br /> We welcome your thoughts and input.
                 If you would like to contact us  			with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Feedback on Mercer e-mail - Asia"> e-mail your feedback</a>.
                 We encourage you to forward this e-mail,  			and to link to content on our site.<br /> <br /> <img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10" />&nbsp;Find  			the <a style="color: #00a7c7; text-decoration: none;" 
href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer  			office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1" />
</td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/termsofuse.jhtml"> Terms of use</a> | <a style="color: #00a7c7; 
text-decoration: none;" href="http://www.mercer.com/privacy.jhtml"> Privacy</a><br /> &copy;2014 Mercer LLC.
                 All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1" />
</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"> This email was sent to: {{lead.Email Address:Default=edit me}}<br /> This email was sent by: <br /> Mercer (Global Headquarters) <br /> 1166 Avenue of the Americas New York New York 10036     
            USA<br /> <br /> Mercer (Hong Kong) Limited<br /> 26/F and 27/F, Central Plaza, 18 Harbour Road, Wanchai, Hong Kong</span></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>