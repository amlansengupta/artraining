<?xml version="1.0" encoding="UTF-8"?>
<EmailTemplate xmlns="http://soap.sforce.com/2006/04/metadata">
    <available>true</available>
    <encodingKey>UTF-8</encodingKey>
    <name>Pac_IC_Defined Contribution_Alert or Notification_201408 MST Monthly Portfolio S</name>
    <style>none</style>
    <subject>Portfolio Management Snapshot</subject>
    <textOnly>Mercer Super Trust Portfolio Management Snapshot
This update from the Mercer Investments Portfolio Management team looks back at the month of July 2014.

IN JULY 2014……


 - Mercer Growth* returned 1.5%, outperforming its composite benchmark by 0.30%.  




 - In peer relative terms, Mercer Growth outperformed (after fees and taxes) both the median balanced master trust (by 0.21%) and the broader median by (by 0.24%).




 - Mercer SmartPath 1969-73*, the largest SmartPath option returned 1.7%, in line with its composite benchmark.



WHAT DID WE DO?


 - Lowered our allocation to Australian and Global Sovereign Bonds by 1% each and temporarily allocated to cash in response to recent market dynamics and ahead of the introduction of an innovative new exposure funded in early August. 




 - No changes were made to the manager line up in July.
IN THIS ISSUE


ECONOMY &amp; MARKETS &lt;#1&gt;



INVESTMENT PERFORMANCE &lt;#2&gt;



ENHANCED TEAM &amp; PROCESS DELIVERING RESULTS &lt;#3&gt;



ASSET ALLOCATION &amp; MANAGER UPDATE &lt;#4&gt;


 &lt;http://www.linkedin.com/groups/Mercer-Investments-Asia-Pacific-4442861/about?trk=anet_ug_grppro&gt;


DOWNLOAD THE LATEST REPORTS


 &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_CSD_MonthlyReportJuly2014.pdf&gt;      &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_Qtly_Q2_2014.pdf&gt;
ECONOMY &amp; MARKETS




Markets were mixed in July. Australian shares were strong, returning 4.4%, while overseas shares returned -0.7% and -0.2% in hedged and unhedged terms respectively. Australian sovereign bonds returned 0.3%, while global sovereign bonds returned 0.7% (hedged) and global credit returned 0.2%.

In many ways, the new financial year (2014-15) kicked off the same way the previous one ended with shares, bonds (prices), and most other asset classes all grinding higher, while volatility remained low. Sentiment was positive, and economic data was generally supportive, with strong releases from the US, and even better data from China (with the exception of house prices), which indicated we may have seen the worst of the recent Chinese slowdown. The world’s shock at the downing of MH17 by a rogue missile on 17 July initially led to a small selloff, but this was quickly reversed and risk markets made some further gains. However, as we approached the end of the month, markets turned, with shares dropping in the US and Europe by 2.0% and 1.3% respectively on the 31 July, wiping out the whole month’s gains, to push monthly returns negative.

Commentators and the media indicated that the rapid fall at the end of the month was a result of increasing geopolitical tensions in the Ukraine and Middle East. Nevertheless, the scale of the fall seems relatively large given that nothing fundamentally changed in these conflicts over that time period. An alternative explanation put forward was that the positive data surprises picked up significantly on 31 July. Data released showed that US Gross Domestic Product (GDP) grew at an annualised pace of 4.0% in the second quarter, ahead of the expected 3.0%, while the first quarter soft number was revised upwards from -2.9% annualised to -2.1% annualised. 
Also that day, the US central bank – the US Federal Reserve (the Fed) – released minutes of its July meeting. The Fed remained unconcerned about inflation in its statement but was considerably more balanced than its very dismissive stance on inflationary pressures. Some investors think that the Fed’s easy monetary policy (low interest rates) has kept equities at higher valuations than they would otherwise be. Therefore, the better-than-expected data could be interpreted as leading to tighter monetary policy (increased interest rates) sooner, and hence a reason to sell shares. There may be some truth to the notion that easy monetary policy encourages risk taking and a change in direction by the Fed may induce increased volatility and some short-term losses. However, our view is that the longer-term fundamentals remain unchanged so we expect global shares to continue to push higher.
 &lt;#top&quot;&gt;&lt;img title=&gt;









INVESTMENT PERFORMANCE





Mercer Growth* performance was strong in July, with a 1.5% return for the month that outperformed the option’s composite benchmark by 0.3%. Performance was supported by our underweight currency hedging positioning, overweight to Emerging Markets shares and the strong performance of our underlying fixed income and unlisted infrastructure managers.

While we are long-term investors and recognise that month-to-month performance can bounce around, it is pleasing to see Mercer Growth commence the new financial year with good peer-relative performance. Mercer Growth was in the top quartile of all funds in the SuperRatings Fund Crediting Rate Survey, outperforming other master trusts by 0.21% and the overall median funds by 0.24%.  At the end of July, Mercer Growth had outperformed the master trust median over all time periods in the 
survey, and is in now line or ahead of the overall median over three years and less.

Due to the large number of Mercer SmartPath options, we only report performance for the Mercer SmartPath1969-73 option here because this is the largest option by funds under management and is therefore the most representative of 
underlying investors. In July, the Mercer SmartPath 1969-73 option returned 1.7%, in line with its composite benchmark.  While asset allocation targets relative to benchmarks are typically consistent across all options, benchmark-relative performance can vary across options on a monthly basis due to different 
weightings to asset classes. This can explain why Mercer Growth outperformed its composite benchmark, while SmartPath 1969-73 was in line with its composite benchmark. 
The July Monthly Report &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_CSD_MonthlyReportJuly2014.pdf&quot;&gt;&lt;span style=&gt; provides more details on the performance of Mercer Growth and Mercer SmartPath 1969-73, along with that of other investment options.
 &lt;#top&gt;









ENHANCED TEAM &amp; PROCESS RESULTS






Russell Clarke reassumed the role of Chief Investment Officer for Mercer Pacific a little over a year ago. We asked him to reflect on his team’s achievements since then. Here’s what he had to say:

“Well, we formalised how our intellectual capital is captured in Mercer’s investment process. We brought Nick White into the role of Head of Mercer Funds’ Research, and David Stuart into the role of Head of Strategy. In addition, Philip Houghton-Brown became Manager of our New Zealand investment portfolios, with Paddy Brown taking responsibility for the unlisted property portfolio. Complementing the team with these appointments was a positive step and each individual has made a strong contribution to the way we manage investment portfolios on behalf of our clients,” Russell said.

“Our investment process has always been built on clear, well-researched principles,” he says. “However, we have sought to make to it more fluid and responsive over the past year. This has included adopting a ‘floating’ asset allocation approach, which has sought to merge the best features of both strategic and dynamic asset allocation. In practice, this has removed some of what we call ‘framing’ issues that can occur with asset allocation, while enabling us to respond to market developments more quickly whenever required.”

On the structure of the investment portfolios, Russell notes that “we have made a range of positive improvements, led by the introduction of an asset allocation overlay that will improve the efficiency of implementing asset 
allocation decisions. We have also introduced a range of new ‘growth fixed income’ components to the portfolios (high yield, multi-asset credit, absolute return bonds). A restructure of the overseas shares sector has also better balanced risk in that particular portfolio.”

The changes have led to materially improved performance of Mercer’s key multi-sector portfolios versus peers over the financial year, which is a pleasing outcome. The performance of the overseas shares sector has also been comfortably exceeding benchmark since the changes referred to above were made.
While pleased with the progress to date, Russell also stressed that “we will not rest on our laurels. There is still room to improve performance and a range of further incremental improvements to the portfolios continue to be investigated.”
 &lt;#top&quot;&gt;&lt;img title=&gt;









ASSET ALLOCATION &amp; MANAGER UPDATE






We lowered our allocation to Australian and Global Sovereign Bonds by 1% each in July and temporarily allocated to cash ahead of the introduction of an innovative new exposure funded in early August. We can provide further details on the new exposure in our next update. Recent market dynamics also played a role in the decision to lower the Australian and Global Sovereign Bonds allocations. Government bonds continue to rally (yields lower) far beyond what we believe can be justified by current economic fundamentals. As extraordinary monetary policies are gradually withdrawn in the United States, and the market starts to look towards higher interest rates, bonds are likely to sell off in Australia and abroad. While yields may stay low for a few months, it was prudent to lower our exposure at these attractive levels because, when yields move higher, they are likely to do so rapidly. 
There were no changes to the manager line-up in July. 
 &lt;#top&gt;
If you want to remove yourself from this distribution list, request to be removed here &lt;mailto:Suzanne.Whiting@mercer.com?subject=Remove from MST Portfolio Snapshot distribution list&quot;&gt;&lt;span style=&gt;.  

If you no longer wish to receive any Mercer e-mails, unsubscribe here &lt;http://info.mercer.com/UnsubscribePage.html&gt;.











SuperRatings has awarded the Mercer Super Trust Corporate Superannuation Division its highest rating of ‘Platinum Rating’ for 2014. ‘SuperRatings’ does not issue, sell, guarantee or underwrite this product. Chant West has awarded the Mercer Super Trust the highest rating of ‘5 Apples’ for the Superannuation Division for 2014. The Chant West ratings logo is a trademark of Chant West Pty Limited and used under licence. For further information about the methodology used by Chant West, please see www.chantwest.com.au &lt;http://www.chantwest.com.au/&gt;. The Heron Partnership (Heron) has awarded Mercer’s Corporate Products (including the Mercer Super Trust’s Corporate Super and SmartSuper divisions) its highest ‘5 Star’ Quality rating for 2014.






 &lt;#2&gt;*NOTE &lt;#2&gt;: &lt;#2&gt; References to the ‘Mercer Growth’ are to the 
Mercer Super Trust’s Mercer Growth investment option and returns quoted are “pre-tax, pre-fees,” with the exception of performance numbers that have been sourced from the SuperRatings Fund Crediting Rate Survey, which are after tax and fees.  This will also apply to references to Mercer SmartPath returns, which relate to the largest Mercer SmartPath investment option in the Mercer Super Trust – Mercer SmartPath 1969-73.  








This email has been prepared by Mercer Superannuation (Australia) Limited (MSAL) ABN 79 004 717 533, Australian Financial Services Licence #235906. Address: Collins Square, 727 Collins Street, Melbourne VIC 3008. 
Tel: 03 9623 5555. MSAL is the trustee of the Mercer Super Trust, ABN 19 905 422 981. Any advice contained in this document is of a general nature only, and does not take into account the personal needs and circumstances of any particular individual. Prior to acting on any information contained in this document, you need to take into account your own financial circumstances, consider the Product Disclosure Statement for any product you are considering, and seek professional advice from a licensed, or appropriately authorised, financial 
adviser if you are unsure of what action to take. Past performance should not be relied upon as an indicator of future performance. &apos;MERCER&apos; is a registered trademark of Mercer (Australia) Pty Ltd ABN 32 005 315 917.

Copyright 2014 Mercer LLC. All rights reserved.﻿﻿

Find a Mercer office &lt;http://www.mercer.com/about-us/locations.html&gt;</textOnly>
    <type>custom</type>
    <uiType>Aloha</uiType>
</EmailTemplate>
