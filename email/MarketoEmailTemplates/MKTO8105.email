<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
</head><body ><div id="Section 1" class="mktEditable" >
<div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="602">
<tbody>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="600">
<tbody>
<tr>
<td><a href="http://www.imercer.com/content/global-mobility-overview.aspx"><img src="http://www.imercer.com/content/GM/email/mmu/mobility-update-banner-1501.jpg" border="0" alt="Global Mobility Updates from Mercer" width="600" height="200" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="30" valign="top">
<p>&nbsp;</p>
</td>
<td width="350" align="center" valign="top">
<h1 style="font: 400 14pt Arial, Helvetica, sans-serif; color: #00a8c8; margin-top: 0; margin-bottom: 20px; text-align: left;">Foundations of an International Assignment: What to Consider, How to Pay</h1>
<p style="font: 10pt Arial, Helvetica, sans-serif; color: #00a8c8; margin-bottom: 20px; text-align: left;">By Aviva Shavit</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">With the increasing complexity that globalization brings to international assignments, employers sometimes overlook the basic elements underlying an expatriate program.
 Although assignment objectives may vary, each posting represents a mutual investment for both the organization and the employee: A financial and human resources investment for the company; a career and family investment for the individual.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #00a8c8; margin-top: 0px; text-align: left;">▶ <a style="color: #006d9e;" href="http://www.imercer.com/content/foundations-of-an-international-assignment.aspx">Continue reading</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="location" name="location"></a>Location Updates</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">Argentina</strong> &ndash; In Mercer's October 2014 pricing of <strong>Buenos Aires</strong>, Argentina, we  measured significant price increases in all categories of goods 
and services.
 The INDEC (Argentina&rsquo;s official statistics agency) is reporting an inflation rate of 24%.
 These price increases will affect indices both into and out of Argentina.
 Most indices into Argentina will increase by varying degrees depending on the home location.
 Conversely, indices out of Argentina are likely to decline, especially to assignment countries with low levels of inflation.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">Republic of the Congo</strong> &ndash; An Expatriate index is now available for <strong>Brazzaville</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">Lithuania</strong> &ndash; On 1 January, Lithuania became the last of the three Baltic countries to adopt the euro, following Estonia in 2011 and Latvia in 2014.
 The official exchange rate is EUR 1 = LTL 3.4528.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">Romania</strong> &ndash; Expatriate and Efficient Purchaser Indices (EPI) are now available for <strong>Pitesti</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">Russia</strong> &ndash; The Russian ruble has lost nearly half of its value in recent months.
  Much of this decline has been attributed to the effects of the economic sanctions imposed against Russia by Western countries and by the declining price of oil, on which Russia&rsquo;s economy is heavily based.
 The ruble&rsquo;s declining value has led to price increases, especially for imported goods, and this will have a direct effect on the cost of living indices into and out of Russia.
 However, at this point it is not clear what will happen to prices when the stocks of imported items are depleted and replaced by less expensive brands, or simply not available.
 Mercer is conducting on-the-ground pricing surveys in <strong>Moscow</strong> in January.
 Results will be published in February.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">United Arab Emirates</strong> &ndash; <strong>Abu Dhabi</strong> has implemented a new water and electricity tariffs structure as of 1 January.
 Consequently,  utility costs are  increasing substantially.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><strong style="color: #00a8c8;">United States</strong> &ndash; Expatriate and Efficient Purchaser Indices (EPI) are now available for <strong>Nashville</strong>, Tennessee, and <strong>Greenville</strong>, 
South Carolina.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; margin-bottom: 0px; text-align: left;"><a id="events" name="events"></a>Upcoming Events</h2>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #002c77; margin-top: 36px; text-align: left;">Register for the First of Our 2015 Webcast Series: Building Local Plus Packages</h3>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia, Australia, and <br /> New Zealand</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">3 February<br /> 11 am Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #00a8c8; padding: 4px; color: #00a8c8; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsap.webex.com/mercereventsap/onstage/g.php?MTID=eee86f30e380a37eac349f5f1b2f17aa2">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">4 February<br /> 2 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #00a8c8; padding: 4px; color: #00a8c8; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?MTID=e1612f697b5fd245a294d8017c82fb805">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">5 February<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #00a8c8; padding: 4px; color: #00a8c8; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?MTID=e44cb6f6bec5507229077eadcee3a0034">Register</a></td>
</tr>
</tbody>
</table>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="lps" name="lps"></a>Local Plus Practices in Key Locations</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">The 2014 location-specific <em>Local  Plus Surveys</em> offer an in-depth look at prevalent policies and practices in  popular Local Plus locations.
 Get reports for<strong> Brazil</strong>, <strong>Hong Kong</strong>, <strong>Singapore</strong>, and <strong>United States</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #00a8c8; padding: 4px; color: #006d9e; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/products/local-plus-surveys.aspx">Order Now</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="housing" name="housing"></a>Control Housing Costs</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Make more defensible, better-informed housing allowance decisions with <strong><a style="color: #006d9e;" href="http://www.imercer.com/uploads/GM/housing/index.html">Mercer Mobilize Housing 
Solution</a></strong>&trade;.
  Our award-winning tool arms you with the information and functionality you need  to confidently set appropriate housing allowances, for more than 400 locations.
  You can instantly:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li> Search for housing options based on a specific budget amount.</li>
<li>View real sample housing listings, with photos, from the time of our surveys.</li>
<li>Explore neighborhoods and plot sites like schools, airports, and your company office on an interactive map.</li>
<li>Recalculate budgets instantly by filtering by neighborhood, cost category, apartment features, or changing currency.
 </li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="color: #006d9e;" href="https://info.mercer.com/Talent_Mobility-Mobilize-Housing-Solution.html">Get in touch</a> to set up a demo.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="gcpr" name="gcpr"></a>Essential HR Reference Guides</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Get comprehensive information at your fingertips to make sound hiring decisions, deploy employees worldwide, retain qualified talent, and understand relevant terms.</p>
<table border="0" cellspacing="0" cellpadding="0" width="348">
<tbody>
<tr>
<td style="font: bold 10pt/14pt Arial, Helvetica, sans-serif; color: #006d9e; margin-top: 0px; text-align: left;" width="110" valign="top">Compensation Handbook</td>
<td valign="top">
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-bottom: 14px; margin-top: 0;">
<li>Compensation components and pay structures.</li>
<li>Pay planning, budgeting, and benchmarking.</li>
<li>Incentives,  allowances, rewards, performance, and equity plans.</li>
</ul>
</td>
</tr>
<tr>
<td style="font: bold 10pt/14pt Arial, Helvetica, sans-serif; color: #006d9e; margin-top: 0px; text-align: left;" width="110" valign="top">Global Diversity and Inclusion Handbooks</td>
<td valign="top">
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-bottom: 14px; margin-top: 0;">
<li>Global approaches and strategies.
 </li>
<li>Stakeholder engagement.</li>
<li>Employee resource groups.</li>
<li>Career level challenges.</li>
<li>Metrics.</li>
</ul>
</td>
</tr>
<tr>
<td style="font: bold 10pt/14pt Arial, Helvetica, sans-serif; color: #006d9e; margin-top: 0px; text-align: left;" valign="top">Global HR Factbook</td>
<td valign="top">
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-bottom: 14px; margin-top: 0;">
<li>Economic environment.</li>
<li>Human capital environment.</li>
<li>Global compensation and benefits.</li>
<li>Global employment conditions.
 </li>
</ul>
</td>
</tr>
<tr>
<td style="font: bold 10pt/14pt Arial, Helvetica, sans-serif; color: #006d9e; margin-top: 0px; text-align: left;" width="110" valign="top">Global Mobility Handbooks</td>
<td valign="top">
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-bottom: 14px; margin-top: 0;">
<li>Volume 1 is a primer for newcomers to the expatriate field,  presenting pay approaches and packages, family and moving issues, tax, and  repatriation.</li>
<li>Volume 2 offers  advanced topics for experienced global mobility administrators.</li>
</ul>
</td>
</tr>
<tr>
<td style="font: bold 10pt/14pt Arial, Helvetica, sans-serif; color: #006d9e; margin-top: 0px; text-align: left;" width="110" valign="top">HR Management Terms</td>
<td valign="top">
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-bottom: 14px; margin-top: 0;">
<li>1,250  + terms in benefits, compensation, general HR practices, global mobility,  executive compensation, and more.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">&nbsp;</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #00a8c8; padding: 4px; color: #006d9e; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/content/global-products-overview.aspx">Order Now</a></p>
</td>
<td style="border-right: solid 1px #bfbfbf;" width="15" valign="top">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td valign="top">
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-right: 0px; margin-bottom: 12px; margin-left: 0px; text-align: left;">January 2015 &bull; Highlights</h6>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#">Foundations of an International Assignment: What to Consider, How to Pay</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#location">Location Updates</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#events">Upcoming Events</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#lps"> Local Plus Surveys</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#housing">Control Housing Costs</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #006d9e; text-decoration: none;" href="#gcpr">Essential HR Reference Guides</a></td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Doing Business in&hellip;<br /> <span style="color: #006d9e;">Mexico</span></h4>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">1.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Courtesy and good manners are ingrained in Mexican culture.
 Make sure to be polite in communications, adding a "please" and "thank you" wherever appropriate.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">2.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Maintaining personal relationships is paramount: Mexicans often communicate indirectly and subtly so that their words are diplomatic and 
non-confrontational.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #00a8c8;">3.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Given the fluid, almost circular, view of time, it is important to be flexible.
 Think of deadlines as desired objectives that can change as events affect timing.</td>
</tr>
</tbody>
</table>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><br /> Sources: <a style="color: #006d9e;" href="http://www.imercer.com/content/cultural-training-passport.aspx">CulturalTrainingPassport&trade;</a>, <a style="color: #006d9e;" 
href="http://www.imercer.com/content/culture-passport-app.aspx">Culture Passport On the Go</a></p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Home-Country Data and Tax Profiles</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New spendable income, housing norm, and hypothetical income tax data is effective for:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding: 0px; list-style: none; text-align: left;">
<li>United States (Boston)</li>
<li>United States (Chicago)</li>
<li>United States Missionary </li>
</ul>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Goods  and Services and Host Housing Survey Data</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New data for the following locations is available:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Argentina</li>
<li>Bahrain</li>
<li>Belgium</li>
<li>Belize</li>
<li>Bhutan</li>
<li>Bolivia</li>
<li>Bulgaria</li>
<li>Burundi</li>
<li>Cameroon</li>
<li>Congo, Republic of the</li>
<li> Cote d&rsquo;Ivoire</li>
<li>Czech Republic</li>
<li>Dominican Republic</li>
<li>Egypt</li>
<li>Ghana</li>
<li>Guatemala</li>
<li>Hong Kong</li>
<li>Hungary</li>
<li>India</li>
<li>Italy</li>
<li>Jamaica</li>
<li>Kazakhstan</li>
<li>Liberia</li>
<li>Macau</li>
<li>Morocco</li>
<li>Nigeria</li>
<li>Oman</li>
<li>Philippines</li>
<li>Romania</li>
<li>Rwanda</li>
<li>Saudi Arabia</li>
<li>Singapore</li>
<li>Spain</li>
<li>Sri Lanka</li>
<li>Sweden</li>
<li>Tunisia</li>
<li>Uganda</li>
<li>Ukraine</li>
<li>United States</li>
<li>Uruguay</li>
<li>Uzbekistan </li>
</ul>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #00a8c8; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">MercerPassport&reg;</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The following locations  have been updated on MercerPassport&reg;:</p>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Employee Mobility Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Argentina (Buenos Aires)</li>
<li>Canada (Montr&eacute;al, Toronto, Vancouver)</li>
<li>Estonia (Tallinn)</li>
<li>France (Lyon, Paris, Toulouse)</li>
<li>Germany (Berlin, Frankfurt, Hamburg, Munich)</li>
<li>Ireland (Dublin)</li>
<li>Italy (Milan, Rome, Turin)</li>
<li>Peru (Lima)</li>
<li>Switzerland (Basel, Bern, Geneva, Zurich) </li>
</ul>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Start Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Bolivia (La Paz)</li>
<li>Trinidad and Tobago (Port of Spain) </li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Start Guides are &lsquo;lite&rsquo; versions of the Employee Mobility  Guides; these country reports outline procedures and requirements for visiting  or relocating to up-and-coming 
locations.</p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more information about MercerPassport,    visit <a style="color: #006d9e;" href="http://www.imercer.com/content/mercer-passport-home.aspx">imercer.com</a>.</p>
<p style="font-size: 8pt; line-height: 10pt;">&nbsp;</p>
</td>
<td width="30" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">5505</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div id="column_text" class="mktEditable" align="center">
<table border="0" cellpadding="0" width="600">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
                 yourself from this  distribution list, <a style="color: #006d9e;" href="mailto:marketing.reply@mercer.com?subject=Remove from distribution list"> please inform us.</a><br /> If you no longer wish to receive any Mercer e-mails, <a style="color: #006d9e;" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
                <br /> <br /> We welcome your thoughts and input.
                      If you would like to contact us with your comments, please <a style="color: #006d9e;" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
                      We encourage you to forward this e-mail, and to link to content on our site.
                <br /> <br /> This email was sent by:<br /> <br /> Mercer<br /> 1166 Avenue of the Americas<br /> New York NY 10036<br /> USA  <br /> <br />Find the <a style="color: #006d9e;" href="http://www.mercer.com/about-us/locations.html">Mercer office</a> near you.</span></span> <span 
style="font-size: xx-small;"><br /> </span></div>
<hr style="color: #bfbfbf;" size="1" noshade="noshade" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a style="color: #006d9e;" href="http://www.mercer.com/terms.html">Terms of use</a> | <a style="color: #006d9e;" href="http://www.mercer.com/privacy.html"> Privacy</a> <br /> &copy;2015 
Mercer LLC, All                    Rights   Reserved</span></span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</body>
</html>