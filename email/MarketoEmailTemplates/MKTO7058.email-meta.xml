<?xml version="1.0" encoding="UTF-8"?>
<EmailTemplate xmlns="http://soap.sforce.com/2006/04/metadata">
    <available>true</available>
    <encodingKey>UTF-8</encodingKey>
    <name>Pac_IC_Defined Contribution_Alert or Notification_201410 MST Monthly Portfolio S</name>
    <style>none</style>
    <subject>Portfolio Management Snapshot</subject>
    <textOnly>Mercer Super Trust Portfolio Management Snapshot
This update from the Mercer Investments Portfolio Management team looks at developments over September 2014.

IN SEPTEMBER 2014...


 - Mercer Growth* returned -0.9%, outperforming its composite benchmark by 0.1%.  




 - In peer relative terms, September returns were in line with the SuperRatings Median Balanced Master Trust, and were slightly behind the broader Balanced Median.  




 - Mercer SmartPath 1969-73*, the largest SmartPath option, returned -1.4%, 0.4% ahead of its composite benchmark.



WHAT DID WE DO?


 - Appointed Allianz to the manager line up for our portfolios’ exposure to Global small cap shares. Refer to the latest September quarter Mercer Super Trust manager list &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf&gt; for a handy summary of all the manager changes made over the three months to 30 September 2014.
IN THIS ISSUE


ECONOMY &amp; MARKETS &lt;#1&gt;



EQUITIES STILL ATTRACTIVE DESPITE VOLATILITY &lt;#2&gt;



INVESTMENT PERFORMANCE &lt;#3&gt;



ASSET ALLOCATION &amp; MANAGER UPDATE &lt;#4&gt;


 &lt;http://www.linkedin.com/groups/Mercer-Investments-Asia-Pacific-4442861/about?trk=anet_ug_grppro&gt;


DOWNLOAD THE LATEST REPORT
ECONOMY &amp; MARKETS




September 2014 was a troubled month for investors, with both equity markets falling and bond yields rising. Australian markets were hit particularly hard, with shares returning -5.4% and Australian Sovereign Bonds returning -0.4%. The Australian dollar also fell 6.3% against a resurgent US dollar. The falling currency boosted overseas equity returns, which delivered 4.3% in unhedged terms, although underlying market weakness was exposed by the -0.9% hedged return. Global Sovereign Bonds and Credit returned -0.2% and -0.5% respectively. 

Somewhat ironically, these market falls can largely be attributed to strong economic data coming out of the United States! The third and final estimate of second quarter GDP growth was revised up to 4.6% from 4.2%, while a key 
index of manufacturing activity rose to its highest level since 2011. Employment data was worse than expected, although the pace of employment gains over the last six months continues to point to growth well above trend. Not surprisingly, the US Federal Reserve (the Fed) lowered the pace of its monthly asset purchase program by another $10b per month to $15b per month. With the program widely expected to conclude in October, words retained in the Fed statement that interest rates will remain on hold “for a considerable time after the asset purchase program ends” seemed inconsistent with economic developments. Indeed, Fed members revised their interest rate projections for the next three years higher. Elsewhere, concerns over the strength of the Chinese economy persisted, with the widely followed PMI showing further deterioration in domestic demand, while the European Central Bank lowered its benchmark interest rate to 0.05% and announced plans for further quantitative easing from October in 
order to prevent deflation becoming entrenched in Europe.
Positive US economic developments pushed bond yields and the US dollar higher but weighed on equities, as markets deliberated over the timing and impact of higher US rates. Commodity prices were hurt by the stronger US dollar and concerns regarding the Chinese economy. These factors impacted the Australian share market and currency particularly hard. Lower commodity prices obviously weighed on the miners, while higher bond yields reduced the attractiveness of yield stocks, including banks and listed property trusts. As Resources and Financials are the largest two sectors in the Australian market, our market significantly underperformed global peers. 

 &lt;#top&gt;









EQUITIES STILL ATTRACTIVE DESPITE RISING VOLATILITY






Market volatility continued into October, with commentators variously ascribing weak data, deflation fears and even the Ebola virus as the primary causes. However, the scale of the correction also needs to be put into perspective. We’ve been lulled by recent market stability (itself partly caused by the stable backdrop of “zero interest rate policy”) into believing that such market movements are unusual. In fact, for the bellwether US stock market, the recent drop (7%) has been matched or exceeded in every year this century except 2013.

We previously noted that the removal of monetary stimulus in the US would herald a return to more normal levels of market volatility. We suspect there is still more to come. However, the overall outlook for global growth, inflation, liquidity and interest rates remains supportive for equities in Mercer’s view. This is particularly so, relative to government bonds affected by current deflation fears that appear excessive in an environment of trend global growth. Conversely, interest rates will only rise if growth is strong, which will be good for earnings growth. Markets were already discounting the start of monetary tightening in the US and the UK in mid-2015 and this persists. Moreover, in an environment of low and stable inflation, there tends to be a positive correlation between bond yields and equity returns. Hence, Mercer has retained an overweight position in global equities.
Read the article &lt;http://www.mercer.com.au/insights/point/2014/mercer-markets-view-october-2014.html&gt; on our online insights  &lt;http://www.mercer.com.au/insights/point.html&gt;channel Mercer/Point^ &lt;#MercerPointfootnote&gt; for more detail.

 &lt;#top&quot;&gt;&lt;img title=&gt;









INVESTMENT PERFORMANCE 






Mercer Growth* returned -0.9% in September, 0.1% ahead of its composite benchmark. Our asset allocation positively contributed to performance, particularly our overweight position to unhedged global equities, as the Australian dollar depreciated 6.3% in the month, and global equities outperformed Australian equities. Manager selection also contributed positively to performance. This was led by the strong performance of our Global Sovereign Bond managers who benefitted from rising yields and a strengthening US dollar. 

From a peer relative perspective, September returns were in line with the SuperRatings Median Balanced Master Trust, but marginally behind the broader Balanced Median. More significantly, Mercer Growth returns have exceeded the Master Trust median over all periods between one quarter and ten years, and have outperformed the broader median over the quarter, year and three years. 

Due to the large number of Mercer SmartPath options, we only report performance for the Mercer SmartPath 1969-73 option here because this is the largest option by funds under management and is therefore the most representative of 
underlying investors. In September, the Mercer SmartPath 1969-73 option returned -1.4%, 0.4% ahead of its composite benchmark. Absolute returns were lower than Mercer Growth due to a higher strategic allocation to equities, particularly Australian equities. As our views are applied consistently across our suite of investment products, the key drivers of outperformance are the same as for Mercer Growth.
The September Monthly Report &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_CSD_MonthlyReportSeptember2014.pdf&gt; 
provides more details on the performance of Mercer Growth and Mercer SmartPath 1969-73, along with that of other investment options. 
 &lt;#top&gt;









ASSET ALLOCATION MANAGER UPDATE





No asset allocation changes were made in September. 
At the end of every quarter, we update our manager lists to reflect manager changes made over the preceding three months. Follow this links to the latest Mercer Super Trust manager list &lt;https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf&gt; (as at the end of September). On the first page of the manager list, you will find a handy summary of all the manager changes made over the quarter, including the appointment at the end of September of Allianz to the global small cap shares manager line up.
 
 

 &lt;#top&gt;
If you want to remove yourself from this distribution list, request to be removed here &lt;mailto:Suzanne.Whiting@mercer.com?subject=Remove%20from%20MST%20Portfolio%20Snapshot%20distribution%20list&quot;&gt;&lt;span style=&gt;.  

If you no longer wish to receive any Mercer e-mails, unsubscribe here &lt;http://info.mercer.com/UnsubscribePage.html&gt;.











SuperRatings has awarded the Mercer Super Trust Corporate Superannuation Division its highest rating of ‘Platinum Rating’ for 2014. ‘SuperRatings’ does not issue, sell, guarantee or underwrite this product. Chant West has awarded the Mercer Super Trust the highest rating of ‘5 Apples’ for the Superannuation Division for 2014. The Chant West ratings logo is a trademark of Chant West Pty Limited and used under licence. For further information about the methodology used by Chant West, please see www.chantwest.com.au &lt;http://www.chantwest.com.au/&gt;. The Heron Partnership (Heron) has awarded Mercer’s Corporate Products (including the Mercer Super Trust’s Corporate Super and SmartSuper divisions) its highest ‘5 Star’ Quality rating for 2014.






 &lt;#2&gt;*NOTE &lt;#2&gt;: &lt;#2&gt; References to the ‘Mercer Growth’ are to the 
Mercer Super Trust’s Mercer Growth investment option and returns quoted are “pre-tax, pre-fees,” with the exception of performance numbers that have been sourced from the SuperRatings Fund Crediting Rate Survey, which are after tax and fees.  This will also apply to references to Mercer SmartPath returns, which relate to the largest Mercer SmartPath investment option in the Mercer Super Trust – Mercer SmartPath 1969-73. 

^ &lt;#MercerPoint1&gt; Feel free to explore Mercer/Point &lt;http://www.mercer.com.au/insights/point.html&gt;, which contains other 
Investments-related articles that may be of interest to you.








This email has been prepared by Mercer Superannuation (Australia) Limited (MSAL) ABN 79 004 717 533, Australian Financial Services Licence #235906. Address: Collins Square, 727 Collins Street, Melbourne VIC 3008. 
Tel: 03 9623 5555. MSAL is the trustee of the Mercer Super Trust, ABN 19 905 422 981. Any advice contained in this document is of a general nature only, and does not take into account the personal needs and circumstances of any particular individual. Prior to acting on any information contained in this document, you need to take into account your own financial circumstances, consider the Product Disclosure Statement for any product you are considering, and seek professional advice from a licensed, or appropriately authorised, financial adviser if you are unsure of what action to take. Past performance should not be relied upon as an indicator of future performance. &apos;MERCER&apos; is a registered trademark of Mercer (Australia) Pty Ltd ABN 32 005 315 917.

Copyright 2014 Mercer LLC. All rights reserved.﻿﻿

Find a Mercer office &lt;http://www.mercer.com.au/about-us/locations.html&gt;</textOnly>
    <type>custom</type>
    <uiType>Aloha</uiType>
</EmailTemplate>
