<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
</head>
<body ><div ><div class="mktEditable" id="edit_text_1" >
<div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="602">
<tbody>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="600">
<tbody>
<tr>
<td><a href="http://www.imercer.com/content/global-mobility-overview.aspx"><img src="http://www.imercer.com/content/GM/email/mmu/mobility-update-banner-1404.jpg" border="0" alt="News for Global Mobility Clients" width="600" height="169" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="30" valign="top">
<p>&nbsp;</p>
</td>
<td width="350" align="center" valign="top">
<h1 style="font: 400 14pt Arial, Helvetica, sans-serif; color: #f48132; margin-top: 0; margin-bottom: 20px; text-align: left;">Mercer Mobility Case Study</h1>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left; font-style: italic;">In this new series, our mobility consultants  share client success stories in managing global mobility challenges.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">The Consultant</strong><br /> <a style="color: #c45f24;" href="mailto:vadim.kostovski@mercer.com">Vadim Kostovski</a>, Senior Associate, New York</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">The Client<br /> </strong>A United States subsidiary of an industrial MNC with a worldwide presence.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">The Challenge<br /> </strong>The US entity of this company planned a temporary assignment of a high level US-based executive to the United Kingdom.
   The company had only a few employees that could be considered "expatriates" and no formal policy or even guidelines for an international transfer.
   Mercer Mobility came in to help define the terms of the assignment.
   The time constraint faced by the client dictated the need to complete the work in under a week.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">The Plan<br /> </strong>We needed to help the client define an expatriate assignment package both in terms of cost and in terms of appropriate assignment  
provisions.
  Due to time constraints, a comprehensive review of what would be appropriate for this expatriate assignment (based on a comparative benchmarking study) was not an option.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">We identified that the best approach was to use a modification of our Mercer Mobility Starter Kit.
  This package would fill the client's immediate need with an eye on a longer-term solution.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">Action</strong><br /> A set of assumptions was established to make an assessment of costs involved in a Cost Projection and to concurrently generate a rough  
outline of assignment provisions.
  These would serve as a foundation for this and future transfers of a similar nature.
  We used Mercer's resources and expertise with respect to general market practices to determine these assumptions.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The next step was to provide the client with a compensation statement and a Letter of Understanding (LOU) to be used to communicate the package to the candidate.
  An LOU template was crafted, incorporating provision assumptions developed for the cost estimate and compensation statement.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">Resolution</strong><br /> A package of deliverables was developed allowing the client's CHRO to quickly communicate the package to internal stakeholders.
  The client successfully addressed the immediate need and now has a foundation for future transfers.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="location" name="location"></a>Location News</h2>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #c45f24; margin-top: 0px; text-align: left;">Cameroon and Nigeria Added as Standard Home Locations</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer is pleased to announce that Cameroon and Nigeria  have been added as standard home locations to our data offering.
   The first Home Country Data and Tax Profiles for Cameroon and Nigeria include tables of goods and services spendable income, housing norms, and hypothetical income taxes.
  These data have been incorporated into the international compensation tables.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Hypothetical income taxes reflect current tax rates and applicable deductions effective in Cameroon and Nigeria.
  Parameters used in the calculation of goods and services spendables and housing norms are given for subscribers who require data not provided in the tables.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer has previously supplied Cameroon- and Nigeria-based data only on a customized basis.
  If you have current expatriates outbound from these two locations, the use of these data may alter their compensation.
  If you would like advice on implementing the new data or communicating with expatriates, please contact your consultant.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">Lebanon</strong> &ndash;  The level of political violence in Lebanon has escalated since early 2013, and this has affected safety and security conditions in  
<strong>Beirut</strong>.
   The current rising level of violence in Lebanon is primarily the result of (1) the factionalized nature of Lebanese politics, which reflects underlying religious differences, and (2) the civil war in neighboring Syria, which has led to a massive influx of highly partisan Syrian refugees and  
Syrian shelling of the border regions of the country.
  The continuing presence of Palestinian refugees from Israel also contributes to tensions within Lebanon and with the state of Israel.
   In Beirut, much of the violence has taken place in the southern Shia districts of the city.
  In November 2013, a double bombing at the Iranian embassy killed dozens of people, and lethal bombings have continued into 2014.
  Demonstrations may turn violent as protesters clash with security forces.
   Mercer's <em>Location Evaluation Report</em> for Beirut has been updated to take into account the latest developments.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">United States </strong>&ndash; The Internal Revenue Service has issued revised Section 911 housing exclusion limitations effective in 2014.
   These adjustments are made on the basis of geographic differences abroad relative to housing costs in the United States.
   The amounts can be used by US citizens and residents who are working abroad to compute their exclusion or deduction for foreign housing costs.
   A <a style="color: #c45f24;" href="http://www.irs.gov/pub/irs-drop/n-14-29.pdf">complete list of exclusion amounts</a> can be found on irs.gov.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #f48132;">Venezuela </strong>&ndash;  Hyperinflation, shortages of basic consumer goods, high levels of crime, and opposition to President Maduro have sparked  
anti-government demonstrations in Venezuela.
  The wave of anti-government protests resulted in several deaths and many more injuries from February through April.
   The following two factors have led to the food shortages in Venezuela, a country that relies heavily on food imports: inadequate domestic food production and, furthermore, the lack of foreign currency needed to import the raw materials necessary for production.
  Certain basic products such as milk, butter, sugar, flour, and toilet paper have been difficult to obtain.
  Attempts at price controls are exacerbating the shortages.
  According  to Venezuela&rsquo;s central bank, the scarcity index (measuring the rate at which  retailers do not stock specific items) exceeded 25% in January; this means that more than one in  four stores do not stock an item.
  Mercer's <em>Location Evaluation Report</em> for <strong>Caracas</strong> has been updated to take into account the latest developments.</p>
<p style="font: italic 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more HR news from  around the world, subscribe to <a style="color: #c45f24;" href="http://select.mercer.com/">Mercer Select</a>, a daily email with insights and analysis on a  
broad spectrum of benefit, compensation, and HR issues.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; margin-bottom: 0px; text-align: left;"><a id="events" name="events"></a>Upcoming Events</h2>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #8c3709; margin-top: 36px; text-align: left;"><em>Next in Our Webcast Series<br /> </em>Unconscious Bias and Candidate Selection: Impact on Talent Identification and Global Development Opportunities</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">This webcast will focus on the impact unconscious bias has on talent identification, assignment candidate selection, and opportunities for development in an international context.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia, Australia, and <br /> New Zealand</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">20  May<br /> 3 pm Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsap.webex.com/mercereventsap/onstage/g.php?t=a&amp;d=969941407">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">20  May<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?t=a&amp;d=666519268">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">20  May<br /> 2 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=666175383">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #8c3709; margin-top: 36px; text-align: left;">Last Chance to Join Us in Miami</h3>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150"><a style="color: #c45f24;" href="http://www.imercer.com/naemc">North America  <br /> Expatriate Management Conference</a></td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">13-15 May<br /> Coral Gables, Florida <br /> <em>Sign up by 5 May!</em></td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" href="https://www.seeuthere.com/naemc2014">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #8c3709; margin-top: 36px; text-align: left;">Canada Global Mobility Trends and Challenges Events</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Join us for these complimentary, half-day events that provide insights into global mobility trends and an opportunity to network with your peers.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Toronto</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">27 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://surveys.az1.qualtrics.com/SE/?SID=SV_0V46AAO7AgNWotf">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Montreal</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">29 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://surveys.az1.qualtrics.com/SE/?SID=SV_0V46AAO7AgNWotf">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Calgary</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">3 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="https://surveys.az1.qualtrics.com/SE/?SID=SV_0V46AAO7AgNWotf">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #8c3709; margin-top: 36px; text-align: left;">Expatriate Management Training</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer offers training courses for international assignment  specialists in various locations.
  Please visit <a style="color: #c45f24;" href="http://www.imercer.com/content/global-mobility-events.aspx">imercer.com/gmevents</a> to learn more  about training opportunities in your region.
  Except where noted, the following upcoming sessions are <strong>fundamentals</strong> courses:</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Frankfurt (Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">15-16 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Madrid</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">20 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Barcelona</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">21 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Frankfurt</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">21 May</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">London (Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">10-11 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Hong Kong</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">12 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/content/apac-expat-seminars.aspx">Details</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Zaragoza</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">17 June</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">New York (Principles <br /> and Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">13-14 August</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/content/NA-expat-seminars.aspx">Register</a></td>
</tr>
</tbody>
</table>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="short" name="short"></a>Are You Setting Accurate Short-term Allowances?</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Our <strong>Short-term Assignment Per Diem Calculator</strong> provides an appropriate, consistent, and cost-effective means of compensating employees for day-to-day living costs in more  
than 375 short-term assignment locations.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The <strong>Short-term Assignment Per Diem Calculator</strong> gives you recommendations that are specific to multinational companies, and based on spending patterns of actual  
business-oriented assignees like yours, including the provision of self-catering facilities.
  Based on our long-standing methodology and up-to-date information, the recommendations are accurate, and are broken down in detail so you know exactly what makes up the amount.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The calculator is easily customized &ndash; use it to build a market basket appropriate for your assignees, and adjust it depending on budget, type of assignment, and other factors.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #f48132; padding: 4px; color: #c45f24; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/products/2014/short-term-assignment-per-diems.aspx#pricingoptions">Pricing and Ordering</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="benchmark" name="benchmark"></a>How Do Your Policies Measure Up?</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Let us help you ensure your global mobility strategy is working.
  Whether you currently have or are considering a "one policy that fits all" approach or policy segmentation for various assignment and expatriate types, Mercer's <a style="color: #c45f24;" href="http://www.imercer.com/content/mobility-benchmarking.aspx">benchmarking offerings</a> can guide you to 
 a more robust and effective mobility program.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Ask you Mercer Mobility consultant about benchmarking your policy  today.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="carpol" name="carpol"></a>Global Car Policies</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The 2014 <a style="color: #c45f24;" href="http://www.imercer.com/products/2014/global-car-policies.aspx">Global Car Policies</a> report keeps HR leaders  abreast of current policy trends and 
 competitive market data to support their  corporate car planning and budget allocations in 90 markets.
  The report:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">
<li>Enables organizations to review company car policies against  local markets.
 </li>
<li>Highlights differences in car policy practices across key  markets.
 </li>
<li>Identifies cost-saving measures.
 </li>
</ul>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="totalemp" name="totalemp"></a>Total Employment Costs Around the World</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer&rsquo;s 2014 <a style="color: #c45f24;" href="http://www.imercer.com/products/2014/total-employment-costs.aspx">Total Employment  Costs around the World </a>report helps human  
resources and business managers  assess employee costs in 72 major markets, across four career streams and 11  position classes (PCs).
  This report:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">
<li>Identifies cost-saving opportunities in talent recruiting.</li>
<li>Delivers key information for business planning and budgeting.</li>
<li>Provides valuable insights on total employment costs.</li>
</ul>
</td>
<td style="border-right: solid 1px #bfbfbf;" width="15" valign="top">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td valign="top">
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-right: 0px; margin-bottom: 12px; margin-left: 0px; text-align: left;">April 2014 &bull; Highlights</h6>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#">Case Study</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#location">Location News</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#events">Webcasts, Conferences, and Training</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#short">Short-term Assignments</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#benchmark">Benchmarking Your Program</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#carpol">Global Car Policies</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #f48132;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #c45f24; text-decoration: none;" href="#totalemp">Total Employment Costs Around the World</a></td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Home-Country Data and Tax Profiles</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New spendable income, housing norm, and hypothetical income tax data are effective for these locations:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding: 0px; list-style: none; text-align: left;">
<li>Cameroon*</li>
<li> Canada</li>
<li> Canada (Calgary)</li>
<li> Canada (Missionary)</li>
<li> Canada (Montreal)</li>
<li>Canada (Toronto)</li>
<li> Colombia</li>
<li> France</li>
<li> Nigeria*</li>
<li> Norway </li>
<li>Mexico </li>
<li>United Arab Emirates </li>
<li>United Kingdom </li>
<li>United Kingdom (London and Southeast England) </li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">* new location</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Goods  and Services and Host Housing Survey Data</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New data for the following locations is available:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Albania</li>
<li>Argentina</li>
<li>Armenia</li>
<li>Azerbaijan</li>
<li>Benin </li>
<li>Bosnia-Herzegovina</li>
<li>Chad</li>
<li>Chile</li>
<li>China</li>
<li>Colombia</li>
<li>Croatia</li>
<li>French Polynesia</li>
<li>Iceland </li>
<li>Japan</li>
<li>Kenya</li>
<li>Kosovo</li>
<li>Latvia</li>
<li>Lithuania</li>
<li>Macedonia </li>
<li>Malta</li>
<li>Mauritania </li>
<li>Mongolia</li>
<li>Niger</li>
<li>Norway</li>
<li>Paraguay</li>
<li>Portugal</li>
<li>Serbia</li>
<li>Slovakia</li>
<li>St.
  Lucia</li>
<li>Togo</li>
<li>Trinidad &amp; Tobago</li>
<li>Ukraine</li>
<li>Venezuela</li>
<li>Vietnam </li>
<li>Zambia</li>
</ul>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">MercerPassport&reg;</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">No locations  were updated on MercerPassport in April.
  The next release will be on 8 May.
  For more information about MercerPassport,  visit <a style="color: #c45f24;" href="http://www.imercer.com/content/mercer-passport-home.aspx">imercer.com</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Get the Latest Mobility Tips</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Follow  us on Twitter <a style="color: #c45f24;" href="http://www.twitter.com/mercermobility">@MercerMobility</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #f48132; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Words.
  Pictures.
  Insights.</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><a style="color: #c45f24;" href="http://mthink.mercer.com/employers-worldwide-explore-alternative-mobility-strategies/">Employers Worldwide Explore Alternative Mobility Strategies</a></p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><a href="http://mthink.mercer.com/employers-worldwide-explore-alternative-mobility-strategies/"><img 
src="http://www.imercer.com/content/GM/email/mmu/140407-mercer-127-alternativeassignments-thumb.png" border="0" alt="MercerThink Infographic" width="160" height="637" /></a></p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">MercerThink  is a weekly infographic of HR-related trends around the world.
  Sign up at <a style="color: #c45f24;" href="http://mercerthink.mercer.com/">mercerthink.mercer.com</a>.</p>
<p style="font-size: 8pt; line-height: 10pt;">&nbsp;</p>
</td>
<td width="30" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">5097</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div id="column_text" class="mktEditable" align="center">
<table border="0" cellpadding="0" width="608">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
                  yourself from this  distribution list, <a style="color: #c45f24;" href="mailto:marketing.reply@mercer.com?subject=Remove from distribution list"> please inform us.</a><br /> If you no longer wish to receive any Mercer e-mails, <a style="color: #c45f24;" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
                 <br /> <br /> We welcome your thoughts and input.
                       If you would like to contact us with your comments, please <a style="color: #c45f24;" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
                       We encourage you to forward this e-mail, and to link to content on our site.
                 <br /> <br />Find the <a style="color: #c45f24;" href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</span></span> <span style="font-size: xx-small;"><br /> </span></div>
<hr style="color: #bfbfbf;" size="1" noshade="noshade" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a style="color: #c45f24;" href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a style="color: #c45f24;" href="http://www.mercer.com/privacy.jhtml"> Privacy</a> <br /> 
&copy;2014 Mercer LLC, All                    Rights   Reserved</span></span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</body>
</html>