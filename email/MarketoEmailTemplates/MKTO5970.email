<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title></title>
</head>
<body ><div ><div class="mktEditable" id="edit_text_1" >
<div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="602">
<tbody>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="600">
<tbody>
<tr>
<td><a href="http://www.imercer.com/content/global-mobility-overview.aspx"><img src="http://www.imercer.com/content/GM/email/mmu/mobility-update-banner-1408.jpg" border="0" alt="News for Global Mobility Clients" width="600" height="169" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td width="30" valign="top">
<p>&nbsp;</p>
</td>
<td width="350" align="center" valign="top">
<h1 style="font: 400 14pt Arial, Helvetica, sans-serif; color: #ef4e45; margin-top: 0; margin-bottom: 20px; text-align: left;">How Do &ldquo;Expat Lite&rdquo; Programs Manage to Cut Costs?</h1>
<p style="font: 10pt Arial, Helvetica, sans-serif; color: #ef4e45; margin-bottom: 20px; text-align: left;">By James O&rsquo;Neill</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">&ldquo;Expat  lite&rdquo; refers to a reduced version of the traditional expatriate balance sheet  compensation approach, which maintains the assignee&rsquo;s standard of living  throughout  
the assignment (with the exception of any incentives the employer  decides to provide).
  The traditional full expatriate package &ndash; base pay, premiums,  a wide array of allowances, and other perquisites (for example, a company car)  &ndash; generally costs the company about 2.5 times the individual&rsquo;s annual base  salary.
  In comparison, &ldquo;expat lite&rdquo; packages may run higher as a multiple of  base pay, <em>but</em> they significantly lower  the overall cost of an individual assignment.
  At the same time, administering  &ldquo;expat lite&rdquo; packages may be simpler than placing assignees on host-based  compensation, as with the Local Plus approach.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #ef4e45; margin-top: 0px; text-align: left;">▶ <a style="color: #ba2c2b;" href="http://www.imercer.com/content/how-expat-lite-programs-cut-costs.aspx">Continue reading</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="mmhs" name="mmhs"></a>Mercer Mobilize Housing Solution&trade;</h2>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #ba2c2b;">New Enhancements and Pricing   + Client Webcasts</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">To help you defend your budget against unrealistic expectations and exception requests, we&rsquo;ve improved our intuitive online expatriate housing platform <span style="font: 10pt/14pt Arial, Helvetica,  
sans-serif; color: #37424a; margin-top: 0px; text-align: left;">&ndash;</span> the  <a style="color: #ba2c2b;" href="http://www.imercer.com/uploads/GM/housing/index.html">Mercer Mobilize Housing Solution&trade;</a> (MMHS).
   Back-end technology enhancements have allowed us to dramatically reduce the fees for MMHS Advanced, which provides interactive maps, real sample listings, detailed neighborhood information, and reverse lookup by budget.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">The new prices are:</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<th style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #fff; vertical-align: top; background-color: #ba2c2b; padding: 4px;">MMHS Advanced</th> <th style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #fff; vertical-align: top; background-color: 
#ba2c2b; padding: 4px;">USD</th> <th style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #fff; vertical-align: top; background-color: #ba2c2b; padding: 4px;">EUR</th> <th style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #fff; vertical-align: 
top; background-color: #ba2c2b; padding: 4px;">GBP</th>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">Per location</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">$350</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">&euro;260</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">&pound;220</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">Full set (350+ locations)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">$10000</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">&euro;7400</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top; border-bottom: solid 1px #ba2c2b; padding: 4px;">&pound;6250</td>
</tr>
</tbody>
</table>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">For more information on the tool and the latest enhancements, join us for one of these client-only webcasts:</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">10 September<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mmc.webex.com/mmc/onstage/g.php?d=715523267&amp;t=a">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia and the Pacific</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">11 September<br /> 10 am Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mmc.webex.com/mmc/onstage/g.php?d=711429392&amp;t=a">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">11 September<br /> 2 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mmc.webex.com/mmc/onstage/g.php?d=715189297&amp;t=a">Register</a></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Watch your email for more details.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="location" name="location"></a>Location News</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ef4e45;">France</strong> &ndash; Expat and Efficient Purchaser (EPI)  indices are now available for <strong>Strasbourg</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ef4e45;">Germany</strong> &ndash; Expat and Efficient Purchaser (EPI)  indices are now available for <strong>Ludwigshafen</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ef4e45;">Russia </strong>&ndash; On 7 August, Russia imposed a ban on imported food items from  Australia, Canada, the European Union, Norwary, and the United States   
for a period of one year.
  The embargo includes pork, beef, poultry, fish, dairy products, fruits, and vegetables.
  Economists predict a price rise in the coming months.
  Mercer is monitoring the situation closely.
  So far, food prices have not increased dramatically.
  For more information on the situation in Russia, please contact your  consultant.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ef4e45;">Sweden</strong> &ndash;<strong> </strong>Expat and Efficient Purchaser (EPI)  indices are now available for <strong>Malmo</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ef4e45;">Uruguay</strong> &ndash; An Efficient Purchaser index (EPI) is now available for <strong>Montevideo</strong>.</p>
<p style="font: italic 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more HR-related news from  around the world, subscribe to <a style="color: #ba2c2b;" href="http://select.mercer.com/">Mercer Select</a>, a daily email with insights and analysis  
on a broad spectrum of benefit, compensation, and HR issues.</p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; margin-bottom: 0px; text-align: left;"><a id="events" name="events"></a>Upcoming Events</h2>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #810009; margin-top: 36px; text-align: left;">Expatriate Management Training</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer offers training courses for international assignment  specialists in various locations.
  Please visit <a style="color: #ba2c2b;" href="http://www.imercer.com/content/global-mobility-events.aspx">imercer.com/gmevents</a> to learn more  about training opportunities in your region.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">London (Fundamentals)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="110">3-4 September</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">Houston (Principles and Advanced)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">16-17 September</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/content/NA-expat-seminars.aspx">Register</a></td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="110">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Bilbao (Fundamentals)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="110">18 September</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">Toronto (Principles)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;">23 September</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/content/NA-expat-seminars.aspx">Register</a></td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="110">&nbsp;</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Seville (Fundamentals)</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="110">25 September</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.imercer.com/uploads/Europe/HTML/landing_pages/2013/MercerLearning/mobility.html">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #810009; margin-top: 36px; text-align: left;">Global Mobility Webcast Series</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><strong style="color: #ba2c2b;">Developmental Moves, Global Nomads, and Intra-regional Assignments</strong><br /> A multi-tier approach to managing your globally mobile workforce can  
introduce complexity to your program, but also bring  exciting results.
  In this webcast, we will cover managing developmental assignments, global nomads, and moving employees within the same region.
  The webcast will feature findings from Mercer's  Alternative International Assignments Policies and Practices Survey.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">18 September<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?t=a&amp;d=666228561">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">18 September<br /> 2 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=660153344">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia and the Pacific</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">23 September<br /> 11 am Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsap.webex.com/mercereventsap/onstage/g.php?t=a&amp;d=963312189">Register</a></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><br /> <strong style="color: #ba2c2b;">Mobility and Global Leadership Development Strategies</strong><br /> Explore the importance of global mobility and international work experience in the 
 development of leaders in multinational organizations.</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Asia and the Pacific</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">21 October<br /> 3 pm Singapore</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsap.webex.com/mercereventsap/onstage/g.php?t=a&amp;d=966824819">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Europe, Middle East, <br /> and Africa</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">21 October<br /> 2 pm London</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercereventsuk.webex.com/mercereventsuk/onstage/g.php?t=a&amp;d=661487436">Register</a></td>
</tr>
<tr>
<td width="150">&nbsp;</td>
<td width="100">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Americas</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">21 October<br /> 1 pm New York</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="https://mercerevents.webex.com/mercerevents/onstage/g.php?t=a&amp;d=663771181">Register</a></td>
</tr>
</tbody>
</table>
<h3 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #810009; margin-top: 36px; text-align: left;">Conferences</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; font-weight: bold; color: #ba2c2b; margin-top: 0px; text-align: left;">2014 Mercer EMEA Compensation &amp; Benefits Conference</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="150">Stockholm</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; vertical-align: top;" width="100">9-10 October</td>
<td style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: right; color: #37424a; vertical-align: top;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" 
href="http://www.mercersignatureevents.com/emea_compensation_benefits_2014/index.shtml">Register</a></td>
</tr>
</tbody>
</table>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="localizer" name="localizer"></a>Host-based Package Calculator</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Mercer's new calculator will streamline the way you establish pay for your international employees when placing them on a local compensation program.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Use <a style="color: #ba2c2b;" href="http://www.imercer.com/content/compensation-localizer.aspx">Compensation Localizer</a> to quantify the economic impact whenever you are putting an employee on a  
host-based compensation package, including for:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Local Plus packages</li>
<li>Localization</li>
<li>Permanent transfers</li>
<li>Locally or directly hired foreigners</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/content/compensation-localizer.aspx">Learn More</a></p>
<h2 style="font: bold 12pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 10px; margin-top: 36px; text-align: left;"><a id="gcpr" name="gcpr"></a>Optimize  Compensation Planning with GCPR</h2>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer&rsquo;s <a style="color: #ba2c2b;" href="http://www.imercer.com/products/global-comp-planning.aspx">Global Compensation Planning Report</a> (GCPR) is a must-have tool for all global  
HR professionals looking to budget worldwide salaries for the coming year.
  GCPR is updated in July and October  to coincide with compensation planning.
  GCPR also offers an annual subscription, with updates as new survey data comes available (at least quarterly), with the ability to add on industry-specific pay increase information.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">GCPR presents  hard-to-find information on salary trends and key economic and labour market  indices, covering 123 countries (and 141 markets):</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">
<li>Data:       Pay increase trends (excluding and including zeros), variable bonus, attraction       and retention, expected headcount changes, and turnover.&nbsp;&nbsp; </li>
<li>Economic trends: GDP, inflation, and unemployment.
 </li>
<li>Employee levels: executive, management, professional sales       and nonsales, and para-professional sales and nonsales.</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;"><a style="border: solid 1px #ef4e45; padding: 4px; color: #ba2c2b; text-decoration: none; text-transform: uppercase;" href="http://www.imercer.com/products/global-comp-planning.aspx">Order Now</a></p>
</td>
<td style="border-right: solid 1px #bfbfbf;" width="15" valign="top">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td valign="top">
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-right: 0px; margin-bottom: 12px; margin-left: 0px; text-align: left;">August 2014 &bull; Highlights</h6>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#">How Do &ldquo;Expat Lite&rdquo; Programs Manage to Cut Costs?</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#mmhs">Mercer Mobilize Housing Solution&trade;</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#location">Location News</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#events">Training, Webcasts, and Conferences</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#localizer">Compensation Localizer</a></td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">▶</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;"><a style="color: #ba2c2b; text-decoration: none;" href="#gcpr">Global Compensation Planning Report</a></td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Doing Business in&hellip;<br /> <span style="color: #ba2c2b;">Germany</span></h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;"><em>Tips for understanding cultural differences while working and living abroad,  from  <a style="color: #ba2c2b;" 
href="http://www.imercer.com/content/cultural-training-passport.aspx">CulturalTrainingPassport&trade;</a>.</em></p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">1.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Germans are famous for their appreciation and practice of order, and the stereotype fits.
  Expect to follow rules and regulations that explain what is expected.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">2.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">Most Germans pride themselves on their mastery of facts and figures  and pay attention to details.
  They believe if something is worth doing, it is worth doing well.</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">&nbsp;</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top;">&nbsp;</td>
</tr>
<tr>
<td style="width: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #ef4e45;">3.</td>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 8pt; line-height: 12pt; text-align: left; vertical-align: top; color: #37424a;">There is a clear distinction between public and private.
  Friendships are often lifelong and are seen as a commitment.</td>
</tr>
</tbody>
</table>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Home-Country Data and Tax Profiles</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New spendable income, housing norm, and hypothetical income tax data is effective for these locations:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding: 0px; list-style: none; text-align: left;">
<li>Egypt</li>
<li>Finland</li>
<li> Israel</li>
<li> Kenya</li>
<li> Luxembourg</li>
<li> Panama</li>
<li> Philippines</li>
<li> Serbia*</li>
<li> Taiwan</li>
<li> Thailand</li>
<li> Venezuela</li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">*New standard home location</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Goods  and Services and Host Housing Survey Data</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">New data for the following locations is available:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Angola</li>
<li>Belize</li>
<li>Bhutan</li>
<li>Bolivia</li>
<li>Burundi</li>
<li>Cameroon</li>
<li>Czech Republic</li>
<li> Dominican Republic</li>
<li>Fiji</li>
<li>France</li>
<li>Gabon</li>
<li>Germany</li>
<li> Guatemala</li>
<li> Israel</li>
<li>Italy</li>
<li>Ivory Coast</li>
<li>Jordan</li>
<li>Nepal</li>
<li>Papua New Guinea</li>
<li> Singapore</li>
<li> Sweden</li>
<li> Tanzania</li>
<li> Tunisia</li>
<li>Turkey</li>
<li> Venezuela</li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Interim housing updates were released in August for these locations:</p>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Argentina (Buenos Aires)</li>
<li>Myanmar (Yangon)</li>
<li>United Arab Emirates (Dubai)</li>
<li> Ukraine (Kiev)</li>
<li>Venezuela (Caracas)</li>
</ul>
<p>&nbsp;</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">MercerPassport&reg;</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">The following locations  have been updated on MercerPassport&reg;:</p>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Employee Mobility Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Bahrain (Manama)</li>
<li>Egypt (Alexandria, Cairo)</li>
<li>Indonesia (Jakarta)</li>
<li>Israel (Tel Aviv)</li>
<li>Jordan (Amman)</li>
<li>Kuwait (Kuwait City)</li>
<li>Oman (Muscat)</li>
<li>Qatar (Doha)</li>
<li>Saudi Arabia (Al Khobar/Dammam, Jeddah, Riyadh) </li>
</ul>
<h6 style="font: bold 8pt Arial, Helvetica, sans-serif; color: #37424a; margin-right: 0px; margin-bottom: 6px; margin-left: 0px; text-align: left;">Start Guides</h6>
<ul style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; margin-left: 0px; padding-left: 0px; list-style: none; text-align: left;">
<li>Bahamas (Nassau)</li>
<li>Costa Rica (San Jose) </li>
</ul>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Start Guides are &lsquo;lite&rsquo; versions of the Employee Mobility  Guides; these country reports outline procedures and requirements for visiting  or relocating to up-and-coming  
locations.</p>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">For more information about MercerPassport,  visit <a style="color: #ba2c2b;" href="http://www.imercer.com/content/mercer-passport-home.aspx">imercer.com</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Get the Latest Mobility Tips</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Follow  us on Twitter <a style="color: #ba2c2b;" href="http://www.twitter.com/mercermobility">@MercerMobility</a>.</p>
<h4 style="font: bold 10pt Arial, Helvetica, sans-serif; color: #ef4e45; border-top: 1px solid #BFBFBF; padding-top: 8px; margin-top: 36px; margin-bottom: 14px; text-align: left;">Words.
  Pictures.
  Insights.</h4>
<p style="font: 8pt/12pt Arial, Helvetica, sans-serif; color: #37424a; margin-top: 0px; text-align: left;">Mercer/Think  is a weekly infographic of HR-related trends around the world.
  Sign up at <a style="color: #ba2c2b;" href="http://info.mercer.com/SubscribetoMThinkblog.html">mercerthink.mercer.com</a>.</p>
<p style="font-size: 8pt; line-height: 10pt;">&nbsp;</p>
</td>
<td width="30" valign="top">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">5266</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<div id="column_text" class="mktEditable" align="center">
<table border="0" cellpadding="0" width="600">
<tbody>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<div align="left"><span style="font-family: Arial,Helvetica,sans-serif;"><strong><span style="font-size: xx-small;">You are receiving this e-mail because our consultants have requested that this be sent to you.</span></strong> <span style="font-size: xx-small;"><br /> <br /> If you want to remove  
                  yourself from this  distribution list, <a style="color: #ba2c2b;" href="mailto:marketing.reply@mercer.com?subject=Remove from distribution list"> please inform us.</a><br /> If you no longer wish to receive any Mercer e-mails, <a style="color: #ba2c2b;" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
                 <br /> <br /> We welcome your thoughts and input.
                       If you would like to contact us with your comments, please <a style="color: #ba2c2b;" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
                       We encourage you to forward this e-mail, and to link to content on our site.
                 <br /> <br />Find the <a style="color: #ba2c2b;" href="http://www.mercer.com/about-us/locations.html">Mercer office</a> near you.</span></span> <span style="font-size: xx-small;"><br /> </span></div>
<hr style="color: #bfbfbf;" size="1" noshade="noshade" />
<div align="right"><span style="font-size: xx-small;"><span style="font-family: Arial,Helvetica,sans-serif;"><a style="color: #ba2c2b;" href="http://www.mercer.com/terms.html">Terms of use</a> | <a style="color: #ba2c2b;" href="http://www.mercer.com/privacy.html"> Privacy</a> <br /> &copy;2014  
Mercer LLC, All                    Rights   Reserved</span></span></div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</body>
</html>