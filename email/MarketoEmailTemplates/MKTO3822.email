<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<title></title>
</head>
<body ><div id="Section 1" class="mktEditable" >
<table id="table2" style="border-collapse: collapse;" border="0" cellpadding="0" width="100%" height="100%" bgcolor="#eeeeee">
<tbody>
<tr>
<td height="10">&nbsp;</td>
</tr>
<tr>
<td height="10">
<p align="center"><img src="http://info.mercer.com/rs/mercer/images/cemailLogo.png" border="0" alt="Mercer" width="601" height="63" /><br /> <img src="http://info.mercer.com/rs/mercer/images/FD-briefing-banner.gif" border="0" alt="" width="600" height="105" /></p>
</td>
</tr>
<tr>
<td height="10">
<div align="center">
<table id="table6" style="border-collapse: collapse;" border="0" cellpadding="0" width="601" bgcolor="#ffffff">
<tbody>
<tr>
<td><!--  // ---- BEFORE COPYING INTO THE SYSTEM, DELETE FROM HERE ---- // --></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
<tr>
<td align="center">
<table id="table3" style="border-collapse: collapse;" border="0" cellpadding="12" width="601" bgcolor="#ffffff">
<tbody>
<tr>
<td style="border-bottom-style: none; border-bottom-width: medium;" align="left" valign="top">
<table id="table8" style="border-collapse: collapse;" border="0" width="100%">
<tbody>
<tr>
<td valign="top">
<table id="table9" style="border-collapse: collapse;" border="0" width="100%">
<tbody>
<tr>
<td valign="top"><!--  // ---- HEADING AND INTRO ---- // -->
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #002c77;"><strong>MARCH 2014 </strong></p>
<strong> </strong>
<p style="color: #00a7c7; font-size: 14px; font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;"><strong> IN THIS EDITION</strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;"> </span></p>
<ul>
<li> <a style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #006d9e; text-decoration: none;" href="#2">Budget 2014 &ndash; Pensions are dead, long live savings?</a> </li>
<li> <a style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #006d9e; text-decoration: none;" href="#3">Investment outlook, strategic themes and opportunities</a></li>
<li> <a style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #006d9e; text-decoration: none;" href="#4">Mercer launches Global Pension Buyout Index</a></li>
<li> <a style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #006d9e; text-decoration: none;" href="#1">Vienna tops Mercer's Quality of Living rankings</a> </li>
</ul>
</td>
<td width="10">&nbsp;</td>
<td style="border-left: 1px solid #ffffff;" width="10">&nbsp;</td>
<td width="220" valign="top">
<div id="content2" class="mktEditable">
<table id="table10" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;"><span style="color: #00a7c7; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-transform: uppercase;"> <br /><br /><br />Quarterly UK briefing on pension accounting</span><br /> <span 
style="color: #333333;">This report briefs finance teams on trends in pension accounting, including investment performance, FRS 102, IAS 19 and other updates.
 </span> <br /><br /> <strong> <a href="http://uk.mercer.com/articles/UK-pension-accounting-briefing"> <span style="text-decoration: none; color: #006d9e;">&gt; Download the latest  edition</span></a></strong></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="border-bottom-style: none; border-bottom-width: medium;" valign="top">
<table id="table12" style="border-bottom-style: solid; border-bottom-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="table12" style="border-bottom-style: solid; border-bottom-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td height="20">&nbsp;</td>
</tr>
<tr>
<!--  // ---- SECTION 2 ---- // -->
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;" valign="top"><span style="font-size: 16px; color: #00a7c7; text-transform: uppercase;"> <a name="2">BUDGET 2014 &ndash; PENSIONS ARE DEAD, LONG LIVE SAVINGS?</a></span><br /> The proposals in the 
Chancellor&rsquo;s 2014 Budget represent the most radical shake up of pensions ever announced in the UK.
  The implications are potentially wide reaching for both DB and DC pension arrangements, but also potentially the stock market, the price of housing, the demand for Government gilts and the economy in general.
   <br /><br /><a style="color: #006d9e; font-size: 12px; text-decoration: none;" href="http://uk.mercer.com/articles/budget2014-pensions-are-dead-long-live-savings"><strong>&gt; Read Mercer&rsquo;s summary and commentary</strong></a></td>
</tr>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table>
<table id="table13" style="border-bottom-style: solid; border-bottom-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td height="20">&nbsp;</td>
</tr>
<tr>
<!--  // ---- SECTION 3 ---- // -->
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; height: 123px;" valign="top"><span style="font-size: 16px; color: #00a7c7; text-transform: uppercase;"> <a name="3">INVESTMENT OUTLOOK, STRATEGIC THEMES AND OPPORTUNITIES</a></span><br /> As major global 
investment markets come off a strong 2013, Mercer has released two related research papers that together assess the investment environment and suggest market opportunities  &ndash; &ldquo;Outlook for 2014 and Beyond&rdquo; and &ldquo;2014 Strategic Themes and Opportunities&rdquo;.
 Together they identify how investors can respond dynamically to changing market conditions and ensure that   portfolios are robust to a range of economic outcomes.<br /> <br /><a style="color: #006d9e; text-decoration: none;" 
href="http://uk.mercer.com/articles/Strategic-Themes-Opportunities"><strong>&gt; Find out more</strong></a></td>
</tr>
<tr>
<td style="height: 20px;"></td>
</tr>
</tbody>
</table>
<table id="table11" style="border-bottom-style: solid; border-bottom-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;" valign="top"><span style="font-size: 16px; color: #00a7c7; text-transform: uppercase;"> <a name="4">MERCER LAUNCHES GLOBAL PENSION BUYOUT INDEX</a></span><br /> The Mercer Global Pension Buyout Index uses 
up-to-date pricing information from the US, the UK, Ireland and Canada to estimate the cost of insuring a sample plan&rsquo;s current  retirees as a percentage of the equivalent estimated accounting liability in each country.<br /> <br /><strong><a style="color: #006d9e; text-decoration: none;" 
href="http://uk.mercer.com/articles/1581750">&gt; Find out more</a></strong></td>
</tr>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;" valign="top">
<table id="table14" style="border-bottom-style: solid; border-bottom-width: 1px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td height="20">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td height="20">&nbsp;</td>
</tr>
<tr>
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;" valign="top">&nbsp;</td>
</tr>
<tr>
<!--  // ---- SECTION 1 ---- // -->
<td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #333333;" valign="top"><span style="font-size: 16px; color: #00a7c7; text-transform: uppercase;"> <a name="1">VIENNA TOPS MERCER'S QUALITY OF LIVING RANKINGS</a></span><br /> Mercer has ranked 223 cities based on data 
from its Quality of Living Survey, which helps multinational organizations compensate  their expatriate employees and aids governments in benchmarking their cities' attractiveness for expatriates.<br /> <br /><a style="color: #006d9e; text-decoration: none;" 
href="http://uk.mercer.com/qualityofliving"><strong>&gt; Find out more</strong></a></td>
</tr>
<tr>
<td height="20">&nbsp;</td>
&nbsp;
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!--  // ---- MAIN HEADING ---- // --></td>
</tr>
<tr>
<td style="border-bottom-style: none; border-bottom-width: medium;" valign="top">
<table id="table5" style="border-collapse: collapse;" border="0" cellpadding="0" width="100%" bgcolor="#ffffff">
<tbody>
<tr>
<td style="font-family: Verdana,Arial,sans-serif; font-size: 10px; line-height: 12px; color: #5c6771;">&nbsp;</td>
</tr>
<tr>
<td style="font-family: Verdana,Arial,sans-serif; font-size: 10px; line-height: 12px; color: #5c6771;">
<p align="right"><img src="http://info.mercer.com/rs/mercer/images/parent_logo.gif" border="0" alt="" width="199" height="27" /></p>
</td>
</tr>
<tr>
<td style="font-family: Verdana,Arial,sans-serif; font-size: 10px; line-height: 12px; color: #5c6771;">&nbsp;</td>
</tr>
<tr>
<!--  // ---- FOOTER - DO NOT EDIT ---- // -->
<td style="font-family: Verdana,Arial,sans-serif; font-size: 10px; line-height: 12px; color: #5c6771;">
<div align="left"><span style="font-size: xx-small;">Issued in the United Kingdom by Mercer Limited which is authorised and regulated by the Financial </span><span style="font-size: xx-small;"> Conduct </span> <span style="font-size: xx-small;">Authority.
  Registered in England No.
  984275.
  Registered Office: 1 Tower Place West, Tower Place, London, EC3R 5BU <br /><br />You are receiving this email because our consultants have requested that this be sent to you.
 <br /> <br /> If you want to remove        yourself from this  distribution list,  	<a class="blue" href="mailto:ukmarketing@mercer.com?subject=Remove from distribution list">please inform us.</a><br /> If you no longer wish to receive any Mercer emails, <a class="blue" 
href="http://info.mercer.com/UnsubscribePage.html">unsubscribe     here</a>.
      <br /> <br /> We welcome your thoughts and input.
            If you would like to contact us with your comments, please <a class="blue" href="mailto:marketing.reply@mercer.com?subject=Feedback on Mercer e-mail"> email your feedback</a>.
            We encourage you to forward this email, and to link to content on our site.
      <br /> <br /> <img src="http://image.exct.net/lib/fef91d70766d05/m/1/icon_more10x10bluewhite.gif" border="0" alt="More" width="10" height="10" />&nbsp;Find the <a class="blue" href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</span> <span style="font-size: 
xx-small;"><br /> </span></div>
<hr size="1" />
<div align="right"><span style="font-family: Arial,Helvetica,sans-serif; font-size: xx-small;"><a class="blue" href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a class="blue" href="http://www.mercer.com/privacy.jhtml"> Privacy</a> <br /> &copy;2014 Mercer LLC, All          Rights  
 Reserve</span></div>
</td>
</tr>
<tr>
<td style="font-family: Verdana,Arial,sans-serif; font-size: 10px; line-height: 12px; color: #5c6771;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center"></td>
</tr>
</tbody>
</table>
</div>
</body>
</html>