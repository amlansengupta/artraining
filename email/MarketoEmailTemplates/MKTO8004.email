<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
img {
        border:none;    
}
body {
        background-color: #eeeeee;
}
a:link {
        color: #00a8c8;
        text-decoration: underline;
}
a:visited {
        color: #00a8c8;
        text-decoration: underline;
}
a:hover {
        color: #00a8c8;
}
a:active {
        color: #00a8c8;
        text-decoration: underline;
}
</style>
<title></title>
</head>
<body ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td valign="top" ><div class="mktEditable" id="header" ><img alt="Mercer" src="https://info.mercer.com/rs/mercer/images/Investments_generic_ext_600_169.jpg " width="600" height="169"></div>
</td>
</tr>
<tr ><td ><div class="mktEditable" id="banner" ></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr ><td width="32">&nbsp;</td>
<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><div class="mktEditable" id="content" ><p><span style="font-size: 14px;">At Mercer, we believe that an investment view that goes beyond traditional financial analysis and considers a wide range of risks and opportunities &ndash; including sustainability factors such as good governance, and environmental and social impacts &ndash; is          more likely to create and  preserve long-term investment capital.           Increasing awareness of the growing and aging population, natural resource constraints, and a shifting public sentiment and regulatory landscape on many environmental and social issues present risks and opportunities to investors. </span></p>
<table border="0">
<tbody>
<tr>
<td>
<p><span style="color: #00a8c8; font-family: arial,helvetica,sans-serif; font-size: 16px;">An Investment Framwork for Sustainable Growth</span><br />&nbsp;<br /><span style="font-family: arial,helvetica,sans-serif; font-size: 14px;"> In Mercer&rsquo;s Investment Framework for  Sustainable Growth,       we distinguish between the financial implications (risks) associated with environmental, social, and corporate governance (ESG) factors, and the growth opportunities in industries most&nbsp;directly affected by sustainability issues.&nbsp;Mitigating emerging risks  requires    flexibility,    foresight, and fresh thinking about risk management.&nbsp;At the same time, investors should adapt their strategies to capitalize on new opportunities.</span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 14px;"><a href="http://www.mercer.com/insights/point/2014/an-investment-framework-for-sustainable-growth.html" target="_blank">Find out more </a></span></p>
</td>
<td>
<p><img src="https://info.mercer.com/rs/mercer/images/esg_framework.gif" alt="framework" width="175" height="185" align="top" /></p>
</td>
</tr>
</tbody>
</table>
<p>________________________________________________________________________________</p>
<table border="0">
<tbody>
<tr>
<td>
<p><span style="color: #00a8c8; font-family: arial,helvetica,sans-serif; font-size: 16px;">The Pursuit of&nbsp;Sustainable Returns <span style="font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: PMingLiU; mso-fareast-theme-font: minor-fareast; mso-ansi-language: EN-US; mso-fareast-language: ZH-TW; mso-bidi-language: AR-SA;">&ndash;</span>Sustainability by Asset Class</span><br />&nbsp;<br /><span style="font-family: arial,helvetica,sans-serif; font-size: 14px;"> Mercer&rsquo;s portfolio reference guide, &lsquo;The Pursuit of Sustainable  Returns: Integrating Environmental, Social, and Corporate&nbsp;&nbsp;&nbsp;&nbsp; Governance Factors and&nbsp;Sustainability by Asset Class&rsquo; outlines the drivers&nbsp;for addressing sustainable growth trends at a portfolio level for each major asset class.</span></p>
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 14px;"><a href="http://www.mercer.com/insights/point/2014/the-pursuit-of-sustainable-returns.html" target="_blank">Find out more </a></span></p>
</td>
<td>
<p><img src="https://info.mercer.com/rs/mercer/images/esg_sustainable_returns.gif " alt="Sustainable Returns" width="175" height="185" /></p>
</td>
</tr>
</tbody>
</table>
<p>________________________________________________________________________________</p>
<p><span style="font-size: 14px;">If you are interested to take the first step and establish your investment/ ESG beliefs, please contact <strong>Beatrice Yu</strong><strong> at +852 3476 3827 or email to <a href="mailto:beatrice.yu@mercer.com">beatrice.yu@mercer.com</a></strong> to help you identify areas that matter to you through Mercer workshop &ldquo;Sensitive Investment Topics Analyzer&rdquo; (SITA).  Once that foundation is laid, we help you implement them through: </span></p>
<ul>
<li><span style="font-size: 14px;">Design your ESG policy. </span></li>
<li><span style="font-size: 14px;">Provide ESG ratings for active and passive strategies.</span></li>
<li><span style="font-size: 14px;">Select highly rated thematic managers.</span></li>
<li><span style="font-size: 14px;">Provide access to portfolio level ESG analysis (including carbon ratings).</span></li>
<li><span style="font-size: 14px;">Advice on divestment/engagement.</span></li>
<li><span style="font-size: 14px;">Discuss external reporting to stakeholders. </span></li>
</ul>
<p><span style="font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px;"><strong>Deborah Bannon</strong><br />Investments Business Leader<br />North Asia Ex-Japan</span></p></div>
</td>
<td width="32">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="mktEditable" id="footer" ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:center;">
<tr>
<td valign="top"><img src="http://info.mercer.com/rs/mercer/images/footer.png" width="568" height="45" alt="Marsh &amp; McLennan Companies" border="0" style="display:block"></td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" >
<table style="table-layout: fixed; background-color: #eeeeee;" border="0" cellspacing="0" cellpadding="0" width="601" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong> You are receiving this e-mail because you have registered at  			mercer.com and opted in to receive our communications or because our  			consultants 
                     have requested that this be sent to you.</strong><br /> <br /> If you want to remove yourself from this distribution list, <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Remove from distribution list - Asia"> please inform 
us</a>.<br /> If you no longer wish to receive any Mercer          e-mails,    <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html"> unsubscribe here</a>.
                      <br /> <br /> We welcome your thoughts and input.
                      If you would like to contact us  			with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="mailto:mercerasia@mercer.com?subject=Feedback on Mercer e-mail - Asia"> e-mail your feedback</a>.
                      We encourage you to forward this e-mail,  			and to link to content on our site.<br /> <br /> <img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10" />&nbsp;Find  			the <a style="color: #00a7c7; text-decoration: 
none;" href="http://www.mercer.com/about-us/locations.html">Mercer  			office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1" />
</td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/terms.html"> Terms of use</a> | <a style="color: #00a7c7; text-decoration: 
none;" href="http://www.mercer.com/privacy.html"> Privacy</a><br /> &copy;2014 Mercer LLC.
                      All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1" />
</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"> This email was sent to: {{lead.Email Address:Default=edit me}}<br /> This email was sent by: <br /> Mercer (Global Headquarters) <br /> 1166 Avenue of the Americas New York New York 10036     
                 USA<br /> <br /> Mercer (Hong Kong) Limited 美世(香港)有限公司<br /> 26/F and 27/F, Central Plaza, 18 Harbour Road, Wanchai, Hong Kong</span></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>