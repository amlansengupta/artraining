<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
</head>
<body ><div id="Section 1" class="mktEditable" ><table width="600" align="center">
<tr>
<td height="24" align="left"><a href="{{system.forwardToFriendLink:default=Forward%20to%20a%20colleague}}"><font color="#777777" size="1" face="Verdana,Arial, sans-serif"><font color="#00A8C8">Forward to a friend</font></font></a></td>
<td align="right"><font color="#777777" size="1" face="verdana">follow us:</font> <a class="SocialIconsLink" href="https://www.facebook.com/MercerInsights" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/facebookemail.gif"></a><a class="SocialIconsLink" href="https://twitter.com/MercerInsights" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/twitteremail.gif"></a><a class="SocialIconsLink" href="http://www.imercer.com/content/mercer-rss-feeds.aspx" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/rssemail.gif"></a><a class="SocialIconsLink" href="http://www.youtube.com/user/mercervideo" target="_blank"><img border="0" src="http://www.imercer.com/content/common/Emails/images/youtubeemail.gif"></a></td>
</tr>
</table>
</div>
<div id="Section 1" class="mktEditable" align="center" ><table width="602" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF; border:#BFBFBF; border-style:solid; border-width:1px;">
<tr>
<td>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="http://www.imercer.com/content/GM/email/aia-report-email-header-b.jpg" alt="Mercer Global Mobility" width="600" height="169" border="0"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td width="30">
<table width="600" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="30" height="670" valign="top">
<p>&nbsp;</p>
</td>
<td width="340" align="center" valign="top">
<h1 style="font: 14pt/18pt Arial, Helvetica, sans-serif; text-align: left; color: #6f83c1;">Latest Alternative International Assignment Research Released</h1>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">We are pleased to announce the release of the 2013 Alternative International Assignments Policies and Practices (AIA) Survey Global Report. The findings from the AIA Survey have further enhanced Mercer’s database of international assignment policies and practices with in-depth information on:</p>
<ul style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">
<li>Local Plus</li>
<li>Developmental/training</li>
<li>Short-term</li>
<li>Commuter</li>
<li>Intra-regional</li>
<li>Global nomad</li>
</ul>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">Use this data to help optimise your mobility program. <a href="mailto:mobility@mercer.com?subject=Policy%20Benchmarking" style="color:#595997;">Order your custom policy benchmarking review today</a>.</p>
<h2 style="font: 12pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #6f83c1; margin-bottom:0;">You Can Still Take Part in the AIA Survey<br>
<br></h2>
<table border="0" align="left" cellpadding="0" cellspacing="0">
<tr>
<td valign="top"><img src="http://www.imercer.com/content/GM/email/gm-aia-report-cover-2013.jpg" width="100" height="141" alt="AIA Global Report"></td>
<td width="20" height="160">&nbsp;</td>
<td valign="top">
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a; margin-top:0px;">The AIA Survey Global Report is available to participants only, at a special price of USD 500 / EUR 370. But it's not too late!</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">The AIA Survey is evergreen. You can still participate and receive a complimentary regional report, with the option to purchase the Global Report. Visit <a href="http://www.imercer.com/aia" style="color:#595997;">imercer.com/AIA</a> to start the survey now or contact us for details.</p>
</td>
</tr>
</table>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #37424a;">&nbsp;</p>
</td>
<td width="15" valign="top" style="border-right: solid 1px #bfbfbf;">&nbsp;</td>
<td width="15" valign="top">&nbsp;</td>
<td width="175" valign="top">
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #6f83c1; margin-bottom:0;">Highlights from<br>
Our Research</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;">61% of AIA Survey respondents have <strong>more than one</strong> alternative international assignment policy.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;">Nearly two-thirds have international employees on a <strong>Local Plus approach</strong>, with 42% having a <strong>formal</strong> <strong>Local Plus policy</strong>.</p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;">For all assignment types, prevalent challenges are <strong>managing exceptions</strong> and <strong>cost containment</strong>.</p>
<h3 style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #6f83c1; margin-bottom:0;">Are You Considering Alternative Expat Compensation Approaches?</h3>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; text-align: left; color: #808080;">If your company is exploring more cost-effective approaches to global staffing challenges, Mercer's experts can help with policy development, programme design, and employee communication.<br>
<a href="mailto:mobility@mercer.com?subject=Alternative%20Assignment%20Polices%20and%20Practices" style="color:#595997;">Contact us for more information</a>.</p>
</td>
<td width="15" valign="top">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:30px; font-family:Arial, Helvetica, sans-serif; font-size: 8pt; color:#808080; text-align:left;"></td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" width="216" height="52" alt="Marsh &amp; McClennan Companies"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<div class="mktEditable" ><table width="600" align="center">
<tr>
<td>
<hr size="1" noshade="noshade" color="#C4CAE6">
<font style="font-family:Arial, Helvetica, sans-serif; font-size:10px">For details, contact your local Mercer representative, or contact us at:<br>
<br>
<a href="mailto:hrsolutions.ap@mercer.com">Asia</a>: +65 6398 2926<br>
<a href="mailto:customerservice@mercer.com">Australia/NZ</a>: 1800 645 186 (Sydney) +61 2 8864 6800 (International) / 0508 645 186 (Auckland) +64 9 984 3500 (International)<br>
<a href="mailto:info.services@mercer.com">Canada</a>: 1-800-333-3070<br>
<a href="http://www.imercer.com/CC/tabs/home.aspx">China</a> +86 10 6533 4300<br>
<a href="mailto:indirect.sales.emea@mercer.com">Europe</a>: 48 22 436 6868<br>
<a href="mailto:client.services.la@mercer.com">Latin America</a>: 54 11 4000 4081 (Argentina) 52 55 5999 4524 (Mexico)<br>
<a href="mailto:surveys@mercer.com">United States</a>: 1-800-333-3070 or 1-502-560-8290</font></td>
</tr>
</table>
</div>
<div class="mktEditable" id="Dynamic_Content_3" ><table width="600" align="center">
<tr>
<td>
<hr size="1">
<div align="left"><font style="font-family:Arial, Helvetica, sans-serif; font-size:10px">You are receiving this e-mail because our consultants have requested that this be sent to you.<br>
<br>
If you want to remove yourself from this distribution list, <a href="mailto:client.services.europe@mercer.com?subject=Remove%20from%20distribution%20list">please inform us.</a><br>
If you no longer wish to receive any Mercer e-mails, <a href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a href="mailto:client.services.europe@mercer.com?subject=Feedback%20on%20Mercer%20e-mail">email your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://image.exct.net/lib/fef91d70766d05/m/1/icon_more10x10bluewhite.gif" border="0" alt="More" width="10" height="10">&nbsp;Find the <a href="http://www.mercer.com/aboutmercerlocation.jhtml">Mercer office</a> near you.</font></div>
<hr size="1">
<div align="right"><font style="font-family:Arial, Helvetica, sans-serif; font-size:10px"><a href="http://www.mercer.com/termsofuse.jhtml">Terms of use</a> | <a href="http://www.mercer.com/privacy.jhtml">Privacy</a><br>
&copy;2013 Mercer LLC, All Rights Reserved</font></div>
</td>
</tr>
</table>
</div>
</body>
</html>