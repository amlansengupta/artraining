<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Role Analysis and Job Evaluation (IPE) Workshop</title>
</head>
<body ><div ><div class="mktEditable" ><div align="center">
<table style="background-color: #fff; border: #BFBFBF; border-style: solid; border-width: 1px; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="715">
<tbody>
<tr>
<td>
<table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td><img src="http://info.mercer.com/rs/mercer/images/ROLE-ANALYSIS-AND-JOB-EVALUATION-(IPE)-Banner.jpg" border="0" alt="Role Analysis and Job Evaluation (IPE) Workshop" width="713" height="267"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding-left: 32px; padding-right: 10px; border-right: solid 1px #e6eaef;" width="420" align="left" valign="top">
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; text-align: left; margin-top: 0px;">Dear Client,</p>
<p style="font: 11pt/14pt Arial, Helvetica, sans-serif, bold; color: #002c77; text-align: center; margin-top: 20px; margin-bottom: 20px;"><strong>Do you often need to compare job positions and<br>
benchmark compensation across regions?</strong></p>
<p style="font: 10pt/14pt Arial, Helvetica, sans-serif; color: #37424a; text-align: left; margin-top: 0px;">If so, our <strong>Role Analysis and Job Evaluation (IPE) workshop</strong> in November will be useful for you and your colleagues.<br>
<br>
At our 2-day session, learn to properly administer and implement position evaluations across job families, within any type of organization, anywhere in the world.</p>
<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 12pt; font-weight: 400; margin-top: 30px; text-align: left; color: #002c77; text-transform: uppercase;"><strong>More about the IPE workshop:</strong></h2>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002C77"><strong style="color: #ffffff;">Workshop details</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB"><strong>Date:</strong> 11 - 12 Nov 2014 (2 days)</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB"><strong>Time:</strong> 9:00 am to 5:00 pm<br>
(registration at 8:30 am)</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB"><strong>Venue:</strong>The Aloft Hotel, KL Sentral</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px;" align="left" valign="top" bgcolor="#FBFBFB">Refreshments at lunch and tea breaks will be provided</td>
</tr>
</tbody>
</table>
<p style="margin-top: 0px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002C77"><strong style="color: #ffffff;">The workshop will cover:</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB">What job evaluation is</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB">Introduction to the job framework and methods used for job evaluation</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB">How job evaluation results can be used to make strategic HR decision, and how this impacts pay outcomes</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB">Mercer's International Position Evaluation (IPE&trade;) methodology</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="6%" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" width="94%" align="left" valign="top" bgcolor="#FBFBFB">Why and how to conduct role analysis</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="padding-top: 8px;" align="left" valign="top" bgcolor="#FBFBFB">Alternative approaches to documenting roles</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" align="left" valign="top" bgcolor="#FBFBFB">&bull;</td>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px;" align="left" valign="top" bgcolor="#FBFBFB">Introduction to creating job descriptions</td>
</tr>
</tbody>
</table>
<p style="margin-top: 0px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="85%" align="center">
<tbody>
<tr>
<td style="padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#002C77"><strong style="color: #ffffff;">Registration is simple</strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#FBFBFB">Complete the order form in the <a style="color: #006d9e;" href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf">brochure</a> and</td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" width="11%" align="right" valign="top" bgcolor="#FBFBFB"><strong>1.</strong></td>
<td style="padding-top: 8px;" width="89%" align="left" valign="top" bgcolor="#FBFBFB"><strong>Scan and email to <a style="color: #006d9e;" href="mailto:juliana.philip@mercer.com">juliana.philip@mercer.com</a></strong></td>
</tr>
<tr>
<td style="padding-top: 8px; padding-left: 10px;" align="right" valign="top" bgcolor="#FBFBFB"><strong>2.</strong></td>
<td style="padding-top: 8px;" width="89%" align="left" valign="top" bgcolor="#FBFBFB"><strong>Or, mail the form to<br></strong> B-9-1, Level 9, Tower B, Menara UOA Bangsar, No.5, Jalan Bangsar Utama 1 59000 Kuala Lumpur</td>
</tr>
<tr>
<td style="border-bottom: solid 1px #002c77; padding-bottom: 8px; padding-left: 10px;" colspan="2" align="left" valign="top" bgcolor="#FBFBFB">Our consultants will be in touch to confirm details of your registration.</td>
</tr>
</tbody>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; text-align: left; color: #37424a; margin-left: 5px; margin-top: 40px;">For any queries on the workshop, please contact <a style="color: #006d9e;" href="mailto:juliana.philip@mercer.com">Juliana Philip</a>.<br>
<br>
Thank you.</p>
</td>
<td style="padding-left: 10px; padding-right: 22px;" width="222" align="left" valign="top">
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#E6EAEF"><strong style="color: #002c77; text-transform: uppercase;">Who should attend</strong></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/12pt Arial, Helvetica, sans-serif, bold; color: #37424a; text-align: left; margin-top: 20px; margin-bottom: 20px;">All HR leaders and practitioners:</p>
<table style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #37424a;" border="0" cellspacing="0" cellpadding="4" width="100%">
<tbody>
<tr>
<td style="font-size: 18pt; padding-bottom: 10px;" width="9%"><strong>&middot;</strong></td>
<td style="padding-bottom: 10px;" width="91%">Who would like to learn more about role analysis and job evaluation</td>
</tr>
<tr>
<td style="font-size: 18pt;"><strong>&middot;</strong></td>
<td>Whose organizations are undergoing role analysis / job evaluation exercises</td>
</tr>
</tbody>
</table>
<p style="margin: 0; margin-bottom: 20px;">&nbsp;</p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#E6EAEF"><strong style="color: #002c77; text-transform: uppercase;">Download the IPE Brochure<br>
and order form</strong></td>
</tr>
</tbody>
</table>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 14pt; margin-top: 30px; text-align: center; color: #37424a; margin-bottom: 40px;"><a href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf"><img src="http://info.mercer.com/rs/mercer/images/2014-Role-Analysis-and-Job-Evaluation_cover.jpg" border="0" alt="2014 IPE Flyer Cover" width="86" height="122"></a></p>
<table style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 12pt; text-align: left; color: #37424a; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="8" width="100%">
<tbody>
<tr>
<td style="padding-left: 2px; right: 2px;" align="center" valign="top" bgcolor="#E6EAEF"><strong style="color: #002c77; text-transform: uppercase;">Workshop fees for 2 days</strong></td>
</tr>
</tbody>
</table>
<p style="font: 10pt/12pt Arial, Helvetica, sans-serif, bold; color: #37424a; text-align: left; margin-top: 20px; margin-bottom: 40px;"><strong>Public participant:</strong><br>
USD 1,300<br>
<br>
<strong>Mercer member &amp; client:</strong><br>
USD 1,100<br>
<br>
<strong>Sign up for 3 or more persons at only USD 900 per participant!</strong><br>
<br>
<em>*Prices exclude 6% GST</em></p>
<table border="0" cellspacing="0" cellpadding="8" width="60%" align="center">
<tbody>
<tr>
<td style="padding: 5pt; alignment-adjust: middle;" align="center" valign="middle" bgcolor="#E36C0A"><span style="color: #ffffff;"><span style="font-size: 13px; text-transform: uppercase; font-family: Arial, Helvetica, sans-serif;"><strong><a style="color: #ffffff;" href="http://info.mercer.com/rs/mercer/images/Role-Analysis-and-Job-Evaluation_Flyer.pdf">Register</a> now!<br>
<span style="text-transform: none;">Seats are limited</span></strong></span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="right">&nbsp;</td>
</tr>
<tr>
<td align="right">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td style="padding: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #808080; text-align: left;">&nbsp;</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" alt="Marsh &amp; McClennan Companies" width="216" height="52"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="mktEditable" id="footer2" ><table style="table-layout: fixed;" border="0" cellspacing="0" cellpadding="0" width="713" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong>You are receiving this e-mail because you have registered at imercer.com and opted in to receive our communications or because our consultants have requested that this be sent to you.</strong> To view your registration details, <a style="color: #00a7c7; text-decoration: none;" href="https://winsts.mercer.com/iMercerSTS/Login.aspx?ReturnUrl=/iMercerSTS/Default.aspx?wa=wsignin1.0&amp;wtrealm=https%3a%2f%2fwww.imercer.com%2fSTSResponse.aspx&amp;languageId=6&amp;languageId=6">log in here</a>, or if you were forwarded this e-mail and would like to subscribe, please <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=regmember">register at imercer.com</a>. Registration is free, provides you with access to premium content, and allows you to tailor the site to your own interests.<br>
<br>
If you no longer wish to receive any Mercer e-mails, <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="https://imercer.az1.qualtrics.com/SE/?SID=SV_1BxbQDMkVU13Rbu">e-mail your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10">&nbsp;Find the <a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/aboutmercerlocation.htm">Mercer office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=term">Terms of use</a> | <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=privacy">Privacy</a><br>
&copy;2014 Mercer LLC. All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;">This email was sent to: {{lead.Email Address:Default=edit me}}<br>
This email was sent by:<br>
Mercer (Global Headquarters)<br>
1166 Avenue of the Americas New York New York 10036 USA<br>
<br>
Mercer (Singapore) Pte Ltd<br>
8 Marina View #09-08, Asia Square Tower 1, Singapore 018960<br>
Company Registration No. 197802499E</span></td>
</tr>
</tbody>
</table>
</div>
</body>
</html>