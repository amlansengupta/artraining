<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Calculate the Right Host-based Compensation Package with Ease</title>
</head>
<body ><div align="center">
<table width="602" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFF; border:#BFBFBF; border-style:solid; border-width:1px;">
<tr>
<td>
<table width="600" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><a href="http://www.imercer.com/content/global-mobility-overview.aspx"><img src="http://www.imercer.com/content/GM/email/gm-general.jpg" alt="Mercer Global Mobility" width="600" height="169" border="0"></a><a href="http://www.imercer.com/content/gm-webcasts-recordings.aspx"></a></td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="30">
<tr>
<td>
<h1 style="font-family: Arial, Helvetica, sans-serif; font-size: 16pt;font-weight: 400; color: #002c77; margin-top:0; margin-bottom: 14px; text-align: left;">Host-Based Compensation Tools</h1>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;">Are you planning to localize a long-term expatriate? Aiming to determine the right local plus package? Moving an employee in a permanent transfer across the globe, or just regionally?</p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;">When placing a globally mobile employee into a host compensation structure, you juggle a lot of numbers. Mercer is here to help you find the right ones with our host-based compensation tools.</p>
<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 14pt;font-weight: 400; color: #00a8c8; margin-top:0; margin-bottom: 14px; text-align: left;">Net-to-Net and Net-to-Gross Compensation Analyses</h2>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;">Address the factors necessary for a move to host-based compensation in one quick calculation:</p>
<ul style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; margin-bottom: 14pt; text-align: left; color:#37424a;">
<li style="margin-bottom:6pt;">Income taxes.</li>
<li style="margin-bottom:6pt;">Social security contributions.</li>
<li style="margin-bottom:6pt;">Costs of goods and services.</li>
<li>Housing expenses.</li>
</ul>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;"><a href="mailto:mobility@mercer?subject=Host-based%20Packages" style="color:#00a8c8;">Contact us</a> now to get started</p>
<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 14pt;font-weight: 400; color: #00a8c8; margin-top:0; margin-bottom: 14px; text-align: left;">Local Plus Services</h2>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;">Whether you are only starting to use Local Plus packages, are aiming to create a standard policy, or want to measure your approach against that of your peers, Mercer can guide you to use Local Plus more effectively and provide the data to set appropriate packages. Our extensive database, based on our <em>Alternative International Assignments Policies and Practices Survey</em>, and our expertise in aiding clients with this approach, will allow us to help develop, benchmark, and apply Local Plus policies.</p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;">Get in touch to discuss your Local Plus needs today.</p>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; line-height: 14pt; margin-top: 0px; text-align: left; color:#37424a;"><a href="mailto:mobility@mercer?subject=Host-based%20Packages" style="border:solid 1px #00a8c8; padding:4px; color:#00a8c8; text-decoration:none; text-transform:uppercase;">Contact Us</a></p>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding:30px; font-family:Arial, Helvetica, sans-serif; font-size: 10pt; color:#999; text-align:left;">&nbsp;</td>
<td width="216"><img src="http://www.imercer.com/content/GM/email/mmc-endorsement.gif" width="216" height="52" alt="Marsh & McClennan Companies"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<div class="mktEditable" id="footer2" ><table style="table-layout: fixed;" border="0" cellspacing="0" cellpadding="0" width="601" align="center">
<tbody>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><strong>You are receiving this e-mail because you have registered at imercer.com and opted in to receive our communications or because our consultants have requested that this be sent to you.</strong> To view your registration details, <a style="color: #00a7c7; text-decoration: none;" href="https://winsts.mercer.com/iMercerSTS/Login.aspx?ReturnUrl=/iMercerSTS/Default.aspx?wa=wsignin1.0&amp;wtrealm=https%3a%2f%2fwww.imercer.com%2fSTSResponse.aspx&amp;languageId=6&amp;languageId=6">log in here</a>, or if you were forwarded this e-mail and would like to subscribe, please <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=regmember">register at imercer.com</a>. Registration is free, provides you with access to premium content, and allows you to tailor the site to your own interests.<br>
<br>
If you no longer wish to receive any Mercer e-mails, <a style="color: #00a7c7; text-decoration: none;" href="http://info.mercer.com/UnsubscribePage.html">unsubscribe here</a>.<br>
<br>
We welcome your thoughts and input. If you would like to contact us with your comments, please <a style="color: #00a7c7; text-decoration: none;" href="https://imercer.az1.qualtrics.com/SE/?SID=SV_1BxbQDMkVU13Rbu">e-mail your feedback</a>. We encourage you to forward this e-mail, and to link to content on our site.<br>
<br>
<img src="http://info.mercer.com/rs/mercer/images/icon_more10x10bluewhite.gif" alt="image" width="10" height="10">&nbsp;Find the <a style="color: #00a7c7; text-decoration: none;" href="http://www.mercer.com/aboutmercerlocation.htm">Mercer office</a> near you.</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="right" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;"><a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=term">Terms of use</a> | <a style="color: #00a7c7; text-decoration: none;" href="http://www.imercer.com/default.aspx?page=privacy">Privacy</a><br>
&copy;2014 Mercer LLC. All Rights Reserved</span></td>
</tr>
<tr>
<td height="15" valign="middle">
<hr size="1"></td>
</tr>
<tr>
<td align="left" valign="top"><span style="font-size: 11px; font-style: normal; color: #808080; font-family: Arial, Helvetica, sans-serif;">This email was sent to: {{lead.Email Address:Default=edit me}}<br>
This email was sent by:<br>
Mercer (Global Headquarters)<br>
1166 Avenue of the Americas New York New York 10036 USA<br>
<br>
Mercer Consulting (China) Limited<br>
31&amp;36F, Hong Kong New World Tower, 300 Huai Hai Zhong Road, Shanghai 200021</span></td>
</tr>
</tbody>
</table>
</div>
</body>
</html>