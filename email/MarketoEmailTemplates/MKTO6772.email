<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
img {
        border:none;    
}
body {
        background-color: #eeeeee;
}
a:link {
        color: #00a8c8;
        text-decoration: underline;
}
a:visited {
        color: #00a8c8;
        text-decoration: underline;
}
a:hover {
        color: #00a8c8;
}
a:active {
        color: #00a8c8;
        text-decoration: underline;
}
</style>
<title></title>
</head>
<body bgcolor="#EEEEEE" ><table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#eeeeee; " ><tr ><td ><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#ffffff; margin-top:10px; margin-bottom:10px; " ><tr ><td ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td valign="top" ><div class="mktEditable" id="header" ></div>
</td>
</tr>
<tr ><td valign="top" ><div class="mktEditable" id="banner" ><a name="top"><img title="Mercer Super Trust Portfolio Management Snapshot" src="http://info.mercer.com/rs/mercer/images/Portfolio%20Management%20Snapshot%20Header.png" alt="Mercer Super Trust Portfolio Management Snapshot" height="292" width="702"></a></div>
</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; " ><tr ><td valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr ><td width="32" align="left" valign="top">&nbsp;</td>
<td ><table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size:12px; font-family:arial,helvetica,sans-serif; " ><tr>
<td colspan="3" height="20">&nbsp;</td>
</tr>
<tr ><td width="48%" align="left" valign="top" ><div class="mktEditable" id="content1" ><p><span style="color: #00a8c8; font-family: 'Arial','sans-serif';">This update from the Mercer Investments Portfolio Management team looks back at developments over the month and quarter to 30 June 2014, as well as the financial year 2013-14.</span></p>
<p><strong><span style="color: #006d9e; font-size: 10.5pt; font-family: 'Arial','sans-serif';">IN THE 2013-14 FINANCIAL YEAR&hellip;&hellip;</span></strong></p>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer      Growth* returned 14.3% in the financial year to 30 June 2014,      outperforming both its composite benchmark by 0.8% and CPI+3% objective by      8.3%.&nbsp; </span></li>
</ul>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">In      peer relative terms, Mercer Growth had a good financial year,      outperforming (after fees and taxes) both the median balanced master trust      by 0.8% and slightly outperforming the broader median.</span><br /><br /></li>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Since      launch, our new MySuper default, Mercer SmartPath, grew to approximately      $1.7 billion of funds under management (FUM) by the end of June and      continues to grow.<br /></span></li>
</ul>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer      SmartPath 1969-73* is our largest SmartPath option. Over the quarter to 30      June 2014, its underlying portfolio returned 1.8%, with returns marginally      negative over the 30 days of June.</span></li>
</ul>
<p><strong><span style="color: #006d9e; font-size: 10.5pt; font-family: 'Arial','sans-serif';">WHAT DID WE DO?</span></strong></p>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Added IFM Investors to the Australian Small Cap Equity manager line up.&nbsp; Download the <a title="MST Investment Managers" href="https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf">manager list</a> as at 30 June 2014.</span></li>
</ul>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">We finalised our Dynamic Asset Allocation (DAA) views for the first quarter of the new financial year (i.e. Q3 2014).&nbsp; Download our </span><a title="DAA Dashboard Q3 2014 " href="http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">DAA Dashboard</span></a><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">.</span></li>
</ul>
<ul>
<li><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">No changes were made to the asset allocation in the month</span></li>
</ul></div>
</td>
<td width="4%" align="center" valign="top"><br></td>
<td width="48%" align="left" valign="top" ><div class="mktEditable" id="content2" ><p><span style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-size: 14px; color: #006d9e;">IN THIS ISSUE<br /></span></strong></span></p>
<hr />
<div><a style="font-size: 12px; color: #00a8c8; font-family: arial,helvetica,sans-serif; text-decoration: none;" href="#1">ECONOMY &amp; MARKETS</a></div>
<div>
<hr />
</div>
<div><a style="font-size: 12px; color: #00a8c8; font-family: arial,helvetica,sans-serif; text-decoration: none;" href="#2">INVESTMENT PERFORMANCE</a></div>
<div>
<hr />
</div>
<div><a style="font-size: 12px; color: #00a8c8; font-family: arial,helvetica,sans-serif; text-decoration: none;" href="#3"><span style="font-size: 12px; color: #00a8c8; font-family: arial,helvetica,sans-serif; text-decoration: none;"><span style="font-family: arial,helvetica,sans-serif; font-size: 12px; color: #00a8c8; text-decoration: none;">ASSET ALLOCATION UPDATE</span></span></a></div>
<div>
<hr />
</div>
<div><a style="font-size: 12px; color: #00a8c8; font-family: arial,helvetica,sans-serif; text-decoration: none;" href="#4"><span style="font-family: arial,helvetica,sans-serif; font-size: 12px; color: #00a8c8; text-decoration: none;">MANAGER UPDATE</span></a></div>
<hr />
<p><span style="font-family: arial,helvetica,sans-serif; font-size: 12px; color: #00a8c8; text-decoration: none;"><a href="http://www.linkedin.com/groups/Mercer-Investments-Asia-Pacific-4442861/about?trk=anet_ug_grppro" target="_blank"><img src="http://info.mercer.com/rs/mercer/images/LinkedIn.jpg" alt="" width="227" height="24" /></a></span></p>
<div><span style="font-family: arial,helvetica,sans-serif; font-size: 14px;"><br /></span></div>
<div><span style="font-family: arial,helvetica,sans-serif; font-size: 12px; color: #00a8c8; text-decoration: none;"><a href="{{system.forwardToFriendLink:default=edit%20me}}">Click Here to Forward Article<br /><br /></a></span></div>
<div><a title="MST Investment Managers" href="https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf"><img style="border: 1px solid #000000;" title="MST Portfolio Snapshot Investment Managers" src="http://info.mercer.com/rs/mercer/images/MST-Monthly-Portfolio-Snapshot-Investment-Managers.jpg" alt="MST Portfolio Snapshot Investment Managers" width="226" height="160" /></a><br /><br /></div>
<div><a title="DAA Dashboard Q3 2014 " href="http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf"><img style="border: 1px solid #000000;" title="MST Monthly Portfolio Snapshot DAA Dashboard Q3 2014" src="http://info.mercer.com/rs/mercer/images/MST-Monthly-Portfolio-Snapshot-DAA-Dashboard.jpg" alt="MST Monthly Portfolio Snapshot DAA Dashboard Q3 2014" width="226" height="160" /></a></div></div>
</td>
</tr>
</table>
<div class="mktEditable" id="content3" >
<hr>
<table style="font-size: 12px; font-family: arial,helvetica,sans-serif; height: 980px;" border="0" cellpadding="2" cellspacing="5" height="980" width="100%">
<tbody>
<tr>
<td style="text-align: left;" align="right"><span style="color: #1f497d; font-size: 13.5pt; font-family: 'Arial','sans-serif';"><a name="1">ECONOMY &amp; MARKETS</a></span></td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">The financial year ended with volatility at, or close to, historically low levels.&nbsp; All major asset classes delivered positive returns with many in double digits.&nbsp; As we enter the 2014-15 financial year, economic growth  
     is looking stronger than a year ago, particularly in the US.&nbsp; Meanwhile, the Australian economy has held up better than many had expected.
 </span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">However, it wasn’t smooth sailing all year.
       Financial markets kicked off the 2013-14 financial year on a slightly jittery note, as markets pondered when the US Federal Reserve would begin to “taper” or decrease the size of its asset purchase program and what the impact of this would be.&nbsp; Bond yields rose (prices  
down),      while emerging markets (particularly emerging markets debt) see-sawed, and the Aussie dollar sold off.
       Global developed market equities rallied however, despite other asset markets being more cautious.
       Nevertheless, many investors speculated that tapering would instigate a downturn in the market.
       While the delay in the commencement of tapering (from September to December), was welcomed by the markets, nothing significant actually happened when the Fed started tapering, despite all the noise beforehand.
       It is fair to say that concerns about emerging markets, particularly the so-called “fragile five” (Brazil, India, Indonesia, South Africa and Turkey), persisted after the commencement of tapering.
       These countries’ current high account deficits and structural issues affected emerging markets, which performed very poorly in January 2014.
 </span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Come February, it seemed that we suddenly entered a different environment altogether.
       While geopolitical issues kept investors on their toes, it wasn’t enough to stop emerging markets and the Australian dollar bouncing back, while global equities and sovereign bonds (ex-Europe) traded within a relatively narrow range for the remainder of the calendar year’s first 
      quarter.
       Meanwhile, the US economy experienced significant weather-induced setbacks, prompting some to question if slower activity was actually weather induced, or a sign of a loss of momentum in the US economy.
       We know now that, fortunately, it was the former.
 </span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Back home, Australian economic growth actually surprised throughout Q1 2014, with better-than-expected labour, consumer and capex data, before easing toward the end of the financial year.
       Through the final three months of the 2013-14 financial year, “Goldilocks” (i.e.
       just right) conditions prevailed globally.&nbsp; Equities continued to rally, volatility fell to ultra-low levels, and virtually all asset classes delivered positive returns.
       The strong equity performance can be explained by a positive macro backdrop and earnings growth, particularly in developed markets, and relatively cheap emerging market equity valuations.
       However, other developments are less easy to explain.
       The Australian dollar remains high, possibly supported by low volatility, despite softer commodity prices and tighter interest rate spreads.&nbsp; Global bond yields have fallen to what seem extraordinarily low levels given the prevailing macro conditions.
       Only time will tell whether these lower yields and low volatility are structural or cyclical phenomena.&nbsp; </span></p>
<span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Our </span><span style="font-size: 12pt; font-family: 'Times New Roman','serif';"><a title="DAA Dashboard Q3 2014" href="http://www.mercer.com.au/content/dam/mercer/attachments/asia-pacific/australia/Investment/DAA%20Dashboard_Q3%202014.pdf"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">DAA Dashboard</span></a></span><span style="font-size: 9pt; font-family: 'Arial','sans-serif';"> 
summarises our current views for the first three months of the new financial year (Q3 2014).&nbsp; Well worth a look.</span><a href="#top"><img title="Top" src="http://info.mercer.com/rs/mercer/images/top2.png" alt="Top" align="right"></a></td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<hr>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="color: #1f497d; font-size: 13.5pt; font-family: 'Arial','sans-serif';"><a name="2">INVESTMENT PERFORMANCE</a></span></p>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer Growth* performance was flat in June, taking financial year returns to 14.3%, a pleasing result, in a challenging and competitive year.
       Over the year, Mercer Growth outperformed its composite benchmark by 0.8%, with significant contributions from our underlying Australian Equity and Unlisted Infrastructure managers in particular.
       Our asset allocation decisions were less supportive in the second half of the financial year as the Australian dollar has strengthened, and some sovereign bond markets unexpectedly advanced.
       Mercer Growth also outperformed its CPI+3% target over the financial year by 8.3%.</span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer Growth has also performed more pleasingly on a peer relative basis, outperforming the Median SuperRatings Balanced Master Trust by 0.8% and finishing the year slightly ahead of the Broader Balanced Median (after fees and    
   taxes), marking a good reversal from the previous financial year’s position.&nbsp; It is also worth noting that Mercer Growth’s performance is ahead of the Master Trust Median over all times periods from one month to ten years in the June SuperRatings survey.&nbsp;&nbsp;&nbsp; 
</span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Inflows to <a title="Mercer SmartPath®" href="http://page.mercer.com/FeoEv00bk0D0H2wh000VbFu">Mercer SmartPath®</a> investment options continue to grow.
       By the end of June, funds under management (FUM) in Mercer SmartPath reached $1.7 billion.
       As FUM has reached critical mass, a number of new asset class exposures required by the introduction of Mercer SmartPath have gradually been added over the course of the year, so we are now in a position to provide updates on Mercer SmartPath performance going forward.&nbsp; </span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Due to the large number of Mercer SmartPath options, we will report performance for the Mercer SmartPath 1969-73 option only here because this is the largest option by FUM and is the most representative of underlying       
investors.&nbsp; This option typically has a higher weighting to growth assets (around     85%) relative to Mercer Growth (around     70%), so we would expect its growth assets exposure to outperform in positive months for risk assets, such as equities, and underperform in weaker risk   
markets.&nbsp;&nbsp;&nbsp; </span></p>
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Because Mercer SmartPath launched partway through the financial year, we are not yet able to report on its full financial year performance.&nbsp; That will have to wait until this time next year.&nbsp; In the month of June,  the   
   Mercer SmartPath 1969-73 option returns were marginally negative in absolute returns, but 1.8% over the June quarter.</span></p>
<span style="font-size: 9pt; font-family: 'Arial','sans-serif';">The </span><span style="font-size: 12pt; font-family: 'Times New Roman','serif';"><a title="MST CSD Monthly Report June 2014" href="https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_CSD_MonthlyReportJune2014.pdf"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">June Monthly Report</span></a></span><span style="font-size: 9pt; font-family: 'Arial','sans-serif';"> provides more details on the performance of Mercer Growth and Mercer SmartPath 1969-73, along with that of other investment  options.<br><a href="#top"><img title="Top" src="http://info.mercer.com/rs/mercer/images/top2.png" alt="Top" align="right"></a></span></td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<hr>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="color: #1f497d; font-size: 13.5pt; font-family: 'Arial','sans-serif';"><a name="3">ASSET ALLOCATION UPDATE</a></span></p>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">No changes were made to the asset allocation in June.
       We remain overweight growth assets in particular overseas shares, and retain underweight exposures to Global Sovereign Bonds and the Australian dollar.<br></span><span style="font-size: 9pt; font-family: 'Arial','sans-serif';"><a href="#top"><img title="Top" src="http://info.mercer.com/rs/mercer/images/top2.png" alt="Top" align="right"></a></span></p>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<hr>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="color: #1f497d; font-size: 13.5pt; font-family: 'Arial','sans-serif';"><a name="4">MANAGER UPDATE</a></span></p>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<p><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">IFM Investors was added to the Australian Small Cap Equity line-up in June.
       Australian Small Caps typically offer significant potential for active manager outperformance.
       However, many good Small Cap managers are closed, limiting our ability to invest as much as we would like in this sector.
       In this context, adding IFM to the line-up is a welcome development.
       IFM’s strategy uses fundamental research to identify mispriced stocks.&nbsp;</span><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Download the 30 June 2014 manager lists for the </span><a title="Mercer Super Trust Investment Managers" href="https://secure.superfacts.com/web/IWfiles/attachments/Form/MST_InvestmentManagers.pdf"><span style="font-size: 12pt; font-family: 'Times New Roman','serif';"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer Super Trust</span></span></a><span style="font-size: 9pt; 
font-family: 'Arial','sans-serif';"></span><span style="font-size: 9pt; font-family: 'Arial','sans-serif';"> for more information on the  line-up of our underlying managers at financial year end.<br><a href="#top"><img title="Top" src="http://info.mercer.com/rs/mercer/images/top2.png" alt="Top" align="right"></a></span></p>
</td>
</tr>
<tr>
<td style="text-align: left;" align="right">
<hr>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
</div>
</td>
<td width="25" align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="18">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
<div class="mkteditable" id="footer1" >
<table style="font-size: 12px; font-family: arial,helvetica,sans-serif; height: 500px;" border="0" cellpadding="8" cellspacing="2" height="500" width="630" align="center">
<thead> 
<tr valign="middle" align="center">
<td style="width: 630px; height: 66px; text-align: left;" valign="middle" align="left">
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">If you want to remove yourself from this distribution list,</span> <a href="mailto:Suzanne.Whiting@mercer.com?subject=Remove%20from%20MST%20Portfolio%20Snapshot%20distribution%20list"><span style="font-size: 9pt; font-family: 
'Arial','sans-serif';">request to be removed here</span></a><span style="color: #888888; font-size: 9pt; font-family: 'Arial','sans-serif';">.</span> <span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">&nbsp;</span></p>
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">If you no longer wish to receive any Mercer e-mails,</span> <a href="http://info.mercer.com/UnsubscribePage.html"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">unsubscribe here</span></a><span style="color: #888888; font-size: 9pt; font-family: 'Arial','sans-serif';">.</span></p>
</td>
</tr>
<tr valign="middle" align="center">
<td style="width: 643px; height: 66px; text-align: left;" valign="top" align="center">
<hr>
<img title="Mercer Awards" src="http://info.mercer.com/rs/mercer/images/MST-Monthly-Portfolio-Snapshot_footer.jpg" alt="Mercer Awards" height="66" width="643"></td>
</tr>
<tr valign="middle" align="center">
<td style="width: 643px; height: 66px; text-align: left;" valign="top" align="center">
<hr>
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">SuperRatings has awarded the Mercer Super Trust Corporate Superannuation Division its highest rating of ‘Platinum Rating’ for 2014.
   ‘SuperRatings’ does not issue, sell, guarantee or underwrite this product.
   Chant West has awarded the Mercer Super Trust the highest rating of ‘5 Apples’ for the Superannuation Division for 2014.
   The Chant West ratings logo is a trademark of Chant West Pty Limited and used under licence.
 For further information about the methodology used by Chant West, please see <a href="http://www.chantwest.com.au/">www.chantwest.com.au</a>.
   The Heron Partnership (Heron) has awarded Mercer’s Corporate Products (including the Mercer Super Trust’s Corporate Super and SmartSuper divisions) its highest ‘5 Star’ Quality rating for 2014.</span></p>
</td>
</tr>
<tr valign="middle" align="center">
<td style="width: 643px; height: 66px; text-align: left;" valign="top" align="center">
<hr>
<p><a name="OptionsPerformance" href="#2"></a><a href="#2"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">*NOTE</span></a><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';"><a href="#2">:</a> References to the ‘Mercer Growth’ are to the 
Mercer Super Trust’s Mercer Growth investment option and returns quoted are “pre-tax, pre-fees,” with the exception of performance numbers that have been sourced from the SuperRatings Fund Crediting Rate  Survey, which are after tax and fees.</span><span style="font-size: 9pt; 
font-family: 'Arial','sans-serif';"> <span style="color: black;">&nbsp;This will also apply to references to Mercer SmartPath returns, which relate to the largest Mercer SmartPath investment option in the Mercer Super Trust  – Mercer SmartPath 1969-73.</span></span>&nbsp;<span style="color: 
black; font-size: 9pt; font-family: 'Arial','sans-serif';"> </span></p>
</td>
</tr>
<tr valign="middle" align="center">
<td style="width: 643px; height: 66px; text-align: left;" valign="top" align="center">
<div align="center">
<hr>
</div>
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">This email has been prepared by Mercer Superannuation (Australia) Limited (MSAL) ABN 79 004 717 533, Australian Financial Services Licence #235906.
   Address: Collins Square, 727 Collins Street, Melbourne VIC 3008.
   <br>Tel: 03 9623 5555.
   MSAL is the trustee of the Mercer Super Trust, ABN 19 905 422 981.
   Any advice contained in this document is of a general nature only, and does not take into account the personal needs and circumstances of any particular individual.
   Prior to acting on any information contained in this document, you need to take into account your own financial circumstances, consider the Product Disclosure Statement for any product you are considering, and seek professional advice from a licensed, or appropriately authorised, financial   
adviser if you are unsure of what action to take.
   Past performance should not be relied upon as an indicator of future performance.
   'MERCER' is a registered trademark of Mercer (Australia) Pty Ltd ABN 32 005 315 917.</span></p>
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">Copyright 2014 Mercer LLC.
   All rights reserved.﻿﻿</span></p>
<p><span style="color: black; font-size: 9pt; font-family: 'Arial','sans-serif';">Find a</span> <a href="http://www.mercer.com/about-us/locations.html"><span style="font-size: 9pt; font-family: 'Arial','sans-serif';">Mercer office</span></a></p>
</td>
</tr>
</thead> 
<tbody>
</tbody>
</table>
</div>
<div class="mktEditable" id="footer2" ><img title="Mercer SuperThinking" src="http://info.mercer.com/rs/mercer/images/SuperThinking_MMC_footer.gif" alt="Mercer SuperThinking" height="61" width="600"></div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>