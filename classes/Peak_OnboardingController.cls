public class Peak_OnboardingController {
    @auraEnabled
    public static User getUserRecord(){
        User currentUser = [SELECT UserOnboardingAttempt__c, Completed_Groups_Slide__c, Completed_Profile_Slide__c, Completed_Topics_Slide__c,
                            Completed_Welcome_Slide__c, First_Name__c, Last_Name__c, Onboarding_Complete__c,Job_Title__c,Primary_Region__c,MP_Region__c,Industry__c,Functional_Business_Area__c,UserRole.Name
                            FROM User
                            WHERE Id = :userinfo.getuserId() limit 1];
        
        if(currentUser!=null && currentUser.UserOnboardingAttempt__c==null){
            currentUser.UserOnboardingAttempt__c=0;
        }
        if(currentUser!=null){
            currentUser.UserOnboardingAttempt__c++;
            update currentUser;
        }
            
        return currentUser;
    }

    @auraEnabled
    public static void updateUserNames(User currentUser){
        update currentUser;
    }

    @auraEnabled
    public static void updatePreferences(Boolean decision){
        Id networkId1 = [SELECT networkId
                        FROM networkmember
                        WHERE memberId = :userinfo.getUserId() limit 1].networkId;
        NetworkMember member = [Select PreferencesDisableAllFeedsEmail
                                FROM NetworkMember
                                WHERE MemberId = :userInfo.getUserId() AND networkId = :networkId1];
        if(!decision){
            member.PreferencesDisableAllFeedsEmail=true;
        } else if(decision){
            member.PreferencesDisableAllFeedsEmail=false;
        }
        update member;
    }


    @auraEnabled
    public static void insertGroupMember(String groupId,  Boolean following){
        List<CollaborationGroupMember> grps = [SELECT CollaborationGroupId
        FROM CollaborationGroupMember
        WHERE MemberId = :UserInfo.getUserId() AND CollaborationGroupId = :groupId];

        if(grps.isEmpty()) {
            CollaborationGroupMember grpMember = new CollaborationGroupMember();
            grpMember.CollaborationGroupId = groupId;
            grpMember.MemberId = UserInfo.getUserId();
            insert grpMember;
        }


    }

    @auraEnabled
    public static void completeSlide(String slide){
        String queryString;
        Map <String, String> slideMap = new Map <String, String>{'Welcome'=>'Completed_Welcome_Slide__c', 'Profile'=>'Completed_Profile_Slide__c',
        'Topic'=>'Completed_Topics_Slide__c', 'Group'=>'Completed_Groups_Slide__c'};
        String slideName = slideMap.get(slide);
        String userId = UserInfo.getUserId();

        if(slide != 'Done') {
            queryString = 'Select ' + slideName + ', Onboarding_Complete__c FROM User WHERE Id = :userId';
        } else {
            queryString = 'Select Onboarding_Complete__c FROM User WHERE Id = :userId';
        }
        User userRecord = Database.query(queryString);

        // complete slide checkbox as 'Save & Next' is hit.  These checkboxes keep track of the user's progress, and also
        // direct the user to the last uncompleted slide when this component is next initalized
        if(slide=='Welcome'){
            userRecord.Completed_Welcome_Slide__c=True;
        } else if(slide=='Profile'){
            userRecord.Completed_Profile_Slide__c=True;
        }  else if(slide=='Topic'){
            userRecord.Completed_Topics_Slide__c=True;
        } else if(slide=='Group'){
            userRecord.Completed_Groups_Slide__c=True;
        }
        system.debug('userrecord: ' +  userRecord);
        update userRecord;
    }

    @auraEnabled
    public static list <OnboardingWrapper> getTopics(list <Id> topicIds ){
        List<OnboardingWrapper> topicWrappers = new List<OnboardingWrapper>();
        Set <Id> topicsAlreadyFollowedIds = new Set <Id>();
        for (EntitySubscription member : [SELECT parentId
                                        FROM EntitySubscription
                                        WHERE SubscriberId = :userInfo.getUserId() limit 500]){
            topicsAlreadyFollowedIds.add(member.parentId);
        }

        for (Topic top : [Select Id, Name
                            FROM Topic
                            WHERE Id in :topicIds]){

           OnboardingWrapper wrapper = new OnboardingWrapper(top.Name, top.Id, false);

            if(topicsAlreadyFollowedIds.contains(top.Id)){
                wrapper.following = True;
            }
            topicWrappers.add(wrapper);
        }
        return topicWrappers;
    }

    @auraEnabled
    public static list <OnboardingGroupWrapper> getGroups(list <Id> groupIds ){
        List<OnboardingGroupWrapper> groupWrappers = new List<OnboardingGroupWrapper>();
        Set <Id> groupsAlreadyJoinedIds = new Set <Id>();
        for (CollaborationGroupMember grp : [SELECT CollaborationGroupId
                                            FROM CollaborationGroupMember
                                            WHERE MemberId = :userInfo.getUserId()]){
            groupsAlreadyJoinedIds.add(grp.CollaborationGroupId);
        }

        for (CollaborationGroup grp : [Select Id, Name,Description
                                        FROM CollaborationGroup
                                        WHERE Id in :groupIds]){

            OnboardingGroupWrapper wrapper = new OnboardingGroupWrapper(grp.Name, grp.Id, false,grp.Description);
            if(groupsAlreadyJoinedIds.contains(grp.Id)){
                wrapper.following = True;
            }
            groupWrappers.add(wrapper);
        }
        return groupWrappers; //
    }

    @auraEnabled
    public static OnboardingGroupWrapper getGroup(Id groupId){
        Boolean following = false;
        Set <Id> groupsAlreadyJoinedIds = new Set <Id>();

        for (CollaborationGroupMember grp : [SELECT CollaborationGroupId
                                            FROM CollaborationGroupMember
                                            WHERE MemberId = :userInfo.getUserId() AND
        CollaborationGroupId = :groupId]){
            groupsAlreadyJoinedIds.add(grp.CollaborationGroupId);
        }

        CollaborationGroup grp = [Select Id, Name, Description
                                FROM CollaborationGroup
                                WHERE Id = :groupId];

        if(groupsAlreadyJoinedIds.contains(grp.Id)){
            following = True;
        }

        OnboardingGroupWrapper wrapper = new OnboardingGroupWrapper(grp.Name, grp.Id, following,grp.Description);
        return wrapper;
    }

    @auraEnabled
    public static void removeGroupMember(Id groupId){

        list <CollaborationGroupMember> grps = [SELECT CollaborationGroupId
                                                FROM CollaborationGroupMember
                                                WHERE MemberId = :userInfo.getUserId() AND
                                                CollaborationGroupId = :groupId];

        if(grps.size() > 0) {
            CollaborationGroupMember grpMember = grps[0];
            delete grpMember;
        }
    }

    @AuraEnabled
    public static void followTopic(String topicId) {
        list <EntitySubscription> memberIds = [SELECT parentId, subscriberId
                                                FROM EntitySubscription
                                                WHERE subscriberId = :userInfo.getUserId() AND
                                                parentId = :topicId limit 500];

        if (memberIds.size() == 0) {
            EntitySubscription subscription = new EntitySubscription();
            subscription.ParentId = topicId;
            subscription.SubscriberId = UserInfo.getUserId();
            Id networkId1 = [Select networkId from networkmember where memberId = :userinfo.getUserId() limit 1].networkId;
            subscription.networkId = networkId1;
            insert subscription;
        } else {
            system.debug('already following, no subscription');
        }
    }

    @AuraEnabled
    public static void unfollowTopic(String topicId) {
        list <EntitySubscription> memberIds = [SELECT Id, parentId, subscriberId
                                                FROM EntitySubscription
                                                WHERE subscriberId = :userInfo.getUserId()
                                                AND parentId = :topicId limit 500];

        if (memberIds.size() > 0) {
            EntitySubscription subscriptionToDelete = memberIds[0];
            delete subscriptionToDelete;
        }
    }

    public class OnboardingWrapper {
        @auraEnabled
        public String name {get; set;}
        @auraEnabled
        public String id {get; set;}
        @auraEnabled
        public Boolean following {get; set;}

        public OnboardingWrapper(String name, String id, Boolean following){
            this.name = name;
            this.id = id;
            this.following = following;
        }

    }

    //Created separate wrapper class for Groups as we are passing description also in getGroup method.
    public class OnboardingGroupWrapper {
        @auraEnabled
        public String name { get; set; }
        @auraEnabled
        public String id { get; set; }
        @auraEnabled
        public Boolean following { get; set; }
        @auraEnabled
        public String description {get; set;}

        public OnboardingGroupWrapper(String name, String id, Boolean following, String description) {
            this.name = name;
            this.id = id;
            this.following = following;
            this.description = description;

        }
    }

    //Method for capturing Mercer Experts


    /*public static void upsertMercerExperts(String ExpertName,String UserName,Boolean Follow)

        {
            Mercer_Experts__c mercerExperts=new Mercer_Experts__c();
            User expertUser = [SELECT SmallPhotoUrl from User where Name=:ExpertName
                                                                and Expert__c=True];

           if (ExpertName !='')
            {
                mercerExperts.Name__c = ExpertName;
            }
            if (UserName !='') {
                mercerExperts.userName__c = UserName;
            }
            If (Follow != mercerExperts.Follow__c) {
                mercerExperts.Follow__c = Follow;
            }


            String firstSubString = expertUser.SmallPhotoUrl.substringBetween('<img', 'img>');
            System.debug('First substring: ' + firstSubString);

            String secondSubString = firstSubString.substringBetween('src="', '"');
            System.debug('Second substring: ' + secondSubString);

            String link = secondSubString.replace('amp;', '');
            System.debug('---link---'+link);
            mercerExperts.userImage__c=link;



        }*/

    /*Method for capturing Mercer Experts   */
    @AuraEnabled
    public static void upsertMercerExperts(String UserName,Boolean Follow)

    {
        Mercer_Experts__c mercerExperts = new Mercer_Experts__c();
        if (UserName !='') {
            mercerExperts.userName__c = UserName;
        }
        If (Follow != null) {
            mercerExperts.Follow__c = Follow;
        }
        upsert mercerExperts;
    }

    /*Method to allow follow / un follow Mercer Experts*/
    @AuraEnabled
    public static void followMercerExperts(String expertId1,Boolean expertFollow1,String expertId2, Boolean expertFollow2,
            String expertId3,Boolean expertFollow3,String expertId4,Boolean expertFollow4) {
        followMercerExpert(expertId1,expertFollow1);
        followMercerExpert(expertId2,expertFollow2);
        followMercerExpert(expertId3,expertFollow3);
        followMercerExpert(expertId4,expertFollow4);
    }
    @AuraEnabled
    public static void followMercerExpert(String expertId, Boolean following) {
    system.debug('---followMercerExper--' + expertId + '---' + following);
        try {

            if (String.isNotEmpty(expertId)) {
                List<EntitySubscription> memberIds = [
                        SELECT ParentId, SubscriberId
                        FROM EntitySubscription
                        WHERE SubscriberId = :UserInfo.getUserId() AND
                        ParentId = :expertId
                        LIMIT 1
                ];

                String networkId = Network.getNetworkId();
                if (Test.isRunningTest()) {
                    networkId = [SELECT Id FROM Network LIMIT 1][0].Id;
                }

                if (memberIds.isEmpty() && following) {
                    EntitySubscription subscription = new EntitySubscription();
                    subscription.ParentId = expertId;
                    subscription.SubscriberId = UserInfo.getUserId();
                    subscription.NetworkId = networkId;
                    insert subscription;
                } else if (!memberIds.isEmpty() && !following) {
                    delete memberIds;
                }
            }
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
//        List<Mercer_Experts__c> mercerExperts = new List<Mercer_Experts__c>();
//
//        List <Id> expertIdsToInsert = new list <Id>();
//        List <Id> expertIdsToDelete = new list <Id>();
//
//        If (expertFollow1 == false)
//            expertIdsToDelete.add(expertId1);
//        Else expertIdsToInsert.add(expertId1);
//
//        If (expertFollow2 == false)
//            expertIdsToDelete.add(expertId2);
//        Else expertIdsToInsert.add(expertId2);
//
//        If (expertFollow3 == false)
//            expertIdsToDelete.add(expertId3);
//        Else expertIdsToInsert.add(expertId3);
//
//        If (expertFollow4 == false)
//            expertIdsToDelete.add(expertId4);
//        Else expertIdsToInsert.add(expertId4);
//
//        delete [select Id from Mercer_Experts__c where Expert_User_Name__c in :expertIdsToDelete and userName__c=:userInfo.GetUserId()];
//        List<User> expertUser = [SELECT Id,Name,SmallPhotoUrl from User where Id=:expertIdsToInsert and Expert__c=true];
//
//        for (User expertToAdd:expertUser)
//        {
//            Mercer_Experts__c mercerExpertsToInsert = new Mercer_Experts__c();
//            mercerExpertsToInsert.userName__c=userInfo.GetUserId();
//            mercerExpertsToInsert.Expert_Image_Url__c=expertToAdd.SmallPhotoUrl;
//            mercerExpertsToInsert.Follow__c=true;
//            mercerExpertsToInsert.Expert_User_Name__c=expertToAdd.Id;
//            mercerExperts.add(mercerExpertsToInsert);
//        }
//
//        upsert mercerExperts;
    }

    /*Method to capture Id, Name and photo for a Mercer User*/

    @AuraEnabled
    public static Mercer_Experts__c getMercerExperts(String UserId) {

        System.debug(UserId);
        List<Mercer_Experts__c> mercerUser = [
                select Id,Expert_User_Name__r.Name,Expert_User_Name__r.SmallPhotoUrl,Follow__c
                from Mercer_Experts__c
                where Expert_User_Name__c = :userId
                LIMIT 1
        ];

        if (!mercerUser.isEmpty()) {
             List<EntitySubscription> memberIds = [
                    SELECT ParentId, SubscriberId
                    FROM EntitySubscription
                    WHERE SubscriberId = :UserInfo.getUserId() AND
                    ParentId = :UserId
                    LIMIT 1
            ];

            if (!memberIds.isEmpty()) {
                mercerUser[0].Follow__c = true;
            }
            System.debug(mercerUser);
            return mercerUser[0];
        } else {
            return new Mercer_Experts__c();
        }
    }

    @AuraEnabled
    public static Peak_Response getMercerExpertsByTopic(String topicId) {
        Peak_Response peakResponse = new Peak_Response();

        List<Mercer_Expert_Topic__c> mercerExpertTopics = new List<Mercer_Expert_Topic__c>([SELECT Id, Mercer_Experts__c,
                Mercer_Experts__r.Expert_User_Name__r.Name, Mercer_Experts__r.Expert_User_Name__r.SmallPhotoUrl FROM
                Mercer_Expert_Topic__c WHERE TopicId__c = :topicId]);
        peakResponse.success = true;
        peakResponse.results = mercerExpertTopics;

        return peakResponse;
    }

    @AuraEnabled
    public static Peak_Response saveMercerExpertsByTopic(String topicId, String expertId) {
        Peak_Response peakResponse = new Peak_Response();
        Mercer_Experts__c mercerExpert = new Mercer_Experts__c();

        try {
            List<Mercer_Experts__c> mercerExperts = new List<Mercer_Experts__c>([SELECT Id FROM Mercer_Experts__c WHERE
                    Expert_User_Name__c = :expertId]);

            if (!mercerExperts.isEmpty()) {
                mercerExpert = mercerExperts[0];
            } else {
                List<User> mercerUser = new List<User>([SELECT Id,Name,SmallPhotoUrl FROM User WHERE Id = :expertId LIMIT 1]);
                mercerExpert.Expert_User_Name__c = expertId;
                mercerExpert.Expert_Image_Url__c = mercerUser[0].SmallPhotoUrl;
                insert mercerExpert;
            }

            Mercer_Expert_Topic__c expertTopic = new Mercer_Expert_Topic__c();
            expertTopic.Mercer_Experts__c = mercerExpert.Id;
            expertTopic.TopicId__c = topicId;
            insert expertTopic;
            System.debug(expertTopic);

            peakResponse.success = true;
        } catch (Exception e) {
            peakResponse.success = false;
            peakResponse.messages.add(e.getMessage());
        }

        return peakResponse;
    }

    @AuraEnabled
    public static Peak_Response deleteMercerExpertsByTopic(String topicId, String expertId) {
        Peak_Response peakResponse = new Peak_Response();

        List<Mercer_Expert_Topic__c> topicAssignments = new List<Mercer_Expert_Topic__c>([SELECT Id FROM
                Mercer_Expert_Topic__c WHERE TopicId__c = :topicId AND Mercer_Experts__c = :expertId]);
        if (!topicAssignments.isEmpty()) {
            delete topicAssignments;
        }

        peakResponse.success = true;

        return peakResponse;
    }

    /*Method for updating Users with the data they have entered on Onboarding model page*/

    @AuraEnabled
    public static void upsertUserProfile(String iFirstName,String iLastName,String iIndustry, String iJobTitle, String iFunctionalArea, String iRegion)
    {

        User currentUser = [SELECT First_Name__c, Last_Name__c,Primary_Region__c,Job_Title__c, MP_Region__c,Industry__c,Functional_Business_Area__c,UserRole.Name,UserRoleId
        FROM User
        WHERE Id = :userinfo.getuserId()];

        If (iFirstName != '')
        {
            currentUser.First_Name__c = iFirstName;
        }

        if (iLastName !='' )
        {
            currentUser.Last_Name__c = iLastName;
        }
        if (iIndustry !='' )
        {
            currentUser.Industry__c = iIndustry;
        }
        /*if (iRole !='' && iRole <> currentUser.UserRole.Name)
        {  List<UserRole> UserRoles=[select Id from UserRole where Name=:iRole];
            if (UserRoles<>null && UserRoles.size()>0){
        currentUser.UserRoleId = UserRoles[0].Id;
                }

        }*/
        if (iJobTitle !='')
        {
          currentUser.Job_Title__c	= iJobTitle; 
        }
        
        if (iFunctionalArea !='' )
        {
            currentUser.Functional_Business_Area__c = iFunctionalArea;
        }
        if (iRegion !='' )
        {
            //currentUser.Mp_Region__c = iRegion;
            currentUser.Primary_Region__c = iRegion;
        }


        update currentUser;

    }
    /*Method to capture Topics a users is active in*/
    @AuraEnabled
    public static list <EntitySubscription> topicsUsersIsActiveIn() {
        list <EntitySubscription> topicIds = [
                SELECT parentId
                FROM EntitySubscription
                WHERE subscriberId = :userInfo.getUserId()
                limit 5
        ];
        //SELECT id , parentid , subscriberid , subscriber.email, Subscriber.name from EntitySubscriptions                                                parent.name FROM EntitySubscription  WHERE parentid = '<Topc Id>'

        system.debug('Topics User is Active in :'+ topicIds);
        return topicIds;
    }
    /*Method to capture total number of Posts and Followers for a topic*/
    @AuraEnabled
    public static list <Integer> getFollowersAndPostsForTopic(string topicId) {
        List<Integer> result=new List<Integer>();
        if(topicId!=null){
            List<EntitySubscription> followers= [SELECT parentId,SubscriberId  FROM EntitySubscription WHERE ParentId=:topicId ];
            if(followers!=null){
                result.add(followers.size());
            }else{
                result.add(0);
            }
           
            List<FeedItem> posts= [SELECT id FROM FeedItem WHERE Id IN (select EntityId from TopicAssignment where TopicId = :topicId)];
            if(posts!=null){
                result.add(posts.size());
            }else{
                result.add(0);
            }
            
        }
        
        
        return result;
    }

    @AuraEnabled
    public static List<String> getPicklistOptions(String pickListField) {
        return Peak_Utils.getPicklistValues(pickListField.substringBefore('.'), pickListField.substringAfter('.'));
    }

}