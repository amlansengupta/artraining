/***************************************************************************************************************
Request Id : 17348
Purpose : This Test Class is created for the Apex Class MF2_Mercer_Opportunity_New_Apex
Created by : Harsh Vats
Created Date : 11-APRIL-2019
Project : MF2
****************************************************************************************************************/
@isTest(seeAllData = true)
private class MF2_Mercer_Opportunity_New_Apex_Test {
 
    public static User user1,user2, user3 = new User();
    
    private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = String.valueOf(Integer.valueOf('1234567890')+ i);
        insert testUser;
        System.runas(testUser){
        }
        return testUser;
        
    }
    
    public static void insertUsers(){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1, 5);
        user2 = createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2, 6);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);
        //user1.EmployeeNumber = '123';                  
    }    
    
    
    static testMethod void myUnitTestsave1() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 Employee_office__c='Boston - High St'      
                                );
        
        insert testUser;
        User testUser1 = new User(LastName = 'tetsLIVESTON',
                                  FirstName='JASON',
                                  Alias = 'jliv',
                                  Email = 'jasontest.liveston@asdf.com',
                                  Username = 'jasontest.test@test.com',
                                  ProfileId = profileId.id,
                                  TimeZoneSidKey = 'GMT',
                                  LanguageLocaleKey = 'en_US',
                                  EmailEncodingKey = 'UTF-8',
                                  LocaleSidKey = 'en_US',
                                  Employee_office__c='Mercer US Mercer Services - US03'      
                                 );
        insert testUser1;
        
        Account testAccount = new Account();
        System.runAs(testUser1){
            
            
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser1.Id;
            insert testAccount;
            
        }
        System.runAs(testUser){
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty1.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = testUser.Employee_office__c;
            
            //String content = '{\"Name\":\"TestOpp4\",\"AccountId\":\"0010m00000J12CAAAZ\",\"CloseDate\":\"2019-04-30\",\"Type\":\"Existing Client - New LOB\",\"Buyer__c\":\"0030m00000JIR2QAAX\",\"CurrencyIsoCode\":\"USD\",\"Opportunity_Name_Local__c\":\"MegaCorp\",\"StageName\":\"Identify\",\"Opportunity_Office__c\":\"Boston - High St\"}';
            String content = JSON.serialize(testOppty1);

            MF2_Mercer_Opportunity_New_Apex.OpportunityWrapper oppWrapper = MF2_Mercer_Opportunity_New_Apex.getAutoPopulateOpportunityFields();
            String oppId;
            try{
                oppId = MF2_Mercer_Opportunity_New_Apex.saveOpp(content);
            }
            Catch(Exception e){}
            List<Opportunity> oppList = [Select StageName from Opportunity where Id =:oppId];
            if(oppList != null && oppList.size() > 0){
                system.assertEquals(testOppty1.StageName, oppList.get(0).StageName);
            }
        }
        
        
    }
    
    
    
    
}