public class NewGrowthPlanController{
    public Account GrowthAccount{get;set;}
    public Growth_Plan__c gp{get;set;}
   
    public NewGrowthPlanController(Apexpages.standardController con){        
        string accid = ApexPages.CurrentPage().getParameters().get('accid');
        gp = new Growth_Plan__c(); 
        gp.Status__c = 'Active';
        GrowthAccount = new Account();      
        if(accid <> null)
        {
            
            GrowthAccount = [Select Id,Account_Country__c,Relationship_Manager__c,Global_Relationship_Manager__c,Relationship_Manager__r.Country__c from Account where Id=: accid  limit 1];
            gp.Country__c = GrowthAccount.Account_Country__c;
            gp.Account__c = GrowthAccount.id;
            
            if(string.valueOf(GrowthAccount.Global_Relationship_Manager__c) != null && string.valueOf(GrowthAccount.Global_Relationship_Manager__c) != ''){
                gp.GP_Account_Type__c = 'Global Account';
            }
            else{
                gp.GP_Account_Type__c = 'Non Key Account';
            }  
            
        }
               
        List<FCST_Fiscal_Year_List__c> lstFY = [select id,Name from FCST_Fiscal_Year_List__c where Name=: string.valueOf(system.today().year()+1)];
        if(lstFY.size() > 0){
            gp.fcstPlanning_Year__c = lstFY[0].id;
            gp.Planning_Year__c = lstFY[0].Name;
        }
    }
    public void  onSelectAccount(){
        gp.Country__c = null;
        if(gp.Account__c <> null)
        {
            GrowthAccount = [Select Id,Account_Country__c,Relationship_Manager__c,Relationship_Manager__r.Country__c from Account where Id=: gp.Account__c limit 1];
            gp.Country__c = GrowthAccount.Account_Country__c;
            
        }
    }
    public PageReference saveGrowthPlan(){
        try{
        
        List<FCST_Fiscal_Year_List__c> lstFY = [select id,Name from FCST_Fiscal_Year_List__c where id=: gp.fcstPlanning_Year__c];
        if(lstFY.size() > 0){
            gp.Planning_Year__c = lstFY[0].Name;
        }
        
        insert gp;
        
        pagereference pg = new pagereference ('/apex/AccountDetail_F4V4?id='+gp.id);
        pg.setRedirect(true);
        return pg;
        }catch(Exception e){
            //apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getmessage()));
            return null;
        }
    }
    public PageReference cancel(){
        string accid = '001/o';
        if(ApexPages.CurrentPage().getParameters().get('accid') <> null && ApexPages.CurrentPage().getParameters().get('accid') <> '')
        accid = ApexPages.CurrentPage().getParameters().get('accid');        
        pagereference pg = new pagereference ('/'+accid);
        pg.setRedirect(true);
        return pg;
    }
}