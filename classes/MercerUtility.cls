/*Purpose:  This Apex class contains static methods to process Contact records on their various stages of insertion/updation/deletion
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE          DETAIL 
   1.0 -    Jyotsna   03/23/2014    Created Apex class as a central libray. All the utility methods will be created in this class.
   
*/
public without sharing class MercerUtility {
    public static final string STR_FIELD_CUSTOM_VALIDATION_EXCEPTION = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
    public static final string STR_BRACKET = ': [';
    /*
    * A utility method used to add errors and display on VF page. 
    * This method removes the Salesforce text from validation errors and picks the exact error mesage for the user
    * This method also avoids having duplicate error messages, if any
    */   
    public static void addErrorMessage(String msg)
    {
            if(msg!=null)
            {
                if(msg.indexOf(STR_FIELD_CUSTOM_VALIDATION_EXCEPTION)>-1) 
                        {
                                msg = msg.substring(msg.indexOf(STR_FIELD_CUSTOM_VALIDATION_EXCEPTION) + String.valueOf(STR_FIELD_CUSTOM_VALIDATION_EXCEPTION).length() + 1);
                                if(msg.indexOf(STR_BRACKET)>-1)
                                        msg = msg.substring(0, msg.indexOf(STR_BRACKET));
                        }
                        boolean hasError = false;
                        System.debug('\n\nADD ERROR: ' + msg);
                        for(ApexPages.Message m: ApexPages.getMessages())
                        {
                            msg = msg.trim();
                            String detail = m.getDetail();
                            if(detail!=null)
                                detail = detail.trim();
                            System.debug('\n\nERROR DETIAL: ' + m.getDetail());
                            System.debug('\n\nERROR CHK: ' + m.getDetail().indexOf(msg));
                            if(detail!=null && detail.indexOf(msg)>-1)
                            {
                                hasError = true;        
                            }
                        }
                        System.debug('\n\nHASERROR: ' + hasError);
                        if(!hasError)
                            {
                                ApexPages.Message em = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
                                ApexPages.addMessage(em);
                            }
            }       
    }
}