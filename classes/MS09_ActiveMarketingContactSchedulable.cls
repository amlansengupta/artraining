/*Purpose: This Apex class implements schedulable interface to schedule MS10_ActiveMarketingContactBatchable class.
==============================================================================================================================================
History ----------------------- 
                                                                 VERSION     AUTHOR       DATE                    DETAIL 
                                                                    1.0 -    Arijit     04/29/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS09_ActiveMarketingContactSchedulable implements Schedulable{
        
    global void execute(SchedulableContext sc)
    {
       Database.executeBatch(new MS10_ActiveMarketingContactBatchable(), Integer.valueOf(System.Label.CL76_MS10BatchSize));
    }        
}