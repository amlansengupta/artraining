/*Purpose: This test class provides data coverage to AP63_UserAccountSchedulable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Srishty   07/18/2013  Created Test class
============================================================================================================================================== 
*/
@isTest
private class Test_AP63_UserAccountSchedulable {
	/*
     * @Description : Test method to provide data coverage to AP63_UserAccountSchedulable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void testDailyBatch() 
    {
    	 
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){

	      AP63_UserAccountSchedulable usersch = new AP63_UserAccountSchedulable();
	      DateTime r = DateTime.now()+5;
	      String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?'; 
	      
	      system.schedule('AP63_UserAccountSchedulable', nextTime, usersch);
        }
    }   
}