/**
Purpose:   This apex class is written for handling errors in  Product Search Pagination 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 2940)Created Apex class 
============================================================================================================================================== 
*/
global class AP90_IllegalStateException extends Exception {
 
 public void IllegalException() 
 {
 AP90_IllegalStateException testexception = new AP90_IllegalStateException();
 }
 
 /*private static testMethod void myUnitTest() {  
 
    test.startTest();
       try {
        AP90_IllegalStateException testexception = new AP90_IllegalStateException();
        //throw new AP90_IllegalStateException('test exception');
      } catch(Exception ex) {
         system.assert(true, ex.getMessage());
            
      }
    test.stopTest();
        
        
    }*/
}