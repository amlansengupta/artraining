/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AP12_ColleagueSchedulable 
{
	/*
     * @Description : Test method for providing code coverage to ColleagueSchedulable schedulable class 
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void testColleagueSchedulable() 
    {
        Test.StartTest();
         
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            AP12_ColleagueSchedulable bc = new AP12_ColleagueSchedulable();
            String sch = '0 0 23 * * ?';
            system.schedule('Test MercerColleagueSchedulable', sch, bc);
        }
        Test.stopTest();
    }
}