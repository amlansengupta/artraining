/*
Purpose: Test Class for providing code coverage to AP65_EmailController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Gyan Chandra   30/8/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_AP65_EmailController{
  	/*
     * @Description : Test method to provide data coverage to AP65_EmailController class 
     * @ Args       : Null 
     * @ Return     : void
  */	
  testMethod static void testsendEmail() {
  
       //Create Custom Setting Record
        MercerSupportEmail__c cs = new MercerSupportEmail__c();
        cs.Name = 'Support Email';
        cs.Value__c = 'test@test.com';
        insert cs;
        
        //SendEmail() Method Test Functionality
        AP65_EmailController.sendLiteEmail('005E00000027zKx','MercerForce_Lite_welcome_email_template');
  }
}