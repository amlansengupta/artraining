/*Purpose: This Apex class implements schedulable interface to schedule MercerForce_Account_Status_Batch class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   12/1/2014  Created Apex Schedulable class
============================================================================================================================================== 
*/

global class MercerForce_Account_Status_Batch_Sched implements Schedulable {
    
    // schedulable execute method
    global void execute(SchedulableContext SC) 
    {       
        // call database execute method to run AP06 batch class
        database.executeBatch(new MercerForce_Account_Status_Batch(), Integer.valueOf(system.label.CL72_MS02BatchSize)); 
    }
}