/*Purpose: Test Class for providing code coverage to MercVFC04_DefaultTeamController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Suchi   02/20/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_MercVFC04_DefaultTeamController 
{
    
    /* * @Description : This test method provides data coverage for the Opportunity Team functionality         
* @ Args       : no arguments        
* @ Return     : void       
*/
    static testMethod void myUnitTest() 
    {
        Opportunity testOppty = new Opportunity();
        Account testAccount = new Account();
        UserTeamMember utm = new UserTeamMember();
        
        PageReference pageRef = Page.Mercer_AddDefaultTeam;
        User user1 = new User();
        User user2 = new User();
        String adminprofile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(adminprofile , 'usert2', 'usrLstName2', 2);
        
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        user2.alias = 'usert2';
        user2.email = 'test@xyzcvfds.com';
        user2.emailencodingkey='UTF-8';
        user2.lastName = 'usrLstName2';
        user2.languagelocalekey='en_US';
        user2.localesidkey='en_US';
        user2.ProfileId =  adminprofile;
        user2.timezonesidkey='Europe/London';
        user2.UserName = randomName + '.' + 'usrLstName2' +'@xyzcvsdc.com';
        user2.EmployeeNumber = '12345678909';
        Database.insert(user2);
        
        system.runAs(user1){
                   
            Test.setCurrentPage(pageRef);
            
            Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
            mogd.Name = 'TestMogd';
            mogd.Office_Code__c = 'TestCode';
            mogd.Office__c = 'Aarhus';
            mogd.Region__c = 'Asia/Pacific';
            mogd.Market__c = 'ASEAN';
            mogd.Sub_Market__c = 'US - Central';
            mogd.Country__c = 'United States';
            mogd.Sub_Region__c = 'United States';
            insert mogd;
            
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.EMPLID__c = '123456781';
            testColleague.LOB__c = '1234';
            testColleague.Location__c = mogd.Id;
            testColleague.Last_Name__c= 'Test';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'moon@moon.com';
            insert testColleague;
        }
            
            /* Colleague__c testColleague1 = new Colleague__c();
testColleague1.Name = 'TestColleague1';
testColleague1.EMPLID__c = '5212345678';
testColleague1.LOB__c = '12345';
testColleague1.Location__c = mogd.Id;
testColleague1.Last_Name__c= 'Test1';
testColleague1.Empl_Status__c = 'Active';
testColleague1.Email_Address__c = 'sun@sun.com';
insert testColleague1;

User u = [select id from User Where isActive = True  and Email_Address__c = 'barb.backus@mercer.com' limit 1];

User testUser = new User();
//String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
//tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 1);


//testUser = new User();
testUser.alias = 'alias1';
testUser.email = 'jupiter@xyz.com';
testUser.emailencodingkey='UTF-8';
testUser.lastName = 'lastName1';
testUser.languagelocalekey='en_US';
testUser.localesidkey='en_US';
testUser.ProfileId = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
testUser.timezonesidkey='Europe/London';
testUser.UserName = 'erk1@bbcm.com';
testUser.EmployeeNumber = '12345678901';
insert testUser;
System.debug('test***a'+testUser.EmployeeNumber);*/
            
            system.runAs(user2){
            
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
           // testAccount.client_Retention__c = 1000;
            system.debug('test%%05'+testAccount.client_Retention__c);
            testAccount.One_Code_Status__c = 'Active';
            testAccount.One_Code__c = 'ABC123';
            testAccount.ownerId = user2.Id;
            
            // testAccount.Relationship_Manager__c = testColleague.Id;
            system.debug('test%%05relation'+testAccount.Relationship_Manager__c);
            testAccount.Domestic_DUNS__c = '123456';
            system.debug('test%%05relation111%%'+testAccount.Domestic_DUNS__c);
            testAccount.DUNS__c = '123456';
            testAccount.HQ_Immediate_Parent_DUNS__c = '123456';
            testAccount.GU_DUNS__c = '123456';
            testAccount.Primary_SIC_Code__c = 'test1234';
            //try{
            insert testAccount;
            //}catch(Exception ex){}
            system.debug('!!test'+testAccount.DUNS__c);
            
            
            // testAccount.Sic= 'TestSic';
            // testAccount.Relationship_Manager__c = testColleague1.Id;
            // testAccount.Name = 'TestAccountName11';
            // testAccount.Domestic_DUNS__c = '1234567';
            // system.debug('test%%05relation1%%'+testAccount.Domestic_DUNS__c);
            // testAccount.Primary_SIC_Code__c = 'test12345';
            //update testAccount;
            system.debug('test%%05relation1%%'+testAccount.Name);
            utm = [Select UserId,TeamMemberRole,OpportunityAccessLevel,OwnerId from UserTeamMember Limit 1]; 
            }
            system.runAs(user1){
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'Montreal - McGill';
            testOppty.ownerid = utm.OwnerId;
             Test.startTest();
            insert testOppty; 
            Test.stoptest();
            
            
             
        
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC04_DefaultTeamController extCont = new MercVFC04_DefaultTeamController(stdController);
            pageRef = extCont.methodToInsert();
            
        }
            
       
    }     
}