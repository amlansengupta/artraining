// ===================
// Test Peak Utils
// ===================
@isTest
public with sharing class Peak_UtilsTest {
	
	// Test building a formatted string from a set of strings
	@isTest
	public static void testBuildStringFromSet(){
		Peak_Utils utils = new Peak_Utils();
		Set<String> sourceSet = new Set<String>();
		sourceSet.add('Hello');
		sourceSet.add('There');
		system.assertEquals('Hello, There', utils.buildStringFromSet(sourceSet)); // test that this is building a string from a set properly
	}

	@isTest
	public static void testGetUser(){
		Peak_TestUtils testUtils = new Peak_TestUtils();
		User user = testUtils.createStandardUser();
		insert user;

		system.runAs(user){
			system.assertEquals(user.Id,Peak_Utils.getUser().Id);
		}
	}

	@isTest
	public static void testIsNullOrEmpty() {
		// Assert return true for empty list
		List<String> stringList = new List<String>();
		system.assertEquals(Peak_Utils.isNullOrEmpty(stringList),true);

		// Assert return false for not empty list
		stringList.add(Peak_TestConstants.ACCOUNT_NAME);
		system.assertEquals(Peak_Utils.isNullOrEmpty(stringList),false);
	}

	@isTest
	public static void testGetSitePrefix() {
		system.assert(Peak_Utils.getSitePrefix() != null);
	}

	@isTest
	public static void testCreateAttachment() {
		Peak_TestUtils testUtils = new Peak_TestUtils();
		User user = testUtils.createStandardUser();
		insert user;

		Attachment testAttachment = testUtils.createAttachment(user.Id);
		system.assert(testAttachment != null);
	}

	@isTest
	public static void testGetPicklistValues(){
//		Peak_Utils peakUtils = new Peak_Utils();
		List<String> options = Peak_Utils.getPicklistValues('User','LanguageLocaleKey');
		System.assert(options.size()>0);
	}

	@isTest
	public static void test_trimString() {
		String originalStringLong = 'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJ' +
				'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJ' +
				'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJ';
		String originalStringGood = 'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJ' +
				'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghijABCDEFGHIJ' +
				'abcdefghijABCDEFGHIJabcdefghijABCDEFGHIJabcdefghij';

		String trimmedStringLong = Peak_Utils.trimString(originalStringLong, 255);
		String trimmedStringGood = Peak_Utils.trimString(originalStringGood, 255);

		System.assertNotEquals(originalStringLong, trimmedStringLong);
		System.assertEquals(originalStringGood, trimmedStringGood);
		System.assertEquals(originalStringGood, trimmedStringGood);
	}

    @isTest
    public static void isNullOrEmptyTest()
    {
        Boolean isNullOrEmpty=Peak_Utils.isNullOrEmpty('test');
        System.assertEquals(false,isNullOrEmpty);
    }
    @isTest
    public static void isGuestUserTest()
    {
        Boolean isGuestUser=Peak_Utils.isGuestUser();
        System.assertEquals(false,isGuestUser);
    }
}