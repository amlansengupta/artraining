/* Purpose: This test class provides data coverage for updating the Concatenated Industry Category associated with 
            an Account 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       08/25/2013  Created test method for Test_DC25_AccountSchedulable batch class
 ==================================================================================================================*/    
@isTest
private class Test_DC25_AccountSchedulable 
{
    /* * @Description : This test method provides data coverage for testAccountSchedulable shedulable class 
                        for updating the Concatenated Industry Category associated with an Account              
       * @ Args       : no arguments        
       * @ Return     : void       
    */  
    static testMethod void testAccountSchedulable() 
    {
        Test.StartTest();
            DC25_AccountSchedulable ac = new DC25_AccountSchedulable();
           DateTime r = DateTime.now();
           String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
           system.schedule('DC25_AccountSchedulable', nextTime, ac);
        Test.stopTest();
    }
}