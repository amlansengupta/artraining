public class TaskEmailSendController{

    public ID taskWhoId {get; set;}
    public ID taskWhatId {get; set;}
    public ID taskSfdcId {get; set;}
    public List<Opportunity> oppList {get;set;}
    public List<Task> taskList {get; set;}
    public List<Contact> contactList {get; set;}
    public List<Lead> leadList {get; set;} 
    public List<conLeadWraper> conLeadWraperList {get; set;} 
    Public class conLeadWraper{
        public ID id {get; set;}
        public String accountid {get; set;}
        public String name {get; set;}
        public String title {get; set;}
        public String accountName {get; set;}
        public String accountRelMngrName {get; set;}
        public String accountType {get; set;}
        public String accountBillingCity {get; set;}
        public String accountBillingStreet {get; set;}
        public String accountBillingState {get; set;}
        public String accountBillingCountry {get; set;}
        public String accountBillingPostalCode {get; set;}
        public String accountPhone {get; set;}
        public String accountCountry {get; set;}
        public String phone {get; set;}
        public Decimal empEditable {get; set;}
        public Integer noOfEmployee {get; set;}
    }

    public void getDetails(){
   
        taskList = new List<Task>();        
        oppList = new List<Opportunity >();
        contactList = new List<Contact>();
		leadList = new List<Lead>();
        conLeadWraperList = new List<conLeadWraper>();
            
        taskList = [Select Id,Subject,Description,whoId,whatId From Task Where Id = :taskSfdcId ];        

        if(!taskList.isEmpty()){            
                if (taskList[0].whatId != null){
                    if (taskList[0].whatId.getSobjectType() == Opportunity.SObjectType){
                        oppList = [Select Id,Name,AccountId,Account.Name,Account.Relationship_Manager__r.Name,Buyer__c,
                               Buyer__r.Name,OwnerId,Owner.Name
                               From Opportunity Where Id = :taskList[0].whatId Limit 1];               
                    }
                }    
                if (taskList[0].whoId.getSobjectType() == Contact.SObjectType){
                    contactList = [Select Id,accountid,Name,Title,Account.Name,Account.Relationship_Manager__r.Name,Account.Type,
                                   Account.BillingCity,Account.BillingStreet,Account.BillingState,Account.BillingCountry,
                                   Account.BillingPostalCode,Account.Phone,Account.Account_Country__c,Phone,
                                   Account.Employees_Editable__c,Account.NumberOfEmployees
                                   From Contact Where Id = :taskList[0].whoId];
                    
                    for(Contact con: contactList){
                        conLeadWraper cw = new conLeadWraper();
                        cw.Id = con.Id;
                        cw.accountid = con.accountid;
                        cw.name = con.name;
                        cw.title = con.Title;
                        cw.accountName = con.Account.Name;
                        cw.accountRelMngrName = con.Account.Relationship_Manager__r.Name;
                        cw.accountType = con.Account.Type;
                        cw.accountBillingCity = con.Account.BillingCity;
                        cw.accountBillingStreet = con.Account.BillingStreet;
                        cw.accountBillingState = con.Account.BillingState;
                        cw.accountBillingCountry = con.Account.BillingCountry;
                        cw.accountBillingPostalCode = con.Account.BillingPostalCode;
                        cw.accountPhone = con.Account.Phone;
                        cw.accountCountry = con.Account.Account_Country__c;
                        cw.phone = con.Phone;
                        cw.empEditable = con.Account.Employees_Editable__c;
                        cw.noOfEmployee = con.Account.NumberOfEmployees;
                        
                        conLeadWraperList.add(cw);
                    }                    
                    
                }    
				if (taskList[0].whoId.getSobjectType() == Lead.SObjectType){
                    leadList = [Select Id,LeanData__Reporting_Matched_Account__c,Name,Title,LeanData__Reporting_Matched_Account__r.Name,LeanData__Reporting_Matched_Account__r.Relationship_Manager__r.Name,LeanData__Reporting_Matched_Account__r.Type,
                                   LeanData__Reporting_Matched_Account__r.BillingCity,LeanData__Reporting_Matched_Account__r.BillingStreet,LeanData__Reporting_Matched_Account__r.BillingState,LeanData__Reporting_Matched_Account__r.BillingCountry,
                                   LeanData__Reporting_Matched_Account__r.BillingPostalCode,LeanData__Reporting_Matched_Account__r.Phone,LeanData__Reporting_Matched_Account__r.Account_Country__c,Phone,
                                   LeanData__Reporting_Matched_Account__r.Employees_Editable__c,LeanData__Reporting_Matched_Account__r.NumberOfEmployees
                                   From Lead Where Id = :taskList[0].whoId];
                    for(Lead lead: leadList){
                        conLeadWraper cw = new conLeadWraper();
                        cw.Id = lead.Id;
                        cw.accountid = lead.LeanData__Reporting_Matched_Account__c;
                        cw.name = lead.name;
                        cw.title = lead.Title;
                        cw.accountName = lead.LeanData__Reporting_Matched_Account__r.Name;
                        cw.accountRelMngrName = lead.LeanData__Reporting_Matched_Account__r.Relationship_Manager__r.Name;
                        cw.accountType = lead.LeanData__Reporting_Matched_Account__r.Type;
                        cw.accountBillingCity = lead.LeanData__Reporting_Matched_Account__r.BillingCity;
                        cw.accountBillingStreet = lead.LeanData__Reporting_Matched_Account__r.BillingStreet;
                        cw.accountBillingState = lead.LeanData__Reporting_Matched_Account__r.BillingState;
                        cw.accountBillingCountry = lead.LeanData__Reporting_Matched_Account__r.BillingCountry;
                        cw.accountBillingPostalCode = lead.LeanData__Reporting_Matched_Account__r.BillingPostalCode;
                        cw.accountPhone = lead.LeanData__Reporting_Matched_Account__r.Phone;
                        cw.accountCountry = lead.LeanData__Reporting_Matched_Account__r.Account_Country__c;
                        cw.phone = lead.Phone;
                        cw.empEditable = lead.LeanData__Reporting_Matched_Account__r.Employees_Editable__c;
                        cw.noOfEmployee = lead.LeanData__Reporting_Matched_Account__r.NumberOfEmployees;
                        
                        conLeadWraperList.add(cw);
                    }
                }                            
          } 
    }
    
}