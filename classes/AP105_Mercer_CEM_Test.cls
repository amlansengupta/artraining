/*Purpose: Test class to provide test coverage for CEM  functionality.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
1.0 -    Madhavi   11/13/2014    Created test class to provide test coverage for  AP105_Mercer_CEM  class

============================================================================================================================================== 
*/
@isTest
private class AP105_Mercer_CEM_Test{
    /*
* @Description : Test method to provide  coverage to AP105_Mercer_CEM  - insert operation
*/  
    static testMethod void myUnitTest() {
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        
        Client_Engagement_Measurement_Interviews__c  cem = new Client_Engagement_Measurement_Interviews__c ();
        cem.Status__c = 'In Progress';
        cem.Overall_rating__c = '5';
        cem.Date_of_Review__c = system.today();
        
        Test.startTest();
        PageReference pageRef = Page.Mercer_CEM;  
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('accid', acc.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(cem); 
        AP105_Mercer_CEM controller = new AP105_Mercer_CEM(stdController);         
        controller.saveAndMore();
        controller.saveAndComplete();
        controller.insertTask();
        controller.Cancel();         
        Test.stopTest();               
    }
    /*
* @Description : Test method to provide  coverage to AP105_Mercer_CEM  - update operation
*/  
    static testMethod void unitTestUpdate() {
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        Client_Engagement_Measurement_Interviews__c  cem2 = mtdata.buildCEM();
        cem2 .Account__c = acc.id;
        cem2 .Status__c = 'In Progress';
        cem2 .Overall_rating__c = '5';
        insert(cem2);
        
        Test.startTest();
        PageReference pageRef = Page.Mercer_CEM;  
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put(ConstantsUtility.STR_ObjID, cem2.id);
        system.currentPageReference().getParameters().put(ConstantsUtility.STR_ACCID, acc.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(cem2); 
        AP105_Mercer_CEM controller = new AP105_Mercer_CEM(stdController);
        controller.mapQuewithAns();
        controller.saveAndComplete();
        controller.saveAndMore();
        controller.insertTask();
        controller.insertTasks(cem2);
        controller.Cancel();         
        Test.stopTest();    
        
        
    }
    /*
* @Description : Test method to provide  coverage to AP105_Mercer_CEM  - insert scenario2
*/  
    static testMethod void TestsaveMethod() {
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();       
        Client_Engagement_Measurement_Interviews__c  cem = new Client_Engagement_Measurement_Interviews__c ();
        cem.Status__c = 'In Progress';
        cem.Overall_rating__c = '5';
        cem.Date_of_Review__c = system.today();
        cem.Account__c = acc.Id;
        insert cem;        
        Test.startTest();
        PageReference pageRef = Page.Mercer_CEM;  
        Test.setCurrentPage(pageRef);
        //system.currentPageReference().getParameters().put('retURL', 'inContextOfRef:test');
        //ApexPages.currentPage().getParameters().put(ConstantsUtility.STR_ACCID, acc.id);     
        ApexPages.currentPage().getParameters().put(ConstantsUtility.STR_ObjID,cem.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(cem); 
        AP105_Mercer_CEM controller = new AP105_Mercer_CEM(stdController);
        controller.mapQuewithAns();
        Test.stopTest();
        
    }
    
    static testMethod void TestsaveMethod2() {
               
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        
        Client_Engagement_Measurement_Interviews__c  cem = new Client_Engagement_Measurement_Interviews__c ();
        cem.Status__c = 'In Progress';
        cem.Overall_rating__c = '5';
        cem.Date_of_Review__c = system.today();
        cem.Account__c = acc.Id;
        insert cem; 
        
        Test.startTest();
        PageReference pageRef = Page.Mercer_CEM;  
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('retURL', 'inContextOfRef');
        system.currentPageReference().getParameters().put('accid', acc.id);
        try{
        ApexPages.StandardController stdController = new ApexPages.StandardController(cem); 
        AP105_Mercer_CEM controller = new AP105_Mercer_CEM(stdController);  
        }catch(Exception ex){}
        Test.stopTest();
        
    }
    
    static testMethod void TestsaveMethod3() {
               
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        
        Client_Engagement_Measurement_Interviews__c  cem = new Client_Engagement_Measurement_Interviews__c ();
        //cem.Status__c = 'In Progress';
        //cem.Overall_rating__c = '5';
        //cem.Date_of_Review__c = system.today();
        //cem.Account__c = acc.Id;
        //insert cem; 
        
        Test.startTest();
        PageReference pageRef = Page.Mercer_CEM;  
        Test.setCurrentPage(pageRef);
        //system.currentPageReference().getParameters().put('retURL', 'inContextOfRef');
        ApexPages.currentPage().getParameters().put(ConstantsUtility.STR_ACCID, acc.id);
        try{
        ApexPages.StandardController stdController = new ApexPages.StandardController(cem); 
        AP105_Mercer_CEM controller = new AP105_Mercer_CEM(stdController);  
        controller.mapQuewithAns();
        }catch(Exception ex){}
        Test.stopTest();
        
    }
}