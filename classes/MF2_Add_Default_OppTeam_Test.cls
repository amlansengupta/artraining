@isTest(SeeAllData=True) 
public class MF2_Add_Default_OppTeam_Test {

static testMethod void test1()
    {   
     Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
       Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = Date.today();
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        
        testOppty.Buyer__c=testContact.Id;
        testOppty.AccountId=testAccount.Id;
     Test.startTest();
        insert testOppty;
      Test.stopTest();   
        ID  opptyId  = testOppty.id;
        List<UserTeamMember> lUt =[select CreatedById,CreatedDate,Id,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,OwnerId,SystemModstamp,TeamMemberRole,UserId FROM UserTeamMember ];
        if(!lUt.isEmpty()){
        User u= [select id from User where id=:UserInfo.getUserId()];
        system.runAs(u){
            User us= new User(Id=lUt.get(0).ownerId);
        us.isActive = true;
            us.Request__c = 'test';
            update us;
        
        }   
           testOppty.ownerId =lUt.get(0).ownerId;
            AP44_ChatterFeedReporting.FROMFEED = true;
            update testOppty;
        }
        ConstantsUtility.STR_ACTIVE='Inactive';
        MF2_Add_Default_OppTeam.addDefaultTeam(testOppty.Id);
       
        }
}