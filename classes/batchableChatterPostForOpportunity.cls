global class batchableChatterPostForOpportunity implements Database.Batchable<sObject> {
    String opp;
global Database.QueryLocator start(Database.BatchableContext bc) {
Date countDay5 = Date.Today().AddDays(-5);
    if(Test.isRunningTest()){
      opp = 'select Id, Name, OwnerId from Opportunity Where StageName=\'Pending Project\' and Chatter_Posted_for_5_days__c = False Limit 100';  
    }else{
        opp = 'select Id, Name, OwnerId from Opportunity Where Age_of_Pending_Project_Stage__c=5 and StageName=\'Pending Project\' and Chatter_Posted_for_5_days__c = False';
    }
    
    return Database.getQueryLocator(opp);
}

global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
   system.debug('%%%' +scope );
   chatterPostInOpportunity.chatterPostMethod((List <Opportunity>)scope);
}

global void finish(Database.BatchableContext bc) {

}
}