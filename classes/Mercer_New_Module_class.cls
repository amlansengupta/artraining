/**
* @Description
* This class is controller for Mercer_New_Module page which is override of New button 
* in Module object. 
**/
public with sharing class Mercer_New_Module_class {

    public Module__c objModule {get;set;}
    public String tcId {get;set;}
    public boolean renderModuleType {get;set;}
    
    //Constructor
    public Mercer_New_Module_class(ApexPages.StandardController controller) {
        objModule = new Module__c();
        tcId = System.currentPageReference().getParameters().get('contractId');
        if(tcId <>null){
            Talent_Contract__c objTc= [select id,name,Product__c from Talent_Contract__c where id=:tcId];
            objModule.talent_contract__c= objTc.id;
            renderModuleType = True;
            if(objTc.Product__c<>'Global Membership')
                renderModuleType = false; 
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.error, 'No assosiated Talent Contract!'));
        }
        
    }
    
    //Action method for Save
    public pageReference Save(){
        try{
            database.insert(objModule);
            return new PageReference('/'+objModule.id);
        }
        Catch(exception e){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.error,''+e.getMessage()));
            return null;
        }
    }
    
    //Action method for Save and New
    public pageReference SaveNew(){
        try{
            database.insert(objModule);
            String recId = objModule.id;
            return new PageReference('/'+recid.subString(0,3)+'/e?contractId='+tcId+'&retURL='+tcId);
        }
        Catch(exception e){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.error,''+e.getMessage()));
            return null;
        }
    }
    
    

}