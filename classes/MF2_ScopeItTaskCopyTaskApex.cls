/***************************************************************************************************************
User Story : US489
Purpose : This apex class created to as the controller of MF2_ScopeItTaskCopyTask vf page.
Created by : Soumil Dasgupta
Created Date : 28/12/2018
Project : MF2
****************************************************************************************************************/

public class MF2_ScopeItTaskCopyTaskApex {
    
    public String scopeItProjId {get;set;}
    
    public MF2_ScopeItTaskCopyTaskApex(ApexPages.StandardSetController controller){}
    
    public pagereference redirectToScopeItProject(){
        //Pagereference pageRef = new Pagereference('/lightning/r/ScopeIt_Project__c/'+scopeItProjId+'/view');
        Pagereference pageRef = new Pagereference('/'+scopeItProjId);
        return pageRef;
    }

}