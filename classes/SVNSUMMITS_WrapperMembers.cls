/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

/*
    @Class Name          : SVNSUMMITS_WrapperMembers
    @Created by          :
    @Description         : Wrapper Apex class used by SVNSUMMITS_MemberController Apex class
                           Mainly this class was created for implementing pagination using setcontroller.
*/

global with sharing class SVNSUMMITS_WrapperMembers {

    @AuraEnabled
    global List<User> membersList {
        get;
        set;
    }

    @AuraEnabled
    global Map<Id, SVNSUMMITS_WrapperNetwork> mapUserId_Wrapper {
        get;
        set;
    }

    @AuraEnabled
    global Integer listSizeValue {
        get;
        private set;
    }

    //Total number of result (records) retured in query
    @AuraEnabled
    global Integer totalResults {
        get;
        set;
    }

    //Total number of pages in pagination as per total results
    @AuraEnabled
    global Integer totalPages {
        get;
        set;
    }

    //Page number for all total number of pages
    @AuraEnabled
    global Integer pageNumber {
        get;
        set;
    }

    //Boolean to check is there previous functionality to show previous button on component
    @AuraEnabled
    global Boolean hasPreviousSet {
        get;
        set;
    }

    //Boolean to check is there nxet functionality to show next button on component
    @AuraEnabled
    global Boolean hasNextSet {
        get;
        set;
    }

    @AuraEnabled
    global ApexPages.StandardSetController setController {
        get;
        set;
    }


    /*
        @Name          :  SVNSUMMITS_WrapperMembers
        @parameters    :  QUERY(query string to query records), intLimit is the limit to query
        @Description   :  constructor for wrapper
    */
    global SVNSUMMITS_WrapperMembers(String QUERY, Integer intLimit, List<String> lstmemberNicKNames, Set<String> setUserId, Boolean nullifySSC, Boolean isFeatured) {

        try {
            this.membersList = new List<User>();

            Boolean isSortByCreateDate = true;

            //set limit to query
            this.listSizeValue = intLimit;

            system.debug('Wrapper Query: ' + QUERY);

            //Query news records as per query string from parameters
            this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(QUERY));

            //setting page size for position based on listSizeValue
            this.setController.setPageSize(listSizeValue);

            //call updateControllerAttributes method
            updateControllerAttributes(isFeatured, lstmemberNicKNames);

            if (nullifySSC) {
                this.setController = null;
            }
        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

    /*
        @Name          :  nextPage
        @Description   :  used in pagination on click on next button
    */
    global void nextPage() {

        try {
            //set page number for next page in pagination
            this.setController.setpageNumber(this.pageNumber + 1 > 0 ? this.pageNumber + 1 : 1);


            //call updateControllerAttributes method
            updateControllerAttributes(false, null);
        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

    /*
        @Name          :  previousPage
        @Description   :  used in pagination on click on previous button
    */
    global void previousPage() {
        try {
            //set page number for previous page in pagination
            this.setController.setpageNumber(this.pageNumber - 1 > 0 ? this.pageNumber - 1 : 1);

            //call updateControllerAttributes method
            updateControllerAttributes(false, null);
        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

    /*

        @Name          :  updateWrapperContent
        @Description   :  used for making data for the wrapper class which will display the values associated with
                          the user like likes recieved, follower count and knowledge topics
    */
    private void updateWrapperContent(List<User> lstUser) {
        try {
            mapUserId_Wrapper = new Map<Id, SVNSUMMITS_WrapperNetwork>();

            for (User objUser: lstUser) {

                SVNSUMMITS_WrapperNetwork objSVNSUMMITS_WrapperNetwork = new SVNSUMMITS_WrapperNetwork(objUser);
                mapUserId_Wrapper.put(objUser.Id, objSVNSUMMITS_WrapperNetwork);

            }

            getFollowersCount(mapUserId_Wrapper);

        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

    /*
        @Name          :    getFollowersCount
        @Description   :    used for fetching the number of followers
    */
    private void getFollowersCount(Map<Id, SVNSUMMITS_WrapperNetwork> mapUserId_Wrapper) {

        try {
            //We can query maximum 100 records for Entity subscription.
            //https://help.salesforce.com/apex/HTViewSolution?id=000181404&language=en_US

            for (AggregateResult results : [
                    SELECT Count(Id) countNumber, ParentId
                    FROM EntitySubscription
                    WHERE NetworkId = :Network.getNetworkId()
                    AND ParentId = :mapUserId_Wrapper.keySet()
                    group by ParentId
                    LIMIT 100
            ]) {

                String userId = String.valueOf(results.get('ParentId'));

                if (mapUserId_Wrapper.containsKey(userId)) {

                    mapUserId_Wrapper.get(userId).intNumberOfFollowers = Integer.valueOf(results.get('countNumber'));

                }
            }
            getLikesCount(mapUserId_Wrapper);

        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }


    /*
        @Name          :    getLikesCount
        @Description   :    used for fetching the number of likes received

    */
    private void getLikesCount(Map<Id, SVNSUMMITS_WrapperNetwork> mapUserId_Wrapper) {
        try {
            for (ChatterActivity results : [
                    Select ParentId, LikeReceivedCount
                    FROM ChatterActivity
                    WHERE NetworkId = :Network.getNetworkId()
                    AND ParentId = :mapUserId_Wrapper.keySet()
            ]) {

                if (mapUserId_Wrapper.containsKey(results.ParentId)) {
                    mapUserId_Wrapper.get(results.ParentId).intLikeReceived = results.LikeReceivedCount;
                }
            }

            getKnowledgeableTopics(mapUserId_Wrapper);

        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

    /*
        @Name          :    getKnowledgeableTopics
        @Description   :    used for fetching the knowledge topics related to user

    */
    private void getKnowledgeableTopics(Map<Id, SVNSUMMITS_WrapperNetwork> mapUserId_Wrapper) {
        try {

            Map<Id, String> mapTopicId_Name = new Map<Id, String>();
            Map<Id, Set<Id>> mapUserId_SetTopicId = new Map<Id, Set<Id>>();

            // Fetching the topic records.
            for (Topic objTopic : [
                    SELECT Id, Name
                    FROM Topic
                    WHERE NetworkId = :Network.getNetworkId()
            ]) {

                mapTopicId_Name.put(objTopic.Id, objTopic.Name);
            }
        }
        catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }

     /*
        @Name          :  updateControllerAttributes
        @parameters    :  isFeatured boolean , featurdNewsIds map
        @Description   :  used to set attributes of wrapper so that it can be used on components with proper data
    */
    private void updateControllerAttributes(Boolean isFeatured, List<String> lstmemberNicKNames) {

        try {
            if (isFeatured) {
                List<User> templist = this.setController.getRecords();
                Map<String, User> tempUserMap = new Map<String, User>();

                for (User cmember: templist) {

                    tempUserMap.put(cmember.CommunityNickname, cmember);

                }

                for (string str : lstmemberNicKNames) {
                    if (tempUserMap.containsKey(str)) {
                        this.membersList.add(tempUserMap.get(str));
                    }
                }
            } else {
                this.membersList = this.setController.getRecords();
            }

            //Call wrapper for intialization
            updateWrapperContent(this.membersList);

            //set totalResults from query result
            this.totalResults = this.setController.getResultSize();

            //set totalPages as per totalResults and page size
            this.totalPages = Math.mod( this.setController.getResultSize(),this.setController.getPageSize()) == 0 ?
                                        this.setController.getResultSize()/this.setController.getPageSize() : this.setController.getResultSize()/this.setController.getPageSize()+1;


            //set pageNumber as per totalPages
            this.pageNumber = this.totalPages > 0 ? this.setController.getPageNumber() : 0;

            //set hasPreviousSet from getHasPrevious of set controller
            this.hasPreviousSet = this.setController.getHasPrevious();

            //set hasNextSet from getHasNext of set controller
            this.hasNextSet = this.setController.getHasNext();
        } catch (exception e) {
            system.debug('*** Exception ***' + e.getMessage());
        }
    }
}