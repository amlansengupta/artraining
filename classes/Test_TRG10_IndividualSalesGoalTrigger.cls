/*Purpose: Test Class for providing code coverage to TRG10_IndividualSalesGoalTrigger trigger
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   02/05/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_TRG10_IndividualSalesGoalTrigger 
{

/*Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    }*/
   /*
    * Test Data for user creation
    */
    private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
     //createCS();
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890'+i;
        insert testUser;
        System.runAs(testUser){
        
        }
        return testUser;
        
    }
    /*
     * @Description : Test method to provide data coverage to TRG10_IndividualSalesGoalTrigger trigger(Scenerio 1)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest1() 
    {
        //createCS();
       
        User tUser = new User();
        User tUser1 = new User();
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = '11111';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        Test.startTest();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
        
        User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
        

        
        
        system.runas(tUser1){
        try
        {
            Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
            iSalesGoal1.Sales_Goals__c = 500;
            //iSalesGoal1.Employee_ID__c = '12345678903';
            iSalesGoal1.Colleague__c = testColleague1.id;
            iSalesGoal1.OwnerId = tu.Id;
            insert iSalesGoal1;
        }catch(DMLException e){
            system.assertEquals(True, e.getMessage().contains('Sales Goal for this Owner already exists'));
        }
        }
        Test.stopTest();
    }
    /*
     * @Description : Test method to provide data coverage to TRG10_IndividualSalesGoalTrigger trigger(Scenerio 2)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest2() 
    {
        Test.startTest();
       // createCS();
        User tUser = new User();
        User tUser1 = new User();
        
         Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = '11111';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        
       
        Test.stopTest();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
        
        User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
        User tu1 = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678903' limit 1];
        System.runas(tUser1){
        try
        {
            Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
            iSalesGoal.Sales_Goals__c = 500;
            iSalesGoal.Colleague__c = testColleague.id;
            iSalesGoal.OwnerId = tu.Id;
            insert iSalesGoal;
            
            Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
            iSalesGoal1.Sales_Goals__c = 500;
            iSalesGoal.Colleague__c = testColleague1.id;
            iSalesGoal1.OwnerId = tu1.Id;
            insert iSalesGoal1;
            
            Individual_Sales_Goal__c iSalesGoal2 = [select Id,  OwnerId from Individual_Sales_Goal__c where OwnerId = :tu1.Id limit 1];
            isalesGoal2.OwnerId = tu.Id;
            update iSalesGoal2;
        }catch(DMLException e){
            system.assertEquals(True, e.getMessage().contains('Sales Goal for this Owner already exists'));
        }
        }
    }
    
    /*
     * @Description : Test method to provide data coverage to TRG10_IndividualSalesGoalTrigger trigger(Scenerio 3)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest3()
    {    
        //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = '11111';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;

    
       
       
       User tUser = new User();
        User tUser1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
        
        User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
        User tu1 = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678903' limit 1];
        
         
       User user1 = new User();
       String adminProfile =Mercer_TestData.getsystemAdminUserProfile();   
       user1 = Mercer_TestData.createUser1(adminProfile, 'usert2', 'usrLstName2', 2);   
        System.runAs(user1){    
             
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
         
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //request id:12638;commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601;replacing stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Birmingham - Brindleyplace';
        Test.startTest();
        insert testOppty;
        Test.stopTest();
        system.debug('\n pbEntry currencycode:'+testOppty.closedate);
       
        
     /*   Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague.Id;
        testSales.Percent_Allocation__c = 10;
        insert testSales; */
        
        
        Sales_Credit__c testSales1 = new Sales_Credit__c();
        testSales1.Opportunity__c = testOppty.Id;
        testSales1.EmpName__c = testColleague1.Id;
        testSales1.Percent_Allocation__c = 10;
        insert testSales1;
        
        Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.OwnerId = tu.Id;
        insert iSalesGoal;
        
        
        //Individual_Sales_Goal__c iSalesGoal1 = [select Id,  OwnerId from Individual_Sales_Goal__c where OwnerId = :tu.Id limit 1];
        iSalesGoal.OwnerId = tu1.Id;
        
        update iSalesGoal;
        
        }
         
    }
    /*
     * @Description : Test method to provide data coverage to TRG10_IndividualSalesGoalTrigger trigger(Scenerio 4)
     * @ Args       : Null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest4()
    {
       // createCS();
        Colleague__c testColleague5 = new Colleague__c();
        testColleague5.Name = 'TestColleague9';
        testColleague5.EMPLID__c = '12345678902';
        testColleague5.LOB__c = '11119';
        testColleague5.Last_Name__c = 'TestLastName6';
        testColleague5.Empl_Status__c = 'Active';
        testColleague5.Email_Address__c = 'abc8@abc.com';
        insert testColleague5;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = 'Employee Health & Benefits';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        testColleague1.Country__c = 'USA';
        insert testColleague1;

    
        User tUser = new User();
        User tUser1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
        
        User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
        
        //Test.startTest();
             
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague5.Id;
        testAccount.One_Code__c = '1234';
        //Test.startTest();
        insert testAccount;
        
        AP44_ChatterFeedReporting.FROMFEED=true;
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //request id:12638;commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601;replacing stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Birmingham - Brindleyplace';
        //Test.startTest();
        insert testOppty;
        
         system.debug('\n pbEntry currencycode:'+testOppty.closedate);
       
        
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague5.Id;
        testSales.Percent_Allocation__c = 10;
        
        insert testSales;
        
        try{
        Sales_Credit__c testSales1 = new Sales_Credit__c();
        testSales1.Opportunity__c = testOppty.Id;
        testSales1.EmpName__c = testColleague1.Id;
        testSales1.Percent_Allocation__c = 110;
        testSales1.Role__c= 'Sales Professional';
        insert testSales1;
        }catch(Exception e){}
        
        
        try{
        testSales.EmpName__c = testColleague1.Id;
        testSales.Percent_Allocation__c = 110;
        testSales.Role__c= 'Sales Professional';
        update testSales;
        }catch(Exception e){}
        
        Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.OwnerId = tu.Id;
        System.runAs(tUser1){
        Test.starttest(); 
        insert iSalesGoal;
        Test.stopTest();
        
        } 
            
    }
    static testMethod void myUnitTest5()
    {
       // createCS();
        Test.startTest();
        User tUser = new User();
        
        Colleague__c testColleague5 = new Colleague__c();
        testColleague5.Name = 'TestColleague9';
        testColleague5.EMPLID__c = '12345678902';
        testColleague5.LOB__c = '11119';
        testColleague5.Last_Name__c = 'TestLastName6';
        testColleague5.Empl_Status__c = 'Active';
        testColleague5.Email_Address__c = 'abc8@abc.com';
        insert testColleague5;
        
       
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        Test.stopTest();
        User tu = [select Id, EmployeeNumber, Goals__c from User where EmployeeNumber = '12345678902' limit 1];
        
        Individual_Sales_Goal__c iSalesGoal;
        System.runAs(tUser)
        {
        iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Colleague__c = testColleague5.id;
        iSalesGoal.OwnerId = tu.Id;
        iSalesGoal.Goal_Year__c = '2016';
        insert iSalesGoal;
        delete iSalesGoal;
        }
        //Individual_Sales_Goal__c iSalesGoal = [select Id,  OwnerId from Individual_Sales_Goal__c where OwnerId = :tu.Id limit 1];
        //isalesGoal.OwnerId = tu.Id;
        
        
        User goals = [select Id, Goals__c from User where Id = :tu.Id];
        //goals.Goals__c = '';
        //update goals;
        
        
        System.assertEquals(goals.Goals__c,NULL);
        
        }
       
       static testMethod void myUnitTest6()
    {
       // createCS();
        Test.startTest();
        User tUser = new User();
        User tUser1 = new User();
        
        Colleague__c testColleague5 = new Colleague__c();
        testColleague5.Name = 'TestColleague9';
        testColleague5.EMPLID__c = '12345678902';
        testColleague5.LOB__c = '11119';
        testColleague5.Last_Name__c = 'TestLastName6';
        testColleague5.Empl_Status__c = 'Active';
        testColleague5.Email_Address__c = 'abc8@abc.com';
        insert testColleague5;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = '11111';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
       
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
        
        Test.stopTest();
        User tu = [select Id, EmployeeNumber, Goals__c from User where EmployeeNumber = '12345678902' limit 1];
        User tu1 = [select Id, EmployeeNumber, Goals__c from User where EmployeeNumber = '12345678903' limit 1];
        
        
        System.runAs(tUser)
        {
        Individual_Sales_Goal__c iSalesGoal;
        iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Revenue_Growth_Target__c = 500;
        iSalesGoal.Colleague__c = testColleague5.id;
        iSalesGoal.OwnerId = tu.Id;
        iSalesGoal.Goal_Year__c = '2016';
        insert iSalesGoal;
        }
        System.runAs(tUser1)
        {
        Individual_Sales_Goal__c iSalesGoal2;
        iSalesGoal2 = new Individual_Sales_Goal__c();
        iSalesGoal2.Revenue_Growth_Target__c = 500;
        iSalesGoal2.Colleague__c = testColleague1.id;
        iSalesGoal2.OwnerId = tu.Id;
        iSalesGoal2.Goal_Year__c = '2016';
        insert iSalesGoal2;
        }
        }
       
}