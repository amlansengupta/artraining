/*Purpose:  Autogenerate task for CEM record.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   11/07/2014   As per december Release 2014(Request #2264) ,created class to autogenerate task for CEM record.
============================================================================================================================================== 
*/
public without sharing class AP122_CEMTriggerUtil{
private static final String EXCEPTION_STR = 'Exception occured with reason :' ;
 /*
       * @Description : Method to autogenerate Task for CEM record 
       * @ Args       : List<Client_Engagement_Measurement_Interviews__c>,Map<Id,Client_Engagement_Measurement_Interviews__c>
       * @ Return     : void
   */  
public static void createTask(List<Client_Engagement_Measurement_Interviews__c> triggernew,Map<Id,Client_Engagement_Measurement_Interviews__c> triggeroldMap){
    List<Task> tasklist = new List<Task>();
    Set<Id> lstInterviwer = new Set<Id>();
    set<Id> lstinterviewer2 = new set<Id>();
    Map<String,User> ActiveuserMap = new Map<String,User>();
    Map<String,User> userMapinterviewer2 = new Map<String,User>();
    Map<Id,String> colleaguemap = new Map<Id,String>();
    Map<Id,String> Colleagueinterviwer2map = new Map<Id,String>();
    set<id> AccountIds = new set<Id>();
    Map<Id,Account> AccountMap;
    
    for(Client_Engagement_Measurement_Interviews__c  c : triggernew){
        lstInterviwer.add(c.Reviewer__c);
        lstinterviewer2 .add(c.CEM_Interviewer_2__c);
        AccountIds.add(c.Account__c);  
    }
    
    if(!lstInterviwer.isEmpty()){
        for(Colleague__c  col :[select id,EMPLID__c from Colleague__c where id in :lstInterviwer]){
            colleaguemap.put(col.id,col.EMPLID__c );
        }
    }
    
    if(!lstinterviewer2.isEmpty()){
        for(Colleague__c  col :[select id,EMPLID__c from Colleague__c where id in :lstinterviewer2]){
            Colleagueinterviwer2map .put(col.id,col.EMPLID__c );
        }    
    }
    
    if(!colleaguemap.isEmpty()){
        for(user u :[Select id, IsActive,EmployeeNumber from user where EmployeeNumber in :colleaguemap.values() and IsActive=true]){
            if(!ActiveuserMap.containskey(u.EmployeeNumber)){
                ActiveuserMap.put(u.EmployeeNumber ,u);
            }
        }    
    }
    
    if(!Colleagueinterviwer2map.isEmpty()){
        for(user u :[Select id, IsActive,EmployeeNumber from user where EmployeeNumber in :Colleagueinterviwer2map.values() and IsActive=true]){
            if(!userMapinterviewer2.containskey(u.EmployeeNumber)){
                userMapinterviewer2.put(u.EmployeeNumber ,u);
            }
        }    
    }
    if(ActiveuserMap.isEmpty() && userMapinterviewer2.isEmpty()){
      AccountMap  = new  Map<Id,Account>([select id,ownerId from Account where id in :AccountIds]);
    }    
    for(Client_Engagement_Measurement_Interviews__c  c : triggernew){
        if((trigger.isinsert && c.Status__c == ConstantsUtility.STR_STATUS && c.Overall_rating__c<=ConstantsUtility.STR_RATING && c.Overall_rating__c!=null ) || 
            (trigger.isupdate && c.Status__c == ConstantsUtility.STR_STATUS && c.Overall_rating__c<=ConstantsUtility.STR_RATING && c.Overall_rating__c!=null &&
             triggeroldMap.get(c.id).Status__c!=ConstantsUtility.STR_STATUS )){

            Task taskrec = new Task();
            taskrec.subject =ConstantsUtility.STR_SUB;
            taskrec.ActivityDate=System.today().addMonths(6);
            taskrec.Status = ConstantsUtility.STR_TASKSTATUS;
            taskrec.Priority =ConstantsUtility.STR_PRIORITY;
            taskrec.Description =Label.TaskComments_CEM;
            if(colleaguemap.containskey(c.Reviewer__c) && !ActiveuserMap.isEmpty() ){
                taskrec.ownerId = ActiveuserMap.get(colleaguemap.get(c.Reviewer__c)).id;
            }
            else if(Colleagueinterviwer2map.containskey(c.CEM_Interviewer_2__c) && ActiveuserMap.isEmpty() && !userMapinterviewer2.isEmpty()){
                taskrec.ownerId = userMapinterviewer2.get(Colleagueinterviwer2map.get(c.CEM_Interviewer_2__c)).id;
 
            }
            else{
                taskrec.ownerId = AccountMap.get(c.Account__c).ownerId;
            }
            taskrec.whatId = c.Account__c;
            taskrec.ReminderDateTime = System.Now().addMonths(1);
            taskrec.IsReminderSet = true;
            
            tasklist.add(taskrec);
            }
        
        }
        if(!tasklist.isEmpty()){
            try{    
                insert tasklist;
            }catch(Exception e){
                for(Integer i =0; i< tasklist.size(); i++) {                  
                tasklist[i].addError(EXCEPTION_STR+e.getMessage());
            } 
            }   
    }





}


}