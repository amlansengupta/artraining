@IsTest
private class Peak_ContentObjectTest {
    @isTest
    public static void testPeak_ContentObject() {

        Test.startTest();
        // Insert a User and get ID so we have a valid ID string
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User user = peakTestUtils.createStandardUser();
        insert user;

        Peak_ContentObject peakContentObject = new Peak_ContentObject();
        peakContentObject.contentID = user.Id;
        peakContentObject.title = Peak_TestConstants.FIRSTNAME;
        peakContentObject.description = Peak_TestConstants.TEST_DESCRIPTION;
        peakContentObject.fullDescription = Peak_TestConstants.TEST_DESCRIPTION;
        peakContentObject.attachments = new List<Attachment>();
        peakContentObject.url = Peak_TestConstants.TEST_URL;
        peakContentObject.featured = false;
        peakContentObject.bannerImage = '';
        peakContentObject.avatar = '';
        peakContentObject.commentCount = 1;
        peakContentObject.commentUrl = '';

        system.assertEquals(peakContentObject.title,Peak_TestConstants.FIRSTNAME);
        Test.stopTest();
    }

}