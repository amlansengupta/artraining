/***************************************************************************************************************
User Story : US518
Purpose : This class is created as the test clas of MF2_ScopeItProjectSaveAsTemplateApex.
Created by : Soumil Dasgupta
Created Date : 25/12/2018
Project : MF2
****************************************************************************************************************/
@isTest(seeAllData = false)
public class MF2_ScopeItProjectSaveAsTemplateApexTest {

    public static testmethod void saveAsTempTest(){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        //Account test data creation
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        String accID = testAccount1.ID;
        
        //Opportunity test data creation
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  accID; 
        //#12638 sprint 2 dependency
        //testOpportunity.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        //#12638 sprint 2 dependency
        testOpportunity.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(2018, 1, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        testOpportunity.Close_Stage_Reason__c ='Other';
        insert testOpportunity;
        Id ScopeOpportunityId = testOpportunity.ID;        
        
        
        //ScopeIt project test data creation
        ScopeIt_Project__c testscopeitProject = new ScopeIt_Project__c();
        testscopeitProject.name = 'testscopeitproj';
        //testscopeitProject.Product__c =  prodId;
        //testscopeitProject.Bill_Type__c = 'Billable';                
        testscopeitProject.opportunity__c = ScopeOpportunityId;
        //testscopeitProject.OpportunityProductLineItem_Id__c = oliId;
        //testscopeitProject.CurrencyIsoCode = oliCurr;
        //testscopeitProject.Sales_Price__c=oliUnitprice;
        insert testscopeitProject;
        ID scopeItProjId = testscopeitProject.id;
        List<ScopeIt_Project__c> lstScopeItProj = new List<ScopeIt_Project__c>();
        lstScopeItProj.add(testscopeitProject);
        
        System.runAs(user1) {
            ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(lstScopeItProj);
            MF2_ScopeItProjectSaveAsTemplateApex controller = new MF2_ScopeItProjectSaveAsTemplateApex(stdController);
            
            controller.redirectToOpp();
            system.assertequals(testscopeitProject.opportunity__c,ScopeOpportunityId);
            
            MF2_ScopeItImportFromTemplateApex importFromController  = new MF2_ScopeItImportFromTemplateApex(stdController);
			importFromController.redirectToOpp();            
        }
    }
    
    public static testmethod void addEmployee(){
        ScopeIt_Task__c scopeItTask = new ScopeIt_Task__c();
        List<ScopeIt_Task__c> selected = new List<ScopeIt_Task__c>();
        selected.add(scopeItTask);
        ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(selected);
        MF2_ScopeItTaskAddEmployeesController controller = new MF2_ScopeItTaskAddEmployeesController(stdController);
    }
}