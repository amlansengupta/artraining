/**
 * Created by aaronwalker on 5/12/17.
 */

@isTest
public with sharing class SVNSUMMITS_WrapperNewsTest {

    @isTest
    public static void netMemGet(){
        String field = 'FieldName';
        String errorMsg = 'ErrorMsg';
        SVNSUMMITS_WrapperNews result = new SVNSUMMITS_WrapperNews(field, errorMsg);

        system.assertEquals(result.netMem, null);
    }

    @isTest
    public static void constructorTest(){
        String field = 'FieldName';
        String errorMsg = 'ErrorMsg';
        SVNSUMMITS_WrapperNews result = new SVNSUMMITS_WrapperNews(field, errorMsg);

        System.assertEquals(field, result.field);
        System.assertEquals(errorMsg, result.errorMsg);
    }

}