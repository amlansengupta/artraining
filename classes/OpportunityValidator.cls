/***************************************************************************************************************
Request Id : 17080
Purpose : This apex class is created to display alert messages on To Do Tile.
Created by : Archisman Ghosh
Created Date : 22/01/2019
Project : MF2
****************************************************************************************************************/

public class OpportunityValidator {
    private static final String PRODUCT_TYPE='Product';
    private static final String MARKETING_TYPE='Marketing Code Message';
    private static final String BUYER_TYPE='Buyer';
    private static final String WEBCAS_TYPE='WebCAS project code';
    private static final String SALES_TYPE='Sales Credit';
    private static final String REVENUE_TYPE='Revenue reallocation';
    private static final String GLOBAL_TYPE='Global Contract message';
    private static final String SCOPE_TYPE='ScopeIt';
    
    /*
*  @Method Name: validateOpportunity
*  @Description: Method gets Opportunity and OpportunityLineItems fields
*  @Args : List 
*/
    @AuraEnabled
    public static List<Opportunity_Alert__mdt> validateOpportunity(String recordId) {
        List<Opportunity_Alert__mdt> alertMessages=new List<Opportunity_Alert__mdt>();
        try{
            //To fetch the fields of the Custom Meta Data Type
            List<Opportunity_Alert__mdt> listTypes = [SELECT Type__c,Message__c,Link__c from Opportunity_Alert__mdt ORDER BY Order__c];  
            for(Opportunity_Alert__mdt obj : listTypes)
            {
                system.debug('xx'+obj.Type__c);
            }
            Opportunity opp=null;
            String scopeId;
            //To fetch ScopeIt project Id
            List<ScopeIt_Project__c> listScopeId=[select Id from ScopeIt_Project__c where Opportunity__c=:recordId limit 1];
            if(listScopeId!=null && listScopeId.size()>0)
            {
                scopeId=listScopeId.get(0).Id;
            }
            //To fetch all the Opportunity fields related to To Do Tile
            List<Opportunity> listFields = [SELECT StageName,Account.Type,Type,Buyer__c,Opportunity_Country__c,Global_Contract__c,Global_Rate_Card__c,Global_Client_Strategy__c,Global_Special_Terms__c,Global_Client_Governance__c,Other_Global_Agreement__c,
                                            Total_of_Allocation__c,Product_LOBs__c,OneCode__c,Total_Scopable_Products_Revenue_USD__c,Opportunity_Region__c,Calc_Scope_Completed__c,
                                            CloseDate,IsRevenueDateAdjusted__c,(select id,Project_Linked__c,Is_Project_Required__c from OpportunityLineItems) from Opportunity where id =:recordId];
            //system.debug('xyz'+listFields);
            String OneCode = System.Label.One_Code_List;
            List<String> listOneCode = OneCode.split(';');
            if(listFields!=null && listFields.size()>0)
            {
                opp=listFields.get(0);
            }
            if(opp !=null)
            {
                boolean productIsNotProjectLinked=false;
                if(opp.OpportunityLineItems!=null)
                {
                    for(OpportunityLineItem li : opp.OpportunityLineItems)
                    {
                        if(li.Is_Project_Required__c==true && li.Project_Linked__c==false)
                        {
                            productIsNotProjectLinked=true;
                            break;
                        }
                    }
                }
                
                for(Opportunity_Alert__mdt obj : listTypes ) 
                {
                    
                    if(obj.Type__c.equalsIgnoreCase(PRODUCT_TYPE))
                    {
                        String stageNamesForProduct = System.Label.oppStageForProduct;
                        List<String> liststageNamesForProduct = stageNamesForProduct.split(',');
                        
                        if(opp.OpportunityLineItems!=null && opp.OpportunityLineItems.size()==0 
                           && (liststageNamesForProduct.contains(opp.StageName))){
                               alertMessages.add(obj);
                           }
                    }
                    if(obj.Type__c.equalsIgnoreCase(MARKETING_TYPE))
                    {
                        String stageNamesForMarket = System.Label.oppStageForMarket;
                        List<String> liststageNamesForMarket = stageNamesForMarket.split(',');
                        
                        if(opp.Account.Type.equalsIgnoreCase('Marketing') && liststageNamesForMarket.contains(opp.StageName)){
                            alertMessages.add(obj);
                        }
                    }
                    if(obj.Type__c.equalsIgnoreCase(BUYER_TYPE))
                    {
                        String stageNamesForBuyer = System.Label.oppStageForBuyer;
                        List<String> liststageNamesForBuyer = stageNamesForBuyer.split(',');
                        
                        if(opp.Buyer__c==null && (liststageNamesForBuyer.contains(opp.StageName)) && (!listOneCode.contains(opp.OneCode__c)) 
                           && (!opp.Opportunity_Country__c.equalsIgnoreCase('Korea'))){
                               alertMessages.add(obj);
                           }
                    }
                    if(obj.Type__c.equalsIgnoreCase(WEBCAS_TYPE))
                    {
                        String stageNamesForWebCAS = System.Label.oppStageForWebCAS;
                        List<String> liststageNamesForWebCAS = stageNamesForWebCAS.split(',');
                        
                        if(productIsNotProjectLinked==true && liststageNamesForWebCAS.contains(opp.StageName))
                        {
                            alertMessages.add(obj);
                        }
                    }
                    if(obj.Type__c.equalsIgnoreCase(SALES_TYPE))
                    {
                        String stageNamesForSales = System.Label.OppStageForSales;
                        List<String> liststageNamesForSales = stageNamesForSales.split(',');
                        List<String> listProductLob;
                        if(opp.Product_LOBs__c!=null)
                        {
                            listProductLob = opp.Product_LOBs__c.split(';');
                        }
                        
                        if((!opp.Type.equalsIgnoreCase('Rebid')||(opp.Type.equalsIgnoreCase('Rebid')
                                                                  && listProductLob!=null && listProductLob.contains('Career')))
                           && (liststageNamesForSales.contains(opp.StageName) 
                               && opp.Total_of_Allocation__c==0)){
                                   alertMessages.add(obj);
                               }
                    }
                    
                    if(obj.Type__c.equalsIgnoreCase(REVENUE_TYPE))
                    {                 
                        String stageNamesForRevenue = System.Label.OppStageForRevenue;
                        List<String> liststageNamesForRevenue = stageNamesForRevenue.split(',');
                        
                        if((liststageNamesForRevenue.contains(opp.StageName)) 
                           && opp.IsRevenueDateAdjusted__c
                           && (opp.CloseDate!=null && (System.today().year()==opp.CloseDate.year() 
                                                       && System.today().month()==opp.CloseDate.month()))){
                                                           alertMessages.add(obj);
                                                       }
                    }
                    if(obj.Type__c.equalsIgnoreCase(GLOBAL_TYPE))
                    {
                        String stageNamesForGlobal = System.Label.OppStageForGlobal;
                        List<String> liststageNamesForGlobal = stageNamesForGlobal.split(',');
                        
                        if((opp.Global_Contract__c!= null || opp.Global_Client_Strategy__c!= null || opp.Global_Rate_Card__c!= null || opp.Global_Special_Terms__c!= null || opp.Global_Client_Governance__c!= null || opp.Other_Global_Agreement__c!= null) 
                           && (liststageNamesForGlobal.contains(opp.StageName))){
                               alertMessages.add(obj);
                           }
                    }
                    if(obj.Type__c.equalsIgnoreCase(SCOPE_TYPE))
                    {
                        String stageNamesForScope= System.Label.OppStageForScope;
                        List<String> liststageNamesForScope = stageNamesForScope.split(',');
                        
                        if(liststageNamesForScope.contains(opp.StageName) && ((opp.Opportunity_Region__c.equalsIgnoreCase('International') && opp.Total_Scopable_Products_Revenue_USD__c>=1000000000) 
                                                                              || ((opp.Opportunity_Region__c.equalsIgnoreCase('North America')||opp.Opportunity_Region__c.equalsIgnoreCase('US & Canada')) && opp.Total_Scopable_Products_Revenue_USD__c>=1000000000)) && opp.Calc_Scope_Completed__c==false)
                        {
                            obj.Link__c='/lightning/r/ScopeIt_Project__c/'+scopeId+'/view';
                            alertMessages.add(obj);
                        }
                    }
                } 
            }
            
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'OpportunityValidator', 'validateOpportunity');
            new AuraHandledException('Opportunity Alerts failed! Please try after sometime.');
        }
        return alertMessages;
    }
}