/***************************************************************************************************************
User Story : US492
Purpose : This test class is created for the MF2_ContactDistributionEditAllApex class.
Created by : Soumil Dasgupta
Created Date : 17/12/2018
Project : MF2
****************************************************************************************************************/

@isTest(SeeAllData = False)
public class MF2_ContactDistributionsEditAllApexTest {

            
    public static testmethod void editAllTestPositive(){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        String accID = testAccount1.ID;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test123@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId =  accID;
        insert testContact;
       
                
        System.runAs(user1) {
        	ApexPages.StandardController stdController = new ApexPages.Standardcontroller(testContact);
            MF2_ContactDistributionsEditAllApex controller = new MF2_ContactDistributionsEditAllApex(stdController);
        }
    }
}