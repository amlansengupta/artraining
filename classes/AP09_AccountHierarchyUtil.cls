/*Purpose: This Class is a utility class for the Account Hierarchy Page. 
           It gets the child account and intermediate account for the parent account record based on Global ID 
           and also the opportunities and contacts related to those accounts.
==============================================================================================================================================
The methods called perform following functionality:

•   Methods are called in the extension Controller 
•   Get Parent Account 
•   Get Child Records
•   Get Intermediate Accounts
•   Returns List Of Opportunities
•   Returns List of Contacts

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   12/26/2012  Created Controller Class
============================================================================================================================================== 
*/
public class AP09_AccountHierarchyUtil {
    
    // map variable of string and list of global account records
    private static Map<String, List<Account>> globalIdAccountListMap = new Map<String, List<Account>>();
    
    // map variable of string and list of intermediate account records
    private static Map<String, List<Account>> intermediateIdAccountListMap = new Map<String, List<Account>>();
    
    // map variable of string and list of account records according to GOC Id
    private static Map<String, List<Account>> gocIdAccountListMap = new Map<String, List<Account>>();
    
    // set variable to store intermediate account Ids
    private static Set<String> intermediateIdSet = new Set<String>();
    
    // list variable to store opportunities
    private static List<Opportunity> opportunities = new List<Opportunity>();
    
    // list variable to store contacts
    private static List<Contact> contacts = new List<Contact>();
    
     /*
     * @Description : This method is called in the extension Controller 
     * @ Args       : Global Id
     * @ Return     : Map of string and list of accounts
     */
    public static Map<String, List<Account>> getParentAccounts(String globalId)
    {
        if(globalIdAccountListMap.size() == 0)
        {
            for(Account account : [select Id,Name,GBL_ID__c,ACCT_ID__c,GOC_ID__c, INT_ID__c
                                   FROM Account 
                                   WHERE (GBL_ID__c <> null 
                                   AND ACCT_ID__c=:globalId) 
                                   ORDER BY GBL_ID__c])
            {
                // if account is parent account
                if(globalIdAccountListMap.containskey(account.GBL_ID__c))
                {
                    globalIdAccountListMap.get(account.GBL_ID__c).add(account);
                }else
                {
                    List<Account> accounts = new List<Account>();
                    accounts.add(account);
                    globalIdAccountListMap.put(account.GBL_ID__c, accounts);
                }   
            }   
        }
        return globalIdAccountListMap;
    }
    
    /*
     * @Description : This method is called in the extension Controller 
     * @ Args       : Global Id
     * @ Return     : Map of string and list of accounts
     */
    public static Map<String, list<Account>> getIntermediateAccounts(String globalId)
    {
        if(globalIdAccountListMap.size() == 0)
        {
            getParentAccounts(globalId);    
        }
        
        if(intermediateIdAccountListMap.size() == 0)
        {
            for(Account account : [Select Id, Name, GBL_ID__c, ACCT_ID__c, Int_ID__c, GOC_ID__c  
                                   FROM Account 
                                   WHERE GBL_ID__c IN :globalIdAccountListMap.keySet()  
                                   AND Int_ID__c <> null 
                                   ORDER BY Name])
            {
                intermediateIdSet.add(account.Int_ID__c);
                // if account is intermediate account
                if(intermediateIdAccountListMap.containskey(account.GBL_ID__c))
                {
                    if(account.ACCT_ID__c == account.INT_ID__c) 
                        intermediateIdAccountListMap.get(account.GBL_ID__c).add(account);   
                }else
                {
                    if(account.ACCT_ID__c == account.INT_ID__c)
                    {
                        List<Account> accounts = new List<Account>();
                        accounts.add(account);
                        intermediateIdAccountListMap.put(account.GBL_ID__c, accounts);
                    }
                   
                }   
            }
        }   
        return intermediateIdAccountListMap;
    }
    
    /*
     * @Description : This method is called in the extension Controller 
     * @ Args       : Global Id
     * @ Return     : Map of string and list of accounts
     */
    public static Map<String, List<Account>> getChildAccounts(String globalId)
    {
        if(globalIdAccountListMap.size() == 0)
        {
            getIntermediateAccounts(globalId);  
        }
        
        if(gocIdAccountListMap.size() == 0)
        {
            for(Account account : [Select Id, Name, GBL_ID__c, ACCT_ID__c, Int_ID__c, GOC_ID__c  
                                   FROM Account 
                                   WHERE GBL_ID__c <> NULL 
                                   AND Int_ID__c IN : intermediateIdSet
                                   AND GOC_ID__c != NULL
                                   ORDER BY Name])
            {
                // if account is child account
                if(gocIdAccountListMap.containskey(account.Int_ID__c))
                {
                    if(account.ACCT_ID__c == account.GOC_ID__c)
                        gocIdAccountListMap.get(account.Int_ID__c).add(account);    
                }else
                {   
                    if(account.ACCT_ID__c == account.GOC_ID__c)
                    {
                        List<Account> accounts = new List<Account>();
                        accounts.add(account);
                        gocIdAccountListMap.put(account.Int_ID__c, accounts);   
                    }                       
                    
                }
            }
        }
        return gocIdAccountListMap;
    }
    
    /*
     * @Description : This method is called in the extension Controller 
     * @ Args       : Account Id Set
     * @ Return     : list of opportunities
     */
    public static List<Opportunity> getOpportunities(Set<String> accIds)
    {
        if(opportunities.size() == 0)
        {
            for(Opportunity opportunity : [Select Id, Total_Opportunity_Revenue_USD__c,MH_Sheet_Attached__c, Name , Account.Name, Amount, OneCode__c, CloseDate, StageName, currencyisocode From Opportunity WHERE AccountId In: accIds])
            {
                opportunities.add(opportunity);
            }
        }   
        return opportunities;   
    }
    
    /*
     * @Description : This method is called in the extension Controller 
     * @ Args       : Account Id set
     * @ Return     : list of contacts
     */
    public static List<Contact> getContacts(Set<String> accIds)
    {
        if(contacts.size() == 0)
        {
            for(Contact contact : [Select  Id, Name , Account.Name, MailingCountry , Title, Phone, Email From Contact WHERE AccountId In: accIds])
            {
                contacts.add(contact);
            }
        }   
        return contacts;    
    }
 

}