/*Purpose: This Apex class implements schedulable interface to scheduleMS02_OpportunityStageIdentifie class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR       DATE                    DETAIL 
   1.0 -    Arijit     04/24/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS18_AccountStatusSchedulable implements schedulable{
	
	global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS02_OpportunityStageIdentifier(), Integer.valueOf(System.Label.CL72_MS02BatchSize)); 
    }
}