/*Purpose:  This Apex class contains static methods to process Contact records on their various stages of insertion/updation/deletion
==============================================================================================================================================
The methods called perform following functionality:

•   Owner gets added to Contact Team
•   D&B address is copied from associated Account 
•   Check for duplicate contacts

•   Automatically add contact to Contact Affiliations when Account is changed
•   Generate feed item when Invalid Work email is checked

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sujata   01/15/2013  Created Apex to support TRG04
   2.0 -    Shashank 03/04/2013  Added Logic to fetch Account address 
                                 check for duplicate contacts
                                 Generate feed item when Invalid Work email is checked
                                 Automatically add contact to Contact Affiliations when Account is changed
   3.0 -    Arijit   06/21/2013  Applied Code fix to filter out inactive users getting added to Contact Share
   4.0 -    Sarbpreet 07/02/2014 As per March release 2014 (Request # 2846) added logic to check if Contact address contains Korea and then send chatter post to the Contact owner. 
   5.0 -    Jagan    03/10/2014  Updated logic for 2846 
   6.0 -    Sarbpreet 01/12/2015 As part of PMO Request # 5521(February 2015 Release) updated logic to populate the Contact Account Address with the Account Address and not the account's D&B Address.
                                 (Earlier, the priority was given to D&B address i.e if we have value in D&B address then contact's account address will be populated with D&B address and if its blank
                                 then account address will be populated and if we have value in both D&B address and Account address then also D&B address will be given priority. Now as part of request 5521,
                                 the logic for populating D&B address have been removed and the contact's account address will only be populated with account address.) 
   7.0 -    Sarbpreet 17/03/2015 As per April Release 2015(Request # 5982) updated logic to provide edit access to contact team members if contact owner is changed. 
   8.0 -    Jyotsna 24/03/2015   Req # 5954 :Commented the logic for checking duplicate contact with email Id, as this has replaced with OOB Duplication Management feature - April 2015 release 
   9.0 -    Keerthi 31/05/2019   Request 17500 : Using database. for DML operations where ever it is required
============================================================================================================================================== 
*/
public class AP19_ContactTriggerUtil {

    public static set < string > accountIdset = new set < string > ();
    public static Map < string, Account > accIdAddressMap = new Map < string, Account > ();
    // public static set < string > emailSet = new set < string > ();
    public static Map < string, List < Contact >> emailContactMap = new Map < string, List < Contact >> ();
    public static Map < string, string > contactAccountIdmap = new Map < string, string > ();
    //Set to hold contact owner IDs
    public static Set < ID > ownerIDset = new Set < ID > ();
    public static Set < ID > updatedOwnerIDset = new Set < ID > ();
    public static Set < ID > oldOwners = new Set < ID > ();

    //Set to hold contact IDs
    public static Set < ID > contactIDs = new Set < ID > ();

    public static Set < ID > checkDuplicacy = new Set < ID > ();

    public static Map < ID, User > userInfo = new Map < ID, User > ();
    public static Map < ID, User > updatedUserInfo = new Map < ID, User > ();
    public static Map < ID, User > oldOwnerUserInfo = new Map < ID, User > ();
    public static Map < ID, String > contactShareMap = new Map < ID, String > ();

    public static Contact_Team_Member__c tempObj;
    public static ContactShare conShare;
    public static List < Contact_Team_Member__c > tempObjList = new List < Contact_Team_Member__c > ();
    public static List < ContactShare > conShareList = new List < ContactShare > ();


    public static Map < ID, ID > deletedContactMap = new Map < ID, ID > ();
    public static List < Contact_Team_Member__c > delContactTeamList = new List < Contact_Team_Member__c > ();
    public static Map < List < Contact_Team_Member__c > , Contact > survivingTeammembersMap = new Map < List < Contact_Team_Member__c > , Contact > ();

    public static boolean isMergeProcess = false;
    public static boolean isContactTeam = false;
    private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember();

    public static List < FeedItem > feedList = new List < FeedItem > ();
    public static List < Contact > contactFeedList = new List < Contact > ();
    
    public static final string STR_ErrorMsg = 'Operation Failed due to: ';
    public static final string STR_ErrorMsg1 = '. Please contact your Administrator.';
    private static final string STR_Error = 'Please provide the correct value for Country because Account Address could not be found.';
    public static final string STR_Edit = 'Edit';
    public static final string STR_ALL = 'ALL';
    public static final string STR_KOREA = 'korea';
    public static final string STR_PENDCONSENT = 'Pending Consent';
    public static final string STR_FEEDBODY =Label.WorkEmail_Error;
     
    /*
     * @Description : This method is called on before insertion of Contact records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    public static void processContactBeforeInsert(List < Contact > triggernew) {
        try {
            for (Contact con: triggernew) {
                con.Ownerlookup__c = con.OwnerID;
                if (con.AccountId <> Null) {
                    //add account ids to set
                    accountIdset.add(con.AccountId);
                }

            }



            if (!accountIdset.isEmpty()) {
                //iterate on accounts
                for (account acc: [select Id, Name, One_Code__c, Country__c, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry, ShippingCity, ShippingState, ShippingCountry, ShippingStreet, ShippingPostalCode from Account where Id IN: accountIdset]) {
                    //add to map
                    accIdAddressMap.put(acc.Id, acc);
                }
            }

            if (!accIdAddressMap.isEmpty() && !isDMUser) {
                //iterate on triggernew
                for (Contact con: triggernew) {

                    con.Primary_OneCode__c = accIdAddressMap.get(con.AccountId).One_Code__c;
                    if (con.AccountId <> Null && con.MailingCountry == null) {
                        //As part of PMO Request # 5521(February 2015 Release) updated logic to populate the Contact Account Address with the Account Address and not the account's D&B Address.
                        //if billing address is found
                        if (accIdAddressMap.get(con.AccountId).BillingCountry <> Null) {
                            con.MailingCity = accIdAddressMap.get(con.AccountId).BillingCity;
                            con.MailingStreet = accIdAddressMap.get(con.AccountId).BillingStreet;
                            con.MailingCountry = accIdAddressMap.get(con.AccountId).BillingCountry;
                            con.MailingPostalCode = accIdAddressMap.get(con.AccountId).BillingPostalCode;
                            con.MailingState = accIdAddressMap.get(con.AccountId).BillingState;
                        }
                        //else display error
                        else if (con.MailingCountry == Null) {
                            triggernew[0].MailingCountry.adderror(STR_Error);
                        }
                    }

                }
            }
        } catch (Exception ex) { triggernew[0].adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1);}
    }


    /*
     * @Description : This method is called on after insertion of Contact records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    public static void processContactAfterInsert(Map < Id, Contact > triggernewmap) {        
        feedList.clear();
        contactFeedList.clear();
        emailContactMap.clear();
       // emailSet.clear();
        try {
            
            for (Contact con: triggernewmap.values()) {
                //Fetch contact owners
                ownerIDset.add(con.ownerID);
             
             // Req # 5954 :Commented below logic as this has replaced with OOB Duplication Management feature - April 2015 release
               
               /* if (con.Email <> Null) {
                    //add eamils to set
                    emailSet.add(con.Email);
                } */
            }
            
            /* Req # 5954 :Commented below logic as this has replaced with OOB Duplication Management feature - April 2015 release
            if (!emailSet.isEmpty()) {
                //iterate on contacts
                for (Contact con: [select Id, Name, Email, Contact_ID2__c from Contact where Email IN: emailSet AND Id NOT IN: triggernewmap.keyset()]) {
                    if (emailContactMap.containsKey(con.Email)) {
                        emailContactMap.get(con.Email).add(con);
                    } else {
                        List <Contact> conList = new List <Contact> ();
                        conList.add(con);
                        emailContactMap.put(con.Email, conList);
                    }
                }
            }

           
           if (!emailContactMap.isEmpty()) {
                //iterate on triggernew
                for (Contact con: triggernewmap.values()) {
                    //if it exist in map
                    if (emailContactMap.containskey(con.Email)) {
                        for (Contact cont: emailContactMap.get(con.Email)) {
                            if (con.Id <> cont.Id) {
                                //display error
                                con.addError('Contact with the same Email already exists. Please search for the following Contact ID: ' + cont.Contact_ID2__c);
                            }
                        }
                    }

                }
            }
            */

            if (!ownerIDset.isEmpty()) {
                //Fetch User Info for the given contact Owner
                for (User usr: [Select EmployeeNumber, ID, First_Name__c, Last_Name__c, Country__c, LOB__c, Employee_Office__c, Empl_Status__c, People_Directory__c, Email_Address__c, UserRole.Name from User where Id IN: ownerIDset]) {
                    userInfo.put(usr.ID, usr);
                }
            }

            //Check if contact owner exists in User table
            if (!userInfo.isEmpty()) {
                //Assign contact owner to contact team
                for (Contact con: triggernewmap.values()) {
                    //if (!con.IsConvertedFromLead__c) {
                        if (userInfo.get(con.ownerID) <> null) {
                            //Jagan: commenting the below code as it will result in query inside for loop
                                tempObj = new Contact_Team_Member__c();
                                tempObj.Contact__c = con.id;
                                tempObj.ContactTeamMember__c = userInfo.get(con.ownerID).ID;
                                tempObj.First_Name__c = userInfo.get(con.ownerID).First_Name__c;
                                tempObj.Last_Name__c = userInfo.get(con.ownerID).Last_Name__c;
                                tempObj.User_ID__c = userInfo.get(con.ownerID).ID;
                                tempObj.Role__c = userInfo.get(con.ownerID).UserRole.Name;
                                tempObjList.add(tempObj); //insert contact team member record in list 
                            //}
                        //}
                    }
                }
                
                if (!tempObjList.isEmpty()) {
                    AP19_ContactTriggerUtil.isContactTeam = true;
                    upsert tempObjList; //insert the list
                   }
            }
        } catch (Exception ex) {System.debug('Exception occured at Contact Trigger processContactAfterInsert with reason :' + Ex.getMessage());triggernewMap.values()[0].adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1);}
    }


    /*
     * @Description : This method is called on before updation of Contact records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    public static void processContactBeforeUpdate(Map < Id, Contact > triggernewMap, Map < Id, Contact > triggeroldMap) {
        try {
            for (Contact con: triggernewMap.Values()) {
                con.Ownerlookup__c = con.OwnerID;
                //if account is changed
                if (con.AccountId <> Null && con.AccountId <> triggeroldMap.get(Con.Id).AccountId) {
                    //add to set
                    accountIdset.add(con.AccountId);
                }

            }


            if (!accountIdset.isEmpty()) {
                //iterate over account
                for (account acc: [select Id, Name, One_Code__c, Country__c, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry, ShippingCity, ShippingState, ShippingCountry, ShippingStreet, ShippingPostalCode from Account where Id IN: accountIdset ]) {
                    //add to map
                    accIdAddressMap.put(acc.Id, acc);
                }
            }

            if (!accIdAddressMap.isEmpty() && !isDMUser) {
                //iterate over map
                for (Contact con: triggernewMap.values()) {
                    if(accIdAddressMap.containsKey(con.AccountId)) {
                        con.Primary_OneCode__c = accIdAddressMap.get(con.AccountId).One_Code__c;
                        if (con.AccountId <> Null) {
                            //As part of PMO Request # 5521(February 2015 Release) updated logic to populate the Contact Account Address with the Account Address and not the account's D&B Address.
                            //if billing address is found
                            if (accIdAddressMap.get(con.AccountId).BillingCountry <> Null) {
                            con.MailingCity = accIdAddressMap.get(con.AccountId).BillingCity;
                            con.MailingStreet = accIdAddressMap.get(con.AccountId).BillingStreet;
                            con.MailingCountry = accIdAddressMap.get(con.AccountId).BillingCountry;
                            con.MailingPostalCode = accIdAddressMap.get(con.AccountId).BillingPostalCode;
                            con.MailingState = accIdAddressMap.get(con.AccountId).BillingState;
                        }
                           
                            //else display error
                            else if (con.MailingCountry == Null) {
                                triggernewMap.get(con.Id).MailingCountry.adderror(STR_Error);
                            }
                        }
                    }
                }
            }
            for (Contact con: triggernewMap.values()) {
                
                 /*Request# 8684: Automatically remove invalid email flag if contact email address is updated*/ 
                if(con.Email <> triggeroldMap.get(Con.Id).Email && con.Email <> Null && triggeroldmap.get(con.Id).Email <> null){     
                    con.Invalid_Work_Email__c =false; 
                }
                /*Request# 8684 ends*/
            } 
        } catch (Exception ex) {triggernewMap.values()[0].adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1);}
    }


    /*
     * @Description : This method is called on after updation of Contact records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    public static void processContactAfterUpdate(List<Contact> triggernew ,  Map <Id,Contact > triggernewMap, Map < Id, Contact > triggeroldmap) {
        // clear cache
        feedList.clear();
        contactFeedList.clear();
        contactIDs.clear();
        contactShareMap.clear();
        updatedOwnerIDset.clear();
        oldOwners.clear();
        conShareList.clear();
        updatedOwnerIDset.clear();
        checkDuplicacy.clear();
        tempObjList.clear();
        contactAccountIdmap.clear();
        emailContactMap.clear();
       // emailSet.clear();
        List < Contact_Affiliations__c > conAffList = new list < Contact_Affiliations__c > ();
        List < FeedItem > feedItemList = new List < FeedItem > ();
        Set < String > oldUserIds = new Set < String > ();
        
        

            
        try {
            // Added as aprt of April Release 2015(Request # 5982)--START
            Map<Id ,List<Contact_Team_Member__c>> mapConTeam = new Map<Id ,List<Contact_Team_Member__c>>() ; 
            List<Contact_Team_Member__c> lstContTeam ; 
            
            for (Contact_Team_Member__c contTeam: [Select ContactTeamMember__c, Contact__c,Contact__r.OwnerId, ID from Contact_Team_Member__c where Contact__c IN : triggernewMap.keySet()]) {
                checkDuplicacy.add(contTeam.ContactTeamMember__c) ; 
                if(mapConTeam.containsKey(contTeam.Contact__c)) {
                    lstContTeam = mapConTeam.get(contTeam.contact__c) ; 
                } else {
                    lstContTeam = new List<Contact_Team_Member__c>();
                }

                    lstContTeam.add(contTeam) ; 
                mapConTeam.put(contTeam.Contact__c, lstContTeam);
                
            }
            
            
        
            Map<Id, User> userIdMap = null ; 
            if (!checkDuplicacy.isEmpty()) {
                userIdMap = new Map<Id, User>([Select Id From User where Id IN: checkDuplicacy  and isActive=true ]);
                
            }
            
            Map<Id ,Map<ID ,ContactShare >> mapConShareID = new Map<Id ,Map<ID ,ContactShare >>() ; 
            Map<ID ,ContactShare > mapContShare  ;   
            for (ContactShare cshare: [Select ContactId, UserOrGroupId, contact.ownerID, ContactAccessLevel FROM ContactShare where ContactId IN: triggernewMap.keySet()]) {
               
                if(mapConShareID.containsKey(cshare.contactId)) {
                    mapContShare = mapConShareID.get(cshare.contactId) ; 
                } else {
                    mapContShare = new Map<ID ,ContactShare >();
                } 
                //if(cshare.UserOrGroupId  <> cshare.contact.ownerID) {
                    mapContShare.put(cshare.UserOrGroupId ,cshare ) ; 
                //}
                mapConShareID.put(cshare.ContactId , mapContShare);
                
            }
              
            // Added as aprt of April Release 2015(Request # 5982)--END 
                
            
            //Fetch contact IDs
            for (Contact con: triggernew) {
                contactIDs.add(con.ID);              
                //if Account is changed
                if (con.AccountId <> triggeroldmap.get(con.Id).AccountId && con.AccountId <> null && triggeroldmap.get(con.Id).AccountId <> null) {
                    //add to map
                    contactAccountIdmap.put(con.Id, triggeroldmap.get(con.Id).AccountId);
                }
                
                // Req # 5954 :Commented below logic as this has replaced with OOB Duplication Management feature - April 2015 release
               
               /* if (con.Email <> Null && con.Email <> triggeroldMap.get(con.Id).Email) {
                    //add to set
                    emailSet.add(con.Email);
                } */
                                
               
                //As per March release 2014 (Request # 2846) added logic to check if Contact address contains Korea and then send chatter post to the Contact owner.    
                if (triggeroldmap.get(con.id).Contact_Status__c != STR_PENDCONSENT && con.Contact_Status__c == STR_PENDCONSENT) {     
                    
                    if ((con.MailingCountry <> null && con.MailingCountry.containsIgnoreCase(STR_KOREA)) || (con.OtherCountry <> null && (con.OtherCountry.containsIgnoreCase(STR_KOREA))) ) {
                        contactFeedList.add(con);
                    } 
                }
                
                //if check box is checked
                FeedItem feedItem ;
                if (con.Invalid_Work_Email__c <> triggeroldmap.get(con.Id).Invalid_Work_Email__c && con.Invalid_Work_Email__c == true) {
                    //create feed item
                    feedItem = new FeedItem();
                    feedItem.Body = STR_FEEDBODY;
                    feedItem.ParentId = con.Id;
                    feedItemList.add(feedItem);
                }


                  
              // Added as aprt of April Release 2015(Request # 5982)--START  
              if(con.OwnerId <> triggeroldmap.get(con.Id).OwnerId && !mapConTeam.isEmpty()){
                
                    List<Contact_Team_Member__c>  conTeamMem =  mapConTeam.get(con.id) ;                                                         
                    ContactShare share;
                    for(Contact_Team_Member__c  conteam : conTeamMem)
                    {
                            Map<ID ,ContactShare >  mapCont = mapConShareID.get(con.id) ;                                                                                    
                            if( (!mapCont.containsKey(conteam.ContactTeamMember__c) || ( mapCont.containsKey(conteam.ContactTeamMember__c) && ( mapCont.get(conteam.ContactTeamMember__c).ContactAccessLevel <> STR_Edit &&  mapCont.get(conteam.ContactTeamMember__c).ContactAccessLevel.toUpperCase() <> STR_ALL ) )  )  && userIdMap.containsKey(conteam.ContactTeamMember__c) ) {
                                share=new ContactShare ();        
                                share.UserOrGroupId=conteam.ContactTeamMember__c;
                                share.ContactId=conteam.Contact__c;
                                share.ContactAccessLevel= STR_Edit;                             
                                 
                                conShareList.add(share) ; 
                            } 
                        } 
                }
                // Added as aprt of April Release 2015(Request # 5982)--END
              }
 
/*************************Request Id:17500 : Using database. for DML operations where ever it is required START********************************************/              
Database.SaveResult[] saveList = Database.insert(feedItemList, false);

// Iterate through each returned result
for (Database.SaveResult sr : saveList) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully inserted  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}
/*************************Request Id:17500 : Using database. for DML operations where ever it is required END********************************************/                          
         
            //insert feedItemList;
            if (!conShareList.isEmpty()) {
               Database.SaveResult[] saveList1 = Database.insert(conShareList, false);

// Iterate through each returned result
for (Database.SaveResult sr : saveList1) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully inserted  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
        
    }
} 
                //insert conShareList;
            }
            //Request # 2846
            if (!contactFeedList.isEmpty()) {
                postChatter(contactFeedList);
            }

            /* Commented below logic as this has replaced with OOB Duplication Management feature - April 2015 release
            if (!emailSet.isEmpty()) {
                //iterate on contacts
                for (Contact con: [select Id, Name, Email, Contact_ID2__c from Contact where Email IN: emailSet AND Id NOT IN: triggeroldmap.keyset()]) {
                    if (emailContactMap.containsKey(con.Email)) {
                        emailContactMap.get(con.Email).add(con);
                    } else {
                        List <Contact> conList = new List <Contact> ();
                        conList.add(con);
                        emailContactMap.put(con.Email, conList);
                    }
                }
            }

           
            if (!emailContactMap.isEmpty()) {
                //iterate on triggernew
                for (Contact con: triggernew) {
                    //if it exist in map
                    if (emailContactMap.containskey(con.Email)) {
                        for (Contact cont: emailContactMap.get(con.Email)) {
                            if (con.Id <> cont.Id) {
                                //display error
                                con.addError('Contact with the same Email already exists. Please search for the following Contact ID: ' + cont.Contact_ID2__c);
                            }
                        }
                    }

                }
            }
            */

            /*if (!contactAccountIdmap.isEmpty()) {
                Contact_Affiliations__c conAff;
                //iterate on trigger.new
                for (Contact con: triggernew) {
                    //if contact is present in map
                    if (contactAccountIdmap.containsKey(con.Id)) {
                        //create contact affiliation
                        conAff = new Contact_Affiliations__c();
                        conAff.Contact__c = con.Id;
                        conAff.Account__c = contactAccountIdmap.get(con.Id);
                        conAffList.add(conAff);
                    }
                }
                insert conAffList;
            }*/
            
            //Request# 17174: 12/12/2018: Fetched value from AccountContactRelation and assigned a specific value to Contact Type field. 
            //Checks if the map is not null
            if(contactAccountIdmap!= null)
            {
                List<AccountContactRelation> listRelationsToUpdate = new List<AccountContactRelation>();
                List<AccountContactRelation> listAccContRelations=[Select accountId,contactId,Contact_Type__c from AccountContactRelation where contactId IN :contactAccountIdmap.keySet()];
                //Checks if the list is not null
                if(listAccContRelations != null)
                {
                    //Loop for specific value assignment in the field Contact_Type__c
                    for(AccountContactRelation relation : listAccContRelations)  
                    {
                        //Checks if the map contains the specific accountId
                        if(contactAccountIdmap.values().contains(relation.accountId))
                        {
                            relation.Contact_Type__c = 'Former Employee - Other';
                            listRelationsToUpdate.add(relation);
                        }
                        
                    }
                }
                //Checks if the list is not null
                if(!listRelationsToUpdate.isEmpty())
                {
                    Database.SaveResult[] srList = Database.update(listRelationsToUpdate, false);

// Iterate through each returned result
for (Database.SaveResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully updated  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}
                    //update listRelationsToUpdate;
                }
            }
            //Request# 17174 ends
            

            
            for (Contact con: triggernew) {
                //Fetch contact owners
                if (con.OwnerId <> triggeroldmap.get(con.ID).OwnerId) {
                    oldUserIds.add(triggeroldmap.get(con.ID).OwnerId);
                    updatedOwnerIDset.add(con.ownerid);
                }
            } 

          

            if (!updatedOwnerIDset.isEmpty()) {
                //Fetch User Info for the given contact Owner
                for (User usr: [Select EmployeeNumber, ID, First_Name__c, Last_Name__c, Country__c, LOB__c, Employee_Office__c, Empl_Status__c, People_Directory__c, Email_Address__c, UserRole.Name from User where Id IN: updatedOwnerIDset]) {
                    updatedUserInfo.put(usr.ID, usr);
                }
            }


            //Check if contact owner exists in User table
            if (!updatedUserInfo.isEmpty()) {
                for (Contact con: triggernew) {
                    
                    if (updatedUserInfo.get(con.ownerID) <> null) {
                           
                        //Check if the new Owner does not already exist in Contact Team as a member. 
                        if (!checkDuplicacy.contains(updatedUserInfo.get(con.ownerID).ID)) {
                            
                            //Jagan: commenting the below code as it will result in query inside for loop
                            //if (!AP17_ExecutionControl.isDataMigrationGroupMember(con.OwnerId)) {
                                tempObj = new Contact_Team_Member__c();
                                tempObj.Contact__c = con.id;
                                tempObj.ContactTeamMember__c = updatedUserInfo.get(con.ownerID).ID;
                                tempObj.First_Name__c = updatedUserInfo.get(con.ownerID).First_Name__c;
                                tempObj.Last_Name__c = updatedUserInfo.get(con.ownerID).Last_Name__c;
                                tempObj.User_ID__c = updatedUserInfo.get(con.ownerID).ID;
                                tempObj.Role__c = updatedUserInfo.get(con.ownerID).UserRole.Name;
                                tempObjList.add(tempObj); //insert contact team member record in list 
                            //}

                        }
                    }
                }
                if (!tempObjList.isEmpty()) {
                    //insert the list
                    Database.UpsertResult[] srList = Database.upsert(tempObjList, false);

// Iterate through each returned result
for (Database.UpsertResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully upserted  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}
                    //upsert tempObjList;
                }
            }
            
        } catch (Exception ex) {System.debug('Exception occured at Contact Trigger processContactAfterUpdate with reason :' + Ex.getMessage());triggernew[0].adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1);}
    }

    /*
     * @Description : This method is called on after deletion of Contact records
     * @ Args       : Trigger.old
     * @ Return     : void
     */
    public static void processContactAfterDelete(List < Contact > triggerold) {
        Set < String > winningContacts = new Set < String > ();
        List < Contact_Team_Member__c > teamForDelete = new List < Contact_Team_Member__c > ();

        try {
            for (Contact contact: triggerold) {
                if (contact.MasterRecordId <> null) {
                    AP19_ContactTriggerUtil.isMergeProcess = false;
                    winningContacts.add(contact.MasterRecordId);
                }
            }

            if (!winningContacts.isEmpty()) {
                Map < Id, List < Contact_Team_Member__c >> userIdContactTeamMap;
                for (Contact contact: [Select Id, Name, (Select Id, Contact__c, ContactTeamMember__c FROM Contact_Team_Members__r) FROM Contact WHERE Id IN: winningContacts]) {
                    userIdContactTeamMap = new Map < Id, List < Contact_Team_Member__c >> ();
                    List < Contact_Team_Member__c > contactTeam = contact.Contact_Team_Members__r;

                    List < Contact_Team_Member__c > contactTeamList;
                    for (Contact_Team_Member__c team: contactTeam) {
                        if (userIdContactTeamMap.containsKey(team.ContactTeamMember__c)) {
                            userIdContactTeamMap.get(team.ContactTeamMember__c).add(team);
                        } else {
                            contactTeamList = new List < Contact_Team_Member__c > ();
                            contactTeamList.add(team);
                            userIdContactTeamMap.put(team.ContactTeamMember__c, contactTeamList);
                        }
                    }


                    if (!userIdContactTeamMap.isEmpty()) {
                        for (String userId: userIdContactTeamMap.keyset()) {
                            if (userIdContactTeamMap.get(userId).size() > 1) {
                                for (Integer i = 1; i <= userIdContactTeamMap.get(userId).size() - 1; i++) {
                                    teamForDelete.add(userIdContactTeamMap.get(userId)[i]);
                                }
                            }
                        }
                    }
                }
            }

            AP19_ContactTriggerUtil.isMergeProcess = true;
            if (!teamForDelete.isEmpty()) {
                
                

Database.deleteResult[] drList = Database.delete(teamForDelete, false);

// Iterate through each returned result
for (Database.deleteResult dr : drList) {
    if (dr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully deleted  ' + dr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : dr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}
                //delete teamForDelete;
            }
        } catch (Exception ex) {System.debug('Exception occured at Contact Trigger processContactAfterDelete with reason :' + Ex.getMessage());triggerold[0].adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1); 
        }
    }

    /*
    * @Description : As per March Release 2014(Request # 2846) this method send chatter post to contact owner if contact address contains 'Korea'.
    * @ Args       : contactsForFeedList
    * @ Return     : void
    */   
    public static void postChatter(List < Contact > contactsForFeedList) {
        feedList.clear();
        FeedItem feedItem;
        try {
        for (Contact con: contactsForFeedList) {
            feedItem = new FeedItem();
            feedItem.Body = Label.Chatter_Post_for_Korea;
            feedItem.ParentId = con.Id;
            feedItem.createdbyid = Label.MercerForceUserid;
            feedList.add(feedItem);
        }
        if (!feedList.isEmpty()) {
            Database.insert(feedList);
        }
        } catch (exception Ex) {for( contact cf :contactsForFeedList ){cf.adderror(STR_ErrorMsg + Ex.getMessage() + STR_ErrorMsg1);}}}
   
   
  
}