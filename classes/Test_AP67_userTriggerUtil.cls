/*Purpose: This test class provides data coverage to AP67_userTriggerUtil class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE          DETAIL 
   1.0 -    Reetika   07/19/2013  Created test class
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_AP67_userTriggerUtil {
    /*
     * @Description : Test method to provide data coverage to AP67_userTriggerUtil class
     * @ Args       : Null
     * @ Return     : void
     */
    
    /*
    static testMethod void myUnitTest() 
    {   
        Group g1 = new Group(Name='All MercerForce Users', type='Queue');

            insert g1;
        //string randomName1 = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        User testUser1 = new User();
        testUser1.alias = 'user1k2';
        testUser1.email = 'tek2@bbcm.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'Verlin';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'bbb24@bbcm.com';
        testUser1.EmployeeNumber = '123391';
        testUser1.isActive = true;
        testUser1.S_Dash_Supervisor__c = null;
        testUser1.S_Dash_Supervisor_2__c = null;  
        testUser1.S_Dash_Supervisor_3__c = null;
        testUser1.S_Dash_Supervisor_4__c = null;  
        testUser1.S_Dash_Supervisor_5__c = null;    
        insert testUser1;
               
        collaborationgroup c = [select id from collaborationgroup where name = 'All Mercer' limit 1];
        
        Test.startTest();
        system.runAs(testUser1) {
            collaborationgroupmember cg = new collaborationgroupmember();
            cg.memberid= testUser1.id;
            cg.CollaborationGroupId = c.id;
            cg.collaborationrole = 'Standard';
            
            insert cg;
            try{
            delete cg; 
            } catch(Exception e){
                system.assert(true, e.getMessage().contains('You cannot unjoin from this group'));
            }
        }
        Test.stopTest();
        system.debug('I am at the end of method1');
    } */
    static testMethod void myUnitTestCoverUpdate() 
    {
        ID profileIDTest = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
        
        
            User testUser1 = new User();
        testUser1.alias = 'user1k2';
        testUser1.email = 'tek2@bbcm.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'Verlin';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = profileIDTest;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'bbb24@bbcm.com';
        testUser1.EmployeeNumber = '1233911';
        testUser1.isActive = true;
        testUser1.S_Dash_Supervisor__c = null;
        testUser1.S_Dash_Supervisor_2__c = null;  
        testUser1.S_Dash_Supervisor_3__c = null;
        testUser1.S_Dash_Supervisor_4__c = null;  
        testUser1.S_Dash_Supervisor_5__c = null; 
        insert testUser1;
                
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1233911';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.User_Record__c = testUser1.id;
        insert testColleague; 
        
            Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
            iSalesGoal1.Sales_Goals__c = 500;
            //iSalesGoal1
            //iSalesGoal1.Employee_ID__c = '12345678903';
            iSalesGoal1.Colleague__c = testColleague.id;
            iSalesGoal1.OwnerId = testUser1.Id;
            insert iSalesGoal1;        
        
            User usr2 =  Mercer_TestData.createUser1(profileIDTest,'someth','khatarnak',201);
            
       System.runAs(testUser1){
           Group g1 = new Group(Name='All MercerForce Users', type='Queue');

            insert g1;
           usr2.AboutMe='Desperately trying to make thi work';
           usr2.ProfileId=[Select id from Profile where name = 'Mercer System Admin Lite' limit 1].id;
           usr2.Request__c='5463';
           usr2.Blue_Sheet_Managers_License__c = true;
           //usr2.IsActive = false;
           test.starttest();
           
               update usr2;
           AP67_userTriggerUtil.RemoveMHLicenses(usr2);
            test.stoptest();
        }
            
        
        system.debug('I am at the end of method2');
    }
    static testMethod void testRoleandInactive() 
    {Group g1 = new Group(Name='All MercerForce Users', type='Queue');

            insert g1;
        system.debug('I am at the beginning of method3');
        ID profileIDTest = [Select id from Profile where name = 'System Administrator' limit 1].id;
        
            User testUser1 = new User();
        testUser1.alias = 'user1k2';
        testUser1.email = 'tek2@bbcm.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'Verlin';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = profileIDTest;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'bbb24@bbcm.com';
        testUser1.EmployeeNumber = '123391';
        testUser1.isActive = true;
        testUser1.S_Dash_Supervisor__c = null;
        testUser1.S_Dash_Supervisor_2__c = null;  
        testUser1.S_Dash_Supervisor_3__c = null;
        testUser1.S_Dash_Supervisor_4__c = null;  
        testUser1.S_Dash_Supervisor_5__c = null;    
        insert testUser1;
            //User usr2 =  Mercer_TestData.createUser1(profileIDTest,'someth','khatarnak',201);
        
        User usr2 = new User();
        usr2.alias = 'user3k2';
        usr2.email = 'tek22@bbcm.com';
        usr2.emailencodingkey='UTF-8';
        usr2.lastName = 'Verlin';
        usr2.languagelocalekey='en_US';
        usr2.localesidkey='en_US';
        usr2.ProfileId = profileIDTest;
        usr2.timezonesidkey='Europe/London';
        usr2.UserName = 'bbb26@bbcm.com';
        usr2.EmployeeNumber = '123399';
        usr2.isActive = false;
        usr2.S_Dash_Supervisor__c = testuser1.Id;
        usr2.S_Dash_Supervisor_2__c = null;  
        usr2.S_Dash_Supervisor_3__c = null;
        usr2.S_Dash_Supervisor_4__c = null;  
        usr2.S_Dash_Supervisor_5__c = null;    
        insert usr2;
        
       System.runAs(testUser1){
           usr2.AboutMe='Desperately trying to make thi work';
           usr2.ProfileId=[Select id from Profile where name = 'Mercer System Admin Lite' limit 1].id;
           usr2.Request__c='5463';
           usr2.IsActive = true;
           usr2.UserRoleId = '00EE0000000cm8l';
           usr2.Blue_Sheet_Managers_License__c = true;
           //usr2.IsActive = false;
           test.starttest();
           
               update usr2;
         AP67_userTriggerUtil.removeSDashSupervisors(usr2);
            test.stoptest();
        }
            
        
        system.debug('I am at the end of method3');
    }
    
}