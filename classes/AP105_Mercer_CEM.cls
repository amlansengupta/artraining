/*Purpose:  This classis ued to insert new CEM record and Associated task.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
1.0 -    Jagan   11/11/2014   As per december Release 2014(Request #2264) ,created class for CEM  record creation 
Created  VF page "Mercer_CEM" as part of this request
============================================================================================================================================== 
*/
public with sharing class AP105_Mercer_CEM  {
    
    public Id AccId;
    public Id CEMid;
    public string rvw{get;set;}
    public Account acct {get;set;}
    public date rDate{get;set;}
    public boolean showMsg{get;set;}
    public integer Rank{get; set;}
    public Client_Engagement_Measurement_Interviews__c CEM {get;set;}
    
    public task cTask{get;set;}
    public List<Task> tList{get;set;}
    
    public boolean taskRender {get;set;}
    public String renderPDF {get;set;}
    
    /*
* @Description : Constructor of Controller class 
* @ Args       : controller
* @ Return     : null 
*/   
    public AP105_Mercer_CEM (ApexPages.StandardController controller) {
        String retur = system.currentPageReference().getParameters().get('retURL');
        System.debug('&&&&&'+retur );            
        if(retur !=null && retur.contains('inContextOfRef')){
            if(retur.contains('recordId')){
            	CEMid = retur.substring(retur.indexOf('recordId=')+9,retur.indexOf('&',retur.indexOf('recordId=')+9));
            }
            Integer index = retur.indexOf('inContextOfRef');
            String sr = 'inContextOfRef';
            Integer len = sr.length();
            System.debug('&&&&&'+index);
            System.debug('retur '+retur);
            Integer index1 = retur.indexOf('%');
            if(index1==-1){
                String str = retur.substring(index+len+3)  ; 
                system.debug('***'+str);
                try{
                    string urlId=EncodingUtil.base64Decode(str).toString();
                    system.debug('***'+urlId);
                    Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(urlId);
                    Map<String, Object>  m2=(Map<String, Object>)meta.get('attributes');
                    System.debug(m2.get('recordId'));
                    AccId =(string) m2.get('recordId');
                }catch(Exception ex){String err = ex.getMessage();if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+err.substring(err.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+35,err.length()-4)));    else ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+ex.getMessage()));    }
            }else{
                try{
                    String str = retur.substring(index+len+3,index1)  ; 
                    system.debug('***'+str);
                    
                    string urlId=EncodingUtil.base64Decode(str).toString();
                    system.debug('***'+urlId);
                    Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(urlId);
                    Map<String, Object>  m2=(Map<String, Object>)meta.get('attributes');
                    System.debug(m2.get('recordId'));
                    AccId = (String)m2.get('recordId');
                }catch(Exception ex){String err = ex.getMessage();if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+err.substring(err.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+35,err.length()-4)));    else ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+ex.getMessage()));    
                    
                }
            }
        }
        else{
            AccId = ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ACCID);
            CEMid = ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ObjID);
        } 
        System.debug('CEM'+CEMid);
        if(AccId == null){
            AccId = [Select Account__c from Client_Engagement_Measurement_Interviews__c where id=:cemid].Account__c;
        }        
        showMsg =false;
        tList= new List<Task>();
        
        //System.debug('^^^'+AcId);
        if(AccId !=null){
            UserRecordAccess  uAc =[SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId=:AccId ];
            if(!uAc.HasEditAccess){
                showMsg=true;
            }
            //String pp= ApexPages.currentPage().getUrl();
            //System.debug('&&&&&'+pp); 
            // showMsg = false;
            
            cTask= new task();
            cTask.Subject = ConstantsUtility.STR_SUB;
            taskRender = True;  
            
            
            renderPDF = ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_RENDER);
            if(CEMid == null){
                CEM = new Client_Engagement_Measurement_Interviews__c();
                CEM.Account__c = AccId;
                CEM.Status__c = ConstantsUtility.STR_STATS;
                acct = [select id,name,Relationship_Manager__r.name from Account where id=:AccId];
                System.debug('****&&'+acct.id);
            }
            else{
                
                CEM = [select id,CEM_Scribe__c,
                       X1_1_Overall_Health_Relationship__c,X1_2_Individual_TeamMember_Assessment__c,X1_3_1_EH_B_Rating__c,Global_Business_Solutions__c,
                       X1_3_3_Retirement_Rating__c,X1_3_4_Talent_Rating__c,X1_4_Mercer_contribution_for_success__c,X2_1_Industry_challeges_Priorities__c,
                       X2_2_Senior_leadership_challenges_Issues__c,X2_3_Add_company_values__c,X2_4_Top_3to5_Priorities_for_your_Org__c,
                       X2_5_Top_3to5_Priorties_to_support_for_y__c,
                       X2_6_Mercer_support_To_your_challenges__c,X3_1_1_Mercer_relationship_gaps__c,
                       X3_1_2_Improve_Mercer_relationship__c,X3_1_3_Who_else_conduct_this_type_of_Int__c,X3_1_Mercer_connections_within_your_org__c,
                       X3_2_1_Mercer_competetive_advantages__c,X3_2_2_Your_Impression_of_our_competitor__c,X3_2_Your_Impression_of_Mercer__c,
                       X3_3_Firms_do_you_work_with_and_Compare__c,X4_1_1_How_to_Mitigate_the_issues__c,X4_1_Is_Mercer_relationship_at_risk__c,
                       X5_1_SWOT_Analysis_Strengths__c,X5_2_SWOT_Analysis_Opportunities__c,X5_3_SWOT_Analysis_Strength__c,X5_4_SWOT_Analysis_Threats__c,
                       X6_1_Closing_Additional_Comments__c,Account__c,CEM__c,Reviewer__c,CEM_Interviewer_2__c,Client_Contact_Reviewer_1__c,
                       Client_Contact_Reviewer_2__c,Client_Contact_Reviewer_3__c,Competitor_Innovative_Score__c,Competitor_Partnership_Score__c,
                       Competitor_Planning_Project_Mgmt_Score__c,Competitor_Proactive_Score__c,Competitor_Quality_Score__c,Competitor_Service_Communication_Score__c,
                       Competitor_Value_Score__c,Date_of_Review__c,Mercer_Innovative_Score__c,Mercer_Partnership_Score__c,Mercer_Planning_Project_Management_Scr__c,
                       Mercer_Proactive_Score__c,Mercer_Quality_Score__c,Mercer_Service_Communication_Score__c,Mercer_Value_Score__c,Overall_rating__c,Notes__c,Status__c,
                       Mercer_C_Suite_Readiness_Score__c,Mercer_Diversity_Inclusion_Score__c,Competitor_C_Suite_Readiness_Score__c,Competitor_Diversity_Inclusion_Score__c,
                       C_Suite_Readiness_Comments__c,Diversity_Inclusion_Comments__c,Innovative_Comments__c,Partnership_Comments__c,Planning_Project_Management_Comments__c,
                       Proactive_Comments__c,Quality_Comments__c,Service_Communication_Comments__c,Value_Comments__c
                       from 
                       
                       Client_Engagement_Measurement_Interviews__c where id=:cemid];
                
                
                /*MercerInnScore = CEM.Mercer_Innovative_Score__c;
CompInnScore = CEM.Competitor_Innovative_Score__c;
MercerProScore = CEM.Mercer_Proactive_Score__c;
CompProScore = CEM.Competitor_Proactive_Score__c;
MercerQuaScore = CEM.Mercer_Quality_Score__c;
CompQuaScore = CEM.Competitor_Quality_Score__c;
MercerValScore = CEM.Mercer_Value_Score__c;
CompValScore = CEM.Competitor_Value_Score__c;
MercerPlanScore = CEM.Mercer_Planning_Project_Management_Scr__c;
CompPlanScore = CEM.Competitor_Planning_Project_Mgmt_Score__c;
MercerPartScore = CEM.Mercer_Partnership_Score__c;
CompPartScore = CEM.Competitor_Partnership_Score__c;
MercerSerScore = CEM.Mercer_Service_Communication_Score__c;
CompSerScore = CEM.Competitor_Service_Communication_Score__c;*/
                
                acct = [select id,name,Relationship_Manager__r.name from Account where id=:CEM.Account__c]; 
                AccId = acct.id;
            }
            
        }
        
    }
    
    
    /***** TO BE UNCOMMENTED IN CASE IF WE HAVE TO DISPLAY THE SCORES AS VALUES *******

public List<SelectOption> getOptions(){

List<SelectOption> lstOptions = new List<SelectOption>();
lstOptions.add(new SelectOption('1','1'));
lstOptions.add(new SelectOption('2','2'));
lstOptions.add(new SelectOption('3','3'));
lstOptions.add(new SelectOption('4','4'));
lstOptions.add(new SelectOption('5','5'));
lstOptions.add(new SelectOption('6','6'));
lstOptions.add(new SelectOption('7','7'));
lstOptions.add(new SelectOption('8','8'));
lstOptions.add(new SelectOption('9','9'));
lstOptions.add(new SelectOption('10','10'));

return lstOptions;
}


public String MercerInnScore {get;set;}
public String CompInnScore {get;set;}

public String MercerProScore {get;set;}
public String CompProScore {get;set;}

public String MercerQuaScore {get;set;}
public String CompQuaScore {get;set;}

public String MercerValScore {get;set;}
public String CompValScore {get;set;}

public String MercerPlanScore {get;set;}
public String CompPlanScore {get;set;}

public String MercerPartScore {get;set;}
public String CompPartScore {get;set;}

public String MercerSerScore {get;set;}
public String CompSerScore {get;set;}

*******TO BE UNCOMMENTED IN CASE IF WE HAVE TO DISPLAY THE SCORES AS VALUES ********/
    /*
* @Description : method to insert  or update CEM record.
* @ Args       : null
* @ Return     : PageReference 
*/ 
    public PageReference mapQuewithAns(){
        try{
            if(cemid == null){
                CEM.Account__c = accid;
                system.debug('DATE OF REVIEW....'+CEM.Date_of_Review__c);
                system.debug('CLIENT CONTACT REVIEWERS....'+CEM.Client_Contact_Reviewer_1__c+'....'+CEM.Client_Contact_Reviewer_2__c);
                
                insert CEM;
                insertTasks(CEM);
            }
            else{
                update CEM;
                insertTasks(CEM);
            }
            return new PageReference('/lightning/r/Account/'+acct.id+'/view');
        }
        catch(Exception e){String err = ''+e.getMessage();if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+err.substring(err.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+35,err.length()-4)));    else ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+e.getMessage())); return null;
        }
        
        
    }
    /*
* @Description : method to insert task.
* @ Args       : Client_Engagement_Measurement_Interviews__c
* @ Return     : Void 
*/ 
    public void insertTasks(Client_Engagement_Measurement_Interviews__c CEM){
        List<Task> newInsertList = new List<Task>();
        if(tList.size()>0){
            for(Task t:tList){
                if(t.subject<>null && t.Status <>null && t.Priority<>null){
                    t.whatid = CEM.id;
                    newInsertList.add(t);
                }
            }        
            if(newInsertList.size()>0){
                database.insert(tList); 
                newInsertList.clear();
                tList.clear();
            }
        }
    }
    /*
* @Description : this method is called on click of save &submit button.
* @ Args       : null
* @ Return     : pageReference 
*/
    public pageReference saveAndComplete(){
        try{
            if(cemid == null){
                CEM.status__c =ConstantsUtility.STR_STATUS;
                CEM.Account__c = accid;
                insert CEM;
                insertTasks(CEM);
            }
            else{
                CEM.status__c =ConstantsUtility.STR_STATUS;
                update CEM;
                insertTasks(CEM);
            }
            return new PageReference('/lightning/r/Account/'+acct.id+'/view');
        }
        catch(Exception e){String err = ''+e.getMessage();if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+err.substring(err.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+35,err.length()-4)));    else ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,''+e.getMessage()));    return null;
        }    
        
    }
    /*
* @Description : this method is called on click of cancel button.
* @ Args       : null
* @ Return     : pageReference 
*/
    public pageReference Cancel(){
        return new PageReference('/'+acct.id);
    }
    /*
* @Description : this method is to insert task.
* @ Args       : null
* @ Return     : void 
*/
    Public void insertTask(){
        taskRender = false;
        tList.add(cTask);
        cTask = new Task();
    }
    
    /*
* @Description : this method is to save task record and render task creation page.
* @ Args       : null
* @ Return     : void 
*/
    Public void saveAndMore(){
        taskRender = true;
        tList.add(cTask);
        cTask = new Task();
        cTask.Status = 'Not Started';
        cTask.Priority = 'Normal';
        cTask.Subject = ConstantsUtility.STR_SUB;
    }
    
    
}