/*Purpose: Test Class for providing code coverage to MercVFC08_OppurtunityEditable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   03/29/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_MercVFC08_OppurtunityEditable 
{
    /* * @Description : This test method provides data coverage for  Opportunity Editable Page        
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '133478911';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Honolulu - Fort';
        insert testOppty;
      
        ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
        MercVFC08_OppurtunityEditable controller = new MercVFC08_OppurtunityEditable(stdController);
        
        controller.editOpportunity();
        }
    }
}