/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:

•   Query all Scope modeling Tasks and Employees.
•   Created a Wrapper Class to capture Related Tasks and Employees w.r.t each Scope Modeling project .

History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Venu    03/03/2014  This Apex class acts as Controller for the Print Scope Modeling VF page.
   2.0      Madhavi 10/16/2014  As part of december 2014 release(#4881)Added IC_Charges__c field to the Scope_Modeling_Task__c  query 
                                Updated vf page "Mercer_PrintScopeModeling" to add this field.
============================================================================================================================================== 
*/
public class AP86_pdfGenerator_ScopeModeling {

    public List<Scope_Modeling_Task__c> taskList { get; set; }
    public Map<Id, List<Scope_Modeling_Task__c>> prjIdTaskListMap { get; set; }
    private Set<Id> projectIds = new Set<Id>(); 
     public String monthShort {get; set;}
  
    private Map<Id, Scope_Modeling__c> prjMap = new Map<Id, Scope_Modeling__c>();
    public List<projectWrapper> projectWrapperList { get; set; }
    /*Constructor used to Query Project,Tasks and Employees of  OpportunitY
     Once required records are fetched they are passed into Wrapper list.
    */
    public AP86_pdfGenerator_ScopeModeling(ApexPages.StandardController controller){
    taskList= new List<Scope_Modeling_Task__c>();
    //As part of december 2014 release(#4881)Added IC_Charges__c field to the query.
     monthShort = getCurrentMonth();
    taskList = [select name,Billable_Expenses__c,Billable_Time_Charges__c,Calc_Billable_Time_Charges__c, 
                Bill_Est_of_Increases__c,Task_Name__c,IC_Charges__c,
                Bill_Rate__c,Cost__c,Non_Billable_Time_Charges__c,Scope_Modeling__c,Bill_Type01__c,
                    (select Billable_Time_Charges__c,Bill_Cost_Ratio__c,Bill_Rate__c,Business__c,
                    Cost__c,Employee_Bill_Rate__c,Hours__c,Market_Country__c,Name__c,Level__c,
                    Scope_Modeling_Task__c from Scope_Modeling_Employees__r limit 50000)
                from Scope_Modeling_Task__c 
                    where Scope_Modeling__c =:apexpages.currentpage().getparameters().get('id') ORDER BY CreatedDate desc limit 50000];
    
    
    prjIdTaskListMap= new Map<Id, List<Scope_Modeling_Task__c>>();
    projectWrapperList = new List<projectWrapper>();
     
    if(taskList.size() > 0) {
            for(Scope_Modeling_Task__c t :taskList) {
                if(!prjIdTaskListMap.containsKey(t.Scope_Modeling__c)){
                    prjIdTaskListMap.put(t.Scope_Modeling__c, new List<Scope_Modeling_Task__c>());
                }
                prjIdTaskListMap.get(t.Scope_Modeling__c).add(t);
            }
            for(Id taskId : prjIdTaskListMap.keySet()) {
                projectWrapperList.add(new projectWrapper(prjMap.get(taskId), prjIdTaskListMap.get(taskId)));
            }
        }
    
    
    }
    
    /*Wrapper Class to capture Related Tasks and Employees w.r.t each Project
    */
      public class ProjectWrapper {
        public Scope_Modeling__c project { get; set; }
        public List<Scope_Modeling_Task__c> taskList { get; set; }
        /*Wrapper Constructor to capture Related Tasks and Employees w.r.t each Project
        */
        public projectWrapper(Scope_Modeling__c project, List<Scope_Modeling_Task__c> taskList) {
            this.project = project;
            this.taskList = taskList;
        }
    }
     public static String getCurrentMonth()
    {
        Map<Integer,String> month = new Map<Integer,String>(); 
        month.put(1,'Jan');
        month.put(2,'Feb');
        month.put(3,'Mar');
        month.put(4,'Apr');
        month.put(5,'May');
        month.put(6,'Jun');
        month.put(7,'Jul');
        month.put(8,'Aug');
        month.put(9,'Sep');
        month.put(10,'Oct');
        month.put(11,'Nov');
        month.put(12,'Dec');
        String MonthName = month.get(Date.Today().Month());
        return MonthName;
    }



}