@isTest(SeeAllData=true)
private class SensitiveRiskSunc_Test {

    
    @isTest static void UnitTest() {
        Account Acc = new Account(name = 'Test Account',BillingCountry='test');
        insert Acc;
        
        
        Sensitve_At_Risk__c clientrelationship = new Sensitve_At_Risk__c(Account__c = Acc.id,At_Risk_Type__c = 'At Risk/High',At_Risk_Reason__c='Other',Revenue_at_Risk__c=12,Status__c= 'Open',GBM_Action__c='Change of local teams');
        insert clientrelationship;
        
        clientrelationship  = [select Id ,LOB_Status__c,At_Risk_Type__c  from Sensitve_At_Risk__c  where Id =: clientrelationship.id ];
        
            System.debug('clientrelationship'+clientrelationship);
        
        system.assertEquals(clientrelationship.At_Risk_Type__c ,'At Risk/High');
        //system.assertEquals([select id from Client_Relationship__c].size(),1);
        
        clientrelationship.LOB_Status__c = 'Pending';   
        update clientrelationship;   
        
        //delete clientrelationship;
        
        //system.assertEquals([select id from Client_Relationship__c].size(),0);
        
        
        test.startTest();
            Id batchId = database.executeBatch(new BATCH_OneDirectionalSensitiveDataSync(),1);
            system.abortJob(batchId);
        test.stopTest();
        

    }    
}