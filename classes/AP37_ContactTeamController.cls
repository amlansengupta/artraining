/*
 Purpose: 
 This Controller takes care of Contact Team functionality          
 ==============================================================================================================================================
 History 
 ------------------------------------------
 VERSION     AUTHOR              DATE        DETAIL   
  1.0 -    Shashank Singhal      05/25/2013  Created Controller Class
  2.0 -    Sarbpreet             7/4/2014    Updated Comments/documentation.
 ============================================================================================================================================== 
 */
public class AP37_ContactTeamController 
{
    public Contact_Team_Member__c conTeam = new Contact_Team_Member__c();
    
    /*
    *   Creation of constructor
    */
    public AP37_ContactTeamController(ApexPages.StandardController controller) 
    {
        this.conTeam = (Contact_Team_Member__c)controller.getRecord();
    }
    
    /*
     * @Description : This method saves the contact Team record 
     * @ Args       : null
     * @ Return     : void
     */
    public pageReference save()
    {
        pageReference pageRef = new pageReference('/'+conTeam.Contact__c);
        try
        {
            upsert conTeam;
        }catch(DMLexception e){ /*if(e.getDmlStatusCode(0).equalsIgnoreCase('FIELD_CUSTOM_VALIDATION_EXCEPTION'))*/ ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0))); pageRef = null;  return pageRef;  } 
        catch(Exception ex){ ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));  pageRef = null; return pageRef;}
        
        return pageRef;
    }
     /*
     * @Description : This method saves and opens the page to create a new team member record
     * @ Args       : null
     * @ Return     : void
     */
    public pageReference savenew()
    {
        
        string url = '/apex/Mercer_ContactTeamPage?CF00NE0000003jeLF='+conTeam.Contact__r.FirstName+conTeam.Contact__r.LastName+'&CF00NE0000003jeLF_lkid='+conTeam.Contact__c+'&scontrolCaching=1&retURL=%2F'+conTeam.Contact__c+'&sfdc.override=1';
        pageReference pageRef = new pageReference(url);
        try
        {
            insert conTeam;
        }catch(DMLexception e){ if(e.getDmlStatusCode(0).equalsIgnoreCase('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0))); pageRef = null; return pageRef;  }
        catch(Exception ex){ ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage())); pageRef = null; return pageRef;}
        pageRef.setRedirect(true);
        return pageRef;
    }
    /*
     * @Description : This method cancels the current page and redirects to previous page
     * @ Args       : null
     * @ Return     : void
     */
    public pageReference cancel()
    {
        
        pageReference pageRef = new PageReference('/'+conTeam.Contact__c);
        return pageRef;
    }

}