/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
private class AP96_scopeit_Add_Task_Employees_2_Test {

    private static Mercer_TestData mtdata;
    private static Mercer_ScopeItTestData mtscopedata;
    
    @testSetup static void setup(){
        Current_Exchange_Rate__c testCurrExch2 = new Current_Exchange_Rate__c();
        mtdata = new Mercer_TestData();
        mtscopedata = new Mercer_ScopeItTestData(); 
        Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
       // setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        setting.StrValue__c = 'International:5000;North America:10000';          
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */  
        
        Insert listofvalues;
        Test.startTest();
            String LOB = 'Retirement';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;   
        Test.stopTest();  
            ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
            ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
            ScopeIt_Employee__c testScopeitEmployee = mtscopedata.buildScopeitEmployee(employeeBillrate);
            Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
            Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
            Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
            mtscopedata.insertCurrentExchangeRate();
            testCurrExch2.name = 'ALL';
            testCurrExch2.Conversion_Rate__c = 3;
            insert testCurrExch2;
    }
  
    /*
     * @Description : Test class for adding task and employees to scopeit project   
    */
    static testMethod void AP96_myUnitTest() {
       User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {

                ScopeIt_Project__c testScopeitProj = [Select Id, name, Product__c, opportunity__c, OpportunityProductLineItem_Id__c, CurrencyIsoCode, Sales_Price__c From ScopeIt_Project__c where name = 'testscopeitproj'];
                ScopeIt_Task__c  testScopeitTask =  [Select Id, task_name__c, ScopeIt_Project__c, Billable_Expenses__c, Bill_Rate__c from ScopeIt_Task__c where task_name__c = 'testscopetask'];
                ScopeIt_Employee__c testScopeitEmployee = [Select Id, ScopeIt_Task__c, Employee_Bill_Rate__c, Cost__c, Hours__c, currenCyISOCode, Billable_Time_Charges__c, Bill_Rate_Opp__c From ScopeIt_Employee__c Where ScopeIt_Task__c =: testScopeitTask.Id];
                String objname = testScopeitProj.name;
                PageReference pageRef = Page.Mercer_Add_Task_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('projid', testScopeitProj.id);
               

                ApexPages.StandardController stdController2 = new ApexPages.StandardController(testScopeitProj); 
                AP96_scopeit_Add_Task_Employees controller2 = new AP96_scopeit_Add_Task_Employees();
              
              
               Test.startTest();             
                 String rid=string.valueof(testScopeitEmployee.id);  
                 String n='Name';      
                 String level ='A';     
                 String busi='Benefits Admin';   
                 String mar='Australia';
                 String sub ='BPO Services' ;     
                 Decimal rt=10.00;        
                 AP96_scopeit_Add_Task_Employees.Selempwrapper semw = new AP96_scopeit_Add_Task_Employees.Selempwrapper(rid,n,level,busi,mar,sub,rt);
              
                 semw.isSelected = true;  
                 controller2.SelempWrpList.add(semw); 
              
                PageReference pageReference3 = controller2.saveEmployees();          
                PageReference pageReference4 =  controller2.cancel(); 
                controller2.updateselectedemployees(); 
                controller2.clearFields();
                
                controller2.RemoveEmployees(); 
                  
                controller2.namesearch = 'Gen';
                controller2.levelsearch = 'A';
                controller2.emp.type__c = 'Generic Employee';
                controller2.emp.Market_Country__c = 'Chile';
                controller2.emp.Business__c = 'Property Administration';
                controller2.emp.Sub_Business__c ='BPO Services';
                controller2.getEmployees();
                                       
                controller2.getSelectedEmployees();
                controller2.RemoveEmployees();
                //controller2.empWrpList[0].isSelected = false; 
                controller2.RemoveEmployees();
                  
                PageReference pageReference5 = controller2.Beginning();
                PageReference pageReference6 =  controller2.Previous();
                PageReference pageReference7 =  controller2.End();
                PageReference pageReference8 =  controller2.Next(); 
                controller2.getDisablePrevious();
                controller2.getDisableNext();
                controller2.getTotal_size();
                controller2.getPageNumber();
                controller2.getTotalPages();
                  
            Test.stopTest();
        }
    }
}