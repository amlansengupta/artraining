/*Purpose: This Apex class implements schedulable interface to schedule MS04_OpportunityClosedWonBatchable class.
==============================================================================================================================================
History ----------------------- 
                                                                 VERSION     AUTHOR       DATE                    DETAIL 
                                                                    1.0 -    Arijit     04/29/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS03_OpportunityClosedWonSchedulable implements Schedulable{
    
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS04_OpportunityClosedWonBatchable(),  Integer.valueOf(System.Label.CL73_MS04BatchSize));  
    }
}