/*
Purpose: This test class provide data coverage to AP35_ContactDistributionHistoryUtil  class
==============================================================================================================================================
History 
----------------------- 
VERSION       AUTHOR       DATE        DETAIL    
1.0 -         Shashank     04/08/2013  Created test class   
============================================================================================================================================== 
*/
@isTest
private class Test_AP35_ContactDistributionHistoryUtil 
{
    /*
     * @Description : Test method to make an entry into Contact Distribution history object whenever 
                      a contact Distribution is added or deleted for an Opportunity
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        try
        {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
      //  insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
      //  testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Mercer_TestData.buildMercerOffice();
        Distribution__c distribution = new Distribution__c();  
        distribution.Name = 'Test Distribution';
        distribution.Scope__c = 'Office';        
        distribution.Office__c = 'New Delhi';        
        insert distribution;
        
        
        Contact_Distributions__c conDis = new Contact_Distributions__c();
        conDis.Distribution__c = distribution.Id;
        conDis.Contact__c = testContact.Id;
        insert conDis;
        delete conDis;
        
         
        Contact_Distributions__c conDis1 = new Contact_Distributions__c();
        conDis1.Distribution__c =distribution.id;
        conDis1.Contact__c = testContact.Id;
        insert conDis1;
        
        Mercer_TestData.buildMercerOffice();
        Distribution__c distribution1 = new Distribution__c();  
        distribution1.Name = 'Test Distribution1';
        distribution1.Scope__c = 'Office';        
        distribution1.Office__c = 'New Delhi';        
        insert distribution1;
        
        Contact_Distributions__c conDis2 = new Contact_Distributions__c();
        conDis2.Distribution__c =distribution1.id;
        conDis2.Contact__c = testContact.Id;
        insert conDis2;
        
        
        conDis2.Distribution__c = distribution.Id;
        update conDis2;
        }
       }catch(Exception ex)       {
       System.debug(Ex.getMessage());                
       }
        
    }
}