@isTest
private class Test_UpdateGPFYPlanRevenue {
	@isTest static void updateGPValueTest() {
        Account Acc = new Account(name = 'Test Account',BillingCountry='test',Account_Country__c='USA');
        insert Acc;
        
        
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()+1),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()+1),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()+1),12,31));
        insert py;
        
        Growth_Plan__c gp = new Growth_Plan__c(Account__c = Acc.id,fcstPlanning_Year__c = py.id);
        insert gp;
        List<Growth_Plan__c> growthPlanList = new List<Growth_Plan__c>();
        growthPlanList.add(gp);
        
        List<Forecast_Model__c> forecastModelNewList = new List<Forecast_Model__c>();
        List<Forecast_Model__c> forecastModelUpdatedList = new List<Forecast_Model__c>();
        for(Growth_Plan__c gobj:growthPlanList){
                
                    for(Integer i=1;i<34;i++){
                        Forecast_Model__c fObjHealth= new Forecast_Model__c();
                        
                        //for Health LOb
                        
                        if(i==1){
                          fObjHealth.LOB__c='Health';
                         }
                        if(i==2){
                            fObjHealth.Sub_Business__c='Associations';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==3){
                            fObjHealth.Sub_Business__c='B2B';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==4){
                            fObjHealth.Sub_Business__c='Government';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==5){
                            fObjHealth.Sub_Business__c='Marketplace 365';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==6){
                            fObjHealth.Sub_Business__c='Mercer Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==7){
                            fObjHealth.Sub_Business__c='Voluntary Benefits';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        if(i==8){
                            fObjHealth.Sub_Business__c='Health - Large Market Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        //for Wealth LOb
                       
                        if(i==9){
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==10){
                            fObjHealth.Sub_Business__c='Administration';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==11){
                            fObjHealth.Sub_Business__c='Defined Benefits';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==12){
                            fObjHealth.Sub_Business__c='Defined Contribution & Financial Wellness - Consumer';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==13){
                            fObjHealth.Sub_Business__c='Defined Contribution & Financial Wellness - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==14){
                            fObjHealth.Sub_Business__c='Leadership & Operations - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==15){
                            fObjHealth.Sub_Business__c='Non-Pension and Specialties';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==16){
                            fObjHealth.Sub_Business__c='Inv CIO';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==17){
                            fObjHealth.Sub_Business__c='Wealth - Large Market Admin';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==18){
                            fObjHealth.Sub_Business__c='Wealth Administration';
                            fObjHealth.LOB__c='Wealth';
                        }
                        
                        
                        //for Career LOB
                        
                        if(i==19){
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==20){
                            fObjHealth.Sub_Business__c='Career-Products';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==21){
                            fObjHealth.Sub_Business__c='Career-Services';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==22){
                            fObjHealth.Sub_Business__c='Career-Workday';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==23){
                            fObjHealth.Sub_Business__c='Career - Career';
                            fObjHealth.LOB__c='Career';
                        }
                        
                         //for Career GBS
                         
                         if(i==24){
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==25){
                            fObjHealth.Sub_Business__c='Intellectual Capital Solutions';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==26){
                            fObjHealth.Sub_Business__c='Global Consulting';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==27){
                            fObjHealth.Sub_Business__c='Global Career';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==28){
                            fObjHealth.Sub_Business__c='Global Health';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==29){
                            fObjHealth.Sub_Business__c='Global Wealth';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==30){
                            fObjHealth.Sub_Business__c='Innovation & M&A Community';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==31){
                            fObjHealth.Sub_Business__c='WRG';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==32){
                            fObjHealth.Sub_Business__c='MCG Leadership/GCM';
                            fObjHealth.LOB__c='GBS';
                         }
                        
                        //for Other LOB
                        if(i==33){
                            fObjHealth.LOB__c='Other';
                         }
                        
                        fObjHealth.Current_Carry_Forward_Revenue__c=0;
                        fObjHealth.Current_FY_Revenue__c=0;
                        fObjHealth.Post1_FY_Revenue__c=0;
                        fObjHealth.Post_FY_Revenue__c=0;
                        fObjHealth.Pre_Prior_FY_Actual__c=0;
                        fObjHealth.Prior_FY_Projected__c=0;
                        fObjHealth.Prior_LTM_Revenue__c=0;
                        fObjHealth.Account__c=gobj.Account__c;
                        fObjHealth.Planning_Year__c=gobj.fcstPlanning_Year__c;
                        fObjHealth.Growth_Plan__c=gobj.Id;
                        forecastModelNewList.add(fObjHealth);  
                      }
               }
                 Test.startTest();
        			if(forecastModelNewList.size()>0){
                      insert forecastModelNewList;
                      }
        			UpdateGPFYPlanRevenue.updateGPValue(forecastModelNewList);
        		  
                 Test.stopTest(); 
        		
                 for(Forecast_Model__c fobj:forecastModelNewList){
                     if(fobj.Sub_Business__c ==null){
                         fobj.Current_FY_Revenue__c=0;
                         forecastModelUpdatedList.add(fobj);
                     }
                }
                if(forecastModelUpdatedList!=null && forecastModelUpdatedList.size()>0)
                    update forecastModelUpdatedList;
            
        }
    
  
    
    	
}