/*Purpose: Test Class for providing code coverage to AP104_OpportunityFieldUpdate class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   04/11/2014  Created class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = false)
private class Test_AP104_OpportunityFieldUpdate 
{

   /* Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    }*/
    /*
     * @Description : Created Opportunity data for populating Revenue Udpated field
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        //createCS();
        ConstantsUtility.STR_BYPASS_BUYINGINFLUENCE_FORBATCH = 'true';
        // User user1 = new User();
        
        Colleague__c Coll;
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        }
         
        //String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        //user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
       Test.startTest();
        system.runAs(User1)
        {
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        //testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '123';
        //Test.startTest();
        insert testAccount;
        //Test.stopTest();
        
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty2';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c = true;
        testOppty.CloseDate = date.Today();
        //Updated as part of 5166(july 2015)
        List<String> oppOffices_List = new List<String>();  
        ApexConstants__c  officesToBeExcluded = new ApexConstants__c();
        officesToBeExcluded.StrValue__c = 'Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        //insert officesToBeExcluded
        //ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
        if(officesToBeExcluded.StrValue__c != null){        
        oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        testOppty.Opportunity_Office__c = oppOffices_List[0];
        }
        
        testOppty.CurrencyIsoCode = 'ALL';
        
      //Test.startTest();
        insert testOppty;
        Test.stopTest();
       }
        
        
        
        //system.runAs(User1)
            AP104_OpportunityFieldUpdate AMsch = new AP104_OpportunityFieldUpdate();
            String sch = '0 0 23 * * ?';
        
            system.schedule('Test AP25_LockOpportunitySchedulable', sch, AMsch);

        
    }
}