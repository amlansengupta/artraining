/***************************************************************************************************************
User Story : US504/US505(17441/17442)
Purpose : This class is as the test class for MF2_LockOpportunityCustomLinkApex.
Created by : Soumil Dasgupta
Created Date : 10/1/2018
Project : MF2
****************************************************************************************************************/

@isTest(seeAllData=false)
public class MF2_LockOpportunityCustomLinkApexTest {

    public static testmethod void LockOppTest(){
        
        String stageFromMethod;
        Boolean stageBool;
        
        //Account test data creation
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        String accID = testAccount1.ID;
        
        //Opportunity test data creation
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  accID; 
        //#12638 sprint 2 dependency
        //testOpportunity.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        testOpportunity.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(2018, 1, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        testOpportunity.Close_Stage_Reason__c ='Other';
        insert testOpportunity;
        Id OpptyId = testOpportunity.ID;
        
        testOpportunity.Stagename = 'Closed / Lost';
        //#12638 Sprint 2 dependency
        //testOpportunity.Step__c = 'Closed / Lost';
        update testOpportunity;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            stageFromMethod =  MF2_LockOpportunityCustomLinkApex.fetchStageOpp(OpptyId); 
            stageBool = MF2_LockOpportunityCustomLinkApex.fetchStageMatch(OpptyId); 
        }
        
        
        system.assertequals(stageFromMethod,'Closed / Lost');
    }
}