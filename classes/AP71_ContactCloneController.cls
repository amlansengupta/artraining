/*Purpose: [Nov Release Change (Request 2465)]:This class clones a Contact 
========================================================================

History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   10/16/2013  Functionality to customize cloning as per Requirement#2465

====================================================================== 
*/
public class AP71_ContactCloneController {
 
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Contact con {get;set;}
    public Contact newCon{get;set;}
              
    public List<Contact> contactList {
    get {
            if (contactList == null) contactList = new List<contact>();
            return contactList ;
        }
    set;}
                     
    public List<Contact_Team_Member__c> contTeamList = new List<Contact_Team_Member__c>();


 
    // initialize the controller
    public AP71_ContactCloneController(ApexPages.StandardController controller) { 
    
        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        con = (Contact)controller.getRecord();       
    }
        
    public PageReference contactsWithContactTeam() { 
         Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Contact.fields.getMap();
         List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();

         String theQuery = 'SELECT ';
         List<Contact_Clone__c> ocs = Contact_Clone__c.getall().values();
         Set<String> fieldset = new Set<String>();
         for(Contact_Clone__c oc: ocs)
         fieldset.add(oc.Field_Name__c);
       
         for(Schema.SObjectField s : fldObjMapValues)
         {
              String theLabel = s.getDescribe().getLabel(); // Perhaps store this in another map
              String theName = s.getDescribe().getName();
                               
              // Continue building your dynamic query string
              if(fieldset.contains(theName))
                  theQuery += theName + ',';
         }
                           
         // Trim last comma
         theQuery = theQuery.subString(0, theQuery.length() - 1);
             
         String conid = con.id;
         
         // Finalize query string
         
         theQuery += ' FROM contact WHERE id =: conid';
      
        theQuery = String.escapeSingleQuotes(theQuery);
         // Make your dynamic call
         con = Database.query(theQuery);
         con.Contact_Status__c = 'Active';                         
         con.ownerid = userinfo.getuserid(); 
        //Request# 17129 : Contact was not getting cloned poperly.      
         //newCon = con.clone(false);
         newCon = con.clone(false,true,false,false);
         newCon.Salutation=con.Salutation;  
         //Request# 17129 ends      
          system.debug('&&1test&&'+newCon.Salutation);       
         return null;
         
                                                              
     }
                 

      /*
      * @Description :This method  saves cloned Contact Page 
      * @ Args       : null   
      * @ Return     : void
      */
      public PageReference saveClone(){
         
          PageReference pageRef;
          contTeamList.clear();
          try{                             
                 insert newcon;
               
                 Map<String, Schema.SObjectField> fldObjMapTeam = schema.SObjectType.Contact_Team_Member__c.fields.getMap();
                 List<Schema.SObjectField> fldObjMapValuesTeam = fldObjMapTeam.values();
                 List<Contact_Team_Member__c> contTeam = new List<Contact_Team_Member__c>();
                
                 String theQueryTeam = 'SELECT ';                                    
                 for(Schema.SObjectField s : fldObjMapValuesTeam)
                 {
                      String theLabelTeam = s.getDescribe().getLabel(); // Perhaps store this in another map
                      String theNameTeam = s.getDescribe().getName();
                                             
                      // Continue building your dynamic query string
                      theQueryTeam += theNameTeam + ',';
                 }
                 
                 // Trim last comma
                 theQueryTeam = theQueryTeam.subString(0, theQueryTeam.length() - 1);
                        
                 String contid = con.id;
                         
                 // Finalize query string
                 theQueryTeam += ' FROM Contact_Team_Member__c WHERE Contact__r.Id =: contid';
                        
                // Make your dynamic call
                contTeam = Database.query(theQueryTeam);
                                                               
                for(Contact_Team_Member__c c : contTeam)
                {            
                    if(c.ContactTeamMember__c <> newcon.ownerid)
                    {
                        Contact_Team_Member__c newConTeam = new Contact_Team_Member__c();                                  
                        newConTeam = c.clone(false);
                        newConTeam .Contact__c  = newCon.id;
                        contTeamList.add(newConTeam);
                    }                           
                }
                                 
                insert contTeamList;
                    
            } catch(System.DmlException e){
                  System.debug('update Con error==  '+e.getDmlMessage(0));
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
                  return null;
            }              
                
            pageRef = new PageReference('/' + newCon.Id);  
            return pageRef;
        }
   
     /*
     * @Description :This method  saves and direct to new Contact Page 
     * @ Args       : null   
     * @ Return     : void
     */
     public pageReference savenewClone() {
        try{
             insert newCon;
            
            
             Map<String, Schema.SObjectField> fldObjMapTeam = schema.SObjectType.Contact_Team_Member__c.fields.getMap();
             List<Schema.SObjectField> fldObjMapValuesTeam = fldObjMapTeam.values();
             List<Contact_Team_Member__c> contTeam = new List<Contact_Team_Member__c>();
            
             String theQueryTeam = 'SELECT ';                                    
             for(Schema.SObjectField s : fldObjMapValuesTeam)
             {
                  String theLabelTeam = s.getDescribe().getLabel(); // Perhaps store this in another map
                  String theNameTeam = s.getDescribe().getName();
                                         
                  // Continue building your dynamic query string
                  theQueryTeam += theNameTeam + ',';
             }
             
             
             
             // Trim last comma
             theQueryTeam = theQueryTeam.subString(0, theQueryTeam.length() - 1);
                    
             String contid = con.id;
                     
             // Finalize query string
             theQueryTeam += ' FROM Contact_Team_Member__c WHERE Contact__r.Id =: contid';
                    
             // Make your dynamic call
             contTeam = Database.query(theQueryTeam);
                                                       
            for(Contact_Team_Member__c c : contTeam)
            {            
                if(c.ContactTeamMember__c <> newcon.ownerid)
                {
                    Contact_Team_Member__c newConTeam = new Contact_Team_Member__c();                                  
                    newConTeam = c.clone(false);
                    newConTeam .Contact__c  = newCon.id;
                    contTeamList.add(newConTeam);
                }                           
            }
            insert contTeamList;              
        } catch(System.DmlException e){
          System.debug('update Con error==  '+e.getDmlMessage(0));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
          return null;
        }

        PageReference pref = new pageReference('/003/e?retURL=%2F'+newCon.id+'');
        pref.setRedirect(true);      
        controller.save();       
        return pref;
        
   }
   
     /*
     * @Description :This method cancels the current page and return to Contact Page
     * @ Args       : null   
     * @ Return     : void
     */
     public pageReference cancelClone()
     {
        pageReference pageRef = new pageReference('/'+con.Id);
        return pageRef;
     }

}