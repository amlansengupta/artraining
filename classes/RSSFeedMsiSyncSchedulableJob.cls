global class RSSFeedMsiSyncSchedulableJob implements Schedulable {
	global void execute(SchedulableContext context) {
		// get the rss feed
		RSSFeedSyncJob.syncFeeds(RSSFeedDownloadService.FEED_TYPE_MSI);
	}

	global void runExecuteMethod () {
		// get the rss feed
		RSSFeedSyncJob.syncFeeds(RSSFeedDownloadService.FEED_TYPE_MSI);
	}
}