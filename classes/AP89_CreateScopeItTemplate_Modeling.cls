/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Takes input Arguments Id's of "Selected Scope Modeling Project from Scope Modeling Detail page 
.Inserts data into TempScopeIt Projects with related Tasks and Employees.


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Venu    02/20/2014  Created this Global class which is being called from button "Save as Template" in Scope Modeling.
   2.0 -    Madhavi 11/25/2014  As part of #4881(Dec 2014 release) Added fields IC_Charge_Type__c,IC_charges__c to the query. 
   3.0 -    Sarbpreet 01/19/2014 As part of CA ticket 34925513 updated logic so that user does not get runtime exception.                             
   4.0 - 	 Sankar 01/07/2019 As part of Request# 17447, createTscope() method has been marked as @AuraEnabled
============================================================================================================================================== 
*/
Global class AP89_CreateScopeItTemplate_Modeling{

       static string BLANK='';
       static string CONTACT='Please contact Administrator';
       static string SCOPE='Scope Project Template has been created. Template Name = "';
       static string DBQTE='"';
       static string TEMPLATE='-Template';
       static string MSGE='When saving as a template, the template name' +    ' is defaulted to "Scope Modeling Name - template". '+
                    'A template already exists with this name.'+
                    'Please update the Scope Modeling field to make it unique.';
    /*
    This Method is Called from button save as Template
    *Request ID # 17447 : This method has been made @AuraEnabled to make it available to lightning component ScopeModelingSaveAsTemplate. 
	*/
    @AuraEnabled
    webservice static string createTscope(list<Id> scpId){ 
        string message=BLANK;
    try{
        //This Map is used to check product Names against Product id 
         Map<id,product2> aMap= new Map<id,product2>([select id,name from Product2 limit 50000]);
         Map<id,Scope_Modeling_Employee__c> empMap= new Map<id,Scope_Modeling_Employee__c> ();
       /* for(Scope_Modeling_Employee__c se:[select id,Scope_Modeling__c,Scope_Modeling_Task__c,
        Bill_Rate__c,Bill_Cost_Ratio__c,Business__c,Market_Country__c,Cost__c,Hours__c,Employee_Bill_Rate__c,
        Billable_Time_Charges__c from Scope_Modeling_Employee__c limit 50000])
        {
            empMap.put(se.Scope_Modeling_Task__c,se);
        }*/
                
        
        
        string names=BLANK;
    
       list<Scope_Modeling__c> sprList = new list<Scope_Modeling__c>();
        //created Scope_Modeling__c variable as part of CA ticket 34925513 
       Scope_Modeling__c scopeModelng;
       //Added IC_Charge_Type__c,IC_charges__c fields to the query(dec 2014 #4881)
        sprList=[select id,name,Sales_Price__c,LOB__c,CurrencyIsoCode,Product__c,Solution_Segment__c,
                (select id,Name,Scope_Modeling__c,Bill_Type01__c,Bill_Rate__c,Billable_Expenses__c,Task_Name__c,IC_Charge_Type__c,IC_charges__c,Bill_Est_of_Increases__c from Scope_Modeling_Tasks__r limit 50000 )
                from Scope_Modeling__c where id=:scpId limit 50000 ];
        
        //assigned first record of sprList list to scopeModelng variable as the list would contain one record as part of CA ticket 34925513
        scopeModelng =  sprList[0];    
 
        //This set is used to Fetch all templates and store there names inorder to prevent duplicates     
         set<string>nameSet= new set<string>();
          //Added where clause in query as part of CA ticket 34925513
          String query='select name from Temp_ScopeIt_Project__c where name like \'' + scopeModelng.name + '%\'  limit 50000';
            //query= String.escapeSingleQuotes(query); 
         List<Temp_ScopeIt_Project__c> tempScopeProjectList = database.query('select name from Temp_ScopeIt_Project__c where name like \'' + scopeModelng.name + '%\'  limit 50000');
        for(Temp_ScopeIt_Project__c t:tempScopeProjectList){
            nameSet.add(t.name);
        }
        
       if(!sprList.isEmpty()){
         //if(scopeModelng.id!=null){
               list<Temp_ScopeIt_Project__c> tspList = new list<Temp_ScopeIt_Project__c>();
               set<id> idSet= new set<id>(); 
           for(Scope_Modeling__c s:sprList){
                 
                 Temp_ScopeIt_Project__c tspt = new Temp_ScopeIt_Project__c();
                 
                 string tempName = s.name + TEMPLATE;
                   
                 //Added if condition as part of CA ticket 34925513  
                 
                 if(!nameSet.isEmpty()&&((tempName.length() > 80 && nameSet.contains( tempName.substring(0,80))) ||
                                         (tempName.length() <= 80 && nameSet.contains( tempName) ))){
                     Message=MSGE;
                 }
                  
                 
                 else{
                     string strname = s.name +TEMPLATE;
                     if(strname.length()>80){
                         tspt.name=strname.substring(0,80);
                     }
                     else{
                         tspt.name=strname; 
                     }
                     tspt.Sales_Price__c=s.Sales_Price__c;
                     if(s.Product__c!=null){
                     tspt.Product__c=aMap.get(s.Product__c).name;
                     }
                     tspt.Product_Id__c=s.Product__c;
                     tspt.LOB__c=s.LOB__c;
                     tspt.currencyisocode= s.currencyisocode;
                     tspt.Solution_Segment__c=s.Solution_Segment__c;
                     tspList.add(tspt);
                     //sp.OpportunityProductLineItem_Id__c=s.OpportunityProductLineItem_Id__c;
                     try {
                         database.insert(tspList,false);
                     
                         if(!tspList.isEmpty()){
                             
                             for(Temp_ScopeIt_Project__c sn:tspList){
                                 idSet.add(sn.id);
                             }
                             
                             names=BLANK;
                             for(Temp_ScopeIt_Project__c sname:[select name from Temp_ScopeIt_Project__c where id in:idSet limit 50000]){
                                 names+=sname.name;
                             }
                      
                         }
                         message=SCOPE+names+DBQTE;
                     }
                     catch (exception ex){Message+=CONTACT;}
                 
                    
                     if(!s.Scope_Modeling_Tasks__r.isEmpty()){
                     
                         List<Temp_ScopeIt_Employee__c> LstTempScopEmp = new List<Temp_ScopeIt_Employee__c>();
                             
                         for( Scope_Modeling_Task__c st:s.Scope_Modeling_Tasks__r){
                         
                             Temp_ScopeIt_Task__c tst= new Temp_ScopeIt_Task__c();
                             tst.Task_Name__c = st.task_name__c;
                             tst.Billable_Expenses__c=st.Billable_Expenses__c;
                             tst.Bill_Rate__c=st.Bill_Rate__c;
                             tst.Bill_Est_of_Increases__c = st.Bill_Est_of_Increases__c;
                             tst.Bill_Type__c=st.Bill_Type01__c;
                             tst.currencyisocode= s.currencyisocode;
                             tst.Temp_ScopeIt_Project__c=tspt.id;
                             tst.IC_Charge_Type__c = st.IC_Charge_Type__c;//Added as part of #4881(dec 2014 release)
                             tst.IC_charges__c = st.IC_charges__c;//Added as part of #4881(dec 2014 release)
                             database.insert(tst);
                             //proceed only if employees exist for task                     
                             
                             for(Scope_Modeling_Employee__c se:[select id,Bill_Rate__c,Bill_rate_opp__c,Bill_Cost_Ratio__c,Billable_Time_Charges__c,Business__c,Market_Country__c,Cost__c,Hours__c,Employee_Bill_Rate__c from Scope_Modeling_Employee__c
                                                    where Scope_Modeling_Task__c=:st.id]){   
                                     Temp_ScopeIt_Employee__c tse= new Temp_ScopeIt_Employee__c();
                                     tse.Employee__c = se.Employee_Bill_Rate__c;
                                     tse.Bill_Rate__c=se.Bill_rate_opp__c;
                                     tse.Business__c=se.Business__c;
                                     tse.Cost__c=se.Cost__c;
                                     tse.Hours__c=se.Hours__c;
                                     tse.Market_Country__c=se.Market_Country__c;
                                     tse.currencyisocode= s.currencyisocode;
                                     tse.Billable_Time_Charges__c=se.Billable_Time_Charges__c;
                                     tse.Bill_Cost_Ratio__c=se.Bill_Cost_Ratio__c;
                                     tse.Temp_ScopeIt_Task__c=tst.id;
                                     tse.ScopeIt_Project_Template__c = tspt.id;
                                     
                                     //Code updated by Jagan. Instead of inserting here, taking in to list and inserting after the for loop
                                     LstTempScopEmp.add(tse);
                                     //database.insert(tse);  
              
                                 }            
                             }
                         
                             if(LstTempScopEmp.size()>0) {
                                 database.insert(LstTempScopEmp);
                                 LstTempScopEmp.clear();
                         }
                     }      
                 }
                 
             }
       }
    }catch(Exception ex){
            system.debug('Error:'+ex.getStackTraceString());
            
            throw new AuraHandledException('Could not create scope modeling template!');
        }
         return message;
     }
}