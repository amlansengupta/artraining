/* Purpose: This test class provides data coverage for populating Concatenated Ranking associated with 
            an Account 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Gyan            12/19/2013       Created test method for Test_DC32_ConcatenatedRankingBatchable batch class
 ==================================================================================================================*/    
@isTest
public class Test_DC32_ConcatenatedRankingBatchable {
      
    /* * @Description : This test method provides data coverage for Test_DC32_ConcatenatedRankingBatchable batch class 
                        for populating Concatenated Ranking associated with an Account              
       * @ Args       : no arguments        
       * @ Return     : void       
    */        
    static testMethod void Test_DC32_ConcatenatedRankingBatchable() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        insert testAccount;
        
        //Test data for Ranking Record.
        Ranking__c rank = new Ranking__c();
        rank.Account__c = testAccount.Id;
        rank.Ranking_Description__c = 'CAC40';
        rank.Ranking_As_Of__c = System.Today();
        rank.Rank__c = 1;
        insert rank;
        
       //Test Execute Method
        Test.StartTest();
        //Test batch
        DC32_ConcatenatedRankingBatchable batch = new DC32_ConcatenatedRankingBatchable();
        database.executeBatch(batch);   

        Test.stopTest();
        
       
    }
      /* * @Description : This test method provides data coverage For Execute Method Of batch class              
       * @ Args       : no arguments        
       * @ Return     : void       
    */        
    static testMethod void Test_DC32_ConcatenatedRankingBatchable_Schedule() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        insert testAccount;
        Test.StartTest();
        
        //Test data for Ranking Record.
        Ranking__c rank = new Ranking__c();
        rank.Account__c = testAccount.Id;
        rank.Ranking_Description__c = 'CAC40';
        rank.Ranking_As_Of__c = System.Today();
        rank.Rank__c = 1;
        insert rank;

        //Test Execute Method
        
        DC32_ConcatenatedRankingBatchable ac = new DC32_ConcatenatedRankingBatchable();
        DateTime r = DateTime.now();
        String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
        system.schedule('DC32_ConcatenatedRankingBatchable', nextTime, ac);
        
         //Update Rank Object.
        rank.Rank__c = 2;
        update rank;
        
        //Delete Rank Object.
        delete rank;
        
        Test.stopTest();
        
       
    }
}