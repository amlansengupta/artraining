/*
==============================================================================================================================================
Request Id                 					 Date                    Modified By
12638:Removing Step 						 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData=false)
public class ParentChildExpandadTORBatch_Test {

  static testMethod void method1(){
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague'; 
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            insert testAccount;
                
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //request id:12638 commenting step
            //testOppty1.Step__c = 'Closed / Won (SOW Signed)';
            testOppty1.StageName = 'Closed / Won';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Opportunity_Region__c='Europac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            testOppty1.Child_Opp_TOR__c = 1010;
            testOppty1.Amount = 100;
            insert testOppty1;
            
            Opportunity testOppty2 = new Opportunity();
            testOppty2.Amount = 200;
            testOppty2.Name = 'TestOppty';
            testOppty2.Type = 'New Client';
            testOppty2.Parent_Opportunity_Name__c = testOppty1.id;
            testOppty2.AccountId = testAccount.Id;
            //request id:12638 commenting  step
            //testOppty2.Step__c = 'Closed / Won (SOW Signed)';
            testOppty2.StageName = 'Closed / Won';
            testOppty2.CloseDate = date.Today();
            testOppty2.CurrencyIsoCode = 'ALL';
            testOppty2.OwnerId = testUser.Id;
            testOppty2.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty2.Opportunity_Region__c='Europac';
            testOppty2.Sibling_Contact_Name__c=testColleague.id;
            testOppty2.Total_Opportunity_Revenue_USD__c = 250;
            insert testOppty2;            
            
            
         Test.startTest();

            ParentChildExpandadTORBatch obj = new ParentChildExpandadTORBatch();
            DataBase.executeBatch(obj,200); 
            
        Test.stopTest();             
        }
  }
    
}