/**
 * Created by sachinkadian on 10/4/18.
 */

public with sharing class SVNSUMMITS_News_RelatedController {
    @auraEnabled
    public static List<NewsWrapper> getRelatedNewsData(String newsId) {
        Set<Id> setOfRelatedNewsIds = new Set<Id>();
        List<NewsWrapper> listOfNewsWrappers = new List<NewsWrapper>();
        News__c currentNews = [
                select id,RelatedNewsId_1__c,RelatedNewsId_2__c,RelatedNewsId_3__c,
                        RelatedNewsId_4__c,RelatedNewsIcon_1__c,RelatedNewsIcon_2__c,
                        RelatedNewsIcon_3__c,RelatedNewsIcon_4__c
                from News__c
                where id = :newsId
        ];

        if (currentNews.RelatedNewsId_1__c != null) {
            setOfRelatedNewsIds.add(currentNews.RelatedNewsId_1__c);
        }
        if (currentNews.RelatedNewsId_2__c != null) {
            setOfRelatedNewsIds.add(currentNews.RelatedNewsId_2__c);
        }
        if (currentNews.RelatedNewsId_3__c != null) {
            setOfRelatedNewsIds.add(currentNews.RelatedNewsId_3__c);
        }
        if (currentNews.RelatedNewsId_4__c != null) {
            setOfRelatedNewsIds.add(currentNews.RelatedNewsId_4__c);
        }


        //query all the related news
        if (setOfRelatedNewsIds.size() > 0) {
            Map<Id, News__c> mapOfNews = new Map<Id, News__c>([SELECT Id,Name FROM News__c]);
            if (currentNews.RelatedNewsId_1__c != null){
                listOfNewsWrappers.add(new NewsWrapper(currentNews.RelatedNewsIcon_1__c, mapOfNews.get(currentNews.RelatedNewsId_1__c)));
            }
            if (currentNews.RelatedNewsId_2__c != null){
                listOfNewsWrappers.add(new NewsWrapper(currentNews.RelatedNewsIcon_2__c, mapOfNews.get(currentNews.RelatedNewsId_2__c)));
            }
            if (currentNews.RelatedNewsId_3__c != null){
                listOfNewsWrappers.add(new NewsWrapper(currentNews.RelatedNewsIcon_3__c, mapOfNews.get(currentNews.RelatedNewsId_3__c)));
            }
            if (currentNews.RelatedNewsId_4__c != null){
                listOfNewsWrappers.add(new NewsWrapper(currentNews.RelatedNewsIcon_4__c, mapOfNews.get(currentNews.RelatedNewsId_4__c)));
            }
        }

        return listOfNewsWrappers;
    }

    //wrapper class to wrap icons with News
    public class NewsWrapper {
        @auraEnabled public String icon;
        @auraEnabled public News__c news;

        public NewsWrapper(String icon, News__c news) {
            this.icon = icon;
            this.news = news;
        }
    }
}