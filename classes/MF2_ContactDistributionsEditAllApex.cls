/***************************************************************************************************************
User Story : US492
Purpose : This apex class is created as the controller of MF2_ContactDistributionNew which supports the 'New' List Button on Contact Distribution Object.
Created by : Soumil Dasgupta
Created Date : 17/12/2018
Project : MF2
****************************************************************************************************************/
public class MF2_ContactDistributionsEditAllApex {
    
    public Contact contact {get; set;}
    public string contactId {get;set;}
    public boolean hasoptedOutOfEmail {get;set;}
    public String contactName {get;set;}
    
    public MF2_ContactDistributionsEditAllApex(ApexPages.StandardController controller){
        //get the contact ID from the parent record.
        contactId = controller.getId();
        system.debug('ContactId '+ContactId);
        contact = [Select id, HasOptedOutOfEmail, Name from Contact where ID =: ContactId];
        hasoptedOutOfEmail = contact.HasOptedOutOfEmail;
        contactName = contact.Name;
    }
    
}