@isTest
public with sharing class Peak_MyGroupsControllerTest {

    // Note, it's very difficult to insert Group Membership via test classes. I haven't found a suitable approach yet so success routes are not tested.

    @isTest
    public static void testIsNotInGroup() {
        // Create user
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User testUser = peakTestUtils.createStandardUser();

        // Make sure they're not yet in a group
        System.runAs(testUser){
            System.assertEquals(Peak_MyGroupsController.isInAGroup(),false);
        }
    }

    @isTest
    public static void testGetFeaturedGroup() {
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();

        // Insert a collab group
        CollaborationGroup testGroup = peakTestUtils.createGroup(Peak_TestConstants.TEST_GROUPNAME,'Public');
        insert testGroup;

        // Call action and cast result as Collab Group
        Peak_Response peakResponse = Peak_MyGroupsController.getFeaturedGroup(testGroup.Id);
        CollaborationGroup foundGroup = (CollaborationGroup)peakResponse.results[0];

        // Test that the collab group name is accurate
        System.assertEquals(foundGroup.Name,Peak_TestConstants.TEST_GROUPNAME);
    }

    @isTest
    public static void testGetMyGroups() {
        // Create user
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User testUser = peakTestUtils.createStandardUser();

        System.runAs(testUser){
            System.assertEquals(Peak_MyGroupsController.getMyGroups('3').results.size(),0);
        }
    }


    @isTest
    public static void testGetPrefix() {
        system.assert(Peak_MyGroupsController.getSitePrefix() != null);
    }



}