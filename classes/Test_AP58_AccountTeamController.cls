/*
Purpose: This test class proviudes data coverage to Test_AP58_AccountTeamController class
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Shashank 03/29/2013  Created test Class.
=============================================================================================================================================== 
*/
@isTest
private class Test_AP58_AccountTeamController 
{
	/*
     * @Description : Test method to provide data coverage to AP58_AccountTeamController class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
         User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
                
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = 'Health & Benefits';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc1@abc.com';
        testColleague1.Level_1_Descr__c = 'Mercer';
        insert testColleague1;
        
        Extended_Account_Team__c testExtTeam = new Extended_Account_Team__c();
        testExtTeam.Account__c = testAccount.Id;
        testExtTeam.Team_Member_Name__c = testColleague1.Id;
        
        system.runAs(User1){
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(testExtTeam);
        AP58_AccountTeamController controller = new AP58_AccountTeamController(stdController);
        controller.save();
        controller.savenew();
        controller.cancel();
        }
    }
}