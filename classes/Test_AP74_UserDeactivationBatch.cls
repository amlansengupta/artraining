/*Purpose: This test class provides data coverage for deactivating the users with corresponding inactive colleagues.
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       10/30/2013  Created test method for AP74_UserDeactivationBatch batch class
==================================================================================================================*/             
@isTest(seeAllData=true)
    private class Test_AP74_UserDeactivationBatch 
    { 
        /* * @Description : This test method provides data coverage for AP74_UserDeactivationBatch batch class             
           * @ Args       : no arguments        
           * @ Return     : void       
        */    
        static testMethod void testUserDeactivationBatch()
        {
            
           // ByPassErrorLogger__c customSetting  = new ByPassErrorLogger__c();
           // customSetting.name = 'ErrorLogger';
           // customSetting.CreateErrorLog__c = TRUE;
           // insert customSetting;
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'Test Colleague1';
            testColleague.EMPLID__c = '12545676208';       
            testColleague.LOB__c = '111214';       
            testColleague.Last_Name__c = 'test LastName';      
            testColleague.Empl_Status__c = 'Inactive';      
            testColleague.Email_Address__c = 'test1@test1.com';
            testColleague.Deactivated_On__c =  system.today();  
            insert testColleague;
            
           
            User testUser = new User();
            testUser.alias = 'usert1';
            testUser.email = 'test@xyz.com';
            testUser.emailencodingkey='UTF-8';
            testUser.lastName = 'lastName';
            testUser.languagelocalekey='en_US';
            testUser.localesidkey='en_US';
            testUser.ProfileId = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
            testUser.timezonesidkey='Europe/London';
            testUser.UserName = 'abc1.aaa11@xyz.com';
            testUser.EmployeeNumber = '12545676208';
            testUser.isActive = true;
            testUser.S_Dash_Supervisor__c = testUser.id;
            testUser.S_Dash_Supervisor_2__c = null;  
            testUser.S_Dash_Supervisor_3__c = null;
            testUser.S_Dash_Supervisor_4__c = null;  
            testUser.S_Dash_Supervisor_5__c = null;    
            insert testUser;
            
           
            User testUser1 = new User();
            testUser1.alias = 'usert2';
            testUser1.email = 'test@xyz1.com';
            testUser1.emailencodingkey='UTF-8';
            testUser1.lastName = 'lastName';
            testUser1.languagelocalekey='en_US';
            testUser1.localesidkey='en_US';
            testUser1.ProfileId = [Select id from Profile where name = 'System Administrator' limit 1].id;
            testUser1.timezonesidkey='Europe/London';
            testUser1.UserName = 'bbb.zzz@xyz1.com';
            testUser1.EmployeeNumber = '1234567891';
            testUser1.isActive = true;
            testUser1.S_Dash_Supervisor__c = testUser.id;
            testUser1.S_Dash_Supervisor_2__c = testUser.Id;  
            testUser1.S_Dash_Supervisor_3__c = testUser.Id;
            testUser1.S_Dash_Supervisor_4__c = testUser.Id;  
            testUser1.S_Dash_Supervisor_5__c = testUser.Id;    
            insert testUser1;
            
            system.runAs(testUser1){
                Test.StartTest();
                String  qry = 'Select id, Empl_Status__c,EMPLID__c from Colleague__c where EMPLID__c != null and Empl_Status__c like \'Inactive\'  and Deactivated_On__c = TODAY and ID = \''+testColleague.Id +'\'';
                database.executeBatch(new AP74_UserDeactivationBatch(qry), 1);  
                Test.StopTest();
            }
            
       }
       
 }