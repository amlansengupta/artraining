/**
 * Created by sachinkadian on 10/8/18.
 */

@IsTest
private class SVNSUMMITS_News_RelatedControllerTest {
    //Hardcoded Network Id as we can't able to get get Network Id in Test classes.
    public Static String strNetId = '0DBB0000000CamAOAS';
    @IsTest
    static void testGetRelatedNewsData() {
        //set NetworkId variables of the Class with hardcoded value.
        //String strNetId = '0DB36000000PB5MGAW';
        SVNSUMMITS_NewsController.networkId = Id.valueOf(strNetId);
        SVNSUMMITS_NewsController.strnetworkId = strNetId;

        //create News Records
        //Create News in Bulk to check with bulk data.
        List<News__c> objNewzBulkList = SVNSUMMITS_NewsUtility.createBulkNews(4, strNetId);

        News__c newsObj = new News__c(Name='Test News',Publish_DateTime__c = system.today().addDays(-5),
                Author__c = UserInfo.getUserId(),NetworkId__c = strNetId,Summary__c='Summary News 5');

        newsObj.RelatedNewsId_1__c = objNewzBulkList[0].Id;
        newsObj.RelatedNewsId_2__c = objNewzBulkList[1].Id;
        newsObj.RelatedNewsId_3__c = objNewzBulkList[2].Id;
        newsObj.RelatedNewsId_4__c = objNewzBulkList[3].Id;

        newsObj.RelatedNewsIcon_1__c = 'Solution';
        newsObj.RelatedNewsIcon_2__c = 'Case Study';
        newsObj.RelatedNewsIcon_3__c = 'Learn More';
        newsObj.RelatedNewsIcon_4__c = 'Video';
        insert newsObj;

        Test.startTest();
        // call method to get current news record
        List<SVNSUMMITS_News_RelatedController.NewsWrapper> listOfNewsWrapper = SVNSUMMITS_News_RelatedController.getRelatedNewsData(newsObj.Id);

        //make sure we are getting correct data
        System.assert(listOfNewsWrapper[0].icon == 'Solution');
        System.assert(listOfNewsWrapper[0].news.Id ==  objNewzBulkList[0].Id);
        System.assert(listOfNewsWrapper[1].icon == 'Case Study');
        System.assert(listOfNewsWrapper[1].news.Id ==  objNewzBulkList[1].Id);
        System.assert(listOfNewsWrapper[2].icon == 'Learn More');
        System.assert(listOfNewsWrapper[2].news.Id ==  objNewzBulkList[2].Id);
        System.assert(listOfNewsWrapper[3].icon == 'Video');
        System.assert(listOfNewsWrapper[3].news.Id ==  objNewzBulkList[3].Id);
        Test.stopTest();
    }
}