/*
* Request No#:17438: This class is responsible to send mail on Delegate Project Setup to invoke the actions served on the Lightning Component-DelegateProjectSetUpComponent
*/
public class DelegateProjectSetUpController {
    //Request No#:17438:This method fetches all opportunity details so that product infos can be embedded in the mail body
    @AuraEnabled
    public static String pushMailBodyAsHTML(String oppId){
        try{
            //to fetch all opportunity details so that product infos can be embedded in the mail body
            Opportunity opp=[select id,Name,StageName,CloseDate,Type,AccountID,Account.Name,Account.One_Code__c,
                             Total_Opportunity_Revenue_USD__c,Concatenated_Products__c,Opportunity_ID__c,
                             (Select Id, Name, OpportunityId,Project_Manager__r.name, Product2Id,Project_Manager__c,
                              Sales_Price_USD_calc__c,Product_Name_LDASH__c, UnitPrice,Is_Project_Required__c,currencyisocode  
                              FROM OpportunityLineItems)
                             from Opportunity where Id=:oppId];
            return generateMailBody(opp);
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'DelegateProjectSetUpController', 'pushMailBodyAsHTML');
            throw new AuraHandledException('Mail Sending failed! Please try after sometime.');
        }
    }
    //Request No#:17438: This method fetches opportunity details
    @AuraEnabled
    public static Opportunity getOpportunity(String oppId){
        //To fetch opportunity details
        Opportunity opportunity = [Select id,Name,StageName,CloseDate,Type,AccountID,Opp_Number_of_LOBs__c,
                                   Account.One_Code__c,Account.Name,Total_Opportunity_Revenue_USD__c,
                                   Concatenated_Products__c,Opportunity_ID__c
                                   From Opportunity where Id=:oppId];
        return opportunity;
        
    }
    //Request No#:17438: This method is responsible for deleting uploaded document
    @AuraEnabled
    public static void deleteUploadedDocument(String docId){
        try{
            ContentDocument doc = new ContentDocument(Id=docId);
            delete doc;
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'DelegateProjectSetUpController', 'deleteUploadedDocument');
            throw new AuraHandledException('Delete Failed! Please try after sometime.');
        }
    }
    //Request No#:17438: This method is responsible for sending email. It receives email content including recipients and attachments
    //from client side js controller and then it composes and send the email to the intended recipients.
    @AuraEnabled
    public static void sendEmail(String emailContent){
        try{
            if(emailContent != null && !String.isEmpty(emailContent)){
                
                Map<String,Object> mapDeserializedData = (Map<String,Object>)JSON.deserializeUntyped(emailContent);
                system.debug('deserializedData:'+mapDeserializedData);
                if(mapDeserializedData != null){
                    String toRecipients = (String)mapDeserializedData.get('To');
                    String ccRecipients = (String)mapDeserializedData.get('Cc');
                    String bccRecipients = (String)mapDeserializedData.get('Bcc');
                    List<String> listDocumentIds = new List<String>();
                    List<Messaging.EmailFileAttachment> listEMailAttachments = new List<Messaging.EmailFileAttachment>();
                    if(mapDeserializedData.get('Attachments') != null){
                        system.debug('attchments : '+mapDeserializedData.get('Attachments'));
                        List<Object> listAttachmentList = (List<Object>)mapDeserializedData.get('Attachments');
                        //It loops over thorugh all the email attachments
                        for(Object entry : listAttachmentList){
                            Map<String, Object> attachmentMap = (Map<String, Object>)entry;
                            for(String key : attachmentMap.keySet()){
                                
                                listDocumentIds.add((String)attachmentMap.get(key));
                            }
                        }
                        
                        
                        listEMailAttachments = getEmailAttachment(listDocumentIds);
                    }
                    String subject = (String)mapDeserializedData.get('Subject');
                    String emailBody = (String)mapDeserializedData.get('Body');
                    emailBody='<div style="font-family: Calibri; font-size:14px !important;">'+emailBody+'</div>';
                    system.debug('emailBody:'+emailBody);
                    List<String> listToAddresses, listCcAddresses, listBccAddresses;
                    if(toRecipients != null && !String.isEmpty(toRecipients)){
                        listToAddresses = toRecipients.split(';');
                    }
                    if(ccRecipients != null && !String.isEmpty(ccRecipients)){
                        listCcAddresses = ccRecipients.split(';');
                    }
                    if(bccRecipients != null && !String.isEmpty(bccRecipients)){
                        listBccAddresses = bccRecipients.split(';');
                    }
                    composeAndSendEmail(listToAddresses, listCcAddresses, listBccAddresses, listEMailAttachments, subject, emailBody);
                }
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'DelegateProjectSetUpController', 'sendEmail');
            throw new AuraHandledException('Something went wrong!! '+ ex.getMessage());
        }
        
    }
    //Request No#:17438: This method is responsible for composing and sending email
    private static void composeAndSendEmail(List<String> listToRecipients, List<String> listCcRecipients, List<String> listBccRecipients, List<Messaging.EmailFileAttachment> listAttchments, String subject, String body){
        try{
            List<Messaging.SingleEmailMessage> listMails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setFileAttachments(listAttchments );
            mail.setSubject(subject);
            mail.setSaveAsActivity(false);
            mail.setHtmlBody(body);
            mail.setToAddresses(listToRecipients);
            if(listCcRecipients != null && !listCcRecipients.isEmpty()){
                mail.setCcAddresses(listCcRecipients);
            }
            if(listBccRecipients != null && !listBccRecipients.isEmpty()){
                mail.setBccAddresses(listBccRecipients);
            }
            listMails.add(mail);
            Messaging.SendEmailResult [] emailResults = Messaging.sendEmail(listMails);
            system.debug('email results : '+emailResults);
            //It loops over thorugh the email results
            for(Messaging.SendEmailResult emailResult:emailResults){
                if(!emailResult.IsSuccess()){
                    throw new AuraHandledException('Mail Sending failed!! Please retry.');
                }
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'DelegateProjectSetUpController', 'composeAndSendEmail');
            throw new AuraHandledException('Mail Sending failed!! '+ ex.getMessage());                                       
        }
    }
    //Request No#:17438: This method is responsible for fetching Email Attachment
    private static List<Messaging.EmailFileAttachment> getEmailAttachment(List<String> listDocumentIds){
        List<Messaging.EmailFileAttachment> listEmailAttachments = new List<Messaging.EmailFileAttachment>();
        //This is to fetch Title,FileExtension,VersionData from Content Version
        if(listDocumentIds !=null){
        List<ContentVersion> listContentVersions = [SELECT Title,FileExtension,VersionData FROM ContentVersion where ContentDocumentId in : listDocumentIds];
        if(listContentVersions != null){
            //It loops over thorugh the content versions
            for(ContentVersion version : listContentVersions){
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(version.Title + '.' + version.FileExtension);
                efa.setBody(version.VersionData);
                listEmailAttachments.add(efa);
            }
        }
        }
        return listEmailAttachments;
    }
    //Request No#:17438: This method is responsible for generating mail body
    private static String generateMailBody(Opportunity opp){
        String htmlContent = '<div style="font-family: Calibri; font-size:14px !important;">';
        htmlContent += 'Opportunity Owners,'+'<br></br>';
        htmlContent += 'Before sending this email to your delegate:'+'<br></br>';
        htmlContent += '1. Complete <span style="color:darkred; "><b>REQUIRED</b></span> missing details below.'+'<br></br>';
        htmlContent += '2. <span style="color:darkred; "><b>Attach the relevant client agreement and/or note any existing client documents </b></span>that should be linked to the project(s).'+'<br></br>';
        htmlContent += '_____________________'+'<br></br>';
        htmlContent += 'Click the link(s) below to set up chargeable project code(s) in WebCAS for the following MercerForce opportunity:'+'<br></br>';
        htmlContent +='<b>Opportunity Name: </b>' + opp.Name +'<br></br>';
        htmlContent +='<b>Account: </b>'+ opp.Account.Name + ' (' + opp.Account.One_Code__c + ')'+'<br></br>';
        htmlContent +='<b>Total Opportunity Revenue: </b>'+ ((opp.Total_Opportunity_Revenue_USD__c!=null)?(opp.Total_Opportunity_Revenue_USD__c).Format():'')+ ' ' + 'USD' +'<br></br>';
        //It loops over thorugh the Opportunity Line items
        for(OpportunityLineItem ol:opp.OpportunityLineItems){
            String req ='N';
            String manager='';
            if(ol.Is_Project_Required__c == true){
                req = 'Y';
            }else if(ol.Is_Project_Required__c == false){
                req = 'N';
            }
            
            if(ol.Project_Manager__c == null){
                manager = ' ';
            }
            else if(ol.Project_Manager__c != null){
                manager = ol.Project_Manager__r.name;
            }
            htmlContent += '<b>Product: </b>' + ol.Product_Name_LDASH__c + '</br>';
            htmlContent += '<b>Project Required: </b>' + req + '</br>';
            htmlContent += '<a href='+Label.WEbcas_URL+'end_form=gocprojsf~'+ opp.Account.One_Code__c+ '~' + opp.Opportunity_ID__c + '~' + ol.Id +'>'+Label.WEbcas_URL+'end_form=gocprojsf~'+ opp.Account.One_Code__c+ '~' + opp.Opportunity_ID__c + '~' + ol.Id + '</a><br></br>';
            htmlContent += '<b>Charge Basis:</b> <span style="color:darkred; "><b>REQUIRED</b></span><br></br>';
            htmlContent += '<b>Payment Terms:</b> <span style="color:darkred; "><b>REQUIRED in US</b></span><br></br>';
            htmlContent += '<b>Engagement: </b><span style="color:darkred; "><b>REQUIRED - enter existing engagement details or request new</b></span><br></br>';
            htmlContent += '<b>Project Manager: </b>' + manager + '<br></br>';
            //htmlContent += '<b>Project Fee: </b>' + ((ol.Sales_Price_USD_calc__c!=null)?(ol.Sales_Price_USD_calc__c).Format():'') + ' USD' + '<br></br>';
            htmlContent += '<b>Project Fee: </b>' + ((ol.UnitPrice!=null)?(ol.UnitPrice).Format()+' '+ol.currencyisocode:'') + '</br>';
            htmlContent += '<b>Program / Program Manager: </b>Enter a value if required<br></br>'; 
            htmlContent += '<b>Opp Prod Id: </b>' + ol.Id + '<br></br>';
            htmlContent +='</div>';
        }
        return htmlContent;
    }
    
}