/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
 * The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest
public class Mercer_TestData_Bulk {

    private static List<User> requesterList = new List<User>();
    private static List<Account> testAccountList = new List<Account>();
    private static List<AccountTeamMember> testAccountTeamMemberList = new List<AccountTeamMember>();
    private static List<Opportunity> testOpportunityList = new List<Opportunity>();
    private static List<OpportunityTeamMember> testOpportunityTeamMemberList  = new List<OpportunityTeamMember>();
    private static List<Contact> testContactList = new List<Contact>();
    private static List<ContactShare> testContactShareList = new List<ContactShare>();
    private static List<Contact_Team_Member__c> testContactTeamMemberList = new List<Contact_Team_Member__c>();
    private static List<Colleague__c> testColleagueList = new List<Colleague__c>();
    
    private static Opportunity testOpportunity = new Opportunity();
    private static OpportunityTeamMember testOpportunityTeamMember = new OpportunityTeamMember();
    private static Contact testContact = new Contact();
    private static ContactShare testContactShare = new ContactShare();
    private static Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
    private static User requester = new User();
    private static Account testAccount = new Account();
    private static AccountTeamMember testAccountTeamMember = new AccountTeamMember();
    private static Colleague__c testColleague = new Colleague__c();
    
    private static String mercerStandardUserProfile;
    
    /*
     * @Description : Test data for creating Standard User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    static{
              mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
          }


    /*
     * @Description : Test data for creating test user
     * @ Args       : Null
     * @ Return     : void
     */   
    public User createUser(String profileId, String alias, String lastName, User testUser)
    {
     for (Integer i=0;i<200;i++){
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890';
        requesterList.add(testUser);
        //buildColleague(testUser.ID);
        
       }
       return testUser;
    } 
    
    /*
     * @Description : Test data for creating test Opporutnity
     * @ Args       : Null
     * @ Return     : void
     */ 
    public void buildOpportunity(String accID)
    {
      for (Integer i=0;i<200;i++){
        
        //Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  accId; 
        //request id:12638;commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate = system.today();
        testOpportunity.Opportunity_Office__c = 'Aarhus';
        //insert testOpportunity;
        
        //testOpportunity.Name = 'testoppty';
        //testOpportunity.Opportunity_Office__c = 'Aberdeen';
     
        testOpportunityList.add(testOpportunity);
      }
        buildOpportunityTeamMember(testOpportunity.ID);
    
    }
    
    /*
     * @Description : Test data for creating test Opportunity Team Member
     * @ Args       : Null
     * @ Return     : void
     */ 
        public void buildOpportunityTeamMember(String opptyId)
    {
      User opportunityMember;
      for (Integer i=0;i<200;i++){
        opportunityMember = new User();
        opportunityMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName', opportunityMember);      
        
        //testOpportunityTeamMember = new OpportunityTeamMember();
        testOpportunityTeamMember.OpportunityId = opptyId;
        testOpportunityTeamMember.UserId = opportunityMember.ID;
        
        testOpportunityTeamMemberList.add(testOpportunityTeamMember);
        
        testOpportunityTeamMemberList.clear();
      }
    }
    /*
     * @Description : Test data for creating test Contact
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildContact()
    {
      for (Integer i=0;i<200;i++){
        //Contact testContact = new Contact(); 
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContactList.add(testContact);
      }   
        buildContactTeamMember(testContact.ID);
                
        User contactOwner = new User();
        contactOwner = createUser(mercerStandardUserProfile, 'tstUsr2', 'TestLastName2', contactOwner);      
       
        testContact.OwnerID = contactOwner.ID;
        
        update testContact;
        buildContactShare(testContact.ID,testContact.ownerID);
 
    } 
    /*
     * @Description : Test data for creating test Contact Share
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildContactShare(String conID,String conOwner)
    {
         testContactShare.ContactId = conID;                    
         testContactShare.UserOrGroupId = conOwner;                   
         testContactShare.ContactAccessLevel = 'Edit'; 
    }
    
    /*
     * @Description : Test data for creating test Contact Team Member
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildContactTeamMember(String conID)
    {
        User contactMember = new User();
        contactMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName', contactMember);      
        
        testContactTeamMember = new Contact_Team_Member__c();
        testContactTeamMember.Contact__c = conID;
        testContactTeamMember.ContactTeamMember__c = contactMember.ID;
        insert testContactTeamMember;
        
        delete testContactTeamMember;
       
    }
    
    /*
     * @Description : Test data for creating test Colleague
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildColleague ()
    {
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        insert testColleague;
        buildAccount(testcolleague.Id);
        testColleague.LOB__c = '22222';
        update testColleague;        
    }
    
    /*
     * @Description : Test data for creating test Colleague search
     * @ Args       : Null
     * @ Return     : void
     */
    public String buildColleaguesearch ()
    {
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        insert testColleague;
        return testColleague.Id;     
    }
    
      /*
     * @Description : Test data for fetching mercer Standard User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    public static String getmercerStandardUserProfile()
    {
        return [Select Id, Name FROM Profile where Name = 'Mercer Standard' LIMIT 1].Id;
    }
     /*
     * @Description : Test data for creating test Requester
     * @ Args       : Null
     * @ Return     : void
     */
        public void createRequester()
    {
        requester = createUser(mercerStandardUserProfile, 'testUser', 'testLastName', requester);    
    }
    
     /*
     * @Description : Test data for creating test Account
     * @ Args       : Null
     * @ Return     : void
     */    
    public void buildAccount(String collID)
    {
        testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = collID;
        insert testAccount;
        
        buildAccountTeamMember(testAccount.ID);
        
    
    }
    /*
     * @Description : Test data for creating test Account Team Member
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildAccountTeamMember(String accId)
    {
        User accountMember = new User();
        accountMember = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', accountMember);
        //opportunityMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName', opportunityMember);      
        
        testAccountTeamMember = new AccountTeamMember();
        //testOpportunityTeamMember = new OpportunityTeamMember();
        testAccountTeamMember.AccountId = accId;
        //testOpportunityTeamMember.OpportunityId = opptyId;
         testAccountTeamMember.UserId = accountMember.Id;
        insert testAccountTeamMember;
        
        delete testAccountTeamMember;
       
    }
    
}