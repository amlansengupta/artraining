/*
 Purpose: This Class contains logic for creating error logs in a batch class         
 ==============================================================================================================================================
 History 
 ----------------------- 
 VERSION     AUTHOR    DATE        DETAIL    
 1.0 -    	Arijit Roy 05/18/2013  Created Controller Class
 ============================================================================================================================================== 
 */
public class MercerAccountStatusBatchHelper {
	/*
     * @Description : Method for calling next batch
     * @ Args       : String batchName
     * @ Return     : void
     */
	public static void callNextBatch(String batchName)
	{
		DateTime sysTime = System.now();
		sysTime = sysTime.addSeconds(120);
		String CRON_EXP = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		
		if(batchName.equalsIgnoreCase('Opportunity Stage'))
		{
			MS18_AccountStatusSchedulable status = new MS18_AccountStatusSchedulable();
			System.schedule('Opportunity stage Identifier ' + String.valueOf(Date.Today()), CRON_EXP, status);
		}
		else if(batchName.equalsIgnoreCase('Recent Win'))
		{
			MS03_OpportunityClosedWonSchedulable recentWin = new MS03_OpportunityClosedWonSchedulable();
        	System.schedule('Recent Win ' + String.valueOf(Date.Today()), CRON_EXP, recentWin);  
		}
		else if(batchName.equalsIgnoreCase('High Revenue'))
		{
			MS05_OpportunityRevenueSchedulable highRevenue = new MS05_OpportunityRevenueSchedulable();
			System.schedule('High Revenue ' + String.valueOf(Date.Today()), CRON_EXP, highRevenue);
		}
		else if(batchName.equalsIgnoreCase('Low Revenue'))
		{
			MS07_OpportunityRevenueSchedulable lowRevenue = new MS07_OpportunityRevenueSchedulable();
			System.schedule('Low Revenue ' + String.valueOf(Date.Today()), CRON_EXP, lowRevenue);
		}else if(batchName.equalsIgnoreCase('Active Marketing Contact'))
		{
			MS09_ActiveMarketingContactSchedulable activeMarketing = new MS09_ActiveMarketingContactSchedulable();
			System.schedule('Active Marketing ' + String.valueOf(Date.Today()), CRON_EXP, activeMarketing);
		}
		else if(batchName.equalsIgnoreCase('Has Activities'))
		{
			MS11_AccountActivitiesSchedulable hasActivities = new MS11_AccountActivitiesSchedulable();
			System.schedule('Has Activities ' + String.valueOf(Date.Today()), CRON_EXP, hasActivities);
		}
		else if(batchName.equalsIgnoreCase('Is Competitor'))
		{
			MS13_AccountCompetitorSchedulable competitor = new MS13_AccountCompetitorSchedulable();
			System.schedule('Is Competitor ' + String.valueOf(Date.Today()), CRON_EXP, competitor);
		}
		else if(batchName.equalsIgnoreCase('Has No Activities'))
		{
			MS15_AccountNoActivitySchedulable noActivity = new MS15_AccountNoActivitySchedulable ();
			System.schedule('Has No Activities ' + String.valueOf(Date.Today()), CRON_EXP, noActivity);
		}
		else if(batchName.equalsIgnoreCase('No Activity Obsolete'))
		{
			MS20_NoActivityObsoleteSchedulable obsolete = new MS20_NoActivityObsoleteSchedulable();
			System.schedule('No Activity - Obsolete ' + String.valueOf(Date.Today()), CRON_EXP, obsolete);
		}
	}
	
	/*
	*	Convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
	*/
    public static String quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }
    /*
     * @Description : Method for storing data in BatchErrorLogger
     * @ Args       : String msg, List<BatchErrorLogger__c> errorLogList, String recordId
     * @ Return     : List<BatchErrorLogger__c>
     */
    public static List<BatchErrorLogger__c> addToErrorLog(String msg, List<BatchErrorLogger__c> errorLogList, String recordId)
    {
    	BatchErrorLogger__c errorLog = new BatchErrorLogger__c();
    	errorLog.Record_Id__c = recordId;
    	errorLog.Failed_Reason__c = msg;
    	errorLogList.add(errorLog);
    	return errorLogList;
    }
    /*
     * @Description : Method to abort all jobs
     * @ Args       : null
     * @ Return     : void
     */
    public static void abortAllJobs()
    {
    	  List<CronTrigger> listCronTrigger = [select Id from CronTrigger where State = 'DELETED' AND nextfiretime = null AND TimesTriggered = 1 AND PreviousFireTime = TODAY];    	
    	  If (listCronTrigger.size() > 0)
		  {
		  		for (Integer i = 0; i < listCronTrigger.size(); i++)
		   		{ 
		   			System.abortJob(listCronTrigger[i].Id); 
		   		}
		  }
    }
    /*
     * @Description : Method for creating error log
     * @ Args       : null
     * @ Return     : boolean 
     */
    public static boolean createErrorLog()
    {
    	return ByPassErrorLogger__c.getInstance('ErrorLogger').CreateErrorLog__c;
    }
}