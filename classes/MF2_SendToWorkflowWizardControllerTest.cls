/***************************************************************************************************************
User Story : US486
Purpose : This Test Class is created for MF2_SendToWorkflowWizardController class.
Created by : Soumil Dasgupta
Created Date : 7/12/2018
Project : MF2
****************************************************************************************************************/

@isTest(SeeAllData=False)
public class MF2_SendToWorkflowWizardControllerTest {
    
    private static Mercer_TestData mtdata = new Mercer_TestData();
    
    public static testmethod void sendWorkflowWizardTestPositive(){
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        
        Account acc = [Select ID from Account where Name = 'TestAccountName1'];
        acc.One_Code_Status__c = 'Pending - Workflow Wizard';
        acc.Integration_Status__c = 'Error';
        Update acc;
        Account accq = [Select ID, name from Account where Name = 'TestAccountName1'];
        String accID = accq.ID;
        String URL = 'abc';
        
        MF2_SendToWorkflowWizardController.fetchAccount(accID,URL);
        system.assertEquals(accq.name, 'TestAccountName1');
    }
    
    public static testmethod void sendWorkflowWizardTestNegative(){
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        
        insert testAccount1;
        
        Account acc = [Select ID, name from Account where Name = 'TestAccountName1'];
        
        String accID = acc.ID;
        String URL = 'abc';
        
        MF2_SendToWorkflowWizardController.fetchAccount(accID,URL);
        system.assertEquals(acc.name, 'TestAccountName1');
    }
    
    public static testmethod void sendWorkflowWizardTestNegative2(){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        
        insert testAccount1;
        
        Account acc = [Select ID, name from Account where Name = 'TestAccountName1'];
        
        String accID = acc.ID;
        String URL = 'abc';
        
        System.runAs(user1) {
            MF2_SendToWorkflowWizardController.fetchAccount(accID,URL);
        }
        system.assertEquals(acc.name, 'TestAccountName1');
    }
}