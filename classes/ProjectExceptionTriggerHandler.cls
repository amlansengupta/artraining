public class ProjectExceptionTriggerHandler implements ITriggerHandler
{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('Project Exception Trigger');
    
        if (TrgSetting.Object_API_Name__c == 'Project_Exception__c' && TrgSetting.Trigger_Disabled__c== true) {
            return true;
        }else{
            return TriggerDisabled;
        }
    }
 //Removed Method argument
    public void BeforeInsert(List<SObject> newItems) {   
    }
 
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
    }
 
    public void BeforeDelete(Map<Id, SObject> oldItems) {
     Set<String> prodCode = new Set<string>();
        for(Project_Exception__c pe:[select id, Product_code__c from Project_Exception__c where id in:oldItems.keyset()]){
            prodCode.add(pe.Product_Code__c);
        }
        if(!prodCode.isEmpty()){
            List<Product2> prodList = new List<Product2>();
            for(Product2 pro:[select id,ProductCode, Product_Exception__c from Product2 where ProductCode in:prodCode]){
               pro.Product_Exception__c = false;
               prodList.add(pro); 
            }
            if(!prodList.isEmpty()){
                update prodList;
            }
    }   
        
    }
 
    public void AfterInsert(List<SObject> newItems,Map<Id, SObject> newItemsMap) {
        markProductAsException(newItemsMap);
    }
 
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
        
        markProductAsException(newItemsMap);
    }
 
    public void AfterDelete(Map<Id, SObject> oldItems) {
        System.debug('In after delete');
       /* Set<String> prodCode = new Set<string>();
        for(Project_Exception__c pe:[select id, Product_code__c from Project_Exception__c where id in:oldItems.keyset()]){
            prodCode.add(pe.Product_Code__c);
        }
        if(!prodCode.isEmpty()){
            List<Product2> prodList = new List<Product2>();
            for(Product2 pro:[select id,ProductCode, Product_Exception__c from Product2 where ProductCode in:prodCode]){
               pro.Product_Exception__c = false;
               prodList.add(pro); 
            }
            if(!prodList.isEmpty()){
                update prodList;
            }
    }*/
    }
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    
    /*
        This Method will check for the exception products and will mark the exception__c 
        field of product as true
    */
    public static void markProductAsException(Map<Id, SObject> newMap){
    
        List<String> ProdCode=new List<String>();
        
        Map<String,Set<String>> combExCode= new Map<String,Set<String>>();        
        
        Map<Id,Project_Exception__c> ProjectExceptionMap= new Map<Id,Project_Exception__c>([Select Exception_Type__c,Product_Code__c,One_Code__c,Office__c,Record_Type_Name__c,LOB__c,Opportunity_Type__c from Project_Exception__c where Id In:newMap.keySet()]);
        List<String> ProdOffCode=new List<String>();
        for(Project_Exception__c projEx:ProjectExceptionMap.values()){
            
            if(ProjectExceptionMap.get(projEx.id).Exception_Type__c=='Product Code' && ProjectExceptionMap.get(projEx.id).Product_Code__c !=null){
                
                ProdCode.add(ProjectExceptionMap.get(projEx.id).Product_Code__c);
                
            }else if(ProjectExceptionMap.get(projEx.id).Exception_Type__c=='Opportunity Country + LoB' || ProjectExceptionMap.get(projEx.id).Exception_Type__c=='Opportunity Type + Opp Country + Product Code' || ProjectExceptionMap.get(projEx.id).Exception_Type__c=='Opportunity Country + Product code'){
             
                if(!combExCode.containsKey(ProjectExceptionMap.get(projEx.id).Product_Code__c)){
                     combExCode.put(ProjectExceptionMap.get(projEx.id).Product_Code__c, new Set<String>{ProjectExceptionMap.get(projEx.id).Exception_Type__c});
            
                }else{
                     combExCode.get(ProjectExceptionMap.get(projEx.id).Product_Code__c).add(ProjectExceptionMap.get(projEx.id).Exception_Type__c);
                }
            System.debug('combExCode********'+combExCode);
            }
        }
        
        if(ProdCode.Size()>0){
            
            List<Product2> prodList=[Select id,ProductCode from Product2 where ProductCode IN:ProdCode];
            
            List<Product2> prodToUpdate=new List<Product2>();
            
            if(prodList.size()>0){
        
                for(Product2 pro:prodList){
                    pro.Product_Exception__c=True;            
                    pro.Exception__c=false;
                    pro.Exempted_Office__c='';
                    prodToUpdate.add(pro);
                }
                
                update prodToUpdate;
            }
        
        }
        if(combExCode.Size()>0){
            
            List<Product2> prodOffList=[Select id,ProductCode,Exempted_Office__c,Exception__c,Product_exception__c from Product2 where ProductCode IN:combExCode.keySet()];
            
            List<Product2> prodOffToUpdate=new List<Product2>();
        
            if(prodOffList.size()>0){
            
                for(Product2 pro:prodOffList){            
                    pro.Exception__c=true;
                    pro.Product_exception__c=false;
                                    
                       
                    prodOffToUpdate.add(pro);
                }
                system.debug('prodOffToUpdate*******'+prodOffToUpdate);
                update prodOffToUpdate;
            }
        }
        
        
    }

}