global class MF2_OppLineItemUpdateBatch implements Database.Batchable<sObject>{
    //public static boolean OppUpdateFlag = true;
    global string option;
    global MF2_OppLineItemUpdateBatch(string option){
        this.option = option;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        string OppLI;
        if(option.equalsIgnoreCase('Open')){
         OppLI='select id from OpportunityLineItem where Opportunity.isClosed = false and createddate>=2019-09-15T00:00:00.000+0000';
        }
        else if(option.equalsIgnoreCase('Closed')){OppLI='select id from OpportunityLineItem where Opportunity.isClosed = true and createddate>=2019-09-15T00:00:00.000+0000';}
            return Database.getQueryLocator(OppLI);
        
    }
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> scope){
         update scope;
        }
        
    
    
    global void finish(Database.BatchableContext bc){
    }
        
}