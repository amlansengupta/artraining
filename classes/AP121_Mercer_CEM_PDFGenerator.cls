/*Purpose:  This class is used  print   selected CEM record
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   11/06/2014   As per december Release 2014(Request #2264) ,created class for CEM print functionality
                                   Created  VF page "Mercer_CEM_PDF" as part of this request
============================================================================================================================================== 
*/
public with sharing class AP121_Mercer_CEM_PDFGenerator {
        public Client_Engagement_Measurement_Interviews__c CEM{set;get;}
        public List<Task> tasklist{get;set;}
        
        /*
             * @Description : Constructor of Controller class 
             * @ Args       : controller
             * @ Return     : null 
         */     
        public AP121_Mercer_CEM_PDFGenerator(ApexPages.StandardController controller){
                CEM = [select id,CEM_Scribe__c,
                            X1_1_Overall_Health_Relationship__c,X1_2_Individual_TeamMember_Assessment__c,X1_3_1_EH_B_Rating__c,Global_Business_Solutions__c,
                            X1_3_3_Retirement_Rating__c,X1_3_4_Talent_Rating__c,X1_4_Mercer_contribution_for_success__c,X2_1_Industry_challeges_Priorities__c,
                            X2_2_Senior_leadership_challenges_Issues__c,X2_3_Add_company_values__c,X2_4_Top_3to5_Priorities_for_your_Org__c,
                            X2_5_Top_3to5_Priorties_to_support_for_y__c,Notes__c,
                            X2_6_Mercer_support_To_your_challenges__c,X3_1_1_Mercer_relationship_gaps__c,
                            X3_1_2_Improve_Mercer_relationship__c,X3_1_3_Who_else_conduct_this_type_of_Int__c,X3_1_Mercer_connections_within_your_org__c,
                            X3_2_1_Mercer_competetive_advantages__c,X3_2_2_Your_Impression_of_our_competitor__c,X3_2_Your_Impression_of_Mercer__c,
                            X3_3_Firms_do_you_work_with_and_Compare__c,X4_1_1_How_to_Mitigate_the_issues__c,X4_1_Is_Mercer_relationship_at_risk__c,
                            X5_1_SWOT_Analysis_Strengths__c,X5_2_SWOT_Analysis_Opportunities__c,X5_3_SWOT_Analysis_Strength__c,X5_4_SWOT_Analysis_Threats__c,
                            X6_1_Closing_Additional_Comments__c,Account__c,Account__r.name,Account__r.Relationship_Manager__r.name,CEM__c,Reviewer__c,CEM_Interviewer_2__c,Client_Contact_Reviewer_1__c,
                            Client_Contact_Reviewer_2__c,Client_Contact_Reviewer_3__c,Competitor_Innovative_Score__c,Competitor_Partnership_Score__c,
                            Competitor_Planning_Project_Mgmt_Score__c,Competitor_Proactive_Score__c,Competitor_Quality_Score__c,Competitor_Service_Communication_Score__c,
                            Competitor_Value_Score__c,Date_of_Review__c,Mercer_Innovative_Score__c,Mercer_Partnership_Score__c,Mercer_Planning_Project_Management_Scr__c,
                            Mercer_Proactive_Score__c,Mercer_Quality_Score__c,Mercer_Service_Communication_Score__c,Mercer_Value_Score__c,Overall_rating__c,Status__c
                            ,CEM_Scribe__r.name,Reviewer__r.Name,CEM_Interviewer_2__r.name,Client_Contact_Reviewer_1__r.name,Client_Contact_Reviewer_2__r.name,
                            Client_Contact_Reviewer_3__r.name,Mercer_C_Suite_Readiness_Score__c,Mercer_Diversity_Inclusion_Score__c,Competitor_C_Suite_Readiness_Score__c,Competitor_Diversity_Inclusion_Score__c,
                            C_Suite_Readiness_Comments__c,Diversity_Inclusion_Comments__c,Innovative_Comments__c,Partnership_Comments__c,Planning_Project_Management_Comments__c,
                            Proactive_Comments__c,Quality_Comments__c,Service_Communication_Comments__c,Value_Comments__c
                            from 
                            Client_Engagement_Measurement_Interviews__c where id=:apexpages.currentpage().getparameters().get(ConstantsUtility.STR_ObjID)];
                            tasklist =[select subject,ActivityDate,Status,Priority,ownerid,owner.name,Description from Task where whatID=:apexpages.currentpage().getparameters().get(ConstantsUtility.STR_ObjID)];
        }
        

}