global class BatchToCopySODataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopySODataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
         String header = 'Account,Growth Plan,Opportunity,Record Status \n';
        string hearder2= 'Id,Account,Growth Plan,Opportunity,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>(); 
         System.debug('swot OPP>>oldGPList>> '+oldGPList.size());
        try {
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
				
		}
		
			
		Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
		List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
		for(Account a : accList){
			for(Growth_Plan__c gg : a.Growth_Plans__r){
				accIdVsNewGp.put(gg.Account__c,gg);
			}  
		}
        
        System.debug('swot OPP>>accList>> '+accList.size());
        List<Swot_Opportunity__c> insertNewSOpObjList = new List<Swot_Opportunity__c>();	 
        List<Swot_Opportunity__c> soObjectList = [select id,name,Account__c,Growth_Plan__c,Opportunity__c,LastModifiedBy.Name,lastModifiedDate from Swot_Opportunity__c where Growth_Plan__c IN :OldGPList];
        System.debug('swot OPP>>soObjectList>> '+soObjectList.size());
        if(soObjectList != null && soObjectList.size()> 0){
            for(Swot_Opportunity__c soOldObj : soObjectList){
                if(accIdVsNewGp.containsKey(soOldObj.Account__c)){
                    Swot_Opportunity__c soNewObj = new Swot_Opportunity__c();
                    
                    if(soOldObj.Account__c != null)
                        soNewObj.Account__c = soOldObj.Account__c;
                    soNewObj.Growth_Plan__c = accIdVsNewGp.get(soOldObj.Account__c).Id;
                    
                    if(soOldObj.Opportunity__c != null)
                        soNewObj.Opportunity__c = soOldObj.Opportunity__c;
                    
                    soNewObj.Opportunity_LastModified_By__c = soOldObj.LastModifiedBy.Name;
                    DateTime dt = soOldObj.LastModifiedDate;
                    soNewObj.Opportunity_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
                   
                   insertNewSOpObjList.add(soNewObj);
                }
            }
        }
        System.debug('swot OPP>>insertNewSOpObjList>> '+insertNewSOpObjList.size());
          if(insertNewSOpObjList!=null && insertNewSOpObjList.size()>0){
    		Database.SaveResult[] srList = Database.insert(insertNewSOpObjList,false);
    		if(srList!=null && srList.size()>0){
				for(Integer i=0;i<srList.size();i++){
					String opp='';
					if(insertNewSOpObjList[i].Opportunity__c!=null)
						opp=insertNewSOpObjList[i].Opportunity__c;
						
					if(srList.get(i).isSuccess()){
						finalSuccessStr+=srList.get(i).getId()+','+insertNewSOpObjList[i].Account__c+','+insertNewSOpObjList[i].Growth_Plan__c+','+opp.replace(',', '')+', Successfully created \n';
					}
					if(!srList.get(i).isSuccess()){
						Database.Error error = srList.get(i).getErrors().get(0);
						finalFailStr+=insertNewSOpObjList[i].Account__c+','+insertNewSOpObjList[i].Growth_Plan__c+','+opp.replace(',', '')+','+error.getMessage()+' \n';
					}
				}
			}
    	}
    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }
    }
    
    global void finish(Database.BatchableContext bc){
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Swot Opportunity Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Swot Opportunity Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Swot Opportunity Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Swot Opportunity copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}