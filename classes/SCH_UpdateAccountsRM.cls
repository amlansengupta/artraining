global class SCH_UpdateAccountsRM implements Schedulable
{
    global SCH_UpdateAccountsRM(){
    }
    
    global void execute(SchedulableContext context)
    {
        String query='Select ID,ByPassVR__c,Relationship_Manager__c,Account_Market__c,Account_Region__c,Managed_Office_Details__c,Account_Sub_Market__c,Account_Sub_Region__c,Office_Code__c,Account_Country__c,Account_Office__c,Relationship_Manager__r.CountryGeo__c,Relationship_Manager__r.Office__c,Relationship_Manager__r.Sub_Region__c,Relationship_Manager__r.Office_Record_Name__c,Relationship_Manager__r.Sub_Market__c,Relationship_Manager__r.Region__c,Relationship_Manager__r.Market__c,Relationship_Manager__r.Location_Changed__c from Account where Relationship_Manager__r.Location_Changed__c= true'; 
        BATCH_UpdateAccountsRM b = new BATCH_UpdateAccountsRM(query);     
        Id batchJobId = Database.executeBatch(b,200);
    }
}