/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest
public class DelegateCodeSetUpEmailController_Test{
    
    private static Product2 testProduct = new Product2();
    private static Mercer_TestData mtdata = new Mercer_TestData();
     static testMethod void myUnitTest1()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Switch_Settings__c ss=new Switch_Settings__c();
        ss.SetupOwnerId=User1.id;//Test user id
        ss.IsOpportunityProductTriggerActive__c=false;
        ss.IsOpportunityProductValidationActive__c=false;
        ss.IsOpportunityTriggerActive__c=false;
        ss.IsOpportunityValidationActive__c=false;
        insert ss;
        
        system.runAs(User1){ 
            
            Account acct = mtdata.buildAccount();
            
          
            //test.startTest();           
           
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty1';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acct.id;
            //Request Id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty.Total_Opportunity_Revenue_USD__c=9.7;
            insert testOppty;
                        
            test.startTest(); 
            
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            //attach.parentId=testOppty.id;
            //insert attach;
            test.stoptest();
            
            ApexPages.currentPage().getParameters().put('id',testOppty.id);
            ApexPages.currentPage().getParameters().put('emailToAdd','test@test.com');
            //Modified for Request #19079 - Start
            ApexPages.currentPage().getParameters().put('subjectToAdd','Project Created Successfully.');
            //Modified for Request #19079 - End
            ApexPages.StandardController controller =new ApexPages.StandardController(testOppty);
            DelegateCodeSetUpEmailController delegate = New DelegateCodeSetUpEmailController();
            DelegateCodeSetUpEmailController delegate1 = New DelegateCodeSetUpEmailController(controller);
            delegate1.emailTo='test@test.com';
            delegate1.emailCc='test@test.com';
            delegate1.templateSubject='test class';
            delegate1.templateBody='test';
            delegate1.attachment=attach;
            delegate1.AttachmentBody=bodyBlob;
            delegate1.AttachmentName='test';
            //delegate1.addAttachment();
            delegate1.doAttach();
            delegate1.SendEmail();
            //Modified for Request #19079 - Start
            delegate1.deleteAttachment();
            //Modified for Request #19079 - End
            
            
        }
    }
    
    @isTest(SeeAllData=true)
     static void myUnitTest2()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = '1234';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        testColleague1.Empl_Status__c = 'Active';
        insert testColleague1;
        
        Switch_Settings__c ss=new Switch_Settings__c();
        ss.SetupOwnerId=User1.id;//Test user id
        ss.IsOpportunityProductTriggerActive__c=True;
        ss.IsOpportunityProductValidationActive__c=false;
        ss.IsOpportunityTriggerActive__c=True;
        ss.IsOpportunityValidationActive__c=false;      
        insert ss;

        
        system.runAs(User1){ 
            
            Account acct = mtdata.buildAccount();
            
            
          
            //test.startTest();           
            Product2 testProduct2 = new Product2();
            testProduct2.Name = 'TestPro';
            testProduct2.Family = 'RRF';
            testProduct2.IsActive = True;
            testProduct2.LOB__c = 'TEST Bus';
            testProduct2.Segment__c = 'TEST seg';
            testProduct2.Classification__c ='Non Consulting Solution Area';
            testProduct2.ProductCode='7576';
            insert testProduct2;
            
            Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
                  
            
            PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :testProduct2.Id and CurrencyIsoCode = 'ALL' limit 1];
            
            /*Project_Exception__c pe1=new Project_Exception__c();
            pe1.Exception_Type__c='Product Code';
            pe1.Product_Code__c='7576';
            insert pe1;*/
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty1';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acct.id;
            //Request Id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Pricebook2Id = pb.id;
            testOppty.Opportunity_Office__c = 'Shanghai - Huai Hai';
            //Modified for Request #19079 - Start
            testOppty.Total_Opportunity_Revenue_USD__c=9.7;
            //Modified for Request #19079 - End
            insert testOppty;
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptyLineItem.Revenue_End_Date__c = date.Today()+30;
            opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
            opptylineItem.UnitPrice = 1.00;
            insert opptylineItem;
            
            opptylineItem.Is_Project_Required__c =false;
            opptylineItem.Project_Manager__c=testColleague1.id;
            update opptylineItem;
            
            
            test.startTest(); 
            
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            //attach.parentId=testOppty.id;
            //insert attach;
            test.stoptest();
            
            ApexPages.currentPage().getParameters().put('id',testOppty.id);
            ApexPages.StandardController controller =new ApexPages.StandardController(testOppty);
            DelegateCodeSetUpEmailController delegate = New DelegateCodeSetUpEmailController();
            DelegateCodeSetUpEmailController delegate1 = New DelegateCodeSetUpEmailController(controller);
            delegate1.emailBcc='test@test.com';
            delegate1.pageMessage='test@test.com';
            
            
            delegate1.emailTo='test@test.com;test1@test.com';
            delegate1.emailCc='test@test.com;test1@test.com';
            delegate1.templateSubject='test class';
            delegate1.templateBody='test';
            delegate1.attachment=attach;
            delegate1.AttachmentBody=bodyBlob;
            delegate1.AttachmentName='test';
            //delegate1.addAttachment();
            delegate1.SendEmail();
            
            
        }
    }
}