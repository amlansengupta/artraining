/***************************************************************************************************************
Request Id : 17173
Purpose : This class is created for checking locked and unlocked Record Types
Created by : Archisman Ghosh
Created Date : 16/05/2019
Project : MF2
****************************************************************************************************************/
public class OpportunityUtility {
    @Auraenabled
    public static boolean recordTypeCheck(String oppId)
    {
        boolean flag = false;
        Opportunity opp=null;
        List<Opportunity> listOpp=[Select ID,RecordType.Name from Opportunity where Id =: oppId];
        try{
            if(listOpp!=null && listOpp.size()>0)
            {
                opp=listOpp.get(0);
            }
            String unlockedRecord = System.Label.UnlockedRecordType;
            List<String> listUnlockedRecord = unlockedRecord.split(',');
            if(listUnlockedRecord.contains(opp.RecordType.Name))
            {
                flag=true;
            }
        }
        catch(Exception ex){
            system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'OpportunityUtility', 'recordTypeCheck');
            throw new AuraHandledException('Could not fetch Opportunity Record Types!');
        }
        return flag;
    }
    @AuraEnabled
    public static Id saveOpp(String fieldSet, String oppID)
    {
        //SavePoint sp;
        Opportunity newRecord=new Opportunity();
        try
        {
            //sp = Database.setSavePoint();
            System.debug('fieldSet'+fieldSet);
            //Profile prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
            
            Map<String,Object> deserializedData = (Map<String,Object>)JSON.deserializeUntyped(fieldSet);
            system.debug('deserializedData:'+deserializedData);
            newRecord.Id = oppID;
            if(deserializedData.get('Sibling_Company__c')!=null)
            {
                newRecord.Sibling_Company__c = (String)deserializedData.get('Sibling_Company__c');
            }
            else{
                newRecord.Sibling_Company__c=null;
            }
            if(deserializedData.get('Business_LOB__c')!=null)
            {
                newRecord.Business_LOB__c = (String)deserializedData.get('Business_LOB__c');
            }
            else{
              newRecord.Business_LOB__c=null;  
            }
            if(deserializedData.get('Sibling_Contact_Name__c')!=null)
            {    
                newRecord.Sibling_Contact_Name__c = (Id)deserializedData.get('Sibling_Contact_Name__c');
            }
            else{
                newRecord.Sibling_Contact_Name__c = null;
            }
            if(deserializedData.get('Potential_Revenue__c')!=null)
            {   
                system.debug(deserializedData.get('Potential_Revenue__c')+'Hello');
                if(deserializedData.get('Potential_Revenue__c') instanceOf String){
                    system.debug('Inside if');
                        newRecord.Potential_Revenue__c = Decimal.valueOf(deserializedData.get('Potential_Revenue__c').toString());
                }
                else{
                    system.debug('Inside else');
                    newRecord.Potential_Revenue__c = (Decimal)deserializedData.get('Potential_Revenue__c');
                }
            }
            else{
                newRecord.Potential_Revenue__c=null;
            }
            if(deserializedData.get('Cross_Sell_ID__c')!=null)
            {
                newRecord.Cross_Sell_ID__c = (String)deserializedData.get('Cross_Sell_ID__c');
            }
            else{
                newRecord.Cross_Sell_ID__c = null;
            }
            system.debug('newrecord' + newrecord);
            
                update newRecord;
                system.debug('Updated');
                 
        }
        catch(Exception e)
        {
            String err = ''+e.getMessage();
            system.debug('error:'+e.getMessage());
            if(err.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                throw new AuraHandledException(err.substring(err.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')+35,err.length()-4));   
            }else if(err.contains('INSUFFICIENT_ACCESS_OR_READONLY')){
                throw new AuraHandledException('INSUFFICIENT ACCESS');
            }
            else{
                throw new  AuraHandledException(err); 
            }
        }
        return newRecord.Id;
    }
}