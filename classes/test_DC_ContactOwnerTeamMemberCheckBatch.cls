/*Purpose: Test Class for providing code coverage to insert the missing contact team member records for contact owners
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   03/21/2014  Created test class
============================================================================================================================================== 
*/
@isTest
public class test_DC_ContactOwnerTeamMemberCheckBatch{
    /*
     * @Description : Test method to provide data coverage to insert the missing contact team member records for contact owners
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void test_contact_share(){
    
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        User tUser = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        Mercer_TestData mtest = new  Mercer_TestData ();
        tUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser);  
        User tUser2 = new user();
        tUser2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser2);  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        insert testAccount;
        
        contact c = new contact();
        c.firstname ='test firstname';
        c.lastname = 'test lastname';
        c.accountid = testAccount.id;
        c.email = 'xyzq2@abc05.com';
        insert c;
        
        c.ownerid=tuser2.id;
        update c;
        
        
        contact_team_member__c[] ctm =[select id,contactteammember__c from contact_team_member__c where contact__c =:c.id and contactteammember__c =:tuser.id];
        if(ctm.size()>0)
        delete ctm;
        
        //c.ownerid=tuser.id;
        //update c;
        
        system.runAs(tUser){
            ID Batchprocessid = database.executeBatch(new DC_ContactOwnerTeamMemberCheckBatch(), 1000);  
        }
        
    
    }

}