/*
=======================================================
Modified By 	Date         Request
Trisha			18-Jan-2019  12638-Commenting Oppotunity Step
=======================================================
*/
@isTest(SeeAllData = TRUE)
private class Email_Notification_Opp_Owner_change_test{

 
      static testMethod void send_email_on_OppOwnerchange()
    {
        
       
       

          Colleague__c testColleague = new Colleague__c();
          testColleague.Name = 'TestColleague';
          testColleague.Last_Name__c = 'TestLastName';
          testColleague.EMPLID__c = '1234567112';
          testColleague.LOB__c = 'Employee Health & Benefits';
          testColleague.Empl_Status__c = 'Active';
          testColleague.Email_Address__c = 'abc@abc.com';
          testColleague.Country__c = 'USA';
          insert testColleague;
          
Opportunity testOppty = new Opportunity();
Account testAccount = new Account();
         User user1 = new User();
         User user3 = new User();
         String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
         user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
          system.runAs(User1){
          
          testAccount.Name = 'TestAccountName';
          testAccount.BillingCity = 'TestCity';
          testAccount.BillingCountry = 'TestCountry';
          testAccount.BillingStreet = 'Test Street';
          testAccount.Relationship_Manager__c = testColleague.Id;
          insert testAccount;
       
        
        
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;  
        //Request Id : 12638-Commenting step__c     
        //testOppty.Step__c = 'Marketing/Sales Lead';
        testOppty.StageName = 'Above the Funnel';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Dallas - Main';
        insert testOppty;
        }
        // user3 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert3', 'usrLstName3', 3);        
        //testOppty.StageName = 'Above the Funnel';
        //testOppty.OwnerId = '00522000000K1gc';
        update testOppty;   
        //Request Id : 12638-Removing step__c from query
        List<Opportunity> listOpp=[Select id,Name,Type,AccountId,StageName,CloseDate,OwnerId,CreatedById from Opportunity where id =: testOppty.Id];
        Map<Id,Opportunity> mapOpp= new Map<Id,Opportunity>();
        mapOpp.put(testOppty.Id,listOpp[0]);
        Email_Notification_Opp_Owner_change.send_email_on_OppOwnerchange(mapOpp);
             

//}

}}