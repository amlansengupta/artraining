/***************************************************************************************************************
Req ID : 17453
Purpose : This apex class is created as the extension of MF2_ScopeItEmployeesSwapEmployee VF page.
Created by : Soumil Dasgupta
Created Date : 26/12/2018
Project : MF2
****************************************************************************************************************/

public class MF2_ScopeItEmployeesSwapEmployeeApex {
    public String ScopeItProjectId {get;set;}
    public boolean taskFound {get;set;}
    
    public MF2_ScopeItEmployeesSwapEmployeeApex(ApexPages.StandardController controller){
    	//Fetch the ScopeIt Project Id from the controller.
        ScopeItProjectId = controller.getId();
        system.debug('scopeitproject '+ScopeItProjectId);
        //Fetch ScopeIt Task record for the particular ScopeIt Project.
        List<ScopeIt_Task__c> scopeItTask = [select Id from ScopeIt_Task__c where ScopeIt_Project__c =: ScopeItProjectId];
        System.debug('scopeItTask '+scopeItTask);
        //If ScopeIt Task record is present for the particular ScopeIt Project.
        if(!scopeItTask.isEmpty() && scopeItTask != null){
            taskFound =  True;
        }
        else{
            taskFound = False;
        }
        system.debug('taskFound '+taskFound);
        //return ScopeItProject;
    }
        
}