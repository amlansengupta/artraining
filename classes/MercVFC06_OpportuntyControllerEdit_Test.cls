/*
=======================================================
Modified By 	Date         Request
Trisha			18-Jan-2019  12638-Commenting Oppotunity Step
Trisha			21-Jan-2019	 17601-Replacing stage value 'Pending Step' with 'Identify'	
=======================================================
*/
@isTest
private class MercVFC06_OpportuntyControllerEdit_Test {

    static testMethod void myUnitTestsave1() 
    {   List<ApexConstants__c> apcList=new List<ApexConstants__c>();
        ApexConstants__c apc=new ApexConstants__c();
        apc.name='Above the funnel';
        apc.Description__c='test';
        apc.RelatedComponent__c='';
        apc.StrValue__c='Marketing/Sales Lead;Executing Discovery;';
        apcList.add(apc);
        
        ApexConstants__c apc1=new ApexConstants__c();
        apc1.name='Active Pursuit';
        apc1.Description__c='test';
        apc1.RelatedComponent__c='';
        apc1.StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        apcList.add(apc1);
        
         /*Request No. 17953 : Removing Finalist START
        ApexConstants__c apc2=new ApexConstants__c();
        apc2.name='Finalist';
        apc2.Description__c='test';
        apc2.RelatedComponent__c='';
        apc2.StrValue__c='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        apcList.add(apc2);
        Request No. 17953 : Removing Finalist END */
        
        ApexConstants__c apc3=new ApexConstants__c();
        apc3.name='Identify';
        apc3.Description__c='test';
        apc3.RelatedComponent__c='';
        apc3.StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        apcList.add(apc3);
        
        ApexConstants__c apc4=new ApexConstants__c();
        apc4.name='Selected';
        apc4.Description__c='test';
        apc4.RelatedComponent__c='';
        apc4.StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
        apcList.add(apc4);
        
        ApexConstants__c apc5=new ApexConstants__c();
        apc5.name='AP02_OpportunityTriggerUtil_1';
        apc5.Description__c='test';
        apc5.RelatedComponent__c='';
        apc5.StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;';
        apcList.add(apc5);
        
        ApexConstants__c apc6=new ApexConstants__c();
        apc6.name='AP02_OpportunityTriggerUtil_2';
        apc6.Description__c='test';
        apc6.RelatedComponent__c='';
        apc6.StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        apcList.add(apc6);
        
        ApexConstants__c apc7=new ApexConstants__c();
        apc7.name='ScopeITThresholdsList';
        apc7.Description__c='test';
        apc7.RelatedComponent__c='';
        apc7.StrValue__c='EuroPac:25000;Growth Markets:2500;North America:25000';
        apcList.add(apc7);
        
        insert apcList;
     
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.OwnerId = testUser.Id;
        insert testAccount;
            
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOppty';
        testOppty1.Type = 'New Client';
        testOppty1.AccountId = testAccount.Id;
        //Request Id 12638-Commenting step__c
        //testOppty1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty1.StageName = 'Identify';
        testOppty1.CloseDate = date.Today();
        testOppty1.CurrencyIsoCode = 'ALL';
        testOppty1.OwnerId = testUser.Id;
        testOppty1.Opportunity_Office__c = 'New York - 1166';
        testOppty1.Cross_Sell_ID__c = 'TestCrossSellId123';
        insert testOppty1;

        ApexPages.currentPage().getParameters().put('id',testOppty1.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
        MercVFC06_OpportuntyControllerEdit controller = new MercVFC06_OpportuntyControllerEdit(stdController);
        controller.OppID=testOppty1.Id;
        testOppty1.Cross_Sell_ID__c = 'TestCrossSellId123';
        PageReference pageref = controller.saveRecord();
        controller.cancelRecord();
        controller.saveAndReturn();
        
      /*  testOppty1.Cross_Sell_ID__c = null;
        PageReference pageref1 = controller.saveRecord();        
        controller.saveAndReturn();    */
        
        Opportunity testOppty2 = new Opportunity();
        testOppty2.Name = 'TestOppty2';
        testOppty2.Type = 'New Client';
        testOppty2.AccountId = testAccount.Id;
        testOppty2.Step__c = 'Identified Deal';
        testOppty2.StageName = 'Pending Step';
        testOppty2.CloseDate = date.Today();
        testOppty2.CurrencyIsoCode = 'ALL';
        testOppty2.OwnerId = testUser.Id;
        testOppty2.Opportunity_Office__c = 'New York - 1166';
        insert testOppty2;        
        
        ApexPages.currentPage().getParameters().put('id',testOppty2.Id);
        ApexPages.StandardController stdController2 = new ApexPages.StandardController(testOppty2);
        MercVFC06_OpportuntyControllerEdit controller2 = new MercVFC06_OpportuntyControllerEdit(stdController2);
        controller2.OppID=testOppty2.Id;
        PageReference pageref2 = controller2.saveRecord();
        controller2.cancelRecord();
        controller2.saveAndReturn();                  
                
    }
}