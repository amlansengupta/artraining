/*Purpose:   This apex class is written for Pagination and used for Product Search Page Pagination
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 2940)Created Apex class 
============================================================================================================================================== 
*/
global interface AP92_ObjectPaginatorListener {
    void handlePageChange(List<Object> newPage);
}