/*Purpose:This test class provides data coverage for the class AP106_ERMonthEndRequestSchedulable
=============================================================================================================
History
---------------
VERSION  AUTHOR  DATE     DETAIL
1.0      Madhavi 6/5/2014 Created testmethod for the schedulable class AP106_ERMonthEndRequestSchedulable.
==============================================================================================================
*/
@isTest(seeAlldata=true)
private class AP106_ERMonthEndRequestSchedulable_test{
/*
*Method Name:testSchedulable
*Description:To schedule the class  at specific time.
*/
    static testmethod void testSchedulable(){
       Test.startTest();
            String jobid = System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new AP106_ERMonthEndRequestSchedulable());
            CronTrigger ct  = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
       Test.stopTest();
   
    System.assertEquals('0 0 0 3 9 ? 2022',ct.CronExpression); 
    System.assertEquals(0, ct.TimesTriggered);
    System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
   }
}