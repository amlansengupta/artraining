/*Purpose: Apex class to expose Webservice method for WEBCAS 
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Reetika     30/7/2014   2014Sept Release/Req#4904 - Exposed webserice on the basis on Mercerforce Account Status logic 
   2.0 -    Reetika     18/03/2015  2015Apr Release/Req#6002 - Logic to determine the account status is updated in Method readAccountStatus
============================================================================================================================================== 
*/
global with sharing class AP109_AccountStatusService {
    
    global static final String STR_NOACTIVITYOBSOLETE_STATUS = 'AP109_MercerAccountStatus_1' ;
    global static final String STR_NOACTIVITY_STATUS = 'AP109_MercerAccountStatus_2' ;
    public static final string STR_WebServiceException = 'This OneCode does not exist in Mercerforce.Please provide a valid OneCode.';
    
    /**
     * Webservice exposed for WEBCAS to get Account Status for Given OneCode
     * MercerForce gets the request and Returns the status = YES or NO based on below logic
     * It checks if there are any active Contacts or open opportunities associated with the account.
     * If there isn’t any activity in MercerForce the webservice will return “YES” 
     * If (Active Contact Exists OR Open Opportunity Exists)
     * Then
     *      Return status = NO
     * Else
     *      Return status = YES
     */
    webservice static String readAccountStatus(String strOneCode) {
        
        Account[] acnts = [Select Id,One_Code__c,One_Code_Status__c,Name, MercerForce_Account_Status__c From    
           Account a where One_Code__c = :strOneCode limit 1 ]  ;      
 
        if(acnts==null || acnts.size()<1) { 
                 throw new WebServiceException(STR_WebServiceException);
                 return null;
        } 

        List<Contact> contactsList = [Select Id,Name , AccountId , Contact_Status__c from Contact 
          where Account.One_Code__c  = :strOneCode and Contact_Status__c =  :ConstantsUtility.STR_ACTIVE ] ;

        List<Opportunity> oppList= [Select Id,Name , AccountId , IsClosed from Opportunity 
          where Account.One_Code__c  = :strOneCode and IsClosed = false ] ;


        String returnVal = ApexConstants__c.getInstance(ConstantsUtility.STR_YES).StrValue__c ;
        if ( (contactsList!=null && contactsList.size()>0)  || (oppList!=null && oppList.size()>0) )
        {
          //String status_NoActivityObsolete = ApexConstants__c.getInstance(STR_NOACTIVITYOBSOLETE_STATUS).StrValue__c ;
          //String status_NoActivity = ApexConstants__c.getInstance(STR_NOACTIVITY_STATUS).StrValue__c ;
          
          returnVal = ApexConstants__c.getInstance(ConstantsUtility.STR_NO).StrValue__c ;
          
        }
        return  returnVal;
     }
}