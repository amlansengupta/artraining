/*Purpose: This is a Test Class to cover Mercer_Add_Employees_class which was created as part of 
3729 :March 2014 Release
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan   05/02/2014  Created test class 
 
============================================================================================================================================== 
*/
@isTest(seeAlldata=true)
Private class Mercer_Add_Employees_class_test{
     private static Mercer_TestData mtdata = new Mercer_TestData();
     private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();   
     
    /*
     * @Description : Test method to provide daat coverage to Mercer_Add_Employees_class class(Scenerio 1) 
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void testAdd_Employees(){
         
         User user1 = new User();
          ScopeIt_Project__c testScopeitProj = new ScopeIt_Project__c ();
          ScopeIt_Task__c testScopeitTask = new ScopeIt_Task__c ();
          ScopeIt_Employee__c testScopeitEmp = new ScopeIt_Employee__c ();
      
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
     
          System.runAs(user1) {
             String LOB = 'Retirement';
             String billType = 'Billable';
             Product2 testProduct = mtscopedata.buildProduct(LOB);
             ID prodId = testProduct.id;
             
             AP44_ChatterFeedReporting.FROMFEED=true;
             Opportunity testOppty = mtdata.buildOpportunity();
             ID opptyId  = testOppty.id;
             Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
             PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
             ID pbEntryId = pbEntry.Id;
       
             OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
             ID oliId= opptylineItem.id;
             String oliCurr = opptylineItem.CurrencyIsoCode;
             Double oliUnitPrice = opptylineItem.unitprice;
             String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
             
                
                   mtscopedata.insertCurrentExchangeRate();
                 testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );    
                 testScopeitTask=mtscopedata.buildScopeitTask();   
                 Test.startTest();            
                 testScopeitEmp=mtscopedata.buildScopeitEmployee(employeeBillrate); 
                 PageReference pageRef = Page.Mercer_Add_Employees;
                Test.setCurrentPage(pageRef);
                 ApexPages.currentPage().getParameters().put('taskid',testScopeitTask.Id);
                 ApexPages.currentPage().getParameters().put('projid',testScopeitProj.id);
                 
                 ApexPages.StandardController Scontroller = new ApexPages.StandardController(testScopeitEmp);
                 Mercer_Add_Employees_class controller = new Mercer_Add_Employees_class(Scontroller);
                 controller.objectType='';
                 String rid=string.valueof(testScopeitEmp.id);
        String n='Name';
        String level ='A';
        String busi='Benefits Admin';
        String mar='Australia';
        Decimal rt=10.00;
        Mercer_Add_Employees_class.Selempwrapper semw = new Mercer_Add_Employees_class.Selempwrapper(rid,n,level,busi,mar,rt);
        semw.isSelected=true;
                   controller.projid=testScopeitProj.id;
                   controller.namesearch='Test';
                   controller.levelsearch='B';
                   controller.emp.type__c='Employee';
                   controller.emp.Business__c='Global Operating and Shared Services';
                   controller.emp.Sub_Business__c = 'BPO Services';
                   controller.emp.Market_Country__c='India';
                   controller.getEmployees();
                   controller.getSelectedEmployees();
          controller.Beginning();
         
          controller.Previous();
          controller.Next();
          controller.Cancel();
           Test.stopTest(); 
          controller.End();
          controller.getDisablePrevious();
          controller.getDisableNext();
          controller.getTotal_size();
          controller.getPageNumber();
          controller.getTotalPages();
          
          controller.SaveEmployees();
          controller.RemoveEmployees();
                                 
                             
   
          }   
     }  
    /*
     * @Description : Test method to provide daat coverage to Mercer_Add_Employees_class class(Scenerio 2) 
     * @ Args       : Null
     * @ Return     : void
     */            
     static testMethod void testAdd_Employees_1(){
         User user1 = new User();
          ScopeIt_Project__c testScopeitProj = new ScopeIt_Project__c ();
          ScopeIt_Task__c testScopeitTask = new ScopeIt_Task__c ();
          ScopeIt_Employee__c testScopeitEmp = new ScopeIt_Employee__c ();
      
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
     
          System.runAs(user1) {
             String LOB = 'Retirement';
             String billType = 'Billable';
             Product2 testProduct = mtscopedata.buildProduct(LOB);
             ID prodId = testProduct.id;
             
             AP44_ChatterFeedReporting.FROMFEED=true;
             Opportunity testOppty = mtdata.buildOpportunity();
             ID opptyId  = testOppty.id;
             Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
             PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
             ID pbEntryId = pbEntry.Id;
       
             OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
             ID oliId= opptylineItem.id;
             String oliCurr = opptylineItem.CurrencyIsoCode;
             Double oliUnitPrice = opptylineItem.unitprice;
             String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
             
               
               mtscopedata.insertCurrentExchangeRate();
                 testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );    
                 testScopeitTask=mtscopedata.buildScopeitTask(); 
                  Test.startTest();              
                 testScopeitEmp=mtscopedata.buildScopeitEmployee(employeeBillrate); 
                  PageReference pageRef = Page.Mercer_Add_Employees;
                    Test.setCurrentPage(pageRef);
                 ApexPages.currentPage().getParameters().put('taskid','');
                 ApexPages.currentPage().getParameters().put('projid',testScopeitProj.id);
                 Test.stopTest();
                
                 
                 ApexPages.StandardController Scontroller = new ApexPages.StandardController(testScopeitEmp);
                 Mercer_Add_Employees_class controller = new Mercer_Add_Employees_class(Scontroller);
                 controller.objectType='Modelling';
                     boolean isSelected=true;
         String isoCode ='ALL';
         String recid=string.valueof(testScopeitEmp.id);
         String name='Test';
         String type ='Employee';
         String business ='Global Operating and Shared Services';
         String market ='India';
         Decimal billrate = 134.00 ;
         Decimal convertedrate = 5761.00;
         String levell ='B';
         String sub = 'Client Services';
      
       Mercer_Add_Employees_class.empWrapper eWpr = new  Mercer_Add_Employees_class.empWrapper(isSelected, recid, name,  type ,  business ,  market ,  recid, billRate,isoCode, levell, sub);
       eWpr.isSelected=true;
                 
                 String rid=string.valueof(testScopeitEmp.id);
        String n='tEST';
        String level ='B';
        String busi='Global Operating and Shared Services';
        String mar='India';
        Decimal rt=134.00;
        Mercer_Add_Employees_class.Selempwrapper semw = new Mercer_Add_Employees_class.Selempwrapper(rid,n,level,busi,mar,rt);
        semw.isSelected=true;
                    controller.projid=testScopeitProj.id;
                    controller.objectType='ScopeIt_Employee__c';
                    controller.namesearch='Test';
                   controller.levelsearch='B';
                   controller.emp.type__c='Employee';
                   controller.emp.Business__c='Global Operating and Shared Services';
                   controller.emp.Market_Country__c='India';
                   controller.selectedTask=string.valueof(testScopeitTask.id);
                   controller.updateselectedemployees();
                   controller.getEmployees();
                   controller.getSelectedEmployees();
          controller.Beginning();
          controller.clearFields();
          controller.Previous();
          controller.Next();
          controller.Cancel();
          controller.End();
          controller.getDisablePrevious();
          controller.getDisableNext();
          controller.getTotal_size();
          controller.getPageNumber();
          controller.getTotalPages();
          
          controller.SaveEmployees();
          controller.RemoveEmployees();
                                 
                              
    
          }
   
     }  

}