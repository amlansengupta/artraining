/*
====================================================================
Request                             Date                Modified By
12638-Replacing step with stage     18-Jan-2019         Trisha
====================================================================
*/
/*Handler Class for Opportunity Object*/
public class OppHandlerClass implements ITriggerHandler
{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    public static Boolean isskipProject = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        AP123_OppProdLinkService.prodLinkFlag = true;
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('Opportunity Trigger');
        
        //if (TrgSetting.Trigger_Disabled__c== true) { //commented by AG - 06-Sep-2018 
        if (TrgSetting!=null && TrgSetting.Trigger_Disabled__c== true) {
            return true;
        }else{
            return TriggerDisabled;
        }
        
    }
    
    //  if(ConstantsUtility.STR_ACTIVE.equals(Label.OpportunityTriggerSwitch)){
    // Switch_Settings__c switchSetting = Switch_Settings__c.getInstance(UserInfo.getUserId());
    // if(switchSetting.IsOpportunityTriggerActive__c ){//|| Test.isRunningTest()) ){
    Map<id,Opportunity> OppLstMap = new Map<id,Opportunity>();
    List<Opportunity> OppLstMap1 = new List<Opportunity>();
    Boolean checkProbability=true;
    
    
    
    
    public void BeforeInsert(List<SObject> newItems) 
    {
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerAutoResponseEmail = true;
        //if(beforeInsertFirstRun)
        //{
        List<Opportunity> typeCastedNewItems = (List<Opportunity>)newItems;
        for(Opportunity Op:typeCastedNewItems){
            
            //if(Op.Type=='Product Expansion'){
            if(Op.isCloned__c==True){
                AP02_OpportunityTriggerUtil.preventFirstCloneCall='STOP Cloning';
            }
            system.debug('in before : record type ; '+Op.RecordTypeId);
        }
        
        system.debug('newItems values1 = ' + newItems);
        AP02_OpportunityTriggerUtil.processOpporunityBeforeInsert(newItems);
        system.debug('newItems values2 = ' + newItems);
        if(AP02_OpportunityTriggerUtil.preventFirstCloneCall!='STOP Cloning'){
            AP007_OppValidations.futureOppUpdate123(newItems);
        }
        system.debug('newItems values3 = ' + newItems); 
        
        //beforeInsertFirstRun=false;
        //}
        /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
        boolean isExcludedProfile = false;
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - Start*/
        //String runningUserProfile = [Select Name from Profile where Id =: userInfo.getProfileId()].get(0).Name;
        String runningUserProfile = [Select Username from User where Id =: userInfo.getUserId()].get(0).Username;
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - End*/
        List<Audit_Excluded_Profiles__mdt> listAuditExcludedProfiles = [Select ExcludedProfiles__c from Audit_Excluded_Profiles__mdt];
        if(listAuditExcludedProfiles != null){
            for(Audit_Excluded_Profiles__mdt profMetadata : listAuditExcludedProfiles){
                if(profMetadata.ExcludedProfiles__c != null && profMetadata.ExcludedProfiles__c.equalsIgnoreCase(runningUserProfile)){
                    
                    isExcludedProfile = true;
                    break;
                }
            }
        }
        if(!isExcludedProfile){
            for(Opportunity opp:typeCastedNewItems)
            {
                opp.User_Last_Modified_Date__c = System.now();
                opp.User_Last_Modified_By__c = UserInfo.getUserId(); 
            }
        }
        //Request# 17079 ends
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) 
    {
        
        
        if(!(ConstantsUtility.skipTestClass == true && Test.isRunningTest() == true)){
            
            
            //if(beforeUpdateFirstRun)
            //{
            if(AP02_OpportunityTriggerUtil.preventFirstCloneCall!='STOP Cloning'){
                AP02_OpportunityTriggerUtil.stoprecFlag = true;
                
                Map<Id, Opportunity> typeCastedNewItems = (Map<Id, Opportunity>)newItemsMap;
                Map<Id, Opportunity> typeCastedOldItems = (Map<Id, Opportunity>)oldItemsMap;                 
                AP02_OpportunityTriggerUtil.checkCloseOpp(typeCastedNewItems ,typeCastedOldItems); 
                                 
                AP02_OpportunityTriggerUtil.processOpportunityBeforeUpdate(typeCastedNewItems ,typeCastedOldItems);
                if(!Ap02_OpportunityTriggerUtil.updateFromChild) {
                AP02_OpportunityTriggerUtil.oppValidationsBasedOnStage(typeCastedNewItems ,typeCastedOldItems);
                }
                //Req # 12444 Change for simplification for updating Buying Influence contact from Buyer field
                
                system.debug('KCBI****Oppf' + checkTriggerRecursive1.opportunityStartFlag);
                if(checkTriggerRecursive1.opportunityStartFlag == false && checkTriggerRecursive1.isStartKCBI2==false){   
                    checkTriggerRecursive1.isStartKCBI2=true;
                    AP02_OpportunityTriggerUtil.updatecontact(typeCastedNewItems ,typeCastedOldItems); 
                    checkTriggerRecursive1.isStartKCBI2=true;
                    
                }
                //  checkTriggerRecursive1.opportunityStartFlag = true;
                
                Boolean buyercheck=false;                 
                List<Id> ownerChangeCandidates = new List<Id>();
                for(Opportunity opp4 : typeCastedNewItems.values()){
                    
                    //system.debug('change ownership? ::'+(opp4.OwnerId == typeCastedOldItems.get(opp4.Id).OwnerId));
                    if(opp4.OwnerId == typeCastedOldItems.get(opp4.Id).OwnerId){
                        ownerChangeCandidates.add(opp4.Id);
                    }
                    if((opp4.AccountID != typeCastedNewItems.get(opp4.Id).AccountID) || (opp4.OwnerID != typeCastedOldItems.get(opp4.Id).OwnerID) || (opp4.Sibling_Contact_Name__c != typeCastedOldItems.get(opp4.Id).Sibling_Contact_Name__c) || ((opp4.Sibling_Contact_Name__c == null) && typeCastedOldItems.get(opp4.Id).Sibling_Contact_Name__c!=null)  ){
                        OppLstMap1.add(opp4);
                    }
                    /******request id:12638 , step values are replaced with that of stage START*******/
                    /* if(opp4.Buyer__c==null && (opp4.step__c !='Marketing/Sales Lead' && opp4.step__c !='Executing Discovery' && opp4.step__c !='Making Go/No Go Decision' && opp4.step__c !='Identified Deal' && opp4.step__c !='Identified Single Sales Objective(s)' && opp4.step__c !='Assessed Potential Solutions & Strategy')){
buyerCheck=true;
System.debug('buyerCheck****'+buyerCheck);
}*/
                    if(opp4.Buyer__c==null && (opp4.stageName!=System.Label.Stage_Above_the_Funnel  && opp4.stageName!=System.Label.Stage_Qualify && opp4.stageName!=System.Label.Stage_Identify)){
                        buyerCheck=true;
                        System.debug('buyerCheck****'+buyerCheck);
                    }
                    /******request id:12638 , step values are replaced with that of stage END*******/ 
                }
                
                
                if(buyerCheck && !Ap02_OpportunityTriggerUtil.updateFromChild){
                    
                    AP02_OpportunityTriggerUtil.buyerCheckAfterIdentifyStage(typeCastedNewItems ,typeCastedOldItems);                       
                    
                } 
                //new validation added for project required
                boolean checkproject=false;
                for(Opportunity opp : typeCastedNewItems.values()){
                    if(opp.stageName == System.Label.CL48_OppStageClosedWon && opp.StageName != typeCastedOldItems.get(opp.id).stageName)
                        checkproject=true;
                }
                if(checkproject && !Ap02_OpportunityTriggerUtil.updateFromChild)
                    AP02_OpportunityTriggerUtil.checkProductBeforeClosedWon(typeCastedNewItems,typeCastedOldItems); 
                
                if(!OppLstMap1.isEmpty()){
                    //    AP007_OppValidations.updateFieldsAfterUpdate(OppLstMap1);
                    system.debug('future update : '+OppLstMap1.get(0).Sibling_Contact_Name__c);
                    AP007_OppValidations.futureOppUpdate123(OppLstMap1);
                    
                }
                /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
                boolean isExcludedProfile = false;
                /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - Start*/
                //String runningUserProfile = [Select Name from Profile where Id =: userInfo.getProfileId()].get(0).Name;
                String runningUserProfile = [Select Username from User where Id =: userInfo.getUserId()].get(0).Username;
                /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - End*/
                List<Audit_Excluded_Profiles__mdt> listAuditExcludedProfiles = [Select ExcludedProfiles__c from Audit_Excluded_Profiles__mdt];
                if(listAuditExcludedProfiles != null){
                    for(Audit_Excluded_Profiles__mdt profMetadata : listAuditExcludedProfiles){
                        if(profMetadata.ExcludedProfiles__c != null && profMetadata.ExcludedProfiles__c.equalsIgnoreCase(runningUserProfile)){
                            
                            isExcludedProfile = true;
                            break;
                        }
                    }
                }
                if(!isExcludedProfile){
                    for(Opportunity opp:typeCastedNewItems.values())
                    {
                        opp.User_Last_Modified_Date__c = System.now();
                        opp.User_Last_Modified_By__c = UserInfo.getUserId(); 
                    }
                }
                //Request# 17079 ends
            }
            //beforeUpdateFirstRun=false;
            //} 
        }   
        
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems)
    {
        /*if(beforeDeleteFirstRun)
{
beforeDeleteFirstRun=false;
}*/ 
        Map<Id, Opportunity> typeCastedOldItems = (Map<Id, Opportunity>)oldItems;
        AP02_OpportunityTriggerUtil.deleteOppSummary(typeCastedOldItems.values());
       
    }
    
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemsMap)
    {
        //if(afterInsertFirstRun)
        //{
        List<Opportunity> typeCastedNewItems = (List<Opportunity>)newItems;
        Map<Id, Opportunity> typeCastedNewItemsMap = (Map<Id, Opportunity>)newItemsMap;
        
        for(Opportunity Op:typeCastedNewItems){
            
            if(Op.Type=='Product Expansion'){
                AP02_OpportunityTriggerUtil.preventFirstCloneCall='STOP Cloning';
            }
            
        }    
        if(!Task_Send_Notification_email.FLAG_CHECK_OPP && AP02_OpportunityTriggerUtil.preventFirstCloneCall!='STOP Cloning'){
            system.debug('why am not printed');
            AP02_OpportunityTriggerUtil.processOpportunityAfterInsert(newItemsMap.values());
            AP02_OpportunityTriggerUtil.totalRevenueChildOpp(typeCastedNewItemsMap,null);
            Map<id,Opportunity> oppnSCewMap=new Map<id,Opportunity>();
            
            for(SObject obj : newItemsMap.values())
            {
                Opportunity opp=(Opportunity)obj;                 
                if(opp.Opportunity_Region__c=='EuroPac' && opp.recordtype.name!='Opportunity Locked' && opp.recordtype.name!='Opportunity Locked With Product Expansions' && opp.Parent_Opportunity_name__C==null)
                {
                    oppnSCewMap.put(opp.id,opp);
                }                     
            }
            if(oppnSCewMap.size()>0) 
            {
                AP02_OpportunityTriggerUtil.validateSalesCreditAllocation(oppnSCewMap);
            }
        }
        //afterInsertFirstRun=false;
        
        //}
    }
    
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap)
    {
        System.debug('Test it in After Update1');
        if(!(ConstantsUtility.skipTestClass == true && Test.isRunningTest() == true)){
            System.debug('Test it in After Update2');
            //if(afterUpdateFirstRun)
            //{
            Map<Id, Opportunity> typeCastedNewItems = (Map<Id, Opportunity>)newItemsMap;
            Map<Id, Opportunity> typeCastedOldItems = (Map<Id, Opportunity>)oldItemsMap; 
            
            AP02_OpportunityTriggerUtil.totalRevenueChildOpp(typeCastedNewItems,typeCastedOldItems);
            if(AP02_OpportunityTriggerUtil.preventFirstCloneCall!='STOP Cloning'){ 
                System.debug('Test it in After Update3');
                //AP02_OpportunityTriggerUtil.processOpportunityAfterUpdate(typeCastedNewItems, typeCastedOldItems);
                if(OpportunityCloneWithProduct.stopMultipleUpd == false)
                {
                    System.debug('Test it in After Update4');
                    OpportunityCloneWithProduct.stopMultipleUpd = true;
                    Boolean TotalRevCheck=false;
                    Boolean Ownercheck=false;
                    if(Test.isRunningTest())
                    {
                        Ownercheck = true;
                    }
                    Map<id,Opportunity> oppnSCewMap=new Map<id,Opportunity>();
                    
                    
                    for(SObject obj : typeCastedNewItems.values()){
                        Opportunity opp=(Opportunity)obj;
                        if((opp.StageName!=typeCastedOldItems.get(opp.Id).StageName) && (opp.Probability!=typeCastedOldItems.get(opp.Id).Probability) && typeCastedNewItems.get(opp.Id).isclosed == false && typeCastedOldItems.get(opp.Id).isclosed == false){
                            
                            checkProbability=true;
                            
                        }
                        /****12638 merging Closed / Client Cancelled and Closed / Declined to Bid to Closed / Cancelled START *****/
                        /* if(opp.Total_Opportunity_Revenue__c!=typeCastedOldItems.get(opp.Id).Total_Opportunity_Revenue__c || (opp.stagename!=typeCastedOldItems.get(opp.Id).stagename && (opp.stagename=='Closed / Declined to Bid' || opp.stagename=='Closed / Client Cancelled'))){
TotalRevCheck=true;
}*/
                        if(opp.Total_Opportunity_Revenue__c!=typeCastedOldItems.get(opp.Id).Total_Opportunity_Revenue__c || (opp.stagename!=typeCastedOldItems.get(opp.Id).stagename &&  opp.stagename=='Closed / Cancelled')){
                            TotalRevCheck=true;
                        }
                        /****12638 merging Closed / Client Cancelled and Closed / Declined to Bid to Closed / Cancelled END*****/
                        if(opp.Opportunity_Region__c=='EuroPac' && opp.recordtype.name!='Opportunity Locked' && opp.recordtype.name!='Opportunity Locked With Product Expansions' ) {
                            oppnSCewMap.put(opp.id,opp);
                        } 
                        if(opp.OwnerId!=typeCastedOldItems.get(opp.Id).OwnerId){
                            Ownercheck=true;
                            
                        }
                        
                    }
                    if(Ownercheck){
                        AP02_OpportunityTriggerUtil.OwnerSalesCreditAllocation(typeCastedNewItems, typeCastedOldItems);
                    }
                    system.debug('TotalRevCheck********************');
                    if(TotalRevCheck==True){
                        system.debug('TotalRevChecsadsadk********************');
                        // AP02_OpportunityTriggerUtil.totalRevenueChildOpp(typeCastedNewItems.values());                         
                    }     
                    if(checkProbability){
                        AP02_OpportunityTriggerUtil.checkProbability(typeCastedNewItems, typeCastedOldItems); 
                    }
                    if(oppnSCewMap.size()>0){
                        AP02_OpportunityTriggerUtil.validateSalesCreditAllocation(oppnSCewMap);
                    } 
                    
                    AP02_OpportunityTriggerUtil.sendEmailCloseWin(typeCastedNewItems, typeCastedOldItems);
                    
                    System.debug('Test it in After Update');
                    AP02_OpportunityTriggerUtil.processOpportunityAfterUpdate(typeCastedNewItems, typeCastedOldItems);
                    for (Opportunity oppty : typeCastedNewItems.values())
                    {
                        
                        if (oppty.OwnerId != typeCastedOldItems.get(oppty.Id).OwnerId && oppty.StageName == 'Above the Funnel' && oppty.LeadSource == 'Marketing Assigned' && typeCastedOldItems.get(oppty.Id).OwnerId != '005E0000006und4' && typeCastedOldItems.get(oppty.Id).OwnerId != '005E0000007enCQ' && typeCastedOldItems.get(oppty.Id).OwnerId != '005E0000007jJLH' && typeCastedOldItems.get(oppty.Id).OwnerId != '005E0000006uleK')
                        {    
                            if(!Validator_cls.hasAlreadyDone()){  
                                OppLstMap.put(oppty.id,oppty);
                                Validator_cls.setAlreadyDone(); 
                            } 
                        }
                        if(oppty.Total_Opportunity_Revenue__c!=typeCastedOldItems.get(oppty.Id).Total_Opportunity_Revenue__c || (oppty.stagename!=typeCastedOldItems.get(oppty.Id).stagename && (oppty.stagename=='Closed / Declined to Bid' || oppty.stagename=='Closed / Client Cancelled'))){
                            TotalRevCheck=true;
                        }
                    }
                    system.debug('TotalReadsdk********************'+TotalRevCheck);
                    if(TotalRevCheck==True){
                        system.debug('TotalRevChecsadsadk********************');
                        
                    }
                    if(!OppLstMap.isEmpty()){
                        Email_Notification_Opp_Owner_change.send_email_on_OppOwnerchange(OppLstMap);}
                    
                } 
                AP02_OpportunityTriggerUtil.setRevDate(typeCastedNewItems,typeCastedOldItems);
               // AP02_OpportunityTriggerUtil.delSplitPrevOwner(typeCastedNewItems,typeCastedOldItems);
            }
            //afterUpdateFirstRun=false;
            //}
        }
        
    }
    
    public void AfterDelete(Map<Id, SObject> oldItemsMap)
    {
        //if(afterDeleteFirstRun)
        //{
        Map<Id, Opportunity> typeCastedOldItemsMap = (Map<Id, Opportunity>)oldItemsMap; 
        
        AP02_OpportunityTriggerUtil.totalRevenueChildOpp(null,typeCastedOldItemsMap);
        AP02_OpportunityTriggerUtil.deleteOppSummary(oldItemsMap.values());
        
        //afterDeleteFirstRun=false;
        //}
        
    }
    
    public void AfterUndelete(Map<Id, SObject> oldItems) 
    {}
    
    @future
    public static void updateOwnerShip(List<Id> ownerChangeOppIds){
        
    }
}