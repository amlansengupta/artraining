global class MF2_Add_Default_OppTeam {
    webservice static string addDefaultTeam(Id oId){
        string status= '';
    Id targetUserId =[select ownerid,id from opportunity where id=:oId].ownerId;
        Set<Id> alreadyInTeamUserSet = new Set<Id>();
        for(OpportunityTeamMember om:[select UserId from OpportunityTeamMember where OpportunityId=:oId]){
            alreadyInTeamUserSet.add(om.UserId);
        }
List<UserTeamMember> lUt =[select CreatedById,CreatedDate,Id,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,OwnerId,SystemModstamp,TeamMemberRole,UserId FROM UserTeamMember where ownerId=:targetUserId];
List<OpportunityTeamMember> lt= new List<OpportunityTeamMember>();
if(!lUt.isEmpty()){
    for(UserTeamMember ut :lUt){
        if(ut.UserId!=null && !alreadyInTeamUserSet.contains(ut.UserId)){
    OpportunityTeamMember otm = new OpportunityTeamMember();otm.UserId =ut.UserId;otm.opportunityId = oId;otm.flag__c= true;
            if(ut.OpportunityAccessLevel!=null)otm.OpportunityAccessLevel =ut.OpportunityAccessLevel;if(ut.TeamMemberRole!=null){ otm.role__c=ut.TeamMemberRole; otm.TeamMemberRole = ut.TeamMemberRole;}lt.add(otm);  }          
        
    } 
    
}
if(!lt.isEmpty()){
    try{insert lt;status= 'success';}catch(exception ex){status = 'error';
    }
}  
        return status;
    }

}