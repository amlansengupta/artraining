/* Purpose:This shedulable class implements DC24_AccountBatchable batch class to mass update the Account records to populate Concatenated Industry Category field 
   as a concatenation of Primary Industry Category, Secondary Industry Category and Tertiary Industry Category fields. 
===================================================================================================================================
The methods contains following functionality;
 • Execute() method fires a query fetch all Account records and executes DC24_AccountBatchable batch class  
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          08/23/2013  Created Shedulable Class to  implement DC24_AccountBatchable batch class to populate Concatenated Industry Category on Account records 
 2.0         Sarbpreet          7/7/2014    Added Comments.
 ======================================================================================================================================*/
global class DC25_AccountSchedulable implements schedulable{
/*
* Creation of constructor
*/
global DC25_AccountSchedulable(){}
/*
 * Schedulable execute method     
 */
global void execute(SchedulableContext SC)    
 {  
     //String variable to fetch the Account id and their names 
     String  qry = 'Select id, Name, Primary_Industry_Category__c, Industry_Category__c, Industry_Category_2__c, Concatenated_Industry_Category__c  from Account where One_Code_Status__c IN (\'Active\', \'Pending\', \'Pending - Workflow Wizard\') AND Duplicate_Flag__c = False' ;        
     database.executeBatch(new DC24_AccountBatchable(qry), 2000);  
     
 }   
 }