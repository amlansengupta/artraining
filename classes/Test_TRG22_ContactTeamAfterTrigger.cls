/* Purpose: This test class provides data coverage for Testing TRG22_ContactTeamAfterTrigger Trigger Functionality
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Gyan       12/19/2013  Created test method for TRG22_ContactTeamAfterTrigger
 ==================================================================================================================*/    
@isTest(seeAllData = true)
public class Test_TRG22_ContactTeamAfterTrigger {
      
    /* * @Description : This test method provides data coverage for TRG22_ContactTeamAfterTrigger           
       * @ Args       : no arguments        
       * @ Return     : void       
    */        
    static testMethod void Test_TRG22_ContactTeamAfterTrigger() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12567802' ;
        insert testColleague;
        
        
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = user1.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc12344yzp@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        User testUser = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        system.debug('@@@@'+mercerStandardUserProfile);
       // testUser = Mercer_TestData.createUser(mercerStandardUserProfile , 'usert1', 'usrLstName',testUser);
        
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        
        testUser.alias = 'usert1';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'usrLstName';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = mercerStandardUserProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + 'usrLstName' +'@xyz.com';
        testUser.EmployeeNumber = '1234567890';             
        insert testUser;         
        
        
        
        
                        
        Contact_Team_Member__c testConTeam = new Contact_Team_Member__c();
        testConTeam.Contact__c = testContact.Id;
        testConTeam.ContactTeamMember__c = testUser.Id;
        insert testConTeam;
        
       
     /* User user1 = new User();
       String mercerStandardUserProfile1 = Mercer_TestData.getmercerStandardUserProfile();      
       user1 = Mercer_TestData.createUser1(mercerStandardUserProfile1, 'usert2', 'usrLstName2', 2);
       system.runAs(User1)*/
       System.Runas(testUser){
        Test.startTest();
       // delete testConTeam;
        Test.StopTest();
     }
    }
}