/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData=True)
public class BatchableChatterPostForOpportunity_Test {
     
   // Opportunity opportunity2 = new opportunity(recordtypeid = recidopp, Name = 'Test Opportunity' + Math.random(),Last_Step_Modified_Date__c = Date.Today().AddDays(-9), Account = Account, Type = 'New Client', Step__c = 'Pending Project', CloseDate = date.today(), Opportunity_Office__c = 'Rotterdam - Conradstraat',Chatter_posted_for_9_days__c = False, StageName = 'Pending Project', CurrencyIsoCode = 'INR' );
    // static User user1 = New User();
    static List<Opportunity> opplist = new List<Opportunity>();
    
    

    static testMethod void chatterpostopportunity () {
        
       
      /*  Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
        */
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        //testColleague.Location__c = mogd.Id;
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        Test.StartTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        testOppty.Amount = 100;
        //testOppty.Buyer__c=    
        insert testOppty;
        
        List<Opportunity> opplist = new List<Opportunity>();
        opplist.add(testOppty);
        System.debug('opplist*****'+opplist);
        //chatterPostInOpportunityNinedays cht=new chatterPostInOpportunityNinedays();
        try{
         
            chatterPostInOpportunityNinedays.chatterPostMethod(opplist); 
            Test.StopTest();
            chatterPostInOpportunityNinedays.saveChatterPosted(opplist);
            
        }catch(exception e){}
        
       // batchableChatterPostForOpportunity newchatter = new batchableChatterPostForOpportunity();
       // Database.executeBatch(newchatter);
         
    }
     
    static testMethod void chatterpostopportunity1 () {
        
       
        /*Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
        */
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        //testColleague.Location__c = mogd.Id;
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
         Test.StartTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        testOppty.Amount = 100;
        //testOppty.Buyer__c=    
        insert testOppty;
        
        List<Opportunity> opplist = new List<Opportunity>();
        opplist.add(testOppty);
        System.debug('opplist*****'+opplist);
        //chatterPostInOpportunityNinedays cht=new chatterPostInOpportunityNinedays();
        try{
            AP44_ChatterFeedReporting.FROMFEED= true; 
            chatterPostInOpportunityNinedays.saveChatterPosted(opplist);
            chatterPostInOpportunity.saveChatterPosted(opplist);
             Test.StopTest();
            chatterPostInOpportunity.chatterPostMethod(opplist);
            batchableChatterPostForOpportunity newchatter = new batchableChatterPostForOpportunity();
        batchableChatterPostForOpportunity9Days newchatter1 = new batchableChatterPostForOpportunity9Days();
        Database.executeBatch(newchatter);
        Database.executeBatch(newchatter1);
        }catch(exception e){}
        
       // batchableChatterPostForOpportunity newchatter = new batchableChatterPostForOpportunity();
       // Database.executeBatch(newchatter);
        
    }
    private static testmethod  void chatterPostTest(){
    
      
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc12@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.Id;
        insert testContact;
        
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;       
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Amount = 100;
        //testOppty.Buyer__c= testContact.id; 
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        testOppty.CreatedDate=Date.today()-5;
        Test.startTest();
        insert testOppty; 
        Test.stopTest();
        
       /*
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        insert opptylineItem;
        */
       testOppty.Buyer__c= testContact.id; 
        /****************Request Id:12638 commenting step and adding stageName START****************/
        //testOppty.Buyer__c =
        testOppty.stageName = 'pending Project';
        //testOppty.Step__c='Pending Project';
        /****************Request Id:12638 commenting step and adding stageName END****************/
        testOppty.Close_Stage_Reason__c='Brand Recognition';
             
            AP44_ChatterFeedReporting.FROMFEED= true;
                update testOppty;    
                         
         
         
           
             
            batchableChatterPostForOpportunity newchatter = new batchableChatterPostForOpportunity();
        batchableChatterPostForOpportunity9Days newchatter1 = new batchableChatterPostForOpportunity9Days();
        SchedulableChatterPostInOpportunity sco=new SchedulableChatterPostInOpportunity();            
            String sch = '0 0 23 * *    ?'; 
            system.schedule('Test Territory Check', sch, sco);  
                
         try{
               //Database.executeBatch(newchatter);
            
               
        }catch(exception e){}
                    
       
       }
    private static testmethod  void chatterPostTest1(){
    
      
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc11@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.Id;
        insert testContact;
        
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;  
        //Request id:12638 commenting step     
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Amount = 100;
        //testOppty.Buyer__c= testContact.id; 
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        testOppty.CreatedDate=Date.today()-9;
         test.starttest();
        insert testOppty; 
        Test.stopTest();
        
       
        /*
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        insert opptylineItem;
        */
        
        testOppty.Buyer__c= testContact.id; 
        //request Id:12638 commenting step adding stageName
        testOppty.stageName = 'Pending Project';
        //testOppty.Step__c='Pending Project';
        testOppty.Close_Stage_Reason__c='Brand Recognition';
          
           
                update testOppty;    
                         
          
         
           try{
           
             
            batchableChatterPostForOpportunity newchatter = new batchableChatterPostForOpportunity();
             
        batchableChatterPostForOpportunity9Days newchatter1 = new batchableChatterPostForOpportunity9Days();
         
                 
               
            //Database.executeBatch(newchatter1);
                 
        }catch(exception e){}
                    
       
       } 
}