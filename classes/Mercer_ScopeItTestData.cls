/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose: Test class to provide test coverage for scopeIt related classes.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   05/02/2014  Created test class 
 
============================================================================================================================================== 
*/
 @isTest(seeAllData = true)
public class Mercer_ScopeItTestData {

private static Mercer_TestData mtdata = new Mercer_TestData();    
private static Product2 testProduct = new Product2();
private static ScopeIt_Project__c testScopeitProject = new ScopeIt_Project__c();
private static ScopeIt_Task__c testscopeTask = new ScopeIt_Task__c();
private static ScopeIt_Employee__c testScopeItEmployee = new ScopeIt_Employee__c();
private static Opportunity testOpportunity = new Opportunity();
private static Account testAccount = new Account();
private static Colleague__c testColleague = new Colleague__c();
private static Scope_Modeling__c testScopeModelProj = new Scope_Modeling__c();
private static Scope_Modeling_Task__c testScopeModelTask = new Scope_Modeling_Task__c();
private static Scope_Modeling_Employee__c testScopeModelEmployee = new Scope_Modeling_Employee__c();
private static Current_Exchange_Rate__c testCurrExch = new Current_Exchange_Rate__c();
private static Current_Exchange_Rate__c testCurrExch2 = new Current_Exchange_Rate__c();
private static Current_Exchange_Rate__c testCurrExch3 = new Current_Exchange_Rate__c();
private static String employeeBillrate;
private static ID proid;
private static ID scopeitprojectid;
private static ID scopeittaskid;
private static ID scopemodelprojectid;
private static ID scopemodeltaskid;
  
    /*
     * @Description : Test data for employee Bill rate
     * @ Args       : Null
     * @ Return     : void
     */
    static
    {
       employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
    }
    
     private static String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
     private static Colleague__c testColleague1 = mtdata.buildColleague();
     private static Account testAccount1 = mtdata.buildAccount();

    /*
     * @Description : Test data created for Product
     * @ Args       : Null
     * @ Return     : void
     */
    public Product2 buildProduct(String productLOB)
    {
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.Classification__c = 'Consulting Solution Area';
        testProduct.LOB__c = productLOB;
        insert testProduct;
        
        proid = testProduct.id;
        return testProduct;
    }
    /*
     * @Description : Test data created for Current Exchange Rate
     * @ Args       : Null
     * @ Return     : void
     */
    public void insertCurrentExchangeRate()
    {
        testCurrExch.name = 'TEST';
        testCurrExch.Conversion_Rate__c = 1;
        insert testCurrExch;
        testCurrExch2.name = 'USD1';
        testCurrExch2.Conversion_Rate__c = 2;
        insert testCurrExch2;

    }

    

     /*
     * @Description : Test data created for Scopeit Project
     * @ Args       : Null
     * @ Return     : void
     */
     public  ScopeIt_Project__c buildScopeitProject(String prodId, String OppId, String oliId, String oliCurr, Double oliUnitprice)
     {
        testscopeitProject.name = 'testscopeitproj';
        testscopeitProject.Product__c =  prodId;
        //testscopeitProject.Bill_Type__c = 'Billable';                
        testscopeitProject.opportunity__c = OppId;
        testscopeitProject.OpportunityProductLineItem_Id__c = oliId;
        testscopeitProject.CurrencyIsoCode = oliCurr;
        testscopeitProject.Sales_Price__c=oliUnitprice;
        insert testscopeitProject;
        
        
        scopeitprojectid = testscopeitProject.id;
        return testscopeitProject;
     }
     
       /*
     * @Description : Test data created for Scopeit Task
     * @ Args       : Null
     * @ Return     : void
     */
     public ScopeIt_Task__c buildScopeitTask()
     {
         testscopeTask.task_name__c = 'testscopetask';
         testscopeTask.ScopeIt_Project__c = scopeitprojectid;
         //testscopeTask.Net_Price__c = netPrice;       
         testscopeTask.Billable_Expenses__c = 2000;
         testscopeTask.Bill_Rate__c = 20;
         //testscopeTask.Cost_Rate__c = 30;
         insert testscopeTask;
         
         scopeittaskid = testscopeTask.id;
         return testscopeTask;
         
     }
     
       /*
     * @Description : Test data created for Scopeit employee
     * @ Args       : Null
     * @ Return     : void
     */
     public ScopeIt_Employee__c buildScopeitEmployee(String empBillrate)
     {
        testScopeItEmployee.ScopeIt_Task__c = testscopeTask.id;
        testScopeItEmployee.Employee_Bill_Rate__c = empBillrate;
        testScopeItEmployee.Cost__c = 500;
        testScopeItEmployee.Hours__c = 2;
        testScopeItEmployee.currenCyISOCode = 'ALL';
        testScopeItEmployee.Billable_Time_Charges__c = 100;
        testScopeItEmployee.Bill_Rate_Opp__c =20;
        insert testScopeItEmployee;
        return testScopeItEmployee;
     }
     
      /*
     * @Description : Test data created for Scope modeling
     * @ Args       : Null
     * @ Return     : void
     */
     public  Scope_Modeling__c buildScopeModelProject(String prodId)
     {
        testScopeModelProj.name = 'testscopeModelproj';
        testscopeitProject.Product__c =  prodId;
        //testscopeitProject.Bill_Type__c = 'Billable';                               
        testscopeitProject.CurrencyIsoCode = 'ALL'; 
        testscopeitProject.Sales_Price__c = 200;   
        insert testScopeModelProj;
        
        
        scopemodelprojectid = testScopeModelProj.id;
        return testScopeModelProj;
     }
     
      /*
     * @Description : Test data created for Scope Model task
     * @ Args       : Null
     * @ Return     : void
     */
     public Scope_Modeling_Task__c buildScopeModelTask()
     {
         testScopeModelTask.Scope_Modeling__c = scopemodelprojectid;               
         testScopeModelTask.Billable_Expenses__c = 2000;
         testScopeModelTask.Bill_Rate__c = 20;        
         insert testScopeModelTask;
         
         scopemodeltaskid = testScopeModelTask.id;
         return testScopeModelTask;
         
     }
     
    /*
     * @Description : Test data created for Scopei model employee
     * @ Args       : Null
     * @ Return     : void
     */
       public Scope_Modeling_Employee__c buildScopeModelEmployee(String empBillrate)
     {
        testScopeModelEmployee.Scope_Modeling_Task__c = testScopeModelTask.id;
        testScopeModelEmployee.Employee_Bill_Rate__c = empBillrate;
        testScopeModelEmployee.Cost__c = 500;
        testScopeModelEmployee.Hours__c = 2;
        testScopeModelEmployee.CurrencyISOCOde= 'ALL';
        testScopeModelEmployee.Billable_Time_Charges__c = 100;
        testScopeModelEmployee.Bill_Rate_Opp__c =20;
        insert testScopeModelEmployee;
        return testScopeModelEmployee;
     }
     
    /*
     * @Description : Test data for getting Employee Bill Rate
     * @ Args       : Null
     * @ Return     : void
     */
       public static String getEmployeeBillRate()
    {
         Employee_Bill_Rate__c Ebr = new  Employee_Bill_Rate__c ();
         ebr.bill_cost_ratio__c =6.06;
         ebr.type__c='Generic Employee';
         ebr.bill_rate_local_currency__c=213;
         ebr.Business__c='Property Administration';
         ebr.Level__c='A';
         ebr.currenCyISOCode ='ALL';
         ebr.Market_Country__c='Chile';
         ebr.Sub_Business__c = 'BPO Services';
         ebr.Employee_Status__c = 'Active';
         insert ebr;
        return [Select e.Id, Bill_Cost_Ratio__c,Employee_Status__c,Sub_Business__c ,type__c,currenCyISOCode, Bill_Rate_Local_Currency__c, Business__c, Level__c, Market_Country__c From Employee_Bill_Rate__c e  limit 1].id;
    }
    /*
     * @Description : Test data for scopeIttemplate object
     * @ Return     : Temp_ScopeIt_Project__c 
     */ 
        public static Temp_ScopeIt_Project__c  createScopeitTemplate(String Lob,String productid,String ProductName)
        {
         Temp_ScopeIt_Project__c tempobj = new Temp_ScopeIt_Project__c();
         tempobj.LOB__c =Lob;
         tempobj.Product__c=productid;
         tempobj.Product_Id__c =ProductName;
         tempobj.Sales_Price__c = 100.0;
         return tempobj;
        }
     /*
     * @Description : Test data for scopeIttask object
     * @ Return     : Temp_ScopeIt_Task__c  
     */
        public static Temp_ScopeIt_Task__c createScopeitTask(string templateId){
        Temp_ScopeIt_Task__c  taskrec = new Temp_ScopeIt_Task__c();
        taskrec.Task_Name__c ='TestTask';
        taskrec.Temp_ScopeIt_Project__c =templateId;
        return taskrec;
        }
    /*
     * @Description : Test data for scopeItEmployee object
     * @ Return     : Temp_ScopeIt_Employee__c 
     */
        public static Temp_ScopeIt_Employee__c createScopeitEmployee(String templateid,String taskId,String billrateId){
        Temp_ScopeIt_Employee__c  employeerec = new Temp_ScopeIt_Employee__c();
        employeerec.Name__c ='TestEmployee';
        employeerec.ScopeIt_Project_Template__c=templateid;
        employeerec.Temp_ScopeIt_Task__c =taskId; 
        employeerec.Employee__c =billrateId;
        return employeerec;
        
        }
}