/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================History -----------------------
                                                        VERSION     AUTHOR  DATE        DETAIL                   
                                                          1.0 -    Arijit Roy 03/29/2013  Created Batch class for Opportunity
=========================================================================================================================================
*/
global with sharing class AP31_OpportunitySyncBatchable implements Database.Batchable<sObject>,Database.stateful{
	
	global final String oppQuery;
	
	global Map<String, Mercer_Office_Geo_D__c> officeMap = new Map<String, Mercer_Office_Geo_D__c>();
	
	global AP31_OpportunitySyncBatchable(String oppQuery)
	{
		this.oppQuery = oppQuery;
		
		 for(Mercer_Office_Geo_D__c officeGeo : [Select Id, Market__c, Office__c, Region__c, Sub_Market__c, Name, Sub_Region__c, Country__c FROM Mercer_Office_Geo_D__c Where isUpdated__c = true])
         {
         	officeMap.put(officeGeo.Name, officeGeo);	
         }
	}
	
	
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
     
    global Database.QueryLocator start(Database.BatchableContext BC)
	{

		//execute the query and fetch the records	
        return Database.getQueryLocator(oppQuery);
    }
    
     /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
     
    global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		List<Opportunity> oppForUpdate = new List<Opportunity>();
		for(Opportunity opportunity : (List<Opportunity>)scope)
		{
			opportunity.Opportunity_Market__c = officeMap.get(opportunity.Office_Code__c).Market__c;
			opportunity.Opportunity_Region__c = officeMap.get(opportunity.Office_Code__c).Region__c;
			opportunity.Opportunity_Sub_Market__c = officeMap.get(opportunity.Office_Code__c).Sub_Market__c;
			opportunity.Opportunity_Sub_Region__c = officeMap.get(opportunity.Office_Code__c).Sub_Region__c;
			opportunity.Office_Code__c = officeMap.get(opportunity.Office_Code__c).Name;
			opportunity.Opportunity_Country__c = officeMap.get(opportunity.Office_Code__c).Country__c;
			opportunity.Opportunity_Office__c = officeMap.get(opportunity.Office_Code__c).Office__c;
			oppForUpdate.add(opportunity);
		}	
		
		if(!oppForUpdate.isEmpty()) Database.update(oppForUpdate, false);
	}
	
	 /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
     
    global void finish(Database.BatchableContext BC)
	{
		DateTime sysTime = System.now();
		sysTime = sysTime.addSeconds(360);
		String CRON_EXP = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		AP28_UserSyncSchedulable userSchedule = new AP28_UserSyncSchedulable();
		System.schedule('Sync User From Geo Table ' + String.valueOf(Date.Today()), CRON_EXP, userSchedule);    
	}
	

}