public class CareerContractController{
public Talent_Contract__c thisContract;
public String showMsg{get;set;}
   public CareerContractController(ApexPages.StandardController controller) {
    thisContract=(Talent_Contract__c)controller.getRecord();
    List<Client_Product_Contact__c> cpc = new List<Client_Product_Contact__c>();
    cpc = [Select Id,Name from Client_Product_Contact__c where Role__c includes('Billing Contact') AND Talent_Contract__c = :thisContract.Id ];
    if(cpc.size()==0){
    showMsg = 'Checked';
    }
   }
}