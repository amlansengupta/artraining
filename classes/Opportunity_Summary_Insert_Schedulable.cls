global class Opportunity_Summary_Insert_Schedulable implements Schedulable{

    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new Opportunity_Summary_Insert_Batch(),200);
    }
}