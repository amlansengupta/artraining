/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

global with sharing class SVNSUMMITS_MembersController {

	global static Id networkId = System.Network.getNetworkId();

	global static final Integer STANDARDSETLIMIT = 10000;
	global static final Integer INTLIMITMEMBERSRECORDS = 8;
	global static final Integer INTLISTSIZE = 50;
	global static final Integer INTPAGENUMBER = 1;

	private static List<String> excludedIds {
		get;
		set;
	}

	@AuraEnabled
	global static Decimal getMemberCountEx(List<String> excludeList) {

		// Exclude the following IDs from the list
		excludedIds = new List<String>();

		// Ignore blank IDs
		for (String i : excludeList) {
			if (String.isNotBlank(i)) {
				excludedIds.add(i);
			}
		}

		return getMemberCount();
	}

	@AuraEnabled
    /*
        @Name           : getMemberCount
        @Description    : Get the exact member count (not limited by the StandardSetController)
    */
	global static Decimal getMemberCount() {
		Decimal total;

		if (excludedIds != null && excludedIds.size() > 0) {
			system.debug('Excluded Ids ' + excludedIds);

			total = [
					SELECT Count()
					FROM User
					WHERE IsActive = true
					AND	(Empl_Status__c = 'Active' OR In_Member_Directory__c = true)
					AND Id IN (SELECT MemberId FROM NetworkMember WHERE NetworkId = :networkId)
					AND Id NOT IN :excludedIds
			];

		}
		else {
			total = [
				SELECT Count()
				FROM User
				WHERE IsActive = true
				AND	(Empl_Status__c = 'Active' OR In_Member_Directory__c = true)
				AND Id IN (SELECT MemberId FROM NetworkMember WHERE NetworkId = :networkId)
			];
		}

		return total;
	}

	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers getMembersEx(
			Integer numberOfMembers,
			String sortBy,
			String searchMyMembers,
			String searchString,
			List<String> excludeList) {

		// Exclude the following IDs from the list
		excludedIds = new List<String>();

		// Ignore blank IDs
		for (String i : excludeList) {
			if (String.isNotBlank(i)) {
				excludedIds.add(i);
			}
		}

		return getMembers(numberOfMembers, sortBy, searchMyMembers, searchString);
	}

	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers getMembers(
			Integer numberOfMembers,
			String sortBy,
			String searchMyMembers,
			String searchString) {

		try {
			//changed user to NetworkMember
			List<User> memberList = new List<User>();

			// excluded ids
			Set<String> excluded = new Set<String>();

			// Set used for store the userId's
			Set<String> setUserId = new Set<String>();

			Integer intLimit = Integer.valueOf(numberOfMembers);

			String Query = getQueryString();

			// Check whether the Member I Follow is chcked or not
			if (!string.isEmpty(searchMyMembers)) {

				if (searchMyMembers == 'Members I Follow') {

					setUserId = getSubscriptionId();
				}
				// Add the query for Member I folow feature
				Query += 'AND Id IN : setUserId';
			}

			if (excludedIds != null && excludedIds.size() > 0) {

				String idList = '';
				Integer pos = 0;
				// Ignore blank IDs

				for (String i : excludedIds) {
					if (String.isNotBlank(i)) {
						idList += '\'' + i + '\'';
						pos += 1;

						if (pos < excludedIds.size()) {
							idList += ', ';
						}
					}
				}

				Query += ' AND Id NOT IN (' + idList + ')';
			}

			if (!String.isEmpty(Query)) {

				// Check for searching feature
				if (searchString != null && searchString.trim().length() > 0) {

					Query += ' And ( Name LIKE \'%' + String.valueOf(searchString) + '%\')';
				}
				if (sortBy == 'Sort by Last Name') { // Condition for Sort By Last Name

					Query += ' ORDER BY LastName';
				}
				// Condition for Sort By First Name
				else if (sortBy == 'Sort by First Name') {

					Query += ' ORDER BY FirstName';
				}

				// StandardSetController LIMIT
				Query += ' LIMIT ' + STANDARDSETLIMIT;

				System.debug(LoggingLevel.Info, 'Controller qry: ' + Query);

				SVNSUMMITS_WrapperMembers membersWrapper = new SVNSUMMITS_WrapperMembers(
						Query,
						intLimit,
						null,
						setUserId,
						true,
						false);

				return membersWrapper;
			}
		} catch (Exception e) {

			system.debug('***Exception ***' + e);
			return null;
		}
		return null;
	}


    /*
        @Name          :  getSubscriptionId
        @Description   :  Method used to get the subscription Id's
    */
	private static Set<String> getSubscriptionId() {

		Set<String> setUserId = new Set<String>();
		try {
			for (EntitySubscription objEntitySubscription : [
					SELECT SubscriberId, ParentId, NetworkId, Id
					FROM EntitySubscription
					WHERE SubscriberId = :userinfo.getUserId()
					LIMIT 100
			]) {

				// Stored the Subscription Id's
				setUserId.add(objEntitySubscription.ParentId);


			}
		} catch (Exception e) {
			system.debug('***Exception ***' + e);
			return null;

		}

		return setUserId;

	}

	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers nextPageEx(
			Integer numberOfMembers,
			Integer pageNumber,
			String sortBy,
			String searchMyMembers,
			String searchString,
			List<String> excludeList) {

		// Excluded the following IDs from the list
		excludedIds = new List<String>();

		// Ignore blank IDs
		for (String i : excludeList) {
			if (String.isNotBlank(i)) {
				excludedIds.add(i);
			}
		}

		return nextPage(numberOfMembers, pageNumber, sortBy, searchMyMembers, searchString);
	}

    /*
        @Name          :  nextPage
        @Description   :  Method used on click of "Next" in pagination to diaplay news records as per numberOfNewsPerPage
    */
	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers nextPage(
			Integer numberOfMembers,
			Integer pageNumber,
			String sortBy,
			String searchMyMembers,
			String searchString) {

		try {

			SVNSUMMITS_WrapperMembers objWrapperMember = membersPaginantion(numberOfMembers, pageNumber, sortBy, searchMyMembers, searchString);

			if (objWrapperMember != null) {
				objWrapperMember.nextPage();

				objWrapperMember.setController = null;
				return objWrapperMember;
			} else {

				return null;
			}
		} Catch (Exception e) {

			system.debug('*** Exception nextPage ***' + e);
			return null;
		}
	}


    /*
    @Name          :  membersPaginantion
    @Description   :  Method used when click on next/previous button.
    */
	private static SVNSUMMITS_WrapperMembers membersPaginantion(Integer numberOfMembers, Integer pageNumber, String sortBy, String searchMyMembers, String searchString) {

		try {
			Integer listSizeValue = numberOfMembers != null ? Integer.valueOf(numberOfMembers) : INTLISTSIZE;
			Integer pageNumberValue = pageNumber != null ? Integer.valueOf(pageNumber) : INTPAGENUMBER;
			Integer intLimit = Integer.valueOf(numberOfMembers);


			String QUERY = getQueryString();

			Set<String> setUserId = new Set<String>();

			// Check whether the Member I Follow is chcked or not
			if (!string.isEmpty(searchMyMembers)) {

				if (searchMyMembers == 'Members I Follow') {

					setUserId = getSubscriptionId();
				}
				// Add the query for Member I folow feature
				QUERY += ' AND Id IN : setUserId';
			}

			if (excludedIds != null && excludedIds.size() > 0) {

				String idList = '';
				Integer pos = 0;
				// Ignore blank IDs

				for (String i : excludedIds) {
					if (String.isNotBlank(i)) {
						idList += '\'' + i + '\'';
						pos += 1;

						if (pos < excludedIds.size()) {
							idList += ', ';
						}
					}
				}

				Query += ' AND Id NOT IN (' + idList + ')';
			}

			// Check for searching feature
			if (searchString != null && searchString.trim().length() > 0) {

				QUERY += ' And ( Name LIKE \'%' + String.valueOf(searchString) + '%\')';
			}

			// Condition for Sort By Last Name
			if (sortBy == 'Sort by Last Name') {
				QUERY += ' ORDER BY LastName';
			}
			// Condition for Sort By First Name
			else if (sortBy == 'Sort by First Name') {
				QUERY += ' ORDER BY FirstName';
			}

			// StandardSetController LIMIT
			QUERY += ' LIMIT ' + STANDARDSETLIMIT;

			if (!String.isEmpty(QUERY)) {

				SVNSUMMITS_WrapperMembers objWrapperMember = new SVNSUMMITS_WrapperMembers(
						Query,
						intLimit,
						null,
						setUserId,
						false,
						false);
				objWrapperMember.pageNumber = pageNumberValue;

				return objWrapperMember;
			} else {

				return null;
			}
		} catch (Exception e) {

			system.debug('*** Exception membersPaginantion ***' + e);
			return null;
		}
	}

	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers previousPageEx(
			Integer numberOfMembers,
			Integer pageNumber,
			String sortBy,
			String searchMyMembers,
			String searchString,
			List<String> excludeList) {

		// Excluded the following IDs from the list
		excludedIds = new List<String>();

		// Ignore blank IDs
		for (String i : excludeList) {
			if (String.isNotBlank(i)) {
				excludedIds.add(i);
			}
		}

		return previousPage(numberOfMembers, pageNumber, sortBy, searchMyMembers, searchString);
	}

    /*
    @Name          :  previousPage
    @Description   :  Method used on click of "Previous" in pagination to diaplay news records as per numberOfNewsPerPage
    */
	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers previousPage(
			Integer numberOfMembers,
			Integer pageNumber,
			String sortBy,
			String searchMyMembers,
			String searchString) {

		try {
			SVNSUMMITS_WrapperMembers objWrapperMember = membersPaginantion(
					numberOfMembers,
					pageNumber,
					sortBy,
					searchMyMembers,
					searchString);

			if (objWrapperMember != null) {

				objWrapperMember.previousPage();

				objWrapperMember.setController = null;

				return objWrapperMember;
			} else {

				return null;
			}
		} Catch (Exception e) {

			system.debug('*** Exception previousPage ***' + e);
			return null;
		}
	}


    /*
    @Name          :  getSitePrefix
    @Description   :  Method to fetch site prefix so that urls are redirected properly dynamically in any org.
    */
	@AuraEnabled
	global static String getSitePrefix() {
		try {
			return System.Site.getPathPrefix();
		} Catch (Exception e) {

			return e.getMessage();

		}
	}

     /*
    @Name          :  getQueryString
    @Description   :  Method to generate dynamic query for objects used in process.
                      this method is generating query for TopicAssignment,Topic as per our requirement
    */

	public static string getQueryString() {
		try {
			Set<String> setUserId = new Set<String>();

			// Query to fetch the user records
			String QUERY = 'SELECT Id, Name, CommunityNickname, Email, Title, Phone, CreatedDate, MediumPhotoUrl FROM User ';
			if (!String.isEmpty(QUERY)) {

				// Query to fetch the user on the basis of network Id
				QUERY += ' WHERE IsActive=true AND (Empl_Status__c=\'Active\' OR In_Member_Directory__c=true) AND Id IN (SELECT MemberId FROM NetworkMember WHERE NetworkId = \'' + networkId + '\')';

				return QUERY;
			} else {

				return '';
			}
		} Catch (Exception e) {

			return e.getMessage();

		}
	}

    /*
    @Name          :  getNickNames
    @Description   :  Method to get the nicknames with validation to varify the names are not blank and to make repeated values optout
    */
	private static String getNickNames(String strRecordNickNames) {

		try {

			String strRecordNickName = '';
			Set<String> setMemberNicKNames = new Set<String>();

			if (!string.isBlank(strRecordNicknames)) {

				if (!setMemberNicKNames.contains(strRecordNicknames)) {

					setMemberNicKNames.add(strRecordNicknames);
					strRecordNickName = strRecordNicknames;
				}
			}
			return strRecordNicknames;
		} catch (Exception e) {

			return e.getMessage();
		}

	}

      /*
    @Name          :  getFeaturedMembers
    @Description   :  Method to fetch featured member records to show on featured components.
    */
	@AuraEnabled
	global static SVNSUMMITS_WrapperMembers getFeaturedMembers(String recordNickName1, String recordNickName2, String recordNickName3, String recordNickName4, String recordNickName5,
			String recordNickName6, String recordNickName7, String recordNickName8) {

		try {
			List<String> lstMemberNicKNames = new List<String>();

			if (!string.isBlank(recordNickName1)) {

				lstMemberNicKNames.add(getNickNames(recordNickName1));
			}
			if (!string.isBlank(recordNickName2)) {

				lstMemberNicKNames.add(getNickNames(recordNickName2));
			}
			if (!string.isBlank(recordNickName3)) {

				lstMemberNicKNames.add(getNickNames(recordNickName3));
			}
			if (!string.isBlank(recordNickName4)) {

				lstMemberNicKNames.add(getNickNames(recordNickName4));
			}
			if (!string.isBlank(recordNickName5)) {

				lstMemberNicKNames.add(getNickNames(recordNickName5));
			}
			if (!string.isBlank(recordNickName6)) {

				lstMemberNicKNames.add(getNickNames(recordNickName6));
			}
			if (!string.isBlank(recordNickName7)) {

				lstMemberNicKNames.add(getNickNames(recordNickName7));
			}
			if (!string.isBlank(recordNickName8)) {

				lstMemberNicKNames.add(getNickNames(recordNickName8));
			}


			String sortBy = '';
			String searchMyMembers = '';
			String searchString = '';
			String Query = getQueryString();

			if (!string.isBlank(Query)) {
				Query += ' AND CommunityNickname IN : lstMemberNicKNames';

				SVNSUMMITS_WrapperMembers membersWrapper = new SVNSUMMITS_WrapperMembers(
						Query,
						INTLIMITMEMBERSRECORDS,
						lstMemberNicKNames,
						null,
						true,
						true);
				return membersWrapper;
			}
		} catch (Exception e) {

			system.debug('*** Exception getFeaturedMembers ***' + e);
			return null;
		}
		return null;
	}
}