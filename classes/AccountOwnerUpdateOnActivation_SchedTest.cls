@isTest
public class AccountOwnerUpdateOnActivation_SchedTest{

    public static void createTestData(){
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        String randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        User testUser = new User();
        testUser.alias = 'usert';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'usrLstName';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = UserInfo.getProfileId();
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + 'usrLstName'+'@xyz.com';
        testUser.EmployeeNumber = '12345678902';     
        testUser.IsActive = true;        
        insert testUser;         

        testColleague1.User_Record__c = testuser.Id;
        update testColleague1;
    }

    public static testMethod void testExecute()
    {
        Test.StartTest();

        createTestData();
    
        AccountOwnerUpdateOnActivation_Schedular schedulable = new AccountOwnerUpdateOnActivation_Schedular();     
        
        String sch = '0 0 23 * * ?'; 
        
        system.schedule('Test Opportunity Summary Insert Batch', sch, schedulable ); 
        
        Test.stopTest();
    
    }
}