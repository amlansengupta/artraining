/*Purpose: Test Class for providing code coverage to test all methods of PDFReportOnAccount class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   08/05/2013  Created test class
============================================================================================================================================== 
*/
@isTest
public class TestPDFReportOnAccount
{
    
    /*
     * @Description : Test Method for Redirect Method with Valid values of newOneCode and newGUDUNS
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void testredirect()
    {
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
       PageReference pg = Page.GUAccountProfile;
       Test.setCurrentPage(pg); 
       ApexPages.currentPage().getParameters().put('onecode','1256');
       ApexPages.currentPage().getParameters().put('guduns','1256');
       
       PDFReportOnAccount obj = new PDFReportOnAccount();
       obj.redirect();
        }
    }
 
      /*
     * @Description : Test Method for Redirect Method with Null values of newOneCode and newGUDUNS
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void testredirectException()
    {
    	  User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        PageReference pg = Page.GUAccountProfile;
       Test.setCurrentPage(pg); 
       ApexPages.currentPage().getParameters().put('onecode','');
       ApexPages.currentPage().getParameters().put('guduns','');
       
       PDFReportOnAccount obj = new PDFReportOnAccount();
       obj.redirect();
        }
    }
    
}