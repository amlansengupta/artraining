/****************************************************************************************************** 
Request# : 17444
Purpose: This test class is responsible to provide test coverage for RefreshBillRatesControllerApex class.
Created by : Harsh Vats 
Project : MF2
*******************************************************************************************************/

@isTest(seeAllData=true)
public class RefreshBillRatesControllerApex_Test
{
    private static Mercer_TestData mtdata = new Mercer_TestData();
    //private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    
    static testMethod void testRefreshBillRateFunctionality()
    {
        ConstantsUtility.skipTestClass = true;
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) 
        {
            //String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
            Employee_Bill_Rate__c Ebr = new  Employee_Bill_Rate__c ();
            ebr.bill_cost_ratio__c =6.06;
            ebr.type__c='Generic Employee';
            ebr.bill_rate_local_currency__c=213;
            ebr.Business__c='Property Administration';
            ebr.Level__c='A';
            ebr.currenCyISOCode ='ALL';
            ebr.Market_Country__c='Chile';
            ebr.Sub_Business__c = 'BPO Services';
            ebr.Employee_Status__c = 'Active';
            insert ebr;
            String employeeBillrate = ebr.id;
            String LOB = 'Retirement';
            
            //Account test data creation
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName1';
            testAccount1.BillingCity = 'TestCity1';
            testAccount1.BillingCountry = 'TestCountry1';
            testAccount1.BillingStreet = 'Test Street1';
            //testAccount1.Relationship_Manager__c = collId;
            testAccount1.One_Code__c='ABC123XYZ1';
            testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
            testAccount1.Integration_Status__c = 'Error';
            testAccount1.Account_Region__c = 'EuroPac';
            insert testAccount1;
            //String accID = testAccount1.ID;
            
            
            
            /*Product2 testProduct = new Product2();
            testProduct.Name = 'TestPro';
            testProduct.Family = 'RRF';
            testProduct.IsActive = True;
            testProduct.Classification__c = 'Consulting Solution Area';
            testProduct.LOB__c = LOB;
            insert testProduct;
            ID prodId = testProduct.id;*/
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'test oppty';
            testOppty.Type = 'New Client';
            //#12638 sprint 2 dependency
            //testOppty.Step__c = 'Identified Deal';
            //testOpportunity.Step__c = 'Pending Chargeable Code';
            //#12638 sprint 2 dependency
            testOppty.StageName = 'Identify';
            //testOpportunity.CloseDate = date.parse('1/1/2015'); 
            testOppty.CloseDate =date.today();
            testOppty.CurrencyIsoCode = 'ALL';        
            testOppty.Opportunity_Office__c = 'Urbandale - Meredith';
            testOppty.Opportunity_Country__c = 'CANADA';
            testOppty.currencyisocode = 'ALL';
            //testOppty.Close_Stage_Reason__c ='Other';
            testOppty.AccountId = testAccount1.ID;
            insert testOppty;
            ID  opptyId  = testOppty.id;
            
            /*Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
            
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;*/
            
            
            Current_Exchange_Rate__c testCurrExch = new Current_Exchange_Rate__c();
            testCurrExch.name = 'ALL';
            testCurrExch.Conversion_Rate__c = 1;
            insert testCurrExch;
            Current_Exchange_Rate__c testCurrExch2 = new Current_Exchange_Rate__c();
            testCurrExch2.name = 'USD1';
            testCurrExch2.Conversion_Rate__c = 2;
            insert testCurrExch2;
            
            ScopeIt_Project__c testscopeitProject = new ScopeIt_Project__c();
            testscopeitProject.name = 'testscopeitproj';
            //testscopeitProject.Product__c =  prodId;
            //testscopeitProject.Bill_Type__c = 'Billable';                
            testscopeitProject.opportunity__c = opptyId;
            //testscopeitProject.OpportunityProductLineItem_Id__c = oliId;
            //testscopeitProject.CurrencyIsoCode = oliCurr;
            //testscopeitProject.Sales_Price__c=oliUnitprice;
            insert testscopeitProject;
            Id scopeitprojectid = testscopeitProject.Id;
            
            //ScopeIt_Task__c testscopeTask = mtscopedata.buildScopeitTask();
            //Creating ScopeIt task record
            ScopeIt_Task__c testscopeTask = new ScopeIt_Task__c();
            testscopeTask.task_name__c = 'testscopetask';
            testscopeTask.ScopeIt_Project__c = scopeitprojectid;
            //testscopeTask.Net_Price__c = netPrice;       
            testscopeTask.Billable_Expenses__c = 2000;
            testscopeTask.Bill_Rate__c = 20;
            //testscopeTask.Cost_Rate__c = 30;
            insert testscopeTask;
            
            /*Scope_Modeling__c testScopeModelProj = new Scope_Modeling__c();
            testScopeModelProj.name = 'testscopeModelproj';
            testscopeitProject.Product__c =  prodId;                             
            testscopeitProject.CurrencyIsoCode = 'ALL'; 
            testscopeitProject.Sales_Price__c = 200;   
            insert testScopeModelProj;
            Scope_Modeling_Task__c  testScopeModelTask =  new Scope_Modeling_Task__c();  
            testScopeModelTask.Scope_Modeling__c = testScopeModelProj.Id;               
            testScopeModelTask.Billable_Expenses__c = 2000;
            testScopeModelTask.Bill_Rate__c = 20;        
            insert testScopeModelTask;
            Scope_Modeling_Employee__c testScopeModelEmployee =  new Scope_Modeling_Employee__c();
            testScopeModelEmployee.Scope_Modeling_Task__c = testScopeModelTask.id;
            testScopeModelEmployee.Employee_Bill_Rate__c = employeeBillrate;
            testScopeModelEmployee.Cost__c = 500;
            testScopeModelEmployee.Hours__c = 2;
            testScopeModelEmployee.CurrencyISOCOde= 'ALL';
            testScopeModelEmployee.Billable_Time_Charges__c = 100;
            testScopeModelEmployee.Bill_Rate_Opp__c =20;
            insert testScopeModelEmployee;*/
            ScopeIt_Employee__c testScopeItEmployee = new ScopeIt_Employee__c();
            testScopeItEmployee.ScopeIt_Task__c = testscopeTask.id;
            testScopeItEmployee.Employee_Bill_Rate__c = employeeBillrate;
            testScopeItEmployee.Cost__c = 500;
            testScopeItEmployee.Hours__c = 2;
            testScopeItEmployee.currenCyISOCode = 'ALL';
            testScopeItEmployee.Billable_Time_Charges__c = 100;
            testScopeItEmployee.Bill_Rate_Opp__c =20;
            insert testScopeItEmployee;
            //testScopeItEmployee.ScopeIt_Task__c = testscopeTask.Id;
            //update testScopeItEmployee;
            system.assert(opptyId != null);
            RefreshBillRatesControllerApex.refreshBillRates(opptyId);
        }
    }
}