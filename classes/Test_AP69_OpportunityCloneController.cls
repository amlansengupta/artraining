/*Purpose:   This test class provides data coverage to AP69_OpportunityCloneController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   9/26/2013  Created test class 
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData=true)
private class Test_AP69_OpportunityCloneController{
    /*
     * @Description : Test class to provide data coverage for opportunity clone functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest1() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'testabc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            Test.starttest();
               Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id:12638 Commenting step
            //testOppty.Step__c = 'Identified Deal';
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'New York - 1166';
            testOppty.Amount = 100;
            testOppty.Buyer__c= testContact.id;  
            testOppty.CurrencyIsoCode='USD';
            testOppty.Concatenated_Products__c='529 Plan;DB Risk';
            insert testOppty;
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            AP69_OpportunityCloneController controller = new AP69_OpportunityCloneController(stdController);
            controller.newOppty.AccountId = testAccount.Id;
            /*************Replacing getstepVal() with getStageVal() START**************/
            //controller.getstepVal();
            controller.getStageVal();
            /*************Replacing getstepVal() with getStageVal() END**************/
            controller.getcloseStageReasonVal();
            controller.getChargeCodeExepVal();
            controller.saveClone();
            //PageReference pageref1 = controller.NewClone();
            controller.cancelClone();
            controller.NewClone();
            Test.stoptest();
        }

    }
    /*
     * @Description : Test class for negative testing
     * @ Args       : Null
     * @ Return     : void
     */
      static testMethod void myUnitTest1_ExceptionUseCase() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity1';
        testAccount.BillingCountry = 'TestCountry1';
        testAccount.BillingStreet = 'Test Street1';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'testabc1@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        test.starttest();
            Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'New York - 1166';
        testOppty.Amount = 100;
        testOppty.Buyer__c= testContact.id;  
        testOppty.CurrencyIsoCode='USD';
        testOppty.Concatenated_Products__c='529 Plan;DB Risk';
            testOppty.Reminder__c = true;
            insert testOppty;
            
           // List<OpportunityTeamMember> otm = [ select id ,OpportunityId from OpportunityTeamMember where OpportunityId =: testOppty.Id and TeamMemberRole !='Opportunity Owner'];
            //System.debug('&&&'+otm);
            //delete otm;
            test.stoptest();
        }

    }
}