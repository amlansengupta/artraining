/*Purpose:  This controller class is created for adding/removing campaign members
 =====================================================================================================================================================
 History 
 ----------------------- 
 VERSION     AUTHOR      DATE        DETAIL 
  1.0 -      Sarbpreet   1/4/2016  As part of request # 8508(March'16 Release) Created controller class for adding/removing campaign members
=====================================================================================================================================================
 */
public with sharing class AP132_CampaignMemberCustom {
        public  Campaign camp {get;set;}
        private String qString;
        private String attachString;
        private string contact_country = '';
        public contact[] avlContacts {get;set;}
        public Attachment[] attachments {get;set;}
        public Boolean inReviewStatus {get;set;}
        
        public String searchParam {get;set;}   
        public String PR1 {get;set;}
        public String PR2 {get;set;}
        public String PR3 {get;set;}
        
        private static final string STR_First_Name = 'First Name';
        private static final string STR_Last_Name = 'Last Name';
        private static final string STR_Job_Function = 'Job Function';
        private static final string STR_Job_Level = 'Job Level';
        private static final string STR_Account_Name = 'Account Name';
        private static final string STR_Equals = 'Equals';
        private static final string STR_Contains = 'Contains';
        private static final string STR_Starts_With = 'Starts With';
        private static final string STR_InReview = 'In review';
    	private static final string STR_InProgress = 'In Progress';
        
        public double perpageRecord{get;set;}
        private double totalRecs = 0;
        private integer OffsetSize = 0;
        private integer LimitSize= 30;
        public integer currentPages{get;set;}
        public integer currentTotalPages{get;set;}
        
        public integer PageNum {get;set;}
        public double pages {get;set;}
        
        public List<CampaignMember> contMemberList {get;set;}
        public Map<id, CampaignMember>  editContactMemberMap ;
        Set<ID> campMemId = new Set<ID>();
        Map<ID, CampaignMember> campcontIDMap = new Map<ID, CampaignMember>();
        public List<CampaignMember> tempupdateCampMemberList = new List<CampaignMember>();
        
        public integer endRowNumber{get;set;}
        public integer endRowOfTable{get;set;}
        
        
        public List<ContactWrapper> contactAll { get;set;} 
           
    

     // the current sort direction. defaults to asc
    public String sortDir {
        get {
            if (sortDir == null) {
                sortDir = 'asc';
            }
            return sortDir;
        }
        set;
    }

    // the current field to sort by. 
    public String sortField {
        get {
            if (sortField == null) {
                sortField = 'c.account.name, c.lastname';
            }
            return sortField;
        }
        set;
    }



   /*
    * @Description : This method toggles the sorting of query from asc<-->desc     
    * @ Args       : null     
    * @ Return     : void
    */ 
     public void toggleSort() {
        try {
            // simply toggle the direction
            sortDir = sortDir.equals('asc') ? 'desc' : 'asc';           

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
    }
    
    
        
    /*
      Creation of constructor for AP132_CampaignMemberCustom class
    */
    public AP132_CampaignMemberCustom(ApexPages.StandardController controller) {
        
        endRowNumber = 5;
        endRowOfTable = 0;
        perpageRecord = 25;
        PageNum = 1;
        Pages =0;

        
        contactAll = new List<ContactWrapper>();
        string IDS = ((Campaign)controller.getRecord()).id;//ApexPages.currentPage().getParameters().get('ids');
        camp = database.query('select Id,Name,Scope__c,Campaign_ID__c, Country__c, LOB__c, Market__c, Sub_Market__c, Marketo_Score__c, Office__c, Region__c, Sub_Region__c, Type, Status, StartDate, EndDate, Description,Ownerid, currencyISOCode, owner.name from Campaign where Id = \'' + IDS + '\' limit 1');
        //Feedback#3156 to add In Progress status along with In review status for adding contact.
        if(STR_InReview!=(String.escapeSingleQuotes(camp.Status)) && STR_InProgress != (String.escapeSingleQuotes(camp.Status)))
        {
                inReviewStatus = true;
        }
        editContactMemberMap =new Map<id,CampaignMember> ([Select c.Id, c.ContactId,c.Contact.LastName,c.Contact.FirstName,c.status, c.CampaignId, c.contact.name, c.LastModifiedDate, c.LastModifiedBy.firstname,LastModifiedBy.lastname From CampaignMember c where c.CampaignId =:camp.id and c.contact.ownerid =:UserInfo.getUserId()]);
       
        for(CampaignMember cmr : editContactMemberMap.values())
        {
            campcontIDMap.put(cmr.contactID, cmr);
        }
        contMemberList = new List<CampaignMember>() ;
        
        contact_country = 'Canada';
         qString = 'Select c.OwnerId, c.Name,c.FirstName,c.LastName, c.HasOptedOutOfEmail, c.Contact_Status__c, c.title,c.Last_Name_Local_Language__c, c.First_Name_Local_Language__c,c.Job_Function__c, c.Job_Level__c, c.Email, c.Invalid_Work_Email__c, c.Functional_Area_of_Interest__c, c.AccountID, c.Account.One_Code__c, c.Account.name,c.Account.Employees__c    From Contact c where c.Contact_Status__c = \'Active\' and c.HasOptedOutOfEmail = false and c.OwnerId =\''+ UserInfo.getUserId() + '\' and (((c.OtherCountry != \''+ String.escapeSingleQuotes(contact_country) + '\' and c.MailingCountry!=\'' + String.escapeSingleQuotes(contact_country) + '\') and c.Email_Opt_In__c= false) or ((c.OtherCountry = \''+ String.escapeSingleQuotes(contact_country) + '\' or c.MailingCountry=\'' + String.escapeSingleQuotes(contact_country) + '\') and c.Email_Opt_In__c!= false))';
         
        attachString= 'Select a.ParentId, a.Name, a.Id, a.LastModifiedDate, a.CreatedById, a.CreatedBy.firstname, a.CreatedBy.lastname From Attachment a where a.ParentId = \'' + camp.id + '\'' ; 
        runQuery();
        
    }
    
       /*
     * @Description : This method runs the actual query.
     * @ Args       : null
     * @ Return     : void
     */
    public void runQuery() {

   try {
                                
            if(totalRecs !=null && totalRecs ==0){
                List<Contact> contTemp = Database.query(qString);
                totalRecs = (contTemp !=null &&contTemp.size()>0)?contTemp.size():0;
            }
            
            if(integer.valueOf(totalRecs) > 0)
            currentPages = 1;
            else
            currentPages = 0;
    
            if(integer.valueOf(totalRecs)<25)
            {
                currentTotalPages = integer.valueOf(totalRecs);
            }
            else
            {
                currentTotalPages = 25;
            }
    
            qString += ' ORDER BY  '+sortField + ' '+ sortDir;
            avlContacts  =Database.query(qString);   
            attachments = database.query(attachString); 

            for(contact con : avlContacts )
            {                    
                contactAll.add(new ContactWrapper(con,editContactMemberMap));

            }

       
       } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }

    }
    
    
     /*
    * @Description : Method for searching the contacts
    * @ Args       : null
    * @ Return     : PageReference 
    */     
    public PageReference runSearch() {
         try {
           contactAll.clear();
           qString = 'Select c.OwnerId, c.Name,c.FirstName,c.LastName, c.HasOptedOutOfEmail, c.Contact_Status__c, c.title,c.Last_Name_Local_Language__c, c.First_Name_Local_Language__c,c.Job_Function__c, c.Job_Level__c, c.Email, c.Invalid_Work_Email__c, c.Functional_Area_of_Interest__c, c.AccountID, c.Account.One_Code__c, c.Account.name,c.Account.Employees__c    From Contact c where c.Contact_Status__c = \'Active\' and c.HasOptedOutOfEmail = false and c.OwnerId =\''+ UserInfo.getUserId() + '\' and (((c.OtherCountry != \''+ String.escapeSingleQuotes(contact_country) + '\' and c.MailingCountry!=\'' + String.escapeSingleQuotes(contact_country) + '\') and c.Email_Opt_In__c= false) or ((c.OtherCountry = \''+ String.escapeSingleQuotes(contact_country) + '\' or c.MailingCountry=\'' + String.escapeSingleQuotes(contact_country) + '\') and c.Email_Opt_In__c!= false))';
      
        if (searchParam != null) {
                    qString += ' and  (c.FirstName LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\' or c.LastName LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\' or c.Job_Level__c LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\' or c.Account.Name LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\')';
                }
            if (STR_First_Name.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (c.FirstName = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_Last_Name.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (c.LastName = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }
               

                if (STR_Job_Level.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (c.Job_Level__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }
                
                if (STR_Account_Name.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (c.Account.Name = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_First_Name.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (c.FirstName LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Last_Name.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (c.LastName LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }
            

                if (STR_Job_Level.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (c.Job_Level__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }
                
                 if (STR_Account_Name.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (c.Account.Name LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_First_Name.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (c.FirstName LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Last_Name.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (c.LastName LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }
               

                if (STR_Job_Level.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (c.Job_Level__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }
                
                if (STR_Account_Name.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (c.Account.Name LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

        runQuery();
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        return null;
    }
    
    
     /*
     * @Description : This method clears the search filters  
     * @ Args       : null     
     * @ Return     : void
     */
    public void clearSearch() {
        try {
            if (searchParam != null) {
                searchParam = ConstantsUtility.STR_Blank;
            }
            if (PR3 != null) {
                PR3 = ConstantsUtility.STR_Blank;
            }
            if (PR2 != null) {
                PR2 = ConstantsUtility.STR_Blank;
            }
            if (PR1 != null) {
                PR1 = ConstantsUtility.STR_Blank;
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
    }
    
   
   
    /* @Description : Method for adding selected contacts as campaigm members
    * @ Args       : null
    * @ Return     : String
    */ 
    public PageReference selectedCont(){           
            for(ContactWrapper ctChecked : contactAll){
                    if(ctChecked.checked) {                 
                           if(!campcontIDMap.containsKey(ctChecked.cont.id)) {
                                CampaignMember campmemberObj= new CampaignMember(CampaignId=camp.Id,ContactId=ctChecked.cont.id, status='Added by Contact Owner', Skip_Campaign_Member_Validation__c=true);
                                contMemberList.add(campmemberObj);                        
                                editContactMemberMap.put(campmemberObj.ContactId, campmemberObj);
                   }
                    
                     if(campcontIDMap.containsKey(ctChecked.cont.id)) {
                                CampaignMember cmp = campcontIDMap.get(ctChecked.cont.id);
                               
                             if((cmp.status == 'Initial List' || cmp.status == 'Member' ||  cmp.status == 'Identified') && (cmp.status!='Added by Contact Owner' &&  cmp.status!='Removed by Contact Owner' ))  {       
                             //if(cmp.status!='Added by Contact Owner' &&  cmp.status!='Removed by Contact Owner' )
                                        cmp.Skip_Campaign_Member_Validation__c=true;
                                        cmp.status = 'Affirmed by Contact Owner';
                                        contMemberList.add(cmp);
                                 }
                                 else {
                                    if(cmp.status!= 'Affirmed by Contact Owner') {
                                    cmp.Skip_Campaign_Member_Validation__c=true;
                                    cmp.status = 'Added by Contact Owner';
                                    contMemberList.add(cmp);                        
                                    editContactMemberMap.put(cmp.ContactId, cmp);
                                    }
                                 }
                                 
                            }                                                  
                  
                   }
                  if(!ctChecked.checked)
                  {
                            if(campcontIDMap.containsKey(ctChecked.cont.id)) {
                                CampaignMember cmp = campcontIDMap.get(ctChecked.cont.id);
                                if(cmp.status == 'Initial List' || cmp.status == 'Member' || cmp.status == 'Added by Contact Owner' || cmp.status == 'Identified'|| cmp.status == 'Affirmed by Contact Owner')
                                {
                                    cmp.Skip_Campaign_Member_Validation__c=true;
                                    cmp.status = 'Removed by Contact Owner';
                                    contMemberList.add(cmp);                        
                                    editContactMemberMap.put(cmp.ContactId, cmp);     
                                }                          
                         }
                  }
                  
                  
                 
            }
            
         return null;   
    }
   
    
     /*
     * @Description : This method saves the campaign members and returns to campaign page.  
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public PageReference saveButton() {
        PageReference pageReference;
        try {
             
            selectedCont();
            upsert contMemberList;
            pageReference = new PageReference('/' + camp.id);
        } catch (System.DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
            pageReference = null;
        }
        return pageReference;
    }
    
     /*
     * @Description : This method is called when Cancel button is clicked.
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public PageReference cancelButton() {
        PageReference pageReference;
        try {
            pageReference = new PageReference('/' + camp.id);
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            pageReference = null;
        }
        return pageReference;
    }
    
     /*
    *   Contact  Wrapper class
    */
    public class ContactWrapper {
    public  Contact Cont { get; set;}   
    public  Boolean checked {get; set;}
    public String ModifiedBy {get; set;}
    public String ModifiedDate {get; set;}
    
    /*
    *   Constructor for ContactWrapper class
     */
     public ContactWrapper(Contact cd, Map<id, CampaignMember> CampaignMemberMap){
            Cont = cd;
            checked = false;
        Map<ID, CampaignMember> contIDsMap = new Map<ID, CampaignMember>();
        for(CampaignMember cmr : CampaignMemberMap.values())
        {
            contIDsMap.put(cmr.contactID, cmr);
        }
             if(contIDsMap.containsKey(Cont.id))
             {
                CampaignMember cm = contIDsMap.get(Cont.id);
                if(cm.status == 'Initial List' || cm.status == 'Member' || cm.status == 'Added by Contact Owner' || cm.status == 'Identified'|| cm.status == 'Affirmed by Contact Owner')
                {
                        checked = true;
                }
                else 
                {
                    checked = false;
                }
                if(cm.LastModifiedBy.firstname ==  null){
                        ModifiedBy =  '' +  cm.LastModifiedBy.lastname;
                }
                else {
                        ModifiedBy = cm.LastModifiedBy.firstname + ' ' +  cm.LastModifiedBy.lastname;
                }
                if(cm.LastModifiedDate!=null) {
                    Datetime dT = cm.LastModifiedDate;
                    ModifiedDate = dT.format('dd-MMM-YYYY');
                }
                
             } 
         }
    }
    
  

public pagereference firstpage(){
    endRowOfTable = 0;
     PageNum =1;
     currentPages =1;
     if(integer.valueOf(totalRecs)<25)
             {
                currentTotalPages = integer.valueOf(totalRecs);
             }
             else
             {
                currentTotalPages = 25;
             }

    return null;
}
public pagereference nextpage(){
    endRowOfTable = Integer.valueOf(endRowOfTable+perpageRecord) ;
    currentPages =currentPages + Integer.valueOf(perpageRecord);
    currentTotalPages = currentTotalPages + Integer.valueOf(perpageRecord);
    if(currentTotalPages > totalRecs)
    {
        integer tempPage = 0;
        integer temp1 =integer.valueOf(totalRecs);
        integer temp2 =integer.valueOf(perpageRecord);
        tempPage = Integer.valueOf(totalRecs-  math.mod(temp1,temp2));
        currentTotalPages = tempPage + math.mod(temp1,temp2);
        
    }
    PageNum++;
    return null;
}
public pagereference previouspage(){
    endRowOfTable = Integer.valueOf(endRowOfTable-perpageRecord) ;
    currentPages =currentPages - Integer.valueOf(perpageRecord);
    integer temp1 =integer.valueOf(totalRecs);
    integer temp2 =integer.valueOf(perpageRecord);
    integer temp3 =integer.valueOf(currentTotalPages);
    if(math.mod(temp1,temp2) == math.mod(temp3,temp2))
    {
        currentTotalPages = currentTotalPages - math.mod(temp1,temp2);
    }
    else 
    {
        currentTotalPages = currentTotalPages - Integer.valueOf(perpageRecord);
    }
    PageNum--;

    return null;
} 
public pagereference lastpage(){    
    integer temp1 =integer.valueOf(totalRecs);
    integer temp2 =integer.valueOf(perpageRecord);
    endRowOfTable = Integer.valueOf(totalRecs-  math.mod(temp1,temp2));
    PageNum = Integer.valueOf(Math.ceil(totalRecs/perpageRecord));
    currentPages = Integer.valueOf(totalRecs-  math.mod(temp1,temp2)) + 1;
    currentTotalPages = Integer.valueOf(totalRecs);
    return null;
} 

Public Integer getTotalPages(){
        pages = totalRecs/perpageRecord;
        return Integer.valueOf(Math.ceil(pages));
    }

}