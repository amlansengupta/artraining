public class customLookUpProdControllerApex {
		 
    @AuraEnabled
    public static List <sObject> fetchLookUpValues(String searchKeyWord, String ObjectName, String filter) {
        String searchKey = searchKeyWord + '%';
        List<sObject> returnList = new List<sObject> ();
      	String sQuery;
        system.debug('Hello '+searchKey);
        if(ObjectName.equalsIgnoreCase('Account')){
         	  sQuery = 'select id, Name, OwnerId, Owner.Name, Phone from Account where Name LIKE: searchKey order by createdDate DESC limit 5';  
        }
        /*else if(ObjectName.equalsIgnoreCase('CustomObject...')){
          ...
        }*/
        else{
            system.debug('Hi '+filter);
            if(filter != null && filter != ''){
                //filter.replace('/', '');
                String activeFilter = filter.replaceAll('/' ,'\'');
                system.debug('Hi '+activeFilter);
                
                sQuery = 'select id, Name,Level_1_Descr__c,LOB__c,Location_Descr__c,Country__c,Empl_Status__c,Email_Address__c from ' +ObjectName + ' where Name LIKE: searchKey AND ' + activeFilter + ' order by createdDate DESC limit 20'; 
                
            }
            else{
                sQuery = 'select id, Name,Level_1_Descr__c,LOB__c,Location_Descr__c,Country__c,Empl_Status__c,Email_Address__c from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 20'; 
            }
           
        }
        system.debug('Hey '+sQuery);
        List<sObject> lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    @AuraEnabled
    public static sObject fetchDefaultLookUp(String recId, String ObjectName) {
        String sQuery = 'select id, Name from ' +ObjectName + ' where Id =: recId ';
        sObject  Record = Database.query(sQuery);
        return Record;
    }
}