/*
This is a test method for GlobalContractAfter trigger.

                               Version History
  Version       Author          Date                   Comments
    1.0       Venkatesh    21st July 2015  Added as a part of July release 2015
           
*/  
@istest(seeAllData = true)
public class Test_GlobalContractAfter{
    
    /*
    Test method for After Delete on Global Contract onject.
    */
    Static TestMethod void testAfterDelete(){
    
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.One_Code__c = 'ARFB12';
        testAccount.GU_DUNS__c = '1256';   
        testAccount.DUNS__c = '1256'; 
        testAccount.Account_Country__c = 'India';
        testAccount.Account_Region__c = 'International';
        testAccount.Account_Market__c = 'ASEAN';
        testAccount.Market_Segment__c = '4 - Key';
        testAccount.Employees_Editable__c = 123;
        testAccount.billingstreet = 'A-302';
        testAccount.billingcountry = 'India';
        Insert testAccount;
        
        Global_Contract_Account__c GAC = new Global_Contract_Account__c ();
        GAC.Global_Account_Name__c = testAccount.Id;
        GAC.Global_Contract__c = 'Yes';
        GAC.Global_Rate_Card__c = 'Custom';
        GAC.Global_Special_Terms__c = 'Global Discount & Incentive';
        Insert GAC;
        
        testAccount = [Select Id, Global_Contract__c, Global_Rate_Card__c, Global_Special_Terms__c  From Account Where Id =: testAccount.Id];
        
        System.AssertEquals(testAccount.Global_Contract__c, GAC.Global_Contract__c);
        System.AssertEquals(testAccount.Global_Rate_Card__c, GAC.Global_Rate_Card__c);
        System.AssertEquals(testAccount.Global_Special_Terms__c, GAC.Global_Special_Terms__c);      
        
        Delete GAC; 
        
        testAccount = [Select Id, Global_Contract__c, Global_Rate_Card__c, Global_Special_Terms__c  From Account Where Id =: testAccount.Id];
        System.AssertEquals(testAccount.Global_Contract__c, null);
        System.AssertEquals(testAccount.Global_Rate_Card__c, null);
        System.AssertEquals(testAccount.Global_Special_Terms__c, null); 
        
    }
}