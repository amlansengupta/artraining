/*Purpose:  This Apex class is created as a Part of ScopeIt Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Display Tasks for the scopeit project
.Method to Clear Search and Perform search for Employees
.Add Employees to selected task


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE            DETAIL 
   1.0 -    Jagan       03/31/2014      This Apex class works as Controller for "Mercer_Add_Employees" VF page which is invoked from 
                                        "Add Employees" button on Employees Related list in scopeIt project.
   2.0 -    Sarbpreet   7/3/2014        Updated Comments/documentation.
   3.0 -    Madhavi     7/24/2014       As part of PMO Request 4258(Sep 2014 Release) added Sub_Business__c fields  to the query filter.
                                        updated "Mercer_Add_Employees" vf page for this requirement
   4.0 -    Madhavi     7/24/2014       As part of PMO Request 4338(Sep 2014 Release) added Employee_Status__c fields to the query filter.
============================================================================================================================================== 
*/
public with sharing class Mercer_Add_Employees_class {
    
    public String namesearch {get;set;}
    public String levelsearch {get;set;}    
    
    public String selectedTask {get;set;}
    public list<selectoption> TaskNames {get;set;}
    
    public String selectedModTask {get;set;}
    public list<selectoption> ModTaskNames {get;set;}
    
    public Employee_Bill_Rate__c emp {get;set;}
    public boolean renderTask {get;set;}
    public List<EmpWrapper> empWrpList = new List<EmpWrapper>();
    //public List<List<EmpWrapper>> empWrpList = new List<List<EmpWrapper>>();
    public List<Selempwrapper> selempWrpList {get;set;}
    public ScopeIt_Employee__c empHr {get;set;}    
    
    List<Employee_Bill_Rate__c> selEmpList = new List<Employee_Bill_Rate__c>();
    
    Set<Id> selset = new Set<Id>();
    String staskid;
    public String oppCurrCodename {get;set;}    
    Set<Id> selectedEmpsinPage = new Set<Id>();
    
    
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
 
    //Integer pagesize = 20;
    //Integer page = 100;
    //Integer counter = (page - 1) * list_size; 
    public ScopeIt_Task__c Objsit {get;set;} 
    public Scope_Modeling_Task__c ObjModsit {get;set;} 
    
    public id projId {get;set;}     
    public String objectType {get;set;}     
    /*
    *   Constructor for Mercer_Add_Employees_class class
    */
    public Mercer_Add_Employees_class(ApexPages.StandardController controller) {
        emp = new Employee_Bill_Rate__c();
        empHr = new ScopeIt_Employee__c();
        selempWrpList = new List<Selempwrapper>();
        staskid = system.currentPageReference().getParameters().get('taskid');
        projId = system.currentPageReference().getParameters().get('projid');
        objectType = system.currentPageReference().getParameters().get('object');
        
        total_size = Integer.valueof(Label.ScopeIt_Emplyee_Count);//[select count() from Employee_Bill_Rate__c]; 
        getEmployees();
        getSelectedEmployees();
        if(staskid <> null && staskid <> ''){
            renderTask = true;
            Objsit = [select id,name,CurrencyIsoCode,scopeit_project__c from ScopeIt_Task__c where id=:staskid];
            projid= Objsit.scopeit_project__c;
            oppCurrCodename = Objsit.currencyisocode;
            oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].Conversion_Rate__c;
            OpportunityCode =  [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].name;
        }
        else{
            if(objectType == null || objectType == ''){
                renderTask = false;
                TaskNames = new List<SelectOption>();
                Scopeit_project__c scp =[select opportunity__r.currencyisocode from scopeit_project__c where id=:projid];
                oppCurrCodename = scp.opportunity__r.currencyisocode;
                OpportunityCode =  [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].name;
                oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :OpportunityCode].Conversion_Rate__c;
                
                TaskNames.add(new selectOption('--None--','--None--'));
                for( ScopeIt_Task__c Objsit: [select id,name,task_name__c,CurrencyIsoCode,scopeit_project__c from ScopeIt_Task__c where scopeit_project__c= :projid])
                TaskNames.add(new selectOption(Objsit.id,Objsit.task_name__c));
            }
            else{
                renderTask = false;
                ModTaskNames = new List<SelectOption>();
                Scope_Modeling__c scp =[select currencyisocode from Scope_Modeling__c where id=:projid];
                oppCurrCodename = scp.currencyisocode;
                OpportunityCode =  [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].name;
                oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :OpportunityCode].Conversion_Rate__c;
                
                ModTaskNames.add(new selectOption('--None--','--None--'));
                for( Scope_Modeling_Task__c Objsit: [select id,name,task_name__c,CurrencyIsoCode,Scope_Modeling__c from Scope_Modeling_Task__c where Scope_Modeling__c = :projid])
                ModTaskNames.add(new selectOption(Objsit.id,Objsit.task_name__c));        
            }
        }
    }
    Decimal oppcurrcode;
    public String OpportunityCode {get;set;}
  /*
   * @Description : This method cancels the current page and redirects to scopeit project page
   * @ Args       : null
   * @ Return     : pageReference
   */      
    public pageReference cancel(){
        return new PageReference('/'+projid);
        
    }
  /*
   * @Description : This method clears the values from the fields
   * @ Args       : null
   * @ Return     : pageReference
   */ 
    public void clearFields(){
        namesearch = null;
        levelsearch = null;
    }
  /*
   * @Description : Method for removing selected employees
   * @ Args       : null
   * @ Return     : void
   */
    public void RemoveEmployees(){
        List<Selempwrapper> newempwrapper = new list<Selempwrapper>();
        integer selcnt =0;
        for(Selempwrapper selemp:selempWrpList){
            if(selemp.isSelected == true)
            selcnt++;
            else
            newempwrapper.add(selemp);
        }
        if(newempwrapper.size()>0){
            selempWrpList.clear();
            selempWrpList.addAll(newempwrapper);
        }
        else if(selcnt ==selempWrpList.size()){
            selempWrpList.clear();
        }
        
        
    }
   /*
   * @Description : Method for saving employees
   * @ Args       : null
   * @ Return     : void
   */
    public pageReference SaveEmployees(){
        try{
            if(objectType == null || objectType == ''){
                if(staskid==null || staskid=='' || staskid=='--None--' )
                    staskid = selectedTask;
                
                system.debug('selectedTask.....'+selectedTask+'.....'+staskid);
                
                if(staskid == null || staskid == '' || staskid == '--None--' || selempWrpList.size()==0){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select Task & employees to save.'));
                     return null;
                }
                else{
                    List<ScopeIt_Employee__c> lstScopEmp = new list<ScopeIt_Employee__c>();
                    Scopeit_Task__c objTask = [select id,scopeit_project__c from scopeit_task__c where id=:staskid];
                    
                    for(Selempwrapper selemp:selempWrpList){
                        ScopeIt_Employee__c objemp = new ScopeIt_Employee__c();
                        objemp.Employee_Bill_Rate__c = selemp.recid;
                        objemp.CurrencyISOCode = OpportunityCode;
                        objemp.bill_rate_opp__c = selemp.billrate;
                        objemp.ScopeIt_Task__c = staskid;
                        objemp.ScopeIt_Project__c = objTask.scopeit_project__c;
                        objemp.Hours__c = selemp.hours;
                        lstScopEmp.add(objemp);
                    }
                    database.insert(lstScopEmp);
                    if(projid<>null){
                        PageReference p= new PageReference('/'+projid);
                        p.setRedirect(true);
                        return p;
                    
                    }
                    else
                    return new PageReference('/'+staskid);
                }
            }
            else {
                if(staskid==null || staskid=='' || staskid=='--None--' )
                    staskid = selectedModTask;
                
                if(staskid == null || staskid == '' || staskid == '--None--' || selempWrpList.size()==0){
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select Task & employees to save.'));
                     return null;
                }
                else{
                    List<Scope_Modeling_Employee__c> lstScopEmp = new list<Scope_Modeling_Employee__c>();
                    
                    system.debug('selectedTask.....'+selectedTask+'.....'+staskid);
                    Scope_Modeling_Task__c objTask = [select id,scope_modeling__c from Scope_Modeling_Task__c where id=:staskid];
                    
                    for(Selempwrapper selemp:selempWrpList){
                        Scope_Modeling_Employee__c objemp = new Scope_Modeling_Employee__c();
                        objemp.Employee_Bill_Rate__c = selemp.recid;
                        objemp.CurrencyISOCode = OpportunityCode;
                        objemp.bill_rate_opp__c = selemp.billrate;
                        objemp.Scope_Modeling_Task__c = staskid;
                        objemp.Scope_Modeling__c = objTask.Scope_Modeling__c;
                        objemp.Hours__c = selemp.hours;
                        lstScopEmp.add(objemp);
                    }
                    database.insert(lstScopEmp);
                    if(projid<>null){
                        PageReference p= new PageReference('/'+projid);
                        p.setRedirect(true);
                        return p;
                    
                    }
                    else
                    return new PageReference('/'+staskid);
                }        
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
            return null;
        }
    }
    
    public map <String,Decimal> empbillratemap = new Map<String,Decimal>();
  /*
   *    Getter method for employees
   */
    public List<EmpWrapper> getEmployees(){
        String nm =ConstantsUtility.STR_BLNK,etype=ConstantsUtility.STR_BLNK,emar=ConstantsUtility.STR_BLNK,ebus=ConstantsUtility.STR_BLNK,ls=ConstantsUtility.STR_BLNK,sub=ConstantsUtility.STR_BLNK;
        if(namesearch <>null) 
            nm = '%' +namesearch+'%';
        if(levelsearch <>null) 
            ls = '%' +levelsearch+'%';
            
        empWrpList.clear();
        //As part of PMO request 4258(Sep 2014 Release) added Sub_Business__c fields  to the query filter.
        //As part of PMO request 4338(Sep 2014 Release) added Employee_Status__c fields  to the query filter.
        String val ='Active';
        String query ='select currencyisocode,name,Type__c,Business__c,Sub_Business__c,Employee_Status__c,Market_Country__c, Bill_Cost_Ratio__c, Bill_Rate_Local_Currency__c,level__c from Employee_Bill_Rate__c ';
        String whercond ='where ';
        
        if(emp.type__c != null) {
                etype=emp.type__c;
                whercond += ' Type__c = \''+etype+'\' and';
        }
        if(emp.Market_Country__c != null){
                emar = emp.Market_Country__c;
                whercond += ' Market_Country__c = \''+emar+'\' and';
        }
        if(emp.Business__c != null){
                ebus = emp.Business__c;
                whercond += ' Business__c = \''+ebus+'\' and';
        }
        //**start**Added as part of  request 4258
         if(emp.Sub_Business__c!= null){
                sub = emp.Sub_Business__c;
                whercond += ' Sub_Business__c = \''+sub+'\' and';
        }//**end**Added as part of  request 4258
        if(nm<>null && nm<>'' && nm.length()>3)
                whercond += ' name like :nm and ';
        if(ls<>null && ls<>'')
                whercond += ' level__c like :ls and ';    
        query +=whercond;
        query += ' Employee_Status__c=\''+val+'\' order by Business__c, Market_Country__c , Level__c limit :list_size offset :counter';
        system.debug('query...'+query+'..'+etype+'...'+emar+'...'+ebus);
        
        for(Employee_Bill_Rate__c ebrc: database.query(query)){
            Boolean isSelected = true;
            if(!selectedEmpsinPage.contains(ebrc.id))
                isSelected = false;
            else
                isSelected = true;
            empWrpList.add(new empWrapper(isSelected,ebrc.id,ebrc.name,ebrc.type__c, ebrc.Business__c,ebrc.Market_Country__c,ebrc.id,ebrc.Bill_Rate_Local_Currency__c,ebrc.currencyisocode,ebrc.level__c,ebrc.Sub_Business__c));
        }
        
        Map<String,Decimal> exchMap = new Map <String,Decimal>();
        
        List<String> ratelist = new List<String>();
         
        for(EmpWrapper ebrc:empWrpList)
            ratelist.add(ebrc.isocode);
              
        for(Current_Exchange_Rate__c er:[select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :ratelist])
            exchMap.put(er.name,er.Conversion_Rate__c);                
        
        for(EmpWrapper em: empWrpList){
            if(em.billrate <> null && exchMap.containsKey(em.isocode) && oppcurrcode<>null)
                em.convertedrate = ((em.billrate * oppcurrcode) / exchMap.get(em.isocode)).round();
            else
                em.convertedrate = 0;
                
            empbillratemap.put(em.recid, em.convertedrate);
        }
        
        
              
        return empWrpList;
    }
    
    
  /*
   *    Getter method for Selected employees
   */
    public void getSelectedEmployees(){
        //selempWrpList.clear();
        selset.clear();
        set<id> recidSet = new set<id>();
        if(selempWrpList.size()>0)
            for(Selempwrapper s:selempWrpList)
                recidSet.add(s.recid);
                
        for(EmpWrapper ew: empWrpList){
            if(ew.isSelected == true && !recidSet.contains(ew.recid))
                selset.add(ew.recid);
        }
        
        
        for(Id sid: selectedEmpsinPage){
            if(!recidSet.contains(sid))
                selset.add(sid);
        }
        if(selset.size()>0){
           selEmpList =[select currencyisocode, id,name,Level__c,Type__c,Business__c,Market_Country__c, Bill_Cost_Ratio__c,  Bill_Rate_Local_Currency__c from Employee_Bill_Rate__c where id in:selset];
           for(Employee_Bill_Rate__c ebrc:selEmplist)
                selempWrpList.add(new Selempwrapper(ebrc.id,ebrc.name,ebrc.Level__c,ebrc.business__c, ebrc.market_country__c,empbillratemap.get(ebrc.id)));
           // return selempWrpList;
        }
        //else
            //return null;
    }
     /*
   * @Description : Method for updating selected employees
   * @ Args       : null
   * @ Return     : void
   */
    public void updateselectedemployees(){
        for(EmpWrapper ew: empWrpList){
            if(ew.isSelected == true)
                selectedEmpsinPage.add(ew.recid);
        }
        
    }
    
    /*
    *   Wrapper class for employees
    */
    public class empWrapper{
        public boolean isSelected {get;set;}
        public String isoCode {get;set;}
        public String recid{get;set;}
        public String name {get;set;}
        public String type {get;set;}
        public String business {get;set;}
        public String market {get;set;}  
        public Decimal billrate {get;set;} 
        public Decimal convertedrate {get;set;} 
        public String level {get;set;}
        public String subbusiness{set;get;}//Added as part of Request 4258(Sep 2014 Release)
        /*
        *    Constructor for empWrapper wrapper class
        */
        public empWrapper(Boolean sel,String id,String n, String t, String b, String m, String rid, Decimal billRat,string iso, string level,String Sub){
            this.isSelected = sel;
            this.name = n;
            this.recid = id;
            this.type = t;
            this.business = b;
            this.market = m;
            this.recid = rid;
            this.billrate = billRat;
            this.isoCode = iso;
            this.level=level;
            this.subbusiness = sub;//Added as part of Request 4258(Sep 2014 Release)
        }
    }
   /*
    *   Wrapper class for selected employees
    */
     public class Selempwrapper{
        public boolean isSelected {get;set;}
        public String recid{get;set;}
        public String business{get;set;}
        public String market{get;set;}
        public String name{get;set;}
        public String level{get;set;}
        public Decimal hours {get;set;}   
        public Decimal billrate {get;set;}
       /*
        *    Constructor for Selempwrapper wrapper class
        */   
        public Selempwrapper(String rid,String n,String l,String busi, String mar, Decimal rt){
            this.recid = rid;
            this.name = n;
            this.level = l;
            this.business = busi;
            this.billrate = rt;
            this.market = mar;
            hours = 0;
        }
    }
    
    /*
     *  Method for navigating to beginning of page
     */
    public PageReference Beginning() {
      counter = 0;
      
      return null;
   }
   /*
   *    Method for navigating to previous page
   */
   public PageReference Previous() {
     updateselectedemployees();   
      counter -= list_size;
   
      return null;
   }
  /*
   *    Method for navigating to next page
   */
   public PageReference Next() { 
      updateselectedemployees();
      counter += list_size;
       
      return null;
   }
  /*
   *    Method for navigating to the last page
   */
   public PageReference End() { 
      counter = total_size - math.mod(total_size, list_size);
      return null;
   }
  /*
   *    Getter method for getting the value of DisablePrevious variable 
   */
   public Boolean getDisablePrevious() {
      
      if (counter>0) return false; else return true;
   }
  /*
   *    Getter method for getting the value of DisableNext variable 
   */
   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) return false; else return true;
   }
  /*
   *    Getter method for getting the total size
   */
   public Integer getTotal_size() {
      return total_size;
   }
  /*
   *    Getter method for getting the page number
   */
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
  /*
   *    Getter method for getting total pages
   */
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }

}