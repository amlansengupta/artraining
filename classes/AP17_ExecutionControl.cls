/*Purpose:  This Apex class contains static variable to check for the first execution of a trigger and prevent recursive execution==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   01/21/2013  Created Apex to support TRG08
============================================================================================================================================== 
*/

public class AP17_ExecutionControl { 
   public static boolean firstRun = true; 
   
   private static List<Group> groupList = new List<Group>();

 /*
     * @Description : This method is called from various codes to bypass the Informatica Admin
     * @ Args       : None
     * @ Return     : boolean
     */
   
   public static boolean isDataMigrationGroupMember()
   {
        boolean isDmUser = false;
        /*Group dmGroup = [Select Name, Id, (Select Id, UserOrGroupId From GroupMembers) FROM Group Where Name ='Data Migration Group' LIMIT 1];
        for(GroupMember member : dmGroup.GroupMembers)
        {
            if(member.UserOrGroupId == UserInfo.getUserId())
            {
                isDmUser = true;
                break;  
            }  
            
        }*/
        return isDmUser;
   }
   
  /*
     * @Description : This method is called from various codes to bypass the Informatica Admin
     * @ Args       : User ID
     * @ Return     : boolean
     */

   public static boolean isDataMigrationGroupMember(String userId)
   {
        boolean isDmUser = false;
       /* if(groupList.isEmpty())
        {
            groupList = [Select Name, Id, (Select Id, UserOrGroupId From GroupMembers) FROM Group Where Name ='Data Migration Group' LIMIT 1];              
        }
        
        if(groupList.size() > 0)
        {
            for(GroupMember member : groupList[0].GroupMembers)
            {
                if(member.UserOrGroupId == userId) 
                {
                    isDmUser = true;
                    break;  
                } 
                
            }   
        }*/
               
        return isDmUser;
   }
   
   public Static set<Id> DataMigrationGroupMembers(){
       Set<Id> setDMGrpMem = new Set<Id>();
       
       if(groupList.isEmpty())
           groupList = [Select Name, Id, (Select Id, UserOrGroupId From GroupMembers) FROM Group Where Name ='Data Migration Group' LIMIT 1];              
        
        
       if(groupList.size() > 0)
           for(GroupMember member : groupList[0].GroupMembers)
               setDMGrpMem.add(member.userorgroupid);
   
       return setDMGrpMem;
   }
}