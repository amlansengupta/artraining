global class BATCH_UpdateAccountsRM implements Database.Batchable<sObject>{
    global String query;
    List<Account> listAcc=new List<Account>();
    Set<Colleague__c> clUpdate = new Set<Colleague__c>();
    List<Colleague__c> clUpdate1 = new List<Colleague__c>();
    Colleague__c clgRec = new Colleague__c();

    global BATCH_UpdateAccountsRM(String q){
      query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        for(Account acc: scope)
        {
                        acc.ByPassVR__c = true;
                        acc.Account_Market__c = acc.Relationship_Manager__r.Market__c;
                        acc.Account_Region__c = acc.Relationship_Manager__r.Region__c;
                        acc.Account_Sub_Market__c = acc.Relationship_Manager__r.Sub_Market__c;
                        acc.Account_Sub_Region__c = acc.Relationship_Manager__r.Sub_Region__c;
                        acc.Office_Code__c = acc.Relationship_Manager__r.Office_Record_Name__c;
                        acc.Account_Country__c = acc.Relationship_Manager__r.CountryGeo__c;
                        acc.Account_Office__c = acc.Relationship_Manager__r.Office__c;
                        clgRec.Location_Changed__c = false;
                        clgRec.Id = acc.Relationship_Manager__c;
                        clUpdate.add(clgRec);
                        
            listAcc.add(acc);
        }
        for(Colleague__c clg:clUpdate){
        if(clUpdate1.isempty()){
        clUpdate1.add(clg);}
        else 
        if(!clUpdate1.isempty()){
        for(Colleague__c clg1:clUpdate1){
        if(clg1.id != clg.id){
        clUpdate1.add(clg);
        }
        }
        
        }
        }
        update listAcc;
        update clUpdate1;
        
    }

    global void finish(Database.BatchableContext BC)
    {

    }
}