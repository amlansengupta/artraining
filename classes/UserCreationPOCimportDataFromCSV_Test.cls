@isTest(SeeAllData=true)
public class UserCreationPOCimportDataFromCSV_Test{
    static String str = 'Fname,Lname,Wmail,Enumber,Is_Active,Request__c,Role,Profile,LOCALESIDKEY,LANGUAGELOCALEKEY,Time Zone(Vlookup),Currency,Requestor,Sales Goals,Marketing User\nBiswajit,nath,biswajit.nath@mercer.com,1112015,TRUE,User_update test 123,Asia - RMD,Mercer PMO Access,English (India),English Africa/Cairo ,USD,1155644,Yes,1\nBiswajit,nath,biswajit.nath@mercer.com,1089654,TRUE,User_update test 123,Asia - RMD,Mercer PMO Access,English (India),English Africa/Cairo ,USD,1155644,Yes,1';       

    public static String[] csvFileLines;
    public static Blob csvFileBody;
    
    static testmethod void testfileupload(){
        Test.startTest();       
        csvFileBody = Blob.valueOf(str);
        String csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n'); 

        UserCreationPOCimportDataFromCSV importData = new UserCreationPOCimportDataFromCSV();
        importData.csvFileBody = csvFileBody;
        importData.importCSVFile();
        importData.updateRecords();
        Test.stopTest();
    } 

    static testmethod void testfileuploadNegative(){
        Test.startTest();       
        csvFileBody = Blob.valueOf(str);
        String csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n'); 

        UserCreationPOCimportDataFromCSV importData = new UserCreationPOCimportDataFromCSV();
        importData.importCSVFile();
        Test.stopTest();
    }
}