public class OppLineItemTriggerUtil_ProjectEx{
    static List<OpportunityLineItem> newOpportunityLineItemList = new List<OpportunityLineItem>();
    //static List<Project_Exception__c> exceptionList = new List<Project_Exception__c>();
    static Map<String,List<Project_Exception__c>> productCodeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> lobMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> officeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> oneCodeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> oppTypeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> recordTypeNameMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> productCode_OfficeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> TypeCountryLoBCommissionMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> CountryProdCodeMap = new Map<String,List<Project_Exception__c>>();
    static Map<String,List<Project_Exception__c>> TypeCountryProdCodeMap = new Map<String,List<Project_Exception__c>>(); 
    static Map<String,List<Project_Exception__c>> CountryLobCommissionMap = new Map<String,List<Project_Exception__c>>(); 
    static Map<String,List<Project_Exception__c>> CountryLobMap= new Map<String,List<Project_Exception__c>>(); 
    
    static List<Project_Exception__c> peList;
    static void createExceptionMap(){
        for(Project_Exception__c pe : [Select Id,Product_Code__c,Exception_Type__c,LOB__c,Office__c,One_Code__c,
                                       Opportunity_Type__c,Record_Type_Name__c,Opportunity_Country__c,Commision__c From Project_Exception__c LIMIT 50000])
        {                      
            if(pe.Exception_Type__c == 'Product Code'){
                peList = new List<Project_Exception__c>();
                if(productCodeMap.containsKey(pe.Product_Code__c)){
                    peList = productCodeMap.get(pe.Product_Code__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                productCodeMap.put(pe.Product_Code__c, peList);
            }
            if(pe.Exception_Type__c == 'Product LOB'){
                peList = new List<Project_Exception__c>();
                if(lobMap.containsKey(pe.LOB__c)){
                    peList = lobMap.get(pe.LOB__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                lobMap.put(pe.LOB__c, peList);
            }
            if(pe.Exception_Type__c == 'Office'){
                peList = new List<Project_Exception__c>();
                if(officeMap.containsKey(pe.Office__c)){
                    peList = officeMap.get(pe.Office__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                officeMap.put(pe.Office__c, peList);
            }
            
            if(pe.Exception_Type__c == 'One Code'){
                peList = new List<Project_Exception__c>();
                if(oneCodeMap.containsKey(pe.One_Code__c)){
                    peList = oneCodeMap.get(pe.One_Code__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                oneCodeMap.put(pe.One_Code__c, peList);
            }
            
            if(pe.Exception_Type__c == 'Opportunity Type'){
                peList = new List<Project_Exception__c>();
                if(oppTypeMap.containsKey(pe.Opportunity_Type__c)){
                    peList = oppTypeMap.get(pe.Opportunity_Type__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                oppTypeMap.put(pe.Opportunity_Type__c, peList);
                System.debug('oppTypeMap***'+oppTypeMap);
            }
            
            if(pe.Exception_Type__c == 'RecordType Name'){
                peList = new List<Project_Exception__c>();
                if(recordTypeNameMap.containsKey(pe.Record_Type_Name__c)){
                    peList = recordTypeNameMap.get(pe.Record_Type_Name__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                recordTypeNameMap.put(pe.Record_Type_Name__c, peList);
            }
            
            if(pe.Exception_Type__c == 'Product + Office Code'){
                peList = new List<Project_Exception__c>();
                if(productCode_OfficeMap.containsKey(pe.Product_Code__c + pe.Office__c )){
                    peList = productCode_OfficeMap.get(pe.Product_Code__c + pe.Office__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                productCode_OfficeMap.put(pe.Product_Code__c + pe.Office__c, peList);
            }
            if(pe.Exception_Type__c == 'Opportunity Type + Opportunity Country + Product LoB + Commission Flag'){
                peList = new List<Project_Exception__c>();
                
                if(TypeCountryLoBCommissionMap.containsKey(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c )){
                    peList = TypeCountryLoBCommissionMap.get(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                TypeCountryLoBCommissionMap.put(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c, peList);
            }
            
            if(pe.Exception_Type__c == 'Opportunity Type + Opp Country + Product Code'){
                peList = new List<Project_Exception__c>();
                if(TypeCountryProdCodeMap.containsKey(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.Product_code__c)){
                    peList = TypeCountryProdCodeMap.get(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.Product_code__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                TypeCountryProdCodeMap.put(pe.Opportunity_Type__c + pe.Opportunity_Country__c + pe.Product_code__c, peList);
            } 
            
            if(pe.Exception_Type__c == 'Opportunity Country + LoB + Commission Flag'){
                peList = new List<Project_Exception__c>();
                if(CountryLobCommissionMap.containsKey(pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c)){
                    peList = CountryLobCommissionMap.get(pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                CountryLobCommissionMap.put(pe.Opportunity_Country__c + pe.LOB__c + pe.Commision__c, peList);
                system.debug('CountryLobCommissionMap****'+CountryLobCommissionMap);
            }  
            if(pe.Exception_Type__c == 'Opportunity Country + Product code'){
                peList = new List<Project_Exception__c>();
                if(CountryProdCodeMap.containsKey(pe.Opportunity_Country__c + pe.Product_code__c  )){
                    peList = CountryProdCodeMap.get(pe.Opportunity_Country__c + pe.Product_code__c  );
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                CountryProdCodeMap.put(pe.Opportunity_Country__c + pe.Product_code__c, peList);
            }  
            if(pe.Exception_Type__c == 'Opportunity Country + LoB'){
                peList = new List<Project_Exception__c>();
                if(CountryLobMap.containsKey(pe.Opportunity_Country__c + pe.LOB__c)){
                    peList = CountryLobMap.get(pe.Opportunity_Country__c + pe.LOB__c);
                    peList.add(pe);
                }else{
                    peList.add(pe);
                }
                CountryLobMap.put(pe.Opportunity_Country__c + pe.LOB__c , peList);
            }   
            
                    
        }
            system.debug('TypeCountryLoBCommissionMap*****'+TypeCountryLoBCommissionMap);
            system.debug('TypeCountryProdCodeMap***'+TypeCountryProdCodeMap);
            system.debug('CountryLobCommissionMap******'+CountryLobCommissionMap);
            system.debug('CountryProdCodeMap***'+CountryProdCodeMap);
            system.debug('CountryLobMap****'+CountryLobMap);
            
    }
    public static void updateProjectRequired(List<OpportunityLineItem> triggerNew){
        
        system.debug('@@@@@ inside updateProjectRequired');
        createExceptionMap();
        List<Id> OppId=new List<Id>();
        List<Id> OliId=new List<Id>();
        for(OpportunityLineItem ol : triggerNew){
        
            OppId.add(ol.OpportunityId);
            OliId.add(ol.id);
        }
        //List<OpportunityLineitem> OliList=[Select ID,OpportunityId,Opportunity.Opportunity_Office__c,Opportunity.Opportunity_Country__c,Opportunity.OneCode__c,Opportunity.Type,Product_Code_del__c,Is_Project_Required__c,LOB__c,Commission__c from OpportunityLineItem where id In:OliId];
         Map<id,Opportunity> oppMap = new Map<id,Opportunity>([Select Id,Opportunity_Office__c,OneCode__c,Account.One_Code__c,Opportunity_Country__c,Type,RecordType.Name
                                   From Opportunity Where Id IN :OppId ]);
        //List<Opportunity> OppList=[Select Id,Opportunity_Office__c,OneCode__c,Account.One_Code__c,Opportunity_Country__c,Type,RecordType.Name from Opportunity Where Id IN :OppId ];
        
        for(OpportunityLineItem ol : triggerNew){
        system.debug('QQQQQol.OpportunityId'+ol.OpportunityId);
             system.debug('****oppMap.get(ol.OpportunityId).Type '+ol.Opportunityid);
               // system.debug('oppTypeMap.containsKey(oppMap.get(ol.OpportunityId).Type)*****'+oppTypeMap.containsKey(oppMap.get(ol.OpportunityId).Type));                    
            //for(Opportunity opp : oppList)
            //{
                //system.debug('QQQQQol.opp'+opp);
                if(oppMap.get(ol.OpportunityId).type!='Product Expansion'){
                Boolean productCodeFlag = false;
                Boolean lobFlag = false;
                Boolean officeFlag = false;
                Boolean oneCodeFlag = false;
                Boolean oppTypeFlag = false;
                Boolean recordTypeNameFlag = false;
                Boolean productCode_OfficeFlag = false;
                Boolean TypeCountryLoBCommissionFlag =false;
                Boolean CountryProdCodeFlag = false;
                Boolean TypeCountryProdCodeFlag = false;
                Boolean CountryLobCommissionFlag = false;
                Boolean CountryLobFlag= false;   
                if(productCodeMap.containsKey(ol.Product_Code_del__c)){
                    //ol.adderror(ol.Product_Code_del__c);
                    system.debug('QQQQQol.Product_Code_del__c'+ol.Product_Code_del__c);
                    for(Project_Exception__c peNew : productCodeMap.get(ol.Product_Code_del__c)){
                        //ol.adderror('qwe'+peNew.Product_Code__c);
                        if(peNew.Product_Code__c == ol.Product_Code_del__c ){
                            productCodeFlag=True;
                            //ol.adderror(String.valueOf( ol.Is_Project_Required__c));
                        }
                                              
                    }
                    System.debug('Exception**1***'+ol.id);
                    
                }                                
               System.debug('oppMap.get(ol.OpportunityId).Opportunity_Office__c***'+oppMap.get(ol.OpportunityId).Opportunity_Office__c); 
               System.debug('oppMap.get(ol.OpportunityId)***'+oppMap.get(ol.OpportunityId)); 
               if(oppMap.get(ol.OpportunityId).Opportunity_Office__c!=null && officeMap.containsKey(oppMap.get(ol.OpportunityId).Opportunity_Office__c)){
                    for(Project_Exception__c peNew : officeMap.get(oppMap.get(ol.OpportunityId).Opportunity_Office__c)){
                        if(peNew.Office__c == oppMap.get(ol.OpportunityId).Opportunity_Office__c  ){
                            officeFlag=True;
                        }
                        
                    }
                    System.debug('Exception**2***'+ol.id);
                    
                }

                if(oneCodeMap.containsKey(oppMap.get(ol.OpportunityId).OneCode__c)){
                
                    for(Project_Exception__c peNew : oneCodeMap.get(oppMap.get(ol.OpportunityId).OneCode__c)){
                        if(peNew.One_Code__c == oppMap.get(ol.OpportunityId).OneCode__c){
                            oneCodeFlag=True;
                        }                        
                    }
                    System.debug('Exception**3***'+ol.id);                    
                }

                if(oppTypeMap.containsKey(oppMap.get(ol.OpportunityId).Type)){
                        System.debug('opp.Type**'+oppMap.get(ol.OpportunityId));
                        System.debug('ol.LOB__c**'+ol.LOB__c);
                        System.debug('oppTypeMap.get(opp.Type)**'+oppTypeMap.get(oppMap.get(ol.OpportunityId).Type));
                        if(!lobMap.containsKey(ol.LOB__c)){
                            
                            lobFlag=True;
                            System.debug('Should come here**');
                        
                        }
                        System.debug('Exception**4***'+ol.id);    
                               
                    }

                
                if(recordTypeNameMap.containsKey(oppMap.get(ol.OpportunityId).RecordType.Name)){
                    for(Project_Exception__c peNew : recordTypeNameMap.get(oppMap.get(ol.OpportunityId).RecordType.Name)){
                        if(peNew.Record_Type_Name__c == oppMap.get(ol.OpportunityId).RecordType.Name){
                           recordTypeNameFlag = True;
                        }                        
                    }
                    System.debug('Exception**5***'+ol.id);                    
                }

                if(productCode_OfficeMap.containsKey(ol.Product_Code_del__c + oppMap.get(ol.OpportunityId).Opportunity_Office__c)){
                
                    for(Project_Exception__c peNew : productCode_OfficeMap.get(ol.Product_Code_del__c + oppMap.get(ol.OpportunityId).Opportunity_Office__c)){
                        if(peNew.Office__c == oppMap.get(ol.OpportunityId).Opportunity_Office__c && penew.Product_Code__c == ol.Product_Code_del__c){
                            productCode_OfficeFlag=True;                            
                        }
                        
                    }
                    System.debug('Exception**6***'+ol.id);
                    
                }
                 if(TypeCountryLoBCommissionMap.containsKey(oppMap.get(ol.OpportunityId).Type+oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c+ol.Commission__c)){
                    
                    for(Project_Exception__c peNew : TypeCountryLoBCommissionMap.get(oppMap.get(ol.OpportunityId).Type+oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c+ol.Commission__c)){
                        if(peNew.Opportunity_Type__c == oppMap.get(ol.OpportunityId).Type && penew.Opportunity_Country__c == oppMap.get(ol.OpportunityId).Opportunity_Country__c && penew.LOB__c ==ol.LOB__c && penew.Commision__c==ol.Commission__c){
                            TypeCountryLoBCommissionFlag=True;                           
                        }
                        
                    }
                    System.debug('Exception**7***'+ol.id);
                    
                }   
                if(TypeCountryProdCodeMap.containsKey(oppMap.get(ol.OpportunityId).Type+oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.Product_Code_del__c)){
                    
                    for(Project_Exception__c peNew : TypeCountryProdCodeMap.get(oppMap.get(ol.OpportunityId).Type+oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.Product_Code_del__c)){
                        if(peNew.Opportunity_Type__c == oppMap.get(ol.OpportunityId).Type && penew.Opportunity_Country__c == oppMap.get(ol.OpportunityId).Opportunity_Country__c && peNew.Product_Code__c == ol.Product_Code_del__c){
                            TypeCountryProdCodeFlag= True;                            
                        }
                        
                    }
                    System.debug('Exception**8***'+ol.id);
                    
                }  
                if(CountryLobCommissionMap.containsKey( oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c+ol.Commission__c)){
                    
                    for(Project_Exception__c peNew : CountryLobCommissionMap.get( oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c+ol.Commission__c)){
                        if(penew.Opportunity_Country__c == oppMap.get(ol.OpportunityId).Opportunity_Country__c && penew.LOB__c ==ol.LOB__c && penew.Commision__c==ol.Commission__c){
                            CountryLobCommissionFlag= True;                            
                        }
                        
                    }
                    System.debug('Exception**9***'+ol.id);
                    
                } 
                if(CountryProdCodeMap.containsKey(oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.Product_Code_del__c)){
                    
                    for(Project_Exception__c peNew : CountryProdCodeMap.get( oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.Product_Code_del__c)){
                        if(penew.Opportunity_Country__c == oppMap.get(ol.OpportunityId).Opportunity_Country__c && peNew.Product_Code__c == ol.Product_Code_del__c){
                            CountryProdCodeFlag = True;                            
                        }
                        
                    }
                    System.debug('Exception**10***'+ol.id);
                
            } 
            if(CountryLobMap.containsKey(oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c)){
                    
                for(Project_Exception__c peNew : CountryLobMap.get(oppMap.get(ol.OpportunityId).Opportunity_Country__c+ol.LOB__c)){
                    if(penew.Opportunity_Country__c == oppMap.get(ol.OpportunityId).Opportunity_Country__c && penew.LOB__c ==ol.LOB__c){
                        CountryLobFlag = True;                            
                    }                    
                }
                System.debug('Exception**11***'+ol.id);
                
            }
            if(productCodeFlag || lobFlag || officeFlag || oneCodeFlag || oppTypeFlag || recordTypeNameFlag || productCode_OfficeFlag || TypeCountryLoBCommissionFlag || CountryProdCodeFlag || TypeCountryProdCodeFlag || CountryLobCommissionFlag || CountryLobFlag){
                ol.Is_Project_Required__c = False; 
            }else{
                system.debug('Not in Map'+ol.id);
                ol.Is_Project_Required__c = true;                    
                break;                    
            }                
                         
                
                system.debug('Debug 6');
            //}
            newOpportunityLineItemList.add(ol);
        }
        
        system.debug('newOpportunityLineItemList'+newOpportunityLineItemList);
       
       } 
        
    }
}