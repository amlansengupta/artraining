/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    1.0 -    Arijit Roy 03/29/2013  Created Batch class for Campaign
=============================================================================================================================================== 
*/

global class AP24_CampaignBatchable implements Database.Batchable<sObject> 
{
    global String query;
    global List<Campaign> campaignList = new List<Campaign>();
    
    global AP24_CampaignBatchable (String query)
    {
        this.query = query;
    }
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query); 
    }
     /*
     *  Method Name: execute
     *  Description: Method is used to check and Update  Campaign Sttaus.
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */    
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        List<Campaign> campaignListToUpdate = new List<Campaign>();
        campaignList = (List<Campaign>)scope;
        
        for(Campaign campaign : campaignList)
        {
            if(campaign.Status <> 'Cancelled')
            {
                campaign.Status = 'Completed';
                campaignListToUpdate.add(campaign);
            }
        }
        
        update campaignListToUpdate;
    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc){}
}