public class MF2_SP_EL_SowAPex {
    @AuraEnabled
    public static Sales_Professional__c getRecord(Id recId){
      Sales_Professional__c sp =[select EL_On_File__c,SOW_and_EL_Uploaded__c,Id, RecordType.name from Sales_professional__c where id=:recId];  
    return sp;
    }

}