/*
Purpose: 
This class contains static methods which are used by trgger TRG14_ContactDistributionHistoryTrigger.
This class contains methods to make an entry into Contact Distribution history object whenever a contact Distribution is added or deleted for an Opportunity
==============================================================================================================================================
History 
----------------------- VERSION       AUTHOR       DATE        DETAIL    
                            1.0 -    Shashank   04/08/2013  Created this util class   
                            1.1 -    Savina     12/12/2013  'Jan Release#3633 ,added method processContactDistributionBeforeUpdate'
============================================================================================================================================== 
*/

public class AP35_ContactDistributionHistoryUtil 
{
    
    private static set<string> conDistributionIdset = new set<string>();
    private static List<Contact_Distribution_History__c> conDisHistoryList = new List<Contact_Distribution_History__c>();
    
    public static void processContactDistributionAfterInsert(Map<Id, Contact_Distributions__c> triggernewMap)
    {
        try
        {
            conDisHistoryList.clear();
            conDistributionIdset.clear();
            for(Contact_Distributions__c conDis:triggernewMap.values())
            {
                 conDistributionIdset.add(conDis.Id);
            }
             
            List<Contact_Distributions__c> lstCont = [select Id, Distribution__r.Name from Contact_Distributions__c where Id IN :conDistributionIdset ] ;
           
            for(Contact_Distributions__c conDis:[select Id, Distribution__r.Name from Contact_Distributions__c where Id IN :conDistributionIdset])
            {
                Contact_Distribution_History__c conDisHistory = new Contact_Distribution_History__c();
                conDisHistory.ContactId__c = triggernewMap.get(conDis.Id).Contact__c;
                conDisHistory.User__c = UserInfo.getUserId();
                conDisHistory.New_Value__c = conDis.Distribution__r.Name;
                conDisHistory.Date__c = system.now();
                conDisHistory.Action__c = 'Added Distribution: '+conDisHistory.New_Value__c;
                conDisHistoryList.add(conDisHistory);
            }
            System.debug('/n/n processContactDistributionAfterInsert : '+conDisHistoryList);
            insert conDisHistoryList;
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());
            triggernewMap.values()[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
            
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());
            triggernewMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        }
    }
    
    public static void processContactDistributionBeforeDelete(Map<Id, Contact_Distributions__c> triggeroldMap)
    {
        try
        {
            conDisHistoryList.clear();
            conDistributionIdset.clear();
            for(Contact_Distributions__c conDis:triggeroldMap.values())
            {
                 conDistributionIdset.add(conDis.Id);
            }
            
            for(Contact_Distributions__c conDis:[select Id, Distribution__r.Name from Contact_Distributions__c where Id IN :conDistributionIdset])
            {
                Contact_Distribution_History__c conDisHistory = new Contact_Distribution_History__c();
                conDisHistory.ContactId__c = triggeroldMap.get(conDis.Id).Contact__c;
                conDisHistory.User__c = UserInfo.getUserId();
                conDisHistory.Old_Value__c = conDis.Distribution__r.Name;
                conDisHistory.Date__c = system.now();
                conDisHistory.Action__c = 'Removed Distribution: '+conDisHistory.Old_Value__c;
                conDisHistoryList.add(conDisHistory);
            }
            
            insert conDisHistoryList;
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());
            triggeroldMap.values()[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
            
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());
            triggeroldMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        }
    }
     
         
    /*
     * @Description : As a part of Request# 3633(January Release) this method prevents duplicate distribution in contact-distribution 'Edit' page 
     * @ Args       : contactDistribution,contactdistributionold
     * @ Return     : void
     */               
    public static void processContactDistributionBeforeUpdate( List<Contact_Distributions__c> contactDistribution, Map<id,Contact_Distributions__c> contactdistributionold) 
    {     
        Map<Id,Contact_Distributions__c> contDistMap = new  Map<Id,Contact_Distributions__c>();  
        Map<id,Set<ID>> conWithDistmap = new Map<id,Set<ID>>();
                
        for ( Contact_Distributions__c condis: contactDistribution)
        {
           if (contactdistributionold.get(condis.id).Distribution__c <> condis.Distribution__c)
           {
               contDistMap.put(condis.Contact__c,condis);               
           }
        }        
        
        Map<id,contact> conmap = new Map<id,contact> ([Select id, name , (Select Distribution__c From Contact_Distributions__r) from contact where id IN : contDistMap.keySet()]);
           
        for(ID cid : conmap.keyset())
        {
            Contact cn = conmap.get(cid);
            Set<ID> distID = new Set<ID>();
            for(Contact_Distributions__c cdt : cn.Contact_Distributions__r)
            {
                distID.add(cdt.Distribution__c);
            }
            conWithDistmap.put(cid,distID);
        }
         
        for(Contact_Distributions__c cd : contDistMap.values())
        {
            
            if(conWithDistmap.containskey(cd.Contact__c))
            {             
              Set<ID> cddID = new Set<ID>();
              cddID =(conWithDistmap.get(cd.Contact__c));
              if(cddID.contains(cd.Distribution__c))
              cd.addError(system.Label.CL96_Contact_distribution_error);
            } 
        }                                       
    }   
    
    
    /*
     * @Description : As a part of Request# 3633(January Release) this method prevents duplicate distribution in contact-distribution 'Edit' page before insertion
     * @ Args       : contactDistribution
     * @ Return     : void
     */               
    public static void processContactDistributionBeforeInsert( List<Contact_Distributions__c> contactDistribution) 
    {   
        Map<Id,Contact_Distributions__c> contDistMap = new  Map<Id,Contact_Distributions__c>();           
        Map<id,Set<ID>> conWithDistmap = new Map<id,Set<ID>>();
        Set<ID> conIDs = new Set<ID>();
        
        for ( Contact_Distributions__c condis: contactDistribution)
        {                      
             contDistMap.put(condis.Contact__c,condis);           
        } 
        
               
        Map<id,contact> conmap = new Map<id,contact> ([Select id, name , (Select Distribution__c From Contact_Distributions__r) from contact where id IN : contDistMap.keySet()]);
           
        for(ID cid : conmap.keyset())
        {
            Contact cn = conmap.get(cid);
            Set<ID> distID = new Set<ID>();
            for(Contact_Distributions__c cdt : cn.Contact_Distributions__r)
            {
                distID.add(cdt.Distribution__c);
            }
            conWithDistmap.put(cid,distID);
        }
         
        for(Contact_Distributions__c cd : contDistMap.values())
        {
            
            if(conWithDistmap.containskey(cd.Contact__c))
            {             
              Set<ID> dID = new Set<ID>();
              dID =(conWithDistmap.get(cd.Contact__c));
              if(dID.contains(cd.Distribution__c))
              cd.addError(system.Label.CL96_Contact_distribution_error);
            } 
        }                                       
    }   
 }