/*
Purpose: This Apex class implements batchable interface========================================================================================================================================History
 ----------------------- VERSION     AUTHOR    DATE        DETAIL    
                         1.0 -    Shashank     03/29/2013  Created Batch Class for Opportunity
                         2.0 -    Sarbpreet    7/6/2014    Updated Comments/documentation.
=============================================================================================================================================== */
global class AP26_LockOpportunityBatchable implements Database.Batchable<sObject> 
{
    global String query;
    global List<Opportunity> opportunityList = new List<Opportunity>();
    /*
    *   Class Constructor for inistializing the value of 'query' variable
    */
    global AP26_LockOpportunityBatchable (String query)
    {
        this.query = query;
    }
     /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query); 
    }
      /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        List<Opportunity> opportunityListToUpdate = new List<Opportunity>();
        opportunityList = (List<Opportunity>)scope;
        RecordType recType = [select Id, Name from RecordType where Name = 'Opportunity Locked' Limit 1];
        RecordType recTypeExpan = [select Id, Name from RecordType where Name = 'Opportunity Product Expansion Locked' Limit 1];
        for(Opportunity opp : opportunityList)
        {
            if(opp.Parent_Opportunity_Name__c==null){
                opp.zz_PervRecordTypeID__c = opp.RecordTypeId;
                opp.RecordTypeId = recType.Id;                
            }else if(opp.Parent_Opportunity_Name__c!=null){
				opp.zz_PervRecordTypeID__c = opp.RecordTypeId;
                opp.RecordTypeId = recTypeExpan.Id;                
            }
            
            opportunityListToUpdate.add(opp);
        }
        
        update opportunityListToUpdate;
    }
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc){}
}