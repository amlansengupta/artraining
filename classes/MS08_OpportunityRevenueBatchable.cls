/*
==============================================================================================================================================
History ----------------------- 
                                               VERSION     AUTHOR         DATE                 DETAIL    
                                                      1.0         Arijit Roy    04/01/2013         Batch class for updating Account Status    
============================================================================================================================================== 
*/
global class MS08_OpportunityRevenueBatchable implements Database.Batchable<sObject>, Database.Stateful 
{
    
    global static Map<String,String> filteredMap = new Map<String,String>
    {
    'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
    'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
    'Recent Win' => 'Recent Win' ,
    'High Revenue' => 'High Revenue'
    };

    global static  List<Schema.PicklistEntry> StatusPickVal = Account.MercerForce_Account_Status__c.getDescribe().getPicklistValues();
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global static String Query =  'Select Id, Total_Opportunity_Revenue_USD__c, AccountId, Account.MercerForce_Account_Status__c From Opportunity Where Account.MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keySet()) + 'AND Total_Opportunity_Revenue_USD__c < 100000.00 ORDER By AccountId';
   
   global static Map<String,String> AccountStatusMap = New Map <String,String>();
    
   
    
    
  /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    if(Test.isRunningTest())
        {
            String oppname = 'Test Opportunity3' + String.valueOf(Date.Today());  
            
            query = 'Select Id, Total_Opportunity_Revenue_USD__c, AccountId, Account.MercerForce_Account_Status__c From Opportunity Where Name IN ' + MercerAccountStatusBatchHelper.quoteKeySet(new Set<String>{oppname}); 
        }
        System.debug('\n Constructed query String :'+query);
        
      return Database.getQueryLocator(query);  
    }
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<String, Account> accountMapForupdate = new Map<String, Account>();
        for(opportunity opportunity : (List<Opportunity>) scope)
        {
            Account account = new Account(Id = opportunity.AccountId);
            account.MercerForce_Account_Status__c = 'Low Revenue';
            accountMapForupdate.put(account.Id, account);
            
        }
        if(!accountMapForupdate.isEmpty())
        { 
            for(Database.Saveresult result : Database.update(accountMapForupdate.values(), false))
            {
                 // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }   
            }
           
        }
    }
    
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {    try
        {
        // TO DO: Error logging code logic to be added
          Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
          system.debug('\n exception has occured');
        }finally
        {
          //MercerAccountStatusBatchHelper.callNextBatch('Active Marketing Contact'); 
        }
           
          
          
    }
    
}