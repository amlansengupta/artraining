/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

global with sharing class SVNSUMMITS_EventListWrapper {

	@AuraEnabled
	global List<Event__c> objEventList {
		get;
		set;
	}

	@AuraEnabled
	global Boolean isEditable {
		get;
		set;
	}

	@AuraEnabled
	global Map <String, String> topicNameToId {
		get;
		set;
	}

	@AuraEnabled
	global List<ConnectApi.Topic> topicLst {
		get;
		set;
	}

	@AuraEnabled
	global Integer intDate {
		get;
		set;
	}

	@AuraEnabled
	global String strMonth {
		get;
		set;
	}

	@AuraEnabled
	global String strDay {
		get;
		set;
	}

	@AuraEnabled
	global String strMinute {
		get;
		set;
	}

	@AuraEnabled
	global Integer daysOfMultiDaysEvent {
		get;
		set;
	}

	@AuraEnabled
	public List<TopicAssignment> topicsOfRecord {
		get;
		set;
	}

	@AuraEnabled
	public String strStartDate {
		get;
		set;
	}

	@AuraEnabled
	public String strEndDate {
		get;
		set;
	}

    /* PAGINATION Variables */
	@AuraEnabled
	global Integer totalResults {
		get;
		set;
	}

	@AuraEnabled
	public Integer totalPages {
		get;
		set;
	}

	@AuraEnabled
	public String dtNow {
		get;
		set;
	}

	@AuraEnabled
	global Integer pageNumber {
		get;
		set;
	}

	@AuraEnabled
	global Boolean hasPreviousSet {
		get;
		set;
	}

	@AuraEnabled
	global Boolean hasNextSet {
		get;
		set;
	}

	@AuraEnabled
	global Integer listSizeValue {
		get;
		private set;
	}

	@AuraEnabled
	global Map<Id, List<TopicAssignment>> eventsToTopicsMap {
		get;
		set;
	}

	@AuraEnabled
	global String strTimeZone {
		get;
		set;
	}

	@AuraEnabled
	global String errorMsg {
		get;
		set;
	}

	@AuraEnabled
	global String field {
		get;
		set;
	}


	global SVNSUMMITS_EventListWrapper(String field, String errorMsg) {
		this.field = field;
		this.errorMsg = errorMsg;
	}

	global static Id netwrkId = System.Network.getNetworkId();

	private ApexPages.StandardSetController setController { get; set; }

	global SVNSUMMITS_EventListWrapper(String QUERY, Integer listSize, datetime fromDt, datetime toDt, set<string> eventIds, String listViewMode, boolean isFeatured, Map<string, string> featurdEventIds) {
		//Initialise EventList
		strTimeZone = String.valueOf(UserInfo.getTimeZone());
		this.objEventList = new List<Event__c>();
		this.topicNameToId = new Map<String, String>();

		System.debug('Query:');
		System.debug(QUERY);

		this.setController = new ApexPages.StandardSetController(Database.getQueryLocator(QUERY));

		if (listViewMode == 'CALENDAR') {
			this.listSizeValue = this.setController.getResultSize();
		} else {
			this.listSizeValue = listSize;
		}

		this.setController.setPageSize(listSizeValue);
		system.debug('call to update controller attributes');
		updateControllerAttributes(isFeatured, featurdEventIds);

	}
    /*
         Method Name : nextPage
         Discription : Method for fetching values of next page
    */
	global void nextPage() {
		this.setController.setpageNumber(this.pageNumber + 1 > 0 ? this.pageNumber + 1 : 1);
		updateControllerAttributes(false, null);
	}
    /*
         Method Name : previousPage
         Discription : Method for fetching values of previous page
    */
	global void previousPage() {
		this.setController.setpageNumber(this.pageNumber - 1 > 0 ? this.pageNumber - 1 : 1);
		updateControllerAttributes(false, null);
	}
    /*
         Method Name : updateControllerAttributes
         Discription : Method for updating values of controlle attributes
    */
	private void updateControllerAttributes(boolean isFeatured, Map<string, string> featurdEventIds) {

		if (isFeatured == true) {
			List<Event__c> templist = this.setController.getRecords();

			Map<id, Event__c> tempEventsMap = new Map<id, Event__c>();

			for (Event__c event: templist) {
				tempEventsMap.put(event.id, event);
			}

			for (string str : featurdEventIds.keyset()) {
				if (tempEventsMap.containsKey(str)) {
					this.objEventList.add(tempEventsMap.get(str));
				}
			}
		}

		//for all otherobjEventList news list get records for set controller directly
		else {
			this.objEventList = this.setController.getRecords();
			system.debug('list = ' + this.objEventList.size());
		}

        /*if Record is updated with All day event  then it takes value for today's date else will take today's date with time*/
		if (this.objEventList.size() > 0) {
			if (this.objEventList[0].All_Day_Event__c) {
				this.dtNow = String.valueOf(Date.today());
			} else {
				this.dtNow = String.valueOf(Datetime.now());
			}
		}

		this.totalResults = this.setController.getResultSize();
		this.totalPages = Math.mod(this.setController.getResultSize(), this.setController.getPageSize()) == 0 ? this.setController.getResultSize() / this.setController.getPageSize() : this.setController.getResultSize() / this.setController.getPageSize() + 1;
		this.pageNumber = this.totalPages > 0 ? this.setController.getPageNumber() : 0;
		this.hasPreviousSet = this.setController.getHasPrevious();
		this.hasNextSet = this.setController.getHasNext();

		set<string> eventsObjIds = new set<string>();
		for (Sobject events : this.setController.getRecords()) {
			eventsObjIds.add(events.Id);
		}

		eventsToTopicsMap = new Map<Id, List<TopicAssignment>>();
		List<TopicAssignment> topics = new List<TopicAssignment>();

		for (TopicAssignment t: [SELECT Id, EntityId, Topic.Id, Topic.Name FROM TopicAssignment WHERE EntityId IN :eventsObjIds and NetworkId = :netwrkId]) {

			this.topicNameToId.put(t.Topic.Name, t.Topic.Id);
			topics = new List<TopicAssignment>();
			if (eventsToTopicsMap.containsKey(t.EntityId)) {
				topics.addAll(eventsToTopicsMap.get(t.EntityId));
			}
			topics.add(t);
			if (eventsObjIds.size() > 1) {
				if (topics.size() < 4) {
					eventsToTopicsMap.put(t.EntityId, topics);
				}
			} else {
				eventsToTopicsMap.put(t.EntityId, topics);
			}
		}
	}
}