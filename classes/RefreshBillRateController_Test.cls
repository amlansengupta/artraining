/*
* This test class is responsible to provide test coverage for RefreshBillRateApexController class.
* Need to made seeAllData=true because of the Pricebook2 record that we can not insert with isStandard = true as this field
* is not writeable.
*/
@isTest(seeAllData=true)
public class RefreshBillRateController_Test {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    
    //This test method creates dummy records for Scope Modeling, Scope Modeling Task and Scope Modeling Employee and calls
    //tests the refreshBillRates() method of RefreshBillRateApexController class
    static testMethod void testRefreshBillRateFunctionality() {
        ConstantsUtility.skipTestClass = true;
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
            
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
            
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
            
            
            Test.startTest();
            
            
            mtscopedata.insertCurrentExchangeRate();
            Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
            Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
            Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
            system.assert(testScopeModelProj != null);
            RefreshBillRateApexController.refreshBillRates(testScopeModelProj.Id);
            
            Test.stopTest();
        }
    }
}