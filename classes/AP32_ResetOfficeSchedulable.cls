/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History ----------------------- 
                                                                               VERSION     AUTHOR  DATE        DETAIL                   
                                                                                   1.0 -    Arijit Roy 03/29/2013  Created Schedulable class
=============================================================================================================================================== 
*/
global with sharing class AP32_ResetOfficeSchedulable implements schedulable{

	global void execute(SchedulableContext sc)
	{
		 Database.executeBatch(new AP33_ResetOfficeBatchable(), 8000);
	}
}