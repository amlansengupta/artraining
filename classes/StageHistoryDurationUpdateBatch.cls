global class StageHistoryDurationUpdateBatch implements Database.Batchable<sObject> {
    
    //public static boolean OppUpdateFlag = true;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('ENTER START');
      string query ='select id,stageName from Opportunity where Opportunity.isClosed = false';
          return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Opportunity> scope){
        Map<string,Integer> mapNameVsOrder = new Map<string,Integer>();
            map<Integer,OpportunityStageOrdering__mdt> mapData = new map<Integer,OpportunityStageOrdering__mdt>();
            integer highest=1 ;
        for(OpportunityStageOrdering__mdt oSo :[select id,order__c,stagename__c,start_date__c,end_date__c from OpportunityStageOrdering__mdt order by order__c desc]){
            if(highest<=oSo.order__c){
                highest = Integer.valueOf(oso.order__c);
            }
                mapNameVsOrder.put(oso.stagename__c,Integer.valueOf(oSo.order__c));
                mapData.put(Integer.valueOf(oSo.order__c),oso);
            }
        
        for(Opportunity op:scope){
            if(mapNameVsOrder.containsKey(op.stageName) && mapNameVsOrder.get(op.stageName)<highest ){
                for(integer i=mapNameVsOrder.get(op.stageName)+1;i<=highest;i++){
                    system.debug('&&'+i);
                    if(mapData.containskey(i) && (mapData.get(i).start_date__c!=null || mapData.get(i).end_date__C!=null) ){
                        string startdate = mapData.get(i).start_date__c;
                        string enddate = mapData.get(i).end_date__C;
                        op.put(startdate,null);
                        op.put(enddate,null);
                    }
                }
            }
        }
        update scope;
        }
        
    
    
    global void finish(Database.BatchableContext bc){
    }
        


}