/*Purpose:  This test class provides code coverage to AP17_ExecutionControl class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   01/21/2013  Created Test Class
============================================================================================================================================== 
*/
@isTest
public class test_AP17_ExecutionControl{
	/*
     * @Description : Test method to provide data coverage to AP17_ExecutionControl class
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void testAP17(){
    
        AP17_ExecutionControl.isDataMigrationGroupMember();
        AP17_ExecutionControl.isDataMigrationGroupMember(userinfo.getuserid());
        AP17_ExecutionControl.DataMigrationGroupMembers();
    
    }

}