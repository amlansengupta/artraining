/*Test Class for ProjectExceptionTriggerHandler*/
@isTest(seeAllData=true)
public class ProjectExceptionTriggerHandler_Test {

    private static Product2 testProduct = new Product2();
    private static Product2 testProduct1 = new Product2();
    private static Mercer_TestData mtdata = new Mercer_TestData();
    
    
    /*
     * @Description : Test method to provide data coverage to AP59_PricebookentryTriggerUtil class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
    {
        TriggerDispatcher trg=new TriggerDispatcher();
            
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        
        Product_Sub_Bus_Classification__c psb = new Product_Sub_Bus_Classification__c();
        psb.Business__c = 'TEST Bus';
        psb.Solution_Segment__c = 'TEST seg';
        psb.Classification__c = 'TEST class';
        psb.Sub_Busines__c = 'TEST sub bus';
        psb.SHORT_Sub_Business__c = 'TES';
        insert psb;   
       
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.LOB__c = 'TEST Bus';
        testProduct.Segment__c = 'TEST seg';
        testProduct.ProductCode='5676';
        insert testProduct;
               
        testProduct.IsActive = False;
        
        test.startTest();
                   
        Project_Exception__c pe1=new Project_Exception__c();
        pe1.Exception_Type__c='Product Code';
        pe1.Product_Code__c='5676';
        insert pe1;
        
        Project_Exception__c pe2=new Project_Exception__c();
        pe2.Exception_Type__c='Office';
        pe2.Office__c='Beijing - Guang Hua';
        insert pe2;
        
        
        Project_Exception__c pe4=new Project_Exception__c();
        pe4.Exception_Type__c='One Code';
        pe4.One_Code__c='Test1';
        insert pe4;
        
        pe4.One_Code__c='Test78'; 
        update pe4;    
        
        Project_Exception__c pe5=new Project_Exception__c();
        pe5.Exception_Type__c='Opportunity Country + LoB';
        pe5.Opportunity_country__c='HongKong';
        pe5.LOB__c='Health';    
        insert pe5;
       
        test.stoptest();
        
        }
        
    }
    static testMethod void myUnitTest1()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){ 
            
        Account acct = mtdata.buildAccount();
        
      
        //test.startTest();           
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'TEST Bus';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode='5436';
        
        
        List<Project_Exception__c> peList=new List<Project_Exception__c>();

        test.startTest(); 
        insert testProduct2;
            Project_Exception__c pe3=new Project_Exception__c();
        pe3.Exception_Type__c='Product + Office Code';
        pe3.Product_Code__c='5436';
        pe3.Office__c='Beijing - Guang Hua';
        insert pe3;
        peList.add(pe3);
             Project_Exception__c pe5=new Project_Exception__c();
        pe5.Exception_Type__c='Product + Office Code';
        pe5.Product_Code__c='5436';
        pe5.Office__c='Guangzhou - Tianhe';
        insert pe5;
             Project_Exception__c pe7=new Project_Exception__c();
        pe7.Exception_Type__c='Product + Office Code';
        pe7.Product_Code__c='0000';
        pe7.Office__c='Guangzhou - Tianhe';
        insert pe7;
         peList.add(pe5);   
             delete pe3;
        undelete pe3;
                    Map<Id,Project_Exception__c> ProjectExceptionMap= new Map<Id,Project_Exception__c>([Select Exception_Type__c,Product_Code__c,One_Code__c,Office__c,Record_Type_Name__c,LOB__c,Opportunity_Type__c from Project_Exception__c where Id In:peList ]);
                    
        test.stoptest();
            
       ProjectExceptionTriggerHandler pth=new ProjectExceptionTriggerHandler();
         pth.AfterInsert(peList, ProjectExceptionMap);
         pth.AfterUndelete(ProjectExceptionMap);
         pth.BeforeDelete(ProjectExceptionMap);
         pth.AfterUpdate(ProjectExceptionMap, ProjectExceptionMap);
         pth.BeforeInsert(peList);    //changed method argument
         pth.BeforeUpdate(ProjectExceptionMap, ProjectExceptionMap);
         pth.IsDisabled();
         ProjectTriggerHandler.TriggerDisabled=false;
        ProjectExceptionTriggerHandler.markProductAsException(ProjectExceptionMap);
        //Product2Update_Batch batch = new Product2Update_Batch();
          //  Database.executeBatch(batch);
        }
        
    }
   
}