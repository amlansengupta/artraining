/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose: This test class provides data coverage to AP08_AccountHierarchyController class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit 05/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP08_AccountHierarchyController {
    /*
     * @Description : Test method to provide data coverage to AP08_AccountHierarchyController class(Scenerio 1)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        //testData.buildContact();
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test123@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.id;
        //insert testContact;
       
        
        acc.GBL_ID__c = 'Test786';
       
        update acc;
        
        
        Account intAccount = new Account();
        intAccount = Mercer_TestData.buildIntermediateAccount(acc.GBL_ID__c); 
        insert intAccount;
        
        Account childAccount = new Account();
        childAccount = Mercer_TestData.buildChildAccount(acc.GBL_ID__c, intAccount.INT_ID__c);
        insert childAccount;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
                
        Test.startTest();
        system.runAs(User1){
            Pagereference pageref = Page.Mercer_AccountHierarchy;
            
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(acc);
            AP08_AccountHierarchyController controller = new AP08_AccountHierarchyController(stdController);               
            controller.createAccountHeirarchy(acc.GBL_ID__c);
           
            String AcId = (String)acc.id;
            pageref.getparameters().put('id',AcId );
            Test.setCurrentPage(pageref);
            controller.AccId = AcId;
            controller.getData();
        }     
        Test.stopTest();
        
        
    }
    /*
     * @Description : Test method to provide data coverage to AP08_AccountHierarchyController class(Scenerio 2)
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        //testData.buildContact();
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test123@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.Id;
       // insert testContact;
       
        
        acc.GBL_ID__c = null;
        update acc;
        
        
        Account intAccount = new Account();
        intAccount = Mercer_TestData.buildIntermediateAccount(acc.GBL_ID__c); 
        insert intAccount;
        
        Account childAccount = new Account();
        childAccount = Mercer_TestData.buildChildAccount(acc.GBL_ID__c, intAccount.INT_ID__c);
        insert childAccount;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
          
        Test.startTest();
        system.runAs(User1){
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(acc);
            AP08_AccountHierarchyController controller = new AP08_AccountHierarchyController(stdController);               
            controller.createAccountHeirarchy(acc.GBL_ID__c);
           
            String AcId = (String)acc.id;
            controller.AccId = AcId;  
            controller.getData();
        }   
        Test.stopTest();
        
        
    }
    /*
     * @Description : Test method to provide data coverage to AP08_AccountHierarchyController class(Scenerio 3) 
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        //testData.buildContact();
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test123@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.Id;
        //insert testContact;
        
         Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  acc.Id; 
        //Request Id:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate = system.today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Aarhus - Axel';
       // insert testOpportunity;
      
      
        acc.GBL_ID__c = 'Test1234';
        //Mercer_TestData.testAccount.Relationship_Manager__c = null;
        update acc;
        
        
        Account intAccount = new Account();
        intAccount = Mercer_TestData.buildIntermediateAccount(acc.GBL_ID__c); 
        insert intAccount;
        
        Account childAccount = new Account();
        childAccount = Mercer_TestData.buildChildAccount(acc.GBL_ID__c, intAccount.INT_ID__c);
        //childAccount = Mercer_TestData.buildChildAccount(Mercer_TestData.testAccount.GBL_ID__c, intAccount.INT_ID__c);
       
        insert childAccount;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
    
         test.startTest();
        system.runAs(User1){
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(acc);
            AP08_AccountHierarchyController controller = new AP08_AccountHierarchyController(stdController);               
            controller.createAccountHeirarchy(acc.GBL_ID__c);
           
            String AcId = (String)acc.id;
            controller.AccId = AcId;  
            controller.getData();
        }
        Test.stopTest();
         }
        
        
    
}