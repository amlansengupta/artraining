/*
=======================================================
Modified By 	Date         Request
Trisha			18-Jan-2019  12638-Commenting Oppotunity Step
=======================================================
*/
@isTest
public class Test_Batch_UpdateOpportunityFields {

    
    static testMethod void test_Batch_UpdateOpportunityFields() {
      ApexConstants__c apexCon = new ApexConstants__c();
        apexCon.Name ='Above the funnel';  
        apexCon.StrValue__c ='Marketing/Sales Lead;Executing Discovery;';
        Insert apexCon;
        ApexConstants__c apexCon1 = new ApexConstants__c();
        apexCon1.Name ='Selected';  
        apexCon1.StrValue__c ='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
        Insert apexCon1;
        
            ApexConstants__c apexCon2 = new ApexConstants__c();
        apexCon2.Name ='Identify';  
        apexCon2.StrValue__c ='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        Insert apexCon2;
            
            ApexConstants__c apexCon3 = new ApexConstants__c();
        apexCon3.Name ='Active Pursuit';  
        apexCon3.StrValue__c ='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        Insert apexCon3;
            
              /*Request No. 17953 : Removing Finalist START
              ApexConstants__c apexCon4 = new ApexConstants__c();
        apexCon4.Name ='Finalist';  
        apexCon4.StrValue__c ='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        Insert apexCon4;
        Request No. 17953 : Removing Finalist END */
          
               ApexConstants__c apexCon5 = new ApexConstants__c();
        apexCon5.Name ='AP02_OpportunityTriggerUtil_2';  
        apexCon5.StrValue__c ='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        Insert apexCon5;
            
               ApexConstants__c apexCon6 = new ApexConstants__c();
        apexCon6.Name ='AP02_OpportunityTriggerUtil_1';  
        apexCon6.StrValue__c ='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;IPIB01;MDMK02;MLTI07;MMMF01;TPEO50;  ';
        Insert apexCon6;
            
               ApexConstants__c apexCon7 = new ApexConstants__c();
        apexCon7.Name ='ScopeITThresholdsList';  
        apexCon7.StrValue__c ='EuroPac:25000;Growth Markets:2500;North America:25000';
        Insert apexCon7;
        
            ApexConstants__c apexCon8 = new ApexConstants__c();
        apexCon8.Name ='Opportunity_Country';  
        apexCon8.StrValue__c ='EuroPac:25000;Growth Markets:2500;North America:25000';
        Insert apexCon8;
        
    
        Integer month = System.Today().month();
        Integer year = System.Today().year();
       User user1 = new User();
       String adminprofile= Mercer_TestData.getsystemAdminUserProfile();      
       user1 = Mercer_TestData.createUser1(adminprofile,'usert7', 'usrLstName7', 7);
       system.runAs(User1){
     
        String s ='Select ID,OwnerId,AccountId,Account_Region__c,Account_Name_LDASH__c,Sibling_Contact_Name__c,Sibling_Contact_Office_Name__c,Sibling_Contact_Name__r.Location_Descr__c,Account.Name,Account.Account_Region__c,Owner_Profile_Name__c,Opp_Owner_Operating_Company__c,Owner.Profile.Name,Owner.Level_1_Descr__c from Opportunity where Owner_Profile_Name__c=null'; 
      
        Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
         testMog.Name = 'ABC';
         testMog.Market__c = 'United States';
         testMog.Sub_Market__c = 'Canada – National';
         testMog.Region__c = 'Manhattan';
         testMog.Sub_Region__c = 'Canada';
         testMog.Country__c = 'Canada';
         testMog.Isupdated__c = true;
         testMog.Office_Code__c = '123';
         testMog.Office__c = 'US';
         insert testMog;
         
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.Location__c = testMog.Id;
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
       // insert testColleague;
        
        Account testAcc = new Account();
        testAcc.Name = 'TestAccountName';
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingCountry = 'TestCountry';
        testAcc.BillingStreet = 'Test Street';
      //  testAcc.Relationship_Manager__c = testColleague.Id;
        insert testAcc;
        
       
        Mercer_Office_Geo_D__c testMog1 = new Mercer_Office_Geo_D__c();
         testMog1.Name = 'ABC2';
         testMog1.Market__c = 'United States';
         insert testMog1; 
             
        //testColleague.Location__c = testMog1.Id;
        //update testColleague;
        
        Opportunity testOpportunity =  new Opportunity();
        testOpportunity.Name = 'test oppty7';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  testAcc.ID; 
        //Request Id 12638-Commenting step__c
        //testOpportunity.Step__c = 'Pending Step';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        testOpportunity.StageName = 'Identified Deal';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(year, month, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Aarhus - Axel';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        testOpportunity.Close_Stage_Reason__c ='Other';
       
        insert testOpportunity;
        List<Opportunity> testOpportunity1 =[Select Id, Name,Owner_Profile_Name__c from Opportunity where Name =: 'test oppty7' Limit 1];
        testOpportunity1[0].Owner_Profile_Name__c='';
        update testOpportunity1;
        database.executeBatch(new Batch_UpdateOpportunityFields(s), 2000);
       // Database.BatchableContext BC;
        //Batch_UpdateOpportunityFields.execute(BC,testOpportunity1);  
       
    }
    }
}