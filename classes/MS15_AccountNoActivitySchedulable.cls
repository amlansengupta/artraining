/*Purpose: This Apex class implements schedulable interface to schedule MS16_AccountNoActivityBatchable class.
==============================================================================================================================================
History ----------------------- 
                                                                 VERSION     AUTHOR       DATE                    DETAIL 
                                                                    1.0 -    Arijit     04/29/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS15_AccountNoActivitySchedulable implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS16_AccountNoActivityBatchable(), Integer.valueOf(System.Label.CL79_MS16BatchSize));
    }
}