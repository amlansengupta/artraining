/*Purpose: This test class provides data coverage to AP62_UserAccountBatchable class
==============================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Srishty 07/18/2013  Created Test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP62_UserAccountBatchable {
    /*
     * Test data for user creation
     */
    private static User createUser(String profileId)
    {
        String randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        
        User testUser = new User();
        testUser.alias = 'te' ;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'User1' ;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.User1'  +'@xyz.com';
        testUser.EmployeeNumber = '101';
        testUser.Location__c = 'India';
        testUser.Newly_Licensed__c = true;
        insert testUser;
        System.runAs(testUser){
        }
         return testUser; 
        
        
        
       
    }

    /*
     * @Description : Test data to provide data coverage to AP62_UserAccountBatchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void test_AP62_UserAccountBatchable() 
    {

        Profile p = [Select Id from Profile where Name='System Administrator'] ;
        
        
        
        
        // Create Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '101' ;
        insert testColleague;

        String randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        
        //Create User Record
        
        
        //Create Account Record      
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        insert testAccount;
        
        User testUser1 = new User();
        testUser1.alias = 'te' ;
        testUser1.email = 'test@xyz.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'User2' ;
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = p.Id;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = randomName + '.User2'  +'@xyz.com';
        testUser1.EmployeeNumber = '101';
        testUser1.Location__c = 'India';
        testUser1.Newly_Licensed__c = true;
        testUser1.isActive = true;
        insert testUser1;
        System.runAs(testUser1){
        //Create Individual Sales Goal Record
        Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Colleague__c = testColleague.id;
        iSalesGoal.OwnerId = testUser1.Id;
        insert iSalesGoal;
        
        //Create Opportunity Record
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request id 12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.OwnerId = testUser1.Id;
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        Test.startTest() ;
         insert testOppty;
     
        // Create Extended Account Team Record
        Extended_Account_Team__c extaccTeam = new Extended_Account_Team__c();
        extaccTeam.Team_Member_Name__c = testColleague.Id;
        extaccTeam.Account__c = testAccount.Id;
        insert extaccTeam;
                
        

        String query='SELECT Id,Employeenumber from User where Id = \''+ testUser1.Id  + '\'';
        database.executeBatch(new AP62_UserAccountBatchable(query),200);
        
        Test.stopTest() ;
        }
    }
    /*
     * @Description : Test data to provide data coverage to DC21_UserAccountBulkBatchable class
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void test_DC21_UserAccountBulkBatchable() 
    {
            
            
            Profile p = [Select Id from Profile where Name='System Administrator'] ;
  
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.EMPLID__c = '123456' ;
            testColleague.LOB__c = '11111';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Level_1_Descr__c = 'Merc';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;

            User testUser = createUser(p.Id);
            testUser.EmployeeNumber = '123456';
            update testUser;
          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName' ;
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.One_Code__c = '111';
            testAccount.Competitor_Flag__c = True;
            insert testAccount;
            
           
            Extended_Account_Team__c extaccTeam = new Extended_Account_Team__c();
            extaccTeam.Team_Member_Name__c = testColleague.Id;
            extaccTeam.Account__c = testAccount.Id;
            insert extaccTeam;
            System.runAs(testUser){    
            Test.startTest() ;
        
            Test.stopTest() ;
           }
    }
  
}