global class OpportunityConcatenatedProductNameBatch implements Database.Batchable<SObject>, Database.Stateful{
    
    
    List<Opportunity> listRecords = new List<Opportunity>();
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query;
        if(Test.isRunningTest()){
            query = 'Select Id, Name, Concatenated_Products__c From Opportunity LIMIT 100';
        }
        else{
            query = label.OpportunityConcatenatedProductNameQuery;
        }
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        system.debug('queried result+++' +scope[0].Id);
        List<OpportunityLineItem> OppProductList = new List<OpportunityLineItem >();
        Set<Id> setOfOppId = new Set<Id>();
        Map<Id,List<OpportunityLineItem>> mapOfProducts = new Map<Id,List<OpportunityLineItem>>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        
        for(Opportunity opp : scope){
            setOfOppId.add(opp.Id);     
        }
        List<OpportunityLineItem> productRecords = new List<OpportunityLineItem >();
        
        productRecords = [Select Id,OpportunityId, Product2Id, Product2.Name FROM OpportunityLineItem where OpportunityId IN :setOfOppId];
        for(OpportunityLineItem ol: productRecords){
            
            if(mapOfProducts.containskey(ol.OpportunityId)){
                OppProductList = mapOfProducts.get(ol.OpportunityId);
                OppProductList.add(ol);
                mapOfProducts.put(ol.OpportunityId,OppProductList);
            }
            else{
                OppProductList = new List<OpportunityLineItem>();
                OppProductList.add(ol);
                mapOfProducts.put(ol.OpportunityId,OppProductList);
            }
        }
        
        for(Opportunity opp1 : scope){
            //system.debug('map value*** inside first for loop' +mapOfProducts.get(obj.Id));
            if(mapOfProducts.containsKey(opp1.Id)){
                
                opp1.Concatenated_Products__c = '';  
                
                for (OpportunityLineItem ol1 : mapOfProducts.get(opp1.Id)){                                         
                    opp1.Concatenated_Products__c = opp1.Concatenated_Products__c + ol1.Product2.Name + ';';    
                }
                
                oppsToUpdate.add(opp1);
            } 
        }        
        
        
        Database.update(oppsToUpdate,false);    
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}