public class TriggerDispatcher 
{
    /*
        Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
        This method will fire the appropriate methods on the handler depending on the trigger context.
    */
    public static void Run(ITriggerHandler handler)
    {
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.IsDisabled())
            return;
             
        // Detect the current trigger context and fire the relevant methods on the trigger handler:
 
        // Before trigger logic
        if (Trigger.IsBefore )
        {
            if (Trigger.IsInsert){
                //if((!Constantsutility.bypassTrigger && Test.isRunningTest()) || (!test.isRunningTest())){
                    system.debug('Gopal**1*' + trigger.new + '::::' + trigger.newMap);
                    handler.BeforeInsert(trigger.new);   //Changed method argument
                //}
            }
 
            if (Trigger.IsUpdate){
                //if((!Constantsutility.bypassTrigger && Test.isRunningTest()) || (!test.isRunningTest())){
                	handler.BeforeUpdate(trigger.newMap, trigger.oldMap);
                //}
            }
            if (Trigger.IsDelete)
                handler.BeforeDelete(trigger.oldMap);
        }
         
        // After trigger logic
        if (Trigger.IsAfter)
        {
            if (Trigger.IsInsert){
                //if(!Constantsutility.bypassTrigger){
                	handler.AfterInsert(trigger.new,Trigger.newMap);
                //}
            }
            if (Trigger.IsUpdate){
                //if(!Constantsutility.bypassTrigger){
                	handler.AfterUpdate(trigger.newMap, trigger.oldMap);
                //}
            }
            if (trigger.IsDelete)
                handler.AfterDelete(trigger.oldMap);
 
            if (trigger.isUndelete)
                handler.AfterUndelete(trigger.oldMap);
        }
    }
}