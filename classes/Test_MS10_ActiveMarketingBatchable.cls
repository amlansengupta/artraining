/*Purpose: Test Class for providing code coverage to MS10_ActiveMarketingBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
@istest
private class Test_MS10_ActiveMarketingBatchable  {
    /*
     * @Description : Test method to provide data coverage to MS10_ActiveMarketingBatchable batchable class(Scenerio 1)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.MercerForce_Account_Status__c ='Active Marketing Contact';
        insert Acc;
        
        
        Contact Con= new Contact();
        
        Con.LastName ='TestContactLastName';
        Con.AccountId = Acc.id;
        Con.FirstName ='TestContactFirstName';
        Con.Contact_ID__c = '896754231';
        Con.email = 'xyzq@abc13.com';
        insert Con;
        
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Distribution';
        distribution.Scope__c = 'Office';        
        distribution.Office__c = 'New Delhi';        
        insert distribution; 
        
        Contact_Distributions__c ConDistributions = new Contact_Distributions__c();
        ConDistributions.Contact__c = Con.Id;
        ConDistributions.Distribution__c = distribution.Id;
        insert ConDistributions;
        
        
        
        
        Test.startTest();
        system.runAs(User1){
        	database.executeBatch(new MS10_ActiveMarketingContactBatchable(), 500);
        }
        Test.stopTest();
    
    }
    
    /*
     * @Description : Test method to provide data coverage to MS10_ActiveMarketingBatchable batchable class(Scenerio 2)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest123()
     {
        Colleague__c Colleague = new Colleague__c();
        Colleague.Name = 'Srishty';
        Colleague.EMPLID__c = '123456';
        Colleague.LOB__c = '12345';
        Colleague.Last_Name__c = 'LastName';
        Colleague.Empl_Status__c = 'Active';
        Colleague.Email_Address__c = 'srishty@accenture.com';
        insert Colleague;
        
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Account Accnt = new Account();
        Accnt.Name = 'TestAccount';
        Accnt.BillingCity = 'TestCity';
        Accnt.BillingCountry = 'TestCountry';
        Accnt.BillingStreet = 'TestStreet';
        Accnt.Relationship_Manager__c = Colleague.Id;
        Accnt.One_Code__c = '123';
        Accnt.MercerForce_Account_Status__c ='Active Marketing Contact';
        insert Accnt;
        
        
        Contact Cont= new Contact();
        
        Cont.LastName ='ContactLastName';
        Cont.AccountId = Accnt.id;
        Cont.FirstName ='ContactFirstName';
        Cont.Contact_ID__c = '4578632190';
        Cont.email = 'xyzq1@abc11.com';
        insert Cont;
        
        
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Campaign';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        campaign.status = 'Completed';
        insert campaign;
        
        CampaignMember CampgnMem = new CampaignMember();
        CampgnMem.ContactId = Cont.Id;
        CampgnMem.CampaignId = campaign.Id;
       
        insert CampgnMem;
        
        
        Test.startTest();
        system.runAs(User1){
        	database.executeBatch(new MS10_ActiveMarketingContactBatchable(), 500);
        }
        Test.stopTest();
    
    }
    }