/*Purpose: This Apex class implements schedulable interface to schedule AP62_UserAccountBatchable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Srishty   07/18/2013  Created Apex Schedulable class
============================================================================================================================================== 
*/

global class AP63_UserAccountSchedulable implements Schedulable {

    
        //final string to hold the base query
        global final String query = 'SELECT Id,Employeenumber,IsActive from User where isActive = true and Newly_Licensed__c=true';        
        // constructor for the class
        global AP63_UserAccountSchedulable(){}
        
        // schedulable execute method
        global void execute(SchedulableContext SC) 
        {  
             // call database execute method to run AP60_UserAccountBatchable batch class 
             database.executeBatch(new AP62_UserAccountBatchable(query), integer.valueof(Label.AP63_Batch_Size)); 
        }
        
        
}