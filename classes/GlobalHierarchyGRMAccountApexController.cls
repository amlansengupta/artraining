/* Request Id#17418:This class is responsible to respond to any action triggered from the lightning component GlobalHierarchyGRMAccountComponent*/
public class GlobalHierarchyGRMAccountApexController {
    /* Request Id#17418:This method retrieves running user's profile name from system sends back to the UI*/
    @AuraEnabled
    public static String getProfileName(){
            //To fetch the running user's profile name
            Profile profile = [select Name from Profile where Id =: UserInfo.getProfileId()];
            return profile.name;
    }
}