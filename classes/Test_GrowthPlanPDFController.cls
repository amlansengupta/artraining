@IsTest
public class Test_GrowthPlanPDFController{
 
  static testmethod void Validatecontroller(){
     //Create Account
     Account ac = New Account();
     ac.Name = 'Apple';
     ac.Country__c='India';
     ac.One_Code__c = 'ABC123';
     
      Insert ac;
      
     Account acc = [Select Id,Name from Account where Name = 'Apple' Limit 1];
     Contact con = new Contact();
     con.LastName='test';
     con.AccountID=Acc.Id;
     con.Email='Test1747@gmail.com';
     con.Country__c ='India';
      
     insert con;
      
    //Create FCST Fiscal Year List
    FCST_Fiscal_Year_List__c yList= New FCST_Fiscal_Year_List__c();
    yList.Name='2018';
    yList.StartDate__c = date.parse('1/1/2017');
    yList.EndDate__c= date.parse('12/31/2017');
    Insert yList;   
     
    /* 
    date odate = system.today();
    Opportunity Oopp = new Opportunity(AccountId = Acc.id,Name='OpenCurrentYear',stageName='Open',Opportunity_Office__c='Aarhus - Axel',Buyer__c=con.Id,CloseDate= odate );
    insert OOpp;
    odate = (system.today().addDays(1)).addYears(-1);
    system.debug(' ODate:'+ system.today());
    Opportunity Popp = new Opportunity(AccountId = Acc.id,Name='OpenPastYear',stageName='Open',Opportunity_Office__c='Aarhus - Axel',Buyer__c=con.Id,Count_Product_LOBs__c=0, CloseDate= system.today());
    insert POpp;
    Opportunity PWopp = new Opportunity(AccountId = Acc.id,Name='ClosedWonPastYear',stageName='Closed Won',Opportunity_Office__c='Aarhus - Axel',Buyer__c=con.Id,Count_Product_LOBs__c=0, CloseDate= system.today());
    insert PWOpp;
    Opportunity Copp = new Opportunity(AccountId = Acc.id,Name='OpenCurrentYear',stageName='Open',Opportunity_Office__c='Aarhus - Axel',Buyer__c=con.Id,Count_Product_LOBs__c=0, CloseDate= system.today());
    insert COpp;
    */

    //Insert GrowthPlan
    Growth_Plan__c gp = New Growth_Plan__c();
    if(acc!=null & yList!=null){
          gp.Account__c = acc.Id;
          gp.Planning_Year__c = yList.Name; 
          gp.fcstPlanning_Year__c = YList.Id;
          gp.Economic_Factors__c='Negative / Blocker';
          gp.Regulatory_Factors__c = 'Negative / Blocker';
          gp.Political_Factors__c = 'Negative / Blocker';
    }
    Insert gp;
      
    List<growth_plan__c> lstGP = [select id from growth_plan__c where account__c =:acc.Id];
       
    // Insert Client Business Imperatives
    Client_Business_Imperatives__c cbi = New Client_Business_Imperatives__c();
    cbi.Account__c = acc.Id;
    cbi.Growth_Plan__c = gp.Id;
    cbi.Client_Business_Imperative__c='Test CBI';
    cbi.Status__c = 'Not_Relevant';
    cbi.Our_Offering_Area__c= 'Health';
    insert cbi;
    
    List<Client_Business_Imperatives__c> lstCBI = [select id from Client_Business_Imperatives__c];
    
    //Insert Account Alignment
    AccountAlignmentClient__c  accAlign = New AccountAlignmentClient__c();
    accAlign.Account__c = acc.Id;
    accAlign.Growth_Plan__c = gp.Id;
    accAlign.Buying_Center__c='Test1';
    accAlign.DECISION_MAKER_CUSTOMER__c = 'Test2';
    accAlign.Relationship_Status__c = 'Negative_Blocker';
    accAlign.Our_Alignment__c = 'Tim Ebbeck - Country GM, Australia'; 
    accAlign.Decision_Criteria_Preferences__c = 'Excellent and long-time';
    accAlign.Actions_for_Improving_relationships__c = 'None, maintain status quo';
    insert accAlign;
    
    //insert forecast model
    Forecast_Model__c  fcstModel= New Forecast_Model__c();
    fcstModel.Account__c = acc.Id;
    fcstModel.Growth_Plan__c = gp.Id;
    fcstModel.Planning_Year__c = yList.Id;
    fcstModel.Pre_Prior_FY_Actual__c = 1000;
    fcstModel.Prior_LTM_Revenue__c =1001;
    fcstModel.Prior_FY_Projected__c = 1002;
    fcstModel.Current_Carry_Forward_Revenue__c = 1004;
    fcstModel.Current_FY_Revenue__c = 1005;
    fcstModel.Post_FY_Revenue__c = 1006;
    fcstModel.Post1_FY_Revenue__c = 1007;
    fcstModel.LOB__c = 'Wealth';
    fcstModel.Sub_Business__c = 'Investments: DC Inv Solutions';
    insert fcstModel;
    
    //Insert Competitive Assessment
    Competitive_Assessment__c cmpAssess =  new Competitive_Assessment__c();
    system.debug('@@@ AccId: ' + acc.Id);
    
    cmpAssess.Account__c = acc.Id;
    cmpAssess.Growth_Plan__c = gp.Id;
    cmpAssess.Account_Footprint_And_Strengths__c = 'Test';
    cmpAssess.Account_s_Perception_of_Competitor__c = 'Test';
    cmpAssess.Area__c = 'Health';
    cmpAssess.Competitor__c ='Test';
    cmpAssess.Estimated_Wallet_Share__c= '0-25%';    
    cmpAssess.Their_Key_Executive_Sponsor_s__c = 'Test';
    Insert cmpAssess;
    
    Client_Relationship__c clntRltp = New Client_Relationship__c();
    clntRltp.Account__c = acc.Id;
    clntRltp.Growth_Plan__c = gp.Id;
    clntRltp.Buying_Center_Detail__c = 'Test';
    clntRltp.LOB__c = 'Health';
    insert clntRltp;
    
    Goal_Relationship__c gr = New Goal_Relationship__c();
    gr.Account__c = acc.Id;
    gr.Growth_Plan__c = gp.Id;
    gr.Area__c = 'Test';
    gr.Goal_Description__c ='Test';
    gr.Relationship_Status__c ='Red';
    gr.Risks_Barriers__c = 'Test';
    gr.Executive_Status__c ='Accomplished';
    gr.Executive_Name_And_Role__c='Test';
    insert gr;
  
    Goal_Action__c ga=new Goal_Action__c();
    ga.Goal_Relationship__c = gr.Id;
    ga.Action_Name__c = 'Test';
    ga.Due_Date__c = Date.Parse('1/1/2017');
    ga.Priority__c = 'High';
    ga.Status__c = 'In Progress';
    insert ga;
    
    Swot_Opportunity__c swt_opp = New Swot_Opportunity__c();
    swt_opp.Account__c = acc.Id;
    swt_opp.Growth_Plan__c = gp.Id;
    swt_opp.Opportunity__c = 'Test';
    insert swt_opp;

    Swot_Strength__c swt_stgh = New Swot_Strength__c();
    swt_stgh.Account__c =  acc.Id;
    swt_stgh.Growth_Plan__c = gp.Id;
    swt_stgh.Strength__c = 'Test';
    insert swt_stgh;
        
    Swot_Threat__c swt_thrt= New Swot_Threat__c();
    swt_thrt.Account__c = acc.Id;
    swt_thrt.Growth_Plan__c = gp.Id;
    swt_thrt.Threat__c = 'Test';
    insert swt_thrt;

    Swot_Weak__c swt_weak = New Swot_Weak__c();
    swt_weak.Account__c = acc.Id;
    swt_weak.Growth_Plan__c = gp.Id;
    swt_weak.Weakness__c = 'Test';
    insert swt_weak;
      
    Client_Engagement_Measurement_Interviews__c cemObj = new Client_Engagement_Measurement_Interviews__c();
	cemObj.Overall_rating__c = '2';
    cemObj.Date_of_Review__c = Date.parse('07/08/2019');
    cemObj.Account__c = acc.Id;
	insert cemObj; 
    
  
    /*Sensitve_At_Risk__c sar = New Sensitve_At_Risk__c();
    sar.Account__c = acc.Id;
    sar.Resolution_Plan__c = 'Test';
    sar.At_Risk_Reason__c = 'Financial';
    sar.At_Risk_Type__c = 'Sensitive';
    sar.Date_Closed__c = Date.Parse('1/1/2017');
    sar.Date_Opened__c  = Date.Parse('1/5/2017');
    sar.Due_Date__c = Date.Parse('1/9/2017');
    sar.Impact__c = 'Full client';
    sar.LOB__c = 'Health';
    insert sar; */
  
    List<Growth_Plan__c> grplan=[Select Id,Account__r.Id from  Growth_Plan__c where Account__r.Name='Apple'];  
    if(grplan.size()>0){
       PageReference pageRef = Page.GrowthPlanPDF;
       Test.setCurrentPage(pageRef);
       pageRef.getParameters().put('id', grplan[0].Id);
       ApexPages.StandardController gpSC = new ApexPages.StandardController(grplan[0]);
       GrowthPlanControllerPDF gpCtrl = new GrowthPlanControllerPDF(gpSC);
       GrowthPlanControllerPDF.convert('USD','Inr',5000);
       gpCtrl.growthPlan=gp;
       gpCtrl.gpId=gp.Id;
       gpCtrl.lstGp = lstGP;
       gpCtrl.lstClnt_BusImper = lstCBI;
       gpctrl.lstCEMRating = string.valueof(5);

       gpctrl.currenttotal = decimal.valueof('12');
       gpctrl.lastTotal = decimal.valueof('13');
       gpctrl.wonTotal = decimal.valueof('15');
        
       gpctrl.currentamount = decimal.valueof('12');
       gpctrl.lastamount = decimal.valueof('13');
       gpctrl.wonamount = decimal.valueof('15');
       gpctrl.dateOfReview = Date.parse('01/01/2019');
       gpctrl.Reviewer = 'Test Reviewer';
       //gpctrl.CEMObj
       

       gpCtrl.getAccountDetails();
       //gpctrl.getOpportunities(acc.Id);
       gpCtrl.getAcct_alignmnt();
       gpCtrl.getSensitiveAtRisk();
       gpCtrl.getSwotOpportunity();
       gpCtrl.getSwotWeakNess();
       gpCtrl.getSwotStrength();
       gpCtrl.getSwotThreat();
       gpCtrl.getClient_Busns_Imprtvs();
       gpCtrl.getCmpt_Assesmnt();
       gpCtrl.getForecast_model();
       gpCtrl.getGoals_Actions();
       gpCtrl.curPlanYear_PrevYear=2017;
       gpCtrl.curPlanYear_Pre_PrevYear=2016;
       gpCtrl.checkImage1='Block';
       gpCtrl.checkImage2='Block';
       gpCtrl.checkImage3='Block';
       
       //gpCtrl.CurrencyInitialization();
       
      }
   }
}