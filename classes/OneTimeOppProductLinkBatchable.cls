global class OneTimeOppProductLinkBatchable implements Database.Batchable<sObject>{
    private String query;       

    // List variable to store error logs 
    private List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    /*
    *   Class Constructor 
    */
    global OneTimeOppProductLinkBatchable (){
         String qry='Select ID from Revenue_Connectivity__c';
        if(Test.isRunningTest()){
            qry='Select ID from Revenue_Connectivity__c limit 1';
        }
         this.query= qry;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
  
    global void execute(Database.BatchableContext BC, List<Revenue_Connectivity__c> scope)
    {  
         Integer errorCount = 0 ;
         List<Revenue_Connectivity__c> rcListUpdate = new List<Revenue_Connectivity__c>();         
                          
         for(Revenue_Connectivity__c rc:scope){ 
           
                rcListUpdate.add(rc);            

        }
                           
                 
     // List variable to save the result of the Opportunity Line Items which are updated   
     List<Database.SaveResult> srList= Database.Update(rcListUpdate, false);  
    
  
     if(srList!=null && srList.size()>0) {
         // Iterate through the saved Opportunity Line Items  
         Integer i = 0;             
         for (Database.SaveResult sr : srList)
         {
                // Check if Oppportunity Product are not updated successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) 
                {
                        errorCount++;
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('OneTimeOppProductLinkBatchable:' +sr.getErrors()[0].getMessage(), errorLogs, rcListUpdate[i].Id);  
                }
                i++;
         }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('OneTimeOppProductLinkBatchable Status :scope:' + scope.size() +' rcListUpdate:' + rcListUpdate.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);
     
      database.insert(errorLogStatus);    
    }
    
    global void finish(Database.BatchableContext BC)
    {
                           
        if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }
    

}