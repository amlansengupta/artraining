/*Purpose: This Batch Apex executes on newly licensed Users and performs below logic:
==============================================================================================================================================
•   If an User is activated in system and has an existing colleague, who is the relationship manager of any account 
    but not Account Owner,the newly active User will be assigned as Account Owner
•   Checks if an corresponding colleague of newly active User is a Team Member of Extended Account Team for any account then colleague is removed from Extended 
    account and user is added as Team Member of same Account.
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Srishty 07/18/2013  Created Apex Batchable class
   2.0 -    Gyan    25/10/2013  Updated Code As Per Req-0726.
   3.0 -    Gyan    25/11/2013  Updated Code As Per Req - 3477
============================================================================================================================================== 
*/

global class AP62_UserAccountBatchable implements Database.Batchable<sObject>{

        //final string to hold the query
        global String query;
        // List variable to store error logs
        public List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
        //List to hold Account results
        List<Database.SaveResult> accResult = new List<Database.SaveResult>();
        //List to hold Account Team Result 
        List<Database.SaveResult> teamAccResult = new List<Database.SaveResult>();
        //List to hold Opportunity Team Member results
        List<Database.SaveResult> oppTeamMemberResult = new List<Database.SaveResult>();
         //List to hold Opportunity Team Member results
        List<Database.SaveResult> saleGoalResult = new List<Database.SaveResult>();
        //List to Hold Opportunity Team Members which needs to be inserted.
        List<OpportunityTeamMember> lstoppTeamMembersToBeInserted = new List<OpportunityTeamMember>();

     //Constructor for class
     public AP62_UserAccountBatchable(String query) 
     {
        this.query = query ;
     }
     
     
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // this method will return 50 million rows
       // AP67_userTriggerUtil.updateFromBatchJob = True;
        return Database.getQueryLocator(query); 

    }   
    
    
    /*
     *  Method Name: execute
     *  Description: Method is used to Check the User created if they are existing colleague and RelationshipManager of account 
                     then User is assigned as account Owner
                     Also Checks if the User created is a member of Extended Account team Member then delete from Extended Account Team and 
                     add as Account Team Member.
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        //Integer to hold error count while updating account owner
        Integer errorCount = 0; 
        
        //Integer to hold error count while updating account Team Member 
        Integer errorCountTM = 0;
        
        //Integer to hold error count while updating account Team Member 
        Integer errorCountOppTeam = 0;
        
        //Integer to hold error count while updating Individual Sales Goal 
        Integer errorCountSalesGoal = 0;
        
        //Map to hold User EmployeeNumber and User
        Map<String,User> userMapForEmp = new Map<String,User>() ;
        //List to store value of Accounts to be updated
        List <Account> updateAccnt = new List <Account>();
        //List to store value of scope
        List<User> userLst = scope;
        //List to store Team Members in Extended Account Team to be deleted
        List <Extended_Account_Team__c> delexAccntteam = new List <Extended_Account_Team__c>();
        
        //Iterate for User
        for (User u: userLst )
        {
            //Add User and Employeenumber to map
            userMapForEmp.put(u.EmployeeNumber,u) ;
        }
        
        //Map to hold colleague's Employee ID for users in scope
        Map<Id,Colleague__c> mapColleague = new Map<Id,Colleague__c>([SELECT EMPLID__c FROM Colleague__c where EMPLID__c IN: userMapForEmp.keyset()]);        
        
        //Iterate for account
        for(Account acc : [Select Id,Relationship_Manager__c,Relationship_Manager__r.EmplID__c ,Owner.EmployeeNumber from Account where Account.Relationship_Manager__c IN :mapColleague.keySet()])
        {
          
              Colleague__c curCol = mapColleague.get(acc.Relationship_Manager__c) ;
              //If the Colleague is not NULL
              if(curCol!=null)
              {
                //If Account Owner and Account RelationshipManager are not same              
                if (acc.Owner.EmployeeNumber != curCol.EMPLID__c) 
                {   
                    //If UserMap is not NULL                  
                    if(userMapForEmp.get(curCol.EMPLID__c)!=null)                        
                    {   
                        //Assign Account Owner as RM of the account                         
                        acc.OwnerId = userMapForEmp.get(curCol.EMPLID__c).Id ;
                        //Add to update Account List
                        updateAccnt.add(acc);
                                     
                    }                       
                } 
              }               
            
        }
        

      //List to hold Account Team Member to be inserted to Account Team
      List <AccountTeamMember >  insertAcntTeamMember= new List<AccountTeamMember >();
      //List to hold Account Share to be inserted
      List<AccountShare> insertAcntShare = new List<AccountShare>();
      //List to hold Team Members of Extended Account Team to be deleted
      List<Extended_Account_Team__c> deleAccntteam = new List<Extended_Account_Team__c>();
      //List to hold Account Team Member     
      List <AccountTeamMember> AcntTeamMember = [SELECT UserId FROM AccountTeamMember WHERE UserId IN: userLst];  
         
        //Iterate for Account     
        for(Account newAcnt: [Select Id,OwnerId,(SELECT id, Team_Member_Name__c FROM Additional_Account_Team_Members__r ) from account where id IN (select account__c from Extended_Account_Team__c where Team_Member_Name__c IN: mapColleague.keySet())])   
        {
           //String to hold Colleague
           Colleague__c curColl; 
           
           //Iterate for Extended Account Team 
           for (Extended_Account_Team__c extnacc: newAcnt.Additional_Account_Team_Members__r )
            {
               //Check if this Colleague is mapped with one of recently licensed user
               curColl = mapColleague.get(extnacc.Team_Member_Name__c) ;
               
                //If the String is not NULL  
                if (curColl != NULL)
                {
                   //Add the Team Member to DeleteAccountTeam List
                   deleAccntteam.add(extnacc);
                    
                   //Create a new Account Team Member   
                   AccountTeamMember accTeam = new AccountTeamMember();
                   //Assign Account ID to Account Team Member
                   accTeam.AccountId = newAcnt.Id;
                   //Assign UserID to account Team Member to be inserted
                   accTeam.UserId = userMapForEmp.get(curColl.EMPLID__c).Id; 
                   //Add to InsertAccountTeamMember List
                   insertAcntTeamMember.add(accTeam);
                   
                   //Create a new Account Share   
                   AccountShare accntshare = new AccountShare();
                   //Assign UserID to Account Share
                   accntshare.UserOrGroupId = userMapForEmp.get(curColl.EMPLID__c).id;
                   //Assign OpportunityAccessLevel to Account Share
                   accntshare.OpportunityAccessLevel = 'Edit';
                   //Assign AccountAccessLevel to Account Share
                   accntshare.AccountAccessLevel = 'Edit';
                   //Assign Account ID to Account Share
                   accntshare.AccountId = newAcnt.Id;
                   //Add to InsertAccountShare List
                   insertAcntShare.add(accntshare); 
                      
                 
                 }           
            }
                
           }          
        //++ Code Added By Gyan Starts Here Under Req-0726.
        Set<String> EmpNumbercollegueId = new Set<String>();
         //List to Hold Opportunity Team Members
        List<OpportunityTeamMember> lstoppTeamMembers = new List<OpportunityTeamMember>();

        //Map to Hold key - EmpNumber of User+ oppId Attached with Opportunity Team Member Record and value as whole Opportunity Team member record.
        Map<String,OpportunityTeamMember> mapOppWithTeamMembers = new Map<String,OpportunityTeamMember>();
        //Map to Hold key - EmpID of collegue + oppId Attached with sales-credit Record and value as whole sales-credit record.
        Map<String,Sales_Credit__c> mapOppWithSalesCredit = new Map<String,Sales_Credit__c>();

        For(Id id:mapColleague.keySet())
        {
            EmpNumbercollegueId.add(mapColleague.get(id).EMPLID__c);
        }

        
        List<Sales_Credit__c> lstSales = [Select Opportunity__c, EmpName__c,Emp_Id__c From Sales_Credit__c s where Emp_Id__c IN :EmpNumbercollegueId];
                // Set to Hold OppID/EmpId attached with sales-credit.
                Set<String> setOppId = new Set<String>();
                Set<String> setEmpId = new Set<String>();
                
                //Iterate through sales-credit list and create Map to Hold key - EmpID of collegue + oppId Attached with sales-credit Record and value as whole sales-credit record.
                for(Sales_Credit__c obj:lstSales)
                {
                       setOppId.add(obj.Opportunity__c);
                       mapOppWithSalesCredit.put(obj.Emp_Id__c+'_'+obj.Opportunity__c,obj);
                       setEmpId.add(obj.Emp_Id__c);
                }
            // Fetch all opportunity Team members record for opportunity
            lstoppTeamMembers = [Select o.OppTeamMemID__r.EmployeeNumber, o.OpportunityId,o.OppTeamMemID__c From OpportunityTeamMember o  where OpportunityId IN :setOppId];   
            //Iterate through lstoppTeamMembers list and create Map to Hold key - EmpNumber of User+ oppId Attached with Opportunity Team Member Record and value as whole Opportunity Team member record
            for(OpportunityTeamMember otm : lstoppTeamMembers)
            {
                mapOppWithTeamMembers.put(otm.OppTeamMemID__r.EmployeeNumber+'_'+otm.OpportunityId,otm);
            }
             //Fetch all user Record those are active in System.
            List<User> userList = [Select Id ,EmployeeNumber From User where EmployeeNumber IN : setEmpId and isActive = True]; 
            Map<String,String> userMap = new Map<String,String>();
            For(User u : userList)
            {
                userMap.put(u.EmployeeNumber,u.Id);
            }
            
            // Check if Opportunity Team Member record not already exist for selected sales-credit record.
            for(String s : mapOppWithSalesCredit.keySet())
            {
                     Integer index = s.indexOf('_');
                     String empNum = s.substring(0,index);
                     String oppId = s.substring(index+1);
                if(mapOppWithTeamMembers.get(s)==Null && userMap.get(empNum)!= Null)
                {
                    //create a new instance of opportunity team member
                     OpportunityTeamMember teamMember = new OpportunityTeamMember();
                     
                     //assign the new record's owner as team member
                     teamMember.userId = userMap.get(empNum);
                     //assign new record's opportunity ID as team member records' opportunity ID
                     teamMember.OpportunityId = oppId;                     
                     // add each instance to the list
                     lstoppTeamMembersToBeInserted.add(teamMember);
                }
            }
        mapOppWithSalesCredit.clear();
        mapOppWithTeamMembers.clear();
        lstoppTeamMembers.clear();
        userMap.clear();
        
        //-- Code Added By Gyan Ends Here Under Req-0726.
        
        //++ Code Added By Gyan Starts here Under Req-3477
            
            /* 
            Whenever a user has been given a license(i.e. Newly License = True and isActive=True),
            Then We need to search  Active colleague corresponding to this newly licensed User.
            If For this colleague,individual sales Goal Record Exist,
            Then Update the owner of Individual sales goal record  from Mercerforce to corresponding  user record of the colleague.     
            */
            
            // List To Hold Sales Goal Which Needs to be Updated.
            List<Individual_Sales_Goal__c> lstSalesGoalToBeUpdated = new List<Individual_Sales_Goal__c>();
            
            
            //Fetch All Individual Sales Goal Record.
            List<Individual_Sales_Goal__c> lstSalesGoal = [select id,Colleague__c from Individual_Sales_Goal__c where Colleague__c != Null and Colleague__c IN : mapColleague.keySet()];
            
            For(Individual_Sales_Goal__c isg : lstSalesGoal)
            {
                isg.OwnerId = userMapForEmp.get(mapColleague.get(isg.colleague__c).EMPLID__c).Id;
                lstSalesGoalToBeUpdated.add(isg);
            } 
            
          
            
    //-- Code Added By Gyan Ends Here Under Req-3477.
        
         //If UpdateAccount List is not empty
         if(updateAccnt.size()>0)
          {
           //Update Account
           accResult = Database.update(updateAccnt,false) ; 
           //Set Integer i to 0
           integer i = 0;
           
           //Iterate for Error Logging
           for(Database.Saveresult result : accResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP62:' + result.getErrors()[0].getMessage() , errorLogs, updateAccnt[i].Id);  
                  errorCount++;
                }
                i++;
                
              }
         }
         //If DeleteAccountTeamMember is not empty
         if(deleAccntteam.size()>0) 
            //Delete Team Member from ExtendedAccountTeam
             Database.delete(deleAccntteam,false); 
         
         //If InsertAccountTeamMember is not empty   
         if(insertAcntTeamMember.size()>0)
        {
           //Insert to Account Team
           teamAccResult = Database.insert(insertAcntTeamMember,false) ;
           
           //Iterate for Error Logging
            for(Database.Saveresult result : teamAccResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP62 AccountTeam:' + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCountTM++;
                }
                
              }           
        } 
        //++ Code Added By Gyan Starts Here Under Req-0726
          // Insert List of New Opportunity Team Members.
        if(!lstoppTeamMembersToBeInserted.isEmpty())
        {
            oppTeamMemberResult = Database.insert(lstoppTeamMembersToBeInserted,false) ; 
            integer i = 0;
         //Iterate for Error Logging
            for(Database.Saveresult result : oppTeamMemberResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP62 OppTeam :' + result.getErrors()[0].getMessage() , errorLogs, lstoppTeamMembersToBeInserted[i].Id);  
                  errorCountOppTeam++;
                }
                i++;
            }
        }
       
        //-- Code Added By Gyan Ends Here Under Req-0726
        
        //++ Code Addded By Gyan starts Here Under Req-3477
        
            
            if(!lstSalesGoalToBeUpdated.isEmpty())
            {
                saleGoalResult = Database.update(LstSalesGoalToBeUpdated,false) ; 
                integer i = 0;
             //Iterate for Error Logging
                for(Database.Saveresult result : saleGoalResult)
                {
                    // To Do Error logging
                    if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                    {
                      errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP62 Individual Sales Goal :' + result.getErrors()[0].getMessage() , errorLogs, lstSalesGoalToBeUpdated[i].Id);  
                      errorCountSalesGoal++;
                    }
                    i++;
                }
            }
        //-- Code Added By Gyan ends Here Under Req-3477
        //If InsertAccountShareList is not empty
        if(insertAcntShare.size()>0)
            //Insert to Account Share
            Database.insert(insertAcntShare,false) ;
        
        //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         errorLogStatus  = MercerAccountStatusBatchHelper.addToErrorLog('AP62 Status :scope:' + scope.size() +' updateAccnt:' + updateAccnt.size()+' Extended Accounts Mem deleted:' + deleAccntteam.size() + ' Account Mem Added : ' + insertAcntTeamMember.size() +'Account share : '+insertAcntShare.size()+'Opportunity Team Member Inserted :'+lstoppTeamMembersToBeInserted.size()+'Individual Sales Goal Inserted:'+lstSalesGoalToBeUpdated.size()+ ' Total Errorlog: '+errorLogs.size() + 'Batch Error Count errorCountTM ' +errorCountTM + ' errorCountAccount ' +errorCount + 'errorCountSalesGoal'+errorCountSalesGoal+' errorCountOppTeam : ' +errorCountOppTeam  ,  errorLogStatus  , scope[0].Id);
         
         //Insert to ErrorLogStatus
         insert  errorLogStatus ;
         
          lstoppTeamMembersToBeInserted.clear();
      
    }      
   /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope.     
     */
    global void finish(Database.BatchableContext bc)
    {
        // Reset the flag 'Newly_Licensed__c' on User object for all the users which are processed by Batch
        
        List<User> userLst = [SELECT Id,Employeenumber,Newly_Licensed__c from User where Newly_Licensed__c=true] ;
        //List to hold Users to be updated
        List<User> updateUserLst = new List<User>();
        //If User List in not NULL
        if(userLst!=null && userLst.size()>0)
         {
            //Iterate for User
            for(User ur:userLst)
             {
                //Set the Newly Licensed flag as false
                ur.Newly_Licensed__c=false ;
                //Add to Update User List
                updateUserLst.add(ur) ; 
            }
        }
        //If UpdateUser List os not empty
        if(updateUserLst.size()>0) {
            //Update UserList
            if(!Test.isRunningTest())
            Database.update(updateUserLst) ;
        }
        
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  
    }
  }