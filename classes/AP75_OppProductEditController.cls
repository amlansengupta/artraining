/*Purpose:   This apex class is created to manually allocate Revenue to Opportunity Product
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   11/18/2013  Created Apex class controller
============================================================================================================================================== 
*/


public with sharing class AP75_OppProductEditController {

    private ApexPages.StandardController controller {get; set;}
    public OpportunityLineItem oli {get;set;}     
    OpportunityLineItem oldOli = new OpportunityLineItem();
    String returl;
    public AP75_OppProductEditController(ApexPages.StandardController controller) {
         
         //initialize the stanrdard controller
         this.controller = controller;
         oli = (OpportunityLineItem)controller.getRecord();

         returl = System.CurrentPageReference().getParameters().get('retURL');    
          
         Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.OpportunityLineItem.fields.getMap();
         List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
         
         String theQuery = 'SELECT ';
                 
         for(Schema.SObjectField s : fldObjMapValues)
         {
              String theLabel = s.getDescribe().getLabel(); // Perhaps store this in another map
              String theName = s.getDescribe().getName();
                                     
              // Continue building your dynamic query string
              theQuery += theName + ',';
         }
         
         String opprodid = oli.id;
         
         // Finalize query string
         theQuery +=  'PricebookEntry.Product2Id FROM OpportunityLineItem WHERE id =: opprodid';
                
         // Make your dynamic call
         oli = Database.query(theQuery);
                
         oldOli = [select id,Revenue_End_Date__c,Revenue_Start_Date__c,Year2Revenue_edit__c,Year3Revenue_edit__c,CurrentYearRevenue_edit__c from OpportunityLineItem where id= :oli.id];
    }
    
    
    /*
     * @Description :This method calculates and allocates revenue to Opportunity Product
     * @ Args       : null   
     * @ Return     : void
     */
    public void ReallocateRevenue(){
          Date rsdt = oli.revenue_start_date__c;
          Date redt = oli.revenue_end_date__c;
          Integer cyr = system.today().year();
          Double dbstend = (rsdt.daysbetween(redt)+1);
          
          if(rsdt.year()<>cyr) {
             oli.CurrentYearRevenue_edit__c = 0;
          }
          else if(redt.year() - rsdt.year() ==0 ){
             oli.CurrentYearRevenue_edit__c = oli.UnitPrice;
          }
          else{
             
             Double dbtw = rsdt.daysbetween(date.newInstance(system.today().year(),12,31)); //days between revenue start date and end of current year
             Double dbtw2 = rsdt.daysbetween(oli.Revenue_End_Date__c); // days between revenue start date and revenue end date
             Double divn = ((dbtw+1) / (dbtw2+1));
             Decimal cyrev = divn * oli.UnitPrice;
             system.debug((dbtw+1) +'....' + (dbtw2+1) + '....' + divn  +'....'+oli.unitprice);
             oli.CurrentYearRevenue_edit__c =  Math.round(divn * oli.UnitPrice);// cyrev;
          }
          
         if(redt.year() == rsdt.year() ){
             if(redt.year() == cyr+2)
                 oli.Year2Revenue_edit__c = 0;
             else if(redt.year() == cyr+1)
                 oli.Year2Revenue_edit__c = oli.UnitPrice;
             else
                 oli.Year2Revenue_edit__c = 0;
          }
          else if(rsdt.year() == cyr){
              if(redt.year() == cyr+1){
                  Double dbtw = date.newInstance(cyr+1,1,1).daysbetween(redt)+1;
                  oli.Year2Revenue_edit__c = Math.round((dbtw/dbstend )*oli.UnitPrice);   
              }
              else {
                  Double calc = 365.0/dbstend;
                  oli.Year2Revenue_edit__c = Math.round(calc *oli.UnitPrice);
              }
          }
          else if(rsdt.year() == cyr+1){
              Double dbtw = rsdt.daysbetween(date.newInstance(cyr+1,12,31))+1;
              oli.Year2Revenue_edit__c =   Math.round((dbtw/(dbstend))*oli.UnitPrice); 
          }
          else
              oli.Year2Revenue_edit__c = 0;
          
          oli.Year3Revenue_edit__c = oli.unitprice - (Math.round(oli.Year2Revenue_edit__c)) - Math.round(oli.CurrentYearRevenue_edit__c);
          
    }
    
    
    /*
     * @Description :This method  saves Opportunity Product
     * @ Args       : null   
     * @ Return     : void
     */
    public pageReference saveRecord(){
        try{
            update oli;
            if(returl == null)    
                return new PageReference('/'+oli.id); 
            else
                return new PageReference(returl); 
        }
        catch(System.DmlException e){ 
                 
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0))); 
                return null;
        }
    }
    
     /*
     * @Description :This method cancels the current Opportunity Product page 
     * @ Args       : null   
     * @ Return     : void
     */
     public pageReference cancel()
     {
        pageReference pageRef = new pageReference('/'+oli.Id);
        return pageRef;
     }


}