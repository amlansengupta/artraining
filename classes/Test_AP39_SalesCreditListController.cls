/*
Purpose: This test class provides data coverage to AP39_SalesCreditListController class
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    Shashank 03/29/2013  Created Test class
=============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP39_SalesCreditListController 
{
    /*
     * @Description : Test method for Mass Edit of Sales Credit.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '1234447912';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();   
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.EMPLID__c = '122255890';
        testColleague1.LOB__c = 'Health & Benefits';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        //testColleague1= [ Select Name, Last_Name__c, EMPLID__c, LOB__c, Empl_Status__c, Email_Address__c from Colleague__c where Empl_Status__c = 'Active' LIMIT 1];
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.Opportunity_Office__c = 'Honolulu - Fort';
        insert testOppty;
        
        test.stopTest();
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague.Id;
        testSales.Percent_Allocation__c = 10;
        insert testSales;
        
              
        
        
        system.runAs(User1){
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            AP39_SalesCreditListController controller = new AP39_SalesCreditListController(stdController);
            controller.salesCreditList[0].Percent_Allocation__c = 20;
            controller.save();
            controller.calculate();
            controller.reload();
            controller.cancel() ;
        }
    }
}