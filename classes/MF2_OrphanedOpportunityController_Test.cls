/***************************************************************************************************************
Request Id : 15559
Purpose : This Test Class is created for the Apex Class MF2_OrphanedOpportunityController
Created by : Archisman Ghosh
Created Date : 22/01/2019
Project : MF2
****************************************************************************************************************/
@isTest(seeAllData = false)
public class MF2_OrphanedOpportunityController_Test {
    public static testmethod void customLinksTestPositive(){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        List<DailyCleanse__mdt> metaList = [Select DeveloperName,UI_Label__c,IsActive__c,Link__c,Display_In_New_Window__c,
                                  Threshold__c,Query__c from DailyCleanse__mdt where IsActive__c = true];
        system.debug(metaList);
        System.runAs(user1) {
            List<DailyCleanse__mdt> dailyCleanseList = MF2_OrphanedOpportunityController.fetchDailyAlertDetails();
            
            MF2_OrphanedOpportunityController.fetchRecordCount('Orphaned_Opportunity');
           // MF2_OrphanedOpportunityController.replacePlaceholders(customLinks.Query__c, customLinks.Threshold__c);
        }
    }
}