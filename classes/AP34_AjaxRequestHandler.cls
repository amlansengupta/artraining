/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Arijit Roy 03/29/2013  Created AjaxHandler class
=============================================================================================================================================== 
*/
global class AP34_AjaxRequestHandler {
    
    /*
     *  @Method Name: lockOpportunity
     *  @Description: Method gets the Id and Name from opportunity record type and Updates them to locked Opportunity
     *  @Args : String
     *  @return: void  
     */
    webservice static string lockOpportunity(String oppId)
    {
        String lockedRecordType = [Select Id, Name From RecordType Where SObjectType = 'Opportunity' AND Name = 'Opportunity Locked' limit 1].Id;
        Opportunity opp = new Opportunity(Id = oppId);
        opp.RecordTypeId = lockedRecordType;
        opp.OppLockFlagIndv__c = true;
       
       String retString = 'success';
  try{
     update opp;
  }catch(Exception e){
      if(string.valueOf(e.getMessage()).contains(','))
    retString = 'ERROR ' + string.valueOf(e.getMessage()).substringAfter(','); 
  }
   
  return retString;

    }
    /*
     *  @Method Name: unlockOpportunity
     *  @Description: Method gets the Id and Name from opportunity record type and Updates them to Unlocked Opportunity
     *  @Args : String
     *  @return: void  
     */
    webservice static string unlockOpportunity(String oppId)
    {
        String unlockedRecordType = [Select Id, Name From RecordType where SObjectType = 'Opportunity' AND Name = 'Opportunity Detail Page Assignment' Limit 1].Id;
        
        Opportunity opp = new Opportunity(Id = oppId);
        opp.RecordTypeId = unlockedRecordType;
        opp.OppLockFlagIndv__c = true;
      
       String retString = 'success';
  try{
     update opp;
  }catch(Exception e){
      if(string.valueOf(e.getMessage()).contains(','))
    retString = 'ERROR ' + string.valueOf(e.getMessage()).substringAfter(','); 
  }
   
  return retString;
    }
    /*
     *  @Method Name: unlockOpportunity
     *  @Description: Method gets the Id and Name from opportunity record type and Updates them to lock Opportunity
     *  @Args : String
     *  @return: void  
     */
    webservice static string lockExpansionOpportunity(String oppId)
    {
        String lockedRecordType = [Select Id, Name From RecordType where SObjectType = 'Opportunity' AND Name = 'Opportunity Product Expansion Locked' Limit 1].Id;
        
        Opportunity opp = new Opportunity(Id = oppId);
        opp.RecordTypeId = lockedRecordType;
        opp.OppLockFlagIndv__c = true;
       
       String retString = 'success';
  try{
     update opp;
  }catch(Exception e){
      if(string.valueOf(e.getMessage()).contains(',')) retString = 'ERROR ' + string.valueOf(e.getMessage()).substringAfter(','); 
   
  } return retString;
   
 
    }
    /*
     *  @Method Name: unlockOpportunity
     *  @Description: Method gets the Id and Name from opportunity record type and Updates them to Unlocked Opportunity
     *  @Args : String
     *  @return: void  
     */
    webservice static string unlockExpansionOpportunity(String oppId)
    {
        String unlockedRecordType = [Select Id, Name From RecordType where SObjectType = 'Opportunity' AND Name = 'Opportunity Product Expansion' Limit 1].Id;
        
        Opportunity opp = new Opportunity(Id = oppId);
        opp.RecordTypeId = unlockedRecordType;
       
       String retString = 'success';
  try{
     update opp;
  }catch(Exception e){ if(string.valueOf(e.getMessage()).contains(',')) retString = 'ERROR ' + string.valueOf(e.getMessage()).substringAfter(','); 
}return retString;
    }
    
}