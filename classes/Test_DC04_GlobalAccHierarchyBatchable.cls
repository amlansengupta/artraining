/*Purpose: This test class provides data coverage to DC04_GlobalAccHierarchyBatchable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Srishty 05/09/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_DC04_GlobalAccHierarchyBatchable{    

	/*
     * @Description : Test method to provide data coverage to DC04_GlobalAccHierarchyBatchable class 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
         
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.DUNS__c = '1009911';        
        Acc.MercerForce_Account_Status__c ='Active Marketing Contact';
        insert Acc;
        
        
        Account Acc1 = new Account();
        Acc1.Name = 'TestAccountName';
        Acc1.BillingCity = 'City';
        Acc1.BillingCountry = 'Country';
        Acc1.BillingStreet = 'Street';
        Acc1.Relationship_Manager__c = Coll.Id;
        Acc1.One_Code__c = '123';
        acc1.gu_duns__c = '1009910';    
        aCC1.HQ_Immediate_Parent_DUNS__c ='1009911';
        Acc1.MercerForce_Account_Status__c ='Active Marketing Contact';
        insert Acc1;        
        Contact Con= new Contact();
        
        Con.LastName ='TestContactLastName';
        Con.AccountId = Acc.id;
        Con.FirstName ='TestContactFirstName';
        Con.Contact_ID__c = '896754231';
        Con.email = 'xyzq@abc11.com'; 
        insert Con;
        
        /*distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Distribution';
        distribution.Scope__c = 'Office';        
        distribution.Office__c = 'New Delhi';        
        insert distribution; 
        
        Contact_Distributions__c ConDistributions = new Contact_Distributions__c();
        ConDistributions.Contact__c = Con.Id;
        ConDistributions.Distribution__c = distribution.Id;
        insert ConDistributions;*/
        system.runAs(User1){
        database.executeBatch(new DC04_GlobalAccountHierarchyBatchable(), 100);
        }
    }


}