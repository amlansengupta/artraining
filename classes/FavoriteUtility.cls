public with sharing class FavoriteUtility{
    
    public static String getRestResponse(String returl, String activeSessionId) {  
        String sessionId = '';
        if(activeSessionId == null || String.isEmpty(activeSessionId)){
            // Refer to the Page
            PageReference reportPage = Page.GetSessionIdVF;
            // Get the content of the VF page
            String vfContent = reportPage.getContent().toString();
            System.debug('vfContent '+vfContent);
            System.debug('user session '+userInfo.getSessionId());
            // Find the position of Start_Of_Session_Id and End_Of_Session_Id
            Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length();
            Integer endP = vfContent.indexOf('End_Of_Session_Id');
            // Get the Session Id
            sessionId = vfContent.substring(startP, endP);
        }else{
            sessionId = activeSessionId;
        }
        system.debug('session id '+sessionId);
        HttpRequest httpRequest = new HttpRequest();  
        //httpRequest.setEndpoint(url);  
        httpRequest.setMethod('GET'); 
        httpRequest.setHeader('Authorization', 'Bearer ' + sessionId);
        httpRequest.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v44.0/ui-api/favorites');
        //httpRequest.setHeader('Content-Type', 'application/json');
        //httpRequest.setHeader('Accept','application/json');
        ///httpRequest.setHeader('Authorization', 'Bearer {!$Favorite_Credens.OAuthToken}');
        //httpRequest.setHeader('APIKEY', '{!$Credential.Password}');
        /* 2. set the auth token */
        //httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());        
        //httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());  
        try {  
            Http http = new Http();  
            /* initiate the actual call */  
            HttpResponse httpResponse = http.send(httpRequest);
            system.debug('httpResponse.getBody():'+JSON.deserializeUntyped(httpResponse.getBody()));
            if (httpResponse.getStatusCode() == 200 ) {  
                return JSON.serializePretty( JSON.deserializeUntyped(httpResponse.getBody()) );  
            } else {  
                System.debug(' httpResponse ' + httpResponse.getBody() );  
                throw new CalloutException( httpResponse.getBody() );  
            }   
        } catch( System.Exception e) {  
            System.debug('ERROR: '+ e);  
            throw e;  
        }  
    }
    
    public static List<FavoriteEntity> fetchFavorites(String activeSessionId){
        String limitsUrl = 'callout:Favorite_Credens/services/data/v44.0/ui-api/favorites'; 
        String response = getRestResponse(limitsUrl, activeSessionId); 
        List<FavoriteEntity> favs = new List<FavoriteEntity>();
        JSONParser parser = JSON.createParser(response);
        while (parser.nextToken() != null) {
            // Start at the array of invoices.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    // Advance to the start object marker to
                    //  find next invoice statement object.
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        // Read entire invoice object, including its array of line items.
                        try{
                            FavoriteEntity fav=(FavoriteEntity)parser.readValueAs(FavoriteEntity.class);
                            favs.add(fav);
                            system.debug('Invoice number: ' + fav.objectType);
                            system.debug('Size of list items: ' + fav.target);
                        }catch(Exception ex){
                            system.debug('parsing error:'+ex);
                            ExceptionLogger.logException(ex, 'FavoriteUtility', 'fetchFavorites');
                        }
                        // Skip the child start array and start object markers.
                        parser.skipChildren();
                    }
                }
            }
        }
        System.debug(' -- response-- : ' + response ); 
        return favs;
    }
    
    public class FavoriteEntity{
        public String objectType{get;set;}
        public Id target{get;set;}
        public FavoriteEntity(String objectType, Id target){
            this.objectType = objectType;
            this.target = target;
        }
    }
}