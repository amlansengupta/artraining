/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

global class SVNSUMMITS_WrapperNetwork {

    @AuraEnabled
    global Integer intLikeReceived                  {get;set;}          // This is used for storing the number of likes associated with user
    @AuraEnabled
    global Integer intNumberOfFollowers             {get;set;}          // This is used for storing the number of followers associated with user

    @AuraEnabled
    global String strKnowledgeTopics                 {get;set;}         // This is used for storing the topics associated with record
    @AuraEnabled
    global String strKnowledgeTopics1                {get;set;}         // This is used for storing the topics associated with record
    @AuraEnabled
    global String strKnowledgeTopics2                {get;set;}         // This is used for storing the topics associated with record

    @AuraEnabled
    global Id strKnowledgeTopicId                 {get;set;}         // This is used for storing the topicsId associated with record
    @AuraEnabled
    global Id strKnowledgeTopicId1                {get;set;}         // This is used for storing the topicsId associated with record
    @AuraEnabled
    global Id strKnowledgeTopicId2                {get;set;}         // This is used for storing the topicsId associated with record



    @AuraEnabled
    global User objUser                              {get;set;}            // This is used for storing the data associated with user


    global SVNSUMMITS_WrapperNetwork(User objUser) {

        this.objUser = objUser;
        this.intLikeReceived = intLikeReceived;
        this.intNumberOfFollowers = intNumberOfFollowers;
        this.strKnowledgeTopics = strKnowledgeTopics;
        this.strKnowledgeTopics1 = strKnowledgeTopics1;
        this.strKnowledgeTopics2 = strKnowledgeTopics2;
        this.strKnowledgeTopicId = strKnowledgeTopicId;
        this.strKnowledgeTopicId1 = strKnowledgeTopicId1;
        this.strKnowledgeTopicId2 = strKnowledgeTopicId2;
    }

}