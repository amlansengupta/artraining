/*Purpose:  This Apex class is created as a Part of March Release for Req:2877
==============================================================================================================================================
The methods called perform following functionality:
•   Query all reports which are not Run from last 90 days.
•   Capture Report id against a Created Id.
•   Send chatter post to Report Owner.

History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/18/2014  Created Batch class to send a Chatter post to user before Report deletion.
   1.1 -    Jagan   03/06/2014  Updated class variables and code alignment 
   1.2 -    Jagan   03/06/2014  Added Database.stateful and declared ownerid set outside
   1.3 -    Jagan   03/06/2014  Updated execute method code to loop on current batch instead of set
   1.4 -    Ravi    03/10/2014  Added fetchingLimit to Limit Records while running Test Class
============================================================================================================================================== 
*/
global class AP85_postChatterwithReportinfo implements Database.Batchable < sObject >, Database.Stateful {
    Private final String query;
    // List variable to store error logs For Account
    Private List < BatchErrorLogger__c > errorLogs_Reportchatter = new List < BatchErrorLogger__c > ();
    private set<Id> ownerIdSet= new set<Id> ();
    
    //Constructor of batch class
    global AP85_postChatterwithReportinfo() {
        Integer fetchingLimit= Test.isRunningTest()? 5: Limits.getLimitQueryLocatorRows();
        /*Jagan: commented below line as the query is placed in custom label **/
        //query = 'Select OwnerId,Id,CreatedById from Report where OwnerId=' + '\'' + Label.Unfiledpublicfolderid + '\'' + ' and CreatedDate < LAST_N_DAYS:' + Label.ChatterforReportDate + '  LIMIT ' + fetchingLimit;
        query = Label.ReportPurgeQuery + '  LIMIT ' + fetchingLimit;
    }

    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /*
     *  Method Name: execute
     *  Description: Method is used push Chatterposts to users .
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext BC, List < Report > rList) {
        
        List < FeedItem > feedList = new List < FeedItem > ();
        Integer repErrorCount = 0;
        FeedItem fitem ;
        for(Report r: rList){
            if(!ownerIdSet.contains(r.CreatedById)){
                ownerIdSet.add(r.CreatedById);
                fitem = new FeedItem();
                fitem.createdbyid = Label.MercerForceUserid;
                fitem.body = label.ChatterpostforReport;
                fitem.parentid = r.CreatedById;
                //fitem.parentid ='005M0000005dDCn';
                feedList.add(fitem);
            }
        }
        // List variable to save the result of the FeedItem Object which are updated   
        List < Database.SaveResult > repChList = Database.Insert(feedList, false);

        if (repChList != null && repChList.size() > 0) {
            // Iterate through the saved chatter object 
            Integer i = 0;
            for (Database.SaveResult sr: repChList) {
                // Check if feeditem object are not inserted successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) {
                    repErrorCount++;
                    errorLogs_Reportchatter = MercerAccountStatusBatchHelper.addToErrorLog('AP85:' + sr.getErrors()[0].getMessage(), errorLogs_Reportchatter, feedList[i].Id);
                }
                i++;
            }
        }

        //Update the status of batch in BatchLogger object
        List < BatchErrorLogger__c > repErrorLogStatus = new List < BatchErrorLogger__c > ();
        if (rList.size() > 0) {
            repErrorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('AP85 Status :rList:' + rList.size() + ' feedList:' + feedList.size() + +' Total Errorlog Size: ' + errorLogs_Reportchatter.size() + ' Batch Error Count :' + repErrorCount, repErrorLogStatus, rList[0].Id);
            insert repErrorLogStatus;
        }
        
    }

    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records . 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC) {

        
        
        //If Error Logs exist
        if (errorLogs_Reportchatter.size() > 0) {
            //Insert to ErrorLogs
            Database.insert(errorLogs_Reportchatter, false);
        }
    }

}