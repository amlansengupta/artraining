/*Purpose: This Apex class implements schedulable interface to schedule AP06_AccountMergeBatchable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   12/24/2012  Created Apex Schedulable class
============================================================================================================================================== 
*/

global class AP05_AccountMergeSchedulable implements Schedulable {
    
    //final string to hold the base query
    global final String baseQuery = 'SELECT Id, Account_ID__c, Type, Auto_Merge_Date__c,Duplicate_Client_Code_Rejected__c,Auto_Merge_Flag__c,Auto_Merge_Status__c, Inactivation_Code__c, One_Code_Status__c,One_Code__c, Workflow_Message__c, Duplicated_Client_Code__c, Workflow_Submit_Status__c, (SELECT Account__c,CurrencyIsoCode,Currency__c,From_Date__c,High__c,Id,IsDeleted,Low__c,Name,Realistic__c,SystemModstamp,Target_ID__c,To_Date__c FROM Revenue_Target__r), (SELECT Account__c,At_Risk_Reason__c,At_Risk_Type__c,CurrencyIsoCode,Date_Closed__c,Date_Opened__c,Due_Date__c,Id,Impact__c,IsDeleted,LOB__c,Name,Resolution_Plan__c,Responsibility__c,SystemModstamp,Why_at_Risk__c FROM Sensitve_At_Risk__r), (SELECT Account__c,Annual_Contract_Value__c,Assets_Under_Mgmt__c,Client_Since__c,Contract_Expiration_Date__c,Contract_Negotiation_Date__c,Contract_Start_Date__c,CurrencyIsoCode,Current_Pricing__c,Id,IsDeleted,Name,of_Lives__c,of_Plans__c,Outsourcing_Service_Center__c,Referenceable__c,Reference_Approval_Date__c,Satisfaction_Date__c,Satisfaction_Ratingg__c,SystemModstamp,Total_Contract_Value__c FROM Contracts_Renewals__r), (SELECT Account__c,Amount__c,CurrencyIsoCode,Id,IsDeleted,Name,Name__c,Sort__c,SystemModstamp,Type__c FROM Account_Revenues__r), (SELECT Account__c,Amount_USD__c,Amount__c,CurrencyIsoCode,Id,IsDeleted,Name,Referral_Description__c,Referral_To_Manager__c,Referral_To__c,SystemModstamp FROM Cross_OpCo_Referrals__r), (SELECT Account__c,Contact_Type__c,Contact__c,CurrencyIsoCode,Id,IsDeleted,Name,SystemModstamp FROM Employee_History__r), (SELECT Account__c,Comments__c,CurrencyIsoCode,Id,Incumbent_Service_Provider__c,IsDeleted,LOB__c,Name,Source_of_Info__c,SystemModstamp FROM Incumbents1__r), (Select Id, Name, StageName, Opportunity_Market__c, Opportunity_Office__c, Opportunity_Region__c, Opportunity_Sub_Market__c, Opportunity_Sub_Region__c FROM Opportunities), (SELECT AccountAccessLevel,AccountId,Id,IsDeleted,SystemModstamp,TeamMemberRole,UserId FROM AccountTeamMembers), (select Id, Name, AccountId from Contacts), (select Id, AccountId, whatId, OwnerId from Tasks), (select Id, AccountId, whatId, OwnerId from Events),(SELECT Body,CreatedById,CreatedDate,Id,IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,OwnerId,ParentId,SystemModstamp,Title FROM Notes) FROM Account';
    
    // final string to hold the where clause
    global final String whereClause = ' WHERE Auto_Merge_Date__c <= TODAY AND Auto_Merge_Flag__c = true AND One_Code_Status__c=\'Active\'';
    
    // final string to hold the sortclause
    global final String sortClause = ' ORDER BY One_Code__c';
    
    // query string build from basequery + whereclause + sortclause
    global String queryStr;
    
    // constructor for the class
    global AP05_AccountMergeSchedulable()
    {
        this.queryStr = baseQuery + whereClause + sortClause;   
    }
    
    // schedulable execute method
    global void execute(SchedulableContext SC) 
    {       
        // call database execute method to run AP06 batch class
        database.executeBatch(new AP06_AccountMergeBatchable(queryStr), Integer.valueOf(system.label.CL01_AutoMergeBatchSize)); 
    }
}