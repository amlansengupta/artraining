public class AP_BuyingInfluenceCustomPage_Extension  {
    Private ApexPages.StandardController stdControl;
    private Buying_Influence__c oppName{get;set;}
    public string accid {get;set;}
    public string oppId {get;set;}
    public AP_BuyingInfluenceCustomPage_Extension(ApexPages.StandardController cont){
        accid =  ApexPages.currentPage().getParameters().get('aid');
        oppId =ApexPages.currentPage().getParameters().get('oid');
        this.stdControl = cont;
        oppName =  (Buying_Influence__c) stdControl.getRecord();
        oppName.Opportunity__c = ApexPages.currentPage().getParameters().get('oid');
        oppName.OwnerId = UserInfo.getUserId();
    }
    
        
    public PageReference save(){
        try{
            stdControl.save();
        } 
        catch(DMLException e){
            System.debug('Error Occured: '+e);
        }
        PageReference pg = new PageReference('/'+oppId);
        pg.setRedirect(true);
        return pg;
	}
    public PageReference cancel(){
        PageReference pg = new PageReference('/'+oppId);
        pg.setRedirect(true);
        return pg;
    }
    public PageReference saveAndNew(){
        stdControl.save();
        
        String currentURL =ApexPages.currentPage().getURL();
        PageReference pg = new PageReference(currentURL);
        pg.setRedirect(true);
        return pg;
        
    }
}