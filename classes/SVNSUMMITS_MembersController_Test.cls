/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

/*
    @Class Name          : SVNSUMMITS_MembersController_Test
    @Created by          :
    @Description         : Apex Test class for SVNSUMMITS_MembersController
*/

@isTest
private class SVNSUMMITS_MembersController_Test {

    User objTestUser;

    private static testMethod void testAllMethods() {

        SVNSUMMITS_MembersController_Test test1 = new SVNSUMMITS_MembersController_Test();

        //TestSetupData
        test.starttest();
        test1.objTestUser = SVNSUMMITS_TestUtil.createUsers(8, SVNSUMMITS_TestUtil.COMPANY_COMMUNITY_PROFILE_NAME)[0];

        system.runAs(test1.objTestUser) {

            Decimal memberCount   = SVNSUMMITS_MembersController.getMemberCount();
            system.debug('-------------------------------------> memberCount:' + memberCount);

            SVNSUMMITS_MembersController.networkId = SVNSUMMITS_TestUtil.NETWORK_ID;
            system.assertNotEquals(SVNSUMMITS_MembersController.networkId, null);

            //To test the methods in members controller class
            SVNSUMMITS_WrapperMembers membrs = SVNSUMMITS_MembersController.getMembers(8, 'Sort by First Name', '', 'Test');  //on load default values & to check only the test records.
            system.assertNotEquals(membrs.membersList, null);
            system.assertEquals(membrs.listSizeValue, 8);
            system.assertEquals(membrs.membersList[0].Name, '0Test 0Test');

            SVNSUMMITS_WrapperMembers nxtmembrs = SVNSUMMITS_MembersController.nextPage(4, 1, 'Sort by First Name', '', 'Test');
            system.assertEquals(2, nxtmembrs.pageNumber);
            system.assertEquals(4, nxtmembrs.listSizeValue);
            //system.assertEquals(14, nxtmembrs.totalResults);

            SVNSUMMITS_WrapperMembers prevmembrs = SVNSUMMITS_MembersController.previousPage(4, 2, 'Sort by First Name', '', 'Test');
            system.assertEquals(1, prevmembrs.pageNumber);
            system.assertEquals(4, prevmembrs.listSizeValue);
            //system.assertEquals(14, prevmembrs.totalResults);

            SVNSUMMITS_MembersController.getSitePrefix();
            SVNSUMMITS_MembersController.getFeaturedMembers(
                    membrs.membersList[0].communitynickname,
                    membrs.membersList[1].communitynickname,
                    membrs.membersList[2].communitynickname,
                    membrs.membersList[3].communitynickname,
                    membrs.membersList[4].communitynickname,
                    membrs.membersList[5].communitynickname,
                    membrs.membersList[6].communitynickname,
                    membrs.membersList[7].communitynickname);

            //To cover the if - else conditions
            SVNSUMMITS_WrapperMembers nxtmembrs1 = SVNSUMMITS_MembersController.nextPage(4, 1, 'Sort by First Name', 'Members I Follow', 'Test');
            //SVNSUMMITS_WrapperMembers nxtmembrs2 = SVNSUMMITS_MembersController.nextPage(4, 1, 'Sort by Last Name', 'Members I Follow', 'Test');

            SVNSUMMITS_WrapperMembers prevmembrs1 = SVNSUMMITS_MembersController.previousPage(4, 2, 'Sort by First Name', 'Members I Follow', 'Test');
            //SVNSUMMITS_WrapperMembers prevmembrs2 = SVNSUMMITS_MembersController.previousPage(4, 2, 'Sort by Last Name', 'Members I Follow', 'Test');
            test.stoptest();
            //SVNSUMMITS_WrapperMembers membrs0 = SVNSUMMITS_MembersController.getMembers(7, 'Sort by First Name', 'Members I Follow', 'Test');
            //system.assertEquals(membrs0.membersList[6].Name, '7Test 7Test');

            SVNSUMMITS_WrapperMembers membrs1 = SVNSUMMITS_MembersController.getMembers(7, 'Sort by Last Name', 'Members I Follow', 'Test');
            system.assertEquals(membrs1.membersList[6].Name, '7Test 7Test');

            SVNSUMMITS_WrapperMembers membrs2 = SVNSUMMITS_MembersController.getMembers(8, 'Sort by First Name', '', 'Test');
            system.assertEquals(membrs2.membersList[0].Name, '0Test 0Test');

            SVNSUMMITS_WrapperMembers membrs3 = SVNSUMMITS_MembersController.getMembers(8, 'Sort by Last Name', '', 'Test');
            system.assertEquals(8, membrs3.membersList.size());

        }
    }

    private static testMethod void testCount() {
        SVNSUMMITS_MembersController_Test test1 = new SVNSUMMITS_MembersController_Test();
        SVNSUMMITS_MembersController.networkId = SVNSUMMITS_TestUtil.NETWORK_ID;
        system.assertNotEquals(SVNSUMMITS_MembersController.networkId, null);

        //TestSetupData
        test1.objTestUser = SVNSUMMITS_TestUtil.createUsers(8, SVNSUMMITS_TestUtil.COMPANY_COMMUNITY_PROFILE_NAME)[0];

        system.runAs(test1.objTestUser) {
            // total count
            Decimal memberCount = SVNSUMMITS_MembersController.getMemberCount();
            system.debug('Member count = ' + memberCount);
            //system.assertEquals(26, memberCount);
        }
    }

    private static testMethod void testExcludedMembers() {
        SVNSUMMITS_MembersController_Test test1 = new SVNSUMMITS_MembersController_Test();
        SVNSUMMITS_MembersController.networkId = SVNSUMMITS_TestUtil.NETWORK_ID;
        system.assertNotEquals(SVNSUMMITS_MembersController.networkId, null);

        //TestSetupData
        test1.objTestUser = SVNSUMMITS_TestUtil.createUsers(8, SVNSUMMITS_TestUtil.COMPANY_COMMUNITY_PROFILE_NAME)[0];

        system.runAs(test1.objTestUser) {
            Decimal memberCount   = SVNSUMMITS_MembersController.getMemberCount();

            List<String> excluded = new List<String>();
            // test with excluded IDs
            SVNSUMMITS_WrapperMembers membrs = SVNSUMMITS_MembersController.getMembers(8, 'Sort by Last Name', '', 'Test');

            excluded.add(membrs.membersList[0].Id);
            excluded.add(membrs.membersList[1].Id);
            SVNSUMMITS_WrapperMembers membrs1 = SVNSUMMITS_MembersController.getMembersEx(6, 'Sort by Last Name', '', 'Test', excluded);
            system.assertEquals(6, membrs1.membersList.size());
            system.assertEquals(membrs1.membersList[0].Name, '2Test 2Test');
            SVNSUMMITS_WrapperMembers nxtmembrs4 = SVNSUMMITS_MembersController.nextPageEx(4, 1, 'Sort by Last Name', 'Members I Follow', 'Test', excluded);
            SVNSUMMITS_WrapperMembers prevmembrs4 = SVNSUMMITS_MembersController.previousPageEx(4, 2, 'Sort by Last Name', 'Members I Follow', 'Test', excluded);

            // total excluding Ids
            Decimal exMemberCount = SVNSUMMITS_MembersController.getMemberCountEx(excluded);
            system.debug('Member count (Ex) = ' + exMemberCount);
            system.assertEquals(memberCount-2, exMemberCount);
        }
    }

}