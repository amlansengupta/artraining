@isTest(seeAllData=true)
public class AP107_ScopeItCopyTaskandEmployees_2_test{
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData(); 

    static testMethod void testcopyModelTask(){

        String Lob='Benefits Admin';
        Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        Product2 testProduct =mtdata.buildScopeProduct(Lob) ;
        String prodId=testProduct.id;
        
        Scope_Modeling__c testSProject = mtscopedata.buildScopeModelProject(prodId);
        
        Scope_Modeling_Task__c testSProjectTask = mtscopedata.buildScopeModelTask();
        String empBillrate= Mercer_ScopeItTestData.getEmployeeBillRate();
        Scope_Modeling_Employee__c testSProjectEmp =mtscopedata.buildScopeModelEmployee(empBillrate);
       
        test.startTest();
        AP107_ScopeItCopyTaskandEmployees.copyModelingTask(testSProjectTask.id);
        test.stopTest(); 
    
    }
  

}