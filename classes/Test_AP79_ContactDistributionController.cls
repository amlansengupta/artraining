/* Purpose: This test class providescode coverage to create contact distribution record forthe contact.
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Jagan         1/10/2014   Created test method for AP79_ContactDistributionController class
 ==================================================================================================================*/ 
@isTest
private class Test_AP79_ContactDistributionController 
{
/*
 *  Method Name: myUnitTest
 *  Description: Method is used to check contactdistribution record  creation. 
 */
    static testMethod void myUnitTest() 
    {
       try
       {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
         User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
               
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Distribution__c dis = new Distribution__c();
        dis.Name = 'TestDistribution';
        dis.Scope__c = 'Country';
        dis.Country__c = 'Abc';
        
        insert dis;
        
        Distribution__c dis1 = new Distribution__c();
        dis1.Name = 'TestDistribution1';
        dis1.Scope__c = 'Country';
        dis1.Country__c = 'Abc';
        dis1.LOB__c = 'Retirement';
        dis1.Region__c = 'testRegion';
        dis1.Market__c = 'testMarket';
        insert dis1;
        
           
        Distribution__c dis2 = new Distribution__c();
        dis2.Name = 'TestDistribution2';
        dis2.Scope__c = 'Country';
        dis2.Country__c = 'Abc';
        insert dis2;
        
        Contact_Distributions__c conDis = new Contact_Distributions__c(); 
        conDis.Contact__c = testContact.Id;
        conDis.Distribution__c = dis.id;
        insert condis;
        
         Contact_Distributions__c conDis1 = new Contact_Distributions__c(); 
         conDis1.Contact__c = testContact.Id;
         conDis1.Distribution__c  = dis2.id;
         insert conDis1;
        
        
         system.runAs(User1){
         	       
         	
         Pagereference  pref = page.Mercer_ContactDistributionSearch;
         pref.getparameters().put('id',testContact.id);
         Test.setCurrentpage(pref);
         
         ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
         AP79_ContactDistributionController controller = new AP79_ContactDistributionController(stdController);
         

         controller.distributionAll[0].checked = true;              
         controller.SelectedDB();
         controller.Savebutton();
         
         controller.searchParam = dis1.name;                         
         controller.runSearch();
                                   
   
         controller.PR1  = 'Equals';
         controller.PR2  = dis1.LOB__c;
         controller.PR3  = 'Distribution LOB';
         controller.runSearch();
         
         controller.PR1  = 'Equals';
         controller.PR2  = dis1.Name;
         controller.PR3  = 'Distribution Name';
         controller.runSearch();
         
         controller.PR1  = 'Equals';
         controller.PR2  = dis1.Region__c;
         controller.PR3  = 'Distribution Region';
         controller.runSearch();
         
         controller.PR1  = 'Equals';
         controller.PR2  = dis1.Market__c;
         controller.PR3  = 'Distribution Market';
		 controller.runSearch();
		 
		 controller.PR1  = 'Contains';
         controller.PR2  = dis1.LOB__c;
         controller.PR3  = 'Distribution LOB';
         controller.runSearch();
         
         controller.PR1  = 'Contains';
         controller.PR2  = dis1.Name;
         controller.PR3  = 'Distribution Name';
         controller.runSearch();
         
         controller.PR1  = 'Contains';
         controller.PR2  = dis1.Region__c;
         controller.PR3  = 'Distribution Region';
         controller.runSearch();
         
         controller.PR1  = 'Contains';
         controller.PR2  = dis1.Market__c;
         controller.PR3  = 'Distribution Market';
		 controller.runSearch();
          
          
         controller.PR1  = 'Starts With';
         controller.PR2  = dis1.LOB__c;
         controller.PR3  = 'Distribution LOB';
         controller.runSearch();
         
         controller.PR1  = 'Starts With';
         controller.PR2  = dis1.Name;
         controller.PR3  = 'Distribution Name';
         controller.runSearch();
         
         controller.PR1  = 'Starts With';
         controller.PR2  = dis1.Region__c;
         controller.PR3  = 'Distribution Region';
         controller.runSearch();
         
         controller.PR1  = 'Starts With';
         controller.PR2  = dis1.Market__c;
         controller.PR3  = 'Distribution Market';
		 controller.runSearch();
         
         controller.clearSearch();
         
         controller.SelectedDB();         
         controller.ConDtListwrap[0].checked = true;
         Pagereference pageref2 = controller.Deletebutton();
         
         
         Pagereference pageref1 = controller.Savebutton();
         Pagereference pageref3 = controller.Cancelbutton();
         }
        
        }catch(Exception ex)       {
        System.debug(Ex.getMessage());                
       }
        
    }
}