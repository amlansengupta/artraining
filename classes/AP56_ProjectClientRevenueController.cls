/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
                            1.0 -    Shashank 03/29/2013  Created Controller Class.
=============================================================================================================================================== 
*/
public class AP56_ProjectClientRevenueController {

    public double sum{get; set;}
    public Opportunity opp{get; set;}
    public AP56_ProjectClientRevenueController(ApexPages.StandardController controller) {
    sum = 0;
    double sum1=0;
    double sum2=0; 
    double sum3=0;
    
    try
    {
        opp = (Opportunity)controller.getRecord();
        AggregateResult[] results = [select OpportunityId, SUM(Prorated_Year_1_Sales__c)proSales1, SUM(Prorated_Year_2_Sales__c)proSales2, SUM(Prorated_Year_3_Sales__c)proSales3 from OpportunityLineItem where OpportunityId = : Opp.Id Group By OpportunityId];
        if(results[0].get('proSales1')<>null)
        {sum1 = (double)results[0].get('proSales1');}
        if(results[0].get('proSales2')<>null)
        {sum2 = (double)results[0].get('proSales2');}
        if(results[0].get('proSales3')<>null)
        {sum3 = (double)results[0].get('proSales3');}
        sum = sum1+sum2+sum3;
        opp.Total_Prorated_Revenue__c = sum;
        opp.Revenue_Capture__c = (opp.Total_Project_Client_Rev_USD__c/opp.Total_Prorated_Revenue__c)*100;
    }catch(TriggerException tEx)
    {
        System.debug('Exception occured with reason :'+tEx.getMessage());    
    }catch(Exception ex)
    {
        System.debug('Exception occured with reason :'+Ex.getMessage());   
    }
    
    }
    
}