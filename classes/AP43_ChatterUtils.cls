/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
 							1.0 -    Arijit 03/29/2013  Created Utility Class.
=============================================================================================================================================== 
*/
global without sharing class AP43_ChatterUtils{

	public static void autoFollow(String recordId, String subscriberId)
	{
        try{
            if(!hasAutoFollowed(recordId, subscriberId))
            {
                EntitySubscription subscription = new EntitySubscription(parentId = recordId, subscriberId = subscriberId);
                insert subscription;
            }	
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'AP43_ChatterUtils', 'autoFollow');
        }
	}
	
	@future (callout = true)
	global static void chatterPostMention(String recordId, String recipientId, String message, String sessionId)
	{
		System.debug('\n Chatter Post Mention Message :'+message);
		String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
		String url = salesforceHost + '/services/data/v26.0/chatter/feeds/record/' + recordId + '/feed-items';
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(url);
		req.setHeader('Content-type', 'application/json');
		req.setHeader('Authorization', 'OAuth ' + sessionId);
		req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + recipientId + '" }, { "type": "text", "text" : "' + ' ' + message + '" } ] } }');
		Http http = new Http();
		System.debug('\n Chatter rest API request :'+req.getBody());
		if(!Test.isRunningTest())
		{
			HTTPResponse res = http.send(req);
			System.debug('Response from callout :'+res.getBody());
		}
	}
	
	public static boolean hasAutoFollowed(String recordId, String subscriberId)
	{
		boolean hasAutoFollowed = false;
		EntitySubscription[] subscriptions = [Select Id From EntitySubscription where parentId =: recordId AND subscriberId = : subscriberId LIMIT 1];
		if(subscriptions.size() > 0)
		{
			hasAutoFollowed = true;
		}
		return hasAutoFollowed;
		
	}
}