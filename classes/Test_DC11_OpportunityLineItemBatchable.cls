/* Purpose: This test class provides data coverage for updating the Product LOB and # of LOBs associated with 
            an Opportunity 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       07/23/2013  Created test method for DC11_OpportunityLineItemBatchable batch class
 1.1         Reetika         07/26/2013  Created test method for DC23_OpportunityLineItemSchedulable schedulable class
 ==================================================================================================================*/             
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData=true)
    private class Test_DC11_OpportunityLineItemBatchable 
    { 
        /* * @Description : This test method provides data coverage for DC11_OpportunityLineItemBatchable batch class             
           * @ Args       : no arguments        
           * @ Return     : void       
        */    
        static testMethod void testOpportunityLineItemBatchable()
        {
            // Test.StartTest();
   
                  /*ApexConstants__c apexCon = new ApexConstants__c();
        apexCon.Name ='Above the funnel';  
        apexCon.StrValue__c ='Marketing/Sales Lead;Executing Discovery;';
        Insert apexCon;
        ApexConstants__c apexCon1 = new ApexConstants__c();
        apexCon1.Name ='Selected';  
        apexCon1.StrValue__c ='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
        Insert apexCon1;
        
            ApexConstants__c apexCon2 = new ApexConstants__c();
        apexCon2.Name ='Identify';  
        apexCon2.StrValue__c ='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        Insert apexCon2;
            
            ApexConstants__c apexCon3 = new ApexConstants__c();
        apexCon3.Name ='Active Pursuit';  
        apexCon3.StrValue__c ='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        Insert apexCon3;
            
              ApexConstants__c apexCon4 = new ApexConstants__c();
        apexCon4.Name ='Finalist';  
        apexCon4.StrValue__c ='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        Insert apexCon4;
          
               ApexConstants__c apexCon5 = new ApexConstants__c();
        apexCon5.Name ='AP02_OpportunityTriggerUtil_2';  
        apexCon5.StrValue__c ='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        Insert apexCon5;
            
               ApexConstants__c apexCon6 = new ApexConstants__c();
        apexCon6.Name ='AP02_OpportunityTriggerUtil_1';  
        apexCon6.StrValue__c ='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;IPIB01;MDMK02;MLTI07;MMMF01;TPEO50;  ';
        Insert apexCon6;
            
               ApexConstants__c apexCon7 = new ApexConstants__c();
        apexCon7.Name ='ScopeITThresholdsList';  
        apexCon7.StrValue__c ='EuroPac:25000;Growth Markets:2500;North America:25000';
        Insert apexCon7;
            */
             Colleague__c testColleague = new Colleague__c();
             testColleague.Name = 'Test Colleague1';
             testColleague.EMPLID__c = '12545676201';       
             testColleague.LOB__c = '111214';       
             testColleague.Last_Name__c = 'test LastName';      
             testColleague.Empl_Status__c = 'Active';      
             testColleague.Email_Address__c = 'test@test.com';  
     
           //  insert testColleague;

             Account testAccount = new Account();
             testAccount.Name = 'Test Account1';
           //  testAccount.Relationship_Manager__c = testColleague.Id;  
             testAccount.BillingCity = 'TestCity1';   
             testAccount.BillingCountry = 'TestCountry1'; 
             testAccount.BillingStreet = 'Test Street1'; 
            
             insert testAccount;
             
             Test.StartTest();
             Opportunity testOpportunity = new Opportunity();
             testOpportunity.Name = 'Sample Opportunity1';
             //Request Id:12638 commenting step
             //testOpportunity.Step__c = 'Identified Deal';
             testOpportunity.StageName = 'Identify';
             testOpportunity.CloseDate = Date.Today();
             testOpportunity.AccountId = testAccount.ID;
             testOpportunity.CurrencyIsoCode = 'USD' ;
             testOpportunity.Type = 'New Client';
             testOpportunity.Product_LOBs__c= '';
             testOpportunity.Count_Product_LOBs__c= 0;
             testOpportunity.Opportunity_Office__c = 'Aarhus - Axel';         
             insert testOpportunity;
        	 Test.StopTest();
             Product2 testproduct = new Product2();
             testproduct.Name = 'Test product';
             testproduct.Family = 'H&B';
             testproduct.CurrencyIsoCode = 'USD' ;
             testproduct.LOB__c= 'Talent' ;
          
             insert testProduct;
            
            // Instantiate the Pricebook2 record first, setting the Id
Pricebook2 standardPricebook = new Pricebook2(
    Id = Test.getStandardPricebookId(),
    IsActive = true
);

// Run an update DML on the Pricebook2 record
// This is the weird workaround that enables IsStandard to become true
// on the PricebookEntry record
update standardPricebook;

// Re-Query for the Pricebook2 record, for debugging
standardPricebook = [SELECT IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];

// This should return true now
system.assertEquals(true, standardPricebook.IsStandard, 'The Standard Pricebook should now return IsStandard = true');





PricebookEntry testPbe = [SELECT Id, Pricebook2.IsStandard FROM PricebookEntry WHERE Pricebook2.IsActive = true and CurrencyIsoCode = 'USD' LIMIT 1];
  testPbe.IsActive = true;
            update testPbe;
             OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
             testOpportunityLineItem.UnitPrice = 4;
             testOpportunityLineItem.Duration__c = 'Multi-Year Open';
             testOpportunityLineItem.Revenue_Start_Date__c = Date.Today();
             testOpportunityLineItem.Revenue_End_Date__c = Date.Today() + 2;
             testOpportunityLineItem.OpportunityId = testOpportunity.Id;
             testOpportunityLineItem.PricebookEntryId = testPbe.Id;
    
             insert testOpportunityLineItem;
             
             User user1 = new User();
             String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
             user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
             system.runAs(User1){
                 Opportunity op= [Select Id,Name from Opportunity where Id= :testOpportunity.Id] ;                   
                 String query = 'Select id, Name, (Select id from OpportunityLineItems) from Opportunity where Id= \''+testOpportunity.Id +'\'' ;
                 database.executeBatch(new DC11_OpportunityLineItemBatchable(query),2000);
             }
             //Test.StopTest();
       }
       
       /*  * @Description : This test method provides data coverage for DC23_OpportunityLineItemSchedulable schedulable class             
           * @ Args       : no arguments        
           * @ Return     : void       
        */
       static testMethod void testOpportunityLineItemSchedulable()  
       { 
              
             User user1 = new User();
             String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
             user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
             system.runAs(User1){                       
                 DC23_OpportunityLineItemSchedulable usersch = new DC23_OpportunityLineItemSchedulable();      
                 DateTime r = DateTime.now();     
                 String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';       
                 Test.StartTest(); 
                 system.schedule('DC23_OpportunityLineItemSchedulable', nextTime, usersch);  
                 Test.StopTest(); 
             }         
               
       }       
   }