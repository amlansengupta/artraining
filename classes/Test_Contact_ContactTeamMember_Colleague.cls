/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
 * The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
/*Purpose: Test Class for providing code coverage to conatct and contact team  functionality
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   12/18/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_Contact_ContactTeamMember_Colleague  
{
    private static Mercer_TestData mtdata = new Mercer_TestData();
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
     private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890'+i;
        insert testUser;
        System.runAs(testUser){
        }
        return testUser;
    }
    
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void contactTest1()
    {
           
            User tUser = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '13333678902';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            
            System.runAs(tUser){          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'korea';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
                insert testAccount;
            
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName1';
            testAccount1.BillingCity = 'TestCity';
            testAccount1.BillingCountry = 'TestCountry1';
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.One_Code__c = '123456';
            insert testAccount1;
            
             Test.startTest();
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';
            testContact2.MailingCity  = 'TestCity2';
            testContact2.MailingCountry = 'Korea';
            testContact2.MailingState = 'TestState2'; 
            testContact2.MailingStreet = 'TestStreet2'; 
            testContact2.MailingPostalCode = 'TestPostalCode2';             
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount1.Id;
            insert testContact2;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'testcountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.OtherCountry = 'korea';
            testContact.Phone = '9999999999';
            testContact.Email = 'abc@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            insert testContact;
            
             
            
            Contact testContact1 = new Contact();
            testContact1.Salutation = 'Fr.';
            testContact1.FirstName = 'TestFirstName1';
            testContact1.LastName = 'TestLastName1';
            testContact1.Job_Function__c = 'TestJob1';
            testContact1.Title = 'TestTitle1';
            testContact1.MailingCity  = 'TestCity1';
            testContact1.MailingCountry = 'Korea';
            testContact1.MailingState = 'TestState1'; 
            testContact1.MailingStreet = 'TestStreet1'; 
            testContact1.MailingPostalCode = 'TestPostalCode1'; 
            testContact1.OtherCountry = 'testCountry';
            testContact1.Phone = '99999999991';
            testContact1.Email = 'abc@xyz.com';
            testContact1.MobilePhone = '99999999991';
            testContact1.AccountId = testAccount.Id;
            try
            {
                insert testContact1;
            }catch(DMLException e){
            system.assert(true, e.getMessage().contains('Contact with the same Email already exists. Please search for the following Contact ID: '));
            }
            
            
            
             Contact testContact3 = [select Id, Email from Contact where FirstName = 'TestFirstName' limit 1];
            testContact3.Email = 'abc1@xyz.com';
            try
            {
                update testContact3;
            }catch(DMLException e){
            system.assert(true, e.getMessage().contains('Contact with the same Email already exists. Please search for the following Contact ID: '));
            }
            
            testContact3 = [select Id, Title, Email, AccountId, Invalid_Work_Email__c  from Contact where FirstName = 'TestFirstName2' limit 1];
            testContact3.Title = 'TestTitle3';
            testContact3.Email = 'abc2@xyz.com';
            testContact3.AccountId = testAccount1.Id;
            testContact3.Invalid_Work_Email__c = true;
            update testContact3;               
            Test.stopTest();
            }
    }
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void contactTest1a()
    {   
        User user1 = new User();
        String systemAdmin = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(systemAdmin , 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'korea';
            testAccount.BillingStreet = 'Test Street';
            //testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
            insert testAccount;
            
      Contact testContact4 = new Contact();
            testContact4.Salutation = 'Fr.';
            testContact4.FirstName = 'TestFirstName';
            testContact4.LastName = 'TestLastName';
            testContact4.Job_Function__c = 'TestJob';
            testContact4.Title = 'TestTitle';
            testContact4.MailingCity  = 'TestCity';
            testContact4.MailingCountry = 'TestCountry';
            testContact4.MailingState = 'TestState'; 
            testContact4.MailingStreet = 'TestStreet'; 
            testContact4.MailingPostalCode = 'TestPostalCode'; 
            testContact4.Phone = '9999999999';
            testContact4.MobilePhone = '9999999999';
            testContact4.AccountId = testAccount.Id;
            testContact4.email = 'xyzq@abc12.com'; 
            insert testContact4;
            
            Contact testContact5 = new Contact();
            testContact5.Salutation = 'Fr.';
            testContact5.FirstName = 'TestFirstName';
            testContact5.LastName = 'TestLastName';
            testContact5.Job_Function__c = 'TestJob';
            testContact5.Title = 'TestTitle';
            testContact5.MailingCity  = 'TestCity';
            testContact5.MailingCountry = 'TestCountry';
            testContact5.MailingState = 'TestState'; 
            testContact5.MailingStreet = 'TestStreet'; 
            testContact5.MailingPostalCode = 'TestPostalCode'; 
            testContact5.Phone = '9999999999';
            testContact5.Email = 'abc5@xyz.com';
            testContact5.MobilePhone = '9999999999';
            testContact5.AccountId = testAccount.Id;
            insert testContact5;
            
            merge testContact4 testContact5;
         }
    }
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void contactTest2()
    {
           
            User tUser = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '123445452';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
                     
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.ShippingCity = 'TestCity';
            testAccount.ShippingCountry  = 'TestCountry';
            testAccount.ShippingStreet = 'Test Street';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingStreet  = 'Test Street';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
            /* Account testAccount1 = new Account();
             testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';
            testAccount1.BillingCountry  = 'TestCountry';
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            insert testAccount1;
            
            Account testAccount2 = new Account();
            testAccount2.Name = 'TestAccountName';
            testAccount2.BillingCity = 'TestCity';
            testAccount2.Country__c  = 'TestCountry';
            testAccount2.BillingStreet = 'Test Street';
            testAccount2.Relationship_Manager__c = testColleague.Id;
            insert testAccount2;*/
            
            User user1 = new User();
            String systemadmin = Mercer_TestData.getsystemAdminUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){
               
            Test.startTest();
            
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';           
            testContact2.OtherCountry = 'testothercountry';
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
           
            insert testContact2;
            Test.stopTest();
         
            
            //try {
            testContact2.MailingCountry = null;
             update testContact2;
            //} catch(DMLException e){
                //system.assert(true, e.getMessage().contains('Please provide the correct value for Country because Account Address couldnot be found. '));
            //} 
            testContact2.OtherCountry = 'korea';
            update testContact2;
            
            
       
           
          
            
           /* Contact testContact4 = new Contact();
            testContact4.Salutation = 'Fr.';
            testContact4.FirstName = 'TestFirstName4';
            testContact4.LastName = 'TestLastName4';
            testContact4.Job_Function__c = 'TestJob2';
            testContact4.Title = 'TestTitle2';
            testContact4.MailingCity  = null;
            testContact4.MailingCountry = 'testmailingCountry';
            testContact4.MailingState = null; 
            testContact4.MailingStreet = null; 
            testContact4.MailingPostalCode = null;
            testContact4.OtherCountry = 'testothercountry';
            testContact4.Phone = '99999999992';
            testContact4.Email = 'abc14@xyz.com';
            testContact4.MobilePhone = '99999999992';
            testContact4.AccountId = testAccount.Id;            
            insert testContact4;
            
            testContact4.OtherCountry = 'korea';
            update testContact4;*/
            
            Contact testContact3 = new Contact();
            testContact3.Salutation = 'Fr.';
            testContact3.FirstName = 'TestFirstName3';
            testContact3.LastName = 'TestLastName3';
            testContact3.Job_Function__c = 'TestJob2';
            testContact3.Title = 'TestTitle2';
            testContact3.MailingCity  = null;
            testContact3.MailingCountry = 'testmailingcountry';
            testContact3.MailingState = null; 
            testContact3.MailingStreet = null; 
            testContact3.MailingPostalCode = null;
            testContact3.OtherCountry = null;
            testContact3.Phone = '99999999992';
            testContact3.Email = 'abc13@xyz.com';
            testContact3.MobilePhone = '99999999992';
            testContact3.AccountId = testAccount.Id;
            insert testContact3;
            
            testContact3.MailingCountry = 'korea';
            update testContact3;
            
             Contact testContact5 = new Contact();
            testContact5.Salutation = 'Fr.';
            testContact5.FirstName = 'TestFirstName5';
            testContact5.LastName = 'TestLastName5';
            testContact5.Job_Function__c = 'TestJob2';
            testContact5.Title = 'TestTitle2';
            testContact5.MailingCity  = null;
            testContact5.MailingCountry = 'testmailingcountry';
            testContact5.MailingState = null; 
            testContact5.MailingStreet = null; 
            testContact5.MailingPostalCode = null;
            testContact5.OtherCountry = 'testOtherCountry';
            testContact5.Phone = '99999999992';
            testContact5.Email = 'abc15@xyz.com';
            testContact5.MobilePhone = '99999999992';
            testContact5.AccountId = testAccount.Id;
            insert testContact5;
            
            testContact5.MailingCountry = 'korea';
            update testContact5;
            
            //Test.stopTest();
            }
    }

/*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
  static testMethod void contactNegativeTest()
    {
           
            User tUser = new User();
            User tUser1 = new User();
            User tUser2 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '1234555902';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
            tUser2 = createUser(mercerStandardUserProfile, 'tUser2', 'tLastName2', tUser2, 4);          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.ShippingCity = 'TestCity';
            testAccount.ShippingCountry  = 'TestCountry';
            testAccount.ShippingStreet = 'Test Street';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingStreet  = 'Test Street';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
             Account testAccount1 = new Account();
             testAccount1.Name = 'TestAccountName';
            testAccount1.ShippingCity = 'TestCity';
            testAccount1.ShippingCountry  = 'TestCountry';
            testAccount1.ShippingStreet = 'Test Street';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.One_Code__c = '123456';
        	insert testAccount1;
            
            Test.startTest();
            
            System.RunAs(tUser) {
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';   
            testContact2.MailingCountry = 'testmailingcountry';        
            testContact2.OtherCountry = 'testothercountry';
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
            testContact2.ownerid = tUser.id;
            insert testContact2;
            
           
           testContact2.MailingCountry = null;
            try {   
                  
             update testContact2;
            }catch(DMLException e){
                system.assert(true, e.getMessage().contains('Please provide the correct value for Country because Account Address couldnot be found. '));
            } 
            
            Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
            testContactTeamMember.Contact__c =testContact2.Id;
            testContactTeamMember.Role__c = 'TestRole';
            testContactTeamMember.ContactTeamMember__c = tUser1.ID;
            insert testContactTeamMember;
            
              testContact2.ownerid = tUser1.id;
               update testContact2;
            
            }
            /*try {
            testContact2.ownerid =  tUser2.ID;
             update testContact2;
            } catch (Exception Ex) {
                system.assert(true, Ex.getMessage().contains('Operation Failed due to:'));
           
            }*/
        
    }
    
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void contactTest3()
    {
           
            User tUser = new User();
            User tUser1 = new User();
            User tUser2 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '112233445';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
            tUser2 = createUser(mercerStandardUserProfile, 'tUser2', 'tLastName2', tUser2, 4);          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry  = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Country__c  = 'TestCountry1';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';           
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Country__c  = 'TestCountry1';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.One_Code__c = '123456';
        	insert testAccount1;
            
            Test.startTest();
            
            System.RunAs(tUser) {
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';   
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
            testContact2.ownerid = tUser.id;
            insert testContact2;
            
            Contact testContact3 = new Contact();
            testContact3.Salutation = 'Fr.';
            testContact3.FirstName = 'TestFirstName3';
            testContact3.LastName = 'TestLastName3';
            testContact3.Job_Function__c = 'TestJob2';
            testContact3.Title = 'TestTitle2';   
            testContact3.Phone = '99999999992';
            testContact3.Email = 'abc12@xyz.com';
            testContact3.MobilePhone = '99999999992';
            testContact3.AccountId = testAccount1.Id;
            testContact3.ownerid = tUser.id;
            insert testContact3;
            
            Contact testContact4 = new Contact();
            testContact4.Salutation = 'Fr.';
            testContact4.FirstName = 'TestFirstName3';
            testContact4.LastName = 'TestLastName3';
            testContact4.Job_Function__c = 'TestJob2';
            testContact4.Title = 'TestTitle2';   
            testContact4.Phone = '99999999992';
            testContact4.Email = 'abc11@xyz.com';
            testContact4.MobilePhone = '99999999992';
            testContact4.MailingCountry = 'testmailingcountry';
            //testContact4.AccountId = testAccount1.Id;
            testContact4.ownerid = tUser.id;
            
            try {
            insert testContact4;
            } catch(DMLException e){
                system.assert(true, e.getMessage().contains('Please provide the correct value for Country because Account Address couldnot be found. '));
            }
            
            
            Contact testContact5 = new Contact();
            testContact5.Salutation = 'Fr.';
            testContact5.FirstName = 'TestFirstName3';
            testContact5.LastName = 'TestLastName3';
            testContact5.Job_Function__c = 'TestJob2';
            testContact5.Title = 'TestTitle2';   
            testContact5.Phone = '99999999992';
            testContact5.Email = 'abc123@xyz.com';
            testContact5.MobilePhone = '99999999992';
            testContact5.AccountId = testAccount1.Id;
            testContact5.ownerid = tUser.id;
            insert testContact5;
            
             testContact5.AccountId = testAccount.Id;
             update testContact5;
            }
            Test.stopTest();
   
    }
    
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void contactTest4()
    {
           
            User tUser = new User();
            User tUser1 = new User();
            User tUser2 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '124444902';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
            tUser2 = createUser(mercerStandardUserProfile, 'tUser2', 'tLastName2', tUser2, 4);          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry  = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Country__c  = 'TestCountry1';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';           
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Country__c  = 'TestCountry1';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.One_Code__c = '123456';
        	insert testAccount1;
            
            Test.startTest();
            
            System.RunAs(tUser) {
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';   
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
            testContact2.ownerid = tUser.id;
            insert testContact2;

            Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
            testContactTeamMember.Contact__c =testContact2.Id;
             testContactTeamMember.Role__c = 'TestRole';
            testContactTeamMember.ContactTeamMember__c = tUser1.ID;
            insert testContactTeamMember;
            
            Contact_Team_Member__c testContactTeamMember1 = new Contact_Team_Member__c();
            testContactTeamMember1.Contact__c =testContact2.Id;
              testContactTeamMember1.Role__c = 'TestRole';
            testContactTeamMember1.ContactTeamMember__c = tUser2.ID;
            insert testContactTeamMember1;
       
            }
            Test.stopTest();
   
    }
    
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void contactTest5()
    {
           
            User tUser = new User();
            User tUser1 = new User();
            User tUser2 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '12345099002';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
            tUser2 = createUser(mercerStandardUserProfile, 'tUser2', 'tLastName2', tUser2, 4);          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry  = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Country__c  = 'TestCountry1';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';           
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Country__c  = 'TestCountry1';
            testAccount1.Relationship_Manager__c = testColleague.Id;
        	testAccount1.One_Code__c = '123456';    
        	insert testAccount1;
            
            Test.startTest();
            
            System.RunAs(tUser) {
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';   
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
            testContact2.ownerid = tUser.id;
            insert testContact2;

            Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
            testContactTeamMember.Contact__c =testContact2.Id;
              testContactTeamMember.Role__c = 'TestRole';
            testContactTeamMember.ContactTeamMember__c = tUser1.ID;
            insert testContactTeamMember;
            try{
            Contact_Team_Member__c testContactTeamMember2 = new Contact_Team_Member__c();
            testContactTeamMember2.Contact__c =testContact2.Id;
              testContactTeamMember2.Role__c = 'TestRole';
            testContactTeamMember2.ContactTeamMember__c = tUser1.ID;
            insert testContactTeamMember2;
            }catch(Exception e){}
       
            }
            Test.stopTest();
   
    }
    
    /*
     * @Description : Test method to provide data coverage to contact and contact team functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void contactTest6()
    {
           
            User tUser = new User();
            User tUser1 = new User();
            User tUser2 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
           
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.EMPLID__c = '17645678902';
            testColleague.LOB__c = 'Health & Benefits';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            testColleague.Last_Name__c = 'TestlastName1';
            update testColleague;

            tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 2);
            tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 3);
            tUser2 = createUser(mercerStandardUserProfile, 'tUser2', 'tLastName2', tUser2, 4);          
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry  = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Country__c  = 'TestCountry1';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '123456';
        	insert testAccount;
            
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';           
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Country__c  = 'TestCountry1';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.One_Code__c = '123456';
        	insert testAccount1;
            
            Test.startTest();
            
            System.RunAs(tUser) {
            Contact testContact2 = new Contact();
            testContact2.Salutation = 'Fr.';
            testContact2.FirstName = 'TestFirstName2';
            testContact2.LastName = 'TestLastName2';
            testContact2.Job_Function__c = 'TestJob2';
            testContact2.Title = 'TestTitle2';   
            testContact2.Phone = '99999999992';
            testContact2.Email = 'abc1@xyz.com';
            testContact2.MobilePhone = '99999999992';
            testContact2.AccountId = testAccount.Id;
            testContact2.ownerid = tUser.id;
            insert testContact2;

            /*Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
            testContactTeamMember.Contact__c =testContact2.Id;
              testContactTeamMember.Role__c = 'TestRole';
            testContactTeamMember.ContactTeamMember__c = tUser.ID;
            insert testContactTeamMember;*/
            
            
       
            }
            Test.stopTest();
   
    }
        
        
        
   }