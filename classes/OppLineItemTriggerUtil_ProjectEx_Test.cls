/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
public class OppLineItemTriggerUtil_ProjectEx_Test {
    
    public static User testUser()
    {           
        String randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        User user = new User();
        user.alias = 'UTest';
        user.email = 'test@xyz.com';
        user.emailencodingkey='UTF-8';
        user.lastName = 'LastName';
        user.languagelocalekey='en_US';
        user.localesidkey='en_US';
        user.ProfileId = [Select Id,Name From Profile Where Name = 'Mercer Standard'][0].Id;
        user.timezonesidkey='Europe/London';
        user.UserName = randomName + '.' + user.lastName +'@xyz.com';
        user.EmployeeNumber = '1234567890';             
        insert user;         
        return user;
    }
    
    public static void createTestData(){        
        List<Project_Exception__c> peList = new List<Project_Exception__c>();
        
        Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Office';
        peOffice.Office__c = 'Aarhus';
        peList.add(peOffice);                
        
        Project_Exception__c peOneCode = new Project_Exception__c();
        peOneCode.Exception_Type__c = 'One Code';
        peOneCode.One_Code__c = 'ABC123XYZ';
        peList.add(peOneCode);
        
        Project_Exception__c peOneCode2 = new Project_Exception__c();
        peOneCode2.Exception_Type__c = 'One Code';
        peOneCode2.One_Code__c = 'ABC956XYZ';
       // peList.add(peOneCode2);
               
        
        Project_Exception__c peOpportunityType = new Project_Exception__c();
        peOpportunityType.Exception_Type__c = 'Opportunity Type';
        peOpportunityType.Opportunity_Type__c = 'Rebid';
        peList.add(peOpportunityType);
        
        Project_Exception__c peProductCode_Office = new Project_Exception__c();
        peProductCode_Office.Exception_Type__c = 'Product + Office Code';
        peProductCode_Office.Product_Code__c = '4126';
        peProductCode_Office.Office__c = 'Test Office 2';
        peList.add(peProductCode_Office);
        
        Project_Exception__c peProductCode = new Project_Exception__c();
        peProductCode.Exception_Type__c = 'Product Code';
        peProductCode.Product_Code__c = '9999';
        peList.add(peProductCode);
        
        Project_Exception__c peLOB = new Project_Exception__c();
        peLOB.Exception_Type__c = 'Product LOB';
        peLOB.LOB__c = 'Career';
        peList.add(peLOB);
        
        Project_Exception__c peRecordTypeName = new Project_Exception__c();
        peRecordTypeName.Exception_Type__c = 'RecordType Name';
        peRecordTypeName.Record_Type_Name__c = 'Opportunity_Detail_Page_Assignment';
        peList.add(peRecordTypeName);
        
        Project_Exception__c peProdOff = new Project_Exception__c();
        peProdOff.Exception_Type__c = 'Product + Office Code';
        peProdOff.Product_Code__c = '5642';
        peProdOff.Office__c = 'Shanghai - Huai Hai';
        peList.add(peProdOff);
        
        Project_Exception__c peTypeCounloncomName = new Project_Exception__c();
        peTypeCounloncomName.Exception_Type__c = 'Opportunity Type + Opportunity Country + Product LoB + Commission Flag';
        peTypeCounloncomName.Opportunity_Type__c = 'Existing Client - New LOB';
        peTypeCounloncomName.Opportunity_Country__c = 'Canada';
        peTypeCounloncomName.LOB__c = 'Health';
        peTypeCounloncomName.Commision__c= True;
        peList.add(peTypeCounloncomName);
        
        Project_Exception__c peTypeconProd = new Project_Exception__c();
        peTypeconProd.Exception_Type__c = 'Opportunity Type + Opp Country + Product Code';
        peTypeconProd.Opportunity_Type__c = 'Rebid';
        peTypeconProd.Opportunity_Country__c = 'Canada';
        peTypeconProd.Product_Code__c = '5642';
        peList.add(peTypeconProd);
        
        
        Project_Exception__c peTypeConComm = new Project_Exception__c();
        peTypeConComm.Exception_Type__c = 'Opportunity Country + LoB + Commission Flag';
        peTypeConComm.Opportunity_Type__c = 'Rebid';
        peTypeConComm.Opportunity_Country__c = 'Canada';
         peTypeConComm.Commision__c= True;
        peList.add(peTypeConComm);
        
        Project_Exception__c peConProd = new Project_Exception__c();
        peConProd.Exception_Type__c = 'Opportunity Country + Product code';
        peConProd.Product_Code__c = '5642';
        peConProd.Opportunity_Country__c = 'Canada';
        peList.add(peConProd);
        
        Database.insert(peList, false);
        
        //Account creation
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount; 
        
        Account testAccount2 = new Account();
        testAccount2.Name = 'TestAccountName2';
        testAccount2.BillingCity = 'TestCity2';
        testAccount2.BillingCountry = 'TestCountry2';
        testAccount2.BillingStreet = 'Test Street2';       
        testAccount2.One_Code__c='ABC956XYZ';
        insert testAccount2; 
        
        // Opportunity creation
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity 1';
        testOpportunity.Type = 'Rebid';
        testOpportunity.AccountId =  testAccount.Id; 
        //Request Id:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate =  Date.Today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        //testOpportunity.Close_Stage_Reason__c ='Other';        
        insert testOpportunity;
        
        Opportunity testOpportunity2 = new Opportunity();
        testOpportunity2.Name = 'Test Opportunity 1';
        testOpportunity2.Type = 'Rebid';
        testOpportunity2.AccountId =  testAccount2.Id; 
        //Request id:12638 commenting step
        //testOpportunity2.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity2.StageName = 'Identify';
        testOpportunity2.CloseDate =  Date.Today();
        testOpportunity2.CurrencyIsoCode = 'ALL';        
        testOpportunity2.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity2.Opportunity_Country__c = 'CANADA';
        testOpportunity2.currencyisocode = 'ALL';
        //testOpportunity2.Close_Stage_Reason__c ='Other';        
       Test.startTest();
        insert testOpportunity2;
         
        Opportunity testOpportunity3 = new Opportunity();
        testOpportunity3.Name = 'Test Opportunity 1';
        testOpportunity3.Type = 'Rebid';
        testOpportunity3.AccountId =  testAccount2.Id; 
        //request id:12638 commenting step
        //testOpportunity3.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity3.StageName = 'Identify';
        testOpportunity3.CloseDate =  Date.Today();
        testOpportunity3.CurrencyIsoCode = 'ALL';        
        testOpportunity3.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity3.Opportunity_Country__c = 'CANADA';
        testOpportunity3.currencyisocode = 'ALL';
        //testOpportunity3.Close_Stage_Reason__c ='Other';        
        //insert testOpportunity3;
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;    
        Database.insert(opptylineItem);
        
        OpportunityLineItem opptylineItem2 = new OpportunityLineItem ();
        opptylineItem2.OpportunityId = testOpportunity2.Id;
        opptylineItem2.PricebookEntryId = pbEntry.Id;        
        opptylineItem2.Revenue_Start_Date__c =  Date.Today();
        opptylineItem2.Revenue_End_Date__c =  Date.Today();
        opptylineItem2.UnitPrice = 100.00;
        opptylineItem2.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;    
        Database.insert(opptylineItem2);
                
        OpportunityLineItem opptylineItem3 = new OpportunityLineItem ();
        opptylineItem3.OpportunityId = testOpportunity3.Id;
        opptylineItem3.PricebookEntryId = pbEntry.Id;        
        opptylineItem3.Revenue_Start_Date__c =  Date.Today();
        opptylineItem3.Revenue_End_Date__c =  Date.Today();
        opptylineItem3.UnitPrice = 100.00;
        opptylineItem3.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;    
       // Database.insert(opptylineItem3);
     Test.stopTest();
    }
     public static testMethod void createTestData1(){        
        List<Project_Exception__c> peList = new List<Project_Exception__c>();
        
        Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Office';
        peOffice.Office__c = 'Shanghai - Huai Hai';
        peList.add(peOffice);                
        
      
        Database.insert(peList, false);
        
        //Account creation
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount; 
              
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity 1';
        testOpportunity.Type = 'Rebid';
        testOpportunity.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate =  Date.Today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        //testOpportunity.Close_Stage_Reason__c ='Other'; 
         
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1; 
        Test.stopTest();   
        Database.insert(opptylineItem);
        
     
    }
   public static testMethod void testOfficeEx(){
       Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Office';
        peOffice.Office__c = 'Norwalk - Merritt';
        insert peOffice;
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount; 
        
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Norwalk - Merritt';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity1;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1; 
        Test.stopTest();    
        Database.insert(opptylineItem);
        
     
        
        
   }
   
    public static testMethod void testOneCodeEx(){
       Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'One Code';
        peOffice.One_Code__c = '54212';
        insert peOffice;
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='54212';
        insert testAccount; 
        
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Norwalk - Merritt';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity1;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;  
        Test.stopTest();   
        Database.insert(opptylineItem);
        
     
        
        
   }
   public static testMethod void testLobTypeEx(){
       Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Opportunity Type';
        peOffice.Opportunity_Type__c = 'Existing Client - New LOB';
        insert peOffice;
        
        Project_Exception__c peOffice1 = new Project_Exception__c();
        peOffice1.Exception_Type__c = 'Product LOB';
        peOffice1.LOB__c = 'Career';
        insert peOffice1;
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='54212';
        insert testAccount; 
        
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Norwalk - Merritt';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity1;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Wealth';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1; 
        Test.stopTest();    
        Database.insert(opptylineItem);
        
     
      
   }
   public static testMethod void testRecordTypeEx(){
       Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'RecordType Name';
        peOffice.Record_Type_Name__c = 'Opportunity Detail Page Assinment';
        insert peOffice;
        
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='54s212';
        insert testAccount; 
        
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request is:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Norwalk - Merritt';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity1;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Wealth';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1; 
        Test.stopTest();    
        Database.insert(opptylineItem);
        
     
        
        
   }
   public static testMethod void testProdCodeOfficeEx(){
       Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Product + Office Code';
        peOffice.Product_Code__c = '2354';
        peOffice.Office__c = 'Shanghai - Huai Hai';
        insert peOffice;
        
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='54s212';
        insert testAccount; 
        
         Opportunity testOpportunity1 = new Opportunity();
        testOpportunity1.Name = 'Test Opportunity 1';
        testOpportunity1.Type = 'Existing Client - New LOB';
        testOpportunity1.AccountId =  testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        testOpportunity1.CloseDate =  Date.Today();
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        //testOpportunity1.Close_Stage_Reason__c ='Other';
        Test.startTest();
         insert testOpportunity1;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Wealth';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '2354';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;
        Test.stopTest();     
        Database.insert(opptylineItem);
        
     
        
        
   }
       public static testMethod void testTypeContryLobCommEx(){        
        List<Project_Exception__c> peList = new List<Project_Exception__c>();
        
        Project_Exception__c peTypeCounloncomName = new Project_Exception__c();
        peTypeCounloncomName.Exception_Type__c = 'Opportunity Type + Opportunity Country + Product LoB + Commission Flag';
        peTypeCounloncomName.Opportunity_Type__c = 'Existing Client - New LOB';
        peTypeCounloncomName.Opportunity_Country__c = 'India';
        peTypeCounloncomName.LOB__c = 'Career';
        peTypeCounloncomName.Commision__c= True;
        peList.add(peTypeCounloncomName);             
        
      
        Database.insert(peList, false);
        
        //Account creation
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='Test1';
        insert testAccount; 
              
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity 1';
        testOpportunity.Type = 'Existing Client - New LOB';
        testOpportunity.AccountId =  testAccount.Id; 
        //request is:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate = Date.Today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity.Opportunity_Country__c = 'India';
        testOpportunity.currencyisocode = 'ALL';
        //testOpportunity.Close_Stage_Reason__c ='Other';        
        Test.startTest();
         insert testOpportunity;
        
        
        
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '1233';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id,Product2.productcode, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        system.debug('pbEntry****'+pbEntry);
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        opptyLineItem.Commission__c=true;
        //opptyLineItem.Year2Revenue_edit__c = 1;
        Test.stopTest();     
        Database.insert(opptylineItem);
        
     
    }
    public static testMethod void testUpdateProjectRequired(){
        System.runAs(testUser()){
           
            createTestData();
            //createTestData1();
           
        }
    }
    
}