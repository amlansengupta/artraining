/***************************************************************************************************************
User Story : US486
Purpose : This Apex Class is created as the controller of 'MF2_SendToWorkflowWizard' lightning component which 
supports the 'Send To Workflow' Quick Action on Account Object.
Created by : Soumil Dasgupta
Created Date : 7/12/2018
Project : MF2
****************************************************************************************************************/

public class MF2_SendToWorkflowWizardController {
    
    //The below wrapper is created to hold the Boolean for URl and the statemant/Url itself.
    public static returnStatementWrapper stmntwrap = new returnStatementWrapper();
    
    @Auraenabled
    public static returnStatementWrapper fetchAccount(String accID, String URL){
        
        //System.debug('Apex on');
        //System.debug('Acc '+accID);
        //System.debug('URL ' + URL);
        String profileName;
        String returnStatement;
        
        Account acc = new Account();
        acc = [SELECT Id,Name,One_Code_Status__c,Integration_Status__c from Account where Id =: accID];
        
        //get the ProfileName of the current User.
        String Profile = userinfo.getProfileId();
        List<Profile> profileNameList = [Select Name from Profile where ID =: Profile Limit 1];
        
        if(profileNameList != null && !profileNameList.isEmpty()){
            profileName =  profileNameList[0].Name;  
        }
        try{
            if(acc != null){
                if((profileName == 'System Administrator')||(profileName == 'Mercer System Admin Lite')){
                    if ((acc.One_Code_Status__c == 'Pending - Workflow Wizard')&&((acc.Integration_Status__c == 'Error')||(acc.Integration_Status__c == 'Not Submitted'))){
                        acc.One_Code_Status__c = 'Pending';
                        acc.Workflow_Message__c = null;
                        acc.Auto_Merge_Flag__c = false;
                        acc.Auto_Merge_Date__c = null;
                        acc.Workflow_Submit_Status__c = null;
                        acc.Integration_Status__c = null;
                        database.update(acc);
                        
                        //If the aboveconditions satisfy and the account is updated, The returnstatement will be the URL of the account page.
                        stmntwrap.returnStatement = URL;
                        stmntwrap.isURl = True;
                        
                        
                    }
                    else
                    {
                        stmntwrap.returnStatement = 'The selected account cannot be submitted for OneCoding. Please ensure that the Integration Status is set to \'Not Submitted\' or \'Error\' and the OneCode status is set to /"Pending - Workflow Wizard/" and try again.';
                        stmntwrap.isURl = False;
                        
                        
                    }
                }
                else
                {
                    stmntwrap.returnStatement = 'This link is used to manually submit new accounts to the Workflow Wizard for One Coding if the integration has failed. Only System Administrators can manually submit accounts to the Workflow Wizard for One Coding.';
                    stmntwrap.isURl = False;
                    
                }
            } 
            else{
                stmntwrap.returnStatement = 'Account not found';
                stmntwrap.isURl = False;
            }
    	}
        catch(Exception e){
            system.debug('Error :'+e);
        }
        return stmntwrap;
    }
    
    
    public class returnStatementWrapper{
        @Auraenabled
        public Boolean isURL {get; set;}
        @Auraenabled
        public String returnStatement {get; set;}
    }
    
}