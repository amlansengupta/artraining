/* Purpose:This Batchable class mass updates the Account records to populate Concatenated Industry Category field 
   as a concatenation of Primary Industry Category, Secondary Industry Category and Tertiary Industry Category fields. 
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Accounts as an input and mas updates the Account records  
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          08/23/2013  Created Batch Class to populate Concatenated Industry Category on Account
 2.0         Sarbpreet          7/7/2014    Added Comments. 
 ======================================================================================================================================*/
global class DC24_AccountBatchable implements Database.Batchable<sObject>, Database.Stateful{
    //String Variable to store query
    public String query ;   
    // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    /*
    *   Class Constructor for inistializing the value of 'query' variable 
    */
    public DC24_AccountBatchable(String qStr)
    {
         this.query= qStr;
    }
     /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */ 
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {  
         Integer errorCount = 0 ;
     
         //List variable to store Accounts
         List<Account> accList = new List<Account>();
         system.debug('\n The size of scope is ' + scope.size());
         //Iterate through Account records fetched through 'query'
         for(Account acc : scope)
         {
              accList.add(acc);                                                                              
         } 
        
     // List variable to save the result of the Accounts which are updated   
     List<Database.SaveResult> srList= Database.Update(accList, false); 
      
     //MercerAccountStatusBatchHelper.createErrorLog();  
     if(srList!=null && srList.size()>0) {
         // Iterate through the saved Accounts 
         Integer i = 0;             
         for (Database.SaveResult sr : srList)
         {
                // Check if Accounts are not updated successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) 
                {
                        errorCount++;
                        System.debug('Error DC24_AccountBatchable '+sr.getErrors()[0].getMessage());
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC24:' +sr.getErrors()[0].getMessage(), errorLogs, accList[i].Id);  
                }
                i++;
         }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('DC24 Status :scope:' + scope.size() +' accList:' + accList.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);
     
     insert errorLogStatus;      
    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */ 
    global void finish(Database.BatchableContext BC)
    {
         // Check if errorLogs contains some failure records. If so, then insert error logs into database.
         if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }    
}