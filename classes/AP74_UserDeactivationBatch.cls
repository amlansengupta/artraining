/* Purpose: Batch for deactivating corresponding users for recently deactivated Colleagues
===================================================================================================================================
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          08/23/2013  PMO Req#3412 ,2013Nov Release : Batch for deactivating corresponding users for recently deactivated Colleagues
 1.0 -       Sarbpreet          7/6/2014    Updated Comments/documentation.
 1.1 -       Bala               10/12/2015  PMO Req# 7439 ,2015 Dec Release : Updated the Batch class code to capture the info if the user is deactivated by batch class.
                                            On to the field 'Track_User_History__c','Logged_In_User__c' and 'Request__c'
 1.2         Dhanusha           28/1/2015   PMO Req#6605 ,2016 March Release : updated logic for removing MH licenses of corresponding users for recently deactivated Colleagues                 
 ======================================================================================================================================*/
global class AP74_UserDeactivationBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    //String Variable to store query
    public String query ;   
    
     // As per the Req# 7439
     Id I= Id.valueOf(Label.MercerForceUserid);
    
    // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();


    /*
    *    Creation of Constructor
    */
    public AP74_UserDeactivationBatch(String qStr)
    {
         this.query= qStr;
    }
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
    
     /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext BC, List<Colleague__c> scope)
    {   
        //AP67_userTriggerUtil.updateFromBatchJob = true;
        //List variable to update users with supervisors
        List<User> userlstsupforupdate = new List<User>();
        //List variable to update users 
        List<User> userlstforupdate = new List<User>();
        //Map variable to store inactive colleagues
        Map<String, Colleague__c>  collMap = new Map<String, Colleague__c>();
    
        System.debug('/n/n AP74_UserDeactivationBatch '+scope);
    
        //List to hold Save Result 
        List<Database.SaveResult> userSaveResult = new List<Database.SaveResult>();
       
        //List to hold Save Result 
        List<Database.SaveResult> userSupSaveResult = new List<Database.SaveResult>();   
         
        //List variable to store users with supervisors
        List<User> userlstwithSup = new List<User>();         
        //Map to store user IDs and users
        Map<ID, User> userMap = new Map<ID, User>();
        
         //Integer to hold error count while updating user
         Integer errorCount = 0; 
         //Integer to hold error count while updating account Team Member 
         Integer errorCountUser = 0;
                            
         try
         {
             //Iterate through Colleague records fetched through 'query'
             for(Colleague__c coll : scope)
             {
                 //Add to colleagueMap              
                 collMap.put(coll.EMPLID__c, coll);
                      
             }                                                                             
                             
             userMap =  new Map<ID, User>([Select ID, Name,EmployeeNumber,S_Dash_Supervisor__c, S_Dash_Supervisor_2__c, S_Dash_Supervisor_3__c, S_Dash_Supervisor_4__c, S_Dash_Supervisor_5__c,Collegue__c  from user where  EmployeeNumber IN:collMap.KeySet() and isActive = TRUE]);
             
             userlstwithSup = [select id,name, S_Dash_Supervisor__c, S_Dash_Supervisor_2__c, S_Dash_Supervisor_3__c, S_Dash_Supervisor_4__c, 
                        S_Dash_Supervisor_5__c,  S_Dash_Supervisor__r.Id, S_Dash_Supervisor_2__r.Id, S_Dash_Supervisor_3__r.Id, S_Dash_Supervisor_4__r.Id,Collegue__r.Id,
                        S_Dash_Supervisor_5__r.Id from user where (S_Dash_Supervisor__r.Id IN :userMap.keyset() or S_Dash_Supervisor_2__r.Id IN :userMap.keyset() or S_Dash_Supervisor_3__r.Id
                        IN :userMap.keyset() or S_Dash_Supervisor_4__r.Id IN :userMap.keyset() or S_Dash_Supervisor_5__r.Id  IN :userMap.keyset()) or (Collegue__r.Id IN :userMap.keyset()) ] ;
            
            System.debug('/n/n AP74_UserDeactivationBatch userlstwithSup '+userlstwithSup ); 
             for(User user : userlstwithSup)
             {
                    if(userMap.containskey(user.S_Dash_Supervisor__r.Id ))
                    user.S_Dash_Supervisor__c = null;
                    
                    if(userMap.containskey(user.S_Dash_Supervisor_2__r.Id ))
                    user.S_Dash_Supervisor_2__c = null;
                     
                    if(userMap.containskey(user.S_Dash_Supervisor_3__r.Id ))
                    user.S_Dash_Supervisor_3__c = null;
                     
                    if(userMap.containskey(user.S_Dash_Supervisor_4__r.Id ))
                    user.S_Dash_Supervisor_4__c = null;
                     
                    if(userMap.containskey(user.S_Dash_Supervisor_5__r.Id ))
                    user.S_Dash_Supervisor_5__c = null;                  

                    if(userMap.containskey(user.Collegue__r.Id ))
                    user.Collegue__c = null;                       
                    
                    userlstsupforupdate.add(user);
    
           }
         } catch(Exception e)
         {
            System.debug('AP74_UserDeactivationBatch Exception occured:'+e.getMessage());
         }  
         
        System.debug('/n/n AP74_UserDeactivationBatch userlstsupforupdate '+userlstsupforupdate ); 
         
        //Update users who were assoicated with these Supervisors 
        if(userlstsupforupdate.size()>0)
        {
        
           userSupSaveResult = Database.update(userlstsupforupdate,false) ;
           
           //Iterate for Error Logging
            for(Database.Saveresult result : userSupSaveResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP74: Users having Supervisor : ' + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCount++;
                }
                
              }           
        }
        System.debug('/n/n AP74_UserDeactivationBatch userSupSaveResult '+userSupSaveResult ); 
        userMap = new Map<ID, User>([Select id, name, isActive, EmployeeNumber from User where EmployeeNumber IN:collMap.KeySet() and isActive =TRUE]); 
     
       // MH licenses remoavl per request#6605 march 2016 release
       
        System.debug('/n/n AP74_UserDeactivationBatch userMap '+userMap ); 
        for(Id uid:userMap.keySet())
         {
              User u = userMap.get(uid);
              u.isActive = false;
              u.Blue_Sheet_Managers_License__c = false;
              u.Blue_Sheet_Read_Only_License__c = false;
              u.mh_Blue_Sheet_Read_Write_License__c = false;
              u.Green_Sheet_Managers_License__c = false;
              u.Green_Sheet_Read_Only_License__c = false;
              u.Green_Sheet_Read_Write_License__c = false;
              u.Gold_Sheet_Managers_License__c = false;
              u.Gold_Sheet_Read_Only_License__c = false;
              u.Gold_Sheet_Read_Write_License__c = false;
              
             // As per the Req# 7439
              u.Logged_In_User__c= I;
              u.Request__c=Label.Batch_Process_Updated_the_User_Record;
              
              userlstforupdate.add(u);
         } 
        System.debug('/n/n AP74_UserDeactivationBatch userlstforupdate '+userlstforupdate ); 
        // List variable to save the result of the Users which are being deactivated    
         if(!userlstforupdate.isEmpty())  {
             userSaveResult = Database.Update(userlstforupdate, false);
             
             //Iterate for Error Logging
             for(Database.Saveresult result : userSaveResult)
             {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP74: User Saving : ' + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCountUser++;
                }
             }    
         }
         System.debug('/n/n AP74_UserDeactivationBatch userSaveResult '+userSaveResult ); 

           
    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */ 
    global void finish(Database.BatchableContext BC)
    {
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  

    }    
}