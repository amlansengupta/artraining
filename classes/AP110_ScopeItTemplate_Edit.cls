/*Purpose:  This Apex class is created as a Part of september release #4813 sep 2014 release
==============================================================================================================================================
The methods called perform following functionality:
.Search Products
.Select One Product
.Update scopeItTemplate record


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi     8/4/2014  This Apex class works as Controller for "Mercer_ScopeItProjectTemp_Edit_Override" VF page which is invoked from 
                                    "Edit" button on scopeItProjectTemplate Object.created as a Part of september 2014 release #4813
   2.0 -    Bala		9/3/2015   As part of Request# 7282 added product ID field in wrapepr class and while creating Scopt it template.                                  
============================================================================================================================================== 
*/
public without sharing class AP110_ScopeItTemplate_Edit{
public Temp_ScopeIt_Project__c tempObj{set;get;}
public String selProduct {get;set;}
public String selProductId {get;set;}
public String searchValue {get;set;}
public wrapper wrpobj {get;set;}
public List<Wrapper> lstwrapper = new List<Wrapper>();
private integer counter=0;  //keeps track of the offset
private integer list_size=10; //sets the page size or number of rows
public integer total_size; //used to show user the total size of the list
/*
  Constructor for AP110_ScopeItTemplate_Edit
*/
public AP110_ScopeItTemplate_Edit(ApexPages.StandardController controller){
     if(controller.getId()!=null){
          tempObj= [SELECT id,name,LOB__c,Product__c,Product_Id__c,Solution_Segment__c,Sales_Price__c FROM Temp_ScopeIt_Project__c where id=:Controller.getId() limit 1];
          selProduct = tempObj.Product__c; 
          selProductId = tempobj.Product_Id__c;
        }
        total_size = [select count() from Product2 where IsActive = true and Classification__c=: ConstantsUtility.STR_FILTER]; 
}
/*
    *   Getter method for lstwrapper fro getting product details
    */
    public List<wrapper> getlstwrapper(){
        lstwrapper.clear();
        wrapper w;
        if(searchValue <> null){
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\' and ';
            sql += '(name like \'%'+searchValue+'%\' or LOB__c like \'%'+searchValue+'%\' or Segment__c like \'%'+searchValue+'%\'  or Cluster__c Like \'%'+searchValue+'%\') order by Name limit :list_size offset :counter';            
            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                //Added as part of Request# 7282
                w.prodid=prod.id;
                lstwrapper.add(w);
            }
           
        }
        else{
            for(Product2 prod: [select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c=:ConstantsUtility.STR_FILTER order by Name limit :list_size offset :counter]){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                //Added as part of Request# 7282
                w.prodid=prod.id;
                lstwrapper.add(w);
            }    
        }
        return lstwrapper;
    }
    /*
   * @Description : Method for searching the products
   * @ Args       : null
   * @ Return     : pageReference
   */
    public pageReference searchProducts(){
        lstwrapper.clear();
        wrapper w;
        if(searchValue <> null){
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\' and ';
            sql += '(name like \'%'+searchValue+'%\' or LOB__c like \'%'+searchValue+'%\' or Segment__c like \'%'+searchValue+'%\'  or Cluster__c Like \'%'+searchValue+'%\')';           
            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                //Added as part of Request# 7282
                w.prodid=prod.id;
                lstwrapper.add(w);
                }       
            return null;
        }
        else{
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\'';

            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                //Added as part of Request# 7282
                w.prodid=prod.id;
                lstwrapper.add(w);
                }
            
            return null;
            
        }
        
    }
     /*
   * @Description : This method saves the product record
   * @ Args       : null
   * @ Return     : pageReference
   */  
    public pageReference SaveProduct(){
        //Scope_Modeling__c objMod = new Scope_Modeling__c();
        if(String.isBlank(selProduct)){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ConstantsUtility.STR_MSG));
             return null;
        }
        else{
            
            tempObj.Product__c = selProduct;
            try{
            update tempObj;
            }catch(Exception ex){
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));            	
            }
            return new PageReference(ConstantsUtility.STR_URL+tempObj.id);
        }
}
/*
    *    Wrapper class for products
    */
    public class wrapper {
        public boolean isSelected {get;set;}
        public String pid {get;set;}
        public String pname{get;set;}
        public String pcode{get;set;}
        public String pdesc{get;set;}
        public String plob{get;set;}
        public String pseg{get;set;}
        public String pclus{get;set;}
        //Added as part of Request# 7282
        public String prodid{get;set;}
    }
      /*
     *  Method for navigating to beginning of page
     */
      public PageReference Beginning() {
      counter = 0;
      
      return null;
   }
  /*
   *    Method for navigating to previous page
   */
   public PageReference Previous() { 
      counter -= list_size;
      return null;
   }
  /*
   *    Method for navigating to next page
   */
   public PageReference Next() { 
      counter += list_size;
      return null;
   }
 
  /*
   *    Method for navigating to the last page
   */
   public PageReference End() { 
      counter = total_size - math.mod(total_size, list_size);
      return null;
   }
 
  /*
   *    Getter method for getting the value of DisablePrevious variable 
   */
   public Boolean getDisablePrevious() {
      
      if (counter>0) return false; else return true;
   }
 
  /*
   *    Getter method for getting the value of DisableNext variable 
   */
   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) return false; else return true;
   }
 
  
 

}