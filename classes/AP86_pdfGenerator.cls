/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:

•   Query all ScopeIt Projects and there Related Tasks and Employees.
•   Created a Wrapper Class to capture Related Tasks and Employees w.r.t each Project .

History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/10/2014  This Apex class acts as Controller for the Printit VF page.
   2.0 -    Venu    03/16/2014  Updated based on ScopeIt - May2014 release changes
   3.0 -    Madhavi 10/21/2014  Added IC_Charges__c field to the queries as per request #4881(december 2014 release)
                                updated  "Mercer_PrintScopeIt" vf page.
============================================================================================================================================== 
*/
public class AP86_pdfGenerator {
    public List<Scopeit_Project__c> projectList { get; set; }
    public List<Scopeit_Task__c> taskList { get; set; }
    public String monthShort {get; set;}
    
    public Map<Id, List<Scopeit_Task__c>> prjIdTaskListMap { get; set; }
    private Set<Id> projectIds = new Set<Id>();
    private Map<Id, Scopeit_project__c> prjMap = new Map<Id, Scopeit_project__c>();
    
    
    public List<ProjectWrapper> projectWrapperList { get; set; }
    /*Constructor used to Query Project,Tasks and Employees of  Opportunity.
     Once required records are fetched they are passed into Wrapper list.
    */
    public AP86_pdfGenerator(ApexPages.StandardController controller){
    monthShort = getCurrentMonth();
    projectList=new List<Scopeit_Project__c>();
    taskList= new List<Scopeit_Task__c>();
    //Added IC_Charges__c to the query  as per dec 2014 release(#4881) 
    projectList=[select name,Billable_Expenses__c,Billable_Time_Charges__c,Cost_Billable_and_Non_Billable__c,
                    Calc_Cost__c,Cluster__c,Cost__c,LOB__c,Margin__c,Calc_Margin__c,
                    Margin_Benchmark__c,Sales_Price__c,
                    Opportunity__c,Opportunity_Country__c,OpportunityProductLineItem_Id__c,Planned_Fee_Adj__c,Non_Billable_Expenses__c,Non_Billable_Time_Charges__c,
                    Product__c,Product__r.name,CalcProduct__c,Profit_Loss__c,Solution_Segment__c ,IC_Charges__c
                from Scopeit_Project__c 
                    where Opportunity__c=:apexpages.currentpage().getparameters().get('id') ORDER BY CreatedDate desc];
    
    prjIdTaskListMap= new Map<Id, List<Scopeit_Task__c>>();
     projectWrapperList = new List<ProjectWrapper>();
     
     if(projectList.size() > 0) {
            for(Scopeit_Project__c p: projectList) {
                projectIds.add(p.Id);
                prjMap.put(p.Id, p);
            }
             //Added IC_Charges__c to the query  as per dec 2014 release(#4881) 
            taskList = [select name,Task_Name__c,Billable_Expenses__c,Billable_Time_Charges__c,Calc_Billable_Time_Charges__c, Bill_Est_of_Increases__c,
                            Bill_Rate__c,Bill_Type01__c,Cost__c,LOB__c,Calc_Non_Billable_Time_Charges__c,IC_Charges__c,
                            Opportunity_Country__c,ScopeIt_Project__c,Total_Hours__c,Cost_Billable_and_Non_Billable__c,
                                (select Billable_Time_Charges__c,Level__c,Bill_Cost_Ratio__c,Bill_Rate__c,Bill_Rate_Opp__c,Business__c,
                                 Cost__c,Employee_Bill_Rate__c,Hours__c,Market_Country__c,Name__c,ScopeIt_Task__c from Employees__r)
                        from ScopeIt_Task__c 
                            where Scopeit_Project__c in:projectIds ORDER BY Scopeit_Project__r.CreatedDate desc ];
            
    }
    if(taskList.size() > 0) {
            for(Scopeit_Task__c t :taskList) {
                if(!prjIdTaskListMap.containsKey(t.ScopeIt_Project__c)){
                    prjIdTaskListMap.put(t.ScopeIt_Project__c, new List<Scopeit_Task__c>());
                }
                prjIdTaskListMap.get(t.ScopeIt_Project__c).add(t);
            }
            for(Id taskId : prjIdTaskListMap.keySet()) {
                projectWrapperList.add(new ProjectWrapper(prjMap.get(taskId), prjIdTaskListMap.get(taskId)));
            }
        }
    
    
    }
       
    /*
     * Wrapper Class to capture Related Tasks and Employees w.r.t each Project
     */
      public class ProjectWrapper {
        public Scopeit_Project__c project { get; set; }      
        public List<Scopeit_Task__c> taskList { get; set; }
        /*
         * Method for getting project and associated tasks
         */
        public ProjectWrapper(Scopeit_Project__c project, List<Scopeit_Task__c> taskList) {
            this.project = project;
            this.taskList = taskList;
        }
    }
    public static String getCurrentMonth()
    {
        Map<Integer,String> month = new Map<Integer,String>(); 
        month.put(1,'Jan');
        month.put(2,'Feb');
        month.put(3,'Mar');
        month.put(4,'Apr');
        month.put(5,'May');
        month.put(6,'Jun');
        month.put(7,'Jul');
        month.put(8,'Aug');
        month.put(9,'Sep');
        month.put(10,'Oct');
        month.put(11,'Nov');
        month.put(12,'Dec');
        String MonthName = month.get(Date.Today().Month());
        return MonthName;
    }
    



}