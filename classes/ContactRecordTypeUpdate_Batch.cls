global class ContactRecordTypeUpdate_Batch implements Database.Batchable<sObject>{

    global String query = Label.ContactRecordTypeUpdate_Batch_Query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(Test.isRunningTest()){
            query = 'Select Id, RecordTypeId From Contact Where RecordTypeId = null LIMIT 200'; 
        }          
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){  
        List<Contact> contactList = new List<Contact>();
        for(Contact con : scope){
            //con.RecordTypeId  = '0120x0000004YWI';
            con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact Detail').getRecordTypeId();
            contactList.add(con);
        }
        Database.update(contactList,false);
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
}