/***************************************************************************************************************
Request# : 17449
Purpose : This class is created as the controller class of MF2_CopyTaskNew visualforce page.
Created by : Harsh Vats
Created Date : 11/01/2019
Project : MF2
****************************************************************************************************************/
public class MF2_CopyTaskNewController {
    public String scopeId{get;set;}
    //public ApexPages.StandardSetController stdCntrlr {get; set;}
    
    public MF2_CopyTaskNewController(ApexPages.StandardSetController controller)
    {
        
    }
    
    /*Request# : 17449: method to redirect the page with the help of actionFunction */
    public pagereference redirectToScope()
    {
        Pagereference pageRef = new Pagereference('/'+scopeId);
        return pageRef;
    }
    
}