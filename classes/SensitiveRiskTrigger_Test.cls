/*Test Class for SensitiveAtRisk Trigger*/
@isTest(seeAllData=false)
public class SensitiveRiskTrigger_Test {

    
    /*@isTest static void UnitTest() {
        Account Acc = new Account(name = 'Test Account',BillingCountry='test');
        insert Acc;
        
        
        Sensitve_At_Risk__c clientrelationship = new Sensitve_At_Risk__c(Account__c = Acc.id,At_Risk_Type__c = 'At Risk',At_Risk_Reason__c='Other',Revenue_at_Risk__c=0);
        insert clientrelationship;
        
        clientrelationship  = [select Id ,LOB_Status__c,At_Risk_Type__c  from Sensitve_At_Risk__c  where Id =: clientrelationship.id];
        system.assertEquals(clientrelationship.At_Risk_Type__c ,'At Risk');
        //system.assertEquals([select id from Client_Relationship__c].size(),1);
        
        clientrelationship.LOB_Status__c = 'Pending';   
        update clientrelationship;   
        
        delete clientrelationship;
        
        //system.assertEquals([select id from Client_Relationship__c].size(),0);
        
        
        test.startTest();
            database.executeBatch(new BATCH_OneDirectionalSensitiveDataSync(),2000);
        test.stopTest();
        

    }   
    */
    @isTest
    public static void testSensitveAtRiskTriggerHandler()
    {
        SensitveAtRiskTriggerHandler triggerHandler =new SensitveAtRiskTriggerHandler();
        
        TriggerSettings__c TrgSetting1 = new TriggerSettings__c();
        TrgSetting1.name='SensitveAtRisk Trigger';
        TrgSetting1.Object_API_Name__c='Sensitve_At_Risk__c';
        TrgSetting1.Trigger_Disabled__c=false;
        insert TrgSetting1;
        
        triggerHandler.IsDisabled();
        
        Account testAccount = new Account(name = 'Test Account',BillingCountry='test');
        insert testAccount;
        
        
        Sensitve_At_Risk__c clientrelationship = new Sensitve_At_Risk__c(Account__c = testAccount.id,At_Risk_Type__c = 'At Risk',At_Risk_Reason__c='Other',Revenue_at_Risk__c=0);
        insert clientrelationship;
                
        
        Account testAccount1 = new Account(name = 'Test Account1',BillingCountry='test1');
        insert testAccount1;
  
        Sensitve_At_Risk__c clientrelationship1 = new Sensitve_At_Risk__c(Account__c = testAccount1.id,At_Risk_Type__c = 'At Risk',At_Risk_Reason__c='Other',Revenue_at_Risk__c=0);
        insert clientrelationship1;
        
        
        
        List<Sensitve_At_Risk__c> beforeInsertList=new List<Sensitve_At_Risk__c>();
        beforeInsertList.add(clientrelationship);
		beforeInsertList.add(clientrelationship1);
        triggerHandler.BeforeInsert(beforeInsertList);
        
        List<Sensitve_At_Risk__c> afterInsertList=new List<Sensitve_At_Risk__c>();
        afterInsertList.add(clientrelationship);
		afterInsertList.add(clientrelationship1);	
        
        Map<Id, Sensitve_At_Risk__c> afterInsertMap=new Map<Id, Sensitve_At_Risk__c>();
        afterInsertMap.put(clientrelationship.id,clientrelationship);
        afterInsertMap.put(clientrelationship1.id,clientrelationship);
        
        triggerHandler.AfterInsert(afterInsertList,afterInsertMap);
        
        
        Map<Id, Sensitve_At_Risk__c> beforeUpdateMap=new Map<Id, Sensitve_At_Risk__c>();
        beforeUpdateMap.put(clientrelationship.id,clientrelationship);
        beforeUpdateMap.put(clientrelationship1.id,clientrelationship1);
        
        clientrelationship.At_Risk_Reason__c = 'RFP';
        
        update clientrelationship;
        
        clientrelationship.At_Risk_Reason__c = 'Rebid';
        update clientrelationship1;
        
        //SObject object3 = testAccount;
        //SObject object4 = testAccount1;
        
        Map<Id, Sensitve_At_Risk__c> afterUpdateMap=new Map<Id, Sensitve_At_Risk__c>();
        afterUpdateMap.put(clientrelationship.id,clientrelationship);        
        afterUpdateMap.put(clientrelationship1.id,clientrelationship1);
        
        triggerHandler.BeforeUpdate(beforeUpdateMap, afterUpdateMap);
        triggerHandler.AfterUpdate(beforeUpdateMap, afterUpdateMap);
        triggerHandler.BeforeDelete(beforeUpdateMap);
        triggerHandler.AfterDelete(beforeUpdateMap);
        triggerHandler.AfterUndelete(beforeUpdateMap);
    }
    
}