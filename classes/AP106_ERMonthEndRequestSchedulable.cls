/*Purpose: This Apex class implements schedulable interface to create ER requests at month end .
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Venu    12/05/2014  Created Apex Schedulable class
   2.0 -    Jagan   13/05/2014  Updated class to pull data from custom settings for Request
============================================================================================================================================== 
*/


global class AP106_ERMonthEndRequestSchedulable implements Schedulable{

     
    global void execute(SchedulableContext sc)
    {
        List<ERRequestData__c> ERFields = ERRequestData__c.getall().values();
        Map<String,String> fieldMap = new Map<String,String>();
        
        String thisMonthYear = system.now().format('MMMMM')+' '+system.today().year();
        
        for(ERRequestData__c er: ERFields){
            if(er.value__c.contains('<YYYY MM>'))
                fieldMap.put(er.name,er.value__c.replace('<YYYY MM>',thisMonthYear));
            else
                fieldMap.put(er.name,er.value__c);
        }
        sObject ReqObj= Schema.getGlobalDescribe().get('Request__c').newSObject() ;
        
        for(String key:fieldMap.keyset())
            ReqObj.put(key,fieldMap.get(key)) ;
        
        
        Database.insert(ReqObj);
        List<PMO_Approvals__c> lstPMOApp = new List<PMO_Approvals__c>();
        if(label.ERMonthEnd_Approver.contains(',')){
            String[] userValuelist = label.ERMonthEnd_Approver.split(',');
            Map<String,String> useridCategory = new Map<String,String>();
            for(String s:userValuelist){
                String[] usercat = s.split(':');
                useridCategory.put(usercat[0],usercat[1]);
            }
            for(String s:userValuelist){
                PMO_Approvals__c objReqApproval = new PMO_Approvals__c();
                objReqApproval.Request__c = ReqObj.Id;
                objReqApproval.Status__c = 'Pending Approval';
                String[] usercat = s.split(':');
                objReqApproval.Comments__c = thisMonthYear +' '+usercat[1];
                objReqApproval.Approver__c = usercat[0];
                lstPMOApp.add(objReqApproval);
                
            }
            Database.insert(lstPMOApp);
        }
        
    
     }
 
 }