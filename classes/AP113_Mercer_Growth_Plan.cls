/*Purpose:  This is a controller class for growth plan
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/11/2014  (As part of Request # 5367) Created Controller class 
   2.0 -    Neeraj      07/29/2015  (As part of request # 6798)September Release 2015 updated the code to add the newly created fields
   3.0 -    Sarbpreet   08/11/2015  As part of request # 6798 (September Release 2015), updated code to check growth indicator field value and highlight it with green, yellow or red
   4.0 -    Sarbpreet   08/12/2015  As part of Request # 8698 (January Release 2016), updated code to add 2 newly created fields.
============================================================================================================================================== 
*/
public with sharing class AP113_Mercer_Growth_Plan {

    public Id AccId;
    public Id GMid;
    public Account acct {get;set;}   
    public Growth_Plan__c gPlan {get;set;}
    
    public task gTask{get;set;}
    public List<Task> tList{get;set;}
    public boolean taskRender {get;set;}
    
    public static final string STR_SUBJECT = 'Growth Plan';
    public static final string STR_CURR = 'USD';
    public static final string STR_STATUS = 'Not Started';
    public static final string STR_PRIORITY = 'Normal';
    
    public String gColor {get;set;}
    public string readonly {get;set;}
        
    /*
     * @Description : Constructor of Controller class 
     * @ Args       : controller
     * @ Return     : null 
     */
        public AP113_Mercer_Growth_Plan(ApexPages.StandardController controller) {
 
        gTask= new task();
        gTask.Subject = STR_SUBJECT;        
        taskRender = True;    
        tList= new List<Task>();
 
         AccId = ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ACID);
         GMid = ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ObjID);
         if(GMid == null){
            gPlan = new Growth_Plan__c();
            gPlan.Account__c = AccId; 
            gPlan.Growth_Indicator__c = 0; 
            acct = [select id,name,Relationship_Manager__r.name, Relationship_Manager__r.Market__c,Account_Sub_Market__c,Account_Country__c, Relationship_Manager__r.Office__c, Type_of_Organization__c from Account where id=:AccId];        
         }
         else{
            //(As part of request # 6798)September Release 2015 Modified and created fields as per the New Growth Plan requirements.
            //As part of Request # 8698 (January Release 2016), updated code to add 2 newly created fields H_B_Yield_Initiative_target_carrier__c, H_B_Yield_Initiative_target_higher_comm__c
            gPlan = [select id,Account__c,Account__r.name, Planning_Year__c, Status__c, Business_Objective__c, key_Buying_Centers_Identified__c, key_relationships_entered__c, Rebid_in_Planning_Year__c,Past_Buying_Centers__c,
                        RET_Projected_Full_Year_Revenue_Actual__c,GBS_Projected_Full_Year_Revenue_Actual__c,TAL_Projected_Full_Year_Revenue_Actual__c,EH_B_Projected_Full_Year_Revenue_Actual__c,
                        OTH_Projected_Full_Year_Revenue_Actual__c, TOT_Projected_Full_Year_Revenue_Actual__c,EH_B_Planning_Yr_and_Carry_Frw_Rev__c,INV_Planning_Yr_and_Carry_Frw_Rev__c,OTH_Planning_Yr_and_Carry_Frw_Rev__c,TOT_Planning_Yr_and_Carry_Frw_Rev__c,
                        RET_Planning_Yr_and_Carry_Frw_Rev__c,GBS_Planning_Yr_and_Carry_Frw_Rev__c,TAL_Planning_Yr_and_Carry_Frw_Rev__c,INTL_Projected_Full_Year_Revenue_Actual__c,INTL_Planning_Yr_and_Carry_Frw_Rev__c,
                        RET_Projected_Current_Year_Revenue__c,RET_Recurring_and_Carry_forward_Revenue__c,RET_Projected_New_Sales_Revenue__c,RET_Projected_Full_Year_Revenue__c,RET_of_Revenue_Growth_Anticipated01__c,
                        EH_B_Projected_Current_Year_Revenue__c ,Current_Mercer_Solutions__c, Proposed_Mercer_Solutions__c,EH_B_Recurring_and_Carry_forward_Revenue__c,EH_B_Projected_New_Sales_Revenue__c,EH_B_Projected_Full_Year_Revenue__c, 
                        Parent_Growth_Plan_On_CGP__c,Proposed_Solution_1_Check__c,Proposed_Solution_2_Check__c,Proposed_Solution_3_Check__c,Proposed_Solution_4_Check__c,Proposed_Solution_5_Check__c,Proposed_Solution_6_Check__c,Proposed_Solution_7_Check__c,Proposed_Solution_8_Check__c,Proposed_Solution_9_Check__c,Proposed_Solution_10_Check__c,
                        TAL_Projected_Current_Year_Revenue__c,TAL_Recurring_and_Carry_forward_Revenue__c,TAL_Projected_New_Sales_Revenue__c,TAL_Projected_Full_Year_Revenue__c, GBS_Projected_Current_Year_Revenue__c,GBS_Recurring_and_Carry_forward_Revenue__c,GBS_Projected_New_Sales_Revenue__c,
                        GBS_Projected_Full_Year_Revenue__c,INTL_Projected_Current_Year_Revenue__c,INTL_Recurring_and_Carry_forward_Revenue__c,
                        INTL_Projected_New_Sales_Revenue__c,INTL_Projected_Full_Year_Revenue__c,OTH_Projected_Current_Year_Revenue__c,
                        OTH_Recurring_and_Carry_forward_Revenue__c,OTH_Projected_New_Sales_Revenue__c,OTH_Projected_Full_Year_Revenue__c,OTH_of_Revenue_Growth_Anticipated01__c,                                               
                        Current_Mercer_Solutions_01__c,Proposed_Mercer_Solutions_01__c,Primary_Client_Buying_Center__c,Next_actions_to_achieve_next_year_s_rev__c,
                        Client_s_strengths__c,Client_s_weaknesses__c,Client_s_opportunities__c,Client_s_threats__c,Key_Client_Dates__c,no_revenue_for_Plan_year_Explanation__c,
                        EH_B_of_Revenue_Growth_Anticipated01__c,TOT_Recurring_Carry_forward_Revenue__c,TOT_Projected_Sales_Revenue__c,TOT_Projectd_Current_Year_Revenue__c,Proposed_Solution1__c,
                        Proposed_Solution2__c,Proposed_Solution3__c,Proposed_Solution4__c,Proposed_Solution5__c,Proposed_Solution6__c,Proposed_Solution7__c,
                        Proposed_Solution8__c,Proposed_Solution9__c,Proposed_Solution10__c,TOT_Projectd_Full_Year_Revenue__c,TAL_of_Revnue_Growth_Anticipated__c, Client_Business_Objectives__c ,Client_Business_Needs__c,GBS_of_Revenue_Growth_Anticipated__c ,INTL_of_Revenue_Growth_Anticipated01__c, 
                        Actions__C,TOT_of_Revenue_Growth_Anticipated01__c, Growth_Indicator__c, Share_of_Wallet__c, Parent_Growth_Plan_ID__c,Is_this_Client_a_Key_Account__c,
                        Current_Solution_1__c,  Current_Solution_2__c,Current_Solution_3__c, Current_Solution_4__c, Current_Solution_5__c, Current_Solution_6__c, Current_Solution_7__c, Current_Solution_8__c, Current_Solution_9__c, Current_Solution_10__c,
                        When_was_the_last_RFP_for_Retirement__c, Overall_rating_in_the_last_CEM__c, When_was_the_last_RFP_for_Investments__c, Change_in_Decision_maker_last_year__c, When_was_the_last_RFP_for_EH_B__c,Relationship_risk_as_assessed_by_team__c,When_was_the_last_RFP_for_Talent__c,
                        LOB_s_for_service_issues__c, When_is_our_contract_expiring__c, LOB_s_change_in_delivery_last_year__c, H_B_Yield_Initiative_target_carrier__c, H_B_Yield_Initiative_target_higher_comm__c,Theme_NA_Healthcare__c,Theme_NA_Pension__c,Theme_NA_Financial_Wellness__c,Theme_NA_Talent__c,Theme_EP_Employee_Engagement__c,Theme_EP_Future_Workforce__c,Theme_EP_Global_Environment__c,Theme_EP_Pension_Risk__c,Theme_GM_Career__c,Theme_GM_Health__c,Theme_GM_Wealth__c from Growth_Plan__c  where id=:GMid];
            acct = [select id,name,Relationship_Manager__r.name, Relationship_Manager__r.Market__c, Relationship_Manager__r.Sub_Market__c,Account_Sub_Market__c,Account_Country__c, Relationship_Manager__r.Office__c, Type_of_Organization__c, NPS_Survey_Sent_Date__c from Account where id=:gPlan.Account__c]; 
            AccId =gPlan.Account__c;                  
         } 
         //As part of request # 6798 (September Release 2015), upadted code to check growth indicator field value and highlight it with green, yellow or red
          if(gPlan.Growth_Indicator__c > 0) {
             gColor = ConstantsUtility.STR_GreenCode;
          }
                            
          if(gPlan.Growth_Indicator__c == 0) {
            gColor = ConstantsUtility.STR_AmberCode;
          }
                           
          if(gPlan.Growth_Indicator__c < 0) {
            gColor = ConstantsUtility.STR_Red;
          } 
          //Endof request # 6798 (September Release 2015)
          //9790 Updates for UATFeedback 1504 start...
           readonly = 'true';

           Id profileId = userinfo.getProfileId();
           String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
           System.debug('ProfileName'+profileName);

           if(profileName == 'System Administrator'){
           readonly = 'false';} 
          else
          {
          readonly = system.Label.GP_Switch;
          }  
           // 9790 Updates for UATFeedback 1504 ends..
                  
        }
        
    /*
     * @Description : This method saves the Growth Plan and returns to Account page.  
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public pageReference Savebutton(){
       try {
            if(GMid == null)
            {
                    gplan.account__c = accid;  
                    gplan.currencyisocode= STR_CURR;  
                    insert gPlan;
                    insertTasks(gPlan);
                
            }
            else
            {
                update gPlan;
                insertTasks(gPlan);
            }
      
                return new PageReference(ConstantsUtility.STR_URL+AccId);
           } catch (Exception ex)
           {
                        ApexPages.addMessages(ex);
           }
        return null;
    }
    
   
    
     /*
     * @Description : This method calculates total of Projected Full Year Revenue fields.
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public pagereference totalFullYearRevenue(){
        try {
            if(gPlan.RET_Projected_Full_Year_Revenue__c==null) gPlan.RET_Projected_Full_Year_Revenue__c=0;
            if(gPlan.GBS_Projected_Full_Year_Revenue__c==null) gPlan.GBS_Projected_Full_Year_Revenue__c=0;
            if(gPlan.EH_B_Projected_Full_Year_Revenue__c==null) gPlan.EH_B_Projected_Full_Year_Revenue__c=0;
            if(gPlan.INTL_Projected_Full_Year_Revenue__c==null) gPlan.INTL_Projected_Full_Year_Revenue__c=0;
            if(gPlan.OTH_Projected_Full_Year_Revenue__c==null) gPlan.OTH_Projected_Full_Year_Revenue__c=0;
            if(gPlan.TAL_Projected_Full_Year_Revenue__c==null) gPlan.TAL_Projected_Full_Year_Revenue__c=0;
            
            //As part of request3 6798(September Release 2015) added RET_of_Revenue_Growth_Anticipated01__c in calculation
            if(gPlan.RET_of_Revenue_Growth_Anticipated01__c==null) gPlan.RET_of_Revenue_Growth_Anticipated01__c=0;    
            if(gPlan.GBS_of_Revenue_Growth_Anticipated__c==null) gPlan.GBS_of_Revenue_Growth_Anticipated__c=0;
            if(gPlan.TAL_of_Revnue_Growth_Anticipated__c==null) gPlan.TAL_of_Revnue_Growth_Anticipated__c=0;
            if(gPlan.EH_B_of_Revenue_Growth_Anticipated01__c==null) gPlan.EH_B_of_Revenue_Growth_Anticipated01__c=0;
            if(gPlan.INTL_of_Revenue_Growth_Anticipated01__c==null) gPlan.INTL_of_Revenue_Growth_Anticipated01__c=0;
            if(gPlan.OTH_of_Revenue_Growth_Anticipated01__c==null) gPlan.OTH_of_Revenue_Growth_Anticipated01__c=0;
            
            //As part of request3 6798(September Release 2015) added RET_Projected_Current_Year_Revenue__c in calculation
            if(gPlan.RET_Projected_Current_Year_Revenue__c == null) gPlan.RET_Projected_Current_Year_Revenue__c = 0;
            if(gPlan.GBS_Projected_Current_Year_Revenue__c == null) gPlan.GBS_Projected_Current_Year_Revenue__c = 0;
            if(gPlan.EH_B_Projected_Current_Year_Revenue__c == null) gPlan.EH_B_Projected_Current_Year_Revenue__c = 0;
            if(gPlan.INTL_Projected_Current_Year_Revenue__c == null) gPlan.INTL_Projected_Current_Year_Revenue__c = 0;
            if(gPlan.TAL_Projected_Current_Year_Revenue__c == null) gPlan.TAL_Projected_Current_Year_Revenue__c = 0;
            if(gPlan.OTH_Projected_Current_Year_Revenue__c == null) gPlan.OTH_Projected_Current_Year_Revenue__c = 0;
            
            //As part of request3 6798(September Release 2015) added RET_Recurring_and_Carry_forward_Revenue__c in calculation 
            if(gPlan.RET_Recurring_and_Carry_forward_Revenue__c == null) gPlan.RET_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gPlan.GBS_Recurring_and_Carry_forward_Revenue__c == null) gPlan.GBS_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c == null) gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gPlan.INTL_Recurring_and_Carry_forward_Revenue__c == null) gPlan.INTL_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gPlan.TAL_Recurring_and_Carry_forward_Revenue__c == null) gPlan.TAL_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gPlan.OTH_Recurring_and_Carry_forward_Revenue__c == null) gPlan.OTH_Recurring_and_Carry_forward_Revenue__c = 0;
            
            //As part of request3 6798(September Release 2015) added RET_Projected_New_Sales_Revenue__c in calculation 
            if(gPlan.RET_Projected_New_Sales_Revenue__c == null) gPlan.RET_Projected_New_Sales_Revenue__c = 0;
            if(gPlan.GBS_Projected_New_Sales_Revenue__c == null) gPlan.GBS_Projected_New_Sales_Revenue__c = 0;
            if(gPlan.EH_B_Projected_New_Sales_Revenue__c == null) gPlan.EH_B_Projected_New_Sales_Revenue__c = 0;
            if(gPlan.INTL_Projected_New_Sales_Revenue__c == null) gPlan.INTL_Projected_New_Sales_Revenue__c = 0;
            if(gPlan.TAL_Projected_New_Sales_Revenue__c == null) gPlan.TAL_Projected_New_Sales_Revenue__c = 0;
            if(gPlan.OTH_Projected_New_Sales_Revenue__c == null) gPlan.OTH_Projected_New_Sales_Revenue__c = 0;
            
            //As part of request6 9790(AUG Release 2016) Projected full year revenue actuals fields added.
            if(gPlan.RET_Projected_Full_Year_Revenue_Actual__c==null) gPlan.RET_Projected_Full_Year_Revenue_Actual__c=0;
            if(gPlan.GBS_Projected_Full_Year_Revenue_Actual__c==null) gPlan.GBS_Projected_Full_Year_Revenue_Actual__c=0;
            if(gPlan.EH_B_Projected_Full_Year_Revenue_Actual__c==null) gPlan.EH_B_Projected_Full_Year_Revenue_Actual__c=0;
            if(gPlan.INTL_Projected_Full_Year_Revenue_Actual__c==null) gPlan.INTL_Projected_Full_Year_Revenue_Actual__c=0;
            if(gPlan.OTH_Projected_Full_Year_Revenue_Actual__c==null) gPlan.OTH_Projected_Full_Year_Revenue_Actual__c=0;
            if(gPlan.TAL_Projected_Full_Year_Revenue_Actual__c==null) gPlan.TAL_Projected_Full_Year_Revenue_Actual__c=0;
            
            //As part of request6 9790(AUG Release 2016) Planning Yr and Carry Frw Rev% fields added.
            if(gPlan.RET_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.RET_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.GBS_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.GBS_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.EH_B_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.EH_B_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.INTL_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.INTL_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.OTH_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.OTH_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.TAL_Planning_Yr_and_Carry_Frw_Rev__c==null) gPlan.TAL_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gPlan.TOT_Projectd_Current_Year_Revenue__c == null) gPlan.TOT_Projectd_Current_Year_Revenue__c = 0;
            if(gPlan.TOT_Planning_Yr_and_Carry_Frw_Rev__c == null) gPlan.TOT_Planning_Yr_and_Carry_Frw_Rev__c = 0;
                 
                  
                  //11475  
                   //if(gPlan.RET_Projected_Full_Year_Revenue_Actual__c!=null && gPlan.GBS_Projected_Full_Year_Revenue_Actual__c!=null && gPlan.TAL_Projected_Full_Year_Revenue_Actual__c!=null && gPlan.EH_B_Projected_Full_Year_Revenue_Actual__c!=null  && gPlan.OTH_Projected_Full_Year_Revenue_Actual__c!=null)      
                      gPlan.TOT_Projected_Full_Year_Revenue_Actual__c= (gPlan.RET_Projected_Full_Year_Revenue_Actual__c + gPlan.TAL_Projected_Full_Year_Revenue_Actual__c + gPlan.EH_B_Projected_Full_Year_Revenue_Actual__c + gPlan.GBS_Projected_Full_Year_Revenue_Actual__c + gPlan.OTH_Projected_Full_Year_Revenue_Actual__c);
                      gPlan.TOT_Projectd_Current_Year_Revenue__c = (gPlan.RET_Projected_Current_Year_Revenue__c +gPlan.GBS_Projected_Current_Year_Revenue__c + gPlan.EH_B_Projected_Current_Year_Revenue__c + gPlan.TAL_Projected_Current_Year_Revenue__c + gPlan.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                     // gPlan.TOT_Projectd_Current_Year_Revenue__c = (gPlan.RET_Projected_Current_Year_Revenue__c +gPlan.GBS_Projected_Current_Year_Revenue__c + gPlan.EH_B_Projected_Current_Year_Revenue__c + gPlan.TAL_Projected_Current_Year_Revenue__c + gPlan.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                      
                  
                  //11475
                      gPlan.RET_Recurring_and_Carry_forward_Revenue__c = ((gPlan.RET_Projected_Current_Year_Revenue__c * gPlan.RET_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gPlan.GBS_Recurring_and_Carry_forward_Revenue__c = ((gPlan.GBS_Projected_Current_Year_Revenue__c * gPlan.GBS_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c = ((gPlan.EH_B_Projected_Current_Year_Revenue__c * gPlan.EH_B_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gPlan.TAL_Recurring_and_Carry_forward_Revenue__c = ((gPlan.TAL_Projected_Current_Year_Revenue__c * gPlan.TAL_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gPlan.OTH_Recurring_and_Carry_forward_Revenue__c = ((gPlan.OTH_Projected_Current_Year_Revenue__c * gPlan.OTH_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                    
                      gPlan.TOT_Recurring_Carry_forward_Revenue__c = (gPlan.RET_Recurring_and_Carry_forward_Revenue__c + gPlan.GBS_Recurring_and_Carry_forward_Revenue__c + gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c + gPlan.TAL_Recurring_and_Carry_forward_Revenue__c + gPlan.OTH_Recurring_and_Carry_forward_Revenue__c).setScale(1); 
                      
                      // WEALTH Calculations for Column 5 and 6
                     if(gPlan.RET_Projected_Current_Year_Revenue__c == 0){
                     gPlan.RET_Projected_New_Sales_Revenue__c = gPlan.RET_of_Revenue_Growth_Anticipated01__c;
                     gPlan.RET_Projected_Full_Year_Revenue__c = gPlan.RET_Projected_New_Sales_Revenue__c;}
                     else{
                     gPlan.RET_Projected_Full_Year_Revenue__c = (((gPlan.RET_Projected_Current_Year_Revenue__c * (gPlan.RET_of_Revenue_Growth_Anticipated01__c/100)) + gPlan.RET_Projected_Current_Year_Revenue__c)).setScale(1);
                     gPlan.RET_Projected_New_Sales_Revenue__c = (gPlan.RET_Projected_Full_Year_Revenue__c - gPlan.RET_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                      
                      // GBS Calculations for Column 5 and 6
                     if(gPlan.GBS_Projected_Current_Year_Revenue__c == 0){
                     gPlan.GBS_Projected_New_Sales_Revenue__c = gPlan.GBS_of_Revenue_Growth_Anticipated__c;
                     gPlan.GBS_Projected_Full_Year_Revenue__c = gPlan.GBS_Projected_New_Sales_Revenue__c;}
                     else{
                     gPlan.GBS_Projected_Full_Year_Revenue__c = ((gPlan.GBS_Projected_Current_Year_Revenue__c * (gPlan.GBS_of_Revenue_Growth_Anticipated__c/100)) + gPlan.GBS_Projected_Current_Year_Revenue__c).setScale(1);
                     gPlan.GBS_Projected_New_Sales_Revenue__c = (gPlan.GBS_Projected_Full_Year_Revenue__c - gPlan.GBS_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                     
                     //HEALTH Calculations for Column 5 and 6
                     if(gPlan.EH_B_Projected_Current_Year_Revenue__c == 0){
                     gPlan.EH_B_Projected_New_Sales_Revenue__c = gPlan.EH_B_of_Revenue_Growth_Anticipated01__c;
                     gPlan.EH_B_Projected_Full_Year_Revenue__c = gPlan.EH_B_Projected_New_Sales_Revenue__c;}
                     else{
                     gPlan.EH_B_Projected_Full_Year_Revenue__c = ((gPlan.EH_B_Projected_Current_Year_Revenue__c * (gPlan.EH_B_of_Revenue_Growth_Anticipated01__c/100)) + gPlan.EH_B_Projected_Current_Year_Revenue__c).setScale(1);
                     gPlan.EH_B_Projected_New_Sales_Revenue__c = (gPlan.EH_B_Projected_Full_Year_Revenue__c - gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                                         
                     // CAREER Calculations for Column 5 and 6
                     if(gPlan.TAL_Projected_Current_Year_Revenue__c == 0){
                     gPlan.TAL_Projected_New_Sales_Revenue__c = gPlan.TAL_of_Revnue_Growth_Anticipated__c;
                     gPlan.TAL_Projected_Full_Year_Revenue__c = gPlan.TAL_Projected_New_Sales_Revenue__c;}
                     else{
                     gPlan.TAL_Projected_Full_Year_Revenue__c = ((gPlan.TAL_Projected_Current_Year_Revenue__c * (gPlan.TAL_of_Revnue_Growth_Anticipated__c/100)) + gPlan.TAL_Projected_Current_Year_Revenue__c).setScale(1);
                     gPlan.TAL_Projected_New_Sales_Revenue__c = (gPlan.TAL_Projected_Full_Year_Revenue__c - gPlan.TAL_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                                          
                     // OTHR Calculations for Column 5 and 6
                     if(gPlan.OTH_Projected_Current_Year_Revenue__c == 0){
                     gPlan.OTH_Projected_New_Sales_Revenue__c = gPlan.OTH_of_Revenue_Growth_Anticipated01__c;
                     gPlan.OTH_Projected_Full_Year_Revenue__c = gPlan.OTH_Projected_New_Sales_Revenue__c;}
                     else{
                     gPlan.OTH_Projected_Full_Year_Revenue__c = ((gPlan.OTH_Projected_Current_Year_Revenue__c * (gPlan.OTH_of_Revenue_Growth_Anticipated01__c/100)) + gPlan.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                     gPlan.OTH_Projected_New_Sales_Revenue__c = (gPlan.OTH_Projected_Full_Year_Revenue__c - gPlan.OTH_Recurring_and_Carry_forward_Revenue__c).setScale(1);}                    

                     // Column 6 (TOTAL Calculation)
                         gPlan.TOT_Projectd_Full_Year_Revenue__c =  (gPlan.RET_Projected_Full_Year_Revenue__c +   gPlan.GBS_Projected_Full_Year_Revenue__c + gPlan.TAL_Projected_Full_Year_Revenue__c + gPlan.EH_B_Projected_Full_Year_Revenue__c+gPlan.OTH_Projected_Full_Year_Revenue__c).setScale(1);
                     // Column 5 (TOTAL Calculation)
                         gPlan.TOT_Projected_Sales_Revenue__c = (gPlan.RET_Projected_New_Sales_Revenue__c +gPlan.GBS_Projected_New_Sales_Revenue__c + gPlan.EH_B_Projected_New_Sales_Revenue__c + gPlan.TAL_Projected_New_Sales_Revenue__c + gPlan.OTH_Projected_New_Sales_Revenue__c).setScale(1); 
                     

                     if(gPlan.TOT_Projected_Full_Year_Revenue_Actual__c <> 0)
                      gPlan.TOT_Planning_Yr_and_Carry_Frw_Rev__c = ((gPlan.TOT_Recurring_Carry_forward_Revenue__c/gPlan.TOT_Projected_Full_Year_Revenue_Actual__c) * 100).setScale(1);       
                       else
                      gPlan.TOT_Planning_Yr_and_Carry_Frw_Rev__c = 0;
                                                 
                            if(gPlan.TOT_Projectd_Current_Year_Revenue__c <> 0)
                               gPlan.TOT_of_Revenue_Growth_Anticipated01__c = (((gPlan.TOT_Projectd_Full_Year_Revenue__c - gPlan.TOT_Projectd_Current_Year_Revenue__c )/ gPlan.TOT_Projectd_Current_Year_Revenue__c)*100).setScale(1);
                               else
                               gPlan.TOT_of_Revenue_Growth_Anticipated01__c = 0;
                               
                               gplan.Growth_Indicator__c = (((gPlan.TOT_Projectd_Full_Year_Revenue__c - gPlan.TOT_Projectd_Current_Year_Revenue__c )/ gPlan.TOT_Projectd_Current_Year_Revenue__c)*100).setScale(1);
                           system.debug('The growth indicator'+gplan.Growth_Indicator__c);
                           //As part of request # 6798 (September Release 2015), upadted code to check growth indicator field value and highlight it with green, yellow or red
                           if(gPlan.Growth_Indicator__c > 0)
                            gColor = ConstantsUtility.STR_GreenCode;
                            
                           if(gPlan.Growth_Indicator__c == 0)
                            gColor = ConstantsUtility.STR_AmberCode;
                           
                           if(gPlan.Growth_Indicator__c < 0)
                            gColor = ConstantsUtility.STR_Red;
                           //End of request # 6798 (September Release 2015)
                           system.debug('The growth indicator 2'+gplan.Growth_Indicator__c);
                      
            }  
            catch (Exception ex)
           {
                gPlan.addError(ConstantsUtility.STR_ERROR+ ex.getMessage());
           }   
           return null;
    }
    
  
     /*
     * @Description : Method for inserting tasks
     * @ Args       : null 
     * @ Return     : void 
     */
    public void insertTasks(Growth_Plan__c gPlan){
    
        List<Task> newInsertList = new List<Task>();
        try {
                 if(tList.size()>0){
                     for(Task t:tList){
                         if(t.subject<>null && t.Status <>null && t.Priority<>null){   
                             t.whatid = gPlan.id;
                             newInsertList.add(t);
                         }
                     }
                 if(newInsertList.size()>0){
                     database.insert(tList); 
                     newInsertList.clear();
                     tList.clear();
                 }
              }
        }catch (Exception ex)
           {
                gPlan.addError(ConstantsUtility.STR_ERROR+ ex.getMessage());
           }
   }
    
    /*
     * @Description : Method for inserting tasks
     * @ Args       : null 
     * @ Return     : void 
     */
    Public void insertTask(){
        try {
                taskRender = false;
                tList.add(gTask);
                gTask = new Task();
        }  catch (Exception ex)
           {
                gTask.addError(ConstantsUtility.STR_ERROR+ ex.getMessage());
           }
    }
    
    /*
     * @Description : Method for saving and inserting more tasks
     * @ Args       : null 
     * @ Return     : void 
     */
    Public void saveAndMore(){
        try {
                taskRender = true;
                tList.add(gTask);
                gTask = new Task();
                gTask.Subject = STR_SUBJECT;
                gTask.Status = STR_STATUS;
                gTask.Priority = STR_PRIORITY;
         }  catch (Exception ex)
           {
                gTask.addError(ConstantsUtility.STR_ERROR+ ex.getMessage());
           }
    }
    
    /*
     * @Description : This method is called when Cancel button is clicked.
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public PageReference cancelButton() {
        PageReference pageReference;
        try {
            pageReference = new PageReference(ConstantsUtility.STR_BackSlash + AccId);
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            pageReference = null;
        }
        return pageReference;
    }
    
}