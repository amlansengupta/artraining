/***************************************************************************************************************
Request Id : 17173
Purpose : This Test Class is created for the Apex Class MF2_OppScopeItDetailReferralController
Created by : Archisman Ghosh
Created Date : 12/04/2019
Project : MF2
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class MF2_OppScopeIt_Test {
    @isTest
    public static void customLinks_Positive(){
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        
        insert testOppty;
        
        ID  opptyId  = testOppty.id;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
           // MF2_OppScopeItDetailReferralController.checkRecordType(opptyId);
            MF2_OppScopeItDetailReferralController.scopeRecordTypeCheck(opptyId);
            //MF2_OppScopeItDetailReferralController.fetchOpportunity(opptyId);
            //system.assertEquals(MF2_OppScopeItDetailReferralController.checkRecordType(opptyId)[0].UI_Label__c,'Lock Opportunity (Admin Only)');
            system.assertEquals(MF2_OppScopeItDetailReferralController.scopeRecordTypeCheck(opptyId),true);
        }
    }
    @isTest
    public static void customLinks_Negative(){
        try{
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
            
            System.runAs(user1) {
                //MF2_OppScopeItDetailReferralController.checkRecordType(null);
            }
        }
        catch(Exception ex)
        {
            
        }
    }
    @isTest
    public static void customLinks_NegativeBoolean(){
        try{
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
            
            System.runAs(user1) {
                MF2_OppScopeItDetailReferralController.scopeRecordTypeCheck(null);
            }
        }
        catch(Exception ex)
        {
            
        }
    }
    /*@isTest
    public static void customLinks_NegativeFetch(){
        try{
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
            
            System.runAs(user1) {
                MF2_OppScopeItDetailReferralController.fetchOpportunity(null);
            }
        }
        catch(Exception ex)
        {
            
        }
    }*/
}