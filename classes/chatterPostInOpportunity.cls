public with sharing class  chatterPostInOpportunity {
    @InvocableMethod 
    public static void chatterPostMethod (List <opportunity> oppValue) {
        system.debug('@@@' + oppValue);
        
        for(Opportunity opp :oppValue){
        Id parentID = opp.Id;
        Id userID = opp.OwnerId;
        String Body = '  ';     
        mentionTextPost(userID,parentID,Body);  
            }
        saveChatterPosted(oppValue); 
    }
    public static void saveChatterPosted (List <opportunity> oppValue){
        List <opportunity> cPost = new List <opportunity>();
        for(Opportunity opp :oppValue){
         opp.Chatter_Posted_for_5_days__c = TRUE;
         cPost.add(opp);   
            }
         Upsert cPost; 
    }  
    public static void mentionTextPost(Id userId, Id userToMentionId, String postText) { 
        
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = userID;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        textSegmentInput.text = 'This opportunity has been at Pending Project step for 5 days. Have you already requested your project code(s)? If so, check on the progress. If not, request via Create Project or delegate to a team member via Delegate Project Set Up.';
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = userToMentionId;
        
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        
    } 
}