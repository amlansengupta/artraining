/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================

. create all required data for testing 
.create Page refer.

----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/24/2014  This class acts as Test class for AP86_pdfGenerator .
                                
 
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP86_pdfGenerator{
  /*
     * @Description : Test method to provide data coverage to AP86_pdfGenerator class 
     * @ Args       : Null
     * @ Return     : void
     */
  private static testMethod void generatePDF_test(){
        
     
  
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
         /*ApexConstants__c apexCon = new ApexConstants__c();
        apexCon.Name ='Above the funnel';  
        apexCon.StrValue__c ='Marketing/Sales Lead;Executing Discovery;';
        Insert apexCon;
        ApexConstants__c apexCon1 = new ApexConstants__c();
        apexCon1.Name ='Selected';  
        apexCon1.StrValue__c ='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
        Insert apexCon1;
        
            ApexConstants__c apexCon2 = new ApexConstants__c();
        apexCon2.Name ='Identify';  
        apexCon2.StrValue__c ='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        Insert apexCon2;
            
            ApexConstants__c apexCon3 = new ApexConstants__c();
        apexCon3.Name ='Active Pursuit';  
        apexCon3.StrValue__c ='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        Insert apexCon3;
            
              ApexConstants__c apexCon4 = new ApexConstants__c();
        apexCon4.Name ='Finalist';  
        apexCon4.StrValue__c ='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        Insert apexCon4;
          
               ApexConstants__c apexCon5 = new ApexConstants__c();
        apexCon5.Name ='AP02_OpportunityTriggerUtil_2';  
        apexCon5.StrValue__c ='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        Insert apexCon5;
            
               ApexConstants__c apexCon6 = new ApexConstants__c();
        apexCon6.Name ='AP02_OpportunityTriggerUtil_1';  
        apexCon6.StrValue__c ='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;IPIB01;MDMK02;MLTI07;MMMF01;TPEO50;  ';
        Insert apexCon6;
            
               ApexConstants__c apexCon7 = new ApexConstants__c();
        apexCon7.Name ='ScopeITThresholdsList';  
        apexCon7.StrValue__c ='EuroPac:25000;Growth Markets:2500;North America:25000';
        Insert apexCon7;
        */
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.id;
        insert testAccount;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =testAccount.Id; 
        //request id:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate = system.today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Aarhus - Axel';
        test.starttest();
        insert testOpportunity;
        test.Stoptest();
        
        Product2 testProduct = new Product2();
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.Classification__c='Consulting Solution Area';
        insert testProduct;
        
        ScopeIt_Project__c testSProject = new ScopeIt_Project__c ();
        testSProject.Opportunity__c=testOpportunity.id;
        testSProject.name='testSProject';
        testSProject.Product__c=testProduct.id;
        
        //testSProject.Bill_Type__c='Billable';
        insert testSProject;
           
       
       ScopeIt_Task__c testSTask = new ScopeIt_Task__c ();
       testSTask.ScopeIt_Project__c=testSProject.id;
       //testSTask.Net_Price__c=90.00;
       insert testSTask;
       
       ScopeIt_Employee__c testSemp = new ScopeIt_Employee__c ();
       testSemp.ScopeIt_Task__c=testSTask.Id;
       insert testSemp;
       
       User user1 = new User();
       String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
       user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
       system.runAs(User1){
       
       
    
      PageReference pageRef = Page.Mercer_PrintScopeIt;  
      Test.setCurrentPage(pageRef);
      ApexPages.CurrentPage().getparameters().put('id', testOpportunity.id);
      ApexPages.StandardController sc = new ApexPages.standardController(testOpportunity);
      AP86_pdfGenerator t = new AP86_pdfGenerator(sc);
      
      
      
      
      }
  }
  
  
    
    
    
}