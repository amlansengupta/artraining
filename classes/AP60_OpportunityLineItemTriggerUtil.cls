/*Purpose: This class contains static methods for OLI Trigger operations Before&After which are used by trgger TRG18
==============================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR             DATE        DETAIL 
   1.0 -    Shashank           06/03/2013  Created this util class
   2.0      Reetika,Sarbpreet  07/19/2012  Created logic to concatenate all the LOBs from the products associated with an opportunity.
   3.0      Gyan               12/11/2013  As per Req- 3485 added logic to populate concatenateProduct Field on Opportunity.
   4.0-     Sarbpreet          12/16/2013  As per Request# 3620 added logic to prevent users from deleting all related product records if opportunity is closed / won. 
   5.0      Jagan              3/14/2014   Update related to scope it project
   6.0-     Sarbpreet          05/06/2014  PMO#3566(June Release) : Updated the code to refer it to current currency rates rather than dated currency  
   7.0-     Sarbpreet          06/1/2015  As part of July Release (PMO# 6454) updated logic to calculate the value of  Sales Price(USD) (Calc) field.         
   8.0-     Venkat             06/14/2015  As part of Req #6244, added logic to update the executeOppValidationsBasedOnStage to true whenever the class is called
   9.0-     Sarbpreet          08/12/2015  As part of Request #5422 added logic to calculate the value of DS: Budgeted Net BPs field on Opportunity product
   10.0-    Trisha             01/18/2019  Request #12638 Replacing step with stage
============================================================================================================================================== 
*/


public class AP60_OpportunityLineItemTriggerUtil 
{
    /*
     * @Description : This method is called on before insertion of OpportunityLineItem. It calculates the Expected Product Sales Price(USD) 
     * Calculates Expected Product Sales Price(USD) and fetch the dated exchange rates.
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    Public Static Boolean executeOppValidationsBasedOnStage;         
    Public AP60_OpportunityLineItemTriggerUtil(){
        //As soon as the constructor is called, set the executeOppValidationsBasedOnStage variable to true. This variable is referenced in TRG02_OpportunityTrigger to check if any changes have happened on Opportunity Items.
        executeOppValidationsBasedOnStage = true; 
    }    
    public static void processOpportunityProductonInsert(List<OpportunityLineItem> triggernew)
    {
        try
        {
            //set variable to store Opportunity Ids
            set<string> opportunityIdSet = new set<string>();
            //set variable to store currency ISO Codes
            set<string> ISOCodeSet = new set<string>();
            //List variable to store exchange rates (Updated as part of PMO#3566(June Release))
            List<CurrencyType> exchangeRates = new List<CurrencyType>();
            //map variable to store Opportunity id and Opportunity record
            Map<string, Opportunity> opportunityMap = new Map<string, Opportunity>(); 
            
            //Added as part of Request # 5422(Sept 2015 Release)
            Set<String> oppMarket = new Set<String>();
            
            //iterate over trigger.new
            for(OpportunityLineItem lineItem:triggernew)
            {
                //add opportunity id to set
                opportunityIdSet.add(lineItem.OpportunityId);
            }
            System.debug('1*****'+opportunityIdSet);
            
            //fetch opportunities
            //Added as part of Request # 5422(Sept 2015 Release) added Opportunity_Market__c field in the query
            for(Opportunity opp:[select Id, CurrencyISOCode, Probability, CloseDate, Opportunity_Market__c from Opportunity where Id IN :opportunityIdSet])
            {
                //add currency ISO Codes to set
                ISOCodeSet.add(opp.CurrencyISOCode);
                //add Opportunity id and Opportunity record to map
                opportunityMap.put(opp.Id, opp);
       
            }
             System.debug('2*****'+opportunityMap);
            
            //if set contains values, fetch the dated exchange rates
            if(!IsoCodeSet.isEmpty())
                    exchangeRates = getDatedExchangeCurrencies(IsoCodeSet);
                    
             
            //Added as part of Request # 5422(Sept 2015 Release)
            for(Id oid:opportunityMap.keyset())
            {
                Opportunity opp =  opportunityMap.get(oid);
                oppMarket.add(opp.Opportunity_Market__c);
            }
               
           
            List<Target_Margin__c> trgMktList = ([Select id, name, Budgeted_Net_BPs__c, Market__c from Target_Margin__c where Market__c IN :oppMarket]);
            Map<String, Target_Margin__c> trgtMrgnMap = new Map<String, Target_Margin__c>();
          
            for(Target_Margin__c tm : trgMktList)
            {
                trgtMrgnMap.put(tm.Market__c, tm); 
            }                     
            //End of of Request # 5422(Sept 2015 Release)
                    
            //iterate over trigger.new
            for(OpportunityLineItem lineItem:triggernew)
            {
                 
                //fetch the related opportunity from map
                if(opportunityMap.containskey(lineItem.OpportunityId)){
                 Opportunity opp = opportunityMap.get(lineItem.OpportunityId);
                if(opp!=null && opp.CurrencyIsoCode<>null&&opp.CloseDate<>null)
                { 
                    //iterate over  exchange rates (Updated as part of PMO#3566(June Release))
                    for(CurrencyType currencyVal : exchangeRates)
                    {
                        if(currencyVal.IsoCode<>null)
                        {
                            //if opportunity closedate falls in  exchange rate range for that particular ISO Code
                                if(currencyVal.IsoCode == opp.CurrencyIsoCode)
                                {
                                    if(lineItem.UnitPrice<>null) {
                                        //calculate the Expected Product Sales Price(USD)
                                        lineItem.Expected_Product_Sales_Price_USD__c=(lineItem.UnitPrice/currencyVal.ConversionRate)*(opp.Probability/100);
                                        //As part of July Release (PMO# 6454) updated logic to calcualte the value of  Sales Price(USD) (Calc) field.
                                        lineItem.Sales_Price_USD_calc__c=lineItem.UnitPrice/currencyVal.ConversionRate;
                                       //Request # 17676
                                       if(lineItem.CurrentYearRevenue_edit__c!=null)
                                        lineItem.Current_Year_Revenue_Product_USD__c = lineItem.CurrentYearRevenue_edit__c/currencyVal.ConversionRate;
                                        if(lineItem.Year2Revenue_edit__c!=null)
                                        lineItem.Year_2_Revenue_Product_USD__c = lineItem.Year2Revenue_edit__c/currencyVal.ConversionRate;
                                        if(lineItem.Year3Revenue_edit__c!=null)
                                        lineItem.Year_3_Revenue_Product_USD__c = lineItem.Year3Revenue_edit__c/currencyVal.ConversionRate;
                                        lineItem.Annualized_Revenue_Product_USD__c = lineItem.Annualized_Revenue__c/currencyVal.ConversionRate;                                                                           
                                    }   
                                                                  
                                    
                                }
                        }
                    }
                }
                
                 
               if(trgtMrgnMap.containskey(opp.Opportunity_Market__c))
            {
                Target_Margin__c tgMrkt = trgtMrgnMap.get(opp.Opportunity_Market__c);
                // Removed the comment on below line to check 7677 on 26th Feb 2016
                
             lineItem.Target_margin__c = tgMrkt.id;               
            } 
                }
            } 
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured at OpportunityLineItem Trigger processOpporunityBeforeInsert with reason :'+tEx.getMessage()); 
            triggernew[0].adderror('Exception occured at OpportunityLineItem Trigger processOpporunityBeforeInsert with reason :'+tEx.getMessage());   
        }catch(Exception ex)
        {
            System.debug('Exception occured at OpportunityLineItem Trigger processOpporunityBeforeInsert with reason :'+Ex.getMessage());
             triggernew[0].adderror('Exception occured at OpportunityLineItem Trigger processOpporunityBeforeInsert with reason:'+Ex.getMessage()+'. Please Contact your Administrator.'+Ex.getLineNumber());    
        }
    }
    
    /*
     * @Description : This method is called on before updation of OpportunityLineItem. It calculates the Expected Product Sales Price(USD) 
     * @ Args       : Trigger.newMap, Trigger.oldMap
     * @ Return     : void
     */
    
    public static void processOpportunityProductonUpdate(Map<Id, OpportunityLineItem> triggernewMap, Map<Id, OpportunityLineItem> triggeroldMap)
    {
        try
        {
            //set variable to store Opportunity Ids
            set<string> opportunityIdSet = new set<string>();
            //set variable to store currency ISO Codes
            set<string> ISOCodeSet = new set<string>();
            //List variable to store dated exchange rates(Updated as part of PMO#3566(June Release))
            List<CurrencyType> exchangeRates = new List<CurrencyType>();
            
            
            
            //map variable to store Opportunity id and Opportunity record
            Map<string, Opportunity> opportunityMap = new Map<string, Opportunity>();
            
            //iterate over trigger.newMap.values()
            for(OpportunityLineItem lineItem:triggernewMap.Values())
            {
                
                
                //if UnitPrice is changed
                //if(lineItem.UnitPrice<>triggeroldMap.get(lineItem.Id).UnitPrice)
                //add opportunity id to set
                opportunityIdSet.add(lineItem.OpportunityId);
            }
            
            //fetch opportunities
            for(Opportunity opp:[select Id, CurrencyISOCode, Probability, CloseDate from Opportunity where Id IN :opportunityIdSet])
            {
                //add currency ISO Codes to set
                ISOCodeSet.add(opp.CurrencyISOCode);
                //add Opportunity id and Opportunity record to map
                opportunityMap.put(opp.Id, opp);
            }
            
            //if set contains values, fetch the dated exchange rates
            if(!IsoCodeSet.isEmpty())
                    exchangeRates = getDatedExchangeCurrencies(IsoCodeSet);
                    
            //iterate over trigger.newMap.values()
            for(OpportunityLineItem lineItem:triggernewMap.Values())
            {
                ////Iterating on the  Opp Product and adding the opportunities to the map per request# 7677
                
              /*  if(lineItem.DS_Sales_Leader__c != null)
                        {
                            OpportunityIds.add(lineItem.Opportunity.ID);
                        } */
                
                //if UnitPrice is changed
                //if(lineItem.UnitPrice<>triggeroldMap.get(lineItem.Id).UnitPrice)
                //{
                    //fetch the related opportunity from map
                    Opportunity opp = opportunityMap.get(lineItem.OpportunityId);
                    if(opp.CurrencyIsoCode<>null&&opp.CloseDate<>null)
                    {
                        //iterate over dated exchange rates(Updated as part of PMO#3566(June Release))
                        for(CurrencyType currencyVal : exchangeRates)
                        {
                            if(currencyVal.IsoCode<>null)
                            {
                                //if opportunity closedate falls in  exchange rate range for that particular ISO Code
                                    if(currencyVal.IsoCode == opp.CurrencyIsoCode)
                                    {
                                        if(lineItem.UnitPrice<>null)
                                        //calculate the Expected Product Sales Price(USD)                                                                 
                                       lineItem.Expected_Product_Sales_Price_USD__c=(lineItem.UnitPrice/currencyVal.ConversionRate)*(opp.Probability/100);
                                       //As part of July Release (PMO# 6454) updated logic to calcualte the value of  Sales Price(USD) (Calc) field.
                                       lineItem.Sales_Price_USD_calc__c=lineItem.UnitPrice/currencyVal.ConversionRate;
                                       //Request # 17676
                                       if(lineItem.CurrentYearRevenue_edit__c!=null)
                                        lineItem.Current_Year_Revenue_Product_USD__c = lineItem.CurrentYearRevenue_edit__c/currencyVal.ConversionRate;
                                        lineItem.Year_2_Revenue_Product_USD__c = lineItem.Year2Revenue_edit__c/currencyVal.ConversionRate;
                                        lineItem.Year_3_Revenue_Product_USD__c = lineItem.Year3Revenue_edit__c/currencyVal.ConversionRate;
                                        lineItem.Annualized_Revenue_Product_USD__c = lineItem.Annualized_Revenue__c/currencyVal.ConversionRate;                                   
                                    }
                            }
                        }
                    }
                //}
            }
            
           
            
         //Request# 7677 March 2016 release ( Placed this code in after trigger )
            
               //get the list of Sales Credit
            /*    List<Sales_Credit__c> salesCreditList = [Select Id, Opportunity__c, EmpName__c from Sales_Credit__c where Opportunity__c IN: OpportunityIds]; 
                
                //Map  of  Opp id as key and its set of Sales Credit            
                Map<Id,Set<Id>> SalsCreditMap= new Map<Id,Set<Id>>();
                
                 This for loop
                    1. Iterates on the SalesCrdit list(Opps having Opp Product)
                        - > If Opportunity doesnt have any SalesCrdit, creates a new map
                        --> else from the second one, add the SalesCrdit(Opp ID, Employee ID) to the existing list) 
                Set<Id> salesCreditIdSet = new Set<Id>();
                for(Sales_Credit__c sc: salesCreditList){
                    salesCreditIdSet.clear();
                    if(SalsCreditMap.get(sc.Opportunity__c) == null){
                       salesCreditIdSet.add(sc.EmpName__c);
                       SalsCreditMap.put(SC.Opportunity__c,salesCreditIdSet);
                    }else{
                       salesCreditIdSet =  SalsCreditMap.get(sc.Opportunity__c);
                       salesCreditIdSet.add(sc.EmpName__c);
                       SalsCreditMap.put(SC.Opportunity__c,salesCreditIdSet);
                    }
                }
                
                
                //The below code is used to insert sales Credit team
                
               //Map<ID,Colleague__c> colleagueMap = new Map<Colleague__c,ID>([Select Id,EMPLID__c From Colleague__c]);
                Map<String, Id> empIdMap = new Map<String, Id>();
                
                Set<Id> salesCredit = new Set<Id>();
                for(OpportunityLineItem op : triggernewMap.values()){
                    salesCredit = SalsCreditMap.get(op.OpportunityID);  
                    if(!salesCredit.isEmpty() && !salesCredit.contains(op.DS_Sales_Leader__c)){
                        //create a new instance of opportunity team member
                        Sales_Credit__c salesc = new Sales_Credit__c();
                        //assign the new record's owner as team member
                        salesc.EmpName__c = op.DS_Sales_Leader__c;
                        //assign new record's opportunity ID as team member records' opportunity ID
                        salesc.Opportunity__c = op.OpportunityId;     
                        salesc.Percent_Allocation__c = 0;               
                        // add each instance to the list
               
                
                       salesCreditToInsert.add(salesc);  
                    }  
                    }   
                
               if(!salesCreditToInsert.isEmpty())
               {
                   database.insert(salesCreditToInsert);
                }  */
               
            //End of Request# 7677 March 2016 release 
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured at OpportunityLineItem Trigger processOpportunityBeforeUpdate with reason :'+tEx.getMessage());
            triggernewMap.Values()[0].adderror('Exception occured at OpportunityLineItem Trigger processOpportunityBeforeUpdate with reason :'+tEx.getMessage());   
        }catch(Exception ex)
        {
            System.debug('Exception occured at OpportunityLineItem Trigger processOpportunityBeforeUpdate with reason :'+Ex.getMessage());
            triggernewMap.Values()[0].adderror('Exception occured at OpportunityLineItem Trigger processOpportunityBeforeUpdate with reason :'+ex.getMessage()+'. Please Contact your Administrator.');   
        }
    }
    
    /*
     * @Description : This method is called for getting Exchange Date Currencies(Updated as part of PMO#3566(June Release))
     * @ Args       : set of CurrencyIsoCodes
     * @ Return     : List<CurrencyType>
     */
    
    public static List<CurrencyType> getDatedExchangeCurrencies(Set<String> CurrCodes)
    {
        
        //Updated as part of PMO#3566(June Release)
        return [SELECT ISOCode, ConversionRate FROM CurrencyType
        WHERE ISOCode in :CurrCodes ];
    }
    
    
  /*
   * @Description : This method is called on after insertion, after updation and after deletion of an Opportunity Line Item record
                    and concatenates all the LOBs of all the products associated with an Opportunity and stores them in a field                 
   * @ Args       : List<OpportunityLineItem>   
   * @ Return     : void
   */
   public static void processOliAfterTrigger(List<OpportunityLineItem> opList,Boolean isDelete )
   {
       System.debug('In Trigger');
          //Create a static variable for access in Opportunity Trigger 
          // Set variable to store IDs of opportunities for all Opportunity Line Items in trigger.new
          Set<Id> opIdSet = new Set<Id>();
          // List variable of Opportunity   
          List<Opportunity> lstOppUpdate = new List<Opportunity>() ;
          // Set variable to store the IDs of Price Book Entry 
          Set<Id> pbSet = new Set<Id>();
          // Map variable to store the ID and Names of Price Book Entry
          Map<ID,PriceBookEntry> pbMap = new Map<ID,PriceBookEntry>();
          // Map variable to store the Product Id and Product LOB(i.e Concatenated LOBs)
          Map<String,String> prMap = new  Map<String,String>();
          
          //List variable to store SalesCredits inserted per request# 7677
            List<Sales_Credit__c> salesCreditToInsert = new List<Sales_Credit__c>();
            
            //To hold Set of Opportunity IDs having Opp Products with DS: Sales Leader
            Set<Id> OpportunityIds = new Set<Id>();
          
          // Map variable to store Opportunity Line Item Id and an Integer variable
          Map<Id,Integer> oppToDelete = new Map<Id,Integer>() ; 
          Integer i=0;
          
          //Map vriable for storing ID and Name opportunity
          //Map<Id,Opportunity> oppMap =new Map<ID,Opportunity>([select  ID,Opportunity_ID__c,Name,Product_LOBs__c , Count_Product_LOBs__c,Concatenated_Products__c,Pricebook2Id ,StageName,Opp_Number_of_LOBs__c,IsClosed,IsWon,(Select Id,OpportunityId,LOB__c, PriceBookEntryId,PricebookEntry.Product2.Name  FROM OpportunityLineItems ) from Opportunity where Id IN :opIdSet ]);
          
          //Added as part of Request # 5422(Sept 2015 Release)
        
          
          
          // Iterate through Opportunity Line Items and fetch the Opportunity IDs and add them in a set.  
          for(OpportunityLineItem oli : opList)
          {
              opIdSet.add(oli.OpportunityId);
               if(oppToDelete.get(oli.OpportunityId)!=null  ) {
                  oppToDelete.put(oli.OpportunityId , oppToDelete.get(oli.OpportunityId)+1) ;
               } else {
              
                  oppToDelete.put(oli.OpportunityId , 1) ;
              }  
              
               ////Iterating on the  Opp Product and adding the opportunities to the map per request# 7677
                
                if(oli.DS_Sales_Leader__c != null)
                        {
                            OpportunityIds.add(oli.OpportunityID);
                        }           
                            
          }
          
         
          //Map vriable for storing ID and Name opportunity  
          Map<Id,Opportunity> oppMap =new Map<ID,Opportunity>([select  ID,Opportunity_ID__c,Name,Product_LOBs__c ,Opportunity_Market__c, Count_Product_LOBs__c,Concatenated_Products__c,Pricebook2Id ,StageName,Opp_Number_of_LOBs__c,IsClosed,IsWon,(Select Id,OpportunityId,LOB__c, PriceBookEntryId,PricebookEntry.Product2.Name FROM OpportunityLineItems ) from Opportunity where Id IN :opIdSet ]);                 
          system.debug('oppMap.keyset is ' + oppMap.keyset());
          
          // Iterate through opportunity map and fetch the Opportunity IDs
          for(Id oid:oppMap.keyset())
         {
             Opportunity opp =  oppMap.get(oid);           
             if(opp.OpportunityLineItems!=null && opp.OpportunityLineItems.size()>0) 
             {
                 opp.Product_LOBs__c = '';
                 opp.Count_Product_LOBs__c = 0;                 
                 opp.Concatenated_Products__c = '';  
                 // Iterate through Opportunity Line Items associated with the Opportunity and concatenate the LOBs of all the products and store them in Product_LOBs__c field
                 for(OpportunityLineItem oli : opp.OpportunityLineItems)
                 { 
                   //  opp.stageName='Pending Project';
                     // Check if newly added product /lob already exist in list or Not?
                     if(oli.LOB__c<>null && !opp.Product_LOBs__c.contains(oli.lob__c))
                      {
                        opp.Product_LOBs__c += oli.lob__c +'; ';
                        opp.Count_Product_LOBs__c += 1;
                     }
                     if(oli.PricebookEntry.Product2.Name<>null && !opp.Concatenated_Products__c.contains(oli.PricebookEntry.Product2.Name)){
                        //++ Code Added By Gyan Under 3485 
                        //Logic to Not add ; in last of String if it contains only one value.
                       if(opp.Concatenated_Products__c.length()!= 0)
                        opp.Concatenated_Products__c += ';'+oli.PricebookEntry.Product2.Name;
                       else
                          opp.Concatenated_Products__c += oli.PricebookEntry.Product2.Name;
                     }
                 }
                 lstOppUpdate.add(opp); 
             } 
             else
             {
                  boolean isOpUpdate = false ; 
                  if(opp.Product_LOBs__c <> null && opp.Product_LOBs__c<>'' ) {
                    opp.Product_LOBs__c = '' ;
                    opp.Count_Product_LOBs__c = 0;
                    isOpUpdate = true ;  
                  }                  
                  if(opp.Concatenated_Products__c <> null && opp.Concatenated_Products__c<>'') {
                     opp.Concatenated_Products__c = '' ;             
                     isOpUpdate = true;
                  } 
                              
                  if(isOpUpdate) {
                      lstOppUpdate.add(opp);
                  }
             }          
         }
                                             
         //Request# 7677 March 2016 release 
         System.debug('Dhanusha here 1');
         for(OpportunityLineItem lineItem : opList)
            {
                ////Iterating on the  Opp Product and adding the opportunities to the map per request# 7677
                
                if(lineItem.DS_Sales_Leader__c != null)
                        {
                            OpportunityIds.add(lineItem.OpportunityID);
                        }
                }
            
               //get the list of Sales Credit
               if(!OpportunityIds.isEmpty()) {
               List<Sales_Credit__c> salesCreditList = [Select Id, Opportunity__c, EmpName__c from Sales_Credit__c where Opportunity__c IN: OpportunityIds]; 
                
                //Map  of  Opp id as key and its set of Sales Credit            
                Map<Id,Set<Id>> SalsCreditMap= new Map<Id,Set<Id>>();
                
               /*  This for loop
                    1. Iterates on the SalesCrdit list(Opps having Opp Product)
                        - > If Opportunity doesnt have any SalesCrdit, creates a new map
                        --> else from the second one, add the SalesCrdit(Opp ID, Employee ID) to the existing list) */
                        
                Set<Id> salesCreditIdSet = new Set<Id>();
                for(Sales_Credit__c sc: salesCreditList){     
                    if(SalsCreditMap.get(sc.Opportunity__c) == null){
                       salesCreditIdSet.clear();
                       salesCreditIdSet.add(sc.EmpName__c);
                       SalsCreditMap.put(SC.Opportunity__c,salesCreditIdSet);
                    }else{
                       salesCreditIdSet =  SalsCreditMap.get(sc.Opportunity__c);
                       salesCreditIdSet.add(sc.EmpName__c);
                       SalsCreditMap.put(SC.Opportunity__c,salesCreditIdSet);
                    }
                }
                System.debug('Dhanusha here 2' + SalsCreditMap);
                
                //The below code is used to insert sales Credit team
                
               //Map<ID,Colleague__c> colleagueMap = new Map<Colleague__c,ID>([Select Id,EMPLID__c From Colleague__c]);
              //  Map<String, Id> empIdMap = new Map<String, Id>();
                salesCreditToInsert.clear();
                Set<Id> salesCredit = new Set<Id>();
                for(OpportunityLineItem op : opList){
                   if(op.DS_Sales_Leader__c != null)
                   {
                        salesCredit = SalsCreditMap.get(op.OpportunityID);  
                        If(salesCredit != null && !salesCredit.isEmpty()){
                            if(!salesCredit.contains(op.DS_Sales_Leader__c)){
                                System.debug('Dhanusha here If1' + op.DS_Sales_Leader__c + '-----' + salesCredit);
                                //create a new instance of opportunity team member
                                Sales_Credit__c salesc = new Sales_Credit__c();
                                //assign the new record's owner as team member
                                salesc.EmpName__c = op.DS_Sales_Leader__c;
                                //assign new record's opportunity ID as team member records' opportunity ID
                                salesc.Opportunity__c = op.OpportunityId;     
                                salesc.Percent_Allocation__c = 0;               
                                // add each instance to the list
                               salesCreditToInsert.add(salesc);  
                           }else{
                               System.debug('Dhanusha here If2');
                               continue;
                           }
                        }else{
                            System.debug('Dhanusha here If3');
                            //create a new instance of opportunity team member
                            Sales_Credit__c salesc = new Sales_Credit__c();
                            //assign the new record's owner as team member
                            salesc.EmpName__c = op.DS_Sales_Leader__c;
                            //assign new record's opportunity ID as team member records' opportunity ID
                            salesc.Opportunity__c = op.OpportunityId;     
                            salesc.Percent_Allocation__c = 0;               
                            // add each instance to the list
                            salesCreditToInsert.add(salesc);  
                        }
                    }                  
                
                }
               
               if(!salesCreditToInsert.isEmpty())
               {
                   database.insert(salesCreditToInsert);
                }  
   }  
            //End of Request# 7677 March 2016 release 
         
         
         // As part of Request# 3620 (January Release)added logic to prevent users from deleting all related product records if opportunity is closed / won. 
          if(isDelete == true)
          {
              for(OpportunityLineItem op: opList) {
                      
                 Opportunity opp =  oppMap.get(op.OpportunityId);
                 if(opp!=null && opp.IsClosed  ==true && opp.IsWon==true) {
                     Integer oppToDel =  oppToDelete.get(opp.id) ;
                     if(opp.Opp_Number_of_LOBs__c == oppToDel ) {
                            opList[i].adderror(Label.Opportunity_Product_Delete_Error );
                     }
                 }
             
                 i++ ;
           
              }
          }
       if(!lstOppUpdate.isEmpty())
        {
            try{
                database.update(lstOppUpdate) ;
                //Added Boolean flag as part of fix for Defect#11204 by Malini Prakash
                 ConstantsUtility.oppLineItemUpdateSTOP = false;
                 }catch(Exception e){}
        }
   }
   
   /* Scope it project related udpates **/
   public static void ScopeitProjectUpdates(List<OpportunityLineItem> triggernew, Map<id,opportunityLineItem> triggeroldmap){
       
       Set<Id> prodIdset = new set<Id>();
       Set<Id> oldprodIdset = new set<Id>();
       Set<Id> opIdSet  = new set<Id>();
       List<OpportunityLineItem> scopableOli= new List<OpportunityLineItem>();
       Set<Id> salesPriceUpdatedSet  = new set<Id>();
       Map<Id,opportunitylineitem> oliMap = new Map<Id,opportunitylineitem>();   
       Boolean isScopable = false;
       
       if(trigger.isDelete){
          for(Id oli : triggeroldmap.keySet())
              oldprodIdset.add(triggeroldmap.get(oli).id);
              
       }
       
       if(trigger.isInsert || trigger.isUpdate){
       
           for(OpportunityLineItem oli : triggernew){
               system.debug('Scope required...'+oli.Scope_Reqd__c); 
               if(oli.Scope_Reqd__c){
                   prodIdset.add(oli.pricebookentryid);
                   isScopable = true;
                   scopableOli.add(oli);
               }
               opIdSet.add(oli.opportunityid);
               if(trigger.isUpdate && oli.unitprice != triggeroldmap.get(oli.id).unitprice){
                   salesPriceUpdatedSet.add(oli.id);
                   oliMap.put(oli.id,oli);
               }
           }
           
           if(scopableOli.size()>0){
               //Map vriable for storing ID and Name opportunity
               Map<Id,Opportunity> oppMap =new Map<ID,Opportunity>([select  ID,Opportunity_ID__c from Opportunity where Id IN :opIdSet ]);
               Map<id, PriceBookEntry> PriceBookMap = new Map<id, PriceBookEntry>([select Product2.Name,Product2id from pricebookentry where id=:prodIdset]);
               
               /* Start of code to insert project */
               if(trigger.isInsert){
                   List<ScopeIt_Project__c> insScopProjLst = new list<ScopeIt_Project__c>();
                   for(OpportunityLineItem oli : scopableOli){
                          ScopeIt_Project__c objProject = new ScopeIt_Project__c();
                          String prodName = PriceBookMap.get(oli.PricebookEntryid).Product2.Name+': '+oppMap.get(oli.opportunityid).opportunity_id__c+' - Project';
                          if(prodName.length()<68)
                              objProject.Name = PriceBookMap.get(oli.PricebookEntryid).Product2.Name;
                          else
                              objProject.Name = PriceBookMap.get(oli.PricebookEntryid).Product2.Name.subString(0,45)+': '+oppMap.get(oli.opportunityid).opportunity_id__c+' - Project';
                          objProject.Sales_Price__c=oli.unitprice;
                          objProject.OpportunityProductLineItem_Id__c = oli.id;
                          objProject.CurrencyIsoCode = oli.CurrencyIsoCode;
                          objProject.Product__c = PriceBookMap.get(oli.PricebookEntryid).Product2Id;
                          objProject.Opportunity__c = oli.opportunityid;
                          insScopProjLst.add(objProject);
                   }
                   if(insScopProjLst.size()>0)
                       Database.insert(insScopProjLst);
                       //Added Boolean flag as part of fix for Defect#11204 by Malini Prakash
                       ConstantsUtility.oppLineItemUpdateSTOP1 = false;
               }
               
               /* End of code to insert project */
               
               if(trigger.isUpdate)
               {
          // Added below IF condition as part of Defect # 11112
                   if(salesPriceUpdatedSet.size()>0)
                   {
                      List<ScopeIt_Project__c> lstProjects= [select id,Sales_Price__c,OpportunityProductLineItem_Id__c from ScopeIt_Project__c where OpportunityProductLineItem_Id__c=:salesPriceUpdatedSet];
                      
                      for(ScopeIt_Project__c scopProj: lstProjects){
                          scopProj.Sales_Price__c = olimap.get(scopProj.OpportunityProductLineItem_Id__c).unitPrice;
                      }
                      database.Update(lstProjects,false);
                      //Added Boolean flag as part of fix for Defect#11204 by Malini Prakash
                   } 
                 ConstantsUtility.oppLineItemUpdateSTOP1 = false;
               }
           }
       
       }
          
       /* Added below IF condition for opportunity product remapping for datalods */ 
       if ((trigger.isDelete) && (Label.Opp_Product_ScopingDelete == 'TRUE')) {
               List<ScopeIt_Project__c> lstProjects= [select id,Sales_Price__c,OpportunityProductLineItem_Id__c from ScopeIt_Project__c where OpportunityProductLineItem_Id__c=:oldprodIdset];
               for(Id oli:triggeroldmap.KeySet()){
                   if(lstProjects.size()>0)                    
                       triggeroldmap.get(oli).addError('A Scopeit project is assosiated with this product. Please delete the project first (or) save as template and then delete the product.');
               }
               
       }
       
       
   }
   
   /* End of Scopeit Project Updates*/
          public static void chatterpost(List<opportunitylineitem> OLI){
        integer count=0;
        List<opportunitylineitem> oppli  = [select Project_Linked__c, opportunityId from OpportunityLineItem where opportunityId =:OLI[0].opportunityId];
        system.debug('#####' + oppli);
        for(opportunitylineitem opli: oppli){
        if(opli.Project_Linked__c == TRUE){
            count++;
        }       
        }
        system.debug('#####' + count);
        if(count == oppli.size()){
        /**** Request :12638 Removing step__c from query and adding stageName Start****/ 
        //opportunity opp = [Select id, Step__c, OwnerId, Name from opportunity where id=:OLI[0].opportunityId];
        opportunity opp = [Select id, stageName, OwnerId, Name from opportunity where id=:OLI[0].opportunityId];
        /**** Request :12638 Removing step__c from query and adding stageName End****/ 
        user use = [Select Name from user where Id=:opp.OwnerId];
        //opp.Step__c = 'Closed / Won (SOW Signed)';
        //update opp;
            FeedItem oppPost = new feeditem();
               oppPost.CreatedById= label.MercerForce_Id;
               oppPost.ParentId = opp.Id;
               oppPost.Body = Use.Name + ' Congratulations on winning your opportunity ' + opp.Name + '. Your opportunity has now closed as won. Please ensure your project team record their time to the correct project.';
               oppPost.title = 'opp line item posted: ';
               insert oppPost;
               system.debug('-- opportunity inserted--');
               
            }
    }
 
 }