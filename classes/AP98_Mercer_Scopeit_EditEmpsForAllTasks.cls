/*
*This Class is created as a Part of Requirement 3729 for Scope_it Requirements
*/
public with sharing class AP98_Mercer_Scopeit_EditEmpsForAllTasks {
     string PROJECT_ID='projectId';
     string SLASH='/';
     string G_ID='id';
     string OBJ='object';
     String projectId;
    //List variable to store Employee Hours Distribution
    public List<Scope_Modeling_Employee__c> ScpModEmpLst {get; set;}
    
    public List<ScopeIt_Employee__c> EHList{get; set;}
    
    // List variable to store updated Employee Hours Distribution
    public List<ScopeIt_Employee__c> EmployeeHoursList {
    get {
            if (EmployeeHoursList == null) {
            EmployeeHoursList = new List<ScopeIt_Employee__c>();
            } return EmployeeHoursList;
        }
        set;} 
        
    public String objectType {get;set;}
    
    /* 
    This Method is used to Edit Employees for all respetive Tasks 
    */
    public AP98_Mercer_Scopeit_EditEmpsForAllTasks() {
        
        objectType = system.currentPageReference().getParameters().get('object');
        projectId =  system.currentPageReference().getParameters().get('id');
        // Fetch ScopeIt_Task__c Distributions
        if(objectType == null){
              EHList = [select Id, Employee_Bill_Rate__c, ScopeIt_Task__c,
            ScopeIt_Task__r.task_name__c,Employee_Bill_Rate__r.Name,Hours__c,Level__c,Business__c,Market_Country__c from ScopeIt_Employee__c where
             ScopeIt_Project__c =:projectId order by ScopeIt_Task__r.name limit 50000];
         
               EmployeeHoursList.addALL(EHList);
             
        }
        else{
            ScpModEmpLst = new List<Scope_Modeling_Employee__c>();
            for(Scope_Modeling_Employee__c ModEmp: [select Employee_Bill_Rate__c, Scope_Modeling_Task__c,
            Scope_Modeling_Task__r.task_name__c,Employee_Bill_Rate__r.Name,Hours__c,Level__c,
            Business__c,Market_Country__c from Scope_Modeling_Employee__c
             where Scope_Modeling__c =:projectId order by Scope_Modeling_Task__r.name limit 50000])
                ScpModEmpLst.add(ModEmp);        
        }
    }
    
    /*
    Method to save updated ScopeIt_Task__c Distribution records
    */
    public PageReference save(){
        
        try{
            if(objectType == null){
            database.update(EmployeeHoursList);
            }else{
              database.update(ScpModEmpLst);
            }  
             
        }catch(System.Exception e){
          
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
          return null;
        }
        
         return new PageReference('/'+projectId);
    }
   
    /*
    Method to cancel the current page and return to ScopeIt_Task__c Page
    */
    public pageReference cancel()
    {
         return new PageReference('/'+projectId);
    }

 }