public class descobj_acl {

    public String obj=null;
    public String contType {get;set;}
    public Map<String, Schema.SObjectType> gd = new Map<String, Schema.SObjectType>();
    public String getobjects(){
        return obj;
    }
    public boolean getshowtab(){
        if(obj<>null && obj<>'--None--')
            return true;
        else
            return false;
    }
    
    public void setobjects(String obj){
        this.obj=obj;
    }
    
    public descobj_acl(){
        contType = system.currentPageReference().getParameters().get('contentType');
        gd = Schema.getGlobalDescribe();
    }
    public List<SelectOption> getobjnames(){
        
        List<SelectOption> options = new List<SelectOption>();
        List<String> l1=new List<String>();
        
        for(Schema.SObjectType s : gd.values()) {
            String x=''+s;
            l1.add(x);            
        }
        l1.sort();
        options.add(new SelectOption('--None--','--None--'));
        for(Integer i=0;i<l1.size();i++)
            options.add(new SelectOption(l1[i],l1[i]));

        return options;
    }
    
    public List<Schema.DescribeFieldResult> getfieldtypes(){
         
         
         List<Schema.DescribeFieldResult> ls=new List<Schema.DescribeFieldResult>();
         
         if(obj<>null && obj<>'--None--'){
             
             Map<String, Schema.SObjectField> fieldMap = gd.get(obj.toLowerCase()).getDescribe().fields.getMap();
             for (String fieldName: fieldMap.keySet()) {
                 Schema.DescribeFieldResult F =fieldMap.get(fieldName).getDescribe();
                 ls.add(F);
             }
         }
        
         return ls;
         
         //Schema.DescribeSObjectResult r =Schema.describeSObject('Account');
                 
         //for(Schema.SObjectField s : fieldMap .values()) 
         //ss.add(''+s) ;
            
    }
    
    public Schema.DescribeSObjectResult getobjdes(){
    
        Schema.DescribeSObjectResult fMap;  
        
        system.debug('getting object description for ......  '+obj);
        system.debug('gd size......  '+gd.keyset());
        if(obj<>null && obj<>'--None--'){
           fMap = gd.get(obj.toLowerCase()).getDescribe();
            return fMap;
        }
        else
        return null;
    }
    List<innercl> lic=new List<innercl>();
    
    public List<innercl> getchildrelvals(){
        try{
            List<Schema.ChildRelationship> lcr;
            if(obj<>null && obj<>'--None--'){
                lcr=gd.get(obj).getDescribe().getChildRelationships();
                
                for(Integer i=0;i<lcr.size();i++){
                    innercl ic=new innercl();
                    ic.a=''+lcr[i].getChildSObject();
                    ic.b=''+lcr[i].getField();
                    ic.c=lcr[i].getRelationshipName();
                    ic.d=''+lcr[i].isCascadeDelete();
                    ic.e=''+lcr[i].isDeprecatedAndHidden();
                    lic.add(ic);
                }
                system.debug(lcr);
                return lic;
            }
            else return null;
        }
        catch(Exception e){
            system.debug(e);
            return null;
        }
    }
    
    public class innercl{
        public String a {get; set;}
        public String b {get; set;}
        public String c {get; set;}
        public String d {get; set;}
        public String e {get; set;}
        public innercl(){}
        public innercl(String a, String b, String c, String d, String e){
            this.a=a;
            this.b=b;
            this.c=c;
            this.d=d;
            this.e=e;
        }
    }
    
    public PageReference save(){
        contType = 'application/excel';
        PageReference p=new PageReference('/apex/descobj?contentType=application%2Fvnd.ms-excel#fields.xls');
        return p;
    
    }
    
    /*public String getconttype(){

            return 'application/vnd.ms-excel#'+obj+'.xls';
    
    }   */
    
   /* static testMethod void descobj_acl_test(){
    
        descobj_acl a=new descobj_acl();
        String t1=a.getobjects();
        Boolean t2=a.getshowtab();
        a.setobjects('Account');
        
        List<SelectOption> t3=a.getobjnames();
        List<Schema.DescribeFieldResult> t4=a.getfieldtypes();
        Schema.DescribeSObjectResult t5=a.getobjdes();
        
        descobj_acl.innercl ai=new descobj_acl.innercl();
        descobj_acl.innercl ai2=new descobj_acl.innercl('a','b','c','d','e');
        
        a.getchildrelvals();
        
        a.setobjects('--None--');
        a.getchildrelvals();        
        a.getconttype();
        a.save();
        
    }*/
    
}