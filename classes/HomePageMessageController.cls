public with sharing class HomePageMessageController {
    @AuraEnabled(cacheable = true)
    public static MessageContainer fetchMessageRecord(){
        boolean isLiteProfile = false;
        Profile currentUserProfile = [Select name from Profile where Id=:UserInfo.getProfileId()];
        if(currentUserProfile != null && currentUserProfile.name.contains('Lite') && !currentUserProfile.Name.contains('System Admin')){
            isLiteProfile = true;
        }
        HomePageMessage__c messageObj = [Select Id, Content__c, LiteContent__c from HomePageMessage__c limit 1];
        boolean hasPermissionToEdit = FeatureManagement.checkPermission('AlertEditPermission');
        MessageContainer msgContainer = new MessageContainer(messageObj, hasPermissionToEdit, isLiteProfile);
        return msgContainer;
    }

    public class MessageContainer{
        @AuraEnabled
        public HomePageMessage__c messageObj{get; set;}
        @AuraEnabled
        public boolean hasPermissionToEdit{get; set;}
        @AuraEnabled
        public boolean isLiteProfile{get;set;}

        public MessageContainer(HomePageMessage__c messageObj, boolean hasPermissionToEdit, boolean isLiteProfile){
            this.messageObj = messageObj;
            this.hasPermissionToEdit = hasPermissionToEdit;
            this.isLiteProfile = isLiteProfile;
        }
    }
}