public class TriggerException extends Exception{
	public static final String TOO_MANY_SOQL_ROWS = 'SOQL too many records, Please limit the number of records getting processed';
	public static final String TOO_MANY_DML_ROWS = 'Too many DML Rows processed. Please try with fewer records';
}