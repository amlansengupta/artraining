/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL                   
                                  1.0 -    Arijit Roy 03/29/2013  Created Batch class for User 

============================================================================================================================================== 
*/
global class AP29_UserSyncBatchable implements Database.Batchable<sObject>,Database.stateful{

	//final string to hold the query
    global final String userQuery;
    
    global Map<String, Mercer_Office_Geo_D__c> officeMap = new Map<String, Mercer_Office_Geo_D__c>();
    
    global AP29_UserSyncBatchable(String userQuery)
    {
    	this.userQuery = userQuery;
    	
    	for(Mercer_Office_Geo_D__c officeGeo : [Select Id, Market__c, Office__c, Region__c, Sub_Market__c, Name, Sub_Region__c, Country__c FROM Mercer_Office_Geo_D__c Where isUpdated__c = true])
        {
         	officeMap.put(officeGeo.Name, officeGeo);	
        }
    }
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */     
    global Database.QueryLocator start(Database.BatchableContext BC)
	{

		//execute the query and fetch the records	
        return Database.getQueryLocator(userQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	List<User> userForUpdationList = new List<User>();
    	List<User> userFromScope = new List<User>();
    	userFromScope = (List<User>)scope;
    	
    	for(User usr : userFromScope){
			usr.Market__c = officeMap.get(usr.Location__c).Market__c;
			usr.Region__c = officeMap.get(usr.Location__c).Region__c;
			usr.Sub_Market__c = officeMap.get(usr.Location__c).Sub_Market__c;
			usr.Sub_Region__c = officeMap.get(usr.Location__c).Sub_Region__c;
			usr.Country__c = officeMap.get(usr.Location__c).Country__c;
			usr.Employee_Office__c = officeMap.get(usr.Location__c).Office__c;
			
			//Add the record to a list        
			userForUpdationList.add(usr);
		}	
		
		if(!userForUpdationList.isEmpty()) Database.update(userForUpdationList, false);
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	DateTime sysTime = System.now();
		sysTime = sysTime.addSeconds(360);
		String CRON_EXP = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
		AP32_ResetOfficeSchedulable office = new AP32_ResetOfficeSchedulable();
		System.schedule('Reset Mercer Geo ' + String.valueOf(Date.Today()), CRON_EXP, office);  
    }
}