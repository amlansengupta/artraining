/*Purpose:  This Apex class is used for displaying prompt for Revenue auto adjustment
==============================================================================================================================================
History 
----------------------- 
VERSION          AUTHOR                                    DATE        DETAIL 
   1.0 -              Shashank Singhal            1/21/2013   Created a controller for Mercer_RevenueAutoAdjustmentPrompt
  
============================================================================================================================================== 
*/


public with sharing class MercVFC07_RevenueAutoAdjustmentCont {
    
    public Opportunity opp{get;set;}
    
    public MercVFC07_RevenueAutoAdjustmentCont(ApexPages.standardController controller)
    {
        this.opp = (Opportunity)controller.getRecord(); 
    }

}