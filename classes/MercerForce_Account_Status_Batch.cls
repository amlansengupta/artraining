/*
*@Description: This is to update the MercerForce Account Status Field as per the qualification Criteria

Open Opportunity - Stage 2+  ===>>  Has an open opportunity with stage 2 or higher
Open Opportunity - Stage 1   ===>>  Has an open opportunity with stage 1 AND 
                                    Status is not above
Recent Win                   ===>>  Has a opportunity closed/won within the last 6 months and 
                                    Status is not all of the above
High Revenue                 ===>>  Total revenue of greater than or equal to USD100K (all LOBS, prior and current year) and 
                                    Status is not all of the above
Low Revenue                  ===>>  Total revenue of less than USD100K (all LOBS, prior and current year) and 
                                    Status is not all of the above
Active Marketing Contact     ===>>  Has an Active contact and Contact Is associated with a Distribution or a Campaign and 
                                    Status is not all of the above
Active Contact               ===>>  Has an Active contact and 
                                    Status is not all of the above
Activities Exist             ===>>  Has an Activity record and 
                                    Status is not all of the above
Competitor                   ===>>  Record is a competitor and has no other activity and 
                                    Status is not all of the above
No Activity                  ===>>  None of the above and                                     
No Activity - OBSOLETE       ===>>  (Indicates "No Activity" records with no activity whatsoever for 3+ years)  and 
                                    Status = "No Activity" and 
                                    (Opps - Maximum Create Date > 3 years old or no opps exist OR 
                                    Opps - Maximum Date Decided > 3 years old or no opps exist OR 
                                    Contacts - Maximum Create Date > 3 years old or no contacts exist OR 
                                    Activities - Maximum Create Date > 3 years old or no activities exist)


==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Jagan Gorre    11/17/2014           Batch class for updating Account Status    
============================================================================================================================================== 
*/
global class MercerForce_Account_Status_Batch implements Database.Batchable<sObject>, Database.Stateful{
    
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();

    global Date stDate = system.today() - 731;
    
    /* Main Batch query is stored in Custom Label MercerForceBatchQuery
    *  In order to run this query only on specific accounts please update the custom label with below text as it is (after replacing id's)
    *  
    *  Select (Select Id from events), (select id from Tasks),Id,MercerForce_Account_Status__c,Total_CY_Revenue__c, Total_PY_Revenue__c, Competitor_Flag__c, CreatedDate FROM Account where id in ('001E000000Z8iox','001E000000Z8cMN')
    */
    global static String query = Label.MercerForceBatchQuery;
    
    /*
     *  Method Name: start
     *  Description: Execute the query and return the pointer to execute method
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(Test.isRunningTest())
        {
            String accname= 'TestAccount' + String.valueOf(Date.Today());  
            
            query = 'select (Select Id from events), (select id from Tasks),Id,MercerForce_Account_Status__c,Total_CY_Revenue__c, Total_PY_Revenue__c, Competitor_Flag__c, CreatedDate FROM Account Where name =\'TestAccountName\''; 
        }
         System.debug('\n query prepared : '+query);
         return Database.getQueryLocator(query); 
    }
    
    /*
     *  Method Name: execute
     *  Description: Start executing logic for the current batch
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        List<Account> accForUpdate = new List<Account>();
        Set<Id> AccountIds = New Set<Id> (); 
        Set<Id> ConIds = New Set<Id> (); 
        
        /*To build a new map with status values for each account*/
        Map<Id,String> AccStatMap = new Map<Id,String>();
        
        /*To store the existing status values*/
        Map<Id,String> ExistingAccStatusMap = new Map<Id,String>();
         
        for(Account a :(List<Account>)scope) {
            AccountIds.add(a.Id); 
            ExistingAccStatusMap.put(a.id,a.MercerForce_Account_Status__c);
        }
        
        List<Opportunity> AllOpportunities = new List<Opportunity>();        
        List<Contact> AllContacts = new List<Contact>();
        
        //Query all the opportunties for the corresponding accounts of current batch
        AllOpportunities  = [ SELECT Id, createdDate, Accountid, StageName, CloseDate, OpportunityCreatedAge__c, ClosedOpportunityAge__c 
                                 FROM Opportunity WHERE Accountid IN :AccountIds ] ;
        
        //Query all the contacts for the corresponding accounts of current batch
        AllContacts = [Select id,Accountid,CreatedDate,Contact_Status__c,ContactCreatedAge__c from Contact where Accountid in :AccountIds];
        
        Date ThreeYearsAgo = system.today() - 1095;
        
        String MFAccStOppStage2 = ApexConstants__c.getInstance('MFAccStOppStage2').StrValue__c;
        String MFAccStOppStage1 = ApexConstants__c.getInstance('MFAccStOppStage1').StrValue__c;
        String MFAccStRecentWin = ApexConstants__c.getInstance('MFAccStRecentWin').StrValue__c;
        String MFAccStHighRevenue = ApexConstants__c.getInstance('MFAccStHighRevenue').StrValue__c;
        String MFAccStLowRevenue = ApexConstants__c.getInstance('MFAccStLowRevenue').StrValue__c;
        String MFAccStActMarkCont = ApexConstants__c.getInstance('MFAccStActMarkCont').StrValue__c;
        String MFAccStActCon = ApexConstants__c.getInstance('MFAccStActCon').StrValue__c;
        String MFAccStActvtExt = ApexConstants__c.getInstance('MFAccStActvtExt').StrValue__c;
        String MFAccStCompetitor = ApexConstants__c.getInstance('MFAccStCompetitor').StrValue__c;
        String MFAccNoActivity = ApexConstants__c.getInstance('MFAccNoActivity').StrValue__c;
        String MFAccNoActOBSO = ApexConstants__c.getInstance('MFAccNoActOBSO').StrValue__c;
        
        String OppStageAp = ApexConstants__c.getInstance('OppStageAP').StrValue__c;
        String OppStageFin = ApexConstants__c.getInstance('OppStageFin').StrValue__c;
        String OppStageSel = ApexConstants__c.getInstance('OppStageSel').StrValue__c;
        String OppStagePS = ApexConstants__c.getInstance('OppStagePS').StrValue__c;
        String OppStageATF = ApexConstants__c.getInstance('OppStageATF').StrValue__c;
        String OppStageIde = ApexConstants__c.getInstance('OppStageIde').StrValue__c;
        String OppStageCW = ApexConstants__c.getInstance('OppStageCW').StrValue__c;
        String conStatInactive = ApexConstants__c.getInstance('conStatInactive').StrValue__c;
        String conStatDeceased = ApexConstants__c.getInstance('conStatDeceased').StrValue__c;
        String CampMemStatCompl = ApexConstants__c.getInstance('CampMemStatCompl').StrValue__c;
        String CampMemStatCanc = ApexConstants__c.getInstance('CampMemStatCanc').StrValue__c;
        String OppStagePP = ApexConstants__c.getInstance('OppStagePP').StrValue__c;
        
        /* For all the corresponding opps, 
        *  If the opp is in Active Pursuit/Finalist/Selected/Pending Step stages, mark it as Stage 2 +
        *  If the opp is in Above the Funnel/Identify/Recent Win stages, mark it as Stage 1
        *  If the opp is Closed / Won and if its closed with in six months, mark it as Recent win.
        */ 
        
        for(Opportunity opp:AllOpportunities) {
            Date sixmonths = system.today() - 180;
            
            if(opp.StageName == OppStageAp || opp.StageName == OppStageFin || opp.StageName == OppStageSel || opp.StageName == OppStagePS || opp.StageName == OppStagePP )
                AccStatMap.put(opp.Accountid, MFAccStOppStage2);
            
            else if((opp.StageName == OppStageATF || opp.StageName == OppStageIde) && (!AccStatMap.containsKey(opp.Accountid) || (AccStatMap.containsKey(opp.Accountid) && AccStatMap.get(opp.Accountid) == MFAccStRecentWin )  ))
                AccStatMap.put(opp.Accountid, MFAccStOppStage1);
            
            else if(opp.StageName == OppStageCW && opp.CloseDate > sixmonths && !AccStatMap.containsKey(opp.Accountid))
                AccStatMap.put(opp.Accountid, MFAccStRecentWin);
        }
        
        /* If the CY+PY revenue is > 100000, then mark it as High Revenue
        *  else mark it is low revenue
        */
        for(Account acc :(List<Account>)scope) {
           
            if(!AccStatMap.containsKey(acc.id)){
            
                Double totRev = 0.0;
                Double cyRev = 0.0;
                Double pyRev = 0.0;
                if(acc.Total_CY_Revenue__c <> null)    cyRev = acc.Total_CY_Revenue__c;
                if(acc.Total_PY_Revenue__c <> null)    pyRev = acc.Total_PY_Revenue__c;
                    
                totRev = cyRev + pyRev ;
                
                if(totRev >= 100000.00)    AccStatMap.put(acc.id,MFAccStHighRevenue);
                else if((((acc.Total_CY_Revenue__c<>null && acc.Total_CY_Revenue__c>0) || (acc.Total_PY_Revenue__c<>null && acc.Total_PY_Revenue__c>0 )) && totRev < 100000.00 ))    
                    AccStatMap.put(acc.id,MFAccStLowRevenue);
            
            }
            
        }
        
        /*Active Marketing contact and Active contact scenarios:- Query contacts along with campaign members and distributions*/
        
        for(Contact con:[Select id,Accountid,CreatedDate,Contact_Status__c,ContactCreatedAge__c, (Select Id, ContactId, Status FROM CampaignMembers where Status = :CampMemStatCompl or Status = :CampMemStatCanc), (Select Id FROM Contact_Distributions__r)  from Contact where Accountid in :AccountIds])
           
           /*continue if the Contact status is not inactive and not deceased*/
           if(con.Contact_Status__c != conStatInactive && con.Contact_Status__c != conStatDeceased ){
                
                /* If the account is not part of master map (or) even if it is part of, but has the value 'Active Contact'        
                *                                                  AND 
                *  if the contact has any distributions or campaign members 
                *  then Mark it as Active Marketing Contact
                *  and put the Account in Master map
                */
                if((!AccStatMap.containsKey(con.Accountid) || (AccStatMap.containsKey(con.Accountid) && AccStatMap.get(con.Accountid) == MFAccStActCon ) ) && (con.Contact_Distributions__r.size()>0 || con.CampaignMembers.size()>0))
                    AccStatMap.put(Con.Accountid,MFAccStActMarkCont);
                
                /* if the account is not part of master map then mark it as Active Contact
                *  Note: This will not override Active Marketing Contact value as we are inserting the account in to master map
                *  the above condition
                */
                else if(!AccStatMap.containsKey(con.Accountid))
                    AccStatMap.put(con.Accountid,MFAccStActCon); 
           }
           
        
        /*Activities Exist Scenario*/
        for(Account acc :(List<Account>)scope)
            if(!AccStatMap.containsKey(acc.id))
                if(acc.Events.size()>0 || acc.Tasks.size()>0)
                    AccStatMap.put(acc.id,MFAccStActvtExt);
        
        /*Competitor scenario*/
        for(Account acc :(List<Account>)scope) 
            if(!AccStatMap.containsKey(acc.id) && acc.Competitor_Flag__c == True)
                AccStatMap.put(acc.id,MFAccStCompetitor);
            
        
        /* Need to confirm if we have to use the below checkpoint for No Activity OBSOLETE */
        Date AccCheckPoint = system.today() - 731;
        
        /* Filter accounts that are not processed so far and query only those accounts.
        *  This is to reduce the processing time of the query.
        */
        Set<Id> newAccountids = new Set<id>();
        for(Account acc :(List<Account>)scope) 
            if(!AccStatMap.containsKey(acc.id)) 
                newAccountids.add(acc.id); 
        
        /* Query those accounts that are not passed the above criteria to check for the No Activity and No Activty OBSOLETE conidtions */
        for(Account acc :[select (SELECT ActivityDate, createddate FROM ActivityHistories where createddate>:ThreeYearsAgo),id,CreatedDate, MercerForce_Account_Status__c FROM Account where id in :newAccountids] ) {
            
            Boolean oppExist=false;    
            Boolean conExist=false;
            Boolean actExist=false;
            Boolean oppCreated3YearsAgo = false;
            Boolean oppClosed3YearsAgo = false;
            Boolean conCreated3YearsAgo = false;
            Boolean actCreated3YearsAgo = false;
            
            for(Opportunity opp:AllOpportunities) {
                    If(opp.accountid == acc.id) {
                        oppExist = TRUE;        
                    
                        if(opp.OpportunityCreatedAge__c > 1095)
                            oppCreated3YearsAgo = true;
                    
                        if(opp.ClosedOpportunityAge__c > 1095)
                            oppClosed3YearsAgo = true;
                    } 
                        
            }
                
            for(Contact con:AllContacts) {
                if(con.accountid == acc.id) {
                    conExist = TRUE;        
                    if(con.ContactCreatedAge__c > 1095)
                        conCreated3YearsAgo = TRUE;
                }    
            }
                
            if(acc.ActivityHistories.size()>0)
                actCreated3YearsAgo = true;
                
            /* IF Contacts/Opps/Activites not exist OR
            *     Max opp created date/max opp closed date/Max Contact Created date/Max Activity created date is 3 years ago
            *  THEN update as No Activity OBSOLETE
            *  ELSE NO activity
            */
            if (acc.createdDate < AccCheckPoint && (!oppExist || !conExist || !actExist || oppCreated3YearsAgo || oppClosed3YearsAgo || conCreated3YearsAgo || actCreated3YearsAgo)   )
                AccStatMap.put(acc.id, MFAccNoActOBSO);  
            else 
                AccStatMap.put(acc.id, MFAccNoActivity);  
                
        }
        
        
        accForUpdate.clear();
        
        /* Check the account if the assigned status is same as the current status. If Yes, then skip else
        *  add it to the list to update
        */
        
        for(Account acc :(List<Account>)scope) {
            if(AccStatMap.containsKey(acc.id) && acc.MercerForce_Account_Status__c <> AccStatMap.get(acc.id) ){ 
                acc.MercerForce_Account_Status__c = AccStatMap.get(acc.id);
                accForUpdate.add(acc); 
            }
        }
        
        /* Update all the accounts in the list and collect errors if any */
        
        if(!accForUpdate.isEmpty())  
             for(Database.Saveresult result :  Database.update(accForUpdate, false))
                    if(!result.isSuccess())
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
         
                                 
    }
    
    /*
     *  Method Name: finish
     *  Description: Insert error logs if any. 
    */
    
    global void finish(Database.BatchableContext bc)
    {
       /* If there are any errors during the update proces, insert the logs in to error log object */
       try    {       Database.insert(errorLogs, false);  }
       catch(DMLException dme) {   system.debug('\n exception has occured');  }
    }
    
}