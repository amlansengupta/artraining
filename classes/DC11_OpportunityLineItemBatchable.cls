/* Purpose:This Batchable class mass updates the Opportunity records when an Opportunity Product associated with it is updated.
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Oportunity as an input and mas updates the Opportunity records when an Opportunity Product associated with it is updated.
 
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          07/23/2013  Created Batch Class to populate Product LOBs and Count of LOBs on Opportunity , by updating Opportunity Line Item
 2.0         Sarbpreet          7/7/2014    Added Comments.
 ======================================================================================================================================*/
global class DC11_OpportunityLineItemBatchable implements Database.Batchable<sObject>, Database.Stateful{
    //String Variable to store query
    public String query ;   
    // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    /*
    *   Class Constructor for inistializing the value of 'query' variable 
    */
    public DC11_OpportunityLineItemBatchable(String qStr)
    {
         this.query= qStr;
    }
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {  
         Integer errorCount = 0 ;
     
         //List valiable to store Opportunity Line Items
         List<OpportunityLineItem> OLIList = new List<OpportunityLineItem>();
         system.debug('\n The size of scope is ' + scope.size());
         //Iterate through Opportunity records fetched through 'query'
         for(Opportunity opp : scope)
         {
           // Check if the Opportunity contains Opportunity Line Items
           if(opp.OpportunityLineItems!=null && opp.OpportunityLineItems.size()>0)
           {
                // Iterate through Opportunity Line Items associated with the Opportunity and add them in a list
                for(OpportunityLineItem opplineitem : opp.OpportunityLineItems)  
                {
                     OLIList.add(opplineitem);
                     system.debug('\n First Opportunity Line Item is ' + opp.OpportunityLineItems);
                     break;
                } 
                               
          }
        } 
        
     // List variable to save the result of the Opportunity Line Items which are updated   
     List<Database.SaveResult> srList= Database.Update(OLIList, false);  
     system.debug('\nTotal Opportunity Line Items are ' + OLIList.size());
     //MercerAccountStatusBatchHelper.createErrorLog();
  
     if(srList!=null && srList.size()>0) {
         // Iterate through the saved Opportunity Line Items  
         Integer i = 0;             
         for (Database.SaveResult sr : srList)
         {
                // Check if Oppportunity Line Items are not updated successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) 
                {
                        errorCount++;
                        System.debug('Error DC11_OpportunityLineItemBatchable '+sr.getErrors()[0].getMessage());
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC11:' +sr.getErrors()[0].getMessage(), errorLogs, OLIList[i].Id);  
                }
                i++;
         }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('DC11 Status :scope:' + scope.size() +' OLIList:' + OLIList.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);
     
     insert errorLogStatus;      
    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC)
    {
         // Check if errorLogs contains some failure records. If so, then insert error logs into database.
         if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }    
}