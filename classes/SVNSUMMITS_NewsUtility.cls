/*
 Copyright ©2016-2017 7Summits Inc. All rights reserved.
@Class Name		  : SVNSUMMITS_NewsUtility
@Created by		  :
@Description		 : Apex Utility class used for creating test records
*/

@isTest
global without sharing class SVNSUMMITS_NewsUtility{

	global static user userObj = createUser();
	global static String strNetworkId = '0DB36000000PB5MGAW';

	//create News__c record
	global static News__c createNews() {

		News__c newsObj = new News__c(Name='Test News',Publish_DateTime__c = system.today().addDays(-5),
		Author__c = userObj.Id,NetworkId__c = strNetworkId,Summary__c='Summary News');

		insert newsObj;
		return newsObj;
	}

	//create News__c record with the NetworkID
	global static News__c createNews(String strNetId) {

		News__c newsObj = new News__c(Name='Test News',Publish_DateTime__c = system.today().addDays(-5),
		Author__c = userObj.Id,NetworkId__c = strNetId,Summary__c='Summary News');

		insert newsObj;
		return newsObj;
	}

	//create News__c record for the Bulk Insertion check
	global static List<News__c> createBulkNews(Integer howmany, String strNetId) {

		List<News__c> objNewzList = new List<News__c>();
		for (Integer i =0; i< howMany; i++){
			News__c newsObj = new News__c(Name='Test News'+i,Publish_DateTime__c = system.today().addDays(-5),
			Author__c = userObj.Id,NetworkId__c = strNetId,Summary__c='Summary News');

			objNewzList.add(newsObj);
		}
		insert objNewzList;
		return objNewzList;
	}

	//create Topic record
	global static Topic createTopic() {

		return null;
	}

	//Create Topics in Bulk with the Network Id
	global static List<Topic> createTopic(Integer howMany, String strNetId) {

		List<topic> topics = new List<topic>();

		for (Integer i =0; i< howMany; i++){
			Topic topicObj = new Topic(Name='Test Topic'+i,Description='Test Topic', NetworkId=strNetId);
			topics.add(topicObj);
		}

		if(!topics.isEmpty()){
			insert topics;
		}

		return topics;
	}

	//Assign Topic , create TopicAssignment with TopicId and entittId to which topic is to be assigned
	global static TopicAssignment createTopicAssignment(String strTopicId,String strEntityId) {
		TopicAssignment topicAssignment = new TopicAssignment(EntityId = strEntityId, TopicId = strTopicId);

		insert topicAssignment;

		return topicAssignment;
	}

	//create user record
	global static User createUser(){

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

		User u = new User(Alias = 'standt', Email='standarduser123@testorg.com',
		EmailEncodingKey='UTF-8', LastName='stand user', LanguageLocaleKey='en_US',
		LocaleSidKey='en_US', ProfileId = p.Id,
		TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234@testorg.com');

		insert u;

		return u;
	}

	static final global Id MY_ID = UserInfo.getUserId();
	// Profiles
	static final global String COMPANY_COMMUNITY_PROFILE_NAME =  'Customer Community User';
	static global Id COMPANY_COMMUNITY_PROFILE_Id {
		get{
			if(COMPANY_COMMUNITY_PROFILE_Id  == null){

				List<Profile> profiles = [select Id from Profile where Name = :COMPANY_COMMUNITY_PROFILE_NAME];
				COMPANY_COMMUNITY_PROFILE_Id = profiles[0].id;
			}
			return COMPANY_COMMUNITY_PROFILE_Id;
		}
		set;
	}

	static global String THIS_COMMUNITY_NAME{
		get {
			String commName = '';
			commName = [select Id,Name from Network][0].Name;
			return commName;
		}
	}

	// this is for standard user to know
	static global String NETWORK_ID{
		get{
			if(NETWORK_ID == null){
			  NETWORK_ID = [select Id from Network where Name = :THIS_COMMUNITY_NAME][0].Id;
			}
			return NETWORK_ID;

		}
		set;

	}

	// Used to create more than 1 user at a time...
	private static Integer userCount = 0;

	//Create Community User
	//As we are using custom object News, we have created user with the custom Community Profile,
	//Because standard community profile do not allow to give permissions to such custom objects.
	global static User createCommunityUsers(String profileName) {
		userCount++;

		Colleague__c colleague = new Colleague__c(Name='Colleague Test');
		insert colleague;

		Account a = new Account(name ='TestAccount123', Relationship_Manager__c=colleague.Id) ;
		insert a;

		Contact c = new Contact(LastName ='testCon',AccountId = a.Id, MailingCountry='US', Email='newsCreateUsers@newsCreateUsers.com');
		insert c;

		Profile p = [SELECT Id FROM Profile WHERE Name=: profileName];

			User u = new User(alias = 'Com', email='testtestCommunity@newsUtilTest.com',communitynickname = c.LastName + userCount,
							  emailencodingkey='UTF-8', lastname='Test', languagelocalekey='en_US',
							  localesidkey='en_US', profileid = p.Id, ContactId = c.Id,
							  timezonesidkey='America/Los_Angeles', username='testtestCommunity@newsUtilTest.com' + userCount);

		insert u;
		return u;
	 }

	//Create User with given Profile name
	global static List<User> createUsers(Integer howMany,String profileName){
		Colleague__c colleague = new Colleague__c(Name='Colleague Test');
		insert colleague;

		Account a = new Account(name ='TestAccount123', Relationship_Manager__c=colleague.Id) ;
		insert a;

		List<Contact> listOfContacts = new List<Contact>();
		Map<Integer,Contact> mapCont = new Map<Integer,Contact>();

		for (Integer i =0; i< howMany; i++){
		  Contact c = new Contact(LastName ='testCon'+i,AccountId = a.Id, MailingCountry='US', 	Email='newsCreateUsers' + i + '@newsCreateUsers.com');
		  listOfContacts.add(c);
		  mapCont.put(i,c);
		}
		insert listOfContacts;

		 // by default bidders
		 Id profileId;
		 // to make user unique
		 String type;
		 if(profileName == COMPANY_COMMUNITY_PROFILE_NAME){
			profileId = COMPANY_COMMUNITY_PROFILE_Id;
			type = 'com';
		 }

		 List<User> listOfUsers = new List<User>();

		 for(Integer key : mapCont.keySet()){
			 User u = new User(alias = type + key , email= key + 'testtest@newsUtilTest.com',communitynickname = key+mapCont.get(key).LastName,
				emailencodingkey='UTF-8', lastname='Test' + type + key, languagelocalekey='en_US',
				localesidkey='en_US', profileid = profileId, ContactId = mapCont.get(key).Id,
				timezonesidkey='America/Los_Angeles', username= key +type+ '@test.com');

			 listOfUsers.add(u);

		  }

		 insert listOfUsers;
		 return listOfUsers;
	 }

}