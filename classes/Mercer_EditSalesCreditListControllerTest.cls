/*Test Class For Mercer_EditSalesCreditListController
 * Created By - TCS
 * Date - 4/16/2019  */
@isTest
public class Mercer_EditSalesCreditListControllerTest {
    public static List<Opportunity> listOpp = new List<Opportunity>();
    public static List<Contact> listCon= new List<Contact>();
    public static List<Account> listAcc = new List<Account>();
    public static List<Sales_Credit__c> listSc = new List<Sales_Credit__c>();
    public static List<Colleague__c> listCol = new List<Colleague__c>();
    public static void createAccount(){
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        listAcc.add(testAccount);
        insert listAcc;
    }
    public static void createContact(){
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = listAcc.get(0).Id;
        listCon.add(testContact);
        insert listCon;
    }
    public static void createOpp(){
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.Step__c='Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Buyer__c=listCon.get(0).Id;
        testOppty.AccountId=listAcc.get(0).Id;
        listOpp.add(testOppty);
        insert listOpp;
    }
    public static void createCol(){
         Colleague__c testColleague = new Colleague__c();
                testColleague.Name = 'TestColleague';
                testColleague.Last_Name__c = 'testLastName';
                testColleague.EMPLID__c = '1234567890';
                testColleague.LOB__c = '11111';
                testColleague.Email_Address__c = 'abc@gmail.com';
                listCol.add(testColleague);
                insert listCol;
    }
    public static void createSc(){
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = listOpp.get(0).Id;
        testSales.EmpName__c = listCol.get(0).Id;
        testSales.Percent_Allocation__c = 90;
        testSales.Role__c ='EH&B Specialist';
        listSc.add(testSales);
        insert listSc;
    }
@isTest 
    public static void method1(){
        createAccount();
        createContact();
        createOpp();
        createCol();
        createSc();
        Mercer_EditSalesCreditListController.fetchOppName(listOpp.get(0).Id);
        Mercer_EditSalesCreditListController.fetchSalesCredit(listOpp.get(0).Id);        
        Mercer_EditSalesCreditListController.fetchRole();
        List<Sales_Credit__c> listSC = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id];
        listSC.get(0).Percent_Allocation__c =95;
        Mercer_EditSalesCreditListController.save(JSON.serialize(listSC));
         List<Sales_Credit__c> listSC1 = [select id,Percent_Allocation__c from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id]; 
        System.assertEquals(listSC1.get(0).Percent_Allocation__c,95);
        //Mercer_EditSalesCreditListController.calculateTotal(listSc);
        List<Mercer_EditSalesCreditListController.SelectionWrapper> lstSC = new List<Mercer_EditSalesCreditListController.SelectionWrapper>();
        Mercer_EditSalesCreditListController.fetchLOB(listCol.get(0).Id);
        Mercer_EditSalesCreditListController.SelectionWrapper sw1 = new Mercer_EditSalesCreditListController.SelectionWrapper(listSc.get(0),false);
        lstSC.add(sw1);
        List<Mercer_EditSalesCreditListController.SelectionWrapper> listSales1 = Mercer_EditSalesCreditListController.delSlctRec(JSON.serialize(lstSC));
        System.assertEquals(listSales1.size(),1);
        lstSC.clear();
        Mercer_EditSalesCreditListController.SelectionWrapper sw = new Mercer_EditSalesCreditListController.SelectionWrapper(listSc.get(0),true);
        lstSC.add(sw);
        List<Mercer_EditSalesCreditListController.SelectionWrapper> listSales = Mercer_EditSalesCreditListController.delSlctRec(JSON.serialize(lstSC));
        //List<Sales_Credit__c> listSales = [select id from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id];
        System.assertEquals(listSales.size(),0);
        List<Sales_Credit__c> lsc= new List<Sales_credit__c>();
        Mercer_EditSalesCreditListController.save(JSON.serialize(lsc));
        Mercer_EditSalesCreditListController.fetchOppName(null);
    }
    @isTest 
    public static void method2(){
        createAccount();
        createContact();
        createOpp();
        //Mercer_EditSalesCreditListController.fetchOppName(listOpp.get(0).Id);
        Mercer_EditSalesCreditListController.fetchSalesCredit(listOpp.get(0).Id); 
        List<Sales_Credit__c> listSales = [select id from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id];
        System.assertEquals(listSales.size(),0);
    }
   @isTest
     public static void method4(){
        try {
          createAccount();
        createContact();
        createOpp();
        createCol();
        createSc();
          List<Sales_Credit__c> listSC = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id];
        listSC.get(0).Percent_Allocation__c=null;
        Mercer_EditSalesCreditListController.save(JSON.serialize(listSC));
        } catch (Exception e) {
        List<Sales_Credit__c> listSC = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c where Opportunity__c=:listOpp.get(0).Id];
        System.debug ('all ok');
        system.assertEquals(listSC.get(0).Percent_Allocation__c,90);
        }

    }
     
    @isTest
     public static void method5(){
        try {
        Mercer_EditSalesCreditListController.fetchLOB(null);
        } catch (Exception e) {
        List<Sales_Credit__c> listSC = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c];
        System.debug ('all ok');
        system.assertEquals(listSC.size(),0);
        System.debug ('all ok');
        //throw e;
        }

    }
    @isTest
     public static void method6(){
        try {
        createAccount();
        createContact();
        createOpp();
        createCol();
        createSc();
        List<Sales_Credit__c> listSc = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c];
        Mercer_EditSalesCreditListController.fetchSalesCredit(listSc.get(0).id);
            
        } catch (Exception e) {
        List<Sales_Credit__c> listSC = [select id ,Percent_Allocation__c,Opportunity__c from Sales_Credit__c];
        System.debug ('all ok');
        system.assertEquals(listSC.size(),0);
        System.debug ('all ok');
        //throw e;
        }

    }
    
}