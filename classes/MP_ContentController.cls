public without sharing class MP_ContentController {

	//Event object = Event__c
	//Blog object = News__c
	//News object = News__c
	//Group object = CollaborationGroup

	public static final String RECORD_TYPE_BLOG = 'Blogs';
	public static final String RECORD_TYPE_NEWS = 'News';
	public static final String RECORD_TYPE_META = 'Meta';
	public static final String TYPE_NEWS = 'news';
	public static final String TYPE_BLOG = 'blog';
	public static final String TYPE_META = 'meta';
	public static final String TYPE_EVENT = 'event';
	public static final String TYPE_GROUP = 'group';
	public static final Map<String, String> objectMap = new Map<String, String>{
		TYPE_NEWS => 'News__c',
		TYPE_BLOG => 'News__c',
		TYPE_META => 'News__c',
		TYPE_EVENT => 'Event__c',
		TYPE_GROUP => 'CollaborationGroup'
	};

	private static final Integer lim = 3;

	@AuraEnabled
	public static ResultsWrapper getContent(String request){
		//List<FeaturedContent> featuredContent, String topicId

		Map<String, Object> requestData = (Map<String, Object>) JSON.deserializeUntyped(request);
		List<Object> featuredContent = (List<Object>) requestData.get('featuredContent');
		String topicId = (String) requestData.get('topicId');


		List<FeaturedContent> records = new List<FeaturedContent>();



		//If there's no topic Id, simply query the records specified in the featured content array
		if(topicId == null || topicId.trim().length() == 0){

			//I know its bad practice to put soql in for loops, but it just makes sense here as the featured content list should never be more than 3 or 4 records. We break after 3 records are found.
			Integer i = 0;
			for(Object objFc : featuredContent){
				Map<String, Object> fc = (Map<String, Object>) objFc;

				if(i >= 4 || records.size() >= 3)
					break;
				if((String) fc.get('type') == TYPE_BLOG || (String) fc.get('type') == TYPE_META || (String) fc.get('type') == TYPE_NEWS) {
					records.add(new FeaturedContent(getNewsArticle(new List<String>{(String) fc.get('recordId')}).get(0), (String) fc.get('type')));
				}else if((String) fc.get('type') == TYPE_EVENT){
					records.add(new FeaturedContent(getEvent(new List<String>{(String) fc.get('recordId')}).get(0), (String) fc.get('type')));
				}else if((String) fc.get('type') == TYPE_GROUP){
					records.add(new FeaturedContent(getGroup(new List<String>{(String) fc.get('recordId')}).get(0), (String) fc.get('type')));
				}
			}
			i++;
		}else{
			//Otherwise, get the most recent record from each type in the featured content up to 3 that has the topic assigned
			Set<String> sObjectTypes = new Set<String>();
			Set<String> contentTypes = new Set<String>();
			for(Object objFc : featuredContent){
				Map<String, Object> fc = (Map<String, Object>) objFc;
				sObjectTypes.add(objectMap.get((String) fc.get('type')));
				contentTypes.add((String) fc.get('type'));
			}

			//Map of sobject to a list of ids
			Map<String, List<String>> recordMap = new Map<String, List<String>>();
			for(TopicAssignment ta : [SELECT EntityType, EntityId FROM TopicAssignment WHERE EntityType IN :sObjectTypes AND TopicId = :topicId]){
				if(recordMap.get(ta.EntityType + '__c') == null)
					recordMap.put(ta.EntityType + '__c', new List<String>{ta.EntityId});
				else
					recordMap.get(ta.EntityType + '__c').add(ta.EntityId);
			}

			//Map of EntityType to a list of records
			Map<String, List<sObject>> entityMap = new Map<String, List<sObject>>();
			for(String key : recordMap.keySet()){
				if(key == objectMap.get(TYPE_NEWS)){
					entityMap.put(objectMap.get(TYPE_NEWS), getNewsArticle(recordMap.get(key)));
				}else if(key == objectMap.get(TYPE_EVENT))
					entityMap.put(objectMap.get(TYPE_EVENT), getEvent(recordMap.get(keY)));
				else if(key == objectMap.get(TYPE_GROUP))
					entityMap.put(objectMap.get(TYPE_GROUP), getEvent(recordMap.get(keY)));
			}

			for(String contentType : contentTypes){
				if(contentType == TYPE_BLOG){
					for(sObject s : entityMap.get(objectMap.get(TYPE_BLOG)))
						if(((News__c) s).RecordType.DeveloperName == RECORD_TYPE_BLOG){
							records.add(new FeaturedContent(s, TYPE_BLOG));
							break;
						}
				}
				else if(contentType == TYPE_NEWS){
					for(sObject s : entityMap.get(objectMap.get(TYPE_NEWS)))
						if(((News__c) s).RecordType.DeveloperName == RECORD_TYPE_NEWS){
							records.add(new FeaturedContent(s, TYPE_NEWS));
							break;
						}
				}else if(contentType == TYPE_META){
					for(sObject s : entityMap.get(objectMap.get(TYPE_META)))
						if(((News__c) s).RecordType.DeveloperName == RECORD_TYPE_META){
							records.add(new FeaturedContent(s, TYPE_META));
							break;
						}
				}else if(contentType == TYPE_EVENT && entityMap.get(objectMap.get(TYPE_EVENT)) != null){
					records.add(new FeaturedContent(entityMap.get(objectMap.get(TYPE_EVENT)).get(0), TYPE_EVENT));
				}else if(contentType == TYPE_GROUP && entityMap.get(objectMap.get(TYPE_GROUP)) != null)
					records.add(new FeaturedContent(entityMap.get(objectMap.get(TYPE_GROUP)).get(0), TYPE_GROUP));
			}
		}

		// create results wrapper
		List<FeaturedContent> resultsContent = spliceBy(records, lim);
		String strTimeZone = String.valueOf(UserInfo.getTimeZone());
		ResultsWrapper resultsWrapper = new ResultsWrapper(resultsContent, strTimeZone);

		return resultsWrapper;
	}

	private static List<sObject> getNewsArticle(List<String> recordIdList){
		try{
			return [SELECT Id, Name, Summary__c,Publish_DateTime__c, Author__r.Name, Author__r.FullPhotoUrl, Author__r.SmallPhotoUrl, RecordType.DeveloperName, (SELECT Id FROM Attachments LIMIT 1), (SELECT Topic.Name FROM TopicAssignments) FROM News__c WHERE Id IN :recordIdList ORDER BY CreatedDate DESC];
		}catch(QueryException e){return new List<sObject>();}
	}

	private static List<sObject> getEvent(List<String> recordIdList){
		try{
				return [SELECT Id, Name, Start_DateTime__c, End_DateTime__c, Location_Name__c, Location_Address__c, Number_of_Attendees__c, All_Day_Event__c, (SELECT Id FROM Attachments LIMIT 1), (SELECT Topic.Name FROM TopicAssignments) FROM Event__c WHERE Id IN :recordIdList ORDER BY CreatedDate DESC];
		}catch(QueryException e){return new List<sObject>();}
	}

	private static List<sObject> getGroup(List<String> recordIdList){
		try{
				return [SELECT Id, Name, Description, BannerPhotoUrl, FullPhotoUrl, SmallPhotoUrl, MemberCount, LastFeedModifiedDate FROM CollaborationGroup WHERE Id IN :recordIdList ORDER BY CreatedDate DESC];
		}catch(QueryException e){return new List<sObject>();}
	}

	private static List<FeaturedContent> spliceBy(List<FeaturedContent> objs, Integer size){
		List<FeaturedContent> returnList = new List<FeaturedContent>();
		for(Integer i = 0; i<objs.size(); i++){
			if(i == size)
				break;

			returnList.add(objs.get(i));
		}

		return returnList;
	}

	public class FeaturedContent{
		@AuraEnabled public sObject record{get;set;}
		@AuraEnabled public String type{get;set;}

		public FeaturedContent(sObject record, String type){
			this.record = record;
			this.type = type;
		}
	}

	public class ResultsWrapper {
		@AuraEnabled public List<FeaturedContent> featuredContent{get;set;}
		@AuraEnabled public String strTimeZone{get;set;}

		public ResultsWrapper(List<FeaturedContent> featuredContent, String strTimeZone){
			this.featuredContent = featuredContent;
			this.strTimeZone = strTimeZone;
		}
	}



}