public class EditOpportunity {
    public Opportunity opp{get; set;}
    public string OppID {get;set;}
    public Opportunity thisOpportunity;
    ApexPages.standardController m_sc = null;

    public EditOpportunity(ApexPages.StandardController controller) {
        this.opp=(Opportunity)controller.getRecord();
        thisOpportunity=(Opportunity)controller.getRecord();
        OppID =opp.id;
        system.debug('@@@@ - '+OppID);
         m_sc=controller;
    }
    
    public PageReference edit(){
    
     PageReference pageRef = new PageReference('/apex/Mercer_Opportunity_EditPage');
     pageRef.getParameters().put('id', String.valueOf(opp.id));

     return pageRef; 
    
    }
    public PageReference save(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        try{
            
            controller.save();
            
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
     pageRef.getParameters().put('id', String.valueOf(opp.id));

     return pageRef; 

    
    }
     public PageReference saveAndReturn(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        try{
            
            controller.save();
            
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/'+opp.id);
     //pageRef.getParameters().put('id', String.valueOf(opp.id));

     return pageRef; 

    
    }
    public PageReference Cancel()
    {
        PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
         pageRef.getParameters().put('id', String.valueOf(opp.id));

         return pageRef; 
    }
    public PageReference doCancel()
        {
             return m_sc.cancel(); 
        }
    
}