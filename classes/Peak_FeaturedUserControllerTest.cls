// ===================
// Featured User Controller Test
// ===================
@isTest
public with sharing class Peak_FeaturedUserControllerTest {
	
	// Test getting the user
	@isTest
	public static void testFeaturedUser(){
		// Set up and run as a standard user
		Peak_TestUtils testUtils = new Peak_TestUtils();
		User testUser = testUtils.createStandardUser();
		insert testUser;

		User queryUser = Peak_FeaturedUserController.getUserInformation(testUser.Id);
		system.assertEquals(Peak_TestConstants.FIRSTNAME + ' ' + Peak_TestConstants.LASTNAME,queryUser.Name);
	}

	// Test getting the site prefix
	@isTest
	public static void testSitePrefix(){
		String prefix = Peak_FeaturedUserController.getSitePrefix();
	}
}