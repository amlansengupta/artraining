/***************************************************************************************************************
Request Id : 17173
Purpose : This class is created for fetching Opportunity details and checking record type for Custom links
Created by : Archisman Ghosh
Created Date : 12/04/2019
Project : MF2
****************************************************************************************************************/
public class MF2_OppScopeItDetailReferralController {
	private static final String ENABLE_EDITING='Enable Editing';
    private static final String LOCK_OPPORTUNITY='Lock Opportunity';
    private static final String ENABLE_EDITING_EXPANSION='Enable Editing Expansion';
    private static final String LOCK_OPPORTUNITY_EXPANSION='Lock Opportunity Expansion';
    
    /*@Auraenabled
    public static Opportunity fetchOpportunity(String oppId){
        Opportunity opp = [Select ID, Total_Scopable_Products_Revenue__c, Planned_Fee_Adj__c, Cost2__c, Profit_Loss__c, Calc_Margin__c,
                           Sibling_Company__c, Business_LOB__c, Sibling_Contact_Name__c,
                           Potential_Revenue__c, Sibling_Contact_Office__c, Cross_Sell_ID__c, Referrer_Name__c, Referral_to_LOB__c, Referrer_LOB__c,
                          Referrer_Office__c, CampaignId, LeadSource, Related_Opportunity__c, Growth_Plan_ID__c, Bundled_Integrated_Offering__c, 
                          Date_as_Of__c ,Total_Project_Client_Rev_USD__c ,CY_Project_Client_Rev_USD__c ,GOC_SUBMIT_DATE__c ,GOC_MESSAGE__c ,GOC_SUBMIT_STATUS__c
                           From Opportunity where Id =: oppId];
        return opp;
	
    }*/
    /*
*  @Method Name: checkRecordType
*  @Description: Method checks for Record Type to display custom links
*/
    /*@Auraenabled
    public static List<OpportunityCustomLink__mdt> checkRecordType(String oppId){
        Opportunity opp=null;
        List<OpportunityCustomLink__mdt> newCustomLinkList = new List<OpportunityCustomLink__mdt>();
        try{
            //To fetch the fields of the Custom Meta Data Type
            List<OpportunityCustomLink__mdt> listOfCustomLink = [Select Label,Link__c,UI_Label__c from OpportunityCustomLink__mdt];
            //To fetch the Record Type Name of Opportunity
            List<Opportunity> listOfOpportunity = [Select ID,RecordType.Name from Opportunity where Id =: oppId];
            
            if(listOfOpportunity!=null && listOfOpportunity.size()>0)
            {
                opp=listOfOpportunity.get(0);
            }
            
            for(OpportunityCustomLink__mdt obj : listOfCustomLink)
            {
                if(obj.Label.equalsIgnoreCase(ENABLE_EDITING))
                {
                    String recordTypeEnable = System.Label.RecordTypeForEnableEditing;
                    List<String> listRecordTypeEnable = recordTypeEnable.split(',');
                    if(listRecordTypeEnable.contains(opp.RecordType.Name))
                    {
                        newCustomLinkList.add(obj);
                    }
                }
                else if(obj.Label.equalsIgnoreCase(LOCK_OPPORTUNITY))
                {
                    String recordTypeLock = System.Label.RecordTypeForLockOpportunity;
                    List<String> listRecordTypeLock = recordTypeLock.split(',');
                    if(listRecordTypeLock.contains(opp.RecordType.Name))
                    {
                        newCustomLinkList.add(obj);
                    } 
                } 
                else if(obj.Label.equalsIgnoreCase(LOCK_OPPORTUNITY_EXPANSION))
                {
                    String recordTypeLockExpansion = System.Label.RecordTypeForLockOpportunityExpansion;
                    List<String> listRecordTypeLockExpansion = recordTypeLockExpansion.split(',');
                    if(listRecordTypeLockExpansion.contains(opp.RecordType.Name))
                    {
                        newCustomLinkList.add(obj);
                    } 
                }
                else if(obj.Label.equalsIgnoreCase(ENABLE_EDITING_EXPANSION))
                {
                    String recordTypeEnableExpansion = System.Label.RecordTypeForEnableEditingExpansion;
                    List<String> listRecordTypeEnableExpansion = recordTypeEnableExpansion.split(',');
                    if(listRecordTypeEnableExpansion.contains(opp.RecordType.Name))
                    {
                        newCustomLinkList.add(obj);
                    } 
                }
            }
        }
        catch(Exception ex){
            system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_OppScopeItDetailReferralController', 'checkRecordType');
            throw new AuraHandledException('Could not fetch Opportunity Record Types!');
        }
        return newCustomLinkList;
        
    }*/
    /*
*  @Method Name: scopeRecordTypeCheck
*  @Description: Method checks for Record Type to display custom buttons
*/
    @Auraenabled
    public static boolean scopeRecordTypeCheck(String oppId){
        boolean flag=false;
        Opportunity opp=null;
        try{
            //To fetch the Record Type Name of Opportunity
            List<Opportunity> listOpp=[Select ID,RecordType.Name from Opportunity where Id =: oppId];
            if(listOpp!=null && listOpp.size()>0)
            {
                opp=listOpp.get(0);
            }
            String recordTypeRefreshPrint = System.Label.RecordTypeRefreshPrint;
            List<String> listRecordTypeRefreshPrint = recordTypeRefreshPrint.split(',');
            
            if(listRecordTypeRefreshPrint.contains(opp.RecordType.Name))
            {
                flag=true;
            }
        }
        catch(Exception ex){
            system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_OppScopeItDetailReferralController', 'scopeRecordTypeCheck');
            throw new AuraHandledException('Could not fetch Opportunity Record Types!');
        }
        return flag;
    }
    
}