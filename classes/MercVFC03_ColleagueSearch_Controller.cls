/*Purpose:  This Apex class contains  the logic for searching a colleague based on First Name and Last Name
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Joban       1/21/2013   Created a controller for Colleague Search Page.
   2.0 -    Sarbpreet   7/2/2014    Updated Comments/documentation.
  
============================================================================================================================================== 
*/
public class MercVFC03_ColleagueSearch_Controller {

   /*
    *  Creation of constructor 
    */
    public MercVFC03_ColleagueSearch_Controller(ApexPages.StandardController controller) {    }
    
    Map<String,Colleague__c> MapEmpNumber_Collegue ;
    Map<String,String> MapCollegueId_EmpId ;
    
    Public Boolean ShowResultsPanel {get;set;}
    Public Boolean ShowErrorPanel {get;set;}
    Public Boolean ShowUserExistsPanel {get;set;}
    Public user UserAlreadyExist {get;set;} 
    Public String SelecedId {get;set;}
    Public List<Colleague__c> results  = new List<Colleague__c>();
    public Integer resultCount = 0;
    public boolean renderCount = false;
    public String Fname {get;set;}
    public String Lname {get;set;}
    public String Enumber {get;set;}
    public String Wmail {get;set;}
    Public String Cname{get;set;}
    String idResult{get;set;}
    
    /*
    *  Getter method for fetching the results 
    */
    public List<Colleague__c> getResults() {
        return results;
    }
    
   /*
    *  Setter method for results 
    */
    public void setResults(List<Colleague__c>results) {
        this.results = results;
    }
    
    /*
    *  Getter method for ResultCount
    */
    public Integer getResultCount (){
        return resultCount;
    }
    
    /*
    *  Setter method for ResultCount
    */
    public void setResultCount (Integer count){
        resultCount = count;
    }
    
    /*
    *  Getter method for RenderCount 
    */
    public boolean getRenderCount(){
        return renderCount;
    }
    
    /*
 * @Description : This method create and prepares the query for searching the colleague record
 * @ Args       : null
 * @ Return     : PageReference
 */ 
    public PageReference fireQuery(){
       
        system.debug('....'+Enumber);
       
        renderCount = true;
        
        FName    =    Fname.trim() ;
        LName    =    LName.trim() ;
        Wmail    =    Wmail.trim() ;
        Enumber  =    Enumber.trim() ;
        
         
        if ( FName.length() == 0 || FName == Null ) {
            FName = '';
        } else {
            FName = '%'+ FName +'%';
        }
         
         
        if ( LName.length() == 0 || LName == Null ) {
            LName = '';
        } else {
            LName = '%'+ LName +'%';
        }
        
        
        System.debug(' Fname   ==> ' + Fname.length()   );
        
        
        CName =   Fname + LName  ;
        
        System.debug(' CName ==> ' + CName );
        
         results = new List<Colleague__c>();
         MapEmpNumber_Collegue = New Map<String,Colleague__c> ();
         MapCollegueId_EmpId = New Map<String,String> ();
            
         results = [SELECT   Name, Email_Address__c, Empl_Status__c, EMPLID__c  
                      FROM Colleague__c 
                      WHERE  ( Name Like :CName AND Name != '' ) OR
                             ( EMPLID__c = :Enumber AND EMPLID__c != Null )  OR 
                             ( Email_Address__c = :Wmail and Email_Address__c != null )
                      ORDER BY First_Name__c , Last_Name__c LIMIT 1000 ];
           
         System.debug(' results ==> ' + results.isEmpty() ); 
            
         if ( results.isEmpty() ) {
                
                ShowResultsPanel    = false ; 
                ShowErrorPanel      = true ;
                
         } else {
                
                ShowResultsPanel    = true ; 
                ShowErrorPanel      = false;
                
                for ( Colleague__c c :results  ) {
                    
                    if ( c.EMPLID__c <> Null )  
                    MapEmpNumber_Collegue.Put(c.EMPLID__c,c);
                    MapCollegueId_EmpId.Put(c.Id,c.EMPLID__c);
                }
                
           }
        resultCount = results.size();
        return null;
    }
    
    
    
   /*
    * @Description : This method creates selected User
    * @ Args       : null
    * @ Return     : PageReference
    */ 
    public PageReference createSelUser(){
        idResult = System.currentPageReference().getParameters().get('select');
        PageReference userPg = null;
        if(idResult == null || idResult == '') {
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a result!');
             ApexPages.addMessage(myMsg);
        
     }else{
         List<User> User = New List<User> ();
         User = [ Select Id, Name FROM User WHERE EmployeeNumber = : MapCollegueId_EmpId.get(idResult) Limit 1 ]  ;
         if ( User.isEmpty() ){
              userPg = MapCollegueDetailstoUser( QueryColleagueAllDetails ( idResult ) ) ;
      }else {
               UserAlreadyExist = user[0] ;
               ShowUserExistsPanel = true ; 
            }  
      } 
      return userPg;
    }
    
    /*
    * @Description : This method maps colleague details with corresponding User
    * @ Args       : Colleague__c result
    * @ Return     : PageReference
    */  
    Public PageReference MapCollegueDetailstoUser( Colleague__c result  ){
        PageReference userPg = null;
        
        if ( result  <> null ) {
             
            String SubRegion = result.Sub_Region__c;
            String SubMarket = result.Sub_Market__c;
            String Region = result.Region__c; 
            String CusFName;
            String CusLName;
           
            if (result.Prf_Last_Name__c == null)
            {
                LName = result.Last_Name__c;
                CusLName = result.Last_Name__c;
            }
            else{
                LName = result.Prf_Last_Name__c;
                CusLName = result.Prf_Last_Name__c;
            }
            if (result.Prf_First_Name__c == null)
            {
                FName = result.First_Name__c;
                CusFName = result.First_Name__c;
            } 
            else{
                FName= result.Prf_First_Name__c;
                CusFName = result.Prf_First_Name__c;
            }
       
            String EmpID = result.EMPLID__c;
            String Emailadd = result.Email_Address__c;
            String Dept = result.Dept_Descr__c;
            String Address1 = result.Address1__c;
            String Address2= result.Address2__c;
            String Address3 = result.Address3__c;
            String City = result.City__c;
            String country = result.Country__c;
            String currencycd = result.currency_CD__c;
            String Department = result.Dept_Descr__c;
            String emplstatus = result.Empl_Status__c;
            String GLDID = result.GLID__c;
            String grade = result.Grade__c;
            String Level10cd = result.Level_10_CD__c;
            String level10descr = result.Level_10_Descr__c;
            String level1cd = result.Level_1_CD__c;
            String level1descr = result.Level_1_Descr__c;
            String level2cd = result.Level_2_CD__c;
            String level2descr = result.Level_2_Descr__c;
            String level3cd = result.Level_3_CD__c;
            String level3descr = result.Level_3_Descr__c;
            String level4cd = result.Level_4_CD__c;
            String level4descr  = result.Level_4_Descr__c;
            String level5cd = result.Level_5_CD__c;
            String level5descr = result.Level_5_Descr__c;
            String level6cd = result.Level_6_CD__c;
            String lob = result.LOB__c;
            String level7cd = result.Level_7_CD__c;
            String level7descr = result.Level_7_Descr__c;
            String level8cd = result.Level_8_CD__c;
            String level8descr = result.Level_8_Descr__c;
            String level9cd = result.Level_9_CD__c;
            String level9descr = result.Level_9_Descr__c;
            String location = result.Location__r.Name;
            String locdesc = result.Location_Descr__c;
            String phone = result.Phone__c;
            String countrycode = result.Phone_Country_Code__c;
            String postal = result.Postal__c;
            String plname = result.Prf_Last_Name__c;
            String pfname = result.Prf_First_Name__c;
            String state = result.State__c;
            String supID = result.Supervisor_ID__c;
            String WWID = result.WWID__c;
            String Market = result.Market__c;
            String Office = result.Office__c;
            String alias = null;
            String cnname;
            if(CusFName == null)
                CusFName = '';
            if(CusLName == null)
                CusLName = '';
            
            if(LName  == null){
                LName  = '';
            }if(FName == null){
                FName = '';
            }
            if(EmpID == null||EmpID == 'null'){
                EmpID = '';
            }
 
            if(Dept == null){
                Dept = '';
            }if(Address1 == null){
                Address1 = '';
            }if(Address2 == null){
                Address2 = '';
            }if(Address3 == null){
                Address3 = '';
            }if(City == null){
                City = '';
            }if(Emailadd== null){
                Emailadd = '';
            }
            if(country == null){
                country = '';
            }if(currencycd == null){
                currencycd = '';
            }if(Department == null){
                Department = '';
            }if(emplstatus == null){
                emplstatus = '';
            }if(GLDID == null){
                GLDID = '';
            }if(grade == null){
                grade = '';
            }if(Level10cd == null){
                Level10cd = '';
            }if(level10descr == null){
                level10descr = '';
            }if(level1cd == null){
                level1cd = '';
            }if(level2cd == null){
                level2cd = '';
            }if(level2descr == null){
                level2descr = '';
            }if(level3cd == null){
                level3cd  = '';
            }if(level3descr == null){
                level3descr = '';
            }if(level4cd== null){
                level4cd = '';
            }if(level4descr == null){
                level4descr = '';
            }if(level5cd  == null){
                level5cd   = '';
            }if(level5descr == null){
                level5descr = '';
            }if(level6cd == null){
                level6cd = '';
            }if(lob == null){
                lob = '';
            }if(level7cd  == null){
                level7cd  = '';
            }if(level7descr == null){
                level7descr = '';
            }if(level8cd  == null){
                level8cd = '';
            }if(level8descr == null){
                level8descr = '';
            }if(level9cd == null){
                level9cd  = '';
            }if(level9descr == null){
                level9descr = '';
            }if( location  == null){
                location = '';
            }if(phone == null){
                phone = '';
            }if(countrycode  == null){
                countrycode = '';
            }if(postal  == null){
                postal = '';
            }if(plname == null){
                plname = '';
            }if( pfname  == null){
                pfname = '';
            }if(supID  == null){
                supID = '';
            }if(state == null){
                state  = '';
            }if( WWID  == null){
                WWID = '';
            }
            if( Market  == null){
                Market = '';
            }

            if((FName.length() > 0) && (LName.length() > 3))
            {
                alias = FName.substring(0,1) + LName.substring(0,3);
                alias = alias.toLowerCase();
            }
            if(Emailadd.length()>0 && Emailadd.indexOf('@',0)>=0)
            {
               cnname = Emailadd.substring(0,Emailadd.indexOf('@',0));
            }
            else
            {
                cnname = '';
            }
        
        
            Map <String, Schema.SObjectField> fieldmap = schema.Sobjecttype.User.fields.getMap();
            Map <String, ID> fieldIDmap = new Map<String, ID>();
            for(Schema.SObjectField sfield : fieldmap.Values())
            {
                schema.describefieldresult fieldinfo = sfield.getDescribe(); 
                system.debug('-----------------------'+ fieldinfo.getName()+ ' ['+ fieldinfo.getLabel()+']' + fieldinfo.gettype().name() + ', '+fieldinfo.getscale()+ ')');
            }

            String Street = Address1 + ' ' + Address2 +  '' + Address3;
            String FedID = Emailadd.toUpperCase(); // Req:3458 - December Release
            userPg = new PageReference('/005/e?retURL=/apex/Mercer_searchcolleague&cancelURL=/apex/Mercer_searchcolleague&name_firstName='+EncodingUtil.urlEncode(FName,'UTF-8')+'&name_lastName='+EncodingUtil.urlEncode(LName ,'UTF-8'));  
         
            userPg.getParameters().put('Email',Emailadd); 
            userPg.getParameters().put('Alias',alias); 
            userPg.getParameters().put('Username',Emailadd); 
            userPg.getParameters().put('CommunityNickname',cnname); 
            userPg.getParameters().put('EmployeeNumber',EmpID);
            userPg.getParameters().put('Phone',phone);
            userPg.getParameters().put('FederationIdentifier',FedID);  
            userPg.getParameters().put('Addresscity',City);
            userPg.getparameters().put('Addressstate',state);
            userPg.getparameters().put('Addressstreet',Street);
            userPg.getParameters().put('Addresscountry',country);
            userPg.getParameters().put('Addresszip',postal);
            userPg.getParameters().put(System.Label.CL07_UserMarketID,Market);
            userPg.getParameters().put(System.Label.CL08_UserOfficeID,Office);
            userPg.getParameters().put(System.Label.CL09_GLIDID,GLDID);
            userPg.getParameters().put(System.Label.CL10_LocationID,location);
            userPg.getParameters().put(System.Label.CL11_pfnameID,pfname);
            userPg.getParameters().put(System.Label.CL13_plnameID,plname);
            userPg.getParameters().put(System.Label.CL12_LocDescrID,locdesc);
            userPg.getParameters().put(System.Label.CL14_currencyID,currencycd);
            userPg.getParameters().put(System.Label.CL40_SupervisorID,SupID);
            userPg.getParameters().put(System.Label.CL15_GradeID,grade);
            userPg.getParameters().put(System.Label.CL16_WWID,WWID);
            userPg.getParameters().put(System.Label.CL17_LOBID,lob);
            userPg.getParameters().put(System.Label.CL18_Level1desID,level1descr);
            userPg.getParameters().put(System.Label.CL19_Level2desID,level2descr);
            userPg.getParameters().put(System.Label.CL20_Level3desID,level3descr);
            userPg.getParameters().put(System.Label.CL21_Level4desID,level4descr);
            userPg.getParameters().put(System.Label.CL22_Level5desID,level5descr);
            userPg.getParameters().put(System.Label.CL23_Level7desID,level7descr);
            userPg.getParameters().put(System.Label.CL24_Level8desID,level8descr);
            userPg.getParameters().put(System.Label.CL25_Level9desID,level9descr);
            userPg.getParameters().put(System.Label.CL26_Level10desID,level10descr);
            userPg.getParameters().put(System.Label.CL27_Level1CDID,level1cd);
            userPg.getParameters().put(System.Label.CL28_Level2CDID,level2cd);
            userPg.getParameters().put(System.Label.CL29_Level3CDID,level3cd);
            userPg.getParameters().put(System.Label.CL30_Level4CDID,level4cd);
            userPg.getParameters().put(System.Label.CL31_Level5CDID,level5cd);
            userPg.getParameters().put(System.Label.CL32_Level6CDID,level6cd);
            userPg.getParameters().put(System.Label.CL33_Level7CDID,level7cd);
            userPg.getParameters().put(System.Label.CL34_Level8CDID,level8cd);
            userPg.getParameters().put(System.Label.CL35_Level9CDID,level9cd);
            userPg.getParameters().put(System.Label.CL36_Level10CDID,level10cd);
            userPg.getParameters().put(System.Label.CL38_SubRegionID,SubRegion);
            userPg.getParameters().put(System.Label.CL37_SubMarketID,SubMarket);
            userPg.getParameters().put(System.Label.CL39_RegionID,Region);
            userPg.getParameters().put(System.Label.CL95_UserCountry, country);
            userPg.getParameters().put(System.Label.CL_First_Name,CusFName);
            userPg.getParameters().put(System.Label.CL_Last_Name,CusLName);
        }
         return userPg ;
    }
    
    /*
    * @Description : This method fetches all the details of Colleague record
    * @ Args       : String Id
    * @ Return     : Colleague__c
    */ 
    Public Colleague__c QueryColleagueAllDetails ( String Id ){
        Colleague__c Colleague = null ;
        Colleague = [SELECT  id, Name, Address1__c, Address2__c, Address3__c, City__c, Country__c, currency_CD__c, Dept_Descr__c,
                                Email_Address__c, Empl_Status__c, EMPLID__c, First_Name__c, GLID__c, Grade__c, Last_Name__c, 
                                Level_10_CD__c, Level_1_CD__c, Level_1_Descr__c, Level_2_CD__c, Level_2_Descr__c, Level_10_Descr__c, 
                                Level_3_CD__c, Level_3_Descr__c, Level_4_CD__c, Level_4_Descr__c, Level_5_CD__c, Level_5_Descr__c,
                                Level_6_CD__c, LOB__c,Level_7_CD__c, Office__c, Sub_Market__c, Sub_Region__c, Region__c, Market__c, 
                                Level_7_Descr__c, Level_8_CD__c, Level_8_Descr__c, Level_9_CD__c, Level_9_Descr__c, Location_Descr__c, 
                                Phone__c, Location__r.Name, Phone_Country_Code__c, Postal__c, Prf_Last_Name__c, Prf_First_Name__c, 
                                State__c, Supervisor_ID__c, WWID__c 
                         FROM Colleague__c where id = : Id ] ;
        return Colleague ;    
    }
    
   /*
    * @Description : This method creates a new User
    * @ Args       : null
    * @ Return     : PageReference
    */
    Public PageReference createNewUser() {
      return new PageReference('/005/e?retURL=/servlet/servlet.Integration?lid=01rT00000008qYN&ic=1&cancelURL=/servlet/servlet.Integration?lid=01rT00000008qYN&ic=1');
    }
}