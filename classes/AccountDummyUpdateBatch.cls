/* Batch to dummy update Account */

global class AccountDummyUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
        
        global static String query = Label.AccountDummyUpdateBatchLabel;        

        global Database.QueryLocator start(Database.BatchableContext BC){
            if(Test.isRunningTest())
            {
              query = 'Select Id From Account Limit 100'; 
            }            
            System.debug('\n query prepared : '+query);             
            return Database.getQueryLocator(query); 
        }
        
        global void execute(Database.BatchableContext bc, List<sObject> scope){
            
            List<Account> accountToUpdate = new List<Account>();
            
            for(Account a :(List<Account>)scope) {
                accountToUpdate.add(a); 
            }
            
            if(!accountToUpdate.isEmpty()){
                database.update(accountToUpdate,false);
            }    
        }
        
        global void finish(Database.BatchableContext bc){
        
        }
}