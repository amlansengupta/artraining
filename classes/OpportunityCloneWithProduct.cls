/*
======================================================================
Request											Date			Modified By
12638-Replacing opprtunity step with stage		18-Jan-2019		Trisha
=====================================================================
*/
Global with sharing class OpportunityCloneWithProduct
{   
    private ApexPages.StandardController controller {get; set;}
    String oppId{set;get;}
    String oldoppId{set;get;}
    public String isCloned{set;get;}
    public Opportunity opps {get;set;}
    public Opportunity newOppty{get;set;}
    List<OpportunityLineItem> oliList= new List<OpportunityLineItem>();
    public boolean clonedBool{get;set;}
    public Opportunity clonedOppty{get;set;}
    ApexPages.standardController m_sc = null;
    public String stepValue{ get; set; }
    //request id:12638 added stageValue for picklist of stage selection
    public String stageValue{ get; set; }
    public String closeStageReasonValue{ get; set; }
    public String ChargeCodeExepValue{ get; set; }
    public boolean checkExpansion{get;set;}
    public static List<productStatusCount> productCountList{get;set;}
    public static boolean stopMultipleUpd = false;
    public static boolean preventFirstCall= True;
    public Boolean isCloneWithProduct = false;
   /****request id:12638 adding stage Value picklist for display in page START******/ 
   /* public List<SelectOption> getstepVal() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption('None','--None--'));
        stOptions.add(new SelectOption('Marketing/Sales Lead','Marketing/Sales Lead'));
            stOptions.add(new SelectOption('Executing Discovery','Executing Discovery'));
            stOptions.add(new SelectOption('Identified Deal','Identified Deal'));
            stOptions.add(new SelectOption('Identified Single Sales Objective(s)','Identified Single Sales Objective(s)'));
            stOptions.add(new SelectOption('Assessed Potential Solutions & Strategy','Assessed Potential Solutions & Strategy'));
            stOptions.add(new SelectOption('Making Go/No Go Decision','Making Go/No Go Decision'));
        if(checkExpansion==true){
            stOptions.add(new SelectOption('Closed / Won (SOW Signed)','Closed / Won (SOW Signed)'));            
        }
        return stOptions;
        
    }*/
    
     public List<SelectOption> getStageVal() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption(System.Label.CL57_OpportunityType,System.Label.None_With_Dash));
        stOptions.add(new SelectOption(System.Label.Stage_Above_the_Funnel,System.Label.Stage_Above_the_Funnel));           
            stOptions.add(new SelectOption(System.Label.Stage_Identify,System.Label.Stage_Identify));            
             //Request No. 17953 : Removing Qualify
            //stOptions.add(new SelectOption(System.Label.Stage_Qualify,System.Label.Stage_Qualify));              
        if(checkExpansion==true){
            stOptions.add(new SelectOption(System.Label.CL48_OppStageClosedWon,System.Label.CL48_OppStageClosedWon));            
        }
        return stOptions;
        
    }
    /****request id:12638 adding stage Value picklist for display in page END******/
     public List<SelectOption> getcloseStageReasonVal() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
        Schema.DescribeFieldResult fieldResult =Opportunity.Close_Stage_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
        
    }
    public List<SelectOption> getChargeCodeExepVal() {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('None','--None--'));
        
        Schema.DescribeFieldResult fieldResult =Opportunity.Project_Exception__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
        
    }
    
    public OpportunityCloneWithProduct(ApexPages.StandardController controller){
        
        String iscloned = apexpages.currentpage().getparameters().get('isCloned');
        String isexpansion = apexpages.currentpage().getparameters().get('isexpansion');
        
        isCloneWithProduct = false;
        if( String.isNotBlank(iscloned) ){
            isCloneWithProduct = Boolean.valueOf(iscloned);
        }
        
        clonedBool=false;
       //initialize the stanrdard controller
        this.controller = controller;
        
        m_sc=controller;
        // load the current record
        opps = (Opportunity)controller.getRecord();
        
        Id rId = opps.Id;
        oldoppId=opps.Id;
        DescribeSObjectResult describeResult = rId.getSObjectType().getDescribe();      
        Map<String, Schema.SObjectField> fieldMap = describeResult.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fieldMap.values();
        
        
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {
        //String theLabel = s.getDescribe().getLabel(); // Perhaps store this in another map
        String theName = s.getDescribe().getName();
        //String theType = s.getDescribe().getType(); // Perhaps store this in another map
        
        // Continue building your dynamic query string
        theQuery += theName + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        System.debug('theQuery***'+theQuery);
        
        // Finalize query string
        theQuery += ' FROM Opportunity where id=:rid';
        System.debug('theQuerywww***'+theQuery);
        // Make your dynamic call
        theQuery = String.escapeSingleQuotes(theQuery);
        opps= Database.query(theQuery);
        String CurrUserOffice = [select employee_office__c from user where id=:userinfo.getuserid()].employee_office__c;
        system.debug('Employee office....'+CurrUserOffice);
        
        List<String> officeList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_Office__c.getDescribe();
        for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
           officeList.add(pickListVal.getLabel());
        }     
                    
        if(CurrUserOffice<>null && !CurrUserOffice.containsIgnoreCase('WFH') && officeList.contains(CurrUserOffice))
        {
            opps.opportunity_office__c = CurrUserOffice;
        }        
        else{
            opps.opportunity_office__c = null;
        }        
        
        //opps.name='Clone - '+opps.name;

        opps.IsCloned__c=true;
        opps.ownerid = userinfo.getuserid(); 
        //opps.opportunity_office__c =CurrUserOffice;
        opps.Description=null;
        opps.Next_Action__c=null;        
        
        opps.Closed_Stage_Reason_Detail__c =null;   
        opps.Project_Exception__c=null;
        opps.Date_as_Of__c=null;
        opps.Total_Project_Client_Rev_USD__c=null;
        opps.CY_Project_Client_Rev_USD__c=null;
        
        
        opps.IsPrivate=false;
        opps.Bundled_Integrated_Offering__c=null;
        
        opps.mh_Associated_Blue_Sheet__c=False;
        opps.Blue_Sheet_Attachment__c=False;
        opps.Blue_Sheet_Attachment_Last_Updated__c=null;
        opps.mh_Last_Updated_Blue_Sheet__c=null;
        opps.Funnel_Scorecard__c=null;
        opps.Cross_Sell_ID__c=null;
        opps.Blue_Sheet_Last_Updated__c=null;
        opps.mh_Specify_Competitors__c=null;
        opps.mh_Managers_Notes__c=null;
        opps.Sibling_Company__c=null;
        opps.Sibling_Contact_Name__c=null;
        opps.Sibling_Contact_Office_Name__c=null;
        opps.Referrer_Name__c=null;
        opps.Referral_to_LOB__c=null;
        opps.Potential_Revenue__c=null;
        opps.Business_LOB__c=null;
        opps.mh_Adequacy_of_Current_Position__c=null;
        opps.Delivery_Geography__c=null;
        //opps.Amount=1.00;
        opps.Current_Year_Revenue_USD__c=1.00;
            opps.Annualized_Revenue_USD__c=1.00;
            opps.Total_Scopable_Products_Revenue_USD__c=1.00;
            opps.Total_Opportunity_Revenue_USD__c=1.00;
        
        opps.Total_Project_Client_Rev_USD__c=null;
        
        opps.CY_Project_Client_Rev_USD__c=null;
        opps.PY_Project_Client_Rev_USD__c=null;
        opps.Date_of_Last_Chatter_Post_by_User__c=null;
        opps.LastStageUpdateDate__c=null;
        opps.Stage_Change_Date__c=null;
        opps.Last_Step_Modified_Date__c=null;
        opps.Above_Funnel_Stage_End_Date__c = null;
        opps.Above_Funnel_Stage_Begin_Date__c=null;
        opps.identify_stage_end_date__c = null;
        opps.Identify_Stage_Begin_Date__c=null;
        opps.selected_stage_end_date__c = null;
        opps.Selected_Stage_Begin_Date__c=null;
        opps.finalist_stage_end_date__c = null;
        opps.Finalist_Stage_Begin_Date__c = null;
        opps.Active_Pursuit_stage_end_date__c = null;
        opps.Active_Pursuit_Stage_Begin_Date__c = null;
        opps.Pending_Project_Stage_End_Date__c =null;
        opps.Pending_Project_Stage_Begin_Date__c =null;
        system.debug('opps.Amount....'+opps.Amount);
        RecordType rt=new RecordType();
        if(iscloned=='True' && iscloned!=null){
            opps.Opportunity_Name_Local__c=null;
            opps.Related_Opportunity__c=null;
            opps.Growth_Plan_ID__c=null;
            opps.LeadSource=null;
            opps.Campaignid=null;
            opps.closeDate=null;
            opps.Buyer__c=null;
            opps.Close_Stage_Reason__c=null;
            opps.accountId=null;
            opps.Type=null;
            checkExpansion=false;
            opps.probability=10;
            stepValue='Identified Deal';
            stageValue = 'Identify';
            opps.StageName = 'Identify';
            //request id:12638 , commenting step
           // opps.Step__c = 'Identified Deal';
            rt = [select Id,DeveloperName from RecordType where DeveloperName = 'Opportunity_Detail_Page_Assinment' and SobjectType = 'Opportunity' limit 1];
        }else if(isexpansion == 'True' && isexpansion !=null){
            if(opps.Opportunity_Name_Local__c!=null){
                    opps.Opportunity_Name_Local__c=opps.Opportunity_Name_Local__c+' – Product Expansion';
                }else{
                    opps.Opportunity_Name_Local__c=null;
                }
            
            
            opps.Child_Opp_TOR__c=0.00;
            opps.Child_Opp_TOR_USD__c=0.00;
            opps.closeDate=Date.Today();
            opps.name=opps.name +' – Product Expansion';
            closeStageReasonValue=opps.Close_Stage_Reason__c;
            opps.Type='Product Expansion';
            checkExpansion=True;
            opps.probability=100;
            stepValue='Closed / Won (SOW Signed)';
            stageValue = 'Closed / Won';
            opps.StageName = 'Closed / Won';
            //request id:12638,commenting step
            //opps.Step__c = 'Closed / Won (SOW Signed)';
            opps.Parent_Opportunity_Name__c=opps.id;
            rt = [select Id,DeveloperName from RecordType where DeveloperName = 'Opportunity_Product_Expansion' and SobjectType = 'Opportunity' limit 1];        
        }
        opps.RecordTypeId=rt.id;
        newOppty = opps.clone(false);
        system.debug('newOppty.Amount....'+newOppty.Amount);
        
        System.debug('opps**********'+opps);
        System.debug('newOppty**********'+newOppty);
    }
    public PageReference saveClone(){
        PageReference pageRef;
        Boolean oliInsert;
        /*****request id:12638, replacing below assignment from step to stage START ****/
        //newOppty.Step__c=stepValue;
        newOppty.stageName = stageValue;
        /*****request id:12638, replacing below assignment from step to stage END ****/
        newOppty.Close_Stage_Reason__c=closeStageReasonValue;
        newOppty.Project_Exception__c=ChargeCodeExepValue;
        
        Mercer_Office_Geo_D__c newOppGeoD = new Mercer_Office_Geo_D__c();
        newOppGeoD = getOppGeo(newOppty);
        if(newOppGeoD != null){
            newOppty.Opportunity_Country__c = newOppGeoD.Country__c;
            newOppty.Opportunity_Region__c =  newOppGeoD.Region__c;
            newOppty.Opportunity_Sub_Region__c = newOppGeoD.Sub_Region__c;
            newOppty.Opportunity_Market__c = newOppGeoD.Market__c;            
            newOppty.Opportunity_Sub_Market__c = newOppGeoD.Sub_Market__c;            
        }
        
        try{
            if(clonedBool!=true){
                System.debug('insert newOppty**********'+newOppty);                 
                insert newOppty ;
                oliInsert=insertOppLineItem(opps.id,newOppty.Iscloned__c);
                if(checkExpansion==True){
                    expansionOpportunity(opps.Id);
                    cloneOppteamMember(newOppty);
                }
                if(isCloneWithProduct){
                    cloneOppteamMember(newOppty);
                }
                clonedBool=true;
            }
            System.debug('insert newOppty2**********'+newOppty);
            //cloneOppteamMember(newOppty.Id);
            //oliInsert=insertOppLineItem(opps.id,newOppty.Iscloned__c);
            if(oliInsert==true){
                pageRef = new PageReference('/'+newOppty.id);            
                return pageRef ;
            }
       }    
        catch(Exception e){
            String errormsg='In order to create an expansion opportunity you need to be either the Opportunity Owner or be on the Account Team with Opportunity read/write access';
            if(e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,errormsg);
                ApexPages.addMessage(myMsg);

            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg);
                System.debug('exception*******'+e);
            } 
                        return null;
        }
                return null; 
    }
    public PageReference doCancel()
    {  if( newOppty.id!=null){
            delete newOppty;
        }
           
         return m_sc.cancel(); 
    }
    public boolean insertOppLineItem(String oppId ,boolean Iscloned){
       
       try{
            List<OpportunityLineItem> oliList=new List<OpportunityLineItem>();
            List<OpportunityLineItem> oliListtoUpdate=new List<OpportunityLineItem>();
            Set<Opportunity> oppUpdList=new Set<Opportunity>();
            oppUpdList.clear();
            if(Iscloned==true){
                 oliList = [Select id, name,UnitPrice,PricebookEntry.Product2Id,Inv_Client_Type__c,DS_Sales_Leader__c,Inv_Project_is_Seeded__c,Inv_Segment__c, opportunityId,PricebookEntryId,Duration__c,Revenue_Start_Date__c,LOB__c,Revenue_End_Date__c,CurrentYearRevenue_edit__c,AnnualizedRevenue__c,
                        Year2Revenue_edit__c,Year3Revenue_edit__c,Cluster__c from OpportunityLineItem where opportunityId = : oppId and Product2.isactive=True];
               integer i=0;
                for(OpportunityLineItem oli:oliList){
                         
                         OpportunityLineItem oliCopy = oli.clone(false,true);                  
                         oliCopy.opportunityId = newOppty.Id;
                         oliCopy.project_manager__c=null;  
                         oliCopy.Total_Product_Client_Revenue_USD__c=null;
                         oliCopy.PY_Product_Client_Revenue_USD__c=null;
                         oliCopy.CY_Product_Client_Revenue_USD__c=null;                     
                         oliCopy.Revenue_End_Date__c=newOppty.closeDate;
                         oliCopy.Revenue_Start_Date__c= newOppty.closeDate;
                         oliCopy.UnitPrice=1.00;
                        
                         oliCopy.Annualized_Revenue_editable__c=0.00;
                         if(checkExpansion==True){
                             oliCopy.Is_Project_Required__c=False;
                         }
                         if(oli.Cluster__c == 'DB delegated solutions' || oli.Cluster__c == 'DC delegated solutions' || oli.Cluster__c == 'WM delegated solutions' || oli.Cluster__c == 'NFP delegated solutions' || oli.Cluster__c == 'Insurers delegated solutions' || oli.Cluster__c == 'National/Public Funds delegated solutions'){
                         newOppty.DSWarning__c = true;
                         oliCopy.Assets_Under_Mgmt__c = null;
                         oliCopy.Inv_Segment__c = oli.Inv_Segment__c;
                         oliCopy.Inv_Client_Type__c = oli.Inv_Client_Type__c;
                         oliCopy.DS_Sales_Leader__c = oli.DS_Sales_Leader__c;
                         oliCopy.DS_Net_BPs_Fee__c = null;
                         oliCopy.DS_Sub_Manager_BPs_Fee__c = null;
                         oliCopy.validateOppProductFlag__c  = TRUE;
                         }                    
                         ReallocateRevenue(oliCopy);
                         oliListtoUpdate.add(oliCopy);
                         oppUpdList.add(newOppty);                    
                         i++;
                 }
                    
              
           }
               
               insert oliListtoUpdate;
                
 
               List<ScopeIt_Project__c> insScopProjLst = new list<ScopeIt_Project__c>();
              /* Set<Id> oppLineItemIdSet = new Set<Id>();
               for(OpportunityLineItem oli : oliListtoUpdate){
                   oppLineItemIdSet.add(oli.Id);
               }*/
               
               for(OpportunityLineItem oli : [Select Id, Name,Product2.Name, Scope_Reqd__c, UnitPrice, CurrencyIsoCode,
                                              Product2Id,OpportunityId From OpportunityLineItem Where Id IN :oliListtoUpdate]){
                   if(oli.Scope_Reqd__c){
                      ScopeIt_Project__c scp = new ScopeIt_Project__c();
                      scp.Name = oli.Product2.Name + ' - Project';
                      scp.Sales_Price__c=oli.unitprice;
                      scp.OpportunityProductLineItem_Id__c = oli.id;
                      scp.CurrencyIsoCode = oli.CurrencyIsoCode;
                      scp.Product__c = oli.Product2Id;
                      scp.Opportunity__c = oli.opportunityid;
                      insScopProjLst.add(scp);                   
                   }
               }
               
               if(!insScopProjLst.isEmpty()){
                   Database.insert(insScopProjLst,false);                   
               }
               
               system.debug('**GOPAL' + oppUpdList);
               List<Opportunity> s = new List<Opportunity>();
               s.addAll(oppUpdList);
               update s;
               
               /*List<OpportunityLineItem> olList = new  List<OpportunityLineItem>();  
               for(OpportunityLineItem oll:oliListtoUpdate){
               oll.validateOppProductFlag__c = FALSE;
               olList.add(oll);
               }
               update olList;*/
           
               return true;
           }Catch(Exception e){
               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg);
               return false;
           }
           
    }
     /*
     * @Description :This method calculates and allocates revenue to Opportunity Product
     * @ Args       : null   
     * @ Return     : void
     */
    public void ReallocateRevenue(OpportunityLineItem oli){
          Date rsdt = oli.revenue_start_date__c;
          Date redt = oli.revenue_end_date__c;
          Integer cyr = system.today().year();
          Double dbstend = (rsdt.daysbetween(redt)+1);
          
          if(rsdt.year()<>cyr) {
             oli.CurrentYearRevenue_edit__c = 0;
          }
          else if(redt.year() - rsdt.year() ==0 ){
             oli.CurrentYearRevenue_edit__c = oli.UnitPrice;
          }
          else{
             
             Double dbtw = rsdt.daysbetween(date.newInstance(system.today().year(),12,31)); //days between revenue start date and end of current year
             Double dbtw2 = rsdt.daysbetween(oli.Revenue_End_Date__c); // days between revenue start date and revenue end date
             Double divn = ((dbtw+1) / (dbtw2+1));
             Decimal cyrev = divn * oli.UnitPrice;
             system.debug((dbtw+1) +'....' + (dbtw2+1) + '....' + divn  +'....'+oli.unitprice);
             oli.CurrentYearRevenue_edit__c =  Math.round(divn * oli.UnitPrice);// cyrev;
          }
          
         if(redt.year() == rsdt.year() ){
             if(redt.year() == cyr+2)
                 oli.Year2Revenue_edit__c = 0;
             else if(redt.year() == cyr+1)
                 oli.Year2Revenue_edit__c = oli.UnitPrice;
             else
                 oli.Year2Revenue_edit__c = 0;
          }
          else if(rsdt.year() == cyr){
              if(redt.year() == cyr+1){
                  Double dbtw = date.newInstance(cyr+1,1,1).daysbetween(redt)+1;
                  oli.Year2Revenue_edit__c = Math.round((dbtw/dbstend )*oli.UnitPrice);   
              }
              else {
                  Double calc = 365.0/dbstend;
                  oli.Year2Revenue_edit__c = Math.round(calc *oli.UnitPrice);
              }
          }
          else if(rsdt.year() == cyr+1){
              Double dbtw = rsdt.daysbetween(date.newInstance(cyr+1,12,31))+1;
              oli.Year2Revenue_edit__c =   Math.round((dbtw/(dbstend))*oli.UnitPrice); 
          }
          else
              oli.Year2Revenue_edit__c = 0;
          
          oli.Year3Revenue_edit__c = oli.unitprice - (Math.round(oli.Year2Revenue_edit__c)) - Math.round(oli.CurrentYearRevenue_edit__c);
          
    }
    /*
     *  @Method Name: expansionOpportunity
     *  @Description: Method gets the Id and Name from opportunity record type and Updates them to Expansion Opportunity
     *  @Args : String
     *  @return: void  
     */
    public static void expansionOpportunity(String oppId)
    {
        String expansionRecordType = [Select Id, Name From RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'Opportunity_Locked_With_Product_Expansions' Limit 1].Id;
        
        Opportunity opp = new Opportunity(Id = oppId);
        opp.RecordTypeId = expansionRecordType ;
        update opp;
    }
    
        public void cloneOppteamMember(Opportunity newopp){
    
        List<OpportunityTeamMember> otmList = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> otmListtoUpdate=new List<OpportunityTeamMember>();          
        Map<Id,OpportunityTeamMember> oppTeamMap = new Map<Id,OpportunityTeamMember>(); 
                   
        otmList = [Select id,OpportunityId,Role__c,UserId,OpportunityAccessLevel,TeamMemberRole,OppTeamMemID__c 
                  From OpportunityTeamMember where OpportunityId =:oldoppId ];//and OppTeamMemID__c!= :UserInfo.getUserId()];
                
        if(otmList.size()>0){        
            for(OpportunityTeamMember  otmem:otmList){            
                OpportunityTeamMember otm=new OpportunityTeamMember();                
                otm=otmem.clone(false,true);
                otm.Opportunityid=newopp.id;                
                otmListtoUpdate.add(otm);            
            }
        }                               

        for(OpportunityTeamMember oppTeam : otmList ){
        
      /*  [Select Id, OpportunityId, UserId From OpportunityTeamMember 
                                             Where OpportunityId =:oldoppId AND 
                                             (UserId = :newopp.OwnerId OR UserId = :newopp.CreatedById) ]){*/
                oppTeamMap.put(oppTeam.UserId,oppTeam);                
        }
        
        if(!oppTeamMap.containsKey(newopp.OwnerId)){
            OpportunityTeamMember otmOwner = new OpportunityTeamMember();
            otmOwner.userId = newopp.OwnerId;                            
            otmOwner.OpportunityId = newopp.Id;                        
            otmListtoUpdate.add(otmOwner);   
        }
        if(!oppTeamMap.containsKey(UserInfo.getUserId())  && newopp.OwnerId != UserInfo.getUserId() ){
            OpportunityTeamMember otmCreator = new OpportunityTeamMember();
            otmCreator.userId = UserInfo.getUserId();                            
            otmCreator.OpportunityId = newopp.Id;                        
            otmListtoUpdate.add(otmCreator); 
        }

        
        if(isCloneWithProduct){
            OpportunityTeamMember otmCloneOwner = new OpportunityTeamMember();
            otmCloneOwner.userId = newopp.OwnerId;                            
            otmCloneOwner.OpportunityId = newopp.Id;                                        
            insert otmCloneOwner;
            
            if(newopp.OwnerId !=  UserInfo.getUserId()){
                OpportunityTeamMember otmCloneCreator = new OpportunityTeamMember();
                otmCloneCreator.userId = UserInfo.getUserId();                            
                otmCloneCreator.OpportunityId = newopp.Id;                        
                insert otmCloneCreator;            
            }             
        }
        else{        
           insert otmListtoUpdate;
        }
    }
    public void cloneSalesCredit(String newoppId){
        system.debug('cloneSalesCredit****'+newoppId);
          List<Sales_Credit__c> scList =  [select Id,CreatedById,LastModifiedById,Opportunity__c,Work_Email__c,Employee_Office__c, EmpName__c, Role__c, LOB__c, Percent_Allocation__c,Employee_Status__c, Country__c from Sales_Credit__c where Opportunity__c =:oldoppId];
        system.debug('scList ****'+scList );  
        
           system.debug('newoppId****'+newoppId);
           List<Sales_Credit__c> sclisttoUpdate=new List<Sales_Credit__c>();
            
            for(Sales_Credit__c sc:scList){
                
                Sales_Credit__c scredit=new Sales_Credit__c();
                scredit= sc.clone(false,true);
                scredit.CreatedById=sc.CreatedById;
                scredit.LastModifiedById=sc.LastModifiedById;
                scredit.Opportunity__c=newoppId;
                scredit.Role__c=sc.Role__c;
                sclisttoUpdate.add(scredit);
            }
            system.debug('sclisttoUpdate****'+sclisttoUpdate);   
            if(sclisttoUpdate.size()>0){
                insert sclisttoUpdate;
            }
    }
    webservice static String getProdStatusCount(String opp){
        productCountList=new List<productStatusCount>();
        List<OpportunityLineItem> oppLIList = [Select Id, Name,Project_linked__c,Product2.isactive, OpportunityId,Product_Name_LDASH__c, Product2Id,Project_Manager__c, Sales_Price_USD_calc__c, is_Project_Required__c FROM OpportunityLineItem WHERE OpportunityId =:opp];
        System.debug('oppLIList***************'+oppLIList);
        Integer actCount=0;
        Integer inActCount=0;
        Integer projectNotLinked=0;
        for(OpportunityLineItem oli:oppLIList){
        
            if(oli.Product2.isactive==True){
                actCount++;
                System.debug('actCount***************'+actCount);
                
            }else if(oli.Product2.isactive==False){
                inActCount++;
                System.debug('inActCount***************'+inActCount);
                
            }
            
            if(oli.is_project_required__c==True){
                if(oli.Project_linked__c==False){
                    projectNotLinked++;
                    System.debug('projectNotLinked***************'+projectNotLinked);
                }
            }
        
        }
        productCountList.add(new productStatusCount(actCount,inActCount,projectNotLinked));
        System.debug('productCountList***************'+productCountList);
        return JSON.serialize(productCountList);

    }
    
  /*  Defct#16538 */
        public Mercer_Office_Geo_D__c getOppGeo(Opportunity newOppty){
            Map<String,Mercer_Office_Geo_D__c> geoDMap = new Map<String,Mercer_Office_Geo_D__c>();
            
            for(Mercer_Office_Geo_D__c geoD : [Select Id, Office__c, Country__c,Region__c, Sub_Region__c,Market__c, 
                                               Sub_Market__c From Mercer_Office_Geo_D__c 
                                               Where Status__c = 'A'])
            {
                    geoDMap.put(geoD.Office__c,geoD);    
            }                                               
            Mercer_Office_Geo_D__c oppGeoD = geoDMap.get(newOppty.Opportunity_Office__c);
        
            return oppGeoD;
        }
    
    class productStatusCount{
        public integer inactivecount{set;get;}
        public integer activecount{set;get;}
        public integer projectNotLinkedCount{set;get;}
        public productStatusCount(Integer actCont,Integer inActCnt,Integer projNotLinkCnt){
            
            this.activecount=actCont;
            this.inactivecount=inActCnt;
            this.projectNotLinkedCount=projNotLinkCnt;
        }
    }
    
}