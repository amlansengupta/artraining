global class BatchToCopySSDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopySSDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String header = 'Account,Growth Plan,Strength,Record Status \n';
        string hearder2= 'Id,Account,Growth Plan,Strength,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear  ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>(); 
        System.debug('swot Strenth>>oldGPList>> '+oldGPList.size());
        try {
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
				
		}
		
		Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
		List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
		for(Account a : accList){
			for(Growth_Plan__c gg : a.Growth_Plans__r){
				accIdVsNewGp.put(gg.Account__c,gg);
			}  
		}
        System.debug('swot Strenth>>accList>> '+accList.size());
        
        List<Swot_Strength__c> insertNewSSObjList = new List<Swot_Strength__c>();	 
        List<Swot_Strength__c> ssObjectList = [select id,name,Account__c,Growth_Plan__c,Strength__c,LastModifiedBy.Name,lastModifiedDate from Swot_Strength__c where Growth_Plan__c IN :OldGPList];
        System.debug('swot Strenth>>ssObjectList>> '+ssObjectList.size());
        if(ssObjectList != null && ssObjectList.size()> 0){
            for(Swot_Strength__c ssOldObj : ssObjectList){
                if(accIdVsNewGp.containsKey(ssOldObj.Account__c)){
                    Swot_Strength__c ssNewObj = new Swot_Strength__c();
                    
                    if(ssOldObj.Account__c != null)
                        ssNewObj.Account__c = ssOldObj.Account__c;
                    ssNewObj.Growth_Plan__c = accIdVsNewGp.get(ssOldObj.Account__c).Id;
                    
                    if(ssOldObj.Strength__c != null)
                        ssNewObj.Strength__c = ssOldObj.Strength__c;
                    
                    ssNewObj.Strength_LastModified_By__c = ssOldObj.LastModifiedBy.Name;
                    DateTime dt = ssOldObj.LastModifiedDate;
                    ssNewObj.Strength_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
                   
                   insertNewSSObjList.add(ssNewObj);
                }
            }
        }
        System.debug('swot Strenth>>insertNewSSObjList>> '+insertNewSSObjList.size());
        if(insertNewSSObjList!=null && insertNewSSObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewSSObjList,false);
	    		if(srList!=null && srList.size()>0){
    				for(Integer i=0;i<srList.size();i++){
    					
    					String strenght='';
					if(insertNewSSObjList[i].Strength__c!=null)
						strenght=insertNewSSObjList[i].Strength__c;
    					
    					if(srList.get(i).isSuccess()){
    						finalSuccessStr+=srList.get(i).getId()+','+insertNewSSObjList[i].Account__c+','+insertNewSSObjList[i].Growth_Plan__c+','+strenght.replace(',', '')+', Successfully created \n';
    					}
    					if(!srList.get(i).isSuccess()){
    						Database.Error error = srList.get(i).getErrors().get(0);
    						finalFailStr+=insertNewSSObjList[i].Account__c+','+insertNewSSObjList[i].Growth_Plan__c+','+strenght.replace(',', '')+','+error.getMessage()+' \n';
    					}
    				}
    			}
	    	}
	    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }	
    }
    
    global void finish(Database.BatchableContext bc){
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Swot Strength Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Swot Strength Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Swot Strength Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Swot Strength copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}