global class AP28_UserSyncSchedulable implements Schedulable{
    
    global Set<String> mogdSet = new Set<String>();
    
    global String userQuery;
     
    global void execute(SchedulableContext sc)
    {
        for(Mercer_Office_Geo_D__c officeGeo : [select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True])
        {
            mogdSet.add(officeGeo.Name);
        }
        
        this.userQuery = 'select Location__c, Market__c, Sub_Market__c, Sub_Region__c, Region__c, Employee_Office__c, Country__c  from User where Location__c IN' + quoteKeySet(mogdSet);
        Database.executeBatch(new AP29_UserSyncBatchable(userQuery), Integer.valueOf(system.label.CL51_OfficeBatchSize)); 
    }
    
    //convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
    global String quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }
}