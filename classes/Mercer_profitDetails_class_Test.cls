/*Purpose: Test class to provide test coverage for Mercer_profitDetails_class class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/1/2014  Created test class to provide test coverage for Mercer_profitDetails_class class
      
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Mercer_profitDetails_class_Test {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    private static Integer month = System.Today().month();
    private static Integer year = System.Today().year();
   /*
     * @Description : Test class for calculating Profit Details for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTest_RET() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            String LOB_RET = 'Retirement';
            
            Product2 testProduct1 = mtscopedata.buildProduct(LOB_RET);
            ID prodId_RET = testProduct1.id;
            
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry1 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_RET and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId1 = pbEntry1.Id;
                 
            OpportunityLineItem opptylineItem = new OpportunityLineItem(); 
            opptylineItem.OpportunityId = opptyId;
            opptylineItem.PricebookEntryId = pbentryId1;
            //Updated start date and end dates by Jyotsna
            opptyLineItem.Revenue_Start_Date__c = date.newInstance(year, month, 1);
            opptyLineItem.Revenue_End_Date__c = date.newInstance(year, month, 10);
            opptylineItem.UnitPrice =600000000;
            opptyLineItem.CurrentYearRevenue_edit__c = 600000000;
            //opptyLineItem.Year2Revenue_edit__c = 1;             
            insert opptylineItem;
         
            ID oliId1= opptylineItem.id;
            String oliCurr1 = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice1 = opptylineItem.unitprice;          
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            
              
                List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                //Map<id,Margin_Benchmark__c> mapMargin = new Map<id,Margin_Benchmark__c>([select id,LOB__c, Country__c, Amber__c, Green__c, Red__c from Margin_Benchmark__c]);
            Test.startTest();    
                ScopeIt_Project__c testScopeitProj1 = mtscopedata.buildScopeitProject(prodId_RET, opptyId,oliId1,oliCurr1,oliUnitPrice1 );
                ScopeIt_Task__c  testScopeitTask1 =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee1 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                scopeitList.add(testScopeitProj1);
                  
                ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
                
            Test.stopTest();
        }
   }
    
    /*
     * @Description : Test class for calculating Profit Details for Product with 'Cross Business Offerings (CBO)' LOB   
    */
   static testMethod void myUnitTest_CBO() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
                 
            String LOB_CBO = 'Cross Business Offerings (CBO)';
            
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
               
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem opptylineItem = new OpportunityLineItem(); 
            opptylineItem.OpportunityId = opptyId;
            opptylineItem.PricebookEntryId = pbentryId2;
            opptyLineItem.Revenue_Start_Date__c = date.newInstance(year, month, 1);
            opptyLineItem.Revenue_End_Date__c = date.newInstance(year, month, 10);
            opptylineItem.UnitPrice =3000;
            opptyLineItem.CurrentYearRevenue_edit__c = 3000;
            insert opptylineItem;
         
            ID oliId2= opptylineItem.id;
            String oliCurr2 = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice2 = opptylineItem.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            Test.startTest();
            
                List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                testScopeitEmployee2.Hours__c = 40;
                update testScopeitEmployee2; 
                scopeitList.add(testScopeitProj2);
                
                
                ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
                         
            Test.stopTest();
        }
    }
    
     /*
     * @Description : Test class for calculating Profit Details for Product with 'Employee Health & Benefits' LOB   
    */ 
    static testMethod void myUnitTest_EHB() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB_CBO = 'Employee Health & Benefits';
                
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
               
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];          
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem opptylineItem2 = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId2);
            ID oliId2= opptylineItem2.id;
            String oliCurr2 = opptylineItem2.CurrencyIsoCode;
            Double oliUnitPrice2 = opptylineItem2.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            Test.startTest();
            
                List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                scopeitList.add(testScopeitProj2);         
                 
                ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
              
             Test.stopTest();
        }
     }
    
     /*
     * @Description : Test class for calculating Profit Details for Product with 'Benefits Admin' LOB   
    */      
    static testMethod void myUnitTest_BA() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB_CBO = 'Benefits Admin';
                
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
              
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
              
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem opptylineItem2 = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId2);
            ID oliId2= opptylineItem2.id;
            String oliCurr2 = opptylineItem2.CurrencyIsoCode;
            Double oliUnitPrice2 = opptylineItem2.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            Test.startTest();
            
                List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                scopeitList.add(testScopeitProj2);
                   
                ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
                  
            Test.stopTest();
        }
   }
    
    
     /*
     * @Description : Test class for calculating Profit Details for Product with 'Investments' LOB   
    */ 
   static testMethod void myUnitTest_INV() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            
            String LOB_CBO = 'Investments';
                
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
               
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];         
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem  opptylineItem2 =new OpportunityLineItem(); 
            opptylineItem2.OpportunityId = opptyId;
            opptylineItem2.PricebookEntryId = pbEntryId2;
            opptyLineItem2.Revenue_Start_Date__c = date.newInstance(year, month, 1);
            opptyLineItem2.Revenue_End_Date__c = date.newInstance(year, month, 10);
            opptylineItem2.UnitPrice = 1.00;
            opptyLineItem2.CurrentYearRevenue_edit__c = 1;
            opptyLineItem2.Inv_Segment__c = 'Corporates/Employers';
            opptyLineItem2.Inv_Client_Type__c = 'Charity';
            //opptyLineItem2.Year2Revenue_edit__c = 1; 
            insert opptyLineItem2;           
            
             ID oliId2= opptylineItem2.id;
             String oliCurr2 = opptylineItem2.CurrencyIsoCode;
             Double oliUnitPrice2 = opptylineItem2.unitprice;
             String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
             Test.startTest();

                 List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                 ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                 ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                 ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                 scopeitList.add(testScopeitProj2);
                
                 ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                 Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
                
             Test.stopTest();
        }
    }
     
    /*
     * @Description : Test class for calculating Profit Details for Product with 'Talent' LOB   
    */   
   static testMethod void myUnitTest_TAL() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
                    
            String LOB_CBO = 'Talent';
                
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
               
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];          
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem opptylineItem2 = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId2);
            ID oliId2= opptylineItem2.id;
            String oliCurr2 = opptylineItem2.CurrencyIsoCode;
            Double oliUnitPrice2 = opptylineItem2.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            Test.startTest();
                List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                scopeitList.add(testScopeitProj2);
                
                ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
                
            Test.stopTest();
        }
   }
   
   
      
   /*
     * @Description : Craeted test class for negative Test   
    */ 
   static testMethod void myUnitTestNegative() {
        User user1 = new User(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
                   
            String LOB_CBO = 'Talent123';
                
            Product2 testProduct2 = mtscopedata.buildProduct(LOB_CBO);
            ID prodId_CBO = testProduct2.id;
            
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry2 = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId_CBO and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId2 = pbEntry2.Id;
               
            OpportunityLineItem opptylineItem2 = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId2);
            ID oliId2= opptylineItem2.id;
            String oliCurr2 = opptylineItem2.CurrencyIsoCode;
            Double oliUnitPrice2 = opptylineItem2.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
          
            Test.startTest();
                 List<ScopeIt_Project__c> scopeitList = new List<ScopeIt_Project__c>();
                 ScopeIt_Project__c testScopeitProj2 = mtscopedata.buildScopeitProject(prodId_CBO, opptyId,oliId2,oliCurr2,oliUnitPrice2 );
                 ScopeIt_Task__c  testScopeitTask2 =  mtscopedata.buildScopeitTask();               
                 ScopeIt_Employee__c testScopeitEmployee2 =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                 scopeitList.add(testScopeitProj2);
                   
                 
                 ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
                 Mercer_profitDetails_class controller = new Mercer_profitDetails_class(stdController);
            
            Test.stopTest();
        }
    }          
}