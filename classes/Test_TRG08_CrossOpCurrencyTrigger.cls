/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose: Test Class for providing code coverage to TRG08_CrossOpCurrencyTrigger class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   02/04/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_TRG08_CrossOpCurrencyTrigger 
{
	/*
     * @Description : Test method to provide data coverage to populate the Amount USD field 
     				  converted to USD on CrossOpCo Referral Object.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        // List of Colleague records
        List<Colleague__c> testColleagueList = new List<Colleague__c>();
        Colleague__c testColleague;
           for(integer i=0; i<200; i++) 
        	{
    			testColleague = new Colleague__c();
	            testColleague.Name = 'TestColleague1'+i;
	            testColleague.EMPLID__c = '12345678901'+i;
	            testColleague.LOB__c = '2222'+i;
	            testColleague.Empl_Status__c = 'Active';
	            testColleague.Level_1_Descr__c = 'Merc';
	            testColleagueList.add(testColleague);
        	}
	            insert testColleagueList;
        
         User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        // List of Account records
        List<Account> testAccList = new List<Account>();
        Account testAcc;
        	for(integer i=0; i<200; i++)
        	 {
        	 	 testAcc = new Account();
	             testAcc.Name = 'TestAccountName'+i;
	             testAcc.BillingCity = 'TestCity'+i;
	             testAcc.BillingCountry = 'TestCountry'+i;
	             testAcc.BillingStreet = 'Test Street'+i;
	             testAcc.Relationship_Manager__c = testColleagueList[i].Id;
	             testAccList.add(testAcc);
        	 }
	             insert testAccList;
         
         // List of CrossOpCo records
         List<Cross_OpCo_Referral__c> testCrossOppList = new List<Cross_OpCo_Referral__c>();
         Cross_OpCo_Referral__c testCrossOpp;    
         	for(integer i=0; i<200; i++)
         	 {         
	        	 testCrossOpp = new Cross_OpCo_Referral__c();
	             testCrossOpp.Referral_To__c = 'Marsh'+i;
	             testCrossOpp.Name = 'ABC'+i;
	             testCrossOpp.Referral_Description__c = 'abcdefgh'+i;
	             testCrossOpp.Account__c = testAccList[i].Id;
	             testCrossOpp.Referral_To_Manager__c = testColleagueList[i].Id;
	             testCrossOpp.Amount__c = 1000;
	             testCrossOpp.CurrencyIsoCode = 'ALL';
	             testCrossOppList.add(testCrossOpp);
         	 }
	             insert testCrossOppList;       
            
            // Update Amount field on CrossOpCo object to fire the trigger
            for(integer i=0; i<200; i++)
             {
             	 testCrossOppList[i].Amount__c = 2000;
             } 
                 update testCrossOppList;
        }        
    }
}