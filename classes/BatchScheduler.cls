/* Purpose: This is a Universal Batch Scheduler class which can be  used  anytime to schedule any batch class.
===================================================================================================================================
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          01/27/2015  Universal Batch Scheduler class which can be  used  anytime to schedule any batch class
 ======================================================================================================================================*/


global class BatchScheduler implements Schedulable {
  global Database.Batchable<SObject> batchClass{get;set;}
  global Integer batchSize{get;set;} {batchSize = Integer.ValueOf(label.BatchSize);} 

  global void execute(SchedulableContext sc) {
   database.executebatch(batchClass, batchSize);
  } 
}


/* Initiating the schedulable class

// Instantiate the batch class
MyBatch myBatch = new MyBatch();

// Instantiate the scheduler
BatchScheduler scheduler = new BatchScheduler();

// Assign the batch class to the variable within the scheduler
scheduler.batchClass = myBatch;

// Run every day at 1pm
String sch = '0 0 13 * * ?';

System.schedule('MyBatch - Everyday at 1pm', sch, scheduler);
*/