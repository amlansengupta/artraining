/*
Class Name - TestDC26_OppTeamMemberInsertBatch
Date -    28/10/2013
Purpose - To Test the Functionality of DC26_OppTeamMemberInsertBatch.
*/
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest
private class Test_DC26_OppTeamMemberInsertBatch 
{
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
          
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    	Request No. 17953 : Removing Finalist END */
    }
    /*
     * @Description : Test method to provide data coverage to DC26_OppTeamMemberInsertBatch class 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void testBatch() 
    {    
       
        createCS();
        User user1 = new User();
        String adminprofile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(adminprofile , 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        //Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
                      
        //Create Account Record
        Account testAccounti = new Account();
        testAccounti.Name = 'TestAccountName' ;
        testAccounti.BillingCity = 'TestCity';
        testAccounti.BillingCountry = 'TestCountry';
        testAccounti.BillingStreet = 'Test Street';
        testAccounti.Relationship_Manager__c = testColleague.Id;
        testAccounti.One_Code__c = '111';
        testAccounti.Competitor_Flag__c = True;
        insert testAccounti;
        
        //Create Opportunity Record
        Opportunity testoppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccounti.Id;
        //request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        testOppty.CloseDate = date.today();
        insert testOppty;
        
         

        
      /*  String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //Creaate User Record
        User testUser = new User();
        testUser.alias = 'test';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'Test';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = mercerStandardUserProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'DC26_OppTeamMemberInsertBatch@abc.com';
        testUser.EmployeeNumber = '1234567890';
        testUser.Location__c = 'Test';
        testUser.isActive = True;
        insert testUser; */
                 Sales_Credit__c sc = new Sales_Credit__c();
        sc.Opportunity__c = testOppty.Id;
        sc.EmpName__c = testColleague.Id;
        sc.Percent_Allocation__c = 20;
        //sc.Emp_Id__c = '1234567890';
        insert sc;
        
        
        DC26_OppTeamMemberInsertBatch batch = new DC26_OppTeamMemberInsertBatch();
        database.executeBatch(batch); 
        }   
        
    }
    
        
}