/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
                            1.0 -    Shashank 03/29/2013  Created Utility Class for TRG17_FeedCommentTrigger
                            1.2 - 	Sarbpreet	5/12/2015 As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.  
=============================================================================================================================================== 
*/

public class AP51_FeedCommentReporting
{
    public static List<ChatterFeedReporting__c> chatterFeedList = new List<chatterFeedReporting__c>();
    public static Map<string , string> objectPrefixMap = new Map<string, string>();
    private static final String STR_Chatter_Free = 'Chatter Free' ;
    
    public AP51_FeedCommentReporting()
    {
        setPrefixMap();
    }
    
    public static void setPrefixMap()
    {
        schema.DescribeSObjectResult accResult = Account.SObjectType.getDescribe();
        string accPrefix = accResult.getKeyPrefix();
        objectPrefixMap.put('Account', accPrefix);
        schema.DescribeSObjectResult conResult = Contact.SObjectType.getDescribe();
        string conPrefix = conResult.getKeyPrefix();
        objectPrefixMap.put('Contact', conPrefix);
        schema.DescribeSObjectResult oppResult = Opportunity.SObjectType.getDescribe();
        string oppPrefix = oppResult.getKeyPrefix();
        objectPrefixMap.put('Opportunity', oppPrefix);
        schema.DescribeSObjectResult useResult = User.SObjectType.getDescribe();
        string usePrefix = useResult.getKeyPrefix();
        objectPrefixMap.put('User', usePrefix);
        schema.DescribeSObjectResult groupResult = CollaborationGroup.SObjectType.getDescribe();
        string groupPrefix = groupResult.getKeyPrefix();
        objectPrefixMap.put('Group', groupPrefix);
    }
    /*
     *  Method Name: insertFeedComment
     *  Description: Insert ChatterFeedReporting on insert of Chatter Feed Comment.
     *  Arguements: List
     */
    public static void insertFeedComment(List<FeedComment> triggernew)
    {

        try
        {
            chatterFeedList.clear();
            setPrefixMap();
                set<string> groupIdSet = new set<string>();
                Map<string, string> groupMap = new Map<string, string>();
                //As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.           
                Set<ID> CreatedByIDSet = new Set<ID>();
	            Set<ID> profileIDSet = new Set<ID>();
            	String Userlicense;
            	ID Userprofileid;
	            
	            for(FeedComment feed:triggernew)
	            {
	            	CreatedByIDSet.add(feed.CreatedByID);
	            }             
	            Map<id, User> userMap = new Map<id, User>([Select id, ProfileID from user where id IN :CreatedByIDSet]);
	            
	            for(User usr : userMap.values())
          		{
           			profileIDSet.add(usr.ProfileID);
           		}

			Map<id, Profile> profileMap = new Map<id, Profile>([select id, UserLicense.name from profile where ID IN: profileIDSet]);
            //End of CA Incident #33609319
               
               
                for(FeedComment feed:triggernew)
                {
                    string parent = feed.ParentId;
                    string feedbody = feed.CommentBody;
                    boolean bypass = false;
                    
                    //As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.                                         	
                 	if(userMap.containsKey(feed.CreatedByID))
                	{
	                	Userprofileid = userMap.get(feed.CreatedByID).ProfileID;
	                	Userlicense = profileMap.get(Userprofileid).UserLicense.name;
                	}
                     
                    if(feedBody <> null)
                    {
                    	//As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.           
              
                    	if( Userlicense == STR_Chatter_Free )
                    	{
                        	bypass = true;  
                    	}
                    	//End of CA Incident #33609319
               	 }
                    
                    if((parent.substring(0,3)==objectPrefixMap.get('Account')
                        ||parent.substring(0,3)==objectPrefixMap.get('Contact')
                        ||parent.substring(0,3)==objectPrefixMap.get('Opportunity')
                        ||parent.substring(0,3)==objectPrefixMap.get('User')
                        ||parent.substring(0,3)==objectPrefixMap.get('Group'))&&!bypass)
                    {
                        if(parent.substring(0,3)==objectPrefixMap.get('Group'))
                        {
                            groupIdSet.add(feed.ParentId);
                        }
                        else
                        {
                            ChatterFeedReporting__c chatterFeed = new chatterFeedReporting__c();
                            chatterFeed.Name = feed.Id;
                            chatterFeed.Posted_By__c = feed.CreatedById;
                            chatterFeed.Post_Type__c = 'TextPost';
                            chatterFeed.Post__c = feed.CommentBody;
                            
                            if(parent.substring(0,3)==objectPrefixMap.get('Account'))
                            {
                                chatterFeed.Account__c = feed.ParentId;
                                //chatterFeed.Chatter_Object__c = 'Account';
                            }
                            else if(parent.substring(0,3)==objectPrefixMap.get('Contact'))
                            {
                                chatterFeed.Contact__c = feed.ParentId;
                                //chatterFeed.Chatter_Object__c = 'Contact';
                            }
                            else if(parent.substring(0,3)==objectPrefixMap.get('Opportunity'))
                            {
                                chatterFeed.Opportunity__c = feed.ParentId;
                                //chatterFeed.Chatter_Object__c = 'Opportunity';
                            }
                            else if(parent.substring(0,3)==objectPrefixMap.get('User'))
                            {
                                chatterFeed.User__c = feed.ParentId;
                                //chatterFeed.Chatter_Object__c = 'User';
                            }
                            
                            chatterFeedList.add(chatterFeed);
                        }
                    }
                }
                
                for(CollaborationGroup grp:[select Id, Name from CollaborationGroup where ID IN:groupIdSet])
                {
                    groupMap.put(grp.Id, grp.Name);
                }
                
                for(FeedComment feed:triggernew)
                {
                    if(groupMap.containsKey(feed.ParentId))
                    {
                        ChatterFeedReporting__c chatterFeed = new chatterFeedReporting__c();
                        chatterFeed.Name = feed.Id;
                        chatterFeed.Posted_By__c = feed.CreatedById;
                        chatterFeed.Post_Type__c = 'TextPost';
                        chatterFeed.Post__c = feed.CommentBody;
                        chatterFeed.Group__c = groupMap.get(feed.ParentId);
                        //chatterFeed.Chatter_Object__c = 'Group';
                        chatterFeedList.add(chatterFeed);
                    }
                }
                
                insert chatterFeedList;
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
            
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        }
        
    }
      /*
     *  Method Name: deleteFeedComment
     *  Description: Delete ChatterFeedReporting on delete of Chatter Feed Comment.
     *  Arguements: List
     */
    public static void deleteFeedComment(List<FeedComment> triggerold)
    {

        try
        {
            chatterFeedList.clear();
            set<string> feedIdSet = new set<string>();
            for(FeedComment feed:triggerold)
            {
                feedIdSet.add(feed.Id);
            }
            
            for(ChatterFeedReporting__c chatterFeed:[select Id from ChatterFeedReporting__c where Name IN:feedIdSet])
            {
                chatterFeedList.add(chatterFeed);
            }
            
            delete chatterFeedList;
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage()); 
            triggerold[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
           
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage()); 
            triggerold[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
          
        }
    } 
}