/*Purpose: Test Class for providing code coverage to MS06_OpportunityRevenueBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   05/18/2013  Created class
   2.0 -    Madhavi 12/3/2014   created test method "checkAccountStatus()"
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = false)
private class Test_MS06_OpportunityRevenueBatchable  {
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
          /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    	Request No. 17953 : Removing Finalist END */
    }
    
    /*
     * @Description : Test method to provide data coverage for updating Account Status
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {  
        createCS();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.MercerForce_Account_Status__c = 'Active Contact';
        Acc.Total_CY_Revenue__c =50000.00;
        Acc.Total_PY_Revenue__c =50000.00;
        insert Acc;
        
        Opportunity opp= new Opportunity();
        opp.Name = 'Test Opportunity2' + String.valueOf(Date.Today());
        opp.Type = 'New Client';
        //Request Id:12638 commenting step
        //opp.Step__c = 'Identified Deal';
        opp.CloseDate = Date.Today();
        opp.CurrencyIsoCode = 'USD';
        opp.AccountId = Acc.id;
        opp.StageName = 'Closed / Won';
        opp.Total_Opportunity_Revenue_USD__c = 200000.00;
        opp.Opportunity_Office__c = 'Montreal - McGill';
        insert opp;
    
      test.startTest();
        database.executeBatch(new MS06_OpportunityRevenueBatchable(), 500);
       test.stopTest();
       }
       
    }
    /*
     * @Description : Test method to provide data coverage when MercerForce_Account_Status__c  not equal to LowRevenue.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void checkAccountStatus(){
        createCS();
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        acc.MercerForce_Account_Status__c ='High Revenue';
        acc.Total_CY_Revenue__c =25000.00;
        acc.Total_PY_Revenue__c =25000.00;
        update acc;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);

        
        system.runAs(User1){
         test.startTest();
             database.executeBatch(new MS06_OpportunityRevenueBatchable(), 500);
         test.stopTest();
        }
       
        
       }
}