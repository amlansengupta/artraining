/*
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL
1.0-        Jagan       11/8/2013   To notify Users on Create or update of Task
2.0-        Sarbpreet   12/16/2013  As per Request#3472 (January Release)added logic to automatically assign Campaign to a task created by Marketo.
3.0-        RaviKiran   5/13/2013   As per Request #4188(June Release)more fields and comments to make it similar to out of box email notification
4.0-        Jagan       5/20/2014   Updated code to show Campaign details as well.
5.0-        Sarbpreet   08/12/2014  As per Request #5094(September Release)added logic to create an Opportunity for Marketo Task
6.0-        Jagan       10/22/2014  Fixed issue related to opportunity creation  (#5235)
7.0-        Madhavi     11/11/2014  Added method "relatedActivityLastModifiedbyUser"  as per #5079(dec 2014 release)
8.0-        Jagan       12/2/2014   Updated code for 5590  
9.0-        Venkat      2/6/2015    Updated code for Req#6244, July release 2015
10.0-       Sarbpreet   6/18/2015   Updated code as per Request#5166 (July Release 2015)
1. Removed the logic of associating the campaign with task. 
2. Removed logic to check if duplicate opportunity is not being created
3. Updated send_email method to send email to atF opportunity owner created by Marketo. 
4. Now this method does not send email to the user when a task is created) 
11.0-        Bala        10/6/2015  As per Request 7972 (December Release) change the from email address to 'MarketingLeads@mercer.com'
If any opportunity created by Marketo and stage is above ATFO send an email notification to opportunity owner.
Now the from address is 'MarketingLeads@mercer.com' through the Organization wide email address. 
12.0        Jigyasu      19/5/2016  Code has been modified to meet the functionality described in req #9506. 
13.0        Apoorva      27/7/2016   Code has been added to meet the requirement of Req 10616                
14.0        Trisha       01/18/2019  Request#12638 Replacing step with stage                                     
=============================================================================================================================
*/

/*
Called From Trigger: Yes
If Yes, Trigger Name: Task_NotifyUser
Purpose : To notify Users on Create or update of Task 
*/

public without sharing class Task_Send_Notification_email {
    private static final String EXCEPTION_STR = 'Exception occured with reason :' ;
    private static final String STR_NEWLEAD = '|New Lead| - ' ;
    private static final String STR_DASH = ' - ' ;
    private static final String STR_STEP = 'Executing Discovery' ;
    private static final String STR_STAGE = 'Above the Funnel' ;
    private static final String STR_TYPE = 'Existing Client - New LOB' ;
    private static final String STR_CURRENCYCODE = 'USD' ;
    private static final String STR_LEADSOURCE = 'Marketing Assigned' ;
    private static final String STR_STATUS = 'Closed - Create Opportunity' ;
    private static final String STR_STATUS_RFC = 'Closed - Create Opportunity' ;    
    //Updated code as per Request#5166
    public static final String STR_MERCERENAME = 'MercerForce Global Team';
    public static final String STR_MERCEREMAIL = 'MercerForce@mercer.com';
    public static final String STR_MERCERSUBJECT = 'Action Required: You have been assigned this marketing qualified Above-the-Funnel opportunity to pursue';
    private static final String STR_MERCER_BRACES = '- ';
    //Updated code as per Request#5166
    private static final String STR_MERCER_IMAGE1 = 'Mercer_Opp_Image1';
    private static final String STR_MERCER_IMAGE2 = 'Mercer_Opp_Image2';
    private static final String STR_MERCER_IMAGE3 = 'Mercer_Opp_Image3';
    private static final String STR_MERCER_IMAGE4 = 'Mercer_Opp_Image4';
    private static final String STR_MERCER_IMAGE5 = 'Mercer_Opp_Image5';
    private static final String STR_MERCER_IMAGE6 = 'Mercer_Opp_Image6';
    private static final String STR_MERCER_IMAGE7 = 'Mercer_Opp_Image7';
    public static boolean FLAG_CHECK_OPP = FALSE;
    public static boolean FLAG_CHECK_TASK = FALSE;    
    public static boolean FLAG_TEMP_OPP_OFFICE = FALSE;  
    public static boolean createOppRFC = false;
    //Send email when Notify user check box is checkced
    //As per Request#5166 (July Release 2015) updated method to send the email when the Opportunity is created when a tsk is generated through Marketo
    //The method will no more send the email when Notify user check box is checkced
    
    /*
* @Description : As apart of Request#3472 (January Release) this method automatically assigns a Campaign to a task created by Marketo. 
* @ Args       : triggernew
* @ Return     : void
*/
    public static void task_assign(Map<Id,Task> triggernewMap)
    {
        Map<Task,String> taskMap = new Map< Task, String>();
        Map<String,Campaign> campaignMap = new Map<String,Campaign> ();
        List<Task> updateTask = new List<Task>();
        //Added as part of Request #5094 (September Release)
        List<opportunity> insertOppList = new List<opportunity>();
        Set<ID> contactID = new Set<ID>();
        Opportunity oppty;
        Contact con;
        String subjectSubstring = '';
        //As per Request#5166 (July Release 2015)
        Map<ID,Task> taskContIDMap = new Map< ID, Task>();
        //As per Request#5166 (July Release 2015)
        Map<Opportunity,Task> oppTaskMap = new Map< Opportunity, Task>();
        
        Map<ID, String> userEmpCode = new Map<ID, String>(); 
        Map<ID, ID> useridsMap =  new Map<ID, ID>();
        
        Set<Id> OppId = new Set<Id>();
        
        // Retrieve all users for Marketo Sync Profile
        String Marketo_Profile = Label.Marketo_Sync_Profile_Name;
        Set<ID> userId = new Set<ID>(); 
        Map<ID,ID> taskWhoidMap = new Map<ID,ID>();
        //Map<Id,User> userMap = new Map<Id,User>([Select id, name, ProfileId from User where Profile.Name = :Marketo_Profile]);          
        Map<Id,User> userMap = new Map<Id,User>([Select id, name, ProfileId from User]);          
        
        // Check for tasks created by Marketo and fetch the Campaign name mentioned inside Subject
        //List<Task> lstTaskUpdate = [Select Id,Whatid,Whoid,Subject,CreatedById,Description,Ownerid,Owner.isActive,LeadSource__c from Task where id in :triggernewMap.keySet()  and createdById IN :userMap.keySet() and status =: STR_STATUS];
        List<Task> lstTaskUpdate = [Select Id,Whatid,Whoid,Subject,CreatedById,Description,Ownerid,Owner.isActive,LeadSource__c from Task where id in :triggernewMap.keySet()  and status =: STR_STATUS];
        
        for(Task tsk : lstTaskUpdate){
            useridsMap.put(tsk.id, tsk.ownerid);
            system.debug('Inside usermap tsk id' + tsk.id + 'ownerid' + tsk.ownerid);
        }   
        
        List<User> userList = [Select id, employee_office__c from user where Id IN :useridsMap.values()];           
        for(user usr : userList) {
            userEmpCode.put(usr.id, usr.employee_office__c);
            system.debug('Inside userEmpCode usr id' + usr.id + 'emp ofc' +usr.employee_office__c );
        }          
        
        
        //For each task if the subject starts with (Lead Score Threshold Met ") then take that subject and put in map with task as key
        //Jagan: This code can be customized to use task id as key instead of task
        Map<Id,Task> taskidMap = new Map<Id, Task>();
        for(Task t : lstTaskUpdate)
        {
            String subjectString = t.Subject;
            
            if(subjectString != null && (subjectString.startsWith(Label.Task_Subject)) ) 
            {        
                subjectSubstring= ((String)t.Subject).subString( Label.Task_Subject.length(), ((String)t.Subject).length()-1);
                taskMap.put(t,subjectSubstring);
                system.debug('taskMap is '+ taskMap);
                //Added as part of Request #5094 (September Release)
                taskWhoidMap.put(t.id,t.whoid);
                taskidMap.put(t.Id, t);
            }
            
        }     
        
        //Added as part of Request #5094 (September Release)
        //Export the details of all the contacts related to new tasks
        
        Map<id, Contact> taskContactMap = new Map<id, Contact>([Select id, name, Account.One_Code__c,accountid,ownerid,owner.Employee_Office__c,owner.isActive from Contact where id IN :taskWhoidMap.values()]);
        
        
        Set<Id> accountIdset = new set<id>(); 
        for(Contact c :taskContactMap.values()){
            accountIdset.add(c.AccountId);
            system.debug('accountIdset' + c.AccountId);
        }           
        
        ID twhoid; 
        System.debug('Test 1');
        // code has been added to meet request 9506
        set<id> ownerIDSet = new set<id>();
        for(task t:taskMap.keySet())
        {
            ownerIDSet.add(t.ownerid);
        }
        System.debug('Test 2');
        List<user> userlst  = [Select id,CurrencyIsoCode,DefaultCurrencyIsoCode from User where ID in: ownerIDSet];
        Map<ID,String> mapIDandISO = new Map<ID,String>();
        for(User u:userlst)
        {
            System.debug('Test 11');
            mapIDandISO.put(u.id,u.DefaultCurrencyIsoCode);
        }
        
        // 9506 ends 
        System.debug('Test 3');
        for(Task tk :  taskMap.keySet())
        {
            system.debug('taskMap.keySet() is '+ taskMap.keySet());
            //if(campaignMap.containsKey( taskMap.get(tk)))     {
            //Campaign cmp = campaignMap.get(taskMap.get(tk));
            //tk.whatId = cmp.id;                        
            updateTask.add(tk); 
            //Added as part of Request #5094 (September Release) to create Opportunity for Marketo task
            oppty = new Opportunity();
            
            twhoid = taskwhoidMap.get(tk.id);
            system.debug('twhoid is '+ twhoid);
            if(taskContactMap.containsKey(taskwhoidMap.get(tk.id)))
            {                            
                con = taskContactMap.get(taskwhoidMap.get(tk.id));
                System.debug('Test 22' + con);
            }                                              
            //As per Request#5166 (July Release 2015)
            oppty.name = tk.subject;
            if(con!=null)
                oppty.accountid = con.accountid;
            //request id:12638 , commenting step
            //oppty.Step__c = STR_STEP;
            oppty.StageName = STR_STAGE;
            oppty.CloseDate = system.today() + 180;
            oppty.Type = STR_TYPE;
            //oppty.CurrencyIsoCode = STR_CURRENCYCODE;
            oppty.LeadSource = tk.LeadSource__c;
            String oppOfc = userEmpCode.get(useridsMap.get(tk.id));    
            if(!checkOpportunityOfc(oppOfc)){
                oppOfc = 'MQL - Temp Office';
            }
            oppty.Opportunity_Office__c = oppOfc;  //userEmpCode.get(useridsMap.get(tk.id));            
            oppty.RFC_Opportunity__c = TRUE;
            
            if(tk.owner.isActive) {
                oppty.Ownerid = tk.ownerid;
                System.debug('Test 4');
                // This portion of code has been added to meet the requirement described in 9506
                
                if(mapIDandISO.containsKey(tk.OwnerId)){
                    //System.debug('CurrencyIsoCode assigned is: '+mapIDandISO.get(tk.OwnerId)+' the owner id is ' +tk.OwnerId);
                    oppty.CurrencyIsoCode= mapIDandISO.get(tk.OwnerId);
                }
            }
            
            else{
                System.debug('Test 5');
                oppty.CurrencyIsoCode= STR_CURRENCYCODE;
                oppty.Ownerid = Label.MercerForceUserid;
                
                // 9506 ended here
            }
            
            oppty.Description = tk.Description;
            oppty.Task_ID__c = tk.Id;
            oppty.Buyer__c = tk.WhoId;
            System.debug('**Test 33' + oppty);
            insertOppList.add(oppty);
            system.debug('insertOppList is '+ insertOppList);
            //As per Request#5166 (July Release 2015)
            taskContIDMap.put(tk.whoid, tk);                                                                                
        }
        if(updateTask!=null && updateTask.size()>0) {
            Database.update(updateTask);
        }
        set<ID> oppIDForTeam = new set<ID>();
        Map<id, id>  oppaccIDMap = new Map<id, id> ();
        Map<id, id> conaccIDMap = new Map<id,id>();
        
        //Added as part of Request #5094 (September Release) 
        FLAG_CHECK_OPP = true;           
        if(insertOppList!=null && insertOppList.size()>0) {
            try {
                List<Buying_Influence__c> BI_InsertList = new List<Buying_Influence__c>();
                Database.insert(insertOppList);
                system.debug('After insert '+ insertOppList);
                List<Task> tskUpdList = new List<Task>();
                for(opportunity opp : insertOppList)
                    system.debug('opp id is '+ opp.id);
                
                for(opportunity opp: insertOppList){
                    oppIDForTeam.add(opp.Id);
                    
                    Buying_Influence__c BI = new Buying_Influence__c();
                    BI.Contact__c = twhoid;
                    BI.Opportunity__c = opp.Id;                                                           
                    BI_InsertList.add(BI);
                    //As per Request#5166 (July Release 2015)
                    oppID.add(opp.ID);
                    
                    Task tt = taskidMap.get(opp.Task_ID__c);
                    tt.WhatId = opp.id;
                    tskUpdList.add(tt);                            
                    
                }
                //Database.insert(BI_InsertList);
                
                update tskUpdList;
                
                //As per Request#5166 (July Release 2015)
                Map<id, opportunity> oppMap = new Map<id, opportunity>([Select id, name, AccountID,ownerid from Opportunity where ID IN : oppID]);                      
                for(Opportunity opp : oppMap.values()) {
                    oppaccIDMap.put(opp.id,opp.Accountid);
                }                   
                
                for(contact cont : [Select id, name, AccountId from Contact where id =: twhoid and AccountID IN :oppaccIDMap.values()]){
                    conaccIDMap.put( cont.AccountID,cont.id);                            
                }                                                                    
                
                task tsk;
                for(Opportunity op : oppMap.values())
                {
                    ID accid = oppaccIDMap.get(op.id);
                    ID contID = conaccIDMap.get(accid);
                    if(taskContIDMap.containsKey(contID))
                    {                               
                        tsk = taskContIDMap.get(contID);
                    }
                    oppTaskMap.put(op, tsk);                        
                }                       
                // send_email(oppTaskMap, oppaccIDMap);
                
                // End of Request#5166 (July Release 2015)   
                // #Req 18452
                System.debug('***1'+oppIDForTeam);
                List<OpportunityTeamMember> lstTeamMember=[select id, userID,OpportunityId,TeamMemberRole from OpportunityTeamMember where OpportunityId in:oppIDForTeam]  ;
                System.debug('***2'+lstTeamMember);   
                if(!lstTeamMember.isEmpty()){
                    System.debug('***3'+lstTeamMember);
                    //ConstantsUtility.STR_ACTIVE ='Inactive';
                    AP15_SalesCreditTeamTriggerUtil.validateSalesCreditAfterInsert2(lstTeamMember);  
                }
                if(!lstTeamMember.isEmpty()){
                    for(OpportunityTeamMember opm:lstTeamMember){
                        if(opm.TeamMemberRole!=null && opm.TeamMemberRole == 'Opportunity Owner'){
                            opm.TeamMemberRole = ''; 
                        }
                    }
                    try{
                        update lstTeamMember;
                    }
                    Catch(Exception ex){}
                }
            }
            catch(Exception e) {
                for(Integer i =0; i< insertOppList.size(); i++) {                  
                    insertOppList[i].addError(EXCEPTION_STR+e.getMessage());
                } 
            }                         
            
        }
    } 
    
    /*
* @Description : As apart of Request#5079 (December 2014 release) this method  populates  date a user creates/updates an Activity (Task)  related  to the opportunity record . 
* @ Args       : triggernew
* @ Return     : void
*/  
    public static void relatedActivityLastModifiedbyUser(List<Task> Triggernew){
        
        String oppKeyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
        String  Userid = UserInfo.getUserId().substring(0,15);
        Set<id>  oppIds = new set<id>();
        List<opportunity> opplist = new List<opportunity>();
        
        for(Task t :Triggernew){
            String parentId = t.whatId;
            if(parentId<>null && parentId.startsWith(oppKeyPrefix) && Userid !=Label.MercerForceUserid ){
                oppIds.add(t.whatId);
            }
        }
        
        if(!oppIds.isEmpty()){
            opplist = [select id ,Date_Related_Activity_LastModifiedbyUser__c  from opportunity where id in :oppIds];
        }
        
        for(opportunity opp :opplist){
            opp.Date_Related_Activity_LastModifiedbyUser__c = system.Today();
            //opp.stagename = '';
        }
        try{
            if(!opplist .isEmpty()){
                
                database.update(opplist) ;
            }
        }
        catch (Exception e){
            for(Integer i=0;i<opplist.size();i++){
                Triggernew[i].addError(EXCEPTION_STR+e.getMessage());
            }
        }
        
    }
    
    /** Request:5590 :- Check if owner is inactive, if yes, assign to Account owner, if Account owner is inactive, assign to Marketo User**/
    public static void checkOwnerBeforeInsert(List<Task> Triggernew){
        Set<Id> owneridset = new set<Id>();
        Set<Id> conidset = new set<Id>();
        Map<Id,Contact> ContMap = new Map<Id,Contact>();
        Map<Id,User> userMap = new Map<Id,User>();
        
        for(Task t: triggernew){
            
            String whid = t.whoid;
            String wtid = t.whatid;
            
            if(whid <>null && whid.startsWith(Contact.sObjectType.getDescribe().getKeyPrefix())) {
                owneridset.add(t.ownerid);
                conidset.add(t.whoid); 
            }
        }
        
        if(conidset.size()>0) {
            
            ContMap = new Map<Id,Contact>([select id,ownerid,owner.isActive,Accountid,Account.Ownerid,Account.Owner.isActive from Contact where id in:conidset]);
            userMap = new Map<Id,User>([select id,isActive from user where id in:owneridset]);    
            
            for(Task t: triggernew){
                if(userMap.get(t.ownerid).isActive == FALSE && ContMap.containsKey(t.whoid) && ContMap.get(t.whoid).Account.Owner.isActive == TRUE)
                    t.ownerid  = ContMap.get(t.whoid).Account.OwnerId;
                
                else if(userMap.get(t.ownerid).isActive == FALSE && ContMap.containsKey(t.whoid) && ContMap.get(t.whoid).Account.Owner.isActive == FALSE)
                    t.ownerid  = Label.MarketoUserid;
            } 
        }
    }
    //Req #10616 
    public static void checkOwnerProfileBeforeDelete(List<Task>triggerold)
    {
        Id profileId=userinfo.getProfileId();
        String profileName=[Select id,Name from Profile where id =: profileId].name;
        profileName=profileName.trim();
        system.debug('>>>profileName'+profileName);
        List<User> list1 = [Select Id from User where name = 'MercerForce' OR name = 'Qualtrics' limit 2];
        if((profileName != 'System Administrator') && (profileName != 'Mercer System Admin Lite'))
            
        {
            for(Task t : triggerold)
            {
                System.debug('The value of t apoorva:'+t.CreatedById);
                if((t.Subject =='NPS – Follow Up') && (t.CreatedById == list1[0].id || t.CreatedById == list1[1].id))
                {
                    System.debug('The value of t is:'+t);
                    System.debug('The value of t apoorva:'+t.CreatedBy.name);
                    t.adderror('You do not have the level of access to delete this record');
                }
            }
        }
        // }
        
    }
    
    public static void create_Opportunity(Map<Id,Task> triggernewMap1,Map<Id,Task> triggeroldMap)
    {
        Id ownerUserId;
        Id recTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('RFC Task').getRecordTypeId();
        List<User> ownerUser = [select id, Email from User where Name ='CEC User'];
        if(!ownerUser.isEmpty()){
            ownerUserId = ownerUser.get(0).Id; 
        }
        // #Req-18183
        for(Task tt: triggernewMap1.values()){
            if((tt.recordTypeId == recTypeId) &&  (tt.LeadSource__c == 'Request Consultation') &&
                   (tt.status =='Closed - Create Opportunity') && (triggeroldmap.get(tt.id).status!= tt.status) && 
                   tt.WhoId!=null && (String.valueOf(tt.whoId).left(3)=='003') ){
                	
                       createOppRFC = true;
                }
        }
        
        FLAG_CHECK_TASK = true;
        FLAG_TEMP_OPP_OFFICE = true;
        Map<Id,Task> triggernewMap = new Map<Id,Task>();
        system.debug('triggeroldMap**'+triggeroldMap);
        system.debug('triggernewMap1**'+triggernewMap1);
        for(Task tt: triggernewMap1.values()){
            system.debug('tt.status***'+tt.status);
            system.debug('triggeroldMap.get(tt.id).status**'+triggeroldMap.get(tt.id).status);
            if(tt.status != triggeroldMap.get(tt.id).status){
                triggernewMap.put(tt.id, tt);
                system.debug('tt.if***'+triggernewMap);
            }
        }
        system.debug('*** INSIDE CREATE OPPTY' + triggernewMap);
        Map<Task,String> taskMap = new Map< Task, String>();
        Map<String,Campaign> campaignMap = new Map<String,Campaign> ();
        List<Task> updateTask = new List<Task>();
        
        List<opportunity> insertOppList = new List<opportunity>();
        Set<ID> contactID = new Set<ID>();
        Opportunity oppty;
        Contact con;
        String subjectSubstring = '';
        Map<ID,Task> taskContIDMap = new Map< ID, Task>();
        Map<Opportunity,Task> oppTaskMap = new Map< Opportunity, Task>();
        
        Map<ID, String> userEmpCode = new Map<ID, String>(); 
        Map<ID, ID> useridsMap =  new Map<ID, ID>();                                       
        
        // Retrieve all users for Marketo Sync Profile
        String Marketo_Profile = Label.Marketo_Sync_Profile_Name;
        Set<ID> userId = new Set<ID>();
        List<task> emailRFCTask = new List<task>(); 
        Map<ID,ID> taskWhoidMap = new Map<ID,ID>();
        Map<Id,User> userMap = new Map<Id,User>([Select id, name, ProfileId from User where IsActive = TRUE]);// where Profile.Name = :Marketo_Profile]);          
        system.debug('**--userMap' + userMap.keySet()); 
        system.debug('**--triggernewMap' + triggernewMap.keySet());
        // Check for tasks created by Marketo and fetch the Campaign name mentioned inside Subject
        //List<Task> lstTaskUpdate = [Select Id,Whatid,Whoid,Subject,CreatedById,Description,Ownerid,Owner.isActive,LeadSource__c, status from Task where id in :triggernewMap.keySet()  and createdById IN :userMap.keySet() and status = 'Closed - Create RFC Opportunity'];
        List<Task> lstTaskUpdate = [Select Id,Whatid,Whoid,Subject,CreatedById,Description,Ownerid,Owner.isActive,LeadSource__c, status from Task where id in :triggernewMap.keySet()  and status = 'Closed - Create Opportunity'];
        system.debug('**--lstTaskUpdate' + lstTaskUpdate); 
        for(Task tsk : lstTaskUpdate){
            system.debug('*****' + tsk.Subject);
            useridsMap.put(tsk.id, tsk.ownerid);
        }   
        
        List<User> userList = [Select id, employee_office__c from user where Id IN :useridsMap.values()]; 
        system.debug('**userList' + userList);
        for(user usr : userList) {
            userEmpCode.put(usr.id, usr.employee_office__c);
        }                     
        
        //For each task if the subject starts with (Lead Score Threshold Met ") then take that subject and put in map with task as key
        //This code can be customized to use task id as key instead of task
        Map<Id,Task> taskidMap = new Map<Id, Task>();
        for(Task t : lstTaskUpdate)
        {
            String subjectString = t.Subject;
            
            if(subjectString != null && (subjectString.startsWith(Label.Task_Subject)) && t.Status == 'Closed - Create Opportunity') 
            {        
                subjectSubstring= ((String)t.Subject).subString( Label.Task_Subject.length(), ((String)t.Subject).length()-1);
                taskMap.put(t,subjectSubstring);
                system.debug('taskMap is '+ taskMap);
                taskWhoidMap.put(t.id,t.whoid);
                taskidMap.put(t.Id, t);
            }
            
        }     
        
        //Export the details of all the contacts related to new tasks
        
        Map<id, Contact> taskContactMap = new Map<id, Contact>([Select id, name, Account.One_Code__c,accountid,ownerid,owner.Employee_Office__c,owner.isActive from Contact where id IN :taskWhoidMap.values()]);
        
        //To store campaign id's related to all tasks
        
        Set<Id> accountIdset = new set<id>(); 
        for(Contact c :taskContactMap.values()){
            accountIdset.add(c.AccountId);
        }
        
        ID twhoid; 
        System.debug('Test 1');
        set<id> ownerIDSet = new set<id>();
        for(task t:taskMap.keySet())
        {
            system.debug('**' + t.Subject);
            ownerIDSet.add(t.ownerid);
        }
        System.debug('Test 2');
        List<user> userlst  = [Select id,CurrencyIsoCode,DefaultCurrencyIsoCode from User where ID in: ownerIDSet];
        Map<ID,String> mapIDandISO = new Map<ID,String>();
        for(User u:userlst)
        {
            mapIDandISO.put(u.id,u.DefaultCurrencyIsoCode);
        }
        
        System.debug('Test 3' + taskMap);
        for(Task tk :  taskMap.keySet())
        {                      
            updateTask.add(tk); 
            //Added to create RFC Opportunity for Marketo task
            oppty = new Opportunity();
            FLAG_CHECK_OPP = true;
            twhoid = taskwhoidMap.get(tk.id);
            if(taskContactMap.containsKey(taskwhoidMap.get(tk.id)))
            {
                con = taskContactMap.get(taskwhoidMap.get(tk.id));
            }    
            system.debug('*** INSIDE CREATE' + tk);
            if(createOppRFC){
                oppty.name = 'Requested Consultation – '+ taskContactMap.get(tk.whoId).Account.One_Code__c;
                emailRFCTask.add(tk);
            }
            else{
                oppty.name = tk.subject;
            }
            oppty.accountid = con.accountid;
            //request id:12638,commenting step
            //oppty.Step__c = STR_STEP;
            oppty.StageName = STR_STAGE;
            oppty.CloseDate = system.today() + 180;
            oppty.Type = STR_TYPE;
            oppty.LeadSource = tk.LeadSource__c;
            String oppOfc = userEmpCode.get(useridsMap.get(tk.id));    
            if(!checkOpportunityOfc(oppOfc)){
                oppOfc = 'MQL - Temp Office';
            }
            oppty.Opportunity_Office__c = oppOfc;  //userEmpCode.get(useridsMap.get(tk.id));    
            oppty.RFC_Opportunity__c = TRUE;
            
            if(tk.owner.isActive) {
                oppty.Ownerid = tk.ownerid;
                
                if(mapIDandISO.containsKey(tk.OwnerId)){
                    oppty.CurrencyIsoCode= mapIDandISO.get(tk.OwnerId);
                }
            }
            
            else{
                System.debug('Test 5');
                oppty.CurrencyIsoCode= STR_CURRENCYCODE;
                oppty.Ownerid = Label.MercerForceUserid;
                
            }
            oppty.Description = tk.Description;
            oppty.Task_ID__c = tk.Id;
            oppty.Buyer__c = tk.WhoId;
            insertOppList.add(oppty);
            taskContIDMap.put(tk.whoid, tk);                                                                                
        }
        if(updateTask!=null && updateTask.size()>0) {
            Database.update(updateTask);
        }
        set<ID> oppID = new set<ID>();
        Map<id, id>  oppaccIDMap = new Map<id, id> ();
        Map<id, id> conaccIDMap = new Map<id,id>();
        List<Opportunity> validateUpdLst = new List<Opportunity>();
        system.debug('**beforeifOPP' + insertOppList);
        // Task_Send_Notification_email.FLAG_CHECK_OPP = true;
        if(insertOppList!=null && insertOppList.size()>0) {
            try {
                system.debug('**tryOPP' + insertOppList);
                List<Buying_Influence__c> BI_InsertList = new List<Buying_Influence__c>();
                Database.insert(insertOppList);
                
                
                
                system.debug('After insert '+ insertOppList);
                List<Task> tskUpdList = new List<Task>();
                for(opportunity opp : insertOppList)
                    system.debug('opp id is '+ opp.id);
                
                for(opportunity opp: insertOppList){
                    
                    Buying_Influence__c BI = new Buying_Influence__c();
                    BI.Contact__c = twhoid;
                    BI.Opportunity__c = opp.Id;                                                           
                    BI_InsertList.add(BI);
                    oppID.add(opp.ID);
                    Task tt = taskidMap.get(opp.Task_ID__c);
                    tt.WhatId = opp.id;
                    tskUpdList.add(tt);
                    
                }
                //Database.insert(BI_InsertList);
                
                update tskUpdList;
                
                Map<id, opportunity> oppMap = new Map<id, opportunity>([Select id, name, AccountID,ownerid from Opportunity where ID IN : oppID]);                      
                for(Opportunity opp : oppMap.values()) {
                    oppaccIDMap.put(opp.id,opp.Accountid);
                }                   
                
                for(contact cont : [Select id, name, AccountId from Contact where id =: twhoid and AccountID IN :oppaccIDMap.values()]){
                    conaccIDMap.put( cont.AccountID,cont.id);                            
                }                                                                    
                
                task tsk;
                for(Opportunity op : oppMap.values())
                {
                    ID accid = oppaccIDMap.get(op.id);
                    ID contID = conaccIDMap.get(accid);
                    if(taskContIDMap.containsKey(contID))
                    {                               
                        tsk = taskContIDMap.get(contID);
                    }
                    oppTaskMap.put(op, tsk);                        
                } 
                system.debug('***Gopal' +oppTaskMap + 'accmap' + oppaccIDMap);
                // #Req 18452
                System.debug('***1'+oppID);
                List<OpportunityTeamMember> lstTeamMember=[select id, userID,OpportunityId,TeamMemberRole from OpportunityTeamMember where OpportunityId in:oppId]  ;
                System.debug('***2'+lstTeamMember);   
                if(!lstTeamMember.isEmpty()){
                    System.debug('***3'+lstTeamMember);
                    //ConstantsUtility.STR_ACTIVE ='Inactive';
                    AP15_SalesCreditTeamTriggerUtil.validateSalesCreditAfterInsert2(lstTeamMember);  
                }
                if(!lstTeamMember.isEmpty()){
                    for(OpportunityTeamMember opm:lstTeamMember){
                        if(opm.TeamMemberRole!=null && opm.TeamMemberRole == 'Opportunity Owner'){
                            opm.TeamMemberRole = ''; 
                        }
                    }
                    try{
                        update lstTeamMember;
                    }
                    Catch(Exception ex){}
                }
                if(createOppRFC){
                    sendEmailToRFC(emailRFCTask);
                }
                
                /* for(opportunity opp: insertOppList){
opp.Temp_Office_Validate_Flag2__c = true;
system.debug('****Temp_Office_Validate_Flag' + opp); 
validateUpdLst.add(opp);
}  */                 
                
                //send_email(oppTaskMap, oppaccIDMap);
                system.debug('****validateUpdLst' + validateUpdLst);         
                
            }
            catch(Exception e) {
                for(Integer i =0; i< insertOppList.size(); i++) {                  
                    insertOppList[i].addError(EXCEPTION_STR+e.getMessage());
                } 
            }
            //update validateUpdLst;
            
        }
    } 
    public static void task_validateLead(List<Task> triggernewList)
    {    
        for(Task t: triggernewList){
            ///MQL Changes June Release
            String whoid = t.WhoId;
            if(whoid != null && whoid.startsWith('00Q') && t.Status == 'Closed - Create Opportunity'){
                t.WhoId.addError('Lead must be converted to a contact and associated with an account before changing the status');
            }
        }
    }
    
    public static boolean checkOpportunityOfc(String office)
    {
        boolean flag = false;
        
        Schema.DescribeFieldResult fieldResult =
            Opportunity.Opportunity_Office__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            //f.getLabel(), f.getValue();
            if(f.getValue() == office)
                flag = true;
        }       
        return flag;
    }  
    
    public static void populateOpportunityCountry(List<Task> triggernew)
    {
        List<Id> oppIDs = new List<Id>();
        for(Task t : triggernew)
        {
            System.debug('The value of t apoorva:'+t.CreatedById);
            if(t.WhatId != null && ((String)t.whatID).startsWith(Opportunity.sObjectType.getDescribe().getKeyPrefix()))
            {
                System.debug('The value of t is:'+t);
                oppIDs.add(t.WhatId);
            }
        }
        Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>([SELECT ID,Opportunity_Country__c FROM Opportunity where id in : oppIDs]);
        for(Task t : triggernew)
        {
            if(t.WhatId != null && ((String)t.whatID).startsWith(Opportunity.sObjectType.getDescribe().getKeyPrefix()))
            {
                System.debug('The value of t is:'+t);
                String oppCountry = oppMap.get(t.WhatId).Opportunity_Country__c;
                t.Opportunity_Country__c = oppCountry;
            }
        }
    }
    public static void sendEmailToRFC(List<Task> lTask )
    { 
        set<Id> oppId =new set<Id>();
        Set<Id> conId =new set<Id>();
        Set<Id> uId = new Set<Id>();
        for(Task tk:lTask ){
            oppId.add(tk.whatId);
            conId.add(tk.whoId);
            uId.add(tk.ownerId);
        }
        Map<Id,Opportunity> mapOpp =new Map<id,Opportunity>();
        Map<Id, Contact> mapCon=new map<id, Contact>();
        for(Opportunity opp:[select id, OwnerId, Owner.Email, Owner.Name,Name,AccountId, Account.Relationship_Manager__r.name, Account.Name, Account.BillingCountry,Account.BillingStreet, Account.Relationship_Manager__c from Opportunity where id in:oppId])
        {
            mapOpp.put(opp.id,opp);
        }
        for(Contact con:[select id ,name, title, phone,Email from Contact where id in:conId]){
            mapCon.put(con.id, Con);
        }
        //List<User> usrList = [select id, Email,Name from User where Id  in:uId];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        for(task t:lTask){
            
            OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress where displayName='US Growth Leadership' limit 1];
            EmailTemplate templateId = [Select id,body,HtmlValue,subject from EmailTemplate where name = 'RFC_EmailNotificationOpp'];
            String emailBody = templateId.HtmlValue;
            if(t.Description!=null)
                emailBody = emailBody.replace('{!Task.Description}',t.Description);
            else
                emailBody = emailBody.replace('{!Task.Description}','');         
            if(mapOpp.containskey(t.whatId)){
                emailBody = emailBody.replace('[Account_LINK]', '<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+mapOpp.get(t.whatId).AccountId+'">'+mapOpp.get(t.whatId).Account.name+'</a>' );
                emailBody = emailBody.replace('[InsertAccountAddress]', mapOpp.get(t.whatId).Account.BillingStreet+','+mapOpp.get(t.whatId).Account.BillingCountry);
                emailBody = emailBody.replace('[InsertRM]', mapOpp.get(t.whatId).Account.Relationship_Manager__r.Name);
                emailBody = emailBody.replace('{!Task.What}', '<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+mapOpp.get(t.whatId).id+'">'+mapOpp.get(t.whatId).name+'</a>' );
            }
            if(mapCon.containskey(t.whoId)){
                emailBody = emailBody.replace('{!Task.Who}', '<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+mapCon.get(t.whoId).id+'">'+mapCon.get(t.whoId).name+'</a>' );
                if(mapCon.get(t.whoId).Title!=null)
                    emailBody = emailBody.replace('[InsertTitle] ',mapCon.get(t.whoId).Title);
                else
                    emailBody = emailBody.replace('[InsertTitle] ','');
                if(mapCon.get(t.whoId).Phone!=null)
                    emailBody = emailBody.replace('[InsertPhone]',mapCon.get(t.whoId).Phone);
                else
                    emailBody = emailBody.replace('[InsertPhone]','');
                if(mapCon.get(t.whoId).Email!=null)
                    emailBody = emailBody.replace('[InsertEmail]',mapCon.get(t.whoId).Email);
                else
                    emailBody = emailBody.replace('[InsertEmail]','');
                
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setSubject(templateId.subject);
            mail.setTargetObjectId(t.OwnerId);
            mail.setSaveAsActivity(false);
            mail.setHtmlBody(emailBody); 
            mail.setOrgWideEmailAddressId(owa.id);
            mails.add(mail);
        }
        Messaging.sendEmail(mails);
        
        
        
    }
}