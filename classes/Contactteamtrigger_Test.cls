/*Test Class for Contact Team Trigger*/
@IsTest(SeeAllData=False)
public class Contactteamtrigger_Test {       //changed classname
    private static Integer month = System.Today().month();
    private static Integer year = System.Today().year();
    
    
    static testmethod void ContactTriggerClassTest(){
        TriggerSettings__c TrgSetting= new TriggerSettings__c();
        TrgSetting.name='Project Trigger';
        TrgSetting.Object_API_Name__c='Revenue_Connectivity__c';
        TrgSetting.Trigger_Disabled__c=false;
        insert TrgSetting;
        TriggerSettings__c TrgSetting1= new TriggerSettings__c();
        TrgSetting1.name='Contact Team Trigger';
        TrgSetting1.Object_API_Name__c='Contact_Team_Member__c';
        TrgSetting1.Trigger_Disabled__c=false;
        insert TrgSetting1;
        TriggerSettings__c TrgSetting2= new TriggerSettings__c();
        TrgSetting2.name='Account Trigger';
        TrgSetting2.Object_API_Name__c='Account';
        TrgSetting2.Trigger_Disabled__c=false;
        insert TrgSetting2;
        Mercer_TestData mdata = new Mercer_TestData();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        id uid = userinfo.getUserId();//Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;  
        
        Account accrec =  mdata.buildAccount();
        //Contact conrec =  mdata.buildContact();
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        //testContact.OwnerId = conrec.Id;
        
        insert testContact;
        
        Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
        User contactMember3;
        User contactMember1;
        //Contact_Team_Member__c testContactteam = new Contact_Team_Member__c();
        System.runAs(user1){
            User contactMember = new User();
            contactMember = Mercer_TestData.createUser(mercerStandardUserProfile, 'tu', 'tm', contactMember);
            
            
            testContactTeamMember.Contact__c = testContact.Id;
            testContactTeamMember.ContactTeamMember__c = contactMember.ID;
            insert testContactTeamMember;
            //conTMember = testContactTeamMember.ContactTeamMember__c;
            contactMember1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'tu2', 'tm2', contactMember);
            testContactTeamMember.ContactTeamMember__c = contactMember1.ID;
            contactMember3 = Mercer_TestData.createUser(mercerStandardUserProfile, 'tu3', 'tm3', contactMember);
        }
        update testContactTeamMember;
        
        
        
        List<Contact_Team_Member__c> rvList=new List<Contact_Team_Member__c>();
        rvList.add(testContactTeamMember);
        testContactTeamMember.ContactTeamMember__c = contactMember3.ID;
        update testContactTeamMember;
        
        Map<id,Contact_Team_Member__c> ContactteamMap =new Map<id,Contact_Team_Member__c>([select id,ContactTeamMember__c,Contact__c,Country__c,CurrencyIsoCode,Name,People_Directory__c,Profile__c,Role__c,User_ID__c,Work_Email__c,Work_Phone__c from Contact_Team_Member__c where Id =:testContactTeamMember.id]);
        
        ContactTeamTriggerHandler pth=new ContactTeamTriggerHandler();
        pth.AfterInsert(rvList, ContactteamMap);
        pth.AfterUndelete(ContactteamMap);
        pth.BeforeDelete(ContactteamMap);
        pth.AfterUpdate(ContactteamMap, ContactteamMap);
        pth.BeforeInsert(rvList);
        pth.BeforeUpdate(ContactteamMap, ContactteamMap);
        delete testContactTeamMember;
    }
    /*Colleague__c testColleague1 = new Colleague__c();
testColleague1.Name = 'TestColleague';
testColleague1.EMPLID__c = '12345678903';
testColleague1.LOB__c = '1234';
testColleague1.Location__c = mogd.Id;
testColleague1.Last_Name__c = 'TestLastName';
testColleague1.Empl_Status__c = 'Active';
insert testColleague1;

Opportunity opprec = new Opportunity();
opprec.Name = 'TestOppty';
opprec.Type = 'New Client';
opprec.AccountId = accrec.Id;
opprec.Step__c = 'Pending Chargeable Code';
opprec.StageName = 'Pending Project';
opprec.CloseDate = date.Today();
opprec.CurrencyIsoCode = 'ALL';
opprec.Opportunity_Office__c = 'London - Queens';
opprec.Amount = 100;
opprec.Buyer__c=testContact.id;
opprec.Close_Stage_Reason__c ='Other';

//Test.startTest();   
insert opprec;




String OppName= opprec.Name;
String OppID=opprec.Id;

Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];

Product2 pro = new Product2();
pro.Name = 'TestPro';
pro.Family = 'RRF';
pro.IsActive = True;
insert pro;

PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];

OpportunityLineItem opptylineItem = new OpportunityLineItem();
opptylineItem.OpportunityId = opprec.Id;
opptylineItem.PricebookEntryId = pbEntry.Id;
opptyLineItem.Revenue_End_Date__c = date.Today()+3;
opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
opptyLineItem.UnitPrice = 1.00;
opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
opptyLineItem.Year2Revenue_edit__c = null;
opptyLineItem.Year3Revenue_edit__c = null;
insert opptylineItem;
Revenue_Connectivity__c testProject = new Revenue_Connectivity__c();
testProject.CEP_Code__c = 'WEB002-CEP-CODE';
testProject.Charge_Basis__c = 'S';
testProject.Project_Amount__c = 20000.00;
testProject.Colleague__c = testColleague1.Id;
testProject.Opportunity__c = opprec.Id;
testProject.Opportunity_Product_Id__c = opptylineItem.id;
testProject.Project_End_Date__c = date.newInstance(year, month, 1);
testProject.Project_LOB__c = 'Benefits Admin';
testProject.Project_Manager__c = '866608';
testProject.Project_Name__c = 'Absence Management - PROJ01';
testProject.Project_Solution__c = '3051';
testProject.Project_Start_Date__c = date.newInstance(year, month, 10);
testProject.Project_Status__c = 'Active';
testProject.Project_Type__c = 'Project';
testProject.WebCAS_Project_ID__c = 'WEB001';

insert testProject;


opprec.Buyer__c=testContact.id;
opprec.Step__c='Pending Chargeable Code';
opprec.Close_Stage_Reason__c ='Other';


testProject.Charge_Basis__c = 'T';
testProject.Project_Status__c = 'InActive';
update testProject;
*/
    @isTest
    public static void method1(){
        Mercer_TestData mdata = new Mercer_TestData();
        Account accrec =  mdata.buildAccount();
        Account accrec1 =  mdata.buildAccount();
        Contact testContact1 = new Contact();
        testContact1.Salutation = 'Fr.';
        testContact1.FirstName = 'TestFirstName1';
        testContact1.LastName = 'TestLastName1';
        testContact1.Job_Function__c = 'TestJob';
        testContact1.Title = 'TestTitle';
        testContact1.MailingCity  = 'TestCity';
        // testContact.BillingCountry = 'TestCountry';
        testContact1.MailingState = 'TestState'; 
        testContact1.MailingStreet = 'TestStreet'; 
        testContact1.MailingPostalCode = 'TestPostalCode'; 
        testContact1.Phone = '9999999999';
        testContact1.Email = 'abc1@xyz.com';
        testContact1.MobilePhone = '9999999999';
        testContact1.AccountId = accrec.Id;
        //Contact conrec =  mdata.buildContact();
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        // testContact.BillingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        //testContact.OwnerId = conrec.Id;
        
        //insert testContact;
        List<contact> lCon = new List<Contact>();
        lCon.add(testContact);
        lCon.add(testContact1);
        insert lCon;
        AP19_ContactTriggerUtil.postChatter(lCon);
        //lCon.get(0).MasterRecordId = lCon.get(1).Id; 
        User us =[Select id from User where id=:UserInfo.getUserId()];
        USer u;
        system.runAs(us){
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            User testUser = new User(LastName = 'LIVESTON',
                                     FirstName='JASON',
                                     Alias = 'jliv',
                                     Email = 'jason.liveston@asdf.com',
                                     Username = 'jason.test@test.com',
                                     ProfileId = profileId.id,
                                     TimeZoneSidKey = 'GMT',
                                     LanguageLocaleKey = 'en_US',
                                     EmailEncodingKey = 'UTF-8',
                                     LocaleSidKey = 'en_US',
                                     //Employee_office__c='Mercer US Mercer Services - US03'
                                     Employee_office__c='Aarhus - Axel'     
                                    );
            insert testUser;
            u= testUser;
        }
        lCon.get(0).OwnerId =u.Id;
        update lCon;
        lCon.get(0).AccountId =  accrec1.Id;
        update lCon;
        Database.MergeResult results = Database.merge(lCon.get(0), lCon.get(1), false);
        
        AP19_ContactTriggerUtil.processContactAfterDelete(lCon);
    }
}