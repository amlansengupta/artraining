/*Purpose: Test class to provide test coverage for AP101_Scopeit_swap_employees class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/1/2014  Created test class to provide test coverage for AP101_Scopeit_swap_employees class
 
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP101_Scopeit_swap_employees_Test {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    
   /*
    * @Description : Test class for swapping employees related to Scopeit Project   
    */
    static testMethod void myUnitTest() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
           
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            
              Test.startTest();  
            Opportunity testOppty = mtdata.buildOpportunity();
            
            ID  opptyId  = testOppty.id;
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
               
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
              
                Test.stopTest();     
                mtscopedata.insertCurrentExchangeRate();      
                ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
                ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                  
                PageReference pageRef = Page.Mercer_scopeit_swap_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('id', testScopeitProj.id);
                ApexPages.StandardController stdController = new ApexPages.StandardController(testScopeitProj);
          
                
                AP101_Scopeit_swap_employees controller = new AP101_Scopeit_swap_employees();
                PageReference pageReference1 = controller.saveEmployees();          
                PageReference pageReference2 =  controller.cancel();
                          
            
        }
                       
 
    }
    
    /*
     * @Description : Test class for swapping employees related to Scope Modeling   
    */
    static testMethod void myUnitTest1() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
            
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            Test.startTest(); 
            Opportunity testOppty = mtdata.buildOpportunity();
            
            ID  opptyId  = testOppty.id;
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
               
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
             Test.stopTest(); 
                 
                mtscopedata.insertCurrentExchangeRate();            
                ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
                ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                
                PageReference pageRef = Page.Mercer_scopeit_swap_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('id', testScopeitProj.id);
               
                AP101_Scopeit_swap_employees controller = new AP101_Scopeit_swap_employees();
                controller.objectType =  testScopeitProj.name;
                AP101_Scopeit_swap_employees controller1 = new AP101_Scopeit_swap_employees();
                PageReference pageReference1 = controller.saveEmployees();          
                PageReference pageReference2 =  controller.cancel(); 
                         
               
          }
    }
    
    /*
    * @Description : Test class for swapping employees related to Scopeit Project   
    */
    static testMethod void aP99_myUnitTest2() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
           
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
         
             
                mtscopedata.insertCurrentExchangeRate(); 
                Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
                Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
                Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
             
                String objname = testScopeModelProj.name;
                PageReference pageRef = Page.Mercer_scopeit_swap_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('id', testScopeModelProj.id);
                system.currentPageReference().getParameters().put('object', objname);
                
             Test.startTest();
               
                AP101_Scopeit_swap_employees controller = new AP101_Scopeit_swap_employees();
                controller.objectType =  testScopeModelProj.name;
                AP101_Scopeit_swap_employees controller1 = new AP101_Scopeit_swap_employees();
                      
                PageReference pageReference1 = controller.saveEmployees();          
                PageReference pageReference2 =  controller.cancel();   
               
                  
            Test.stopTest();
        }
    }
    
}