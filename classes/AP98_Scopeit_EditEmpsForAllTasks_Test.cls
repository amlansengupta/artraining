/*Purpose: Test class to provide test coverage for AP98_Scopeit_EditEmpsForAllTasks class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/1/2014  Created test class to provide test coverage for AP98_Scopeit_EditEmpsForAllTasks class
 
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP98_Scopeit_EditEmpsForAllTasks_Test {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
      /*
      * Test Method  to Edit Employees for all respetive Tasks related to Scopeit Project
      */
    static testMethod void myUnitTest() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            
            Test.startTest();   
            Opportunity testOppty = mtdata.buildOpportunity();
            ID  opptyId  = testOppty.id;
            
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID  pbEntryId = pbEntry.Id;
            
            Test.stopTest();   
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
         
                ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
                ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee =  mtscopedata.buildScopeitEmployee(employeeBillrate);
                
    
                PageReference pageRef = Page.Mercer_Scopeit_EditEmpsforAllTasks;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('id', testScopeitProj.id);
                AP98_Mercer_Scopeit_EditEmpsForAllTasks controller = new AP98_Mercer_Scopeit_EditEmpsForAllTasks();
                controller.save();
                controller.cancel(); 
              
           
        }
   }

   /*
    * Test Method  to Edit Employees for all respetive Tasks related to Scope Modeling Project
    */
   static testMethod void myUnitTest1() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
               
         
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
              
            Test.startTest();
            
                Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
                Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
                Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
                            
            
                 PageReference pageRef = Page.Mercer_Scopeit_EditEmpsforAllTasks;  
                 Test.setCurrentPage(pageRef);
                 system.currentPageReference().getParameters().put('id', testScopeModelProj.id);
                 system.currentPageReference().getParameters().put('object','ScopeIt_Project__c');
                 AP98_Mercer_Scopeit_EditEmpsForAllTasks controller = new AP98_Mercer_Scopeit_EditEmpsForAllTasks();
                 controller.save();
                 controller.cancel();   
           Test.stopTest();
        }
   }
   

}