/* Purpose:This schedulable class is used to schedule AP76_OpportunityReminderBatchable batchable class.
===================================================================================================================================
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          12/11/2013  As a part of Request# 3587(January Release) created Schedulable Class to schedule AP76_OpportunityReminderBatchable batchable class.
 ======================================================================================================================================*/

global class AP77_OpportunityReminderShedulable implements schedulable{

global AP77_OpportunityReminderShedulable (){}

global void execute(SchedulableContext SC)    
 {  
     String qry='';
     Date seventhDay =  system.today().addDays(7) ;
     String closeDt = String.valueOf(seventhDay );
     
     //String variable to fetch the Opportunity id, their names and close dates closing in next week
     //String  qry = 'Select id,Name, CloseDate, Accountid from Opportunity where  CloseDate <> null and  CloseDate = ' + closeDt ;
      if( Label.AP77_batchquery != null)
     {
         qry = Label.AP77_batchquery+ closeDt;                      
         database.executeBatch(new AP76_OpportunityReminderBatchable(qry), 2000);
     }      
                                     
  }   
}