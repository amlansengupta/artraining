global class Batch_UpdateOpportunityFields implements Database.Batchable<sObject>{
 global String query;
  global Batch_UpdateOpportunityFields(String q){
      query = q;
    }
 global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {
    List<Account> accList4 = new List<Account>();
         List<Id> accIdList4 = new List<Id>();
         List<User> ownerDetList4= new List<User>();
         List<Id> usrIdList4 = new List<Id>();
         List<Colleague__c> clgDetList= new List<Colleague__c>();
          List<Id> clgIdList5 = new List<Id>();
        for(Opportunity opp :scope){  
            usrIdList4.add(opp.OwnerId);
            accIdList4.add(opp.AccountId);
            clgIdList5.add(opp.Sibling_Contact_Name__c);
        } 

         List<Opportunity> oppUpdateList = new List<Opportunity>();
         for(Opportunity opp4:scope){
            
             system.debug('--->Inside Loop MP');
             opp4.Account_Region__c = opp4.Account.Account_Region__c;
             opp4.Account_Name_LDASH__c = opp4.Account.Name;
             //system.debug('--->region'+oppAccMap4.get(opp4.AccountId).Account_Region__c);
            // system.debug('--->ldash'+oppAccMap4.get(opp4.AccountId).Name);
             //system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
            //system.debug('--->sibling2'+opp4.Sibling_Contact_Name__c);
             if(opp4.Sibling_Contact_Name__c != null )
             {
             opp4.Sibling_Contact_Office_Name__c = opp4.Sibling_Contact_Name__r.Location_Descr__c;
            // system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
             }
             else{
             opp4.Sibling_Contact_Office_Name__c = '';
             }
            
            opp4.Owner_Profile_Name__c = opp4.Owner.Profile.Name;
             opp4.Opp_Owner_Operating_Company__c = opp4.Owner.Level_1_Descr__c;
             
             oppUpdateList.add(opp4);
         }
         system.debug('-->updateList'+oppUpdateList);
         update oppUpdateList;
    }
       global void finish(Database.BatchableContext BC)
    {

    }
    
}