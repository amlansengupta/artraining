/*Purpose: Test class to provide test coverage for AP102_OppProd_Del_Override class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan   05/02/2014  Created test class 
 
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest (seeAllData = true)
private class AP102_OppProd_Del_Override_test{

    /*
     * @Description : Test method to display warning message to user to inform that deleting opportunity product will also delete scope it tasks and employees
     * @ Args       : Null
     * @ Return     : void
     */
    Private Static TestMethod Void AP102_OppProd_Del_Override_testMethod(){
        
       // Mercer_TestData testData = new Mercer_TestData();
        //Account testAccount = testData.buildAccount();   
       // testAccount.one_code__c = '013331' ;        
        //update testAccount;
          
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();  
        //String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();    
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        
        system.runAs(User1){
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        //Test class fix, as starttest() repositioned at 45
        Test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        //RequestId:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601; Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        //Updated as part of 5166(july 2015)
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
         insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        //pro.Classification__c = 'Consulting Solution Area';
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.Project_Linked__c = true;
        insert opptylineItem;
        
         
            ApexPages.StandardController con = new ApexPages.StandardController(opptylineItem);
            AP102_OppProd_Del_Override test1 = new AP102_OppProd_Del_Override(con);
            test1.deleteProject();
            test1.cancel();
            Test.stopTest();
        }
    }
    /*
     * @Description : Test method to display warning message to user to inform that deleting opportunity product will also delete scope it tasks and employees
     * @ Args       : Null
     * @ Return     : void
     */
     Private Static TestMethod Void AP102_OppProd_Del_Override_testMethod1(){
        
       // Mercer_TestData testData = new Mercer_TestData();
        //Account testAccount = testData.buildAccount();   
       // testAccount.one_code__c = '013331' ;        
        //update testAccount;
          
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();  
        //String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();    
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        
        system.runAs(User1){
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        //Test class fix, as starttest() repositioned at 45
        Test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        //RequestId:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601; Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Selected';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        //Updated as part of 5166(july 2015)
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
         insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        //pro.Classification__c = 'Consulting Solution Area';
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.Project_Linked__c = true;
        insert opptylineItem;
         OpportunityLineItem opptylineItem1 = new OpportunityLineItem();
        opptylineItem1.OpportunityId = testOppty.Id;
        opptylineItem1.PricebookEntryId = pbEntry.Id;
        opptyLineItem1.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem1.Revenue_Start_Date__c = date.Today(); 
        opptylineItem1.UnitPrice = 100.00;
        opptyLineItem1.Project_Linked__c = true;
        insert opptylineItem1;
            
        
         
            ApexPages.StandardController con = new ApexPages.StandardController(opptylineItem);
            AP102_OppProd_Del_Override test1 = new AP102_OppProd_Del_Override(con);
            test1.deleteProject();
            test1.cancel();
            Test.stopTest();
        }
    }
     Private Static TestMethod Void AP102_OppProd_Del_Override_testMethod2(){
        
       // Mercer_TestData testData = new Mercer_TestData();
        //Account testAccount = testData.buildAccount();   
       // testAccount.one_code__c = '013331' ;        
        //update testAccount;
          
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();  
        //String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();    
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        
        system.runAs(User1){
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        //Test class fix, as starttest() repositioned at 45
        Test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        //RequestId:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601; Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Selected';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        //Updated as part of 5166(july 2015)
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
         insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        //pro.Classification__c = 'Consulting Solution Area';
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.Project_Linked__c = true;
        insert opptylineItem;
         OpportunityLineItem opptylineItem1 = new OpportunityLineItem();
        opptylineItem1.OpportunityId = testOppty.Id;
        opptylineItem1.PricebookEntryId = pbEntry.Id;
        opptyLineItem1.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem1.Revenue_Start_Date__c = date.Today(); 
        opptylineItem1.UnitPrice = 100.00;
        opptyLineItem1.Project_Linked__c = true;
        insert opptylineItem1;
            
        
         
            ApexPages.StandardController con = new ApexPages.StandardController(opptylineItem);
            AP102_OppProd_Del_Override test1 = new AP102_OppProd_Del_Override(con);
            test1.deleteProject();
            test1.cancel();
            Test.stopTest();
        }
    }   
    /* Private Static TestMethod Void AP102_OppProd_Del_Override_testMethod2(){
        
        //Mercer_TestData testData = new Mercer_TestData();
        //Account testAccount = testData.buildAccount();   
        //testAccount.one_code__c = '013331' ;        
        //update testAccount;   
          User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();   
       // String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();   
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Single Sales Objective(s)';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        //Updated as part of 5166(july 2015)
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
         insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        OpportunityLineItem opptylineItem2 = new OpportunityLineItem();
        opptylineItem2.OpportunityId = testOppty.Id;
        opptylineItem2.PricebookEntryId = pbEntry.Id;
        opptylineItem2.Revenue_End_Date__c = date.Today()+750;
        opptylineItem2.Revenue_Start_Date__c = date.Today(); 
        opptylineItem2.UnitPrice = 100.00;
        insert opptylineItem2;

        scopeit_project__c spc = new scopeit_project__c();
        spc.Name = 'Test';
        spc.OpportunityProductLineItem_Id__c = opptylineItem2.Id;
        spc.Opportunity__c = testOppty.Id;
        //scp.EmpName__c
        insert spc;
      
            ApexPages.StandardController con2 = new ApexPages.StandardController(opptylineItem2);
            AP102_OppProd_Del_Override test2 = new AP102_OppProd_Del_Override(con2);
            Test.startTest();
            test2.deleteProject();            
            Test.stopTest();       
          }
    }*/
}