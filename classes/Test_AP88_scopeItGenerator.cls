/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/24/2014  This class acts as Test class for AP88_scopeTempGen .
                                
 
============================================================================================================================================== 
*/
@isTest(seeAllData = false)
public class Test_AP88_scopeItGenerator{

  private static Mercer_TestData mtdata;
  private static Mercer_ScopeItTestData mtscopedata;  
  
  @testSetup static void setup(){        
    mtdata = new Mercer_TestData(); 
    mtscopedata = new Mercer_ScopeItTestData();
    Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
     List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
       // setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        setting.StrValue__c = 'International:5000;North America:10000';         
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
         /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */
        Insert listofvalues;

     
   
          String LOB = 'Retirement';
          String billType = 'Billable';
          Product2 testProduct = mtscopedata.buildProduct(LOB);          
          ID prodId = testProduct.id;
         
         Opportunity testOppty_123 = mtdata.buildOpportunity();

         
         ID  opptyId  = testOppty_123.id;
         Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
         
           
       PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
       
   
       
       ID  pbEntryId = pbEntry.Id;
       
       OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
       ID oliId= opptylineItem.id;
       String oliCurr = opptylineItem.CurrencyIsoCode;
       Double oliUnitPrice = opptylineItem.unitprice;
       String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
       
        Test.startTest();   
       list<id> sList= new list<id>();          
       ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
       sList.add(testScopeitProj.id);
       
      ScopeIt_Task__c testScopeitTask=mtscopedata.buildScopeitTask();               
      ScopeIt_Employee__c testScopeitEmp=mtscopedata.buildScopeitEmployee(employeeBillrate);
       AP87_createTempScope.createTscope(sList);
          Temp_ScopeIt_Project__c tsp= new Temp_ScopeIt_Project__c ();
          tsp.name='Template123test';
          tsp.Product_Id__c=testProduct.id;
          insert tsp;
       
         /*Temp_ScopeIt_Task__c tst = new Temp_ScopeIt_Task__c ();
         tst.Temp_ScopeIt_Project__c=tsp.id;
         insert tst;
       
        Temp_ScopeIt_Employee__c tse = new Temp_ScopeIt_Employee__c();
        tse.Temp_ScopeIt_Task__c=tst.id;
         insert tse;*/
           Test.stopTest();
       
  }    
  /*
     * @Description : Test method to provide data coverage to AP88_scopeItGenerator class
     * @ Args       : Null
     * @ Return     : void
     */
  /*Commenting out the below code due to 101 SOQL on line #135
  
  private static testMethod void scopeTempGen_test(){

      ScopeIt_Project__c testScopeitProj1 = [Select id, name from ScopeIt_Project__c where name =  'testScopeitProj' LIMIT 1];
       Opportunity testOppty1 = [Select id, name from Opportunity where name = 'test oppty' LIMIT 1];
       Temp_ScopeIt_Project__c tsp1 = [Select id, name from Temp_ScopeIt_Project__c where name =  'Template123test' LIMIT 1];
       
      ApexPages.CurrentPage().getparameters().put('id',testScopeitProj1.id);
      ApexPages.CurrentPage().getparameters().put('oppid',testOppty1.id);
      List<ScopeIt_Project__c> tspt= new  List<ScopeIt_Project__c> ();
      tspt.add(testScopeitProj1);
      
      ApexPages.StandardSetController controller = new ApexPages.StandardSetController(tspt);
      Test.startTest(); 
      AP88_scopeItGenerator t = new AP88_scopeItGenerator(controller);
      Test.stopTest();
     PageReference pageRef = Page.Mercer_Importfromtemplate;  
     pageRef.getParameters().put('Id', testScopeitProj1.id);
      Test.setCurrentPage(pageRef);
     t.scpobj=tsp1.id;
      t.PR1='test';
      t.PR2='test';
      t.PR3='test';
      t.PR4='test';
      t.PR5='test';
      
      t.runSearch();
      t.clearSearch();
       
      t.PR1='';
      t.PR2='';
      t.PR3='';
      t.PR4='';
      t.PR5='';
      t.runSearch();
      t.cancel();
      t.insertTemplate();
      t.getItems();
      t.getLob();
       ApexPages.CurrentPage().getparameters().put('oppid','');
        t.cancel();
     
  
  }*/
  
    
    
    
}