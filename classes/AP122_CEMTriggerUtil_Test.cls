/*Purpose: Test class to provide test coverage for CEM trigger.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   11/13/2014    Created test class to provide test coverage for  AP122_CEMTriggerUtil  class
 
============================================================================================================================================== 
*/
@istest
private class AP122_CEMTriggerUtil_Test{

 /*
    * @Description : Testmethod to test task creation for cem record
 */ 
static testmethod void testTaskcreation(){
String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
    User user1 = new User();
    user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
    Mercer_TestData mtdata = new Mercer_TestData();
    System.runAs(user1) {
    Colleague__c collg = mtdata.buildColleague();
    Account acc = mtdata.buildAccount();
    Contact con =  mtdata.buildcontact();
    Client_Engagement_Measurement_Interviews__c  cem = mtdata.buildCEM();
    cem.Account__c = acc.id;
    cem.Status__c = 'Completed';
    cem.Overall_rating__c = '5';
    cem.Reviewer__c =collg.id;
    cem.CEM_Interviewer_2__c = collg.id;
    cem.Client_Contact_Reviewer_1__c = con.id;
    insert(cem);
    
    List<Task> tasklist = [select id,subject from Task where whatid=:acc.id limit 1];
    System.assertEquals(tasklist.size(),1);
    }
}

}