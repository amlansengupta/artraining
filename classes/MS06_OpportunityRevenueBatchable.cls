/*
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Arijit Roy     04/01/2013           Batch class for updating Account Status    
2.0         Jagan          05/22/14             Updated class to check if the status is not already same.
============================================================================================================================================== 
*/

global class MS06_OpportunityRevenueBatchable implements Database.Batchable<sObject>, Database.Stateful{
    
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
        'Recent Win'                  => 'Recent Win'   
    };
    
    global static List<Schema.PicklistEntry> statusPickVal = Account.MercerForce_Account_Status__c.getDescribe().getPicklistValues();
        
    global static String query = 'Select Id,Total_CY_Revenue__c ,Total_PY_Revenue__c,  MercerForce_Account_Status__c From Account where MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keySet());
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global Map<String, String> accountStatusMap = new Map<String, String>();
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
          if(Test.isRunningTest())
        {
            String AccName= 'TestAccountName123' + String.valueOf(Date.Today());  
            
            query = 'Select Id,Total_CY_Revenue__c ,Total_PY_Revenue__c, MercerForce_Account_Status__c  From Account where MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()); 
        }
        System.debug('\n Constructed query String :'+query);
        
        return Database.getQueryLocator(query); 
    }
    
     /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<Id, Account> accForUpdateMap = new Map<Id, Account>();
            
        for(Account acc : (List<Account>)scope)
        {
            Double totRev = 0.0;
            Double cyRev = 0.0;
            Double pyRev = 0.0;
            if(acc.Total_CY_Revenue__c <> null)
                cyRev = acc.Total_CY_Revenue__c;
            if(acc.Total_PY_Revenue__c <> null)
                pyRev = acc.Total_PY_Revenue__c;
                
            totRev = cyRev + pyRev ;
            
            if(totRev >= 100000.00 && acc.MercerForce_Account_Status__c <> 'High Revenue'){
                acc.MercerForce_Account_Status__c = 'High Revenue';
                accForUpdateMap.put(acc.Id, acc);
            }
            else if((((acc.Total_CY_Revenue__c<>null && acc.Total_CY_Revenue__c>0) || (acc.Total_PY_Revenue__c<>null && acc.Total_PY_Revenue__c>0 )) && totRev < 100000.00 ) && acc.MercerForce_Account_Status__c <> 'Low Revenue'){
                acc.MercerForce_Account_Status__c = 'Low Revenue';
                accForUpdateMap.put(acc.Id, acc);
            }        
            
             
        }
        
        if(!accForUpdateMap.isEmpty())
        {
            for(Database.SaveResult result : Database.update(accForUpdateMap.values(), false))
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }
            }
        }       
    }
    
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
       
        // TO DO: Error logging code logic to be added
        try
        {
          Database.insert(errorLogs, false);
        }
        catch(DMLException dme)
        {
          system.debug('\n exception has occured');
        }finally
        {
           MercerAccountStatusBatchHelper.callNextBatch('Active Marketing Contact');   
        }
         
    }

}