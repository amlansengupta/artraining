@istest
public class HelpMessages_Test {
    
    static testMethod void helpMessagesMethod1() {
        FCST_Help_Message__c h = new FCST_Help_Message__c();
        h.CAssesmentMessage__c ='CAssesmentMessage';
        h.CBI_Help_Message__c ='CBI_Help_Message';
        h.CLandscapeMessage__c ='CBI_Help_Message';
        h.GoalsActionsMessage__c ='GoalsActionsMessage';
        h.LOBMessage__c = 'LobMessage';
        h.GU_RollUpMessages__c ='GURollupMessage';
        h.MF_RoollUpMessage__c ='MF_RoollUpMessage';
        h.SAnalysysMessage__c = 'SAnalysysMessage';
        h.CRelationshipMessage__c ='CRelationshipMessage';
        insert h;    
        
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
        hm.CAssesmentMessageHelpMessagesave();
        hm.CBIHelpMessagesave();
        hm.ClandscapeHelpMessagesave();
        hm.LOBHelpMessagesave();
        hm.MFRoollUpHelpMessagesave();
        hm.GURollUpHelpMessagesave();
        hm.GoalsActionsHelpMessagesave();
        hm.CRelationshipHelpMessagesave();
        hm.SAnalysysHelpMessagesave();
        
    }
  static testMethod void helpMessagesMethod2() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.CAssesmentMessageHelpMessagesave();
    } 
    static testMethod void helpMessagesMethod3() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.CBIHelpMessagesave();
    } 
    static testMethod void helpMessagesMethod4() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.ClandscapeHelpMessagesave();
    } 
    static testMethod void helpMessagesMethod5() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.LOBHelpMessagesave();
    } 
 static testMethod void helpMessagesMethod6() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.MFRoollUpHelpMessagesave();
    } 
    static testMethod void helpMessagesMethod7() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.GURollUpHelpMessagesave();
    } 
     static testMethod void helpMessagesMethod8() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.SAnalysysHelpMessagesave();
    }
     static testMethod void helpMessagesMethod9() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.CRelationshipHelpMessagesave();
    } 
     static testMethod void helpMessagesMethod10() {
       
        HelpMessagesControllerClass hm = new HelpMessagesControllerClass();
      	
        hm.GoalsActionsHelpMessagesave();
    } 
    
}