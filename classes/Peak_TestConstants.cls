public with sharing class Peak_TestConstants {
    public static final String GUEST_ALIAS = 'Guest';
    public static final String GUEST_USERNAME = 'guestuser@peak.com';
    public static final String GUEST_EMAIL = 'guestuser@peak.com';

    public static final String STANDARD_ALIAS = 'Standard';
    public static final String STANDARD_USERNAME = 'standarduser@peak.com';
    public static final String STANDARD_EMAIL = 'standarduser@peak.com';

    public static final String LASTNAME = 'Testing';
    public static final String FIRSTNAME = 'Name';

    public static final String LOCALE = 'en_US';
    public static final String TIMEZONE = 'America/Los_Angeles';

    public static final String ENCODING = 'utf-8';

    public static final String ACCOUNT_NAME = 'Test Account';

    public static final String TEST_MESSAGE = 'Test message';

    public static final String TEST_URL = 'http://7summitsinc.com';
    public static final String TEST_DESCRIPTION = 'Donec ac dolor nec libero sagittis facilisis quis vel mauris. Curabitur eu dignissim augue. Sed ut consequat purus. Curabitur porta eget leo a molestie.';

    public static final String TEST_FILENAME = 'test.png';
    public static final String TEST_FILETYPE = 'image/png';

    public static final String TEST_GROUPNAME = 'Testing Group';

    public static final String MAILING_COUNTRY = 'US';
    public static final String COLLEGUE_NAME = 'Test Collegue';

    public static final String FIELDSTRING = 'Field';


    public static final String EVENT_TESTNAME = 'Test Event';


    public static final String EVENTS_QUERY_STRING = 'Select Id,Name,All_Day_Event__c, (SELECT Id FROM Attachments) from Event__c';
    public static final Datetime FROMDT = Datetime.newInstance(2017,1,1);
    public static final Datetime TODATE = Datetime.newInstance(2017,6,1);
    public static final Set<String> STRINGSET = new Set<String>();

}