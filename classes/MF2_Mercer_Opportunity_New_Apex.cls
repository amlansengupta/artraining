/***************************************************************************************************************
Request Id : 17348
Purpose : This class is created as the controller class for the Lightning Component MF2_Mercer_Opportunity_New
Created by : Harsh Vats
Created Date : 11-APRIL-2019
Project : MF2
****************************************************************************************************************/
public without sharing class MF2_Mercer_Opportunity_New_Apex { 
    
    public Opportunity opp{get; set;}
    public string stageValue{get;set;}
    
    /*  @Method Name: getAutoPopulateOpportunityFields
@Description: Method to auto populate Opportunity field values on New Opportunity page */
    
    @AuraEnabled
    public static OpportunityWrapper getAutoPopulateOpportunityFields()
    {
        Opportunity oppty = new Opportunity();
        String currUserOffice = [select employee_office__c from user where id=:userinfo.getuserid()].employee_office__c;
        List<String> officeList = new List<String>();
        officeList.add('--None--');
        Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_Office__c.getDescribe();
        for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues())
        {
            officeList.add(pickListVal.getLabel());
        } 
        if(currUserOffice<>null && !currUserOffice.containsIgnoreCase(System.Label.Opportunity_New_UserOfficeIgnoreCase) && officeList.contains(currUserOffice))
        {
            oppty.opportunity_office__c = currUserOffice;
        }
        else{
            oppty.opportunity_office__c = '--None--';
        }
        oppty.StageName= System.Label.Opportunity_New_StageValue;
        OpportunityWrapper objWrapper = new OpportunityWrapper(getStageVal(),officeList,oppty.opportunity_office__c,oppty.StageName);
        
        return objWrapper;
    }
    
    //Wrapper Class
    public class OpportunityWrapper
    {
        @AuraEnabled public List<String> stages;
        @AuraEnabled public List<String> officeList;
        @AuraEnabled public String oppOffice;
        @AuraEnabled public String selectedStage;
        
        public OpportunityWrapper(List<String> stages,List<String> officeList,String oppOffice, String selectedStage)
        {
            this.stages = stages;
            this.officeList = officeList;
            this.oppOffice = oppOffice;
            this.selectedStage = selectedStage;
        }
    }
    
    /*  @Method Name: saveOpp
@Description: onClick method for the Save Button on Lightning Component  */
    
    @AuraEnabled
    public static String saveOpp(String fieldSet)
    {
        SavePoint sp;
        Account account;
        Opportunity newRecord=new Opportunity();
        try
        {
            sp = Database.setSavePoint();
            System.debug('fieldSet'+fieldSet);
            Profile prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
            
            Map<String,Object> deserializedData = (Map<String,Object>)JSON.deserializeUntyped(fieldSet);
            
            newRecord.Name = (String)deserializedData.get('Name');
            newRecord.AccountId = (Id)deserializedData.get('AccountId');
            if(deserializedData.get('CloseDate')!=null)
            {
                newRecord.CloseDate = Date.valueOf((String)deserializedData.get('CloseDate'));
            }
            newRecord.Type = (String)deserializedData.get('Type');
            newRecord.Buyer__c = (Id)deserializedData.get('Buyer__c');
            newRecord.CurrencyIsoCode = (String)deserializedData.get('CurrencyIsoCode');
            newRecord.Opportunity_Name_Local__c = (String)deserializedData.get('Opportunity_Name_Local__c');
            newRecord.StageName = (String)deserializedData.get('StageName');
            newRecord.Opportunity_Office__c = (String)deserializedData.get('Opportunity_Office__c');
            newRecord.IsPrivate = (Boolean)deserializedData.get('IsPrivate');
            newRecord.campaignId = (Id)deserializedData.get('CampaignId');
            //system.debug('recc'+Schema.SObjectType.Opportunity.getRecordTypeInfosByName());
            //newRecord.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Detail Page Assinment').getRecordTypeId();
            account = getAccountDetail(newRecord.AccountId);
            if(prof.Name != System.Label.Opportunity_New_ProfName && (account != null) && (account.BillingCity==null||account.BillingStreet==null) && (account.shippingcity == null || account.shippingstreet == null))
            {
                AuraHandledException et = new AuraHandledException(''); et.setMessage('Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.'); throw et;
                //throw new Exception('Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.');
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.'));
                //return null;
            }
            else if(newRecord.StageName == 'None'){
                AuraHandledException et = new AuraHandledException(''); et.setMessage('Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.'); throw et;
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, Please set stage value'));
                //return null;
            }
            else
            {
                
                
                insert newRecord;
                
            }
            Opportunity opp = getOpportunityDetail(newRecord);
            
            if(opp.OwnerId <> opp.Account.OwnerId)
            {
                String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id;
                String post = 'New Opportunity created:  '  + opp.Name + '<' + fullRecordURL + '>';
                
                if(opp.Account.Owner.isActive)
                {
                    //call the method to Notify on chatter the Opportunity owner that Opportunity is created
                    AP43_ChatterUtils.chatterPostMention(opp.AccountId, opp.Account.OwnerId, post, UserInfo.getSessionId());
                    //call the method to follow the Opportunity Owner
                    AP43_ChatterUtils.autoFollow(opp.Id, opp.Account.OwnerId);
                }  
            }
        }
        catch(Exception ex)
        {
            ExceptionLogger.logException(ex, 'MF2_Mercer_Opportunity_New_Apex', 'saveOpp');
            String errorMessage = ex.getMessage();
            Integer occurence;
            if (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                occurence = errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 34;
                errorMessage = errorMessage.mid(occurence, errorMessage.length()); occurence = errorMessage.lastIndexOf(':'); errorMessage = errorMessage.mid(0, occurence);
            }
            else {
                errorMessage = ex.getStackTraceString();
                system.debug('Hi'+errorMessage);
            }
            throw new AuraHandledException(errorMessage);            
        }
        return newRecord.Id;
    }
    
    /*UAT 2670
@Method Name: getStageVal
@Description: Method to get the list of stagevalues  */
    
    public static List<String> getStageVal() 
    {
        List<String> stOptionsList = new List<String>();
        //stOptionsList.add(System.Label.None_With_Dash);
        stOptionsList.add(System.Label.Stage_Above_the_Funnel);           
        stOptionsList.add(System.Label.Stage_Identify);            
        
        return stOptionsList;
    }
    
    /*  @Method Name: getOpportunityDetail
@Description: Method to get the Opportunity details  */
    
    public static Opportunity getOpportunityDetail(Opportunity opp)
    {
        return [Select Id, OwnerId, Name, Account.OwnerId, Account.Name, AccountId, Account.Owner.isActive From opportunity where Id = : opp.Id];
    }
    
    /*  @Method Name: getAccountDetail
@Description: Method to get the Account details  */
    
    public static Account getAccountDetail(string accId)
    {
        if(accId<>null)
        {
            return [select Id, BillingCity, BillingStreet,shippingcity, shippingstreet from account where Id =:accId];
        }
        else
        {
            return null;
        }
    }
}