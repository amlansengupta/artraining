/*Purpose:  This Apex class contains static methods to process OpportunityTeamMember records on their various stages of insertion/deletion==============================================================================================================================================
The methods called perform following functionality:

•   Assign user id to opportunity team Member id
•   Add the opty team member to concatenated opty field with read/write access
•   Fetch contact share record 
•   Populate Concatenated Oppotunity Team Member Field

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sujata   12/18/2012  Created Apex to support TRG03(populates the opp team member id with the user id,Adds the opportunity owner to opty team with read/write access)
   2.0 -    Shashank 04/22/2013  Added Logic to add opty team member to Concatenated Opportunity Team field 
============================================================================================================================================== 
*/
public class AP03_OpportunityTeamMemberTriggerUtil {
 	public static boolean skipFlag= false;
    private static List<OpportunityShare> oppShareList = new List<OpportunityShare>();
    private static OpportunityShare oppShare = new OpportunityShare();
    private static Set<ID> opptIDs = new Set<ID>();    
    private static Set<ID> delcontID = new Set<ID>();
    private static Set<ID> delTeamMembers = new Set<ID>();
    private static Map<ID,Opportunity> opportunityMap = new Map<ID,Opportunity>(); 
    private static Set<ID> oppOwner = new Set<ID>();
    
    private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember(); 

    /*
     * @Description : This method is called on before insertion of OpportunityTeamMember records and is used to assign user id to opty team id
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
     
    public static void processOpportunityTeamMemberBeforeInsert(List<OpportunityTeamMember> triggernew)
    {
        for(OpportunityTeamMember oppteam : triggernew)
            oppteam.OppTeamMemID__c =  oppteam.UserId;                
    }
    
     //Added as part of #12271 May Release 2017
    
    public static void UpdateOpportunityAfterInsert (Map<Id,OpportunityTeamMember> triggernewmap)
    {
        Map<Id,OpportunityTeamMember> oppTeamMap = new Map<Id,OpportunityTeamMember>();
        for(OpportunityTeamMember ot : triggernewmap.values())
        {
            oppTeamMap.put(ot.OpportunityId,ot);
        }
        
        Map<Id,Opportunity> OppMap =new Map<Id,Opportunity> ([Select id,Sales_Coach__c,Executive_Sponsor__c from Opportunity where id =: oppTeamMap.keyset()]);
        
        
        for(OpportunityTeamMember otm : triggernewMap.values())
        {
            for(ID oid : OppMap.keyset())
            {
                if(otm.TeamMemberRole  == 'Executive Sponsor')
                {
                    Opportunity o = OppMap.get(oid);
                    o.Executive_Sponsor__c = true;
                }
                else if(otm.TeamMemberRole == 'Sales Coach')
                {
                    Opportunity o1 = OppMap.get(oid);
                    o1.Sales_Coach__c = true;
                }
            }
         }
        	
            update OppMap.values();
    }
    
    //Added as part of #12271 May Release 2017
    
    //Added as part of #12271 May release 2017
    
    public static void processOpportunityAfterUpdate(Map<Id,OpportunityTeamMember> triggeroldmap)
    {
       
        Map<Id,OpportunityTeamMember> oppTeamMap = new Map<Id,OpportunityTeamMember>();
        integer var = 0;
        integer var1 = 0;
        for(OpportunityTeamMember ot : triggeroldmap.values())
        {
            oppTeamMap.put(ot.OpportunityId,ot);
        }
         Map<Id,Opportunity> OppMap =new Map<Id,Opportunity> ([Select id,Sales_Coach__c,Executive_Sponsor__c from Opportunity where id =: oppTeamMap.keyset()]);
         
         Map<Id,OpportunityTeamMember> otMap = new Map<Id,OpportunityTeamMember> ([Select id,OpportunityId,Opportunity.Type,TeamMemberRole from OpportunityTeamMember where OpportunityId =: OppMap.keyset()]);
         system.debug('>>>'+otMap);
         if(otMap.size() <> 0)
         {
             for(ID oid : OppMap.keyset())
                 {
                     Opportunity o = OppMap.get(oid);
                     o.Sales_Coach__c= false;
                     o.Executive_Sponsor__c = false;
                 }
                AP44_ChatterFeedReporting.FROMFEED=true;           update oppmap.values();
             for(OpportunityTeamMember ot1 : otMap.values())
             {
                 //18930-Fix1
                /* if(ot1.Opportunity.Type =='Product Expansion'){
                     flagcheck = true;
                 }*/
             
                 if(ot1.TeamMemberRole == 'Executive Sponsor')
                 {
                     var = var+1;
                     system.debug('???'+var);
                 }
                 else if(ot1.TeamMemberRole == 'Sales Coach')
                 {
                     var1 = var1 + 1;
                     system.debug('///'+var1);
                 }
             }
             
             if(var > 0)
             {
                 for(ID oid : OppMap.keyset())
                 {
                     Opportunity o = OppMap.get(oid);
                     o.Executive_Sponsor__c = true;
                 }
                 //18930-Fix1
               // if(! flagcheck)
                 update oppmap.values();
                 
             }
             
             if(var1 > 0)
             {
                 system.debug('|||'+var1);
                 for(ID oid : OppMap.keyset())
                 {
                     Opportunity o = OppMap.get(oid);
                     o.Sales_Coach__c= true;
                    
                 }
                 //18930-Fix1
                //if(! flagcheck )
                 update oppmap.values();
             }

         }
         
       
        skipFlag = true;
    }
    
     /*
     * @Description : This method is called on after insertion of OpportunityTeamMember records, this adds the opty team member to concatenated opty field 
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
    public static void processOpportunityTeamMemberAfterInsert(List<OpportunityTeamMember> triggernew)
    {
       //clear cache
       opptIDs.clear();
       oppShareList.clear();
       set<string> opptyTeamMemId = new set<string>(); 
       Map<string, string> userMap = new Map<string, string>();
       List<Opportunity> oppList = new List<Opportunity>();
       try
       {        
           for (OpportunityTeamMember otm : triggernew)
           { 
               opptIDs.add(otm.OpportunityId);
               
           }
                      
          for(OpportunityShare oShare : [Select Id, OpportunityId, OpportunityAccessLevel From OpportunityShare Where OpportunityId IN : opptIDs AND RowCause = 'Sales Team' AND OpportunityAccessLevel != 'All'])
          {
                oShare.OpportunityAccessLevel = 'Edit'; 
                oppShareList.add(oShare);
            
          }
          
          if (oppShareList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
          if(!oppShareList.isEmpty()) Database.insert(oppShareList);
      }
      
      catch(Exception ex)
      {
          System.debug('Exception occured at OpportunityTeamMember Trigger processOpportunityTeamMemberAfterInsert with reason :'+Ex.getMessage());
          triggernew[0].adderror('Exception occured at OpportunityTeamMember Trigger processOpportunityTeamMemberAfterInsert with reason :'+ex.getMessage()+'. Please Contact your Administrator.');
          throw ex;      
      }
    }
    
    
     /*
     * @Description : This method is called on before deletion of OpportunityTeamMember records,to fetch contact share record and populate Concatenated Oppotunity Team Member Field. 
     * @ Args       : Trigger.oldmap
     * @ Return     : void
     */
    public static void processOpportunityTeamMemberBeforedelete(Map<Id, OpportunityTeamMember> triggeroldmap)
    {
        
            List<OpportunityShare> oppShareForDeleteList = new List<OpportunityShare>();
            set<string> memberHashMap = new set<string>();
            set<string> opptyId = new set<string>();
            List<Opportunity> oppList = new List<Opportunity>();
            //clear cache
            delTeamMembers.clear();
            
            try
            {
                for(OpportunityTeamMember  rec : triggeroldmap.values())
                {
                    delcontID.add(rec.OpportunityId);
                    delTeamMembers.add(rec.userId );
                    string hashVal = rec.OpportunityId+'#'+rec.UserId;
                    memberHashMap.add(hashVal);
                    opptyId.add(rec.opportunityId);
                }
                
                for(Opportunity opp : [Select ID,ownerID from Opportunity where ID IN : delcontID])
                {
                    oppOwner.add(opp.ownerID);
                }
    
            
              //Fetch ContactShare records corresponding to deleted Contact team member.
                for(OpportunityShare opptShare : [Select OpportunityId ,UserOrGroupId ,OpportunityAccessLevel from OpportunityShare where OpportunityId IN : delcontID and UserOrGroupId IN : delTeamMembers and RowCause = 'Manual'])
                {
                   if(!oppOwner.contains(opptShare.UserOrGroupId))
                   {
                        //Delete the corresponding record from ContactShare.
                        oppShareForDeleteList.add(opptShare);
                         //delete   opptShare ;
                   }  
       
                }
                if (oppShareForDeleteList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
                if(!oppShareForDeleteList.isEmpty()) delete oppShareForDeleteList;
            }            
            
            catch(Exception ex)
            {
                System.debug('Exception occured at OpportunityTeamMember Trigger processOpportunityTeamMemberBeforedelete with reason :'+Ex.getMessage());
                 triggeroldmap.values()[0].adderror('Exception occured at OpportunityTeamMember Trigger processOpportunityTeamMemberBeforedelete with reason :'+ex.getMessage()+'. Please Contact your Administrator.');    
                throw ex;  
            }
    }
    /*public static void processSalesCredteAfterDelete(List<OpportunityTeamMember> triggerOld){
        Set<Id> usrId =new Set<Id>();
        Set<Id> OpptyId = new Set<Id>();
        for(OpportunityTeamMember otm : triggerOld){
            usrId.add(otm.userID);
            OpptyId.add(otm.OpportunityId);
        }
        Set<String> UserEmpId = new Set<String>();
        if(!usrId.isEmpty()){
            for(User us:[select id, EmployeeNumber from User where id in:usrId]){
                UserEmpId.add(us.EmployeeNumber);
            }
        }
        List<Sales_Credit__c> delScList =new List<Sales_Credit__c>();
        if(!OpptyId.isEmpty()){
            for(Sales_Credit__c sc: [select Id, Opportunity__c,EmpName__c,EmpName__r.EMPLID__c from Sales_Credit__c where Opportunity__c in:OpptyId]){
                if(UserEmpId.contains(sc.EmpName__r.EMPLID__c)){
                    delScList.add(sc);
                }
            }
        }
        if(!delScList.isEmpty()){
            database.delete(delScList);
            
        }
   
    }*/
     
}