/*
class Name - AP65_EmailController
Created By - Gyan Chandra
Created Date - 27/8/2013
Purpose - This class will be used to send Welcome Email to User

*/
Global class AP65_EmailController 
{
	public static final String STR_ORGWIDEEMAIL = 'Support Email';
    webService static void sendLiteEmail(ID userId,string TemplateName)
    {
        //Fetch user record based on Record ID
       User u = [select email,collegue__r.email from User where Id =:userId];
       
       //Fetch data from Email template
       EmailTemplate objEmail=[Select e.Subject, e.Name, e.Body, e.Id, HTMLValue From EmailTemplate e where e.DeveloperName=:TemplateName limit 1];
       OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: MercerSupportEmail__c.getInstance(STR_ORGWIDEEMAIL).Value__c];


 
       if(objEmail != null)
       {
        String[] ccAddresses;
        string strBody=objEmail.Body;
        string strSubject=objEmail.Subject;
        MercerSupportEmail__c myCS1 = MercerSupportEmail__c.getValues('Support Email');
        String strEmail = myCS1.Value__c;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        String[] toAddresses = new String[]{u.email}; 
        //mail.setToAddresses(toAddresses); 
        if(u.collegue__r.email!=Null)
        {
        ccAddresses = new String[]{u.collegue__r.email}; 
        mail.setCcAddresses(ccAddresses);
        }
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        //mail.setSenderDisplayName ('MercerForce Support'); 
        mail.setTemplateId(objEmail.Id);
        mail.setTargetObjectId(userId);//Required if using a template, optional otherwise. The ID of the contact, lead, or user to which the email will be sent. The ID you specify sets the context and ensures that merge fields in the template contain the correct data.
        mail.saveAsactivity=false;
        mail.setBccSender(false); 
        mail.setReplyTo(strEmail);
        mail.setUseSignature(false); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }
        
    }
}