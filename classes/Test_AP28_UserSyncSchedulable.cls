/* Purpose: This test class provides data coverage for AP28_UserSyncSchedulable schedulable class
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Arijit       	 03/29/2013  Created test method for AP28_UserSyncSchedulable schedulable class
 ==================================================================================================================*/    
@isTest
private class Test_AP28_UserSyncSchedulable {
	/*
     * @Description : Test method to provide data coverage for AP28_UserSyncSchedulable schedulable class
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
        Test.StartTest();
        	 
        	 Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
        	 testMog.Name = 'ABC';
        	 testMog.Market__c = 'United States';
        	 testMog.Sub_Market__c = 'Canada – National';
        	 testMog.Region__c = 'Manhattan';
        	 testMog.Sub_Region__c = 'Canada';
        	 testMog.Country__c = 'Canada';
        	 testMog.Isupdated__c = true;
        	 testMog.Office_Code__c = '123';
        	 testMog.Office__c = 'US';
        	 insert testMog;
        	
        	//String q = 'select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True';
			AP28_UserSyncSchedulable bc = new AP28_UserSyncSchedulable();
	        String sch = '0 0 23 * * ?';
	        system.schedule('Test UserSyncSchedulable', sch, bc);
		Test.stopTest();
    }
}