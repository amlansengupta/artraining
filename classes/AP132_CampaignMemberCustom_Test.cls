/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AP132_CampaignMemberCustom_Test {

private static Mercer_TestData mtdata = new Mercer_TestData();
    static testMethod void myUnitTest() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Campaign camp1 = new Campaign();
        camp1.Name = 'TestCampaign';
        camp1.CurrencyIsoCode = 'AED';
        camp1.Scope__c = 'Global';
        camp1.EndDate = date.Today() - 1;
        camp1.IsActive = true;
        insert Camp1;
        
        Account acct = mtdata.buildAccount();
        Contact testCont = mtdata.buildContact();
        testCont.ownerid = user1.id;
        update testCont;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test1234pp@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId =  acct.Id;
        testContact.ownerid = user1.id;
        insert testContact;
        
        Contact testContact1 = new Contact();
        testContact1.Salutation = 'Fr.';
        testContact1.FirstName = 'TestFirstName1';
        testContact1.LastName = 'TestLastName1';
        testContact1.Job_Function__c = 'TestJob';
        testContact1.Title = 'TestTitle';
        testContact1.MailingCity  = 'TestCity';
        testContact1.MailingCountry = 'TestCountry';
        testContact1.MailingState = 'TestState'; 
        testContact1.MailingStreet = 'TestStreet'; 
        testContact1.MailingPostalCode = 'TestPostalCode'; 
        testContact1.Phone = '9999999999';
        testContact1.Email = 'test1234q@xyz.com';
        testContact1.MobilePhone = '9999999999';
        testContact1.AccountId =  acct.Id;
        testContact1.ownerid = user1.id;
        //testContact1.NPS_Survey_Sent_Date__c= Date.today();
        //insert testContact1;
        

        
        

        Contact testContact2 = new Contact();
        testContact2.Salutation = 'Fr.';
        testContact2.FirstName = 'TestFirstName2';
        testContact2.LastName = 'TestLastName2';
        testContact2.Job_Function__c = 'TestJob';
        testContact2.Title = 'TestTitle';
        testContact2.MailingCity  = 'TestCity';
        testContact2.MailingCountry = 'TestCountry';
        testContact2.MailingState = 'TestState'; 
        testContact2.MailingStreet = 'TestStreet'; 
        testContact2.MailingPostalCode = 'TestPostalCode'; 
        testContact2.Phone = '9999999999';
        testContact2.Email = 'test1231q1@xyz.com';
        testContact2.MobilePhone = '9999999999';
        testContact2.AccountId =  acct.Id;
        testContact2.ownerid = user1.id;
        //testContact2.NPS_Survey_Sent_Date__c= Date.today();
        //insert testContact2;
        

        
        CampaignMember campmember= new CampaignMember();
        campmember.CampaignId=camp1.Id;
        campmember.ContactId=testContact.id;
        campmember.status='Identified';
        insert campmember;
        
        /*CampaignMember campmember1= new CampaignMember();
        campmember1.CampaignId=camp1.Id;
        campmember1.ContactId=testContact2.id;
        campmember1.status='Identified';
        insert campmember1;*/
        
        
      
        Test.startTest();
        system.runAs(User1){
            
            PageReference pageRef = Page.Mercer_CampaignMember;
            test.setCurrentPageReference(pageRef);
            ApexPages.StandardController stdController = new ApexPages.StandardController(Camp1);
            ApexPages.CurrentPage().getParameters().put('ids',Camp1.Id);    
            AP132_CampaignMemberCustom  controller = new AP132_CampaignMemberCustom(stdController);
            
            controller.contactAll[0].checked = true;
            controller.saveButton();
            
            controller.searchParam = 'Test Contact';
            controller.runSearch();
            
             
            
             controller.PR1  = 'Equals';
             controller.PR2  = testContact1.firstname;
             controller.PR3  = 'First Name';
             controller.runSearch();
             controller.clearSearch();
             
             controller.PR1  = 'Equals';
             controller.PR2  = testContact1.lastname;
             controller.PR3  = 'Last Name';
             controller.runSearch();
             
             controller.PR1  = 'Equals';
             controller.PR2  = 'VP';
             controller.PR3  = 'Job Level';
             controller.runSearch();
             
             controller.PR1  = 'Equals';
             controller.PR2  = 'TestAccountName';
             controller.PR3  = 'Account Name';
             controller.runSearch();
             
             controller.PR1  = 'Contains';
             controller.PR2  = testContact1.firstname;
             controller.PR3  = 'First Name';
             controller.runSearch();
             
             controller.PR1  = 'Contains';
             controller.PR2  = testContact1.lastname;
             controller.PR3  = 'Last Name';
             controller.runSearch();
             
             controller.PR1  = 'Contains';
             controller.PR2  = 'VP';
             controller.PR3  = 'Job Level';
             controller.runSearch();
             
             controller.PR1  = 'Contains';
             controller.PR2  = 'TestAccountName';
             controller.PR3  = 'Account Name';
             controller.runSearch();
              
              
             controller.PR1  = 'Starts With';
             controller.PR2  = testContact1.firstname;
             controller.PR3  = 'First Name';
             controller.runSearch();
             
             controller.PR1  = 'Starts With';
             controller.PR2  = testContact1.lastname;
             controller.PR3  = 'Last Name';
             controller.runSearch();
             
             controller.PR1  = 'Starts With';
             controller.PR2  = 'VP';
             controller.PR3  = 'Job Level';
             controller.runSearch();
             
             controller.PR1  = 'Starts With';
             controller.PR2  = 'TestAccountName';
             controller.PR3  = 'Account Name';
             controller.runSearch();
             
             controller.toggleSort();
             controller.firstpage();
             controller.nextpage();
             controller.previouspage();
             controller.lastpage();
             controller.getTotalPages(); 
                       
             controller.contactAll[1].checked = true; 
             //controller.selectedCont();
             controller.saveButton();
             controller.cancelButton();
            
            
        
        }
        Test.stopTest();
        
        
    }
    
     
     static testMethod void myUnitTest3() {
        
        Campaign camp = new Campaign();
        camp.Name = 'TestCampaign';
        camp.CurrencyIsoCode = 'AED';
        camp.Scope__c = 'Global';
        camp.EndDate = date.Today() - 1;
        camp.IsActive = true;
        insert Camp;
        
        Account acct = mtdata.buildAccount();
        Contact testCont = mtdata.buildContact();

        
        CampaignMember campmember= new CampaignMember();
        campmember.CampaignId=camp.Id;
        campmember.ContactId=testCont.id;
        campmember.status='Identified';
        insert campmember;
      
        Test.startTest();
            delete campmember;
         Test.stopTest();
     }
}