/*Request Id#17448: This class is responsible to refresh bill rates on Scope Modeling Employee record */
public class RefreshBillRateApexController {
    /*
    * Request Id#17448:This method accepts the scope modeling record id and refreshes all related 
    * scope modeling employee records' bill rates 
    * according to the currency code
    */
    @AuraEnabled
    public static void refreshBillRates(Id recordId){
        try{
            if(recordId != null){
                Map<String, Double> mapIsoConversionMap = new Map<String, Double>();
                String currentRecordIsoCode = null;
                //To fetch all scope modeling employee records related to scope modeling record. 
                List<Scope_Modeling_Employee__c> listScopeItEmpRecs = [select Id, Bill_Rate_Opp__c,CurrencyIsoCode,
                                                                   Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c,
                                                                   Employee_Bill_Rate__r.CurrencyIsoCode from Scope_Modeling_Employee__c 
                                                                   where Scope_Modeling_Task__r.Scope_Modeling__c =: recordId];
                //To fetch all currency code and conversion rate from system.
                List<CurrencyType> listExchangeRates = [Select IsoCode, ConversionRate FROM CurrencyType];
                if(listExchangeRates != null){
                    //To keep a map of currency code and corresponding conversion rate
                    for(CurrencyType rate : listExchangeRates){
                        mapIsoConversionMap.put(rate.IsoCode, rate.ConversionRate);
                    }
                }
                List<Scope_Modeling_Employee__c> listScopeModelToUpdate = new List<Scope_Modeling_Employee__c>();
                //To ensure system has scope modeling employee record(s) for the provided scope modeling record
                if(listScopeItEmpRecs != null && !listScopeItEmpRecs.isEmpty()){
                    currentRecordIsoCode = listScopeItEmpRecs.get(0).CurrencyIsoCode;
                    //To go thorough all related scope modeling employee records and refresh their bill rate as per the updated conversion rate
                    for(Scope_Modeling_Employee__c scopeModelEmployee : listScopeItEmpRecs){
                        scopeModelEmployee.Bill_Rate_Opp__c = ((scopeModelEmployee.Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c
                                                                * mapIsoConversionMap.get(currentRecordIsoCode))
                                                               /mapIsoConversionMap.get(scopeModelEmployee.Employee_Bill_Rate__r.CurrencyIsoCode)); 
                        listScopeModelToUpdate.add(scopeModelEmployee);
                    }
                }
                //To update the bill rates in system.
                if(!listScopeModelToUpdate.isEmpty()){
                    update listScopeModelToUpdate;
                }
            }
        }catch(Exception ex){
            System.debug('Error:'+ex.getStackTraceString());
            throw new AuraHandledException('Could not refresh Bill Rates! Please try after sometime.');
        }
    }
}