/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Shashank 03/29/2013  Created Controller Class.
=============================================================================================================================================== 
*/
public with sharing class AP58_AccountTeamController 
{
    public Extended_Account_Team__c accTeam = new Extended_Account_Team__c();
    
    public AP58_AccountTeamController(ApexPages.StandardController controller) 
    {
        this.accTeam = (Extended_Account_Team__c)controller.getRecord();
    }
    
    public pageReference save()
    {
        pageReference pageRef = new pageReference('/'+accTeam.Account__c);
        try
        {
            upsert accTeam;
        }catch(DMLexception e){ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));pageRef = null; return pageRef;  
        }catch(Exception ex){ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage())); pageRef = null;return pageRef;}
        
        return pageRef;
    }
    
    public pageReference savenew()
    {
        string url = '/apex/Mercer_ExtendedAccountTeamPage?CF00Nc0000000iD28='+accTeam.Account__r.Name+'&CF00Nc0000000iD28_lkid='+accTeam.Account__c+'&scontrolCaching=1&retURL=%2F'+accTeam.Account__c+'&sfdc.override=1';
        pageReference pageRef = new pageReference(url);
        try
        {
            upsert accTeam;
        }catch(DMLexception e){ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0))); pageRef = null; return pageRef;  
        }catch(Exception ex){ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage())); pageRef = null; return pageRef;}
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public pageReference cancel()
    {
        pageReference pageRef = new PageReference('/'+accTeam.Account__c);
        return pageRef;
    }
}