/*Purpose: Test Class for providing code coverage to MS01_AccountMDriveStatusSchedulable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_MS01_AccountMDriveStatusSchedulable
{
    /* * @Description : This test method provides data coverage to MS01_AccountMDriveStatusSchedulable class         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest() 
    {
        
        User user1 = new User();
       String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
       user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
       system.runAs(User1){
        Test.StartTest();
        String jobId = System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new MS01_AccountMDriveStatusSchedulable());
        CronTrigger ct  = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
        Test.stopTest();
        System.assertEquals('0 0 0 3 9 ? 2022',ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        }
    }
}