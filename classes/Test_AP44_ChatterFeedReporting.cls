/*
Purpose: This test class provides data coverage  to AP44_ChatterFeedReporting class
================================================================================================================================
History
 ------     
  VERSION     AUTHOR     DATE            DETAIL    
               1.0 -    Shashank    03/29/2013      Created test Class.
               1.1 -    Gyan        10/25/2013      Modified test Class as per Req-3407
================================================================================================================================
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP44_ChatterFeedReporting 
{
    /*
     * @Description : Test method to provide data coverage to AP44_ChatterFeedReporting class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '1223237902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        User usr = [Select Id from user where id=: UserInfo.getUserId()];
        User testUser = new User();
        User user1 = new User();
        system.runAs(usr){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        testUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', testUser);
        
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        }
        CollaborationGroup testGroup = new CollaborationGroup();
        testGroup.Name = 'Test Group AP44';
        testGroup.CollaborationType = 'Public';
        insert testGroup;
        
        
        //test.startTest(); 
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = '123456';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc_test@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        //test.stopTest(); 
          
        system.runAs(User1){
        test.startTest(); 
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638-Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
            testOppty.Opportunity_Office__c = 'Houston - Dallas';
        insert testOppty;
                     
        List<FeedItem> testFeedList = new List<FeedItem>();
        FeedItem f;
        for(integer i=0; i<5; i++)
        {
            f = new FeedItem();
            f.body = 'Hello';
            f.Type = 'TextPost';
            testFeedList.add(f);
        }
        testFeedList[0].ParentId = testAccount.Id;
        testFeedList[1].ParentId = testContact.Id;
        testFeedList[2].ParentId = testOppty.Id;
        testFeedList[3].ParentId = testGroup.Id;
        testFeedList[4].ParentId = testUser.Id;
        insert testFeedList;
        test.stopTest();
        delete testFeedList;
        }
        
    }
}