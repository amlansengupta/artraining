/*****************************************************************************************************************************
User story : 17673 (epic)
Purpose : This apex class is created for custom lookup component.
Created by : Soumil Dasgupta
Created Date : 29/03/2019
Project : MF2
******************************************************************************************************************************/

public class customLookUpControllerApex {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        String searchKey = searchKeyWord + '%';
        searchKey = String.escapeSingleQuotes(searchKey);
        
        List < sObject > returnList = new List < sObject > ();
      	// Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        //Appending extra fields
        String sQuery;
        if(ObjectName.equalsIgnoreCase('Account')){
         	  sQuery =  'select id, Name,Account_Name_Local__c,one_code__c from Account where Name LIKE: searchKey order by createdDate DESC limit 5';  
        }
        else if(ObjectName.equalsIgnoreCase('Colleague__c')){
           sQuery =  'select id, Name, Email_Address__c, First_Name__c, Last_Name__c, EMPLID__c from Colleague__c where Name LIKE: searchKey order by Name DESC limit 5';  
        }
        else{
           sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5'; 
        }
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    @AuraEnabled
    public static  sObject fetchDefaultLookUp(String recId,String ObjectName) {
        String sQuery =  'select id, Name from ' +ObjectName + ' where Id =: recId ';
         sObject  Record = Database.query(sQuery);
        return Record;
    }
}