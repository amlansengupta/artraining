/* Purpose: TThis test class provides data coverage to DC08_OpportunityDataUpdate class. 
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Oportunity as an input and updates Opp record.
 
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Suchi           05/27/2013    Created test class.
 ======================================================================================================================================
 */
/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest
private class Test_DC08_OpportunityDataUpdate  {
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    	Request No. 17953 : Removing Finalist END */
    }
    
    /*
     * @Description : Test method to provide daat coverage to DC08_OpportunityDataUpdate class 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
         createCS();
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        insert Acc;
        
        Opportunity oppty= new Opportunity();
        oppty.Name = 'Test Opportunity3' + String.valueOf(Date.Today());
        oppty.Type = 'New Client';
        //request id:12638;commenting step
        //oppty.Step__c = 'Identified Deal';
        oppty.CloseDate = Date.Today();
        oppty.CurrencyIsoCode = 'USD';
        oppty.AccountId = Acc.id;
        oppty.StageName = 'Closed / Won';
        oppty.Amount = 23000.20;
        oppty.Total_Opportunity_Revenue_USD__c = 90000.00;
        oppty.row_flag__c = true;
         oppty.Opportunity_Office__c = 'Aarhus - Axel';
        insert oppty;
    
        //String query = 'SELECT CloseDate,CurrencyIsoCode,Total_Opportunity_Revenue_USD__c,Amount FROM Opportunity where Id =:' + '\'' + oppty.Id + '\'';
        String query = 'SELECT CloseDate,CurrencyIsoCode,Total_Opportunity_Revenue_USD__c,Amount FROM Opportunity where row_flag__c = true' ;
        database.executeBatch(new  DC08_OpportunityDataUpdate(query));
    
    }
}