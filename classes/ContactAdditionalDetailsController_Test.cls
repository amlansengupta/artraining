/***************************************************************************************************************
Request Id : 17560
Purpose : This test class is created for the Apex Class ContactAdditionalDetailsController
Created by : Archisman Ghosh
Created Date : 04/03/2019
Project : MF2
****************************************************************************************************************/
@isTest
public class ContactAdditionalDetailsController_Test
{
    static testMethod void testGetContact()
    {
        Account testAccount = new Account();  
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.Phone = '9999999999';
        testContact.Email = 'abc711@xyz.com';
        testContact.MobilePhone = '9999999999';
        
        testContact.AccountId = testAccount.Id;
        testContact.M_A__c=2;
        testContact.Benefits_Advisory__c=2;
        testContact.HR_Transformation__c=2;    
        testContact.Core_Brokerage__c=2;
        testContact.Defined_Benefit_Pension_Risk__c=2;
        testContact.Mobility_Talent_IS__c=2;
        testContact.Defined_Contribution__c=2;
        testContact.Non_Medical_Voluntary_Benefits__c=2;
        testContact.Endowment_Foundations_Mgmt__c=2;
        testContact.Private_Exchange__c=2;
        testContact.Executive_Rewards__c=2;
        testContact.Talent_Strategy__c=2;
        testContact.Financial_Wellness__c=2;
        testContact.Wealth_Management__c=2;    
        testContact.Global_Benefits__c=2;    
        testContact.Workforce_Rewards__c=2; 
        insert testContact;
        
        jsonToApex ob=new jsonToApex();
        ob.Id= testContact.Id;
        ob.Mac= String.valueOf(testContact.M_A__c);
        ob.Bac=String.valueOf(testContact.Benefits_Advisory__c);
        ob.Hr=String.valueOf(testContact.HR_Transformation__c);
        ob.Cb=String.valueOf(testContact.Core_Brokerage__c);
        ob.DefinedBenefit=String.valueOf(testContact.Defined_Benefit_Pension_Risk__c);
        ob.MobilityTalent=String.valueOf(testContact.Mobility_Talent_IS__c);
        ob.DefinedContribution=String.valueOf(testContact.Defined_Contribution__c);
        ob.NonMedical=String.valueOf(testContact.Non_Medical_Voluntary_Benefits__c);
        ob.EndowmentFoundations=String.valueOf(testContact.Endowment_Foundations_Mgmt__c);
        ob.Sl=testContact.Salutation;
        ob.fn=testContact.FirstName;
        ob.ln=testContact.LastName;
        ob.ph=testContact.Phone;
        ob.jf=testContact.Job_Function__c;
        ob.mob=testContact.MobilePhone;
        ob.acc=testContact.AccountId;
        ob.email=testContact.Email;
        ob.title=testContact.Title;
        
        ID contId=testContact.id;
        system.assert(contId != null);
        
        ContactAdditionalDetailsController.getContact(contId);
        String testContFinal=JSON.serialize(ob);
        ContactAdditionalDetailsController.saveContact(testContFinal);
        
    }
    static testMethod void testGetContactStandard()
    {
        Set<ID> RoleIds = new Set<ID>();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        User testUser = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        RoleIds.add(testUser.UserRoleId);
        
        Account testAccount = new Account();  
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.Phone = '9999999999';
        testContact.Email = 'abc711@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        ID contId=testContact.id;
        
        system.runAs(testUser){
            test.startTest();
            ContactAdditionalDetailsController.isRunningUserInParentHierarchyCon(contId);
            ContactAdditionalDetailsController.getAllSubRoleIds(RoleIds);
            test.stopTest();
        }
    }
    public class jsonToApex{
        public String Id;
        public String Mac;
        public String Bac;
        public String Hr; 
        public String Cb;
        public String DefinedBenefit;
        public String MobilityTalent;
        public String DefinedContribution;
        public String NonMedical;
        public String EndowmentFoundations;
        public String Sl;
        public String fn;
        public String ln;
        public String ph;
        public String jf;
        public String mob;
        public String acc;
        public String email;
        public String title;
    }
}