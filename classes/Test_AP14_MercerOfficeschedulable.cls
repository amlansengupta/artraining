/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AP14_MercerOfficeschedulable 
{
	/*
     * @Description : Test method to provide data coverage to AP14_MercerOfficeschedulable schedulable class  
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void testSchedulable() 
    {
        Test.StartTest();
        	  
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        	 Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
        	 testMog.Name = 'ABC';
        	 testMog.Market__c = 'United States';
        	 testMog.Sub_Market__c = 'Canada – National';
        	 testMog.Region__c = 'Manhattan';
        	 testMog.Sub_Region__c = 'Canada';
        	 testMog.Country__c = 'Canada';
        	 testMog.Isupdated__c = true;
        	 testMog.Office_Code__c = '123';
        	 testMog.Office__c = 'US';
        	 insert testMog;
        	
        	system.runAs(User1){
        	//String q = 'select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True';
			AP14_MercerOfficeschedulable bc = new AP14_MercerOfficeschedulable();
	        String sch = '0 0 23 * * ?';
	        system.schedule('Test MercerOfficeSchedulable', sch, bc);
        	}
		Test.stopTest();
    }
}