/*Purpose: This apex class provides data coverage to AP21_MultipleDistributionController  class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE             DETAIL 
   1.0 -             Arijit            3/4/2013      Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_AP21_MultipleDistributionController {
	/*
     * @Description : Test method for creating contact distribution and adding multiple distributions to it. 
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Distribution__c dis = new Distribution__c();
        dis.Name = 'TestDistribution';
        dis.Scope__c = 'Country';
        dis.Country__c = 'Abc';
        insert dis;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '123';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Contact_Distributions__c conDis = new Contact_Distributions__c(); 
        conDis.Contact__c = testContact.Id;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(conDis);
        AP21_MultipleDistributionsController controller = new AP21_MultipleDistributionsController(stdController);
        
        controller.distributionList[0].Distribution.Distribution__c = dis.Id;
        
        controller.addDistributionRow();
        controller.removeIndex = 1;
        controller.removeDistribution();
        
        Pagereference pageref = controller.save();
    }
}