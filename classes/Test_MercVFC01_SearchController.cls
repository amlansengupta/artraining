/*Purpose: Test Class for providing code coverage to MercVFC01_SearchController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   01/10/2014  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_MercVFC01_SearchController {
   /*
    * @Description : This test method provides data coverage for Account Search functionality         
    * @ Args       : no arguments        
    * @ Return     : void       
    */   
    static testMethod void testMercVFC01_SearchController()
    {
       //page instatiated
       User user1 = new User();
       String AdminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
       user1 = Mercer_TestData.createUser1(AdminUserprofile , 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        
           PageReference pageRef = Page.Mercer_Account_Search;
             
           Test.setCurrentPage(pageRef);
           
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        List<Account> testAccountList = new List<Account>();
        Account testAccount;
        for(integer i = 0; i<=5; i++)
        {
            testAccount = new Account();
            testAccount.Name = 'TestAccountName'+i;
            testAccount.BillingCity = 'TestCity'+i;
            testAccount.BillingCountry = 'TestCountry'+i;
            testAccount.BillingStreet = 'Test Street'+i;
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccountList.add(testAccount);
        }
            insert testAccountList;
         ApexPages.StandardController stdController = new ApexPages.StandardController(testAccountList[0]);
          MercVFC01_SearchController controller1 = new MercVFC01_SearchController(stdController);
          MercVFC01_SearchController controller = new MercVFC01_SearchController();
          controller.nameQuery = 'Test';
          controller.executeSearch();
          controller.nameQuery = 'shashank';
          controller.executeSearch();
          controller.createnew();
          testAccountlist = controller.getAccounts();
          controller.setSortDirection('');
          controller.setSortDirection('ASC');
          controller.sortExpression = 'name';  
          controller.setSortDirection('name');            
          controller.sortExpression = '';
          controller.sortExpression = null;
          controller.getSortDirection();
           
      
          } 
                    
    }
}