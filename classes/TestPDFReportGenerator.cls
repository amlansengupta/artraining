/*Purpose: Test Class for providing code coverage to all methods of PDFReportGenerator class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   02/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData=false)
public class TestPDFReportGenerator
{

    @testSetup static void setup(){


        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
         setting = new ApexConstants__c();
        setting.Name = 'Opportunity_Country';
        setting.StrValue__c = 'Korea;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul - Gangnamdae-ro    ;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */
        
        Insert listofvalues;
        List<Opportunity> oppList = new List<Opportunity>();
       List<OpportunityLineItem> olList = new List<OpportunityLineItem>();
       List<String> accIdList = new List<String>();
      
        
        //Id AccId = [Select Id from Account where Name = 'Test1'].Id;
           

       // Create Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '13513578902';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        //insert testColleague;
       
        
             
       // Create Account Record
        Account a1 = new Account();
        a1.Name = 'Test1';
        a1.One_Code__c = 'ARFB12';
        a1.GU_DUNS__c = '1256';   
        a1.DUNS__c = '1256'; 
        a1.Account_Country__c = 'India';
        a1.Account_Region__c = 'EuroPac';
        a1.Account_Market__c = 'ASEAN';
        a1.Market_Segment__c = '4 - Key';
        a1.Talent_CY_Revenue__c = 45.35;
        a1.Health_CY_Revenue__c = 89.52;
        a1.Retirement_CY_Revenue__c = 75.96;
        a1.Investments_CY_Revenue__c = 52.36;
        a1.Outsourcing_CY_Revenue__c = 56.63;
        a1.Other_CY_Revenue__c = 42.56;
        a1.Global_Business_Solutions_CY__c = 89.52;
        a1.Global_Business_Solutions_PY__c = 189.52;
        a1.Talent_PY_Revenue__c = 145.35;
        a1.Health_PY_Revenue__c = 189.52;
        a1.Retirement_PY_Revenue__c = 175.96;
        a1.Investments_PY_Revenue__c = 152.36;
        a1.Outsourcing_PY_Revenue__c = 156.63;
        a1.Other_PY_Revenue__c = 42.56;
        a1.Industry = 'Agriculture';
        a1.Primary_Industry_Category__c = 'test';
        a1.Employees_Editable__c = 123;
        a1.billingstreet = 'A-302';
        a1.billingcountry = 'India';
        a1.Total_CY_Revenue__c = 452.85;
        a1.Total_PY_Revenue__c = 879.52;
        //a1.Relationship_Manager__c = testColleague.Id;  
        insert a1;
        
         Contact c = new Contact();
            c.accountid = a1.id;
            c.LastName = 'test';
            c.mailingcountry = 'India';
            c.Job_Function__c = 'test';
            c.title = 'test';
            c.phone = '986895689';
            c.mailingcity = 'Delhi';
            c.Contact_Status__c = 'Active';
            c.FirstName = 'test firstname';
            c.Email = 'abc@xyz.com';
            insert c;
      
       
        // Create Opportunity record with Stage = Identify
        Opportunity opp = new Opportunity();
        opp.name = 'TestOppI';
        opp.Business_LOB__c = 'test';
        opp.AccountId = a1.Id;
        opp.Total_Opportunity_Revenue_USD__c = 25.36;
        opp.Opportunity_Region__c = 'EuroPac';
        opp.StageName = 'Identify';
        //Request Id 12638 Commenting step
        //opp.Step__c = 'Identified Deal';
        opp.CloseDate = date.Today();
        opp.CurrencyIsoCode = 'ALL';
        opp.Opportunity_Market__c = 'ASEAN';
        opp.Close_Stage_Reason__c = 'Other' ;
         opp.Opportunity_Office__c ='Houston - Dallas';
        opp.Buyer__c=c.id;
        oppList.add(opp);
        
        // Create Opportunity record with Stage = Active Pursuit
        Opportunity opp1 = new Opportunity();
        opp1.name = 'TestOppAP';
        opp1.Business_LOB__c = 'test1';
        opp1.AccountId = a1.Id;
        opp1.Total_Opportunity_Revenue_USD__c = 25.36;
        opp1.Opportunity_Region__c = 'EuroPac';
        //Updated as part of 5166(july 2015)
        List<String> oppOffices_List = new List<String>();               
       // ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
      //  if(officesToBeExcluded.StrValue__c != null){        
     //   oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        opp1.Opportunity_Office__c ='Houston - Dallas';
      //  }
        opp1.StageName = 'Identify';
        //Request Id 12638 commenting step
        //opp1.Step__c = 'Identified Deal';
        //opp1.StageName = 'Active Pursuit';
        //opp1.Step__c = 'Identified Deal';
        opp1.CloseDate = date.Today();
        opp1.CurrencyIsoCode = 'ALL';
        opp1.Opportunity_Market__c = 'ASEAN';
        opp1.Close_Stage_Reason__c = 'Other' ;
         opp1.Buyer__c=c.id;
        oppList.add(opp1);
       
        // Create Opportunity record with Stage = Finalist
        Opportunity opp2 = new Opportunity();
        opp2.name = 'TestOppFinalist';
        opp2.Business_LOB__c = 'test1';
        opp2.AccountId = a1.Id;
        opp2.Total_Opportunity_Revenue_USD__c = 25.36;
        opp2.Opportunity_Region__c = 'EuroPac';
        opp2.StageName = 'Identify';
        //Request Id 12638 Commenting step
        //opp2.Step__c = 'Identified Deal';
        //opp2.StageName = 'Finalist';
        //opp2.Step__c = 'Identified Deal';
        //Updated as part of 5166(july 2015)
        oppOffices_List = new List<String>();               
        //officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');    
        
       // if(officesToBeExcluded.StrValue__c != null){        
       // oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        opp2.Opportunity_Office__c = 'Houston - Dallas';
      //  }
        opp2.CloseDate = date.Today();
        opp2.Close_Stage_Reason__c = 'Other' ;
        opp2.CurrencyIsoCode = 'ALL';
        opp2.Opportunity_Market__c = 'ASEAN';
         opp2.Buyer__c=c.id;
        oppList.add(opp2);
        
      // Create Opportunity record with Stage = Selected
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOpptyID';
        testOppty.Type = 'New Client';
        testOppty.AccountId = a1.Id;
        testOppty.StageName = 'Identify';
        //Request Id 12638 Commenting step
        //testOppty.Step__c = 'Identified Deal';
        //testOppty.Step__c = 'Identified Deal';
        //testOppty.StageName = 'Selected';
        //Updated as part of 5166(july 2015)
        oppOffices_List = new List<String>();               
      //  officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
       // if(officesToBeExcluded.StrValue__c != null){        
    //    oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        testOppty.Opportunity_Office__c ='Houston - Dallas';
      //  }
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Market__c = 'ASEAN';
        testOppty.Close_Stage_Reason__c = 'Other' ;
         testOppty.Buyer__c=c.id;
        oppList.add(testOppty);
        
         // Create Opportunity record with Stage = Closed / Won
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOpptyClosedWon';
       // testOppty1.Type = 'New Client';
        testOppty1.Type = 'Win';       
        testOppty1.AccountId = a1.Id;
        testOppty1.StageName = 'Closed / Won';
       // testOppty1.Step__c = 'Identified Deal';
        //Request Id 12638 Commenting step
        //testOppty1.Step__c = 'Closed / Won (SOW Signed)';
        //testOppty1.StageName = 'Closed / Won';
        //Updated as part of 5166(july 2015)
        oppOffices_List = new List<String>();               
     //   officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
    //    if(officesToBeExcluded.StrValue__c != null){        
    //    oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        testOppty1.Opportunity_Office__c = 'Houston - Dallas';
    //    }
        testOppty1.CloseDate = date.Today();
        testOppty1.CurrencyIsoCode = 'ALL';
        testOppty1.Opportunity_Market__c = 'ASEAN';
        testOppty1.Amount = 23.52;
        testOppty1.Close_Stage_Reason__c = 'Other' ;
        testOppty1.Buyer__c=c.id;
        oppList.add(testOppty1);

      //  insert opportunity List
       //test.startTest();
       insert oppList;
     // test.stopTest();
    
        // Create PriceBook Record
        Pricebook2 pbOld = new Pricebook2();
        pbOld.Name = 'Test Pricebook';
        Insert pbOld;
        
        
        Pricebook2 pb = new Pricebook2();
        pb.Id = Test.getStandardPricebookId();
        Update pb;
        
         // Create Product2 Record
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
       
        test.starttest();
         
       // Create PricebookEntry Record
       PricebookEntry pbEntry = new PricebookEntry();
       pbEntry.Product2Id = pro.Id;
       pbEntry.UnitPrice = 220.30;
       pbEntry.Pricebook2Id = pbOld.Id ;
       pbEntry.CurrencyIsoCode = 'ALL';
       pbEntry.IsActive = true;
       insert pbEntry;
       
        OpportunityLineItem opptylineItemNew = new OpportunityLineItem();
        opptylineItemNew.OpportunityId = oppList[4].Id;
        opptylineItemNew.PricebookEntryId = pbEntry.Id;
        opptyLineItemNew.Revenue_End_Date__c = date.Today()+21;
        opptyLineItemNew.Revenue_Start_Date__c = date.Today()+20;
        opptyLineItemNew.UnitPrice = 1.00;
        opptyLineItemNew.CurrentYearRevenue_edit__c = 1; 
        olList.add(opptyLineItemNew);
        
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = oppList[3].Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+21;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
        opptylineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1; 
        olList.add(opptyLineItem);
        
        OpportunityLineItem opptylineItem1 = new OpportunityLineItem();
        opptylineItem1.OpportunityId = oppList[2].Id;
        opptylineItem1.PricebookEntryId = pbEntry.Id;
        opptyLineItem1.Revenue_End_Date__c = date.Today()+21;
        opptyLineItem1.Revenue_Start_Date__c = date.Today()+20; 
        opptylineItem1.UnitPrice = 1.00;
        opptyLineItem1.CurrentYearRevenue_edit__c = 1; 
        olList.add(opptyLineItem1);
        
        OpportunityLineItem opptylineItem2 = new OpportunityLineItem();
        opptylineItem2.OpportunityId = oppList[1].Id;
        opptylineItem2.PricebookEntryId = pbEntry.Id;
        opptyLineItem2.Revenue_End_Date__c = date.Today()+21;
        opptyLineItem2.Revenue_Start_Date__c = date.Today()+20;
        opptylineItem2.UnitPrice = 1.00;
        opptyLineItem2.CurrentYearRevenue_edit__c = 1;  
        olList.add(opptyLineItem2);
        
        

        insert olList;
        
        oppList[1].StageName = 'Active Pursuit';
        //Request Id 12638 Commenting step
        //oppList[1].Step__c = 'Identified Deal';
        oppList[2].StageName = 'Finalist';
        //oppList[2].Step__c = 'Identified Deal';
        oppList[3].StageName = 'Selected';
        //oppList[3].Step__c = 'Identified Deal';
        oppList[4].StageName = 'Closed / Won';
        //oppList[4].Step__c = 'Closed / Won (SOW Signed)';
     
        try{update oppList; }
        catch(Exception ex) {
         }
        test.stoptest();

    }


 /*
  * @Description : This test method provides data coverage to all methods of PDFReportGenerator class          
  * @ Args       : no arguments        
  * @ Return     : void       
  */ 
 static testMethod void testgetAccountRevenueDetails()
    {
       
          List<String> accIdList = new List<String>();
          
        // Create Contact Record
                        
         
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        test.startTest();
            PageReference pg = Page.GUAccountProfile;
            Test.setCurrentPage(pg); 
            ApexPages.currentPage().getParameters().put('onecode','ARFB12');
            ApexPages.currentPage().getParameters().put('guduns','1256');
           ApexPages.currentPage().getParameters().put('closeddate','2/13/2017');
            PDFReportGenerator obj = new PDFReportGenerator();
         
            
            obj.cyYear = 2013;
            Id AccId = [Select Id from Account where Name = 'Test1'].Id;
            accIdList.add(AccId);
           
           //obj.oneCodeId = 'ARFB12';
          // obj.guDUNSId = '';
            obj.Closed_Not_Won_Opportunities_by_Sub_Stage(accIdList);
        }
        test.stopTest();

    } 
}