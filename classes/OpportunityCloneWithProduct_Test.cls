/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData=True)
private class OpportunityCloneWithProduct_Test {

    static testMethod void method1(){
        Test.startTest();
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            insert testAccount;
                
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            AP44_ChatterFeedReporting.FROMFEED =true;
            insert testOppty1;
            
             Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        insert opptylineItem;
            
            //ApexPages.currentPage().getParameters().put('id',testOppty1.Id);
            
            ApexPages.currentPage().getParameters().put('isCloned','True');
            //ApexPages.currentPage().getParameters().put('isexpansion','True');
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
            OpportunityCloneWithProduct cntrl=new OpportunityCloneWithProduct(stdController);
            cntrl.isCloned='True';
            /*************Replacing getstepVal() with getStageVal() START**************/
            //cntrl.getstepVal();
            cntrl.getStageVal();
            /*************Replacing getstepVal() with getStageVal() END**************/
            cntrl.getcloseStageReasonVal();
            cntrl.getChargeCodeExepVal();
            try{
               cntrl.saveClone(); 
            }catch(Exception e){
                System.debug('ecsad**+'+e);
            }
            
            cntrl.doCancel();
            cntrl.insertOppLineItem(testOppty1.Id,true);
            cntrl.ReallocateRevenue(opptylineItem);
         }
         
         Test.stopTest();
    }
    static testMethod void method2(){
        

        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            testAccount.One_Code__c='Test1';
            insert testAccount;
            
            Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'absa2c@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
                
       Opportunity testOppty1 = new Opportunity();
  

            
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            testOppty1.Buyer__c=testContact.id;
            Test.startTest();
            insert testOppty1;
            Test.stopTest();
             
            
            //ApexPages.currentPage().getParameters().put('id',testOppty1.Id);
            
            
            ApexPages.currentPage().getParameters().put('isexpansion','True');
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
            
            
            OpportunityCloneWithProduct cntrl=new OpportunityCloneWithProduct(stdController);
            cntrl.newOppty.Opportunity_Office__c = 'Shanghai - Huai Hai';
            
            
            cntrl.saveClone(); 
            cntrl.cloneSalesCredit(testOppty1.id);
            cntrl.cloneOppteamMember(testOppty1);
            OpportunityCloneWithProduct.expansionOpportunity(testOppty1.id);
            OpportunityCloneWithProduct.getProdStatusCount(testOppty1.id);
            
            
         }


    }     
    
    
    
}