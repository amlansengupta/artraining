/* Purpose: Query Colleague id and Proudct id based on information from WebCAS and associate them to look up fields in Project object
===================================================================================================================================
 VERSION     AUTHOR             DATE             DETAIL 
 1.0         Jagan              1st April 2015   PMO Req#5568 ,2015April Release : Class to call from the "Project" trigger
 2.0         Jyotsna            8th May 2015     PMO Req#5568, June Release : Updated the code to remove hardcoded Url and other parmas, Updated the format of Chatter post.
 3.0         Bala               20th August 2015   PMO Req#5568, September Release: Updated code to check for active chargeable products
 4.0         Bala               9th June 2016    PMO Req#8676, June Release: Updated code for Status field from 'Approved' to 'Active' updated code not to trigger Email alert for closed/won Opportunity for specific talent product as per the request.
 5.0		 Trisha				18th Jan 2019	 Req#12638,Replacing step with stage
======================================================================================================================================
*/
global without sharing class AP125_ProjectTriggerClass {
    
    public static final string STR_PROJECTSTATUS = 'Active';
    public static final string STR_MESSAGE = 'Congratulations on winning your opportunity.\''; 
    public static final string STR_MESSAGE1 ='Please ensure your project team record their time to the correct project.';
    public static final string STR_MESSAGE2 ='\' has been linked to project(s) and has automatically closed as won.';
    public static final string STR_AP125 = 'AP125_ProjectTriggerClass';
    public static final string STR_AP02 = 'AP02_OpportunityTriggerUtil';
    public static final string STR_ClosedWon = 'Closed / Won (SOW Signed)';
    //request id:12638 adding stagevalue for 'Closed / Won (SOW Signed)' step value 
    public static final string STR_StageClosedWon = System.Label.CL48_OppStageClosedWon;
    public static final string STR_PendingChareableCode = 'Pending Chargeable Code';
    //request id:12638 adding stagevalue for 'Pending Chargeable Code' step value 
    public static final string STR_PendingProject = System.Label.Stage_Pending_Project ;
    public static final string STR_S = 'S';  
    public static final string STR_T = 'T';
    public static final string STR_C = 'C';
    public static final string STR_R = 'R';
    public static Map<String, String> errorMap = new Map<String, String>();
    
    /**
     * Method to query Colleague id and Proudct id 
     * based on information from WebCAS and associate them to look up fields in Project object
     */
    public static void updateColleagueandProudct(List<SObject> triggernew){
        
        Set<String> empnoSet = new Set<String>();
        set<String> prodcodeset = new set<String>();
        Map<String,id> collMap = new Map<String,id>();
        Map<String,id> prodMap = new Map<String,id>();
        Map<String,OpportunityLineItem> oppprodMap = new Map<String,OpportunityLineItem>();
        List<Colleague__c> lstColl = new List<Colleague__c>();
        LIst<product2> lstprod = new List<Product2>();
        Set<String> setProdId = new Set<String>();
        List<opportunitylineitem> lstoppprod = new List<opportunitylineitem>();
        List<Id> lstOpp = new List<Id>();
        Map<Id, Id> mapRevOpp = new Map<Id, Id>();
        
        try {
            //Loop around trigger.new and take the project manager and project solution values in to two sets
            for (SObject obj:triggernew){
        Revenue_Connectivity__c rev=(Revenue_Connectivity__c)obj;    //typecasting as per interface change
                if(rev.Project_Manager__c <> null)
                    empnoSet.add(rev.Project_Manager__c);
                
                if(rev.Project_Solution__c <> null)    
                    prodcodeset.add(rev.Project_Solution__c);
                    
                if(rev.Opportunity_Product_Id__c <> null)
                    setProdId.add(rev.Opportunity_Product_Id__c);
                
               
            }     
            
            System.debug('Debug125..1..setProdId..'+setProdId +'.prodcodeset..'+prodcodeset+'.empnoSet..'+empnoSet);
            //Query from colleague and project objects
            if(empnoset.size() > 0)
                lstColl = [select id,emplid__c from Colleague__c where emplid__c in :empnoset];
                
            if(prodcodeset.size()>0)
                lstprod = [select id,isActive, ProductCode from Product2 where isActive = true and productcode in :prodcodeset];
            
            if(setProdId.size()>0)
                lstoppprod = [select id,opportunityid,opportunity.name,opportunity.ownerId from opportunitylineitem where id in:setProdId];
            System.debug('Debug125..1..lstoppprod ..'+lstoppprod +'.lstprod ..'+lstprod +'.lstColl ..'+lstColl );
            //Loop around colleague and product objects and put the values in map    
            for(Colleague__c c:lstColl){ collMap.put(c.emplid__c, c.id); }
            
            for(product2 p: lstprod){
                prodMap.put(p.productcode, p.id);
            }
            
            for(opportunitylineitem oppprod:lstoppprod){
                String oppid = oppprod.id;
                oppprodMap.put(oppid.subString(0,15), oppprod);
            }
             System.debug('Debug125..3..oppprodMap..'+oppprodMap +'.prodMap..'+prodMap+'.collMap..'+collMap);    
             System.debug('Debug125..4..'+mapRevOpp);
            //use the values in map to assign the ids to the look up fields in project object.    
            for (SObject obj:triggernew){        
                Revenue_Connectivity__c rev=(Revenue_Connectivity__c)obj;
                if(rev.Project_Manager__c <> null && !collMap.isEmpty() && collMap.containsKey(rev.Project_Manager__c)){rev.Colleague__c = collMap.get(rev.Project_Manager__c);}
                
                if(rev.Project_Solution__c <> null && !prodMap.isEmpty() && prodMap.containsKey(rev.Project_Solution__c))    
                    rev.Product__c = prodMap.get(rev.Project_Solution__c);
                             

            }
         if(test.isRunningTest()){
            integer i=null;
            i=i+0;
        }
        }

        catch(Exception e){
            system.debug('Exception '+e.getmessage());
        }
        
    }
    
    
    /*Public static void closeCheckonProductProject(List<Revenue_Connectivity__c> triggernew, Map<Id, Revenue_Connectivity__c> triggernewMap){
        List<Id> lstOpp = new List<Id>();
        Map<Id, Id> mapRevOpp = new Map<Id, Id>();
       
        for (Revenue_Connectivity__c rev:triggernew){        
          
            lstOpp.add(rev.Opportunity__c);
            mapRevOpp.put(rev.Opportunity__c, rev.Id);

        }

        Map<Id, Opportunity> OppQueryMap = new Map<Id, Opportunity>([Select Id, Step__c,Opportunity_Office__c, Ownerid, Name from Opportunity where Id IN: lstOpp]);
        if(lstOpp.size()>0) {
           updateOpportunityStep(OppQueryMap, STR_AP125); 
        }
        for(String error : errorMap.keySet()){ triggernewMap.get(mapRevOpp.get(error)).addError(errorMap.get(error));
        }    
     }*/
     
    Public static void updateOpportunityStep(Map<Id, Opportunity> OppMap, String calledFrom){
        if(System.Label.RevenueConnectivitySwitch == ConstantsUtility.STR_TRUE){
            List<Id> oppIdList = new List<Id>();
            String ownerId = null;
            String recordId = null;
            String opportunityName = null;
            List<Id> prodID = new List<Id>();
            List<Opportunity> lstoppupdate = New List<Opportunity>(); 
            Boolean productProject_Present = false;
            Boolean letCloseOpp = false;
            Integer oppProdCount = 0;
            Integer oppProjectCount = 0 ;
            
            Set<String> productCodeList=new Set<String>();
            Set<String> OneCodeList=new Set<String>(); 
            Set<String> AccountIdList = new Set<String>();
            Boolean AccountExcep=false;
            Boolean OppRecTypeExcep=false;
            Boolean OppOfficeExcep=false;
            Boolean TypeLOBExcep=false;
            boolean oppRecordException=false;
            
                oppIdList.addAll(OppMap.keyset());
                Map<Id, Opportunity> relatedOpportunityMap = OppMap;
                List<OpportunityLineItem> oppProductsList = [select id,OpportunityID,Product2ID,Product2.Exempted_Office__c,Product2.ProductCode,Opportunity.AccountId,LOB__c,Project_Linked__c,is_Project_Required__c from OpportunityLineItem where OpportunityID IN :oppIdList];
                for(OpportunityLineItem oline : oppProductsList ){
                    prodID.add(oline.Product2ID);
                   // AccountIdList.add(oline.Opportunity.AccountId);
                    productCodeList.add(oline.Product2.ProductCode);
                }
                
               // List<Account>  accList=[Select Id, one_code__c from Account where Id In:AccountIdList];
                
                /*for(account acc:accList){
                
                    OneCodeList.add(acc.one_code__c);
                }*/
                
                /*Map<String, String> ExceptionMap = new Map<String, String>();
                List<Project_Exception__c> exceptionLst=[Select Exception_Type__c,Product_Code__c,One_Code__c,Office__c,Record_Type_Name__c,LOB__c,Opportunity_Type__c from Project_Exception__c where One_Code__c IN:OneCodeList OR Office__c !=null OR Record_Type_Name__c!=null OR Opportunity_Type__c!=null OR LOB__c!=null];
                if(exceptionLst.size()>0){
                    
                    for(Project_Exception__c exLst:exceptionLst){    
                        
                        if(exLst.One_Code__c!=null){
                            ExceptionMap.put(exLst.Exception_Type__c,exLst.One_Code__c);
                        }
                        if(exLst.Product_Code__c==null && exLst.Office__c!=null){
                            ExceptionMap.put(exLst.Exception_Type__c,exLst.Office__c);
                        }
                        if(exLst.Record_Type_Name__c!=null){
                            ExceptionMap.put(exLst.Exception_Type__c,exLst.Record_Type_Name__c);
                        }
                        if(exLst.LOB__c!=null){
                            ExceptionMap.put(exLst.Exception_Type__c,exLst.LOB__c);
                        }
                        if(exLst.Opportunity_Type__c!=null){
                            ExceptionMap.put(exLst.Exception_Type__c,exLst.Opportunity_Type__c);
                        }
                    
                    
                    }
                }*/
                
                
                system.debug('ProdIdCheck-'+prodID.size());
                //if(prodID.size()>0){
                /*List<Product2> prodExcep = [Select id,Product_Exception__c,Exception__c,Exempted_Office__c from Product2 where id IN :prodID];
                
                Map<Id, Boolean> prodExceptionMap = new Map<Id, Boolean>();
                Map<Id, Product2> offProdExceptionMap = new Map<Id, Product2>();
                for(Product2 pr : prodExcep){
                    System.debug('pr.Exempted_Office__c****'+pr.Exempted_Office__c);
                    System.debug('prodExceptionMap****'+prodExceptionMap);
                    
                     if(pr.Product_Exception__c==True){
                        prodExceptionMap.put(pr.Id, pr.Product_Exception__c );
                        System.debug('prodExceptionMap****'+prodExceptionMap.size());
                        System.debug('prodExceptionMapindise****'+prodExceptionMap);
                    }else if(pr.Exception__c==True && pr.Exempted_Office__c!=null){
                        offProdExceptionMap.put(pr.Id, pr);
                        System.debug('offProdExceptionMap****'+offProdExceptionMap.size());
                        System.debug('offProdExceptionMapsadad****'+offProdExceptionMap);
                    }
                    
                }*/
                
                Map<Id, List<OpportunityLineItem>> lineItemOpportunityMap = new Map<Id, List<OpportunityLineItem>>();
                Map<Id, List<Revenue_Connectivity__c>> projectOpportunityMap = new Map<Id, List<Revenue_Connectivity__c>>();
                List<OpportunityLineItem> oliList = null ;
                for(OpportunityLineItem oli : oppProductsList){
                    if(lineItemOpportunityMap.get(oli.OpportunityId) == null){
                        oliList = new List<OpportunityLineItem>();
                        oliList.add(oli);
                        lineItemOpportunityMap.put(oli.OpportunityId,oliList);
                    }else{
                        lineItemOpportunityMap.get(oli.OpportunityId).add(oli);
                    }
                }
                System.debug('lineItemOpportunityMap Vala****'+lineItemOpportunityMap);
                System.debug('lineItemOpportunityMap****'+lineItemOpportunityMap.size());
                List<Revenue_Connectivity__c > revList = null ; 
                /*for(Revenue_Connectivity__c rev : [Select Id, Opportunity__c, Project_Status__c, Project_LOB_Formula__c, Charge_Basis__c from Revenue_Connectivity__c Where Opportunity__c IN :oppIdList] ){
                    if(projectOpportunityMap.get(rev.Opportunity__c) == null){
                        revList = new List<Revenue_Connectivity__c >();
                        revList.add(rev);
                        projectOpportunityMap.put(rev.Opportunity__c,revList);
                    }else{
                        projectOpportunityMap.get(rev.Opportunity__c).add(rev);
                    }
                }*/
                
                /*Map<Id,RecordType> rtMap=new Map<Id,RecordType>([Select Id, Name From RecordType Where SObjectType = 'Opportunity' ]);
                for(RecordType r:rtMap.values()){
                    if(ExceptionMap.ContainsKey('RecordType Name') && ExceptionMap.get('Opportunity Type')==r.name){
                       //OppRecTypeExcep=true;
                       oppRecordException=true;
                       break;
                    }  
                
                }*/
                /*List<Opportunity> OppList=[Select id,recordType.Name,Opportunity_Office__c,Type from Opportunity where id in:oppMap.keyset()];
                for(Opportunity ops:OppList){
                
                    for(String a:OneCodeList){
                       if(ExceptionMap.ContainsKey('One Code') && ExceptionMap.get('One Code')==a){
                           //AccountExcep=true;
                           oppRecordException=true;
                           break;
                       }
                    }
                    if(ExceptionMap.ContainsKey('Office') && ExceptionMap.get('Office')==ops.Opportunity_Office__c){
                       //OppOfficeExcep=true;
                       oppRecordException=true;
                           break;
                    }
                    if(ExceptionMap.ContainsKey('Opportunity Type') && ExceptionMap.get('Opportunity Type')==ops.Type){
                        
                        for(OpportunityLineItem oli : lineItemOpportunityMap.get(ops.id)){
                            if(ExceptionMap.ContainsKey('Product LOB') && ExceptionMap.get('Product LOB')!=oli.LOB__c){
                                //TypeLOBExcep=true;
                                oppRecordException=true;
                                //break;
                            }else if(ExceptionMap.ContainsKey('Product LOB') && ExceptionMap.get('Product LOB')==oli.LOB__c){
                            
                                if(oppRecordException!=True){
                                    oppRecordException=false;
                                     break;
                                }
                            }
                        }
                    
                    }
                    System.debug('recordtype3443****'+ops.recordtype.name);
                    if(ExceptionMap.ContainsKey('RecordType Name') && ExceptionMap.get('RecordType Name')==ops.recordtype.name){
                       //OppOfficeExcep=true;
                       System.debug('recordtype****');
                       oppRecordException=true;
                           break;
                    }
                    System.debug('oppRecordException********'+oppRecordException);
                }*/
               List<String> exmptOffValuesList=new List<String>();
               Set<String> exmptOffValuesSet=new Set<String>();
                for(Opportunity updoppId : OppMap.values()){
                    System.debug('updoppId ****'+updoppId);
                    oppProdCount = 0;
                    //try {
                    System.debug('calledFrom****'+calledFrom);
                    System.debug('STR_AP125****'+STR_AP125);
                    //request id:12638, replacing step with stage in below if condition
                       // if((calledFrom == STR_AP125 ||calledFrom == STR_AP02) && relatedOpportunityMap.get(updoppId.id).Step__c == STR_PendingChareableCode){
                          if((calledFrom == STR_AP125 ||calledFrom == STR_AP02) && relatedOpportunityMap.get(updoppId.id).stageName== STR_PendingProject){
                           System.debug('first condn satisfy****');
                            if(lineItemOpportunityMap.get(updoppId.id) != null){
                                System.debug('lineItemOpportunityMap2222****'+lineItemOpportunityMap.size());
                                for(OpportunityLineItem oli : lineItemOpportunityMap.get(updoppId.id)){
                                   System.debug('oli inside loop***************'+oli);
                                   /*if(oli.Product2.Exempted_Office__c!=null){
                                       exmptOffValuesList= offProdExceptionMap.get(oli.Product2Id).Exempted_Office__c.trim().split(';');
                                       
                                        exmptOffValuesSet.addAll(exmptOffValuesList);
                                   }*/
                                   
                                    //if(!( oppRecordException || ((prodExceptionMap.ContainsKey(oli.Product2Id) && prodExceptionMap.get(oli.Product2Id)) || (offProdExceptionMap.ContainsKey(oli.Product2Id) && offProdExceptionMap.get(oli.Product2Id).Exception__c==true && offProdExceptionMap.get(oli.Product2Id).Exempted_Office__c!=null && exmptOffValuesSet.contains(updoppId.Opportunity_Office__c))))){//offProdExceptionMap.get(oli.Product2Id).Exempted_Office__c==updoppId.Opportunity_Office__c)))){
                                      if(oli.is_Project_Required__c){
                                        oppProdCount ++;
                                        System.debug('oli.id***************'+oli);
                                        System.debug('oli.Product2Id***************'+oli.Product2Id);
                                        //if(projectOpportunityMap.get(oli.OpportunityId) != null){
                                            //for(Revenue_Connectivity__c rev: projectOpportunityMap.get(oli.OpportunityId)){
                                                
                                                if(oli.Project_Linked__c==True){ 
                                                    productProject_Present = true;
                                                    
                                                    //break;
                                                }else{
                                                    productProject_Present = false;  
                                                }
                                           // }
                                        //}
                                        if(productProject_Present){ 
                                            letCloseOpp = true;
                                        }
                                        else{
                                            letCloseOpp = false;
                                        }
                                    }else{
                                        System.debug('oli elseeoppProdCount***************'+oppProdCount);
                                        oppProdCount++;
                                        letCloseOpp = true;
                                    }
                                    if(letCloseOpp && lineItemOpportunityMap.get(updoppId.id).size() == oppProdCount && (calledFrom == STR_AP125 ||calledFrom == STR_AP02)){
                                        Opportunity opp= relatedOpportunityMap.get(updoppId.id);
                                        /****request id:12638 replacing below assignment from step to stage start *****/
                                        //opp.Step__c = STR_ClosedWon;
                                        opp.stageName = STR_StageClosedWon;
                                        /****request id:12638 replacing below assignment from step to stage end *****/
                                        opp.No_Email_Alert__c=true;
                                        lstoppupdate.add(opp);
                                        ownerId=opp.Ownerid;
                                        recordId=opp.id;
                                        opportunityName=opp.Name;
                                        System.debug('chatterPost***************');
                                        chatterPost(ownerId,opportunityName,STR_PROJECTSTATUS,recordId,calledFrom);    
                                        break;   
                                    }else{
                                        if(!letCloseOpp){
                                            if(calledFrom == STR_AP02){ 
                                            /****request id:12638, replacing step with corresponding stage start****/
                                                //OppMap.get(updoppId.id).step__c = STR_PendingChareableCode; 
                                                OppMap.get(updoppId.id).stageName= STR_PendingProject; 
                                            /****request id:12638, replacing step with corresponding stage end****/
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                                    
                /*}catch(Exception e){
                    if(calledFrom == STR_AP02){ 
                    System.debug('Excptionnasda********'+e);
                    AP02_OpportunityTriggerUtil.errorMap.put(updoppId.id, e.getmessage());
                    }
                } */
            }    
            
            if(lstoppupdate.size()>0  && calledFrom == STR_AP125 ) {
                Database.saveresult[] sr = database.update(lstoppupdate);
                for (Database.SaveResult sresult : sr){
                  for (Database.Error err : sresult.getErrors()){
                    if(calledFrom == STR_AP02){ AP02_OpportunityTriggerUtil.errorMap.put(sresult.getId() , err.getMessage());
                    }
                    if(calledFrom == STR_AP125){ errorMap.put(sresult.getId(), err.getMessage());
                    }
                  }
                }
            }
        }
      //} 
    }
        
    
    /**Req #5568: 
     * Method to post a chatter notification to Opportunity Owners.
     */
    @future (callout = true)
    global static void chatterPost(String recipientId, String OppName, String Status, String recordid, String calledFrom){
        String message = '';
        String messagelink = constantsutility.STR_SINGLESPACE + STR_MESSAGE+ constantsutility.STR_SINGLESPACE + OppName + '  ' + constantsutility.STR_SINGLESPACE +STR_MESSAGE2 + constantsutility.STR_SINGLESPACE+ STR_MESSAGE1;
        String messagenotlink = ' Congratulations on winning your opportunity ' + OppName + '. Your opportunity has now closed as won. Please ensure your project team record their time to the correct project.';
        if(calledFrom == STR_AP02){
            message = messagenotlink;
        }else{
            message = messagelink;
        }
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = recipientId;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        List<ConnectApi.MessageSegmentInput> segmentList = new List<ConnectApi.MessageSegmentInput>();
        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = message;
        segmentList.add((ConnectApi.MessageSegmentInput)textSegment);
        messageBodyInput.messageSegments.addAll(segmentList);

        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = recordid;
        if ( !Test.isRunningTest() ){ ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);   }          
    }
}