/*
 * Class for handling web service exception
 */
public without sharing class WebServiceException extends Exception{
}