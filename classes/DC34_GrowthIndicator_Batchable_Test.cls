/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DC34_GrowthIndicator_Batchable_Test {

 private static Mercer_TestData mtdata = new Mercer_TestData();
  /*
     * @Description : Test method to provide test coverage to DC34_GrowthIndicator_Batchable class
     * @ Args       : null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            Colleague__c collg = mtdata.buildColleague();
            Account acct = mtdata.buildAccount();
            
            FCST_Fiscal_Year_List__c objFFYL = new FCST_Fiscal_Year_List__c();
            objFFYL.EndDate__c = System.today() + 5;
            objFFYL.StartDate__c = System.today() - 5;
            insert objFFYL;
            
            Growth_Plan__c gplan1 = new Growth_Plan__c();
            gplan1.Account__c = acct.id;
            gplan1.Planning_Year__c ='2014';
            gplan1.EH_B_Recurring_and_Carry_forward_Revenue__c = 1;
            gplan1.EH_B_Projected_New_Sales_Revenue__c =2;
            gplan1.EH_B_Projected_Full_Year_Revenue__c = 4;
            gplan1.EH_B_Projected_Current_Year_Revenue__c = 8;
            gplan1.fcstPlanning_Year__c = objFFYL.Id;
            gplan1.TOT_of_Revenue_Growth_Anticipated01__c = 25.4;
            gplan1.Growth_Indicator__c =  null;
            insert gplan1;
            
                                                
            
            Test.startTest();
                database.executeBatch(new DC34_GrowthIndicator_Batchable(), 500);
            Test.stopTest();
        }
    }
}