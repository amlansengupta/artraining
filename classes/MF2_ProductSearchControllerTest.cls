/*****************************************************************************************************************************
User story : 17673 (epic)
Purpose : This is a test class for MF2_ProductSearchController.
Created by : Soumil Dasgupta
Created Date : 29/03/2019
Project : MF2
******************************************************************************************************************************/

@isTest
global class MF2_ProductSearchControllerTest {
    public static ID opptyId;
    static testMethod void testProductSearch()
    {   
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
        /* Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
insert customPB;*/
        Id pricebookId = Test.getStandardPricebookId(); 
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        string pbid = customPB.id;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.ProductCode = '12345';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        pro.Segment__c='International Consulting Group';
        pro.Cluster__c='Legal and Compliance';
        pro.Sub_Business__c='Global Consulting';
        pro.LOB__c='Global Business Solutions';
        insert pro;
        Id proId = pro.Id;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = proId,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.Step__c='Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        testOppty.PriceBook2Id = pbid;
        
        testOppty.AccountId=testAccount.Id;
        
        insert testOppty;
        
        opptyId  = testOppty.id;
        Current_Exchange_Rate__c cer = new Current_Exchange_Rate__c();
        cer.Name='test oppty';
        cer.Conversion_Rate__c=20;
        insert cer;
        
        /*PricebookEntry customPrice = new PricebookEntry(
Pricebook2Id = pbid, Product2Id = pro.Id,
UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
insert customPrice;*/
        ID pbEId = customPrice.Id;
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opptyId;
        oli.PricebookEntryId = pbEId;
        oli.Product2Id = proId;
        oli.UnitPrice = 1.00;
        oli.Sales_Price_USD_calc__c = 1.00;
        oli.CurrentYearRevenue_edit__c = 1.00;
        oli.Year2Revenue_edit__c = 0;
        oli.Year3Revenue_edit__c = 0;
        oli.Revenue_Start_Date__c = Date.newInstance(2019, 04, 22);
        oli.Revenue_End_Date__c = Date.newInstance(2019, 04, 30);
        insert oli;
        Id oliId = oli.Id;
        
        MF2_ProductSearchController.productFieldsInitialize(opptyId);
         MF2_ProductSearchController.fetchOLI(oliId);
        MF2_ProductSearchController.fetchOpportunity(opptyId);
        //MF2_ProductSearchController.fetchCurrEx(opptyId);
        MF2_ProductSearchController.fetchCurrEx();
        MF2_ProductSearchController.fetchDigitalPicklist();
        try{
            MF2_ProductSearchController.fetchPbEntry('TestProd', '12345', opptyId, pro.LOB__c, pro.Sub_Business__c, pro.Segment__c, pro.Cluster__c);
        }catch(Exception ex){
            
        }
        MF2_ProductSearchController.reallocateRevenue(1500, Date.newInstance(2019, 04, 22), Date.newInstance(2019, 04, 26));
        MF2_ProductSearchController.reallocateRevenue(1500, Date.newInstance(2019, 04, 22), Date.newInstance(2021, 04, 26));
        MF2_ProductSearchController.reallocateRevenue(1500, Date.newInstance(2020, 04, 22), Date.newInstance(2021, 04, 26));
        //MF2_ProductSearchController.fetchFavourites(opptyId);
        OpportunityLineItem oliresult = MF2_ProductSearchController.fetchOLI(oliId);
        system.assertEquals(opptyId,oliresult.OpportunityId);
        
        
        
        Test.startTest();
        
        //For MF2_ProductEditExt;
        ApexPages.StandardController sc = new ApexPages.standardController(oli);
        MF2_ProductEditExt controller = new MF2_ProductEditExt(sc); 
        
        Test.setMock(HttpCalloutMock.class, new FavoriteUtilityMock()); 
        try{
            MF2_ProductSearchController.fetchFavourites(opptyId,UserInfo.getSessionId());
            FavoriteUtility.FavoriteEntity fav = new FavoriteUtility.FavoriteEntity('abc', oliID);
        }catch(Exception ex){}
        //FavoriteUtility.fetchFavorites();
        Test.stopTest();       
    }
    
    /*static testMethod void testPostCallout() {
Test.setMock(HttpCalloutMock.class, new FavoriteUtilityMock()); 
MF2_ProductSearchController.fetchFavourites(opptyId);
FavoriteUtility.fetchFavorites();
}*/
    
    
    global class FavoriteUtilityMock implements HttpCalloutMock {
        
        global HTTPResponse respond(HTTPRequest request) {
            
            HttpResponse response = new HttpResponse();
            
            response.setHeader('Content-Type', 'application/json');
            
            response.setBody('{"favorites" : [ {"targetType" : "Record","target" : "01t4400000AlEADAA3","subtitle" : "Product","sortOrder" : 1,"objectType" : "Product2","name" : "DB investment consulting","lastAccessDate" : "2019-04-01T13:10:16.000Z","id" : "0MV0m0000008PDQGA2","iconUrl" : "https://mercer--LtngDEV.cs65.my.salesforce.com/img/icon/t4v35/standard/product_120.png","iconColor" : "B781D3","accessCount" : 1} ]}');
            
            response.setStatusCode(200);
            
            return response;
            
        }
        
    }
    
    
    
}