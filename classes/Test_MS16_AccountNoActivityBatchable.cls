/*Purpose: Test Class for providing code coverage to MS16_AccountNoActivityBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = false)
private class Test_MS16_AccountNoActivityBatchable  {
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
         /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    	Request No. 17953 : Removing Finalist END */ 
    }
    /*
     * @Description : Test method to provide data coverage to MS16_AccountNoActivityBatchable batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
        createCS();
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '9876543210';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        Account Acc = new Account();
        Acc.Name = 'TestAccount';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.MercerForce_Account_Status__c = 'No Activity';
        Acc.Competitor_Flag__c = true;
        insert Acc;
        
        Opportunity opp= new Opportunity();
        opp.Name = 'Test Opportunity1' + String.valueOf(Date.Today());
        opp.Type = 'New Client';
        //Request id:12638 commenting step
        //opp.Step__c = 'Identified Deal';
        opp.StageName = 'Closed / Won';
        //opp.CloseDate = date.Today()-2;
        opp.CloseDate = date.Today().toStartOfMonth();
        //opp.CreatedDate = date.Today();
        opp.CurrencyIsoCode = 'ALL';
        opp.AccountId = Acc.id;
         opp.Opportunity_Office__c ='Montreal - McGill';
        insert opp; 
        
        Contact Con= new Contact();
        Con.LastName ='TestContactLastName';
        Con.AccountId = Acc.id;
        Con.FirstName ='TestContactFirstName';
        Con.Contact_ID__c = '896754231';
        //Con.createdDate = date.Today();
        Con.email = 'xyzq2@abc14.com';
        insert Con;
        
         //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
        
        Task tas = new Task();
        Tas.subject ='Test';
        Tas.Status = 'Not Started';
        Tas.Priority ='Normal';
        Tas.whatId = Acc.id; 
        Tas.Completion_Date__c = System.today()-1;       
        insert Tas; 
        
        system.runAs(User1){
            database.executeBatch(new MS16_AccountNoActivityBatchable(), 500);
        }
       }
    }