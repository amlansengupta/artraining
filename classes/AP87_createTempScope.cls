/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Takes input Arguments Id's of "Selected Scope it Projects from Opportunity Detail page "
.Inserts data into TempScopeIt Projects with related Tasks and Employees.


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/10/2014  Created this Global class which is being called from button "Save as Template".
   2.0 -    Jagan   03/03/2014  Updated Alignment and added condition to prevent null pointer exception                                 
   3.0 -    Jagan   05/15/2014  Fixed the issue related to Template
   4.0 -    Madhavi 11/25/2014  As part of #4881(Dec 2014 release) Added fields IC_Charge_Type__c,IC_charges__c to the query
============================================================================================================================================== 
*/
Global class AP87_createTempScope{

      /*
     * @Description : Method for creating template for Scopeit Project 
     * @ Args       : list<Id> scpId  
     * @ Return     : string
     */
    webservice static string createTscope(list<Id> scpId){ 
        
        //This Map is used to check product Names against Product id 
        Map<id,product2> aMap= new Map<id,product2>([select id,name from Product2]);
        //Map<id,ScopeIt_Employee__c> empMap= new Map<id,ScopeIt_Employee__c> ();
        string message=ConstantsUtility.STR_Blank;
        string names = null;
        
       
        
        //This set is used to Fetch all templates and store there names inorder to prevent duplicates     
        set<string>nameSet= new set<string>();
        for(Temp_ScopeIt_Project__c t:[select name from Temp_ScopeIt_Project__c]){
            nameSet.add(t.name);
        }
    
    
        
        list<ScopeIt_Project__c> sprList = new list<ScopeIt_Project__c>();
        //As part of #4881(Dec 2014 release) Added fields IC_Charge_Type__c,IC_charges__c to the query   
        sprList=[select id,name,currencyisocode,sales_price__c,LOB__c,Product__c,OpportunityProductLineItem_Id__c,Solution_Segment__c,
                    (select id,Name,task_name__c,ScopeIt_Project__c,Bill_Rate__c,Bill_type01__c, Billable_Expenses__c,Bill_Est_of_Increases__c,IC_Charge_Type__c,IC_Charges__c from ScopeIt_Tasks__r )
                    from ScopeIt_Project__c where id=:scpId];
        
        if(!sprList.isEmpty()){
        
            list<Temp_ScopeIt_Project__c> tspList = new list<Temp_ScopeIt_Project__c>();
            set<id> idSet= new set<id>(); 
            
            for(ScopeIt_Project__c s:sprList){
                 
                 Temp_ScopeIt_Project__c spTemp = new Temp_ScopeIt_Project__c();
                 string tempName = s.name + ConstantsUtility.STR_Template;
                 if((tempName.length() > 80 && nameSet.contains( tempName.substring(0,80))) || (tempName.length() <= 80 && nameSet.contains( tempName) )){
                     Message= ConstantsUtility.STR_Message;
                 }
                 else{
                     string strName=s.name +  ConstantsUtility.STR_Template;
                     
                     if(strName.length()>80)
                         spTemp.name = strName.substring(0,80); 
                     else
                         spTemp.name = strName;
                         
                     if(s.Product__c!=null) {
                         spTemp.Product__c=aMap.get(s.Product__c).name;
                     }
                     spTemp.Product_Id__c=s.Product__c;
                     spTemp.LOB__c=s.LOB__c;
                     spTemp.currencyisocode= s.currencyisocode;
                     spTemp.Solution_Segment__c=s.Solution_Segment__c;
                     spTemp.Sales_Price__c = s.sales_Price__c;
                     tspList.add(spTemp);
                     //spTemp.OpportunityProductLineItem_Id__c=s.OpportunityProductLineItem_Id__c;
                    
                     try {
                         database.insert(tspList,false);
                         if(!tspList.isEmpty()){
                             for(Temp_ScopeIt_Project__c sn:tspList){
                                 idSet.add(sn.id);
                                 system.debug('Id of the template project..... '+sn.id);
                             }
                             names='';
                             for(Temp_ScopeIt_Project__c sname:[select name from Temp_ScopeIt_Project__c where id in:idSet]){
                                 names+=sname.name;
                             }
                         }
                         message=ConstantsUtility.STR_TemplateMessage +names+'"';
                     }
                     catch (exception ex){  Message+='Please contact Administrator '; }
                     
                    
                     if(!s.ScopeIt_Tasks__r.isEmpty()){
                     
                         List<Temp_ScopeIt_Employee__c> LstTempScopEmp = new List<Temp_ScopeIt_Employee__c>();
                         
                         for( ScopeIt_Task__c st:s.ScopeIt_Tasks__r){
                     
                             Temp_ScopeIt_Task__c tst= new Temp_ScopeIt_Task__c();
                             tst.Task_Name__c = st.task_name__c;
                             tst.Billable_Expenses__c=st.Billable_Expenses__c;
                             tst.Bill_Rate__c=st.Bill_Rate__c;
                             tst.Temp_ScopeIt_Project__c=spTemp.id;
                             tst.currencyisocode = s.currencyisocode;
                             tst.bill_type__c = st.bill_Type01__c;
                             tst.Bill_Est_of_Increases__c = st.Bill_Est_of_Increases__c;
                             tst.IC_Charges__c = st.IC_Charges__c;//Added as part of #4881(Dec 2014 release) 
                             tst.IC_Charge_Type__c = st.IC_Charge_Type__c;//Added as part of #4881(Dec 2014 release) 
                             database.insert(tst);
                             for(ScopeIt_Employee__c se:[select id,Bill_Rate__c,Bill_rate_opp__c,Billable_Time_Charges__c,Business__c,Market_Country__c,Cost__c,Hours__c,Employee_Bill_Rate__c from ScopeIt_Employee__c
                                                where ScopeIt_Task__c=:st.id]){
                    
                                 Temp_ScopeIt_Employee__c tse= new Temp_ScopeIt_Employee__c();
                                 tse.Employee__c = se.Employee_Bill_Rate__c;
                                 tse.Bill_Rate__c=se.Bill_Rate__c;
                                 tse.Billable_Time_Charges__c = se.Billable_Time_Charges__c;
                                 tse.Business__c=se.Business__c;
                                 tse.Cost__c=se.Cost__c;
                                 tse.Hours__c=se.Hours__c;
                                 tse.currencyisocode = s.currencyisocode;
                                 tse.Market_Country__c=se.Market_Country__c;
                                 tse.Temp_ScopeIt_Task__c=tst.id;
                                 tse.ScopeIt_Project_Template__c =  spTemp.id;

                                 //Code updated by Jagan. Instead of inserting here, taking in to list and inserting after the for loop
                                 LstTempScopEmp.add(tse);
                                 //insert tse;            
                             }       
                         }     
                         
                         if(LstTempScopEmp.size()>0) {
                             database.insert(LstTempScopEmp);
                             LstTempScopEmp.clear();
                         }                 
                     }      
                 }
            }
    
        }
        return message;
    }
}