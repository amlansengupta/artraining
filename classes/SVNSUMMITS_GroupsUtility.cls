/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

/*
@Class Name          : SVNSUMMITS_GroupsUtility
@Created by          :
@Description         : Apex Utility class used for creating test records
*/

@isTest
public class SVNSUMMITS_GroupsUtility{

    public static user usr = createUser();

    //create collaboration group
    public static List<CollaborationGroup> createGroup(Integer noOfGroups) {

        List<CollaborationGroup> collGrupLst = new List<CollaborationGroup>();
        for(Integer i=0; i<noOfGroups; i++)
        {
            CollaborationGroup cgroup = new CollaborationGroup();
            cgroup.Name = 'Test Group'+i;
            cgroup.Description = 'Test Description';
            cgroup.collaborationType = 'Private';
            collGrupLst.add(cgroup);
        }

        insert collGrupLst;
        return collGrupLst;
    }

    //create user record
    public static User createUser(){

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

        User u = new User(Alias = 'standt', Email='standarduser123@testorg.com',
        EmailEncodingKey='UTF-8', LastName='stand user', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234@testorg.com');

        insert u;

        return u;
    }
}