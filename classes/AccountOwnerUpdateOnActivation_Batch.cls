global class AccountOwnerUpdateOnActivation_Batch implements Database.Batchable<sObject>{
        
    global String query = Label.AccountOwnerUpdateOnActivation_Batch_Query_Label;
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(Test.isRunningTest()){
            query = 'Select Id,Name, IsActive, EmployeeNumber, Newly_Licensed__c From User Where IsActive = true AND CreatedDate = Today LIMIT 200';            
        }          
        return Database.getQueryLocator(query);
    }
        
    global void execute(Database.BatchableContext BC, List<User> scope){  
        system.debug('$$$scope'+scope.size());
        Map<String,User> userMap = new Map<String,User>();
        Set<Id> colleagueIdSet = new Set<Id>();

        List<Account> accountsToUpdate = new List<Account>();
        List<user> usersToUpdate = new List<User>();
        
        for(User u : scope){
            userMap.put(u.Id,u);
         system.debug('@@@userMap'+u.Name);   
        }        
        for(Colleague__c col : [Select Id, EMPLID__c, User_Record__c From Colleague__c 
                                Where User_Record__c IN : userMap.keySet() ]) 
        {
            system.debug('###Colleague'+col.User_Record__c);
            colleagueIdSet.add(col.Id);
        }
                                 
        for(Account acc: [Select Id, Relationship_Manager__c, Relationship_Manager__r.User_Record__c, OwnerId 
                          From Account Where Relationship_Manager__c IN : colleagueIdSet])
        {
            if(acc.Relationship_Manager__r.User_Record__c != null){
                if(acc.Relationship_Manager__r.User_Record__c != acc.OwnerId){
                    acc.OwnerId = acc.Relationship_Manager__r.User_Record__c;
                    accountsToUpdate.add(acc);                
                }
            }
        }
        
        try{
            Database.update(accountsToUpdate, false);
           /* for(Account acc : accountsToUpdate){
                User  u = userMap.get(acc.OwnerId);
                u.Newly_Licensed__c = false;
                usersToUpdate.add(u); 
            }
            Database.update(usersToUpdate, false);    */
        }
        catch(DmlException e){
        }
        
    }
        
    global void finish(Database.BatchableContext BC){
    
    }
        
}