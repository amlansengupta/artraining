/* 
Purpose: Update Revenue Start data and End date of opportunity products that belongs to all open opportunities
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0     Sarbpreet   7/7/2014    Added Comments.
==============================================================================================================================================
*/
global class DC_OppLineItemRevenueDateAdjustment implements Database.Batchable<sObject>, Database.Stateful{
   
    // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    private static final String TESTQRY= 'Select id,Revenue_Start_date__c,Revenue_End_date__c from OpportunityLineItem where  Opportunity.isClosed = false and Revenue_Start_date__c >= LAST_YEAR and LastModifiedDate <> LAST_YEAR LIMIT 100 ';
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         system.debug(Label.OppLineItemYearEndQuery);
         String query = Test.isRunningTest()? TESTQRY : Label.OppLineItemYearEndQuery;
        
         return Database.getQueryLocator(query);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */ 
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem > scope)
    {  
         Integer errorCount = 0 ;
         List<OpportunityLineItem> UpdateList = new list<OpportunityLineItem>();
         
         //Iterate through Opportunity records fetched through 'query'
         for(OpportunityLineItem oppli : scope)
         {
             Integer diff = oppli.revenue_start_date__c.daysbetween(oppli.revenue_end_date__c ); 
             if(oppli.revenue_start_date__c.year() == integer.valueOf(Label.OpportunityYearEndConstant)){
                 oppli.revenue_start_date__c = date.parse(Label.Opp_Prod_Rev_Start_date);
                 oppli.revenue_end_date__c = date.parse(Label.Opp_Prod_Rev_Start_date) + diff; 
             }
             UpdateList.add(oppli);
             
         } 
        
            
         if(UpdateList.size() >0 ){
             List<Database.SaveResult> srList= Database.Update(UpdateList, false);  
              // Iterate through the saved Opportunity Line Items  
             Integer i = 0;             
             for (Database.SaveResult sr : srList)
             {
                    // Check if Oppportunity Line Items are not updated successfully.If not, then store those failure in error log. 
                    if (!sr.isSuccess()) 
                    {
                            errorCount++;
                            System.debug('Error DC11_OpportunityLineItemBatchable '+sr.getErrors()[0].getMessage());
                            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC11:' +sr.getErrors()[0].getMessage(), errorLogs, UpdateList[i].Id);  
                    }
                    i++;
             }
         }
       
         
    }
  /*
   *  Method Name: finish
   *  Description: Method is used perform any final operations on the records which are in scope.     
   */ 
    global void finish(Database.BatchableContext BC)
    {
         // Check if errorLogs contains some failure records. If so, then insert error logs into database.
         if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }    
}