/* =============================================================================================
Request Id : 17100 
This class is used as the controller class for MF2_AccountBussGeoSpecs
Date : 06-MAR-2019 
Created By : Sankar Mitra
===============================================================================================*/

public with sharing class UserRoleHierarchyUtil {
    @AuraEnabled
    public static boolean isRunningUserInParentHierarchy(Id recordId) {
        try{
            UserRecordAccess  uAc =[SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId=:recordId ];
            if(uAc.HasEditAccess){
                return true;
            }
        }catch(Exception ex)
        {
            ExceptionLogger.logException(ex, 'UserRoleHierarchyUtil', 'isRunningUserInParentHierarchy');
            throw new AuraHandledException('Record access could not be determined!');
        }
        return false;
    }
    
    @AuraEnabled
    public static boolean isRunningUserInParentHierarchyCon(Id recordId) {
        boolean isNotChild = true;
        try{
            List<Profile> runningUserProfileList = [Select Name from Profile where Id=:userInfo.getProfileId()];
            String runningUserProfileName;
            if(runningUserProfileList != null && runningUserProfileList.size() > 0){
                runningUserProfileName = runningUserProfileList.get(0).Name;
            }
            if(runningUserProfileName != null && runningUserProfileName.equalsIgnoreCase('System Administrator')){
                return true;
            }
            List<Contact> conList = [Select Owner.UserRoleId from Contact where Id=:recordId];
            Id userRoleId;
            if(conList != null && conList.size() > 0){
                userRoleId = conList.get(0).Owner.UserRoleId;
            }
            Set<Id> ownerRoleIds = new Set<Id>();
            ownerRoleIds.add(userRoleId);
            Set<Id> childRoleIds = getAllSubRoleIds(ownerRoleIds);
            if(childRoleIds != null){
                for(Id childRoleId : childRoleIds){
                    if(childRoleId == UserInfo.getUserRoleId()){
                        isNotChild = false;
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'UserRoleHierarchyUtil', 'isRunningUserInParentHierarchyCon');
            throw new AuraHandledException('Could not fetch role hierarchy!');
        }
        return isNotChild;
    }
    @AuraEnabled
    public static boolean isRenderSections()
    {
        boolean flag=false;
         List<PermissionSetAssignment> inPermissionSetAsgmnt = new List<PermissionSetAssignment>();
        inPermissionSetAsgmnt = [Select Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment where AssigneeId = :UserInfo.getUserId()];
        List<Id> ids = new List<Id>();
         for(PermissionSetAssignment pma:inPermissionSetAsgmnt){
          String tempvar = pma.PermissionSetId;
          ids.add(tempvar);
         }
        List<PermissionSet> inPermissionSet = new List<PermissionSet>();
        inPermissionSet = [Select Id, Name FROM PermissionSet where id in :ids AND Name = 'Know_Your_Client'];
        if(!inPermissionSet.isEmpty()){
           flag = true;
        }
        return flag;
    }
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        
        Set<ID> currentRoleIds = new Set<ID>();
        
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
                                IN :roleIds AND ParentRoleID != null]) {
                                    currentRoleIds.add(userRole.Id);
                                }
        
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        
        return currentRoleIds;
    }
    @Auraenabled
    public static boolean recordTypeCheckAcc(String accId)
    {
        boolean flag = false;
        Account acc=null;
        List<Account> listAcc=[Select ID,RecordType.Name from Account where Id =: accId];
        try{
            if(listAcc!=null && listAcc.size()>0)
            {
                acc=listAcc.get(0);
            }
            String readOnlyRecord = 'Account Read Only View';
            //List<String> listReadOnlyRecord = readOnlyRecord.split(',');
            if(readOnlyRecord.equalsIgnoreCase(acc.RecordType.Name))
            {
                flag=true;
            }
        }
        catch(Exception ex)
        {
            ExceptionLogger.logException(ex, 'UserRoleHierarchyUtil', 'recordTypeCheckAcc');
            throw new AuraHandledException('Could not fetch Account Record Types!');
        }
        return flag;
    }
}