/* Purpose: This Batchable class updates respective Opportunity records 
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Oportunity as an input and updates Opp record.
 
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Suchi           05/27/2013    Created Batch Class to update Oportunity records.
 2.0         Sarbpreet       05/06/2014    PMO#3566(June Release) : Updated the code to refer it to current currency rates rather than dated currency
 3.0         Sarbpreet       7/7/2014      Added Comments. 
 ======================================================================================================================================*/
global class DC08_OpportunityDataUpdate  implements Database.Batchable<sObject> 
{
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global String query = System.Label.CL_94_DC08Oppty;    
    //Updated as part of PMO#3566(June Release)
    global static List<CurrencyType> exchangeRates = new List<CurrencyType>();
    global static  Set<String> IsoCodeSet = new Set<String>();  
    
    /*
    *    Creation of Constructor
    */
    global DC08_OpportunityDataUpdate()
    {
        
    }
    /*
    *       Constructor to assign the query string to a final variable  
    */ 
    global DC08_OpportunityDataUpdate(String query)
    {
        this.query = query; 
    }
     /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */    
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
            
        return Database.getQueryLocator(query);    
    }
    
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */     
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {   
        List<Opportunity> oppForUpdate =  new List<Opportunity>();
        // declare a list of Opportunity for updation and iterate through Opportunity
        for(Opportunity opp : (List<Opportunity>)scope)
        {
         
         // add opportunity currency iso code to IsoCodeSet 
             IsoCodeSet.add(opp.CurrencyIsoCode);        
         }
         
         if(!IsoCodeSet.isEmpty())
                exchangeRates = getDatedExchangeCurrencies(IsoCodeSet);
            
            for(Opportunity opp : (List<Opportunity>)scope)
            {
                  Double datedCurrency = null;       
                  Date oppCloseDate = opp.CloseDate;
                  if(!exchangeRates.isEmpty())
                  {                         
                        //Updated as part of PMO#3566(June Release)
                        for(CurrencyType currencyVal : exchangeRates)
                        {   
                             if(currencyVal.IsoCode == opp.CurrencyIsoCode)  
                            {
                                datedCurrency = currencyVal.ConversionRate;
                                
                                break;
                            }
                        }
                        
                       
                  }
                  
                  if(datedCurrency <> null && opp.Amount <> null) opp.Total_Opportunity_Revenue_USD__c = opp.Amount/datedCurrency;
                  // add it to list for updation
                  
                  // make dml update on opprtunity for updation
                  
                    oppForUpdate.add(opp);
            }
            
            if(!oppForUpdate.isEmpty())
            {
                 // make dml statement for updation
                for(Database.SaveResult result : Database.update(oppForUpdate, false))
                {
                    // To Do Error logging
                    if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                    {
                    // insert error logs
                      errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                    }
                }
            }       
                  
     }
      /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
      global void finish(Database.BatchableContext BC)
      {
      
      }
     /*
     * @Description : This method is called for getting Exchange  Currencies(Updated as part of PMO#3566(June Release))
     * @ Args       : List<DatedConversionRate>
     * @ Return     : ISOCode,ConversionRate,StartDate, NextStartDate
     */
    public static List<CurrencyType> getDatedExchangeCurrencies(Set<String> CurrCodes)
    {
        
        //Updated as part of PMO#3566(June Release)
        return [SELECT ISOCode, ConversionRate FROM CurrencyType
        WHERE ISOCode in :CurrCodes ];
    }
}

/* Query for executing the class
    String qry = 'SELECT CloseDate,CurrencyIsoCode,Total_Opportunity_Revenue_USD__c,Amount FROM Opportunity where row_flag__c = true' ;
    database.executeBatch(new DC08_OpportunityDataUpdate(qry), 2000);
*/