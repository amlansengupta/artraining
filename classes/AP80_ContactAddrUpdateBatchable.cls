/*Purpose:  This batch class updates the Contact Address when the associated account address is updated.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Gyan        12/07/2013   Created batch class
   2.0 -    Sarbpreet   18/02/2014   As Part of March Release 2014(Request 3546) added logic to post chatter on Contact if Contact's Account address contains Taiwan.
============================================================================================================================================== 
*/
global class AP80_ContactAddrUpdateBatchable implements Database.Batchable<sObject>, schedulable {
    
    global String query;
      // List variable to store error logs For Account
    private List<BatchErrorLogger__c> errorLogs_Account = new List<BatchErrorLogger__c>();
      // List variable to store error logs For Contact
    private List<BatchErrorLogger__c> errorLogs_Contact = new List<BatchErrorLogger__c>();
      // List variable to store error logs For FeedItem
    private List<BatchErrorLogger__c> errorLogs_Feed = new List<BatchErrorLogger__c>();
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
    */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // Query to Fetch only Those Accounts which have contacts associated and Account Address updation date as Today or Yesterday.
      //  query = 'Select BillingStreet,BillingState,BillingPostalCode,BillingCountry,BillingCity,Account_Address_Updated_date__c, (Select MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, OtherCountry, Contact_Status__c,OwnerId From Contacts) From Account where ID IN (select AccountId  from contact) AND  (Account_Address_Updated_date__c = TODAY OR Account_Address_Updated_date__c = YESTERDAY)';
        query = Label.AP80_ContactAddrUpdateBatchableQuery;
        return Database.getQueryLocator(query);
    }
    
    
    /*
     *  Method Name: execute
     *  Description: Method is used to update Contact Addresss .
     *  Note: execute method is a mandatory method to be extended in this batch class.     
    */
    global void execute(Database.BatchableContext bc, List<Account> lstAccount)
    {
        List<Account> lstAccountToUpdate = new List<Account>();
        List<Contact> lstContactToUpdate = new List<Contact>(); 
        List<FeedItem> feedList = new List<FeedItem>();      
        Integer accErrorCount = 0 ;
        Integer conErrorCount = 0 ;
        Integer feedErrorCount = 0 ;
        
  
    FeedItem f ;     
   For(Account acc : lstAccount)
         {
        
                 // Copy Mailing address (not D&B Address) of Account in  contacts’s Account/Mailing address
                 for(Contact con : acc.contacts)
                 { 
                
                 // As Part of March Release 2014(Request 3546) added logic to post chatter on Contact if Contact's Account address contains Taiwan.

                         if(acc.BillingStreet != con.MailingStreet || acc.BillingState != con.MailingState || acc.BillingPostalCode != con.MailingPostalCode || acc.BillingCountry != con.MailingCountry || acc.BillingCity != con.MailingCity )
                            {
                                if(acc.BillingState!=null && acc.BillingCountry== null) 
                                { 
                                    if (!acc.BillingState.containsIgnoreCase('taiwan')) 
                                    
                                    {
                                        con.MailingStreet = acc.BillingStreet;
                                        con.MailingState = acc.BillingState; 
                                        con.MailingPostalCode = acc.BillingPostalCode;
                                        con.MailingCountry = acc.BillingCountry;
                                        con.MailingCity = acc.BillingCity;
                                        
                                        lstContactToUpdate.add(con);
                                    }
                                    
                                    else {
                                        f = new FeedItem();
                                        f.createdbyid = Label.MercerForceUserid;
                                        f.body = Label.TaiwanChatterPost;
                                        f.parentid = con.Id;
                                        feedList.add(f);    
                                    }
                                }
                                else if(acc.BillingState==null && acc.BillingCountry!= null)
                                {
                                        if(!acc.BillingCountry.containsIgnoreCase('taiwan'))
                                         {
                                                con.MailingStreet = acc.BillingStreet;
                                                con.MailingState = acc.BillingState; 
                                                con.MailingPostalCode = acc.BillingPostalCode;
                                                con.MailingCountry = acc.BillingCountry;
                                                con.MailingCity = acc.BillingCity;
                                            
                                        lstContactToUpdate.add(con);
                                        }
                                        else {
                                            f = new FeedItem();
                                            f.createdbyid = Label.MercerForceUserid;
                                            f.body = Label.TaiwanChatterPost;
                                            f.parentid = con.Id;
                                            feedList.add(f); 
                                        }
                                    }
                                  else
                                  {
                                  if(acc.BillingState!=null && acc.BillingCountry!= null) {
                                      if((!acc.BillingState.containsIgnoreCase('taiwan')) && (!acc.BillingCountry.containsIgnoreCase('taiwan')) ) {
                                                con.MailingStreet = acc.BillingStreet;
                                                con.MailingState = acc.BillingState; 
                                                con.MailingPostalCode = acc.BillingPostalCode;
                                                con.MailingCountry = acc.BillingCountry;
                                                con.MailingCity = acc.BillingCity;
                                            
                                        lstContactToUpdate.add(con);    
                                    }
                                    else {
                                        f = new FeedItem();
                                        f.createdbyid = Label.MercerForceUserid;
                                        f.body = Label.TaiwanChatterPost;
                                        f.parentid = con.Id;
                                        feedList.add(f); 
                                    }   
                                  }
                                  }  
                                }                                                           
                            
                  } 
                 
                  acc.Account_Address_Updated_date__c = Null;// Reset Account Address Updated date.
                  lstAccountToUpdate.add(acc); 
                      
             }

           

                      // *********** For FeedItem Record Updation *************************//
        // List variable to save the result of the feedItem Object which are updated   
    /* List<Database.SaveResult> feedSrList= Database.Insert(feedList, false);  

  
     if(feedSrList!=null && feedSrList.size()>0) {
       // Iterate through the saved FeedItem object 
       Integer i = 0;             
       for (Database.SaveResult sr : feedSrList)
       {
              // Check if Account object are not updated successfully.If not, then store those failure in error log. 
              if (!sr.isSuccess()) 
              {
                      feedErrorCount++;
                      errorLogs_Feed  = MercerAccountStatusBatchHelper.addToErrorLog('AP80:' +sr.getErrors()[0].getMessage(), errorLogs_Feed , lstAccount[i].Id);  
              }
              i++;
       }
     }
     system.debug('feedList.size is'+ feedList.size());
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> feedErrorLogStatus = new List<BatchErrorLogger__c>();

      if (feedList.size() > 0) feedErrorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('AP80 Status :lstAccount:' + lstAccount.size() +' feedList:' + feedList.size() +
     + ' Total Errorlog Size of feedItem: '+errorLogs_Feed.size() + ' Batch Error Count :'+feedErrorCount  ,  feedErrorLogStatus , feedList[0].Id);
     
     insert feedErrorLogStatus; */
     

                                 // *********** For Account Record Updation *************************//
        // List variable to save the result of the Account Object which are updated   
     List<Database.SaveResult> accSrList= Database.Update(lstAccountToUpdate, false);  

  
     if(accSrList!=null && accSrList.size()>0) {
       // Iterate through the saved Account object 
       Integer i = 0;             
       for (Database.SaveResult sr : accSrList)
       {
              // Check if Account object are not updated successfully.If not, then store those failure in error log. 
              if (!sr.isSuccess()) 
              {
                      accErrorCount++;
                      errorLogs_Account = MercerAccountStatusBatchHelper.addToErrorLog('AP80:' +sr.getErrors()[0].getMessage(), errorLogs_Account, lstAccount[i].Id);  
              }
              i++;
       }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> accErrorLogStatus = new List<BatchErrorLogger__c>();

     if (lstAccountToUpdate.size() > 0) accErrorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('AP80 Status :lstAccount:' + lstAccount.size() +' lstAccount:' + lstAccount.size() +
     + ' Total Errorlog Size: '+errorLogs_Account.size() + ' Batch Error Count :'+accErrorCount  ,  accErrorLogStatus , lstAccountToUpdate[0].Id);
     
     insert accErrorLogStatus; 
     
                                     //***********For Cotact Records Updation**********************//
     // List variable to save the result of the Contact Object which are updated   
     List<Database.SaveResult> conSrList= Database.Update(lstContactToUpdate, false);  

  
     if(conSrList!=null && conSrList.size()>0) {
       // Iterate through the saved Contact object 
       Integer i = 0;             
       for (Database.SaveResult sr : conSrList)
       {
              // Check if Contact object are not updated successfully.If not, then store those failure in error log. 
              if (!sr.isSuccess()) 
              {
                      conErrorCount++;
                      System.debug('Error AP80_ContactAddrUpdateBatchable '+sr.getErrors()[0].getMessage());
                      errorLogs_Contact = MercerAccountStatusBatchHelper.addToErrorLog('AP80:' +sr.getErrors()[0].getMessage(), errorLogs_Contact, lstAccount[i].Id);  
              }
              i++;
       }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> conErrorLogStatus = new List<BatchErrorLogger__c>();
      if(lstContactToUpdate.size()>0)
      conErrorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('AP80 Status :lstAccount:' + lstAccount.size() +' lstContactToUpdate:' + lstContactToUpdate.size() +
     + ' Total Errorlog Size: '+errorLogs_Contact.size() + ' Batch Error Count :'+conErrorCount  ,  conErrorLogStatus , lstContactToUpdate[0].Id);
     
     insert conErrorLogStatus;      
             
        
    }
    
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records . 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
         // Check if errorLogs_Feed contains some failure records. If so, then insert error logs into database.
         if(errorLogs_Feed.size()>0) 
         {
             database.insert(errorLogs_Feed, false);
         }
         
    
        // Check if errorLogs_Account contains some failure records. If so, then insert error logs into database.
         if(errorLogs_Account.size()>0) 
         {
             database.insert(errorLogs_Account, false);
         }
         // Check if errorLogs_Contact contains some failure records. If so, then insert error logs into database.
         if(errorLogs_Contact.size()>0) 
         {
             database.insert(errorLogs_Contact, false);
         }
         
         
         
    }
    
    /*
     *  Method Name: execute
     *  Note: execute method is a mandatory method to be extended in this batch class for Scheduling Purpose.     
    */
    public void execute(SchedulableContext sc)
    {
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch,Integer.valueOf(Label.AP80_ContactAddrUpdateBatch_Size));    
    }

}