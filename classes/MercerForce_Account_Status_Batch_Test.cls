/*Purpose: Test Class for providing code coverage to MercerForce_Account_Status_Batch class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   12/1/2014  Created class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Removing Pending Step                                              21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class MercerForce_Account_Status_Batch_Test {
    /*
     * @Description : Test method to provide data coverage for updating Account Status
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {  
     
        Map<String,String> custSett = new Map<String,String>();
        custSett.put('CampMemStatCanc','Cancelled');
        custSett.put('CampMemStatCompl','Completed');
        custSett.put('conStatDeceased','Deceased');
        custSett.put('conStatInactive','Inactive');
        custSett.put('MFAccNoActivity','No Activity');
        custSett.put('MFAccNoActOBSO','No Activity - OBSOLETE');
        custSett.put('MFAccStActCon','Active Contact');
        custSett.put('MFAccStActMarkCont','Active Marketing Contact');
        custSett.put('MFAccStActvtExt','Activities Exists');
        custSett.put('MFAccStCompetitor','Competitor');
        custSett.put('MFAccStHighRevenue','High Revenue');
        custSett.put('MFAccStLowRevenue','Low Revenue');
        custSett.put('MFAccStOppStage1','Open Opportunity - Stage 1');
        custSett.put('MFAccStOppStage2','Open Opportunity - Stage 2+');
        custSett.put('MFAccStRecentWin','Recent Win');
        custSett.put('OppStageAP','Active Pursuit');
        custSett.put('OppStageATF','Above the Funnel');
        custSett.put('OppStageCW','Closed / Won');
        //Request No. 17953 : Removing Finalist
        //custSett.put('OppStageFin','Finalist');
        custSett.put('OppStageIde','Identify');
        //Request Id:17601;Commenting step
        //custSett.put('OppStagePS','Pending Step');
        custSett.put('OppStageSel','Selected');

        List<ApexConstants__c > lstApxCon =  new List<ApexConstants__c>();
        for(String s:custSett.keySet()) {
            ApexConstants__c ac = new ApexConstants__c();
            ac.name = s;
            ac.StrValue__c =custSett.get(s);
            lstApxCon.add(ac);
        }
        
        //insert lstApxCon;
        
         
         Colleague__c Coll;
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        }
        system.runAs(User1){
       ConstantsUtility.STR_ACTIVE ='Inactive';
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        
        insert Acc;
            
        //Adding buyer to avoid buyer mandatory custom validation
        Contact con1 = new Contact();
        con1.lastName = 'test';
        con1.AccountId = Acc.id;
        con1.Job_Function__c = 'Administration';
        con1.Job_Level__c = 'VP';
        con1.firstName='test';
        con1.title ='mr';
        con1.email = 'xyz2@abc.com';
        insert con1;
        
        Opportunity opp= new Opportunity();
        opp.Name = 'Test Opportunity2' + String.valueOf(Date.Today());
        opp.Type = 'New Client';
        //Request id:12638 commenting step
        //opp.Step__c = 'Identified Deal';
        opp.CloseDate = Date.Today();
        opp.CurrencyIsoCode = 'USD';
        opp.AccountId = Acc.id;
        opp.StageName = 'Closed / Won';
        opp.Total_Opportunity_Revenue_USD__c = 200000.00;
        opp.Opportunity_Office__c ='New York - 1166';
        opp.Buyer__c = con1.id;
        insert opp;
    
        Account Acc2 = new Account();
        Acc2.Name = 'TestAccountName';
        Acc2.BillingCity = 'City';
        Acc2.BillingCountry = 'Country';
        Acc2.BillingStreet = 'Street';
        acc2.Total_CY_Revenue__c = 5000;
        Acc2.Relationship_Manager__c = Coll.Id;
        Acc2.One_Code__c = '123';
        insert Acc2;
            
        Account Acc3 = new Account();
        Acc3.Name = 'TestAccountName';
        Acc3.BillingCity = 'City';
        Acc3.BillingCountry = 'Country';
        Acc3.BillingStreet = 'Street';
        Acc3.Total_CY_Revenue__c = 0;
        Acc3.Relationship_Manager__c = Coll.Id;
        Acc3.One_Code__c = '123';
        insert Acc3;
            
         Test.starttest();
        Contact con = new contact();
        con.accountid = acc.id;
        con.firstName ='Test';
        con.lastname ='Testl';
        con.title ='mr';
        con.email = 'xyz1@abc.com';
        insert con;
        
        
        
        database.executeBatch(new MercerForce_Account_Status_Batch(), 500);
        Test.stopTest();
       }
    }
}