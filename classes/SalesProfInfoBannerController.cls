public class SalesProfInfoBannerController {
	private ApexPages.StandardController controller;
    public boolean isProjectCodeAdded{get; set;}
    public boolean isELSOWAttached{get; set;}
	public SalesProfInfoBannerController(ApexPages.StandardController controller){
        this.controller = controller;
        Id recordId = controller.getId();
        Sales_Professional__c salesProfObj = [Select Id, EL_On_File__c, SOW_and_EL_Uploaded__c,Is_Project_Added__c from Sales_Professional__c where Id =:recordId];
        if(salesProfObj != null){
            if(salesProfObj.EL_On_File__c || salesProfObj.SOW_and_EL_Uploaded__c){
                isELSOWAttached = true;
            }else{
                isELSOWAttached = false;
            }
            if(salesProfObj.Is_Project_Added__c){
                isProjectCodeAdded = true;
            }
            else{
                isProjectCodeAdded = false;
            }
        }
        /*List<WebCas_Details__c> projectCodes = [Select Id from WebCas_Details__c where Sales_Professional__c =:salesProfObj.Id and WebCas_Project_Number__c != null];
        if(projectCodes != null && projectCodes.size() > 0 ){
            isProjectCodeAdded = true;
        }else{
            isProjectCodeAdded = false;
        }*/
    }
}