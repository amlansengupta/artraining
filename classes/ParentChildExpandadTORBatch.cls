global class ParentChildExpandadTORBatch implements Database.Batchable<sObject>{
 //global String query = 'Select ID,Parent_Opportunity_Name__c,Parent_Expanded_TOR__c,Child_Opp_TOR__c,Expanded_TOR__c, Parent_Total_Oppty_Revenue__c from Opportunity where Parent_Opportunity_Name__c = null AND ID = \'0060U00000DbFZL\'';
 global String query = 'Select ID,Parent_Opportunity_Name__c,Parent_Expanded_TOR__c,Total_Opportunity_Revenue_USD__c,Child_Opp_TOR__c,Expanded_TOR__c, Parent_Total_Oppty_Revenue__c from Opportunity where Parent_Opportunity_Name__c = null';

    //Parent_Expanded_TOR__c = sum of all closed WON child opp Expanded_TOR__c +  Parent_Total_Oppty_Revenue__c
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(test.isRunningTest()){
         query = 'Select ID,Parent_Opportunity_Name__c,Parent_Expanded_TOR__c,Total_Opportunity_Revenue_USD__c,Child_Opp_TOR__c,Expanded_TOR__c, Parent_Total_Oppty_Revenue__c from Opportunity where Parent_Opportunity_Name__c = null limit 200';   
        }
        return Database.getQueryLocator(query);
    }
 global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {
        
         Map<Id,Opportunity> parentoppMap = new Map<Id,Opportunity>();
         List<id> ids = new List<id>();
         List<Opportunity> parentToBeUpd = new List<Opportunity>();
         for(Opportunity opp:scope){
             ids.add(opp.id);
             parentoppMap.put(opp.id, opp);
         }
       system.debug('****' + ids);      
       List<Opportunity> childOppList = new List<Opportunity>([Select ID,Parent_Opportunity_Name__c,Total_Opportunity_Revenue_USD__c,Child_Opp_TOR__c,Total_Opportunity_Revenue__c, Parent_Expanded_TOR__c, Expanded_TOR__c, Parent_Total_Oppty_Revenue__c from Opportunity where StageName = 'Closed / Won' AND Parent_Opportunity_Name__c in:ids]); 
       system.debug('**childOppList' + childOppList);
       Map<Id, List<Opportunity>> parentChildMap = new Map<Id, List<Opportunity>>();
       for(Opportunity opp:childOppList){
           if(parentChildMap.containskey(opp.Parent_Opportunity_Name__c)){
               List<opportunity> clist = parentChildMap.get(opp.Parent_Opportunity_Name__c);
               clist.add(opp);
               parentChildMap.put(opp.Parent_Opportunity_Name__c, clist);
               system.debug('***if' + opp.Parent_Opportunity_Name__c);
           }else{
               List<opportunity> clist = new List<Opportunity>();
               clist.add(opp);
               parentChildMap.put(opp.Parent_Opportunity_Name__c, clist);
               system.debug('***else' + opp.Parent_Opportunity_Name__c);
           }
         }  
        // Loop through parent to get all child TOR
         for(Opportunity opp:scope){
			//ge tthe child 
			List<Opportunity> child = new List<Opportunity>();
			child = parentChildMap.get(opp.Id);
            Decimal totalChildTor = 0;
            Decimal totalChildTorUSD = 0;
            Decimal pexpandadTor = 0;
            Decimal ptotalOppRev = 0;
             
            if(child!=null){ 
            for(Opportunity chopp: child){
                totalChildTor = totalChildTor + chopp.Total_Opportunity_Revenue__c;
                totalChildTorUSD = totalChildTorUSD + chopp.Total_Opportunity_Revenue_USD__c;
               // ptotalOppRev = chopp.Parent_Total_Oppty_Revenue__c;
               // pexpandadTor = chopp.Parent_Expanded_TOR__c;
            }
             
             if(opp.Child_Opp_TOR__c != totalChildTor){
                 opp.Child_Opp_TOR__c = totalChildTor;
                 opp.Child_Opp_TOR_USD__c = totalChildTorUSD;
                 opp.TOR_Updated_Batch__c = system.today();
                 parentToBeUpd.add(opp);
             }
             }
            
         }        
        update parentToBeUpd;
    }
 global void finish(Database.BatchableContext BC)
    {

    }

}