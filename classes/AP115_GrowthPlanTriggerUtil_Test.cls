/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose: This Test Class provide test coverage for growth plan functionality.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sarbpreet   17/11/2014  Created Test Class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class AP115_GrowthPlanTriggerUtil_Test {

    private static Mercer_TestData mtdata = new Mercer_TestData();
     
    /*
     * @Description : Test method to provide test coverage to growth plan functionality
     * @ Args       : null
     * @ Return     : void
     */       
    static testMethod void myUnitTest() {
        User user1 = new User();
        User user2 = new User();

        String systemAdministratorUserProfile = Mercer_TestData.getsystemAdminUserProfile();
        user1 = Mercer_TestData.createUser1(systemAdministratorUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            Colleague__c collg = mtdata.buildColleague();
            Account acct = mtdata.buildAccount();
            Account acct1 = mtdata.buildAccount();
            mtdata.buildAccountTeamMember();
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty1';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acct.id;
            //Request Id:12638 Commenting step
            //testOppty.Step__c = 'Identified Deal';
            //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'Dallas - Main';
            insert testOppty;
            
             FCST_Fiscal_Year_List__c fct = new FCST_Fiscal_Year_List__c(); 
             fct.name='test';
             fct.CurrencyIsoCode = 'USD';
             insert fct;
             ConstantsUtility.STR_ACTIVE = 'Inactive';                                        
            List<Growth_Plan__c> list1 = new List<Growth_Plan__c>();
            Growth_Plan__c gplan1 = new Growth_Plan__c();
            gplan1.Account__c = acct.id;
            gplan1.Planning_Year__c ='2014';
            gplan1.fcstPlanning_Year__c=fct.id;
            list1.add(gplan1);
                                    
            Growth_Plan__c gplan2 = new Growth_Plan__c();
            gplan2.Account__c = acct.id;
            gplan2.Planning_Year__c ='2015';
            gplan2.GBS_Recurring_and_Carry_forward_Revenue__c = 2;
            gplan2.GBS_Projected_New_Sales_Revenue__c = 4;
            gplan2.GBS_Projected_Current_Year_Revenue__c = 6;
            gplan2.fcstPlanning_Year__c=fct.id;
            list1.add(gplan2);
           // insert gplan2;
            //Test.startTest();
            Growth_Plan__c gplan3 = new Growth_Plan__c();
            gplan3.Account__c = acct.id;
            gplan3.Planning_Year__c ='2014';
            gplan3.fcstPlanning_Year__c=fct.id;
            list1.add(gplan3);
            Growth_Plan__c gplan4 = new Growth_Plan__c();
            gplan4.Account__c = acct.id;
            gplan4.Planning_Year__c ='2016';
            gplan4.fcstPlanning_Year__c = fct.id;
            gPlan4.EH_B_Recurring_and_Carry_forward_Revenue__c = 1;
            gPlan4.EH_B_Projected_New_Sales_Revenue__c =2;
            gPlan4.EH_B_Projected_Current_Year_Revenue__c = 8;
            gplan4.fcstPlanning_Year__c=fct.id;
            //insert gplan4;
            list1.add(gplan4);
            
            Growth_Plan__c gplanParent = new Growth_Plan__c();           
            gplanParent.Account__c = acct1.id;
            gplanParent.Planning_Year__c = '2016';
            gplanParent.fcstPlanning_Year__c=fct.id;
            //insert gplanParent;
            list1.add(gplanParent);
            insert list1;
            Growth_Plan__c gplan5 = new Growth_Plan__c();
            gplan5.Account__c = acct1.id;
            gplan5.Planning_Year__c ='2017';
            gplan5.fcstPlanning_Year__c = fct.id;
            gplan5.Parent_Growth_Plan_ID__c = list1.get(0).id;
            Test.startTest();
            insert gplan5;
           // list1.add(gplan5);
           
            
             gplan5.Parent_Growth_Plan_ID__c = list1.get(0).id;
              
             update gplan5;
             //Test.stopTest();  
            
            task tk = new task();
            tk.status = 'Not Started';
            tk.Priority = 'Normal';         
            insert tk;

             
                         
                PageReference pageRef = Page.Mercer_Growth_Plan;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('acid', acct.id);
                
                ApexPages.StandardController stdController = new ApexPages.StandardController(gplan1); 
                AP113_Mercer_Growth_Plan controller = new AP113_Mercer_Growth_Plan(stdController);
                
                controller.gPlan.EH_B_Recurring_and_Carry_forward_Revenue__c = 1;
                controller.gPlan.EH_B_Projected_New_Sales_Revenue__c =2;
                controller.gPlan.EH_B_Projected_Current_Year_Revenue__c = 8;  
                
                controller.gPlan.BENA_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.BENA_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.BENA_Projected_Current_Year_Revenue__c = 5; 
                
                controller.gPlan.BBC_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.BBC_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.BBC_Projected_Current_Year_Revenue__c = 5; 
                
                controller.gPlan.INV_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.INV_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.INV_Projected_Current_Year_Revenue__c = 5; 
                
                controller.gPlan.RET_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.RET_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.RET_Projected_Current_Year_Revenue__c = 5; 
                
                controller.gPlan.OTH_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.OTH_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.OTH_Projected_Current_Year_Revenue__c = 5;
                
                controller.gPlan.TAL_Recurring_and_Carry_forward_Revenue__c = 10;
                controller.gPlan.TAL_Projected_New_Sales_Revenue__c =12;
                controller.gPlan.TAL_Projected_Current_Year_Revenue__c = 5;
                
                controller.gPlan.Growth_Indicator__c = 20;
                
                
                
                
                controller.totalFullYearRevenue();
                controller.insertTask();    
                controller.insertTasks(gPlan1); 
                 
                controller.saveAndMore();
                controller.gTask.status = 'Not Started';
                controller.gTask.Priority = 'Normal';
                controller.insertTask(); 
                
                controller.savebutton();
                controller.cancelButton();
                                                          
                system.currentPageReference().getParameters().put('id', gplan2.id);
                ApexPages.StandardController stdController1 = new ApexPages.StandardController(gplan2); 
                AP113_Mercer_Growth_Plan controller1 = new AP113_Mercer_Growth_Plan(stdController1);
                
                controller1.gPlan.Planning_Year__c = '2014';
                controller1.savebutton();
               
                ApexPages.StandardController stdController2 = new ApexPages.StandardController(gplan3); 
                AP113_Mercer_Growth_Plan controller2 = new AP113_Mercer_Growth_Plan(stdController2);
                controller2.savebutton();
                //Test.stoptest();
                ApexPages.StandardController stdController5 = new ApexPages.StandardController(gplan2);
                AP113_Mercer_Growth_Plan controller5 = new AP113_Mercer_Growth_Plan(stdController5);
                //controller5.gPlan.Planning_Year__c = '2015';
                controller5.gPlan.TAL_Recurring_and_Carry_forward_Revenue__c = 20;
                controller5.savebutton();
                
                ApexPages.StandardController stdController3 = new ApexPages.StandardController(gplan2); 
                AP114_GrowthPlan_pdfgenerator controller3 = new  AP114_GrowthPlan_pdfgenerator(stdController3);
                controller3.getProcuredServices1();
                controller3.getProcuredServices2();
                controller3.getProcuredServices3();
                controller3.getProcuredServices4();
                
                //Merging from UAT
                ApexPages.StandardController stdController4 = new ApexPages.StandardController(gplan4); 
                AP114_GrowthPlan_pdfgenerator controller4 = new  AP114_GrowthPlan_pdfgenerator(stdController4);
                controller.totalFullYearRevenue();
                
            
           
                 //Test.startTest();
                List<Growth_Plan__c> gplanrec1=[Select id,Parent_Growth_Plan_ID__c,Parent_Growth_Plan_On_CGP__c from Growth_Plan__c where id =: gplan5.Id Limit 1];
                Map<Id,Growth_Plan__c> gplanmap1= new Map<Id,Growth_Plan__c>();
                gplanmap1.put(gplan5.Id,gplanrec1[0]);
                AP115_GrowthPlanTriggerUtil.processGrowthPlanAfterInsert(gplanmap1);
                 //Test.stopTest();
                 //Test.startTest();
                List<Growth_Plan__c> gplanrec=[Select id,Parent_Growth_Plan_ID__c,Parent_Growth_Plan_On_CGP__c from Growth_Plan__c where id =: gplan5.Id Limit 1];
                
                Map<Id,Growth_Plan__c> gplanmap= new Map<Id,Growth_Plan__c>();
                gplanmap.put(gplan5.Id,gplanrec[0]);
                AP115_GrowthPlanTriggerUtil.processGrowthPlanAfterUpdate(gplanmap,gplanmap); 
                Test.stopTest();
                   
                /*ApexPages.StandardController stdController4 = new ApexPages.StandardController(gplan4); 
                AP114_GrowthPlan_pdfgenerator controller4 = new  AP114_GrowthPlan_pdfgenerator(stdController4);
                controller.totalFullYearRevenue();  */       
                  
            //Test.stopTest();
         }
   }        
}