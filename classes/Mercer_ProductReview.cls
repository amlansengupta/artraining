public class Mercer_ProductReview{

        public boolean salesrevcheck{get;set;}
        public boolean revcheck{get;set;}
        public boolean salescheck{get;set;}
        public boolean revenueAdjustment{get;set;}
        Public String OppId{get;set;}
        
    public Mercer_ProductReview(ApexPages.standardController controller)
    {    
        Opportunity opp=(Opportunity)controller.getRecord();
        OppId=opp.Id;
        salesrevcheck=false;
        revcheck=false;
        salescheck=false;
        revenueAdjustment=false;
        
        String srcheck=ApexPages.currentPage().getParameters().get('salesrevcheck');
        String rcheck=ApexPages.currentPage().getParameters().get('revcheck');
        String scheck=ApexPages.currentPage().getParameters().get('salescheck');
        String revenueAdjustments=ApexPages.currentPage().getParameters().get('isrevadjustbool');
        
        
        if(srcheck=='True'){
            salesrevcheck=True;
        } 
        if(rcheck=='True'){
            revcheck=True;
        }
        if(scheck=='True'){
            salescheck=True;
        } 
        if(revenueAdjustments=='True'){
            revenueAdjustment=True;
        } 
        
        
        
        
    }
    public Pagereference delegatesetUp(){
    
        PageReference pageref=new PageReference('/apex/DelegateCodeSetUpEmail?id='+OppId);
        return pageref;
    }

}