/*
Purpose: This test class provides data coverage to AP38_MultipleSalesCreditController class
           
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    Shashank 03/29/2013  Created test class
=============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP38_MultipleSalesCreditController 
{
    /*
     * @Description : Test method for  New Sales Credit Page
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = 'Health & Benefits';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
         
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id 12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.Opportunity_Office__c = 'New York - 1166';
        insert testOppty;
        
        test.stopTest();
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        
        system.runAs(User1){
        ApexPages.StandardController stdController = new ApexPages.StandardController(testSales);
        AP38_MultipleSalesCreditController controller = new AP38_MultipleSalesCreditController(stdController);
        
        controller.wrapSalesList[0].salesCredit.EmpName__c = testColleague1.Id;
        controller.wrapSalesList[0].salesCredit.Percent_Allocation__c = 10;
        
        controller.addSalesRow();
        controller.removeIndex = 1;
        controller.removeSalesCredit();
        
        controller.save();
        controller.cancel();
        }
    }
    /*
     * @Description : Test method to check null value
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void myUnitTest_NullValueTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = 'Health & Benefits';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
         
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id 12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.Opportunity_Office__c = 'New York - 1166';
        insert testOppty;
        
        test.stopTest();
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = Null;
        
        system.runAs(User1){
       PageReference pg = Page.Mercer_MultipleSalesCredit;
       Test.setCurrentPage(pg); 
       ApexPages.currentPage().getParameters().put('retURL',testOppty.Id);

        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testSales);
        AP38_MultipleSalesCreditController controller = new AP38_MultipleSalesCreditController(stdController);
        
        controller.wrapSalesList[0].salesCredit.EmpName__c = testColleague1.Id;
        controller.wrapSalesList[0].salesCredit.Percent_Allocation__c = 10;
        
        controller.addSalesRow();
        controller.removeIndex = 1;
        controller.removeSalesCredit();
        
        controller.save();
        controller.cancel();
        }
    }
}