global class Mercer_DelegatedCodeSetUpController {

/*   public Opportunity opportunity{get; set;}
public string OppID {get;set;}

public Mercer_DelegatedCodeSetUpController(ApexPages.StandardController controller) {
    this.opportunity = (Opportunity)controller.getRecord();
    OppID =ApexPages.currentPage().getParameters().get('id');

    if(OppID!=Null){
        opportunity=[select id,Name,StageName,CloseDate,Type,AccountID,Total_Opportunity_Revenue_USD__c,Concatenated_Products__c,Opportunity_ID__c   from Opportunity where Id=:OppID];
       Account OneCode=  [Select One_Code__c from Account where Id = :opportunity.AccountID];
     //    Id OppOwnerID = [Select OwnerId from Opportunity where Id = :opprec.Id].OwnerId;
    }

}*/
    public Mercer_DelegatedCodeSetUpController(ApexPages.StandardController controller){}
    webservice static String getOppDetail(String opp){
    System.debug('****');
    List<Opportunity> oppList = [Select id,Name,StageName,CloseDate,Type,AccountID,Opp_Number_of_LOBs__c,
                                 Account.One_Code__c,Account.Name,
                                 Total_Opportunity_Revenue_USD__c,Concatenated_Products__c,Opportunity_ID__c
                                 From Opportunity where Id=:opp];

    
    return JSON.serialize(oppList);

}

    webservice static String getOppLineItemDetail(String opp){
    
    List<OpportunityLineItem> oppLIList = [Select Id, Name, OpportunityId,Product_Name_LDASH__c, Product2Id,Project_Manager__c, Sales_Price_USD_calc__c, is_Project_Required__c FROM OpportunityLineItem WHERE OpportunityId =:opp];

    
    return JSON.serialize(oppLIList);

    }
    
   
}