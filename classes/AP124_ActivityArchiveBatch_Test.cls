/*Purpose: Apex test class to to provide coverage for AP124_ActivityArchiveBatch
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jyotsna     6/4/2015    Provide coverage for AP124_ActivityArchiveBatch class
============================================================================================================================================== 
*/
@isTest
private class AP124_ActivityArchiveBatch_Test {

    private static Mercer_TestData mtdata =new  Mercer_TestData();
    
    static testMethod void myUnitTest() {
        
        Colleague__c col = mtdata.buildColleague();
         
         User user1 = new User();
         String adminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
         user1 = Mercer_TestData.createUser1(adminUserprofile , 'usert2', 'usrLstName2', 2);
         
         Task testTask = new Task();
         testTask.OwnerId = '005E0000002ABz8';
   		 testTask.Subject = 'Was Sent Email';
   		 testTask.Status = 'Completed';
   		 testTask.Priority = 'Normal';
   		 insert testTask;
                 
         Activity_Archive__c actArchive = mtdata.buildActArchive();       
          
         actArchive.Priority__c = 'Normal';
         actArchive.isRelatedToAccount__c = 'NO';
         update actArchive;
                
         system.runAs(User1){
         Test.startTest(); 
          
         String  qry = 'Select Id, WhoId, WhatId, WhoCount, WhatCount, Subject, ActivityDate, Status, Priority, OwnerId, Description, CurrencyIsoCode, IsDeleted, AccountId, IsClosed, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, IsArchived, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet, RecurrenceActivityId, IsRecurrence, RecurrenceStartDateOnly, RecurrenceEndDateOnly, RecurrenceTimeZoneSidKey, RecurrenceType, RecurrenceInterval, RecurrenceDayOfWeekMask, RecurrenceDayOfMonth, RecurrenceInstance, RecurrenceMonthOfYear, MH_Action__c, mh_Additional_Notes_Exist__c, mh_Associated_Green_Sheet__c, mh_Attitude_Questions__c, mh_Basic_Issues__c, mh_Best_Action_Commitment__c, mh_Buying_Influence_Concept__c, mh_Commitment_Questions__c, mh_Confirmation_Questions__c, mh_Credibility__c, mh_Last_Updated_Green_Sheet__c, mh_Managers_Notes_Exist__c, mh_Managers_Notes__c, mh_Managers_Review_Date__c, mh_Minimum_Acceptable_Action__c, mh_New_Information_Questions__c, mh_Possible_Basic_Issues__c, mh_Post_Call_Assessment_Exists__c, mh_Post_Call_Assessment__c, mh_Sheet_Section__c, mh_Unique_Strengths__c, mh_Valid_Business_Reason__c, MDrive_Activity_Id__c, Completion_Date__c, isRelatedToAccount__c, ROW_Flag__c, Notify_User__c, Prospecting_Stage__c FROM Task where OwnerId = \'005E0000002ABz8\' AND ( Subject like \'Was Sent Email%\'  or  Subject like \'Opened Email%\' or  Subject like \'Clicked Link in Email%\' )  AND isclosed=true AND Id  = \''+ testTask.Id+'\'' ;
         database.executeBatch(new Ap124_ActivityArchiveBatch(qry), 100); 
         Test.stopTest();
        }    
    } 
}