/*Purpose:  This Apex class is created as a Part of ScopeIt Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Search Products
.Select One Product
.Create new scope modeling record


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan       03/31/2014  This Apex class works as Controller for "Mercer_ScopeIt_New_Modelling" VF page which is invoked from 
                                    "New" button on scopeIt Modeling Object.
   2.0 -    Sarbpreet   7/3/2014    Updated Comments/documentation.
   3.0 -    Madhavi     7/25/2014   Updated apex methods to include edit functionlaity as per request#4813,sep 2014 release.
============================================================================================================================================== 
*/
public class Mercer_New_Modelling_class{
    
    public opportunity opportunity{get;set;}  
    public PriceBookEntry pbe {get;set;}
    public List<Wrapper> lstwrapper = new List<Wrapper>();
    public String selProduct {get;set;}
    public String selProductId {get;set;}
    public wrapper wrpobj {get;set;}
    public String searchValue {get;set;}
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list

 
    /*
    *    Creation of constructor for Mercer_New_Modelling_class class
    */
    public Mercer_New_Modelling_class(ApexPages.StandardController controller) {
        //**start***Added as part of req#4813 sep 2014 release
        if(controller.getId()!=null){
          objMod= [SELECT Product__c, Product_Name__c, CurrencyIsoCode, Sales_Price__c, Name,id FROM Scope_Modeling__c where id=:Controller.getId() limit 1];
          selProduct = objMod.Product_Name__c; 
          selProductId = objMod.Product__c;
        }//**end***Added as part of req#4813 sep 2014 release
        else{       
        objMod = new Scope_Modeling__c();
        }
        total_size = [select count() from Product2 where IsActive = true and Classification__c= :ConstantsUtility.STR_FILTER]; 
        
    }
    
    /*
    *   Getter method for lstwrapper fro getting product details
    */
    public List<wrapper> getlstwrapper(){
        lstwrapper.clear();
         wrapper w;
        if(searchValue <> null){
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\' and ';
            sql += '(name like \'%'+searchValue+'%\' or LOB__c like \'%'+searchValue+'%\' or Segment__c like \'%'+searchValue+'%\'  or Cluster__c Like \'%'+searchValue+'%\') order by Name limit :list_size offset :counter';
            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                lstwrapper.add(w);
            }
           
        }
        else{
            for(Product2 prod: [select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= 'Consulting Solution Area' order by Name limit :list_size offset :counter]){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                lstwrapper.add(w);
            }    
        }
        return lstwrapper;
    }
    
    public Scope_Modeling__c objMod {get;set;}
    
  /*
   * @Description : This method saves the product record
   * @ Args       : null
   * @ Return     : pageReference
   */  
    public pageReference SaveProduct(){
        //Scope_Modeling__c objMod = new Scope_Modeling__c();
        if(String.IsBlank(selProductId)){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ConstantsUtility.STR_MSG));
             return null;
        }
        else{
            objMod.Product__c = selProductId;
            upsert objMod;//changed dml from insert to upsert as part of req#4813 sep 2014 release.
            return new PageReference(ConstantsUtility.STR_URL+objMod.id);
        }
    }
    
	
  /*
   * @Description : Method for searching the products
   * @ Args       : null
   * @ Return     : pageReference
   */
    public pageReference searchProducts(){
        lstwrapper.clear();
        wrapper w;
        if(searchValue <> null){
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\' and ';
            sql += '(name like \'%'+searchValue+'%\' or LOB__c like \'%'+searchValue+'%\' or Segment__c like \'%'+searchValue+'%\'  or Cluster__c Like \'%'+searchValue+'%\')';
            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                lstwrapper.add(w);
            }
            return null;
        }
        else{
            String sql = 'select id,name,ProductCode,Description,LOB__c,Segment__c,Cluster__c from Product2 where IsActive = true and Classification__c= \'Consulting Solution Area\'';

            for(Product2 prod: database.query(sql)){
                w = new Wrapper();
                w.pid = prod.id;
                w.pname = prod.name;
                w.pcode = prod.ProductCode;
                w.pdesc = prod.Description;
                w.plob = prod.Lob__c;
                w.pseg = prod.segment__c;
                w.pclus= prod.cluster__c;
                lstwrapper.add(w);
            }
            return null;
        }
    }
  
    /*
    *    Wrapper class for products
    */
    public class wrapper {
        public boolean isSelected {get;set;}
        public String pid {get;set;}
        public String pname{get;set;}
        public String pcode{get;set;}
        public String pdesc{get;set;}
        public String plob{get;set;}
        public String pseg{get;set;}
        public String pclus{get;set;}
    }
    
        
   
    
     /*
     *  Method for navigating to beginning of page
     */
      public PageReference Beginning() {
      counter = 0;
      
      return null;
   }
  /*
   *    Method for navigating to previous page
   */
   public PageReference Previous() { 
      counter -= list_size;
      return null;
   }
  /*
   *    Method for navigating to next page
   */
   public PageReference Next() { 
      counter += list_size;
      return null;
   }
 
  /*
   *    Method for navigating to the last page
   */
   public PageReference End() { 
      counter = total_size - math.mod(total_size, list_size);
      return null;
   }
 
  /*
   *    Getter method for getting the value of DisablePrevious variable 
   */
   public Boolean getDisablePrevious() {
      
      if (counter>0) return false; else return true;
   }
 
  /*
   *    Getter method for getting the value of DisableNext variable 
   */
   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) return false; else return true;
   }
 
  /*
   *    Getter method for getting the total size
   */
   public Integer getTotal_size() {
      return total_size;
   }
 
  /*
   *    Getter method for getting the page number
   */
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
  /*
   *    Getter method for getting total pages
   */
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }

}