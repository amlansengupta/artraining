/*Purpose:  This Apex class is created as a Part of July 2014 Break fix Release for Req#4562
==============================================================================================================================================
History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   06/17/2014  Create class to copy the ScopeIt Task data.
   2.0 -    Jagan   06/23/2014  Added method to copy Modeling Tasks
============================================================================================================================================= 
*/
Global class AP107_ScopeItCopyTaskandEmployees {

    webservice static String copyTask(Id TaskId){
        String message = '';
        ScopeIt_Task__c newTask = new ScopeIt_Task__c();
        Set<Id> taskidset = new Set<Id>();
        taskidset.add(taskId);
        SavePoint newTaskSP = Database.setSavepoint();
        //Map<String, Labels__c> labelsmap = Labels__c.getAll();
        try{
            //Get the query framed in a string for scopeit task
            String taskQuery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL('ScopeIt_Task__c', 'id in :taskidset');
            ScopeIt_Task__c ObjScpTsk = database.query(taskQuery);
            //clone task
            newTask = ObjScpTsk.clone();
            //change the Task Name
            newTask.Task_Name__c = 'Copy - '+newTask.Task_name__c;
            //insert task
            database.Insert(newTask);
            
            //Get the query framed in a string for scopeit employee where task id is original task id
            String empQuery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL('ScopeIt_Employee__c', 'ScopeIt_Task__c in :taskidset');
            //query all scope it employees for this task
            List<ScopeIt_Employee__c> lstScpEmp = database.query(empQuery);
            List<ScopeIt_Employee__c> newlstScpEmp = new List<ScopeIt_Employee__c>();
            
            //clone all the employees and update task id to new one for each employee
            for(ScopeIt_Employee__c se: lstScpEmp) {
                ScopeIt_Employee__c newScpEmp = se.clone();
                newScpEmp.ScopeIt_Task__c = newTask.id;
                newlstScpEmp.add(newScpEmp);
            }
            
            //insert all employees
            database.Insert(newlstScpEmp);
            //return success
            message = Label.TaskCopySuccess;
            return message;
        }
        catch(Exception e){
            Database.rollback(newTaskSP);
            //return failue with exception
            message = 'Copy Failed: '+e;
            return message;
        }
    }
    
    
    webservice static String copyModelingTask(Id TaskId){
        String message = '';
        Scope_Modeling_Task__c newTask = new Scope_Modeling_Task__c();
        Set<Id> taskidset = new Set<Id>();
        taskidset.add(taskId);
        SavePoint newTaskSP = Database.setSavepoint();
        //Map<String, Labels__c> labelsmap = Labels__c.getAll();
        try{
            //Get the query framed in a string for scopeit task
            String taskQuery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL('Scope_Modeling_Task__c', 'id in :taskidset');
            Scope_Modeling_Task__c ObjScpTsk = database.query(taskQuery);
            //clone task
            newTask = ObjScpTsk.clone();
            //change the Task Name
            newTask.Task_Name__c = 'Copy - '+newTask.Task_name__c;
            //insert task
            database.Insert(newTask);
            
            //Get the query framed in a string for scopeit employee where task id is original task id
            String empQuery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL('Scope_Modeling_Employee__c', 'Scope_Modeling_Task__c in :taskidset');
            //query all scope it employees for this task
            List<Scope_Modeling_Employee__c> lstScpEmp = database.query(empQuery);
            List<Scope_Modeling_Employee__c> newlstScpEmp = new List<Scope_Modeling_Employee__c>();
            
            //clone all the employees and update task id to new one for each employee
            for(Scope_Modeling_Employee__c se: lstScpEmp) {
                Scope_Modeling_Employee__c newScpEmp = se.clone();
                newScpEmp.Scope_Modeling_Task__c = newTask.id;
                newlstScpEmp.add(newScpEmp);
            }
            
            //insert all employees
            database.Insert(newlstScpEmp);
            //return success
            message = Label.TaskCopySuccess;
            return message;
        }
        catch(Exception e){
            Database.rollback(newTaskSP);
            //return failue with exception
            message = 'Copy Failed: '+e;
            return message;
        }
    }
    
    
    /**
    *Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
    */
    public static string getCreatableFieldsSOQL(String objectName, String whereClause){
        String selects = '';
        
        //get a map of field names and field tokens
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        List<String> selectFields = new List<String>();
        
        if(fMap!=null){
            for(Schema.SObjectField ft : fMap.values()){ //loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); //describe each field (fd)
                if (fd.isCreateable()){ //field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
        
        if(!selectFields.isEmpty()){
            for (string s: selectFields){
                selects += s + ',';
            }
            if(selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            }            
        }
        if(whereClause == null || whereClause == '')
            return 'SELECT ' + selects+ ' FROM ' + objectName;
        else
            return 'SELECT ' + selects+ ' FROM ' + objectName + ' WHERE '+whereClause;
    }
    
    
}