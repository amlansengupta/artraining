global class Individual_Sales_Goal_ShareBatch implements Database.Batchable<sObject>{
     global static Individual_Sales_Goal__Share createShareRecord(Id parentId, Id userId){
        Individual_Sales_Goal__Share supervisorRec = new Individual_Sales_Goal__Share();
        supervisorRec.ParentId = parentId;
        supervisorRec.UserOrGroupId = userId;
        supervisorRec.AccessLevel = 'Read';
        supervisorRec.RowCause = Schema.Individual_Sales_Goal__Share.RowCause.Supervisor__c;
        return supervisorRec;
    } 
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
       
        string query='select id ,Colleague__c from Individual_Sales_Goal__c where Colleague__c!=null';
        
            return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext bc, List<Individual_Sales_Goal__c> newGoals){
          Map<Id, List<Individual_Sales_Goal__c>> ownerIdSalesGoalMap = new Map<Id, List<Individual_Sales_Goal__c>>();
        List<Individual_Sales_Goal__Share> supervisorShareRecords = new List<Individual_Sales_Goal__Share>();
        //String ownerId;
        try{
            if(newGoals == null){
                return;
            }
            Set<Id> recordIds = new Set<Id>();
            for(Individual_Sales_Goal__c isg:newGoals){
                recordIds.add(isg.Id);
            }
            List<Id> colleagueIds = new List<Id>();
            for(Individual_Sales_Goal__c salesGoal : newGoals){
                colleagueIds.add(salesGoal.Colleague__c);
            }
            Map<Id,Colleague__c> colleagueMap = new Map<Id, Colleague__c>([Select Id, User_Record__c from Colleague__c where Id IN:colleagueIds]); 
            if(colleagueMap != null){
                for(Individual_Sales_Goal__c salesGoal : newGoals)
                {
                    if(salesGoal.Colleague__c != null){
                        system.debug('per colleague::'+colleagueMap.get(salesGoal.Colleague__c).User_Record__c);
                        if(ownerIdSalesGoalMap.containsKey(colleagueMap.get(salesGoal.Colleague__c).User_Record__c))
                        {
                            if(ownerIdSalesGoalMap.get(colleagueMap.get(salesGoal.Colleague__c).User_Record__c)!=null)
                            {
                                ownerIdSalesGoalMap.get(colleagueMap.get(salesGoal.Colleague__c).User_Record__c).add(salesGoal);
                            }
                            else
                            {
                                List<Individual_Sales_Goal__c> shareIndividUserGoals = new List<Individual_Sales_Goal__c>();
                                shareIndividUserGoals.add(salesGoal);
                                ownerIdSalesGoalMap.put(colleagueMap.get(salesGoal.Colleague__c).User_Record__c,shareIndividUserGoals);
                            }
                            
                            
                        }
                        else{
                            List<Individual_Sales_Goal__c> shareIndividUserGoals = new List<Individual_Sales_Goal__c>();
                            shareIndividUserGoals.add(salesGoal);
                            ownerIdSalesGoalMap.put(colleagueMap.get(salesGoal.Colleague__c).User_Record__c,shareIndividUserGoals);
                        }
                    }
                }
            }
            
            List<User> ownerList = [Select Id,S_Dash_Supervisor__c,S_Dash_Supervisor_2__c,S_Dash_Supervisor_3__c,S_Dash_Supervisor_4__c,S_Dash_Supervisor_5__c from User where Id in: ownerIdSalesGoalMap.keySet()]; 
           
            for(User owner : ownerList)
            {
                List<Individual_Sales_Goal__c> ownerGoals = ownerIdSalesGoalMap.get(owner.Id);
                if(ownerGoals!=null && !ownerGoals.isEmpty())
                {
                    for(Individual_Sales_Goal__c indivGoal: ownerGoals)
                    {
                        if(owner.S_Dash_Supervisor__c!=null)
                        {
                            supervisorShareRecords.add(createShareRecord(indivGoal.Id,owner.S_Dash_Supervisor__c));
                        }
                        if(owner.S_Dash_Supervisor_2__c!=null)
                        {
                            supervisorShareRecords.add(createShareRecord(indivGoal.Id,owner.S_Dash_Supervisor_2__c));
                        }
                        if(owner.S_Dash_Supervisor_3__c!=null)
                        {
                            supervisorShareRecords.add(createShareRecord(indivGoal.Id,owner.S_Dash_Supervisor_3__c));
                        }
                        if(owner.S_Dash_Supervisor_4__c!=null)
                        {
                            supervisorShareRecords.add(createShareRecord(indivGoal.Id,owner.S_Dash_Supervisor_4__c));
                        }
                        if(owner.S_Dash_Supervisor_5__c!=null)
                        {
                            supervisorShareRecords.add(createShareRecord(indivGoal.Id,owner.S_Dash_Supervisor_5__c));
                        }
                    }
                    
                }
            }
            if(!supervisorShareRecords.isEmpty()){
                system.debug('supervisorShareRecords::'+supervisorShareRecords);
                insert supervisorShareRecords;
            }
        }
        catch(Exception e)
        {
            ExceptionLogger.logException(e, 'AP18_IndividualSalesGoalTriggerUtil', 'shareRecordWithSupervisor');
            throw e;
        }
        }
        
    
    
    global void finish(Database.BatchableContext bc){
    }
        

}