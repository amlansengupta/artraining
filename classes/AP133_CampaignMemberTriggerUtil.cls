/*Purpose:  This class restrict users from deleting Camapign Members
 =====================================================================================================================================================
 History 
 ----------------------- 
 VERSION     AUTHOR      DATE        DETAIL 
  1.0 -      Sarbpreet   2/3/2016  As part of request # 8508(March'16 Release) Created class to restrict users from deleting Camapign Members
=====================================================================================================================================================
 */
public with sharing class AP133_CampaignMemberTriggerUtil {
    private static set<string> CampaignMemberIdset = new set<string>();
    public static final String STR_ERROR = 'You may remove your contact from this campaign by clicking the Add/Remove My Contacts button on this campaign while the campaign’s status is ‘In review’.';


public static void campaignMemberAfterDelete(List<CampaignMember> campMemList,Boolean isDelete) {
    Integer i=0;        
    if(isDelete == true) {
        for(CampaignMember cMem : campMemList)
        {
            if(UserInfo.getProfileId()!= label.System_Admin_Profile && UserInfo.getProfileId()!= label.Marketo_Sync_ID) {
                
            campMemList[i].adderror(STR_ERROR);
            i++ ;
        }
        }
    }
    
}

}