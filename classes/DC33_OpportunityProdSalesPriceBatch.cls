/*Purpose:  This batch class will calculate and update the value of Sales Price(USD) (Calc) field for existing Opportunity Product records.
=============================================================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   6/3/2015    As part of July Release(PMO# 6454) created batch class to calculate and update the value
                                    of Sales Price(USD) (Calc) field for existing Opportunity Product records.
============================================================================================================================================== 
*/
global class DC33_OpportunityProdSalesPriceBatch implements Database.Batchable<sObject>, Database.Stateful  {  
    
    private String query; 
    public static final string constErrorStr1 = 'DC33_OpportunityProdSalesPriceBatch Exception occured:' ;
    public static final string constErrorStr2 = 'DC33_OpportunityProdSalesPriceBatch: Error with Opportunity Product updation : ';
    public static final string constErrorStr4 = 'DC33_OpportunityProdSalesPriceBatch :scope:' ;   
    public static final string constErrorStr5 =  ' oppProdList:' ;
    public static final string constErrorStr6 = ' Total Errorlog: ' ;
    public static final string constErrorStr7 =  ' Batch Error Count errorCount: ' ;
    public static final string constErrorStr8 =  'IN DC 33: the total Opportunity Products to be processed are: ';
    public static final string constErrorStr9 = 'IN DC 33 Finish: the Opportunity Products to be processed are: '; 
    private Integer i = 0;
    
     // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();


    /*
    *    Creation of Constructor
    */
    global DC33_OpportunityProdSalesPriceBatch() {       
                
         String qry =  Label.Sales_Price_USD_calc_Query;
         this.query= qry;
    }
    
    /*
    *    Creation of Constructor
    */
    global DC33_OpportunityProdSalesPriceBatch(String qStr)  {        
         this.query= qStr;
    }
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }

    /*
     *  Method Name: execute
     *  Description: Updating Sales Price USD calc field on existing opportunity products
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        list<Opportunitylineitem> oppProductlist = scope;
        List < Opportunitylineitem > oppProdList = new List < Opportunitylineitem > ();
        //List to hold Save Result 
        List<Database.SaveResult> oppProdSaveResult = new List<Database.SaveResult>(); 
        Integer errorCount = 0;
        try {
        
        //set variable to store Opportunity Ids
            set<string> opportunityIdSet = new set<string>();
            //set variable to store currency ISO Codes
            set<string> ISOCodeSet = new set<string>();
            Map<string, CurrencyType> ISOCodeMap = new Map<string, CurrencyType>();
            //List variable to store exchange rates (Updated as part of PMO#3566(June Release))
            List<CurrencyType> exchangeRates = new List<CurrencyType>();
            //map variable to store Opportunity id and Opportunity record
            Map<string, Opportunity> opportunityMap = new Map<string, Opportunity>(); 
            
            //iterate over trigger.new
            for(OpportunityLineItem lineItem:oppProductlist)
            {
                //add opportunity id to set
                opportunityIdSet.add(lineItem.OpportunityId);
            }
            
            //fetch opportunities
            for(Opportunity opp:[select Id, CurrencyISOCode, Probability, CloseDate from Opportunity where Id IN :opportunityIdSet])
            {
                //add currency ISO Codes to set
                ISOCodeSet.add(opp.CurrencyISOCode);
                //add Opportunity id and Opportunity record to map
                opportunityMap.put(opp.Id, opp);
            }    
            
            for(CurrencyType currVal : [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE ISOCode in :IsoCodeSet ]) 
            {
                ISOCodeMap.put(currVal.ISOCode, currVal);
            }    
                   system.debug('ISOCodeSet is '+ ISOCodeSet);
            //iterate over trigger.new
            for(OpportunityLineItem lineItem:oppProductlist)
            {
                //fetch the related opportunity from map
                Opportunity opp = opportunityMap.get(lineItem.OpportunityId);
                if(opp.CurrencyIsoCode<>null&&opp.CloseDate<>null)
                { 
                    
                                if(ISOCodeMap.containsKey(opp.CurrencyIsoCode))
                                {
                                    if(lineItem.UnitPrice<>null)  {
                                        CurrencyType currencyVal = ISOCodeMap.get(opp.CurrencyIsoCode);
                                        //calculate  Sales Price(USD) (Calc)
                                        lineItem.Sales_Price_USD_calc__c=lineItem.UnitPrice/currencyVal.ConversionRate;
                                        system.debug('lineItem.Sales_Price_USD_calc__c is ' + lineItem.Sales_Price_USD_calc__c);
                                    }
                                    oppProdList.add(lineItem);
                                  
                                }                    
                }
            }
        
        } catch(Exception e)
         {
            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + e.getMessage(), errorLogs, scope[0].Id);             
            System.debug(constErrorStr1 +e.getMessage());
         } 
      
        //Update Opportunity Product
        if(oppProdList.size()>0)
        {
        
           oppProdSaveResult = Database.update(oppProdList,true) ;
          
           //Iterate for Error Logging
            for(Database.Saveresult result : oppProdSaveResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCount++;
                }
                
              }           
        }
        
       
         //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatusforcount = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         
         errorLogStatusforcount  = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr8 + i ,  errorLogStatusforcount  , scope[0].Id);
         //Insert to ErrorLogStatus
         insert  errorLogStatusforcount ;
         
         //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         
         errorLogStatus  = MercerAccountStatusBatchHelper.addToErrorLog(constErrorStr4  + scope.size() +constErrorStr5  + oppProdList.size() + constErrorStr6 +errorLogs.size() + constErrorStr7 +errorCount  ,  errorLogStatus  , scope[0].Id);
         //Insert to ErrorLogStatus
         insert  errorLogStatus ;
        
        
    }
    
    

    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC) {
        
        List<BatchErrorLogger__c> errorLogStatusforcount = new List<BatchErrorLogger__c>();
        //Values that will be stored while Error Logging
        errorLogStatusforcount  = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr9 + i ,  errorLogStatusforcount  , null);
        //Insert to ErrorLogStatus
        insert  errorLogStatusforcount ;
         
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  

    }
    
  
    
}