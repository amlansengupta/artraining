global class SCH_UpdateOpportunityFields implements Schedulable
{
    global SCH_UpdateOpportunityFields(){
    }
    
    global void execute(SchedulableContext context)
    {
        String query='Select ID,OwnerId,AccountId,Account_Region__c,Account_Name_LDASH__c,Sibling_Contact_Name__c,Sibling_Contact_Office_Name__c,Sibling_Contact_Name__r.Location_Descr__c,Account.Name,Account.Account_Region__c,Owner_Profile_Name__c,Opp_Owner_Operating_Company__c,Owner.Profile.Name,Owner.Level_1_Descr__c from Opportunity where Owner_Profile_Name__c=null AND StageName NOT IN (\'Closed / Won\',\'Closed / Lost\',\'Closed / Declined to Bid\',\'Closed / Client Cancelled\') AND CALENDAR_YEAR(CloseDate) >= 2015'; 
      
        Batch_UpdateOpportunityFields b = new Batch_UpdateOpportunityFields(query);     
        Id batchJobId = Database.executeBatch(b,integer.valueof(Label.Opp_Batch_Size));
       
    }
}