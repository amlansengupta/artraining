/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
                            1.0 -    Shashank 03/29/2013  Created Schedulable class for Opportunity
 --------------------------2.0  -    Trisha    25/01/2019    Modified
=============================================================================================================================================== 
*/
global class AP25_LockOpportunitySchedulable implements schedulable
{
    global final string baseQuery = 'Select Id,Parent_Opportunity_Name__c, RecordTypeId from Opportunity ';
    //12638 : merging   Closed / Declined to Bid and Closed / Client Cancelled to Closed / Cancelled
    global final string whereClause = 'where CloseDate < THIS_MONTH AND StageName IN (\'Closed / Won\', \'Closed / Lost\' , \'Closed / Cancelled\') AND (RecordType.DeveloperName != \'Opportunity_Locked\' AND RecordType.DeveloperName != \'Opportunity_Product_Expansion_Locked\'AND RecordType.DeveloperName != \'Opportunity_Locked_With_Product_Expansions\')';

    global final string query;
    
    global AP25_LockOpportunitySchedulable()
    {
        this.query = baseQuery + whereClause;
    }
    
    global void execute(SchedulableContext SC) 
    {       
       database.executeBatch(new AP26_LockOpportunityBatchable(query), 5);  
    }
}