/*Purpose:  This Apex class is created as a Part of May Release for Req:3729
==============================================================================================================================================

. create all required data for testing 
.create Page refer.

----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    04/043/2014  This class acts as Test class for AP86_pdfGenerator_ScopeModeling .
                                
 
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP86_pdfGenerator_ScopeModeling_test{
     private Static Mercer_TestData mtdata = new Mercer_TestData();   
     private static Mercer_ScopeItTestData mtSdata = new Mercer_ScopeItTestData(); 
  /*
   * @Description : Test method to provide data coverage for Print Scope Modeling page
   * @ Args       : Null
   * @ Return     : void
   */  
  private static testMethod void generatePDF_test(){
     
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        String Lob='Benefits Admin';
        Product2 testProduct =mtdata.buildScopeProduct(Lob) ;
        String prodId=testProduct.id;
        Scope_Modeling__c testSProject = mtSdata.buildScopeModelProject(prodId);
        mtSdata.buildScopeModelTask();
      String empBillrate= Mercer_ScopeItTestData.getEmployeeBillRate();
       mtSdata.buildScopeModelEmployee(empBillrate);
        
       
       Scope_Modeling_Task__c testSTask = new Scope_Modeling_Task__c ();
       testSTask.Scope_Modeling__c=testSProject.id;
       //testSTask.Net_Price__c=90.00;
       insert testSTask;
       
       Scope_Modeling_Employee__c testSemp = new Scope_Modeling_Employee__c ();
       testSemp.Scope_Modeling_Task__c=testSTask.Id;
       insert testSemp;
       
       test.starttest();
      system.runAs(User1){
          PageReference pageRef = Page.Mercer_PrintScopeModeling;  
          Test.setCurrentPage(pageRef);
          ApexPages.CurrentPage().getparameters().put('id', testSProject.id);
          ApexPages.StandardController sc = new ApexPages.standardController(testSProject);
          AP86_pdfGenerator_ScopeModeling t = new AP86_pdfGenerator_ScopeModeling (sc);
      }  
      test.Stoptest();
  }
  
  
    
    
    
}