/*Purpose:  This batch class will give edit access to contact team members if the owner of contact is changed
=============================================================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   4/16/2015  (As part of Request # 5982) April Release:Created batch class to give edit access to contact team members if the owner of contact is changed
============================================================================================================================================== 
*/
global class AP126_ContactTeamBatchable implements Database.Batchable<sObject>, Database.Stateful  {  
    
    public String query; 
    public String testquery = 'Select id, name, (select id, ContactTeamMember__c, Contact__c from Contact_Team_Members__r) from contact LIMIT 50';
    public static final string constErrorStr1 = 'AP126_ContactTeamBatchable Exception occured:' ;
    public static final string constErrorStr2 = 'AP126_ContactTeamBatchable: Error with Task insertion : ';
    public static final string constErrorStr3 = 'AP126_ContactTeamBatchable Exception at DELETION : ' ;
    public static final string constErrorStr4 = 'AP126_ContactTeamBatchable :scope:' ;   
    public static final string constErrorStr5 =  ' conShareList:' ;
    public static final string constErrorStr6 = ' Total Errorlog: ' ;
    public static final string constErrorStr7 =  ' Batch Error Count errorCount: ' ;
    public static final string constErrorStr8 =  'IN AP 26: the total contact team members to be processed are: ';
    public static final string constErrorStr9 = 'IN AP 26 Finish: the total contact team members to be processed are: ';
    public static final string STR_Edit = 'Edit'; 
    public Integer i = 0;
    
     // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();


    /*
    *    Creation of Constructor
    */
    global AP126_ContactTeamBatchable() {       
        
         String qry='';
         qry =  'Select id, name, (select id, ContactTeamMember__c, Contact__c from Contact_Team_Members__r) from contact WHERE Contact_Status__c <> \'Inactive\' ';
         this.query= qry;
    }
    
    global AP126_ContactTeamBatchable(String qStr)  {        
         this.query= qStr;
    }
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // if(!Test.isRunningTest())
            return Database.getQueryLocator(query);
       // else
            //return Database.getQueryLocator(testquery);
    }

    /*
     *  Method Name: execute
     *  Description: Giving edit contact share to contact team members 
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        list<contact> contlist = scope;
        List < ContactShare > conShareList = new List < ContactShare > ();
        //List to hold Save Result 
        List<Database.SaveResult> conteamSaveResult = new List<Database.SaveResult>(); 
        Integer errorCount = 0;
        try {
            map<id, set<id>> mapofcontwithConTeam = new map<id, set<id>>();
        
        Map<Id, User> userIdMap = new Map<Id, User>([Select Id From User where  isActive=true ]);
        for(contact cc : contlist)
        {
            set<id> cctset = new set<id>();
            for(Contact_Team_Member__c ctm : cc.Contact_Team_Members__r)
            {       
                cctset.add(ctm.ContactTeamMember__c);
            }
            mapofcontwithConTeam.put(cc.id, cctset);
        }
        
        Map<Id ,Map<ID ,ContactShare >> mapConShareID = new Map<Id ,Map<ID ,ContactShare >>() ; 
        Map<ID ,ContactShare > mapContShare  ; 
        for (ContactShare cshare: [Select ContactId, UserOrGroupId, contact.ownerID, ContactAccessLevel FROM ContactShare where ContactId IN: mapofcontwithConTeam.keySet()])
        {
                        
                        if(mapConShareID.containsKey(cshare.contactId)) {
                            mapContShare = mapConShareID.get(cshare.contactId) ; 
                            
                        } else {
                            mapContShare = new Map<ID ,ContactShare >();
                        }
                        mapContShare.put(cshare.UserOrGroupId ,cshare ) ; 
                        mapConShareID.put(cshare.ContactId , mapContShare);
                        
        }
        Map<id, set<id>>  mapofconteam = new Map<id, set<id>> ();
        for(id cctid : mapofcontwithConTeam.keyset())
        {
            set<id> conteamid = new set<id>();
            conteamid = mapofcontwithConTeam.get(cctid);
            set<id> conteamid1 = new set<id>();
            for(id coteid : conteamid)
            {
                Map<ID ,ContactShare > conmap = mapConShareID.get(cctid);               
                if((!conmap.containskey(coteid) ||(conmap.containskey(coteid) && (conmap.get(coteid).ContactAccessLevel <> 'Edit' && conmap.get(coteid).ContactAccessLevel <> 'All'))) && userIdMap.containsKey(coteid))
                {    
                    ContactShare share = new ContactShare (); 
                    share.UserOrGroupId=coteid;
                    share.ContactId=cctid;
                    share.ContactAccessLevel= STR_Edit;                             
                                 
                    conShareList.add(share) ;       
                    conteamid1.add(coteid);
                }
                
            }
            if(!conteamid1.isempty())
            mapofconteam.put(cctid, conteamid1);        
        }
        
        for(id cct : mapofconteam.keyset())
        {
            set<id> conteamid2 = new set<id>();
            conteamid2 = mapofconteam.get(cct);
            i++;        
        }
                   
       } catch(Exception e)
         {
            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + e.getMessage(), errorLogs, scope[0].Id);             
            System.debug(constErrorStr1 +e.getMessage());
         } 
      
        //Insert Tasks into Contact share
        if(conShareList.size()>0)
        {
        
           conteamSaveResult = Database.insert(conShareList,true) ;
          
           //Iterate for Error Logging
            for(Database.Saveresult result : conteamSaveResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCount++;
                }
                
              }           
        }
        
       
         //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatusforcount = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         
         errorLogStatusforcount  = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr8 + i ,  errorLogStatusforcount  , scope[0].Id);
         //Insert to ErrorLogStatus
         insert  errorLogStatusforcount ;
         
         //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         
         errorLogStatus  = MercerAccountStatusBatchHelper.addToErrorLog(constErrorStr4  + scope.size() +constErrorStr5  + conShareList.size() + constErrorStr6 +errorLogs.size() + constErrorStr7 +errorCount  ,  errorLogStatus  , scope[0].Id);
         //Insert to ErrorLogStatus
         insert  errorLogStatus ;
        
        
    }
    
    

    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC) {
        
        List<BatchErrorLogger__c> errorLogStatusforcount = new List<BatchErrorLogger__c>();
        //Values that will be stored while Error Logging
        errorLogStatusforcount  = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr9 + i ,  errorLogStatusforcount  , null);
        //Insert to ErrorLogStatus
        insert  errorLogStatusforcount ;
         
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  

    }
    
  
    
}