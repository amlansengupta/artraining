/*Purpose:  This Apex class contains static methods to process Sales Credit records on insertion of Opportunity Team Members for a particular Opportunity==============================================================================================================================================
The methods called perform following functionality:

•   On Insertion of Sales Credit Record 0% Allocation and a Role is assigned to User.  
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sujata   01/09/2013  Created Apex to support TRG03
   2.0 -    Reetika  22/07/2013  Create Apex to Support TRG07_SalesCreditTrigger/Requirement#2590
                                 (Update Concatenated Opportunity Team field=Employee Team<space>Percent Allocaiton)

============================================================================================================================================== 
*/

public class AP15_SalesCreditTeamTriggerUtil {

    //Map between User ID and OpportunityTeamMember record ID.
    public static Map<ID,ID> userOppMap = new Map<ID,ID>();
    
    //Map between User ID and OpportunityTeamMember record ID.
    public static Map<ID,sales_credit__c> salesCredMap = new Map<ID,sales_credit__c>();
    
    //Set to contain ID of Opportunity corresponding to Opportunity Team Member.
    public static Set<ID> OppIDList = new Set<ID>();
    
    public static ID parentOppID ;
    
    //Map to contain Opportunity ID and its corresponding Sales Credit Team Members List as key,value.
    public static Map<ID,List<Sales_Credit__c>> SCreditTeamMemberMap = new Map<ID,List<Sales_Credit__c>>();
    
    //Map between User's Employee Number and OpportunityTeamMember record ID.
    public static Map<String,ID> empNoOppMap = new Map<String,ID>();
    
    //Map between OpportunityTeamMember record ID and Colleague ID.
    public static Map<ID,ID> collOppMap = new Map<ID,ID>();
    public static List<Sales_Credit__c> scList=new List<Sales_Credit__c>();
    //List of Sales Credit records to be inserted.
    public static List<Sales_Credit__c> sCreditTeamList = new List<Sales_Credit__c>();
    private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember(); 
 
    
    /*
     * @Description : This method is called on after insertion of OpportunityTeamMember records
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
    public static void validateSalesCreditAfterInsert2(List<OpportunityTeamMember> oppteams)
    {
        //clear cache (to avoid re-insertion of same IDs).
        OppIDList.clear();
        SCreditTeamMemberMap.clear();
        userOppMap.clear();
        sCreditTeamList.clear();
        Decimal sum=0.00;
        Map<id, String> prodExpandOpp=new Map<id, String>();
        try
        {
            
            for(OpportunityTeamMember oppT : oppteams)
            {
                userOppMap.put(oppT.userID,oppT.ID);
                OppIDList.add(oppT.OpportunityId);  
            }
            
            
            if(!OppIDList.isEmpty())
            {    
                for(Opportunity opp : [Select Id, Name,Ownerid,Opportunity_Region__c,Parent_Opportunity_Name__c,Type, (Select Id, Opportunity__c,EmpName__c,EmpName__r.EMPLID__c FROM Sales_Creditings__r  ) FROM Opportunity WHERE Id IN :OppIDList])
                {    
                                System.debug('oppid***********'+opp.id);
                                System.debug('Parent_Opportunity_Name__c*************'+opp.Parent_Opportunity_Name__c);
                      SCreditTeamMemberMap.put(opp.Id,opp.Sales_Creditings__r); 
                      
                      if(opp.Parent_Opportunity_Name__c!=null ){
                          parentOppID = opp.Parent_Opportunity_Name__c ;
                          prodExpandOpp.put(parentOppID,opp.Opportunity_Region__c);

                      }
                        System.debug('prodExpandOpp***********'+prodExpandOpp);
                        System.debug('parentOppID ***********'+parentOppID );

                      System.debug('SCreditTeamMemberMap************'+SCreditTeamMemberMap); 
                }
            }
            if(parentOppID !=null){
                scList =  [select Id,IsCreatedFromTeamMember__c,CreatedById,LastModifiedById,Opportunity__c,Opportunity__r.recordtype.name,Opportunity__r.Opportunity_Region__c,Work_Email__c,Employee_Office__c, EmpName__c, Role__c, LOB__c,Sales_Credit__c, Percent_Allocation__c,Employee_Status__c, Country__c from Sales_Credit__c where Opportunity__c =:parentOppID ];
                for(Sales_Credit__c sc:scList){
                    salesCredMap.put(sc.EmpName__c,sc);
                    
                    if(sc.Opportunity__r.Opportunity_Region__c=='International' || prodExpandOpp.get(sc.Opportunity__c)=='International'){
                        sum=sum+sc.Percent_Allocation__c;
                    
                    }
                }
                System.debug('scList errer************'+scList ); 
            }
            
            for(User usr : [Select ID,EmployeeNumber from User where ID IN : userOppMap.keyset()])
            {
                if(usr.EmployeeNumber <> null)
                    empNoOppMap.put(usr.EmployeeNumber,userOppMap.get(usr.ID));
                    System.debug('empNoOppMap************'+empNoOppMap);
                    
            }
                          
            for(Colleague__c coll : [Select EMPLID__c,ID from Colleague__c where EMPLID__c IN : empNoOppMap.keyset()])
            {
                collOppMap.put(empNoOppMap.get(coll.EMPLID__c),coll.ID);
                System.debug('collOppMap************'+collOppMap);
            }
            
            
                    for(OpportunityTeamMember otm : oppteams)
                    {
                        
                        
                                boolean hasMatch = false;
                                
                                //Checks if the Sales Credit Employee to be inserted already exists in the Sales Credit Team for that Opportunity.
                                for(Sales_Credit__c teamMember : SCreditTeamMemberMap.get(otm.OpportunityId))
                                {    System.debug('collOppMap.get(otm.ID)************'+collOppMap.get(otm.ID)); 
                                    System.debug('teamMember.EmpName__c************'+teamMember.EmpName__c);  
                                     if(collOppMap.get(otm.ID) == teamMember.EmpName__c )
                                     {
                                         hasMatch = true;
                                         break;
                                         System.debug('hasMatch ************'+hasMatch );  
                                     }                  
                                }
                                
                                //If the Sales Credit Employee is a new member.
                                if(!hasMatch)
                                {    System.debug('1******************************');
                                    if(collOppMap.get(otm.Id) <> null )
                                    {
                                        System.debug('2******************************'+collOppMap.get(otm.Id));
                                        Sales_Credit__c sCredit = new Sales_Credit__c();
                                        sCredit.IsCreatedFromTeamMember__c = true;
                                        
                                        if(parentOppID !=null){
                                             
                                             if(salesCredMap.containsKey(collOppMap.get(otm.Id))) {
                                                 
                                                    Sales_Credit__c scd=new Sales_Credit__c();
                                                    scd= salesCredMap.get(collOppMap.get(otm.Id)).clone(false,true);
                                                    if(sum > 100){
                                                        scd.Percent_Allocation__c=0.00; 
                                                    }
                                                    scd.CreatedById=salesCredMap.get(collOppMap.get(otm.Id)).CreatedById;
                                                    scd.LastModifiedById=salesCredMap.get(collOppMap.get(otm.Id)).LastModifiedById;
                                                    scd.Opportunity__c=otm.OpportunityId;
                                                 if(otm.TeamMemberRole!=null && otm.TeamMemberRole == 'Opportunity Owner')
                                                     scd.Role__c = '';
                                                 else
                                                    scd.Role__c = otm.TeamMemberRole;                                                    
                                                    sCreditTeamList.add(scd);
                                                 System.debug('&&&&'+sCreditTeamList);
                                             
                                             }else{
                                                     sCredit.Opportunity__c = otm.OpportunityId; 
                                                    sCredit.EmpName__c = collOppMap.get(otm.ID);
                                                    sCredit.Percent_Allocation__c = 0;
                                                    if(otm.TeamMemberRole !=null && otm.TeamMemberRole == 'Opportunity Owner')
                                                     sCredit.Role__c = '';
                                                 else
                                                    sCredit.Role__c = otm.TeamMemberRole;                                                     
                                                    sCreditTeamList.add(sCredit); 
                                                 System.debug('&&&&1'+sCreditTeamList);

                                            }
                                        
                                        }else{
                                        
                                            sCredit.Opportunity__c = otm.OpportunityId; 
                                            sCredit.EmpName__c = collOppMap.get(otm.ID);
                                            sCredit.Percent_Allocation__c = 0;
                                            if(otm.TeamMemberRole !=null && otm.TeamMemberRole == 'Opportunity Owner')
                                                     sCredit.Role__c = '';
                                                 else
                                                    sCredit.Role__c = otm.TeamMemberRole;                                                                                               
                                            sCreditTeamList.add(sCredit); 
                                            System.debug('&&&&2'+sCreditTeamList);
                                        
                                        }
                                        
                                        System.debug('scredit************'+sCredit);  
                                    }
                                      
                                }
                            
                    }
                
            System.debug('sCreditTeamList********'+sCreditTeamList.size());
            if(!sCreditTeamList.isEmpty()){
                //if(ConstantsUtility.oppTeamsAfterUpdatekip){
                    AP44_ChatterFeedReporting.FROMFEED = true;
                //}         
                insert sCreditTeamList;
                
                
            } 
        }
        
        catch(TriggerException tEx)
        {
            System.debug('Exception occured at SalesCredit Trigger processSalesCreditAfterInsert with reason :'+tEx.getMessage()+tEx.getStackTraceString());    
            oppTeams[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
        
        }
       /* catch(Exception ex)
        {
            System.debug('Exception occured at SalesCredit Trigger processSalesCreditAfterInsert with reason :'+Ex.getMessage());    
            oppTeams[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'+Ex.getStackTraceString()); 
        
        }*/
    }
    
    /*
     * @Description : This method is called on after insert/UPDATE/Delete of Sales Credit records
     * And updates data in Concatenated Opp Team field, data pulled from Sales Credit - "Employee name <space> Percentage allocation"
     * @ Args       : Trigger.new/Trigger.old
     * @ Return     : void
     */
    public static void processSalesCredit(List<Sales_Credit__c> triggernew)
    {
        List<Opportunity> oppList = new List<Opportunity>();
        Map<string, string> SalesCreditIdset = new Map<string, string>();
        
        try
        {
            // Iterate through Trigger.new
            for(Sales_Credit__c SalesCredit : triggernew)
            {
                // Add to set
                salesCreditIdset.put(SalesCredit.Opportunity__c, salesCredit.Id);
            }
            if(!isDMUser) {
                if(!SalesCreditIdset.isEmpty())
                {
                    for(Opportunity opp : [select Id, (select Id,Percent_Allocation__c, EmpName__r.Name from Sales_Creditings__r) from Opportunity where Id IN :SalesCreditIdset.Keyset()])
                    {
    
                            String teamMembers = '';
                            Integer count = 1;
                            for(Sales_Credit__c salesCredit : opp.Sales_Creditings__r)
                            {
                                if(count == 1)
                                    teamMembers = salesCredit.EmpName__r.Name + ' ' + salesCredit.Percent_Allocation__c + '%' ;
                                else
                                    teamMembers = teamMembers + ', ' + salesCredit.EmpName__r.Name + ' ' + salesCredit.Percent_Allocation__c + '%';
                                count++;    
                            }
                           
                            opp.Concatenated_Opportunity_Team_Member__c = teamMembers;
                            oppList.add(opp);
                            
                        
                    }
                    if(oppList.size()>0) {
                        Database.update(oppList,false);
                    }
                }
            }
        }
        catch(TriggerException tEx)
        {
            System.debug('Exception occured at SalesCredit Trigger processSalesCredit with reason :'+tEx.getMessage());
        }
        catch(Exception ex)
        {
            System.debug('Exception occured at SalesCredit Trigger processSalesCredit with reason :'+Ex.getMessage());
        }
    }

}