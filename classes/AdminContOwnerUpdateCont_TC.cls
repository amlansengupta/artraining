/*Purpose: This test class provides data coverage to AdminContOwnerUpdateCont class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan 05/02/2013  Created test class
============================================================================================================================================== 
*/
@isTest (SeeAllData=true)
private class AdminContOwnerUpdateCont_TC {
    /*
     * @Description : Test method to provide data coverage to AdminContOwnerUpdateCont class 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void init() {
    
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        system.runAs(user1){
        Contact[] Cons   = [SELECT Id, Contact_ID2__c  FROM Contact LIMIT 2] ;
        User[] Users = [SELECT Id, EmployeeNumber  FROM User WHERE EmployeeNumber  != NULL LIMIT 2 ];
            
        PageReference pageRef = Page.AdminContOwnerUpdate;
        
        Test.setCurrentPage(pageRef);         
        
        AdminContOwnerUpdateCont a = new AdminContOwnerUpdateCont();
        
        String CSVFile  = 'contact ID,Owner PeopleSoft Id \n ' ;
               CSVFile += Cons[0].Contact_ID2__c  +','+ Users[0].EmployeeNumber   ;
        Test.startTest();       
        a.csvFile = Blob.valueOf(CSVFile);
        a.FetchContDetails();
        a.updatecons () ;
        Test.stopTest();
      }
      
     
    }
}