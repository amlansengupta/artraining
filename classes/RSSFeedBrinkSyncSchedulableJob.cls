global class RSSFeedBrinkSyncSchedulableJob implements Schedulable {
    global void execute(SchedulableContext context) {
        // get the rss feed
        RSSFeedSyncJob.syncFeeds(RSSFeedDownloadService.FEED_TYPE_BRINK);
    }

    global void runExecuteMethod() {
        // get the rss feed
        RSSFeedSyncJob.syncFeeds(RSSFeedDownloadService.FEED_TYPE_BRINK);
    }
}