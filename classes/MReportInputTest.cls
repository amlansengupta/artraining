@isTest(SeeAllData=true)
public class MReportInputTest
{
    
    static TestMethod void MyMethod1()
    {
           Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        Account a = new Account();
        a.Name = 'Test1';
        a.One_Code__c = 'ARFB12';
        a.GU_DUNS__c = '1256';   
        a.DUNS__c = '1256';
        a.Global_Ultimate_Name__c = 'Test GUNAME';
        a.Relationship_Manager__c = testColleague.Id;
        //a.Relationship_Manager_Mobile__c =  testColleague.Name;
        a.Account_Country__c = 'India';
        a.Account_Region__c = 'EuroPac';
        a.Account_Market__c = 'ASEAN';
        a.Market_Segment__c = '4 - Key';
        a.Talent_CY_Revenue__c = 45.35;
        a.Health_CY_Revenue__c = 89.52;
        a.Retirement_CY_Revenue__c = 75.96;
        a.Investments_CY_Revenue__c = 52.36;
        a.Outsourcing_CY_Revenue__c = 56.63;
        a.Other_CY_Revenue__c = 42.56;
        a.Talent_PY_Revenue__c = 145.35;
        a.Health_PY_Revenue__c = 189.52;
        a.Retirement_PY_Revenue__c = 175.96;
        a.Investments_PY_Revenue__c = 152.36;
        a.Outsourcing_PY_Revenue__c = 156.63;
        a.Other_PY_Revenue__c = 42.56;
        a.Industry = 'Agriculture';
        a.Primary_Industry_Category__c = 'test';
        a.Employees_Editable__c = 123;
        a.billingstreet = 'A-302';
        a.billingcountry = 'India';
        a.Total_CY_Revenue__c = 452.85;
        a.Total_PY_Revenue__c = 879.52;
        a.Total_PY_Revenue__c = 50.00;
        insert a;
        
     
        
       PageReference pg = Page.MReportGenerator1;
       Test.setCurrentPage(pg); 
       
       ApexPages.currentPage().getParameters().put('onecode','ARFB12');
       ApexPages.currentPage().getParameters().put('guduns','1256');
       ApexPages.currentPage().getParameters().put('guname','Test GUNAME');
       ApexPages.currentPage().getParameters().put('planningyear','2016');
        
       
        
         MReportInput1 controller = new MReportInput1();
         controller.redirect();
        
        
    }
    static TestMethod void MyMethod2()
    {
        PageReference pg = Page.MReportGenerator1;
       Test.setCurrentPage(pg); 
       ApexPages.currentPage().getParameters().put('onecode','');
       ApexPages.currentPage().getParameters().put('guduns','');
       ApexPages.currentPage().getParameters().put('guname','');
       ApexPages.currentPage().getParameters().put('planningyear','2016');
        MReportInput1 controller1 = new MReportInput1();
         controller1.redirect();
        controller1.getFromPlanningVersion();
    }
}