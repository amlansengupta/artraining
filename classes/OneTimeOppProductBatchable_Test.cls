/*
==============================================================================================================================================
Request Id                                			 Date                    Modified By
12638:Removing Step 								 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData=True)
public class OneTimeOppProductBatchable_Test {
    
    static testMethod void method1(){
        
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            insert testAccount;
                
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //request id:12638 commenting step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Opportunity_Region__c='Europac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            insert testOppty1;
            
             Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty1.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        insert opptylineItem;
            
         Test.startTest();

            OneTimeOppProductBatchable obj = new OneTimeOppProductBatchable();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();   
         }
    }
}