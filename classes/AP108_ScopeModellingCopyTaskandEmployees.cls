/*Purpose:  This Apex class is created as a Part of September 2014  Release for Req#4918
This class is to copy scopemodelling  and scopeitTemplate record along with related list data.
==============================================================================================================================================
History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
1.0         Madhavi     8/4/2014    September 2014  Release for Req#4918-copy scopemodelling and scopeitTemplate  record along with related list data
2.0         Madhavi     10/16/2014  As part of  december 2014 release(request #4881) added IC_Charges__c,IC_Charge_Type__c to scopemodelling task and scopeitTask template query.
============================================================================================================================================= 
*/
Global without sharing class AP108_ScopeModellingCopyTaskandEmployees{
    private static final string STR_Id ='Id=:ModId';
    private static final string STR_TEMPID ='Id=:tempId';  
    /*
* @Description : This method is called on click of copy button on scope modelling
* @ Args       : String
* @ Return     : String
*/
    @AuraEnabled
    webservice static String CopyModellingTaskEmployee(String ModId){
        Scope_Modeling__c  objmod = new Scope_Modeling__c();
        SavePoint newmodSP = Database.setSavepoint();
        String Message=ConstantsUtility.STR_BLNK;
        try{
            //get querystring for Scope Modelling.
            String Uid = userinfo.getUserId();
            String modquery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL(ConstantsUtility.STR_OBJ,STR_Id);
            Scope_Modeling__c   modrec = Database.Query(modquery);
            objmod =modrec.clone();
            objmod.name = modrec.name +ConstantsUtility.STR_SYM+ConstantsUtility.STR_COPY;
            objmod.ownerID = Uid;
            //insert scope modelling record.
            database.insert(objmod);   
            List<Scope_Modeling_Task__c>  tasklist = new List<Scope_Modeling_Task__c>();
            List<Scope_Modeling_Task__c>  newtasklist = new List<Scope_Modeling_Task__c>();
            //get Task and employee records associated with scope modelling
            Map<Scope_Modeling_Task__c,List<Scope_Modeling_Employee__c>>employeemap  = new Map<Scope_Modeling_Task__c,List<Scope_Modeling_Employee__c>>();
            //Clone Scope meodelling Task  records.
            //As part of  december 2014 release(request #4881) added IC_Charges__c,IC_Charge_Type__c to the query.
            for(Scope_Modeling_Task__c tas:[select id,CurrencyIsoCode,Bill_Est_of_Increases__c,Bill_Rate__c,IC_Charge_Type__c,IC_Charges__c,Bill_Type01__c,Billable_Expenses__c,Scope_Modeling__c,                                      
                                            Task_Name__c,(select id,Cost__c,CurrencyIsoCode,Scope_Modeling__c,Scope_Modeling_Task__c,Bill_Rate_Opp__c,Billable_Time_Charges__c,                                      
                                                          Employee_Bill_Rate__r.Name,Employee_Bill_Rate__c, Hours__c from Scope_Modeling_Employees__r)
                                            from Scope_Modeling_Task__c where Scope_Modeling__c=:ModId]){
                                                Scope_Modeling_Task__c taskrec = tas.Clone(false);
                                                taskrec.Scope_Modeling__c = objmod.id;
                                                newtasklist.add(taskrec);
                                                employeemap.put(taskrec,tas.Scope_Modeling_Employees__r.deepClone(false));
                                            }
            //insert scopemodelling task records
            database.insert(newtasklist);
            //insertscopemodelling employees
            List<Scope_Modeling_Employee__c> employeelist = new List<Scope_Modeling_Employee__c>();
            for(Scope_Modeling_Task__c tsk:employeemap.keySet()){
                for(Scope_Modeling_Employee__c emp:employeemap.get(tsk)){
                    emp.Scope_Modeling__c = objmod.id;
                    emp.Scope_Modeling_Task__c = tsk.id;
                    employeelist.add(emp);
                }
            }
            database.insert(employeelist);
            //return success
            message = objmod.id;
            return message ;
        }catch(Exception e){
            Database.rollback(newmodSP);
            //return failue with exception
            message =ConstantsUtility.STR_EXM +e;
            return message;
        }
    }
    /*
* @Description : This method is called on click of copy button on scopeit template.
* @ Args       : String
* @ Return     : String
*/
    @AuraEnabled
    webservice static String CopyTemplateTaskEmployee(String tempId){
        Temp_ScopeIt_Project__c  objmod = new Temp_ScopeIt_Project__c();
        SavePoint newmodSP = Database.setSavepoint();
        String Message=ConstantsUtility.STR_BLNK;
        String Uid = userinfo.getUserId();
        try{
            //get querystring for ScopeItTemplate.
            String modquery = AP107_ScopeItCopyTaskandEmployees.getCreatableFieldsSOQL(ConstantsUtility.STR_TEMPOBJ,STR_TEMPID);
            Temp_ScopeIt_Project__c   modrec = Database.Query(modquery);
            objmod =modrec.clone();
            objmod.name = modrec.name +ConstantsUtility.STR_SYM+ConstantsUtility.STR_COPY;
            objmod.ownerId = Uid;
            //insert ScopeItTemplate record.
            database.insert(objmod);
            List<Temp_ScopeIt_Task__c>  tasklist = new List<Temp_ScopeIt_Task__c>();
            List<Temp_ScopeIt_Task__c>  newtasklist = new List<Temp_ScopeIt_Task__c>();
            //get Task and employee records associated with scopeitTemplate
            Map<Id,List<Temp_ScopeIt_Employee__c>> employeemap  = new Map<Id,List<Temp_ScopeIt_Employee__c>>();
            Map<Id,Temp_ScopeIt_Task__c> taskMap = new Map<Id,Temp_ScopeIt_Task__c>();
            //Clone ScopeItTemplate Task  records.
            //As part of  december 2014 release(request #4881) added IC_Charges__c,IC_Charge_Type__c to the query.
            for(Temp_ScopeIt_Task__c tas:[select id,CurrencyIsoCode,Bill_Est_of_Increases__c,Bill_Rate__c,IC_Charges__c,IC_Charge_Type__c,Bill_Type__c,Billable_Expenses__c,Task_Name__c,Temp_ScopeIt_Project__c,
                                          (select CurrencyIsoCode,Billable_Time_Charges__c,Bill_Cost_Ratio__c,Bill_Rate__c,Business__c,Cost__c,Employee__c,Hours__c,Market_Country__c,
                                           Name__c,ScopeIt_Project_Template__c,Temp_ScopeIt_Task__c from Temp_ScopeIt_Employees__r)
                                          from Temp_ScopeIt_Task__c where Temp_ScopeIt_Project__c=:tempId]){
                                              Temp_ScopeIt_Task__c taskrec = tas.clone(false,false,false,false);
                                              taskrec.Temp_ScopeIt_Project__c = objmod.id;
                                              newtasklist.add(taskrec);
                                              taskMap.put(taskrec.getCloneSourceId(), taskrec);
                                              List<Temp_ScopeIt_Employee__c> scopeItEmployees = new List<Temp_ScopeIt_Employee__c>();
                                              for(Temp_ScopeIt_Employee__c scopeItEmployee : tas.Temp_ScopeIt_Employees__r){
                                                  Temp_ScopeIt_Employee__c clonedScopeItEmployee = scopeItEmployee.clone(false,true,false,false);
                                                  clonedScopeItEmployee.Temp_ScopeIt_Task__c = null;
                                                  scopeItEmployees.add(clonedScopeItEmployee);
                                              }                       
                                              employeemap.put(taskrec.getCloneSourceId(),scopeItEmployees);
                                          }
            //insert ScopeItTemplate task records
            database.insert(newtasklist);
            //insert ScopeItTemplate employees
            List<Temp_ScopeIt_Employee__c> employeelist = new List<Temp_ScopeIt_Employee__c>();
            for(Id key : employeemap.keySet()){
                system.debug('key:'+key);
                for(Temp_ScopeIt_Employee__c emp:employeemap.get(key)){
                    emp.ScopeIt_Project_Template__c = objmod.id;
                    emp.Temp_ScopeIt_Task__c = taskMap.get(key).Id;
                    employeelist.add(emp);
                }
            }
            database.insert(employeelist);
            //return success
            message = objmod.id;
            return message ;
        }catch(Exception e){
            system.debug(e.getMessage()+' : '+e.getStackTraceString());
            Database.rollback(newmodSP);
            //return failue with exception
            message =ConstantsUtility.STR_EXM +e;
            return message;
        }
    }
}