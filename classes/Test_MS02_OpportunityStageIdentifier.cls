/*Purpose: Test Class for providing code coverage to MS02_OpportunityStageIdentifier class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
=======================================================
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_MS02_OpportunityStageIdentifier  {
    /* * @Description : This test method provides data coverage to MS02_OpportunityStageIdentifier class         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest()
     {
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'TestColleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        insert Acc;
        
        Opportunity opp= new Opportunity();
        opp.Name = 'Test Opportunity' + String.valueOf(Date.Today());
        opp.Type = 'New Client';
        //Request Id:12638 - commenting step__c
        //opp.Step__c = 'Identified Deal';
        opp.StageName = 'Active Pursuit';
        opp.CloseDate = date.Today();
        opp.CurrencyIsoCode = 'ALL';
        opp.AccountId = Acc.id;
         opp.Opportunity_Office__c='Hoboken - River';
        test.starttest();
        insert opp;
        
        
    /*
   
    string baseQuery = 'Select Id, StageName, AccountId From Opportunity ';
    string whereClause = 'where StageName IN (\'Active Pursuit\')';

        string query = baseQuery + whereClause;
      */  
       system.runAs(User1){
        database.executeBatch(new MS02_OpportunityStageIdentifier(), 500);
       }
       test.stoptest();
        //Account account=[Select Id, StageName, AccountId, Account.MercerForce_Account_Status__c From Opportunity Where AccountId = :opp.AccountId];
        //System.assertEquals(Oppty.Account.MercerForce_Account_Status__c,'Open Opportunity - Stage 2+');
    }
}