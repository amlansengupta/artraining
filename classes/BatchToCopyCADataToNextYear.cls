global class BatchToCopyCADataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopyCADataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String header = 'Account,Account Footprint And Strengths,Account’s Perception of Competitor,Area,Competitor,Estimated Wallet Share,Growth Plan,Their Key Executive Sponsor (s),Record Status \n';
        string hearder2= 'Id,Account,Account Footprint And Strengths,Account’s Perception of Competitor,Area,Competitor,Estimated Wallet Share,Growth Plan,Their Key Executive Sponsor (s),Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear   ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>(); 
        System.debug('Competitive oldGPList>>>'+oldGPList.size());
        try {
			for(Growth_Plan__c gpOldobj :oldGPList){
					accountIdSet.add(gpOldobj.Account__c);
					
			}
			
				
			Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
			List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
			for(Account a : accList){
				for(Growth_Plan__c gg : a.Growth_Plans__r){
					accIdVsNewGp.put(gg.Account__c,gg);
				}  
			}
	         System.debug('Competitive accList>>>'+accList.size());
	        
	        List<Competitive_Assessment__c> insertNewCAObjList = new List<Competitive_Assessment__c>();
	        List<Competitive_Assessment__c> caObjectList = [select id,name,Account__c,Growth_Plan__c,Area__c,Account_Footprint_And_Strengths__c,Account_s_Perception_of_Competitor__c,Competitor__c,Estimated_Wallet_Share__c,Their_Key_Executive_Sponsor_s__c,LastModifiedBy.Name,lastModifiedDate from Competitive_Assessment__c where Growth_Plan__c IN:oldGPList] ;
	    	  System.debug('Competitive caObjectList>>>'+caObjectList.size());
	        if(caObjectList != null && caObjectList.size()>0){
				for(Competitive_Assessment__c caOldObj : caObjectList){
					if(accIdVsNewGp.containsKey(caOldObj.Account__c)){
					
						Competitive_Assessment__c caNewObj = new Competitive_Assessment__c();
						if(caOldObj.Account__c != null)
							caNewObj.Account__c = caOldObj.Account__c;
						caNewObj.Growth_Plan__c = accIdVsNewGp.get(caOldObj.Account__c).Id;
						
						if(caOldObj.Area__c != null)
							caNewObj.Area__c = caOldObj.Area__c;
						if(caOldObj.Account_Footprint_And_Strengths__c != null)
							caNewObj.Account_Footprint_And_Strengths__c = caOldObj.Account_Footprint_And_Strengths__c;
						if(caOldObj.Account_s_Perception_of_Competitor__c != null)
							caNewObj.Account_s_Perception_of_Competitor__c = caOldObj.Account_s_Perception_of_Competitor__c;
						if(caOldObj.Competitor__c != null)
							caNewObj.Competitor__c = caOldObj.Competitor__c;
						if(caOldObj.Estimated_Wallet_Share__c != null)
							caNewObj.Estimated_Wallet_Share__c = caOldObj.Estimated_Wallet_Share__c;
						if(caOldObj.Their_Key_Executive_Sponsor_s__c != null)
							caNewObj.Their_Key_Executive_Sponsor_s__c= caOldObj.Their_Key_Executive_Sponsor_s__c;
                        
                        caNewObj.Assessment_LastModified_By__c = caOldObj.LastModifiedBy.Name;
                        DateTime dt = caOldObj.LastModifiedDate;
                        caNewObj.Assessment_LastModified_Date__c = Date.newInstance(dt.year(), dt.month(), dt.day());
						
						insertNewCAObjList.add(caNewObj);
					}
				}
			}
			System.debug('Competitive insertNewCAObjList>>>'+insertNewCAObjList.size());
	        if(insertNewCAObjList != null && insertNewCAObjList.size()>0){
		    		Database.SaveResult[] srList = Database.insert(insertNewCAObjList,false);
		    		if(srList!=null && srList.size()>0){
		    			
		    				
		    				
	    				for(Integer i=0;i<srList.size();i++){
	    					String accountFoot='',accountPerception='',areaStr='',CompetitorStr='',estimatedWallet='',KeyExecute='';
		    			
		    			if(insertNewCAObjList[i].Account_Footprint_And_Strengths__c!=null)
		    				accountFoot=insertNewCAObjList[i].Account_Footprint_And_Strengths__c;
		    				
		    			if(insertNewCAObjList[i].Account_s_Perception_of_Competitor__c!=null)
		    				accountPerception=insertNewCAObjList[i].Account_s_Perception_of_Competitor__c;
		    				
		    			if(insertNewCAObjList[i].Area__c!=null)
		    				areaStr=insertNewCAObjList[i].Area__c;
		    				
		    			if(insertNewCAObjList[i].Competitor__c!=null)
		    				CompetitorStr=insertNewCAObjList[i].Competitor__c;
		    				
		    			if(insertNewCAObjList[i].Estimated_Wallet_Share__c!=null)
		    				estimatedWallet=insertNewCAObjList[i].Estimated_Wallet_Share__c;
		    				
		    			if(insertNewCAObjList[i].Their_Key_Executive_Sponsor_s__c!=null)
		    				KeyExecute=insertNewCAObjList[i].Their_Key_Executive_Sponsor_s__c;
	    					if(srList.get(i).isSuccess()){
	    						finalSuccessStr+=srList.get(i).getId()+','+insertNewCAObjList[i].Account__c+','+accountFoot.replace(',', '')+','+accountPerception.replace(',', '')+','+areaStr.replace(',', '')+','+CompetitorStr.replace(',', '')+','+estimatedWallet.replace(',', '')+','+insertNewCAObjList[i].Growth_Plan__c+','+KeyExecute.replace(',', '')+', Successfully created \n';
	    					}
	    					if(!srList.get(i).isSuccess()){
	    						Database.Error error = srList.get(i).getErrors().get(0);
	    						finalFailStr+=insertNewCAObjList[i].Account__c+','+accountFoot.replace(',', '')+','+accountPerception.replace(',', '')+','+areaStr.replace(',', '')+','+CompetitorStr.replace(',', '')+','+estimatedWallet.replace(',', '')+','+insertNewCAObjList[i].Growth_Plan__c+','+KeyExecute.replace(',', '')+','+error.getMessage()+' \n';
	    					}
	    				}
	    			}
		    	}
	
			System.debug('Competitive finish finalSuccessStr>>>>'+finalSuccessStr);
			System.debug('Competitive finish finalFailStr>>>> '+finalFailStr);
		}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        } 
    }
    
    global void finish(Database.BatchableContext bc){
    	
    	System.debug('Competitive finish finalSuccessStr>>>>'+finalSuccessStr);
		System.debug('Competitive finish finalFailStr>>>> '+finalFailStr);
    	
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Competitive Assessment Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Competitive Assessment Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Competitive Assessment Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Competitive Assessment copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}