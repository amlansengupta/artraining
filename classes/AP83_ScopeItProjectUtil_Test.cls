/*Purpose: Test class to provide test coverage for AP83_ScopeItProjectUtil_Test class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/1/2014  Created test class to provide test coverage for AP83_ScopeItProjectUtil_Test class
 
============================================================================================================================================== 
*/
 @isTest(seeAllData = true)
private class AP83_ScopeItProjectUtil_Test {

        
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData(); 
    private static Integer month = System.Today().month();
    private static Integer year = System.Today().year();   

    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestCrossBusinessOfferingsCBO() {  
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Cross Business Offerings (CBO)';
            String billType = 'Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
           
           Test.startTest();
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           Test.stopTest();
           
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
           String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
                           
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();               
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           
        }    
    }
    
    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestEmployeeHealthBenefits() {  
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Employee Health & Benefits';
            String billType = 'Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
           
           Test.startTest();
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           Test.stopTest();
           
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
           String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
                           
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();               
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           
        }    
    }
    
    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestBenefitsAdmin() {  
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Benefits Admin';
            String billType = 'Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
           
           Test.startTest();
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           Test.stopTest();
           
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
           String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
                           
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();               
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           
        }    
    }
    
    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestInvestments() {  
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Investments';
            String billType = 'Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
           
           Test.startTest();
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           Test.stopTest();
           
            OpportunityLineItem  opptylineItem2 =new OpportunityLineItem(); 
            opptylineItem2.OpportunityId = opptyId;
            opptylineItem2.PricebookEntryId = pbEntryId;
            /* opptyLineItem2.Revenue_End_Date__c = date.parse('1/10/2015');
            opptyLineItem2.Revenue_Start_Date__c = date.parse('1/1/2015'); */
            //Updated start date and end dates by Jyotsna
            opptyLineItem2.Revenue_Start_Date__c = date.newInstance(year, month, 1);
            opptyLineItem2.Revenue_End_Date__c = date.newInstance(year, month, 10);
            opptylineItem2.UnitPrice = 1.00;
            opptyLineItem2.CurrentYearRevenue_edit__c = 1;
            //opptyLineItem2.Year2Revenue_edit__c = 1;  
            opptyLineItem2.Inv_Segment__c = 'Corporates/Employers';
            opptyLineItem2.Inv_Client_Type__c = 'Charity';
            insert opptyLineItem2;
                       
           ID oliId= opptylineItem2.id;
           String oliCurr = opptylineItem2.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem2.unitprice;
           String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
                           
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();               
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           
        }  
    }
    
    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestTalent() {  
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Talent';
            String billType = 'Non-Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
           
           Test.startTest();
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           Test.stopTest();
           
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
           String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
           
                           
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();               
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           
        }    
    }
    
    
}