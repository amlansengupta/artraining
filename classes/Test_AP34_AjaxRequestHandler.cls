/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -       Arijit  03/29/2013  Created AjaxHandler class
=======================================================
Modified By     Date         Request
Trisha          17-Jan-2019  12638-Commenting Oppotunity Step
Trisha          21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify' 
=======================================================
=============================================================================================================================================== 
*/
@isTest
private class Test_AP34_AjaxRequestHandler 
{
    /*
     * @Description : Test method to provide data coverage to AP34_AjaxRequestHandler class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        List<Opportunity> lstOpp = new List<Opportunity>();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        testOppty.Total_Opportunity_Revenue_USD__c = 1;
        //Request id:12638 Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.Opportunity_Office__c='New York - 1166';
        //testOppty.
       // insert testOppty;
       lstOpp.add(testOppty);
        //Test class fix
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOppty1';
        testOppty1.Type = 'New Client';
        testOppty1.AccountId = testAccount.Id;
        testOppty1.Total_Opportunity_Revenue_USD__c = 1;
        testOppty1.Parent_Opportunity_Name__c = testOppty.Id;
        //Request id:12638 Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty1.StageName = 'Identify';
        testOppty1.CloseDate = date.Today();
        testOppty1.Opportunity_Office__c='New York - 1166';
        //insert testOppty1;
        lstOpp.add(testOppty1);
        insert lstOpp;        
        AP34_AjaxRequestHandler.lockOpportunity(testOppty.Id);
        AP34_AjaxRequestHandler.unlockOpportunity(testOppty.Id);
        // passing child opportunityfor lock product expansion and unlock. Test class fix
        Test.StartTest();
        AP34_AjaxRequestHandler.lockExpansionOpportunity(testOppty1.Id);        
        AP34_AjaxRequestHandler.unlockExpansionOpportunity(testOppty1.Id);
        Test.StopTest();
    }
}