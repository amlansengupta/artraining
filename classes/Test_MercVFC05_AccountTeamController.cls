/*Purpose: Test Class for providing code coverage to MercVFC05_AccountTeamController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Suchi   02/20/2014  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
Trisha          21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify' 
=======================================================
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_MercVFC05_AccountTeamController 
{
    /* * @Description : This test method provides data coverage to Mercer Add Account Team functionality (Scenerio 1)  
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890'+i;
        insert testUser;
        System.runAs(testUser){
        }
        return testUser;
    }
   /* 
    * @Description : This test method provides data coverage to Mercer Add Account Team functionality (Scenerio 2) 
    * @ Args       : no arguments        
    * @ Return     : void       
    */
    static testMethod void myUnitTest() 
    {
        PageReference pageRef = Page.Mercer_AddDefaultTeam;
        Test.setCurrentPage(pageRef);
        
        User tUser = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
       
        
        Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Location__c = mogd.Id;
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;  
         tUser = createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser, 1);
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638-Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        test.starttest();
        insert testOppty;     
        
        system.runAs(tUser){
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC05_AccountTeamController extCont = new MercVFC05_AccountTeamController(stdController);
            extCont.opprec = testOppty;
            pageRef = extCont.methodToInsertTeam();
            extCont.postChatter();
     
        }
        test.stoptest();
        
    }
}