/*Handler Class for Account Object*/
public class AccountTriggerHandler implements ITriggerHandler
{
    // Allows unit tests (or other code) to disable this trigger for the transaction
     public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('Account Trigger');
        
        //if (TrgSetting.Trigger_Disabled__c== true) { //commented by AG 06-Sep-2018
        if (TrgSetting!=null && TrgSetting.Trigger_Disabled__c== true) {
            return true;
        }else{
            return TriggerDisabled;
        }
    }
    public void BeforeInsert(List<SObject> newItems)
    {
        /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
        boolean isExcludedProfile = false;
        List<Account> typeCastedNewItems = (List<Account>)newItems;
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - Start*/
        //String runningUserProfile = [Select Name from Profile where Id =: userInfo.getProfileId()].get(0).Name;
        String runningUserProfile = [Select Username from User where Id =: userInfo.getUserId()].get(0).Username;
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - End*/
        List<Audit_Excluded_Profiles__mdt> listAuditExcludedProfiles = [Select ExcludedProfiles__c from Audit_Excluded_Profiles__mdt];
        if(listAuditExcludedProfiles != null){
            for(Audit_Excluded_Profiles__mdt profMetadata : listAuditExcludedProfiles){
                if(profMetadata.ExcludedProfiles__c != null && profMetadata.ExcludedProfiles__c.equalsIgnoreCase(runningUserProfile)){
                    isExcludedProfile = true;
                    break;
                }
            }
        }
        if(!isExcludedProfile){
            for(Account ac:typeCastedNewItems){
                ac.User_Last_Modified_Date__c = System.now();
                ac.User_Last_Modified_By__c = UserInfo.getUserId(); 
            }
        }
        AP01_AccountTriggerUtil.processAccountBeforeInsert(newItems);
        AP01_AccountTriggerUtil.updateManagedOfficeForUS(newItems);
        AP01_AccountTriggerUtil.updateSICInfo(newItems);
    }
    
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemsMap)
    {
        AP01_AccountTriggerUtil.processAccountAfterInsert(newItems);
        AP01_AccountTriggerUtil.createTask(newItems);
    }
    
    
    public void BeforeUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap)
    {
        
        /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */ 
        Map<Id,Account> typeCastedNewItems = (Map<Id,Account>)newItemsMap;
        Map<Id,Account> typeCastedOldItems = (Map<Id,Account>)oldItemsMap;
        List<Account> ListtypeCastedNewItems1=typeCastedNewItems.values();
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - Start*/
        //String runningUserProfile = [Select Name from Profile where Id =: userInfo.getProfileId()].get(0).Name;
        String runningUserProfile = [Select Username from User where Id =: userInfo.getUserId()].get(0).Username;
        /*Request# 18848: 05/11/2019: Fixed the population of fields UserLastModifiedBy and UserLastModifiedDate. - End*/
        system.debug('runningUserProfile : '+runningUserProfile);
        List<Audit_Excluded_Profiles__mdt> listAuditExcludedProfiles = [Select ExcludedProfiles__c from Audit_Excluded_Profiles__mdt];
        boolean isExcludedProfile = false;
        if(listAuditExcludedProfiles != null){
            for(Audit_Excluded_Profiles__mdt profMetadata : listAuditExcludedProfiles){
                if(profMetadata.ExcludedProfiles__c != null && profMetadata.ExcludedProfiles__c.equalsIgnoreCase(runningUserProfile)){
                    isExcludedProfile = true;
                    break;
                }
            }
        }
        if(!isExcludedProfile){
            for(Account ac:ListtypeCastedNewItems1){
                ac.User_Last_Modified_Date__c = System.now();
                ac.User_Last_Modified_By__c = UserInfo.getUserId(); 
            }
        }
        
            AP01_AccountTriggerUtil.processAccountBeforeUpdate(typeCastedNewItems, typeCastedOldItems);
            AP01_AccountTriggerUtil.updatePreviousImmediateUltimateParent(typeCastedNewItems, typeCastedOldItems);    
         //AP01_AccountTriggerUtil.updateManagedOfficeForUS(newItemsMap.values());
            AP01_AccountTriggerUtil.updateManagedOfficeForUS(typeCastedNewItems.values());
            //AP01_AccountTriggerUtil.updateSICInfo(newItemsMap.values());
            AP01_AccountTriggerUtil.updateSICInfo(typeCastedNewItems.values());           
            List<Account> accnewList=new List<Account>();
            for(Account ac:typeCastedNewItems.values())
            {
                if(ac.client_Retention__c!=0 && ac.client_Retention__c!=null && typeCastedOldItems.get(ac.Id).client_retention__c!=ac.client_retention__c)
                {
                    accnewList.add(ac);
                }
            }
            
            AP01_AccountTriggerUtil.createTask(accnewList);
            //}
       
    }
        
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap)
    {
       
            
            Map<Id,Account> typeCastedNewItems = (Map<Id,Account>)newItemsMap;
            Map<Id,Account> typeCastedOldItems = (Map<Id,Account>)oldItemsMap;
            AP01_AccountTriggerUtil.processAccountAfterUpdate(typeCastedNewItems, typeCastedOldItems);
            AP01_AccountTriggerUtil.updateContactGDPR(typeCastedNewItems, typeCastedOldItems);
            List<Account> OppAccList = new List<Account>();
            for(Account OpptyAcc : typeCastedNewItems.values() )
            {
                if((OpptyAcc.Account_Region__c != typeCastedOldItems.get(OpptyAcc.Id).Account_Region__c ) ||
                    (OpptyAcc.Name != typeCastedOldItems.get(OpptyAcc.Id).Name) )
                {
                    OppAccList.add(OpptyAcc); 
                }
                
            } 
            
            if(!OppAccList.IsEmpty())
            {
                AP007_OppValidations.updateFieldsAfterAccountUpdate(OppAccList);
            }
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItemsMap)
    {}
    
    public void AfterDelete(Map<Id, SObject> oldItemsMap)
    {}
    
    public void AfterUndelete(Map<Id, SObject> oldItemsMap)
    {}
}