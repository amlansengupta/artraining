public Class MReportInput{

    // Variables
    public String newOneCode {get; set;}
    public String newGUDUNS {get; set;}
    public String newGUNAME {get; set;}
    public transient string newplanningyear{
        get;
        set;
    }
    //public integer newplanningyearInt;
    // Constructor Method for Class
    public MReportInput(){
        newOneCode = ApexPages.currentPage().getParameters().get('onecode');
        newGUDUNS = ApexPages.currentPage().getParameters().get('guduns');
        newGUNAME = ApexPages.currentPage().getParameters().get('guname');
        //newplanningyearInt=Integer.valueof(newplanningyear.trim());
    }
    
    //Method added to Redirect to Report page
    public PageReference redirect()
    {
        //if ((newOneCode==null||newOneCode=='')&& (newGUDUNS==null||newGUDUNS=='')&&( newplanningyear=='--None--')){
        if (( newplanningyear=='--None--')){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please select planning year.');
            ApexPages.addMessage(myMsg);

        }
        else if ((newGUDUNS =='' || newGUDUNS == null)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Selected Account GU DUNS is blank and hence the report cannot be generated.');
            ApexPages.addMessage(myMsg);

        }
        else{
            // Redirect to Report page with one code and duns no to generate report
            PageReference page = new PageReference('/apex/MReportGenerator');
            page.getParameters().put('onecode',newOneCode); 
            page.getParameters().put('guduns',newGUDUNS); 
            page.getParameters().put('guname',newGUNAME);
            page.getParameters().put('planningyear',newplanningyear); 
            page.setRedirect(true);
            return page;
        }
        return null;
    }
}