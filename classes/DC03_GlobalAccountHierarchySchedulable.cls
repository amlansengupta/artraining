/*Purpose: This Apex class implements Schedulable interface to schedule AP60_AccountUtilBatchable 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE          DETAIL 
   1.0 -    Srishty     05/09/2013    Created Apex Schedulable class
   2.0 -    Sarbpreet   7/7/2014      Added Comments
============================================================================================================================================== 
*/
global class    DC03_GlobalAccountHierarchySchedulable implements Schedulable{
    Set<String> dunsSet = new Set<String>();
    /*
     * Schedulable execute method     
     */
    global void execute(SchedulableContext sc)
    {
        
        DC04_GlobalAccountHierarchyBatchable accHierarchy = new DC04_GlobalAccountHierarchyBatchable ();
        Database.executeBatch(accHierarchy, 1000);
    }
}