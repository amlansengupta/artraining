global class Opportunity_Summary_Insert_Batch implements Database.Batchable<sObject>{
    
    global String query = Label.Opportunity_Summary_Insert_Batch_Query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(Test.isRunningTest()){
            query = 'Select Id, AccountId, Opportunity_ID__c From Opportunity Where CALENDAR_YEAR(CloseDate) > = 2015 LIMIT 200';
        }
        else{        
            query = Label.Opportunity_Summary_Insert_Batch_Query;
        }    
        return Database.getQueryLocator(query);
    }
            
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){    
        Set<Id> oppIdSet = new Set<Id>();        
        Map<String,Opportunity_Lite__c> summaryMap = new Map<String,Opportunity_Lite__c>();
        List<Opportunity_Lite__c> oppSummaryListToInsert = new List<Opportunity_Lite__c>();
        List<Opportunity_Lite__c> oppSummaryListToUpdate = new List<Opportunity_Lite__c>();
        Opportunity_Lite__c lite;
        
        for(Opportunity opp : scope){
            oppIdSet.add(opp.Id);
            system.debug('$$$1opp'+opp.Id);
        }
               
        for(Opportunity_Lite__c summary : [Select Id,Opportunity__c,Account__c From Opportunity_Lite__c 
                                           Where Opportunity__c IN :oppIdSet ])
        {
           // String idCombination = String.valueOf(summary.Opportunity__c) + String.valueOf(summary.Account__c);
           // summaryMap.put(idCombination ,summary );
           summaryMap.put(summary.Opportunity__c , summary );
        }
        system.debug('$$$3 summaryMap'+summaryMap);
        
        for(Opportunity opp : scope){
          //  String idCombinationOpp = String.valueOf(opp.Id) + String.valueOf(opp.AccountId);
           // if(!summaryMap.containsKey(idCombinationOpp )){
            if(!summaryMap.containsKey(opp.Id)){           
                system.debug('$$$opp.Opportunity_ID__c'+opp.Opportunity_ID__c);
                lite = new Opportunity_Lite__c();
                lite.Account__c = opp.Accountid;
                lite.Name = opp.Opportunity_ID__c;
                lite.Opportunity__c = opp.Id;                
                oppSummaryListToInsert.add(lite);
            }
            else{
                lite = new Opportunity_Lite__c();
                lite = summaryMap.get(opp.Id);
                if(lite.Account__c != opp.AccountId){
                    lite.Account__c = opp.AccountId;
                    oppSummaryListToUpdate.add(lite);
                }
            }    
        }
        
        if(!oppSummaryListToInsert.isEmpty()){
            Database.insert(oppSummaryListToInsert,false);            
        }
        if(!oppSummaryListToUpdate.isEmpty()){
            Database.update(oppSummaryListToUpdate,false);
        }
    }    
    
    global void finish(Database.BatchableContext BC){
        
    }

}