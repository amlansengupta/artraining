/*
Purpose: This test class provides data coverage to AP51_FeedCommentReporting class
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    Shashank 03/29/2013  Created test class
=============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP51_FeedCommentReporting 
{

     /*
      * @Description : Test method to provide data coverage to AP51_FeedCommentReporting  class
      * @ Args       : Null
      * @ Return     : void
      */
        static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '00000000000';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        User usr = [Select Id from user where id=: UserInfo.getUserId()];
        User user1 = new User();
        User testUser = new User();
        system.runAs(usr){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
        testUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', testUser);
        }
        system.runAs(User1){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = '123456';    
        testAccount.Relationship_Manager__c = testColleague.Id;
        
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc_test@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.Opportunity_Office__c = 'Chicago - North Wacker';
        AP44_ChatterFeedReporting.FROMFEED=true;
        insert testOppty;
        
               
            
        CollaborationGroup testGroup = new CollaborationGroup();
        testGroup.Name = 'Test Group AP51';
        testGroup.CollaborationType = 'Public';
        insert testGroup;
        
        
        List<FeedItem> testFeedList = new List<FeedItem>();
        FeedItem fItem;
        for(integer i=0; i<5; i++)
        {
            fItem = new FeedItem();
            fItem.body = 'Hello';
            fItem.Type = 'TextPost';
            testFeedList.add(fItem);
        }
        testFeedList[0].ParentId = testAccount.Id;
        testFeedList[1].ParentId = testContact.Id;
        testFeedList[2].ParentId = testAccount.Id;
        testFeedList[3].ParentId = testGroup.Id;
        testFeedList[4].ParentId = testUser.Id;
        test.startTest();
        insert testFeedList;
        
        
        List<FeedComment> testFeedList1 = new List<FeedComment>();
        FeedComment fComment;
        for(integer i=0; i<5; i++)
        {
            fComment = new FeedComment();
            fComment.CommentBody = 'Hello';
            fComment.FeedItemId = testFeedList[i].Id;
            testFeedList1.add(fComment);
        }
        insert testFeedList1;
        delete testFeedList1;
        test.stopTest();
        } 
    }
    
}