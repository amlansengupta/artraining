global class ObjectRecordCount_Batch implements Database.Batchable<sObject>, Database.Stateful {
        
        global static String query = 'Select Id,Name,Record_Count__c From Object_Record_Count__c';

        global Database.QueryLocator start(Database.BatchableContext BC){
            if(Test.isRunningTest())
            {
              query = 'Select Id,Name,Record_Count__c From Object_Record_Count__c Limit 100'; 
            }                     
            return Database.getQueryLocator(query); 
        }
        
        global void execute(Database.BatchableContext bc, List<Object_Record_Count__c> scope){
            Map<String,Object_Record_Count__c> recordCountMap = new Map<String,Object_Record_Count__c>();
            List<Object_Record_Count__c> countListUpdate = new List<Object_Record_Count__c>();            
            
            Integer opportunityCount, opportunityLineItemCount;
            
            opportunityCount = [Select Count() From Opportunity];
            opportunityLineItemCount = [Select Count() From OpportunityLineItem];
               
            for(Object_Record_Count__c orc : scope)   {
                recordCountMap.put(orc.Name,orc);                
            }
            
            if(recordCountMap.containsKey('Opportunity')){
                Object_Record_Count__c count = recordCountMap.get('Opportunity');
                count.Record_Count__c = opportunityCount;
                countListUpdate.add(count);
            }
            
            if(recordCountMap.containsKey('OpportunityLineItem')){
                Object_Record_Count__c count = recordCountMap.get('OpportunityLineItem');
                count.Record_Count__c = opportunityLineItemCount;
                countListUpdate.add(count);
            }                     
            
            Database.update(countListUpdate,false);          
        }
        
        global void finish(Database.BatchableContext bc){
            List<Object_Record_Count_Batch_Starter__c> orcBtachStarterList = new List<Object_Record_Count_Batch_Starter__c>();
            orcBtachStarterList = [Select Id, Name, Batch_Starter_Name__c, HasBatchStarted__c 
                                   From Object_Record_Count_Batch_Starter__c Where Batch_Starter_Name__c = 'ORCBatchStarter' ];
                                   
            if(!orcBtachStarterList.isEmpty()){
                if(orcBtachStarterList[0].HasBatchStarted__c){
                    orcBtachStarterList[0].HasBatchStarted__c = false;
                }    
            }
            
            update orcBtachStarterList;                                                         
        }
}