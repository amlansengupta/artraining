/*
Purpose: This Apex class implements batchable interface
========================================================================================================================================History
 History
 ----------------------- 
 VERSION     AUTHOR   DATE        DETAIL    
 1.0 -       Shashank 03/29/2013  Created Tets class
=============================================================================================================================================== */
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest
private class Test_AP26_LockOpportunityBatchable 
{
   
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul - Gangnamdae-ro;Taipei - Minquan East;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
         
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
        Request No. 17953 : Removing Finalist END */
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:25000;Growth Markets:2500;North America:25000';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Opportunity_Country';
        setting.StrValue__c = 'Korea;';
        insert setting;
    }
    /*
     * @Description : Test method for making an  Opportunity Locked after certain duration
     * @ Args       : null
     * @ Return     : void
     */
    public static testMethod void myUnitTest() 
    {
        createCS();
        Test.startTest();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '00000000000';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '123';
        insert testAccount;
        
       /* Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty2';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Pending Step';
        testOppty.CloseDate = date.Today();
        //Updated as part of 5166(july 2015)
        List<String> oppOffices_List = new List<String>();               
        ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
        if(officesToBeExcluded.StrValue__c != null){        
        oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
        testOppty.Opportunity_Office__c = oppOffices_List[0];
        }
        testOppty.CurrencyIsoCode = 'ALL';
        
        insert testOppty;
        */
            
            
        Opportunity TestOppLock = New Opportunity();
        TestOppLock.Name = 'TestOppLock';
        TestOppLock.Type = 'New Client';
        TestOppLock.AccountId = testAccount.Id;
        //request id:12638 commenting step
        //TestOppLock.Step__c = 'Closed / Lost';
        TestOppLock.StageName = 'Closed / Lost';
        TestOppLock.CloseDate = date.today();
        //Updated as part of 5166(july 2015)
        TestOppLock.Opportunity_Office__c = 'Seoul - Gangnamdae-ro';
        TestOppLock.CurrencyIsoCode = 'ALL';
        insert TestOppLock;
        Opportunity TestOppLockUpdate = [Select ID, Stagename From Opportunity where Name = 'TestOppLock'];
        TestOppLockUpdate.Stagename = 'Closed / Lost';
        Update TestOppLockUpdate;
        System.debug('Soumil3>>>' + Database.query('Select ID,CloseDate,StageName,Opportunity_Office__c From Opportunity where ID = \''+TestOppLock.ID+'\''));    
        
        Opportunity TestOppLock2 = new Opportunity();
        TestOppLock2.Name = 'TestOppLock2';
        TestOppLock2.Type = 'New Client';
        TestOppLock2.AccountId = testAccount.Id;
        //request id 12638 commenting step
        //TestOppLock2.Step__c = 'Closed / Lost';
        TestOppLock2.StageName = 'Closed / Lost';
        TestOppLock2.CloseDate = date.today();
        //Updated as part of 5166(july 2015)
        TestOppLock2.Opportunity_Office__c = 'Seoul - Gangnamdae-ro';
        TestOppLock2.CurrencyIsoCode = 'ALL';
        TestOppLock2.Parent_Opportunity_Name__c = TestOppLock.ID;
        insert TestOppLock2;
        Opportunity TestOppLock2Update = [Select ID, Stagename From Opportunity where Name = 'TestOppLock2'];
        TestOppLock2Update.Stagename = 'Closed / Lost';
        Update TestOppLock2Update;
        System.debug('Soumil2>>>' + Database.query('Select ID,CloseDate,StageName,Opportunity_Office__c From Opportunity where ID = \''+TestOppLock2.ID+'\''));                     
        
        
              
        /*Opportunity testOppty3 = [select Id, Name, Step__c, Opportunity_Office__c, Amount from Opportunity where Name = 'TestOppty2'  limit 1];
        testOppty3.Step__c = 'Closed / Declined to Bid ';
        testOppty3.Close_Stage_Reason__c = 'Other';
        update testOppty3;
        
        Opportunity testOppty4 = [select Id, Name, Step__c, Opportunity_Office__c, Amount from Opportunity where Name = 'TestOppty2'  limit 1];
        //testOppty4.CloseDate = date.Today() - 2;
        testOppty4.CloseDate = date.Today().toStartofMonth()+3;
        testOppty4.Close_Stage_Reason__c = 'Other';
        //testOppty4.Parent_Opportunity_Name__c = testOppty3.id; 
        update testOppty4;
        */
        
        /*string baseQuery = 'Select Id, RecordTypeId,Parent_Opportunity_Name__c from Opportunity';
       //string whereClause = 'where CloseDate = LAST_MONTH AND StageName IN (\'Closed / Won\', \'Closed / Lost\' , \'Closed / Declined to Bid\' , \'Closed / Client Cancelled\')';
        string whereClause = ' where Opportunity_Office__c = \'Seoul - Gangnamdae-ro\' AND CloseDate = THIS_MONTH AND StageName IN (\'Closed / Won\', \'Closed / Lost\' , \'Closed / Declined to Bid\' , \'Closed / Client Cancelled\')';
        */
        //12638 : merging Closed / Declined to Bid and Closed / Client Cancelled to Closed / Cancelled
        //string query = 'Select Id, RecordTypeId,Parent_Opportunity_Name__c from Opportunity where Opportunity_Office__c = \'Seoul - Gangnamdae-ro\' AND CloseDate = THIS_MONTH AND StageName IN (\'Closed / Won\', \'Closed / Lost\' , \'Closed / Declined to Bid\' , \'Closed / Client Cancelled\')';
        string query = 'Select Id, RecordTypeId,Parent_Opportunity_Name__c from Opportunity where Opportunity_Office__c = \'Seoul - Gangnamdae-ro\' AND CloseDate = THIS_MONTH AND StageName IN (\'Closed / Won\', \'Closed / Lost\' , \'Closed / Cancelled\')';
        System.debug('Soumil>>>'+database.query(query));
        database.executeBatch(new AP26_LockOpportunityBatchable(query), 100);
        }
        Test.stopTest();
        /*Opportunity OppWithoutParent = [Select Id, RecordType.Name , Parent_Opportunity_Name__c from Opportunity where Opportunity_Office__c = 'Seoul - Gangnamdae-ro' AND CloseDate = THIS_MONTH AND StageName IN ('Closed / Won', 'Closed / Lost' , 'Closed / Declined to Bid' , 'Closed / Client Cancelled') AND Parent_Opportunity_Name__c = null Limit 1];
        Opportunity OppWithParent = [Select Id, RecordType.Name , Parent_Opportunity_Name__c from Opportunity where Opportunity_Office__c = 'Seoul - Gangnamdae-ro' AND CloseDate = THIS_MONTH AND StageName IN ('Closed / Won', 'Closed / Lost' , 'Closed / Declined to Bid' , 'Closed / Client Cancelled') AND Parent_Opportunity_Name__c != null Limit 1];
        System.Assertequals(OppWithoutParent.Recordtype.name, 'Opportunity Locked');
        System.Assertequals(OppWithParent.Recordtype.name, 'Opportunity Product Expansion Locked');
        */
    }
}