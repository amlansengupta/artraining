/*Purpose:  This Apex class contains methods to process Lead conversion 
=================================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL   
 1.0 -    Shashank      14/08/2013  Created Apex class to handle Custom Lead Conversion Functioanlity
 2.0 -    Venkatesh     14/06/2015  Changed as a part of Req #5166
 3.0 -    Sarbpreet     14/08/2015  As part of Request# 6733(September 2015 Release) updated logic to add validation for making conatct work email mandatory
 4.0 -    Trisha        18/01/2019  Request#12638 Replacing step with stage
================================================================================================================================================= 
*/
public class AP36_CustomLeadConversionController {
    
    public Account account{get; set;}
    public Opportunity opportunity{get; set;}
    public Contact contact{get; set;}
    public boolean check{get; set;}
    public boolean checkOpp{get; set;}
    public Lead lead{get; set;}
    List<sObject> duplicateRecords {get; set;}
    public boolean hasDuplicateResult{get;set;}
    public String accName{get; set;}
    public String oppName{get; set;}
    public string stageVal{get;set;}
    public String profilename;
    public ID AccownerID;
    public static final string STR_SystemAdmin = 'System Administrator';
    public static final string STR_Qualified = 'Qualified';
    public static final string STR_AboveTheFunnel = 'Above the Funnel';
    public static final string STR_Identify = 'Identify';
    public static final string STR_Selected = 'Selected';
    public static final string STR_JAPAN = 'japan';
    public static final string STR_NON_EXECUTIVE_DIRECTOR = 'Non-Executive Director';

    
    // constructor for the class.
    public AP36_CustomLeadConversionController(ApexPages.StandardController controller) { 
        this.lead = (Lead)controller.getRecord();
        lead.Status = STR_Qualified ;                       
        init();
        
    }
    
    // Return duplicate records to the Visualforce page for display
    public List<sObject> getDuplicateRecords() {
        return this.duplicateRecords;
    }
    /*Initialize intial stage values*/
      public List<SelectOption> getStageValue() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption(System.Label.CL57_OpportunityType,System.Label.None_With_Dash));
        stOptions.add(new SelectOption(System.Label.Stage_Above_the_Funnel,System.Label.Stage_Above_the_Funnel));           
         stOptions.add(new SelectOption(System.Label.Stage_Identify,System.Label.Stage_Identify));            
         //sOptions.add(new SelectOption(System.Label.Stage_Qualify,System.Label.Stage_Qualify));           
        
        return stOptions;
        
    }
    
    /*
     * Initialize the variables.
     */
     
    private void init()
    {
        this.account = new Account();
        
        this.opportunity = new Opportunity();
        this.accName = lead.Company;
        this.contact = new Contact();
        this.contact.FirstName = lead.FirstName;
        this.contact.LastName = lead.lastName;
        this.account.Name = lead.Company;
        //this.account.Created_From_Custom_Page__c=true;
        this.contact.Email= lead.Email;
       
        this.contact.Phone = lead.Phone;
        //System.debug('Address: ' + this.lead.Street + this.lead.City + this.lead.Country + this.lead.State);
        this.contact.OtherStreet = lead.Street;
        this.contact.OtherCity = lead.City;
        this.contact.OtherCountry = lead.Country;
        this.contact.OtherState = lead.State;
        
        this.account.OwnerId = UserInfo.getUserId();
        
        AccownerID = this.account.OwnerId;
        profilename = [Select id, profileid, profile.name from user where ID =:AccownerID].profile.name;
        check = false;
        checkOpp = false;
    }
   
     /*
     * @Description : This method is called to Convert Lead 
     * @ Args       : None
     * @ Return     : Pagereference
     */
    public pageReference convertLead()
    {
        if(validateRequired()){
            return null;
        }
        Database.LeadConvert leadConvert;
        PageReference pref;
        Database.LeadConvertResult leadConvertResult;
        Database.saveResult SR_update;
        SavePoint sp;
            sp = Database.setSavepoint();
            
            if(check)
            {
                if(validate())
                {
                    return null;
                }
                //ConstantsUtility.STR_Active ='Inactive';
                contact.isConvertedFromLead__c= true;
                account.Id = null;
                account.Name = accName;
               try{
                insert account;
                }
                Catch(Exception ex){
                ExceptionLogger.logException(ex, 'AP36_CustomLeadConversionController', 'convertLead');
                System.debug('%%%%%%'+ex.getMessage());
                   /* ApexPages.addMessage(
                           new ApexPages.Message(
                               ApexPages.Severity.ERROR, 
                              ex.getMessage()
                           )
                       );*/
                            return null;
                }
            }
            else
            {
                if(validateExistingAccount())
                {
                    return null;
                }
            }
            //lead.Street = contact.MailingStreet;
            //lead.City = contact.MailingCity;
            //lead.Country = contact.MailingCountry;
            //lead.State = contact.MailingState;
            lead.IsConvertedFromLead__c = true;
            Database.DMLOptions dml = new Database.DMLOptions(); 
            dml.DuplicateRuleHeader.allowSave = false;
            
            SR_update = database.update(lead, dml);            
            if(!SR_update.isSuccess())
            {
                for (Database.Error error : SR_update.getErrors()){
                   System.debug('Inside error: '+error);
                   
            
                   if (error instanceof Database.DuplicateError) {                                    
                       System.debug('Inside duplicate error');
                       Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                       Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
    
                       this.duplicateRecords = new List<sObject>();
                    
                       // Return only match results of matching rules that find duplicate records
                       Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();
    
                       // First match result (which contains the duplicate record found and other match info)
                       Datacloud.MatchResult matchResult = matchResults[0];
    
                       Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();
    
                       // Add matched record to the duplicate records variable
                       for (Datacloud.MatchRecord matchRecord : matchRecords) {
                           System.debug('MatchRecord: ' + matchRecord.getRecord());
                           this.duplicateRecords.add(matchRecord.getRecord());
                       }
                       ApexPages.addMessage(
                           new ApexPages.Message(
                               ApexPages.Severity.ERROR, 
                               System.Label.Manual_contact_creation_duplicate
                           )
                       );
                       Database.rollback(sp);                       
                       this.hasDuplicateResult = !this.duplicateRecords.isEmpty();
                    }else{                   
                        ApexPages.addMessage(
                           new ApexPages.Message(
                               ApexPages.Severity.ERROR, 
                               error.getMessage()
                           )
                       ); 
                       Database.rollback(sp);     
                    } 
                }
                return null;
            } 
            system.debug('***'+lead.Alternate_Email__c);
            /*****request id:12638 below code is unused as stage will not depend on step START*****/
             /*List<String> aboveTheFunnelSteps_List = new List<String>();
             List<String> identifySteps_List = new List<String>();
             List<String> selectedSteps_List = new List<String>();
             Map<String, String> aboveTheFunnelSteps_Map = new Map<String, String>();
             Map<String, String> identifySteps_Map = new Map<String, String>(); 
             Map<String, String> selectedSteps_Map = new Map<String, String>();    
            ApexConstants__c Stage_Steps = ApexConstants__c.getValues('Above the funnel');
            if(Stage_Steps.StrValue__c != null)        
            aboveTheFunnelSteps_List = Stage_Steps.StrValue__c.split(';');
            for(String s : aboveTheFunnelSteps_List){
                aboveTheFunnelSteps_Map.put(s,s);    
            }
            Stage_Steps = ApexConstants__c.getValues('Identify');      
            if(Stage_Steps.StrValue__c != null)        
            identifySteps_List = Stage_Steps.StrValue__c.split(';');
            for(String s : identifySteps_List){
                identifySteps_Map.put(s,s);    
            }
            Stage_Steps = ApexConstants__c.getValues('Selected');      
            if(Stage_Steps.StrValue__c != null)        
            selectedSteps_List = Stage_Steps.StrValue__c.split(';');
            for(String s : selectedSteps_List){
                selectedSteps_Map.put(s,s);    
            }  */    
            /*****request id:12638 below code is unused as stage will not depend on step END*****/
            leadConvert = new database.LeadConvert();
            leadConvert.setLeadId(lead.Id);
            leadConvert.setOwnerId(account.OwnerId);            
            if(check)leadConvert.setAccountId(account.Id);
            else leadConvert.setAccountId(contact.AccountId);
            leadConvert.setConvertedStatus('Qualified');
            leadConvert.setDoNotCreateOpportunity(true);
            
            If(profilename != STR_SystemAdmin && ((lead.Job_Function__c == null || lead.Job_Level__c == null ) && lead.title == null))
            {
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.Label.CL99_Contact_Title_JobLevel_JobFunction));
                return null;
            }
            else {   
                if(!check)disableValidationRule(contact.AccountId);  
                try{   
                    leadConvertResult = Database.convertLead(leadConvert);  
                }catch(Exception e){
                    //feedback-3324:start: Account validation made disable, if custom lead conversion gets error, previously it was
                    //returning error message to page without enabling the validation.
                    if(!check)enableValidationRule(contact.AccountId);
                    //feedback-3324:end
                    List<String> errorStack = e.getMessage().split(':');
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errorStack[2]));
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
                    return null;
                }
                if(!check)enableValidationRule(contact.AccountId);         
            }
            
            if(leadConvertResult.isSuccess())
            {     
                if(check) {
                    disableValidationRule(account.Id); 
                    updateAccountAddress(account.Id);   
                    enableValidationRule(account.Id); 
                }
                //addToContactTeam(leadConvertResult.getContactId(), account.OwnerId);
                updateContactAddress(leadConvertResult.getContactId(), lead.Street, lead.City, lead.Country, lead.State);
                updateContactOwner(account.OwnerId, leadConvertResult.getContactId());
                if(checkOpp){          
                    if(check){
                        opportunity.AccountId = account.Id;
                    }else{
                        opportunity.AccountId = contact.AccountId;
                    }
                    opportunity.Name = oppName;
                    opportunity.stageName = stageVal;
                    //opportunity.Step__c = 'Identified Marketing/Sales Lead';
                    //opportunity.StageName = 'Above the Funnel';
                    /****request id:12638,removing below block, as stageName will no longer depend on step.START*****/
                    /*if(aboveTheFunnelSteps_Map.get(opportunity.Step__c)!=null)opportunity.StageName = STR_AboveTheFunnel;
                    if(identifySteps_Map.get(opportunity.Step__c)!=null)opportunity.StageName = STR_Identify;
                    if(selectedSteps_Map.get(opportunity.Step__c)!=null)opportunity.StageName = STR_Selected; */ 
                    /****request id:12638,removing below block, as stageName will no longer depend on step.END*****/
                           
                    database.SaveResult sr = database.insert(opportunity);
                    
                    
                    if(sr.isSuccess()){                  
                        Buying_Influence__c BI = new Buying_Influence__c();
                        BI.Contact__c = leadConvertResult.getContactId();
                        BI.Opportunity__c = sr.getId();
                        try{
                        database.insert(BI);
                        }
                        Catch(Exception e){
                        ExceptionLogger.logException(e, 'AP36_CustomLeadConversionController', 'convertLead');
                            ApexPages.addMessage(
                           new ApexPages.Message(
                               ApexPages.Severity.ERROR, 
                               'You do not have access on the recent opportunity.Select Opportunity Owner same or below your role.'
                           )
                       );
                            return null;
                        }
                    }
                    else{
                        //ExceptionLogger.logException(sr.getErrors()[0], 'AP36_CustomLeadConversionController', 'convertLead');
                            ApexPages.addMessage(
                           new ApexPages.Message(
                               ApexPages.Severity.ERROR, 
                               sr.getErrors()[0].getMessage()
                           )
                       );
                            return null;
                        }
                    
                }
            }else{           
                ApexPages.addMessage(
                   new ApexPages.Message(
                       ApexPages.Severity.ERROR, 
                       leadConvertResult.getErrors()[0].getMessage()
                   )
               );
               Database.rollback(sp);      
               return null;        
            }
     
            pref = new pageReference('/'+leadConvertResult.getAccountId()); 
        
        return pref;        
    }

    /*
     * @Description : This method is called to validate Existing Account
     * @ Args       : None
     * @ Return     : boolean
     */
    public boolean validateExistingAccount()
    {
        boolean error = false;
        if(contact.AccountId == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL67_LeadAccountError));
            error = true;
        }
        return error;
    }
    
    /*
    This method is to update the account address post lead conversion
    */
    public void updateAccountAddress(Id accId){
        /*Existing SOQL 101 Error elimination*/ 
        ConstantsUtility.STR_Active ='Inactive';
        Account acc = new Account(Id = accId);
        acc.BillingStreet = account.BillingStreet;
        acc.BillingCity = account.BillingCity;
        acc.BillingState = account.BillingState;
        acc.BillingPostalCode = account.BillingPostalCode;
        acc.Country__c = account.Country__c;
        update acc;
    }
    
    /*
    This method is to disable account validation rules before updating account post lead conversion
    */
    public void enableValidationRule(Id accId)
    {  
    /*Existing SOQL 101 Error elimination*/ 
     ConstantsUtility.STR_Active ='Inactive';
        Account acc = new Account(Id = accId);
        acc.Validations_For_Account__c = true;
        update acc;        
    }
    
    /*
    This method is to enable account validation rules before updating account post lead conversion
    */
    public void disableValidationRule(Id accId)
    {
    /*Existing SOQL 101 Error elimination*/ 
      ConstantsUtility.STR_Active = 'Inactive';
        Account acc = new Account(Id = accId);
        acc.Validations_For_Account__c = false;
        update acc;        
    }
    
    /*
    This method is to validate all required fields for lead conversion
    */
    public boolean validateRequired()
    {
        boolean error = false;
        try {
        if(checkOpp){
        opportunity.stageName = stageVal;
            if(oppName == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL101_Opportunity_Name));
                error = true;
            }
            /****Request id:12638, step will be removed, so replacing below block with stage; CL102_Opportunity_Step is changed accordingly START*****/
            /*if(opportunity.Step__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL102_Opportunity_Step));
                error = true;
            }*/
            if(opportunity.stageName == null||opportunity.stageName == 'None'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL102_Opportunity_Step));
                error = true;
            }
            /****Request id:12638, step will be removed, so replacing below block with stage; CL102_Opportunity_Step is changed accordingly END*****/
            if(opportunity.CloseDate == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL103_Opportunity_CloseDate));
                error = true;
            }
            if(opportunity.Type == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL104_Opportunity_Type));
                error = true;
            }
            if(opportunity.CurrencyIsoCode == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL105_Opportunity_CurrencyIsoCode));
                error = true;
            }
            if(opportunity.OwnerId == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL106_Opportunity_Owner));
                error = true;
            }
            if(opportunity.Opportunity_Office__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL100_Opportunity_Office));
                error = true;
            }
        }
        if( profilename != STR_SystemAdmin && (lead.Title == null && (lead.Job_Function__c == null || lead.Job_Level__c == null)) )
        {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL99_Contact_Title_JobLevel_JobFunction));
                error = true;
        }
        //Added As part of Request# 6733(september 2015 Release)made country field required on new contact
        if(lead.Country==null)
        lead.Country = '';
        if(lead.email == null  && (!lead.Country.containsIgnoreCase(STR_JAPAN) && !lead.Job_Function__c.containsIgnoreCase(STR_NON_EXECUTIVE_DIRECTOR))){
             
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL107_Contact_Email));
                error = true;
            }
        // End  of 6733
        }
         catch (Exception ex)
           {
                lead.addError(ConstantsUtility.STR_ERROR+ ex.getMessage());
           }        
        return error;
        
        
    }
    
    /*
     * @Description : This method is called to validate Account
     * @ Args       : None
     * @ Return     : boolean
     */
    public boolean validate()
    {
        boolean error = false;
        
        if(accName == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL68_LeadAccountNameError));
            error = true;
        }
        
        if(account.Relationship_Manager__c == null) 
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL69_LeadAccountManagerError));
            error = true;
        }
        
        if(account.Country__c == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL70_LeadAccountCountryError));
            error = true;
        }
        
         if(profilename != STR_SystemAdmin && (account.BillingCity == null || account.BillingStreet == null))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.CL98_Account_City_Street));
            error = true;
        }
        
        
        
        return error;
    }
    /*
    public static void addToContactTeam(String convertedContactId, String userId)
    {
        if(!isAddedToContactTeam(convertedContactId, userId))
        {
                User user = [Select First_Name__c, Last_Name__c, Id, UserRole.Name From User Where Id = : userId];
                Contact_Team_Member__c member = new Contact_Team_Member__c();
                member.First_Name__c = user.First_Name__c;
                member.Last_Name__c = user.Last_Name__c;
                member.User_ID__c = user.Id;
                member.Role__c = user.UserRole.Name;
                member.Contact__c = convertedContactId;
                member.ContactTeamMember__c = user.Id;
                insert member;
        }
    }*/
    
    /*
    public static boolean isAddedToContactTeam(String convertedContactId, String userId)
    {
        boolean isAdded = false;
        for(Contact_Team_Member__c conTeam : [Select ContactTeamMember__c FROM Contact_Team_Member__c Where Contact__c =: convertedContactId])
        {
                if(conTeam.ContactTeamMember__c == userId)
                {
                        isAdded = true;
                }
                else
                {
                        delete conTeam;
                }
        }
        return isAdded;
    }*/
    
    /*
     * @Description : This method is called to update Contact Owner
     * @ Args       : String
     * @ Return     : void
     */
    public static void updateContactOwner(String userId, String conId)
    {
        
        Contact contact = new Contact(Id = conId);
        contact.Ownerlookup__c = userId;
        update contact;
    }
    
    /*
     * @Description : This method is called to update Contact Address
     * @ Args       : String
     * @ Return     : void
     */
    public static void updateContactAddress(String conId, String Street, String City, String Country, String State)
    {
        Contact contactupd = new Contact(Id = conId);
        contactupd.OtherStreet = Street;
        contactupd.OtherCity = City;
        contactupd.OtherCountry = Country;
        contactupd.OtherState = State;
        update contactupd;
    }
    

}