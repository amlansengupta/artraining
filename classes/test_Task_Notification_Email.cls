/*Purpose: Test Class for providing code coverage to Task_Notification_Email class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
@isTest
public class test_Task_Notification_Email  {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    /*
     * @Description : Test method to provide data coverage to notify Users on Create or update of Task
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void test_task_email (){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        System.runas(user1){
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        test.startTest();
        contact c = new contact();
        c.accountid = testAccount.id;
        c.firstName = 'test first name';
        c.lastname = 'test last name';
        c.email = 'xyzq2@abc07.com';
        insert c;
        
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
              
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
        
        Task t = new Task();
        t.whatid = testAccount.id;
        t.notify_user__c = true;
        t.reminderdatetime = system.now();
        t.isreminderset = true;
        t.subject ='Request Consultation';
        t.ownerid = userinfo.getuserid();
        t.whoid = c.id;
        insert t;
        test.stopTest();
        }     
    }
    /*
     * @Description : Test method to provide data coverage to check if opportunity exists for the task inserted.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
{
    String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
    User u =[select id from user where id=:UserInfo.getUserId()];
        User testUser = new User();
    system.runAs(u){
        testUser.alias = 'user k1';
        testUser.email = 'aa@bbcm.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'chk lastname';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = MarketoSyncProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'userk1@bbcm.com';
        testUser.EmployeeNumber = '1238901';
        testUser.isActive = true;
        insert testUser;
    }
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
               
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
      
      
        System.runAs(testuser){
        Test.startTest();
 
        Task testtask = new Task();
        //testtask.Subject = 'Lead Score Threshold Met "HB_Exchanges"';
        testtask.Subject = 'Organic';
        testtask.Status = 'Not Started';   

        testtask.Priority = 'Normal';
        insert testtask;
        
        
        Campaign TestCamapign = new Campaign();
        TestCamapign.Name = 'HB_Exchanges';
        TestCamapign.Scope__c = 'Global';
        TestCamapign.Type = 'Lead Score';
        insert TestCamapign;
        
        
        
        Test.StopTest();
        }
       
             
}

    /*
     * @Description : Test method to provide data coverage to automatically assign Campaign to a task created by Marketo
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest3() 
{
    
          
        Colleague__c coll = mtdata.buildColleague();
    
        
        String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
        User testUser = new User();
        testUser.alias = 'user k1';
        testUser.email = 'aa@bbcm.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'chk lastname';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = MarketoSyncProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'userk1@bbcm.com';
        testUser.EmployeeNumber = '1238901';
        testUser.isActive = true;
        insert testUser;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
       Test.startTest(); 
        Account acc = mtdata.buildAccount();
    	
        Contact con = mtdata.buildContact();
        
        
        Campaign TestCamapign = new Campaign();
        TestCamapign.Name = 'HB_Exchanges';
        TestCamapign.Scope__c = 'Global';
        TestCamapign.Type = 'Lead Score';
        insert TestCamapign;
        
        System.runAs(testuser){
                
            Task testtask = new Task();
            //testtask.Subject = 'Lead Score Threshold Met "HB_Exchanges"';
            testtask.Subject = 'Telequalified';
            testtask.Status = 'Not Started';
            testtask.Priority = 'Normal';
            testtask.whoid = con.id;
            insert testtask;
        
           }
           
   
           
           
}
/*
     * @Description : Test method to provide data coverage to automatically assign Campaign to a task created by Marketo
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest1() 
{
    
          
        Colleague__c coll = mtdata.buildColleague();            
        String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
        test.startTest();
        Account acc = mtdata.buildAccount();
    	test.stopTest();
        Contact con = mtdata.buildContact();
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(MarketoSyncProfile, 'usert2', 'usrLstName2', 2);

     
        System.runAs(user1){
                
            Task testtask = new Task();
           // testtask.Subject = 'Marketing Qualified Lead - Core Brokrage - 1234';
            testtask.Subject = 'Third Party';
            testtask.Status = 'Completed - Opportunity Created';
            testtask.Priority = 'Normal';
            testtask.whoid = con.id;
            insert testtask;
        
           }
           
   
           
           
}
/*
     * @Description : Test method to check lastmodified date on opportunity.
     * @ Args       : Null
     * @ Return     : void
     */
     private static testmethod void testlastTaskmodifieddate(){
     Mercer_TestData mdata = new Mercer_TestData();
     //create Account
     
     Account acc =mdata.buildAccount();
     test.starttest();
     
     //create opportunity
     opportunity opp =mdata.buildOpportunity();
     User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
     test.stoptest();
     System.runAs(user1 ){
     //Create Task
      Task testtask = new Task();
      testtask.Subject = 'test';
      testtask.Status = 'Not Started';
      testtask.Priority = 'Normal';
      testtask.whatid = opp .id;
      insert testtask;

     // delete testtask;
     }
     }
     
static testMethod void myUnitTest4() 
{
    String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
        User testUser = new User();
     User u =[select id from user where id=:UserInfo.getUserId()];
    system.runAs(u){
        testUser.alias = 'user k1';
        testUser.email = 'aa@bbcm.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'chk lastname';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = MarketoSyncProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'userk1@bbcm.com';
        testUser.EmployeeNumber = '1238901';
        testUser.isActive = true;
        insert testUser;
    }
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
               
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
         
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
                 
        contact c = new contact();
        c.accountid = testAccount.id;
        c.firstName = 'test first name';
        c.lastname = 'test last name';
        c.email = 'xyzq2@abc07.com';
        insert c;
      
        System.runAs(testuser){
        Test.startTest();
 
        Task testtask = new Task();
        
        testtask.Subject = 'Marketing Qualified Lead - 001';
        testtask.Status = 'Closed - Create Opportunity';     
        testtask.Priority = 'Normal';

        testtask.WhoId = c.Id;
        insert testtask;      
    
        testTask.Priority = 'High';
        
        List<Task> taskList = new List<Task>();
        taskList.add(testTask);
        
        Task_Send_Notification_email.checkOwnerProfileBeforeDelete(taskList);
        
                        
        Campaign TestCamapign = new Campaign();
        TestCamapign.Name = 'HB_Exchanges';
        TestCamapign.Scope__c = 'Global';
        TestCamapign.Type = 'Lead Score';
        insert TestCamapign;
        

        Test.StopTest();
        }
       
             
    }    
    
static testMethod void myUnitTest5() 
{
    String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
        User testUser = new User();
     User u =[select id from user where id=:UserInfo.getUserId()];
    system.runAs(u){
        testUser.alias = 'user k1';
        testUser.email = 'aa@bbcm.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'chk lastname';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = MarketoSyncProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'userk1@bbcm.com';
        testUser.EmployeeNumber = '1238901';
        testUser.isActive = true;
        insert testUser;
    }
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
               
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
         
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
                 
        contact c = new contact();
        c.accountid = testAccount.id;
        c.firstName = 'test first name';
        c.lastname = 'test last name';
        c.email = 'xyzq2@abc07.com';
        insert c;
      
        System.runAs(testuser){
        Test.startTest();
 
        Task testtask = new Task();
        
        testtask.Subject = 'Marketing Qualified Lead - 001';
        testtask.Status = 'Assigned';     
        testtask.Priority = 'Normal';

        testtask.WhoId = c.Id;
        insert testtask;
            
            
       // testtask.Subject = 'Marketing Qualified Lead - 001';
       // testtask.Status = 'Assigned';     
       // testtask.Priority = 'Normal';

        //testtask.WhoId = c.Id;
       // insert testtask;  
    
        testTask.Status = 'Closed - Create Opportunity';
        Task_Send_Notification_email.FLAG_CHECK_TASK = false;
        update testtask;

        Test.StopTest();
        }
       
             
    }  

static testMethod void myUnitTest6() 
{
    Id recTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('RFC Task').getRecordTypeId();
    String MarketoSyncProfile = [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
    User u=[select id from user where id=:UserInfo.getUserId()];
    
        User testUser = new User();
    system.runAs(u){
        testUser.alias = 'user k1';
        testUser.email = 'aa@bbcm.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'chk lastname';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = MarketoSyncProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'userk1@bbcm.com';
        testUser.EmployeeNumber = '1238901';
        testUser.isActive = true;
        insert testUser;
    }
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
               
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
         
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
                 
        contact c = new contact();
        c.accountid = testAccount.id;
        c.firstName = 'test first name';
        c.lastname = 'test last name';
        c.email = 'xyzq2@abc07.com';
        insert c;
      
        System.runAs(testuser){
        Test.startTest();
 
        Task testtask = new Task();
        
        testtask.Subject = 'Marketing Qualified Lead - 001';
        testtask.Status = 'Not started';     
        testtask.Priority = 'Normal';
		testtask.RecordTypeId=recTypeId;
        testtask.WhoId = c.Id;
        testtask.Temp_Lead_Source__c = 'Request Consultation';
            
        insert testtask;
            
		testtask.Status  ='In Progress';       
        testtask.Subject = 'Marketing Qualified Lead - 001';
       testtask.Opportunity_Country__c='US';    
        testtask.Priority = 'Normal';

        testtask.WhoId = c.Id;
        update testtask;  
    
        testTask.Status = 'Closed - Create Opportunity';
        Task_Send_Notification_email.FLAG_CHECK_TASK = false;
        update testtask;

        Test.StopTest();
        }
}    
    
     
    
}