/*
Purpose: This Controller is an extension controller for the Opportunity Team Page. 
           
==============================================================================================================================================
History ----------------------- 
                                                           VERSION     AUTHOR  DATE        DETAIL 
                                                                  1.0 -           Suchi     2/20/2013  Created Controller Class
============================================================================================================================================== 
*/
public class MercVFC04_DefaultTeamController {
    public Opportunity opprec{get; set;}
    public Map<Id,UserTeamMember> UserIdUserTeamMap = new Map<Id,UserTeamMember>();
    public List<OpportunityTeamMember> OpportunityTeamMemberList = new List<OpportunityTeamMember>();
    public MercVFC04_DefaultTeamController(ApexPages.StandardController controller) {
    
        this.opprec = (Opportunity)controller.getRecord();
        
        
        Id OppOwnerID = [Select OwnerId from Opportunity where Id = :opprec.Id].OwnerId;
        
        for(UserTeamMember utm : [Select UserId,TeamMemberRole,OpportunityAccessLevel,OwnerId from UserTeamMember where OwnerID = :OppOwnerID ])
        {
            
            UserIdUserTeamMap.put(utm.UserId,utm);
        }    
        
        for(Id userId : UserIdUserTeamMap.keyset())
        {
                        //create a new instance of opportunity team member
                        OpportunityTeamMember teamMember = new OpportunityTeamMember();
                        //assign new record's owner to the team member user ID    
                        teamMember.userId = UserIdUserTeamMap.get(userId).userId;
                        //assign the new records ID to the opportunity team's opportunity reference    
                        teamMember.OpportunityId = opprec.Id;
                         //assign the new records Role to the opportunity team's opportunity reference 
                        teamMember.TeamMemberRole = UserIdUserTeamMap.get(userId).TeamMemberRole;
                        //add the instance to the list
                        
                        OpportunityTeamMemberList.add(teamMember);  
        }
        
         
    }
    
   public PageReference methodToInsert(){
            
            if(!OpportunityTeamMemberList.isEmpty()) Database.insert(OpportunityTeamMemberList, false);
            
            PageReference pageRef = new PageReference('/'+opprec.Id);
            return pageRef;
            
            
    }
    

}