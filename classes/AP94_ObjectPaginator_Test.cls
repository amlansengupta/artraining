/* ============================================================
 * This code is part of the "apex-lang" open source project avaiable at:
 * 
 *      http://code.google.com/p/apex-lang/
 *
 * This code is licensed under the Apache License, Version 2.0.  You may obtain a 
 * copy of the License at:
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * ============================================================
 */ 
 
 /*Purpose: Test class to provide test coverage for AP94_ObjectPaginator class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/21/2014  Created test class 
 
============================================================================================================================================== 
*/
@IsTest
private class AP94_ObjectPaginator_Test {

	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static void assertDefaultPageSize(AP94_ObjectPaginator paginator){
        System.assertEquals(AP94_ObjectPaginator.DEFAULT_PAGE_SIZE, paginator.pageSize);
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static void assertDefaultPageSizeOptions(AP94_ObjectPaginator paginator){
        System.assertEquals(AP94_ObjectPaginator.DEFAULT_PAGE_SIZE_OPTIONS, paginator.pageSizeIntegerOptions);
        final List<SelectOption> SEL_OPTIONS = new List<SelectOption>(); 
        for(Integer i : AP94_ObjectPaginator.DEFAULT_PAGE_SIZE_OPTIONS){
            SEL_OPTIONS.add(new SelectOption(''+i,''+i));
        }
        //ArrayUtils.assertArraysAreEqual(SEL_OPTIONS, paginator.pageSizeSelectOptions);
        System.assertNotEquals(null, paginator.getPageSizeOptions());
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static void assertDefaultSkipSize(AP94_ObjectPaginator paginator){
        System.assertEquals(AP94_ObjectPaginator.DEFAULT_SKIP_SIZE, paginator.skipSize);
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static void assertDefaultListener(AP94_ObjectPaginator paginator){
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(0, paginator.listeners.size());
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor01(){
        //global ObjectPaginator(){
        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator();
        assertDefaultPageSize(paginator);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor02(){
        //global AP94_ObjectPaginator(AP92_ObjectPaginatorListener listener ){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor03(){
        //global AP94_ObjectPaginator(List<Integer> pageSizeIntegerOptions ){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{-1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor04(){
        //global AP94_ObjectPaginator(List<Integer> pageSizeIntegerOptions,AP92_ObjectPaginatorListener listener ){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor05(){
        //global AP94_ObjectPaginator(List<Integer> pageSizeIntegerOptions,Integer skipSize ){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,10);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor06(){
        //global AP94_ObjectPaginator(List<Integer> pageSizeIntegerOptions,Integer skipSize,AP92_ObjectPaginatorListener listener ){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(EXAMPLE_PAGE_LIST_OPTIONS,10,EXAMPLE_LISTENER);
        assertDefaultPageSize(paginator);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor07(){
        //global AP94_ObjectPaginator(Integer pageSize ){

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor08(){
        //global AP94_ObjectPaginator(Integer pageSize,AP92_ObjectPaginatorListener listener ){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
    /*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor09(){
        //global AP94_ObjectPaginator(Integer pageSize,Integer skipSize ){
        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,10);
        
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor10(){
        //global AP94_ObjectPaginator(Integer pageSize,Integer skipSize,AP92_ObjectPaginatorListener listener ){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        assertDefaultPageSizeOptions(paginator);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
       
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor11(){
        //global AP94_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        assertDefaultListener(paginator);
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor12(){
        //global AP94_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions,AP92_ObjectPaginatorListener listener){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        assertDefaultSkipSize(paginator);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor13(){
        //global AP94_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions,Integer skipSize){
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,10);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        assertDefaultListener(paginator);
    }
   	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testConstructor14(){
        //global AP94_ObjectPaginator(Integer pageSize,List<Integer> pageSizeIntegerOptions, Integer skipSize, AP92_ObjectPaginatorListener listener){
        final AP92_ObjectPaginatorListener EXAMPLE_LISTENER = new AP93_ObjectPaginatorListenerForTesting();
        final List<Integer> EXAMPLE_PAGE_LIST_OPTIONS = new List<Integer>{1,2,3};

        final AP94_ObjectPaginator paginator = new AP94_ObjectPaginator(10,EXAMPLE_PAGE_LIST_OPTIONS,10,EXAMPLE_LISTENER);
        System.assertEquals(10, paginator.pageSize);
        System.assertEquals(EXAMPLE_PAGE_LIST_OPTIONS, paginator.pageSizeIntegerOptions);
        System.assertEquals(10, paginator.skipSize);
        System.assertNotEquals(null, paginator.listeners);
        System.assertEquals(1, paginator.listeners.size());
        System.assertEquals(EXAMPLE_LISTENER, paginator.listeners.get(0));
    }
  	/*
     * @Description : Test method to provide data coverage to AP94_ObjectPaginator class
     * @ Args       : Null
     * @ Return     : void
     */
    private static List<Object> createTestObject(Integer count){
        List<Object> records = new List<Object>();
        for(Integer i = 0; i < count; i++){
            records.add(i,i); 
        }
        return records;
    }
}