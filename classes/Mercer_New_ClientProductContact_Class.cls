/**
* @Description
* This class is controller for Mercer_New_ClientProductContact page which is override of New button 
* in Client Product Contact object. 
**/

public class Mercer_New_ClientProductContact_Class {
    public Client_Product_Contact__c objClient {get;set;}
    public String tcId {get;set;}
    
    //Constructor
    public Mercer_New_ClientProductContact_Class(ApexPages.StandardController controller) {
        objClient = new Client_Product_Contact__c();
        tcId = System.currentPageReference().getParameters().get('contractId');
        objClient.talent_contract__c= [select id,name from Talent_Contract__c where id=:tcId].id;
        
    }
    
    //Method related to Save Action
    public pageReference Save(){
        try{
            database.insert(objClient);
            return new PageReference('/'+tcId);
        }
        catch(Exception e){
            system.debug('Exception ..'+e);
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.error,''+e.getMessage()));
            return null;
        }
    }
    
    //Method related to Save and New Action
    public pageReference SaveNew(){
        try{
            database.insert(objClient);
            String recid = objClient.id;
            return new PageReference('/'+recid.subString(0,3)+'/e?contractId='+tcId+'&retURL='+tcId);
        }
        catch(Exception e){
            system.debug('Exception ..'+e);
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.error,''+e.getMessage()));
            return null;
        }
    }
}