/**
 * Created by emaleesoddy on 3/17/17.
 */

public without sharing class MP_CommunityUtilities {

    public class UserInfoWrapper {
        @AuraEnabled
        public User user {
            get;
            set;
        }
        @AuraEnabled
        public Contact contact {
            get;
            set;
        }
        @AuraEnabled
        public Account account {
            get;
            set;
        }

        public UserInfoWrapper(User u, Contact c, Account a) {
            user = u;
            contact = c;
            account = a;
        }

        public UserInfoWrapper infoWrapper { get; set; }
    }

    @AuraEnabled
    public static User getCurrentUser() {

        User user = [SELECT Id, ContactId, FirstName, LastName, CompanyName, CreatedDate FROM User WHERE Id = :UserInfo.getUserId()];
        return user;

    }

    @AuraEnabled
    public static UserInfoWrapper getAccountInfo() {

        User user = [SELECT Id, ContactId, FirstName, LastName, CompanyName, CreatedDate FROM User WHERE Id = :UserInfo.getUserId()];
        Contact contact = [SELECT Id, AccountId FROM Contact WHERE Id = :user.ContactId];
        Account acc = [SELECT Id, Name, PrivateCollaborationGroupID__c FROM Account WHERE Id = :contact.AccountId];

        UserInfoWrapper infoWrapper = new UserInfoWrapper(user, contact, acc);
        return infoWrapper;

    }

    @AuraEnabled
    public static User getGroupKeyContact(String groupId){
      try{
        Account a = [SELECT Global_Relationship_Manager__r.User_Record__c FROM Account WHERE PrivateCollaborationGroupID__c = :groupId LIMIT 1];
        return [SELECT Name, SmallPhotoUrl, MediumPhotoUrl, FullPhotoUrl, Title FROM User WHERE Id = :a.Global_Relationship_Manager__r.User_Record__c LIMIT 1];
      }catch(QueryException e){
        return null;
      }
    }

    @AuraEnabled
    public static Peak_Response getPrivateGroup() {
        Peak_Response response = new Peak_Response();
        User user = [SELECT Contact.Account.PrivateCollaborationGroupID__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        String privateGroupID = user.Contact.Account.PrivateCollaborationGroupID__c;

        if (!String.isEmpty(privateGroupID)){
            CollaborationGroup privateGroup = [
                    select BannerPhotoUrl, Description, FullPhotoUrl, Id, MemberCount, Name, SmallPhotoUrl
                    from CollaborationGroup
                    where Id = :privateGroupID
            ];

            if (privateGroup != null){
                response.results.add(privateGroup);
            }
        }

        return response;
    }
}