@isTest(seeAllData=true)
public class GetPicklistValues_Test {
    static testMethod void testgetPicklistValues()
    {	
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        test.startTest();
        testAccount.Account_Sub_Market__c='Canada';
        testAccount.GU_Country__c='Canada';
        testAccount.Outsourcing_Status__c='In Conversion';
        testAccount.Clients_in_Common__c='Single';
        testAccount.National_ID__c='TestNation';
        update testAccount;
        MF2_GlobalRelationshipCustomLinks__c customlinks = new MF2_GlobalRelationshipCustomLinks__c();
        customlinks.Link__c = 'https://www.mercer.com';
        customlinks.Type__c='link';
        customlinks.Name = 'testcustomlink';
        customlinks.UI_Label__c = 'testlabel';
        insert customlinks;
        ID  accId  = testAccount.id;
        String link = customlinks.UI_Label__c;
        
        getPicklistValues_LightningPOC.fetchCustomLinks(link);
        getPicklistValues_LightningPOC.getSubMarket();
        getPicklistValues_LightningPOC.getGUCountry();
        getPicklistValues_LightningPOC.getOutsourcingStatus();
        getPicklistValues_LightningPOC.getClientsinCommon();
        Account accObj = getPicklistValues_LightningPOC.getaccList(accId);
        getPicklistValues_LightningPOC.saveAccount(testAccount);
        system.assert(accObj.id == accId);
       // getPicklistValues_LightningPOC.isRunningUserInParentHierarchy(testAccount.Id);
        test.stopTest();
    }

    
    
}