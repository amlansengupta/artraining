/*Purpose: Test Class for providing code coverage to colleague functionality
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sarbpreet    12/06/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_Colleague 
{
    Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'LiteProfiles';
        setting.StrValue__c = [Select Id from Profile where Name LIKE '%LITE' LIMIT 1].Id;
        insert setting;
    }
/*
     * @Description : Test method to provide test coverage to colleague functionality
     * @ Args       : Null
     * @ Return     : void
     */
 static testMethod void testcolleague() 
    {
        createCS();
        System.runas(new User(Id = UserInfo.getUserId())){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //Create User Record
        User testUser = new User();
        testUser.alias = 'test';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'Test';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = mercerStandardUserProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'DC26_OppTeamMemberInsertBatch@abc.com';
        testUser.EmployeeNumber = '1234567890';
        testUser.Location__c = 'Test';
        testUser.isActive = True;
        testUser.Address2__c = null;
         testUser.CompanyName= null;
         testUser.Country= null;
         testUser.Department= null;
         testUser.Grade__c= null;
        testUser.Level_1_CD__c = null;
         testUser.Address3__c = null;
         testUser.Address2__c = null;                  
        insert testUser;
        //insert Merceroffice Geo
        Mercer_Office_Geo_D__c  mogd = new Mercer_Office_Geo_D__c ();
        mogd.Office__c = 'New Delhi';
        mogd.Region__c = 'International';
        mogd.Sub_Market__c = 'India';
        mogd.Sub_Region__c = 'Europe';
        mogd.Country__c = 'Japan';
        mogd.Market__c = 'Europe';
        insert mogd ;
        //insert merceroffice geo to differetiate location
        Mercer_Office_Geo_D__c  mogd2 = new Mercer_Office_Geo_D__c ();
        mogd2.Office__c = 'New Delhi';
        mogd2.Region__c = 'International';
        mogd2.Sub_Market__c = 'India';
        mogd2.Sub_Region__c = 'Asia';
        mogd2.Country__c = 'Japan';
        mogd.Market__c = 'Europe';
        insert mogd2 ;
        //Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.Level_1_CD__c = 'M1';
        testColleague.Address3__c = 'Test1';
        testColleague.Address2__c = 'Test2';
        testColleague.City__c = 'TestCity';
        testColleague.Country__c = 'TestCountry';
        testColleague.Currency_CD__c = 'USD';
        testColleague.Empl_Status__c = 'Active';
        testColleague.GLID__c = 'AMILLE00';                       
        testColleague.Prf_First_Name__c = 'prftest';
        testColleague.Prf_Last_Name__c = 'lasttest';
        testColleague.Last_Name__c = null;
        testColleague.First_Name__c = null;
        testColleague.Location__c = mogd.id;
        insert testColleague;
        
        //Collegue Record having Prf_First_Name__c <> null && Prf_Last_Name__c == null && Last_Name__c <> null
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678901';
        testColleague1.LOB__c = '111111';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Level_1_Descr__c = 'Merc1';
        testColleague1.Email_Address__c = 'abc1@abc.com';
        testColleague1.Level_1_CD__c = 'M11';
        testColleague1.Address3__c = 'Test11';
        testColleague1.Address2__c = 'Test21';
        testColleague1.City__c = 'TestCity1';
        testColleague1.Country__c = 'TestCountry1';
        testColleague1.Currency_CD__c = 'USD';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.GLID__c = 'AMILLE001';                       
        testColleague1.Prf_First_Name__c = 'prftest1';
        testColleague1.Prf_Last_Name__c = Null;
        testColleague1.Last_Name__c = 'Test1';
        testColleague1.First_Name__c = null;
        insert testColleague1;
        
         //Collegue Record having colleague.Prf_First_Name__c == null && colleague.Prf_Last_Name__c <> null && colleague.First_Name__c <> null
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '12345678902';
        testColleague2.LOB__c = '111112';
        testColleague2.Last_Name__c = 'TestLastName2';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.Level_1_Descr__c = 'Merc2';
        testColleague2.Email_Address__c = 'abc2@abc.com';
        testColleague2.Level_1_CD__c = 'M12';
        testColleague2.Address3__c = 'Test12';
        testColleague2.Address2__c = 'Test22';
        testColleague2.City__c = 'TestCity2';
        testColleague2.Country__c = 'TestCountry2';
        testColleague2.Currency_CD__c = 'USD2';
        testColleague2.Empl_Status__c = 'Active2';
        testColleague2.GLID__c = 'AMILLE002';                       
        testColleague2.Prf_First_Name__c = Null;
        testColleague2.Prf_Last_Name__c = 'Test2';
        testColleague2.Last_Name__c = 'Test2';
        testColleague2.First_Name__c = 'Test2';
        insert testColleague2;
        
          //Collegue Record having colleague.Prf_First_Name__c <> null && colleague.Prf_Last_Name__c <> null
        Colleague__c testColleague3 = new Colleague__c();
        testColleague3.Name = 'TestColleague3';
        testColleague3.EMPLID__c = '12345678903';
        testColleague3.LOB__c = '111113';
        testColleague3.Last_Name__c = 'TestLastName3';
        testColleague3.Empl_Status__c = 'Active3';
        testColleague3.Level_1_Descr__c = 'Merc3';
        testColleague3.Email_Address__c = 'abc3@abc.com';
        testColleague3.Level_1_CD__c = 'M13';
        testColleague3.Address3__c = 'Test13';
        testColleague3.Address2__c = 'Test23';
        testColleague3.City__c = 'TestCity3';
        testColleague3.Country__c = 'TestCountry3';
        testColleague3.Currency_CD__c = 'USD3';
        testColleague3.Empl_Status__c = 'Active3';
        testColleague3.GLID__c = 'AMILLE003';                       
        testColleague3.Prf_First_Name__c = 'Test3';
        testColleague3.Prf_Last_Name__c = 'Test3';
        testColleague3.Last_Name__c = 'Test3';
        testColleague3.First_Name__c = 'Test3';
        insert testColleague3;
        
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        insert testAccount;
        
        
        Test.StartTest();
            testColleague.Address2__c = null;
            testColleague.Address3__c = null;
            testColleague.GLID__c = null;
            //testColleague.EMPLID__c  = null;
            testColleague.Location__c = mogd2.id;
            update testColleague;
        Test.StopTest();  
        
      
        }
        
    }
    /*
     * @Description : Test method to provide test coverage to colleague functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testmethod void CheckUserUpdate(){
        createCS();
    System.runas(new User(Id = UserInfo.getUserId())){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //Create User Record
        User testUser = new User();
        testUser.alias = 'test';
        testUser.email = 'test@testabc.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'Test';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = mercerStandardUserProfile;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'madhavi@test1.com';
        testUser.EmployeeNumber = '12345678904';
        testUser.Location__c = 'Test';
        testUser.isActive = True;
        testUser.Address2__c = null;
         testUser.CompanyName= null;
         testUser.Country= null;
         testUser.Department= null;
         testUser.Grade__c= null;
        testUser.Level_1_CD__c = null;
         testUser.Address3__c = null;
         testUser.Address2__c = null;                  
        insert testUser;
        
        //Collegue Record having colleague.Prf_First_Name__c == null && colleague.Prf_Last_Name__c == null && colleague.First_Name__c <> null && colleague.Last_Name__c <> null
        
        Colleague__c testColleague4 = new Colleague__c();
        testColleague4.Name = 'TestColleague4';
        testColleague4.EMPLID__c = '12345678904';
        testColleague4.LOB__c = '111114';
        testColleague4.Last_Name__c = 'TestLastName4';
        testColleague4.Empl_Status__c = 'Active';
        testColleague4.Level_1_Descr__c = 'Merc4';
        testColleague4.Email_Address__c = 'abc4@abc.com';
        testColleague4.Level_1_CD__c = 'M14';
        testColleague4.Address3__c = 'Test14';
        testColleague4.Address2__c = 'Test24';
        testColleague4.City__c = 'TestCity4';
        testColleague4.Country__c = 'TestCountry4';
        testColleague4.Currency_CD__c = 'USD';
        testColleague4.Empl_Status__c = 'Active';
        testColleague4.GLID__c = 'AMILLE004';                       
        testColleague4.Prf_First_Name__c = Null;
        testColleague4.Prf_Last_Name__c = Null;
        testColleague4.Last_Name__c = 'Test4';
        testColleague4.First_Name__c = 'Test4';
        insert testColleague4;
        
        Test.startTest();    
        testColleague4.Address3__c = 'New Address4';
        testColleague4.Email_Address__c = 'abc5@abc.com';
        update testColleague4;
        Test.stopTest();
    }
    
    }
    
       
    
    
 }