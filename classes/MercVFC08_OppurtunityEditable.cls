/*Purpose: This Controller is an extension controller for the OppurtunityEditable Page. 
           
=========================================================================================================================================

History ----------------------- 
                                                                       VERSION     AUTHOR     DATE                 DETAIL    
                                                                          1.0                 Arijit Roy    03/29/2013      Created a controller for MercVFC08_OppurtunityEditable Page.  ============================================================================================================================================== 
*/

public with sharing class MercVFC08_OppurtunityEditable{
    
    public Opportunity opp{get;set;}
    
    public MercVFC08_OppurtunityEditable(ApexPages.standardController controller)
    {
        this.opp = (Opportunity)controller.getRecord();
    }
    

    public pagereference editOpportunity()
    {
            
        RecordType recType = [select Id from RecordType where Name = 'Opportunity Detail Page Assignment'];    
        opp.recordtypeid = recType.Id;
        PageReference pageRef = new PageReference('/'+opp.id);
        update opp;
        
        return  pageRef ;
        
     }
  }