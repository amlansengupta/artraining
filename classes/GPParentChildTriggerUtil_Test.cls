@isTest
public class GPParentChildTriggerUtil_Test {
    private static testmethod void GPParentChildMethod(){
        Account Acc = new Account(name = 'Test Account',BillingCountry='test');
        insert Acc;
        
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()),12,31));
        insert py;
        
        Growth_Plan__c gp1 = new Growth_Plan__c(Account__c = Acc.id,fcstPlanning_Year__c = py.id);
        insert gp1;
        
        Account Acc2 = new Account(name = 'Test Account2',BillingCountry='test2');
        insert Acc2;
        
        Growth_Plan__c gp2 = new Growth_Plan__c(Account__c = Acc2.id,fcstPlanning_Year__c = py.id,Parent_Growth_Plan_ID__c = gp1.id);
        insert gp2;
        
       
        
        List<Growth_Plan__c> parentList = new List<Growth_Plan__c>();
        parentList.add(gp2);      
        
        List<Growth_Plan__c> childList = new List<Growth_Plan__c>();
        childList.add(gp1);    
        GPParentChildTriggerUtil.parentChildafterUpdate(parentList);        
    }
}