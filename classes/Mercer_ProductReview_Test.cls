/*
==============================================================================================================================================
Request Id                                				 Date                    Modified By
12638:Removing Step										 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest
public class Mercer_ProductReview_Test {
static testMethod void myUnitTest1() {
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Mercer Standard']; 
    
    List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
         
          setting = new ApexConstants__c();
        setting.Name = 'LiteProfiles';
        setting.StrValue__c = p1.id;
        listofvalues.add(setting);
         
        
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
          /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
    Request No. 17953 : Removing Finalist END */
        
        Insert listofvalues;
    
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User tUser = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        insert tUser;
    Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = '1234';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        testColleague1.Empl_Status__c = 'Active';
        insert testColleague1;
    
         System.runAs(tUser){
            Mercer_TestData mdata = new Mercer_TestData();
            Account acc = mdata.buildAccount();
            acc.one_code__c ='123';
            acc.One_Code_Status__c ='Active';
            Update acc;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = acc.Id;
            insert testContact;
                 
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty4';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acc.id;
            testOppty.Opportunity_Office__c = 'New York - 1166';
            //Request id:12638 commenting step        
            //testOppty.Step__c = 'Identified Deal';
            testOppty.OwnerId = tUser.Id;
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Amount = 100;
            testOppty.Buyer__c= testContact.id;    
            insert testOppty; 
             ApexPages.currentPage().getParameters().put('salesrevcheck','True');
             ApexPages.currentPage().getParameters().put('revcheck','True');
             ApexPages.currentPage().getParameters().put('salescheck','True');
            ApexPages.currentPage().getParameters().put('isrevadjustbool','True');
             ApexPages.standardController controller =new ApexPages.standardController(testOppty);
             Mercer_ProductReview cntrl=new Mercer_ProductReview(controller);
             cntrl.delegatesetUp();
             
         }
    }
}