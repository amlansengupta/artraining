/***************************************************************************************************************
Request Id : 17173
Purpose : This Test Class is created for the Apex Class OpportunityUtility
Created by : Archisman Ghosh
Created Date : 16/05/2019
Project : MF2
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class OpportunityUtility_Test {
	@isTest
    public static void customLinks_Positive(){
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        testOppty.Sibling_Company__c='Marsh';
        testOppty.Business_LOB__c='Health';
       // testOppty.Sibling_Contact_Name__c = 'Barbara Tester';
        testOppty.Potential_Revenue__c=100;
        testOppty.Cross_Sell_ID__c='CS731SY945';
        insert testOppty;
        String JSONString = JSON.serialize(testOppty);

        
        ID  opptyId  = testOppty.id;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            OpportunityUtility.recordTypeCheck(opptyId);
            OpportunityUtility.saveOpp(JSONString,opptyId);
        }
     }
    @isTest
    public static void customLinks_Negative(){
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        testOppty.Cross_Sell_ID__c='12345';
        
       Colleague__c testColleague = new Colleague__c();
                testColleague.Name = 'TestColleague';
                testColleague.Last_Name__c = 'testLastName';
                testColleague.EMPLID__c = '1234567890';
                testColleague.LOB__c = '11111';
                testColleague.Email_Address__c = 'abc@gmail.com';
                insert testColleague;
        testOppty.Sibling_Contact_Name__c = testColleague.Id;        
        
        
        insert testOppty;
        String JSONString1 = JSON.serialize(testOppty);
        
        ID  opptyId  = testOppty.id; 
        
        try{
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
            
            System.runAs(user1) {
                OpportunityUtility.recordTypeCheck(opptyId);
                OpportunityUtility.saveOpp(JSONString1,opptyId);
            }
        }
        catch(Exception ex)
        {
            
        }
    }
}