/*Purpose: This Apex class implements schedulable interface to schedule MS12_AccountActivitiesBatchable class.
==============================================================================================================================================
History ----------------------- 
                                                                 VERSION     AUTHOR       DATE                    DETAIL 
                                                                    1.0 -    Arijit     04/29/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS11_AccountActivitiesSchedulable implements Schedulable{
    
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS12_AccountActivitiesBatchable(),  Integer.valueOf(System.Label.CL77_MS12BatchSize));
    }
}