public class DisplayGlobalContractMsgController{
    public Opportunity thisOpportunity;
    public String showGlobalContractMsg{get;set;}
    public String showGOCMsg{get;set;}
    
    public DisplayGlobalContractMsgController(ApexPages.StandardController controller) {    
    thisOpportunity=(Opportunity)controller.getRecord();
    Opportunity opp = new Opportunity();
    opp = [Select Id,Name,StageName,Account.Type,GOC_SUBMIT_STATUS__c,Account.GOC_SUBMIT_STATUS__c,Account.Global_Contract__c,Account.Global_Client_Strategy__c,Account.Global_Rate_Card__c, 
           Account.Global_Client_Governance__c,Account.Global_Special_Terms__c,Account.Other_Global_Agreement__c
           From Opportunity Where Id = :thisOpportunity.Id ];
    
    if(String.isNotBlank(opp.Account.Global_Contract__c) || String.isNotBlank(opp.Account.Global_Client_Strategy__c) || 
       String.isNotBlank(opp.Account.Global_Rate_Card__c) || String.isNotBlank(opp.Account.Global_Client_Governance__c) ||
       String.isNotBlank(opp.Account.Global_Special_Terms__c) || String.isNotBlank(opp.Account.Other_Global_Agreement__c))
       {
            showGlobalContractMsg = 'A Global Contract/Rate Card is in place for this client. Please review account for details';   
       }
    String stageNametemp = opp.StageName;
    String accountType = opp.Account.Type;
    String gocStatus = opp.GOC_SUBMIT_STATUS__c;
        if(gocStatus == null){
            gocStatus = 'None';
        }else{
            gocStatus = gocStatus;
        }
        //12638 merging Closed / Client Cancelled and Closed / Declined to Bid to Closed / Cancelled
    /*if((stageNametemp!= 'Above the Funnel' && stageNametemp!= 'Identify' && stageNametemp!= 'Closed / Lost' && 
       stageNametemp!= 'Closed / Declined to Bid' && stageNametemp!= 'Closed / Client Cancelled' && stageNametemp!= 'Closed / Won') && accountType == 'Marketing'){
            showGOCMsg = 'This account is a Marketing client. To be able to set up project codes and close your opportunity as won this account must be promoted to Client Type "Client" via Workflow Wizard. The automatic promotion request is currently ' + gocStatus + '. See Additional Details for more information.';
    }*/
    //showGlobalContractMsg = 'Global Contract in place';
    if((stageNametemp!= 'Above the Funnel' && stageNametemp!= 'Identify' && stageNametemp!= 'Closed / Lost' && 
      stageNametemp!= 'Closed / Cancelled' && stageNametemp!= 'Closed / Won') && accountType == 'Marketing'){
            showGOCMsg = 'This account is a Marketing client. To be able to set up project codes and close your opportunity as won this account must be promoted to Client Type "Client" via Workflow Wizard. The automatic promotion request is currently ' + gocStatus + '. See Additional Details for more information.';
    }
    }
}