/*
Purpose: This test class provides data coverage to AP40_BuyingInfluenceTriggerUtil class

==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Shashank 03/29/2013  Created Test Class.
=============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP40_BuyingInfluenceTriggerUtil 
{
    /*
     * @Description : Test method to create/delete contact role whenever a buying infleunce is created/deleted.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '1234562222';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc3579@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        test.starttest();
         Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638-Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Honolulu - Fort';
        testOppty.Amount = 100;
        testOppty.Buyer__c= testContact.id;  
        testOppty.CurrencyIsoCode='USD';
        testOppty.Concatenated_Products__c='529 Plan;DB Risk';
        insert testOppty;
        
          
        Buying_Influence__c testBuy = new Buying_Influence__c();
        testBuy.Contact__c = testContact.Id;
        testBuy.Opportunity__c = testOppty.Id;
        insert testBuy;
        
        //delete testBuy;
        checkTriggerRecursive1.isStartKCBI1 = false;
        testBuy.Contact__c = testContact.Id;
        update testBuy;
        
        checkTriggerRecursive1.isStartKCBI2 = false;
        testBuy.Contact__c = testContact.Id;
        update testBuy;
        
        checkTriggerRecursive1.isStartKCBI1 = false;
        delete testBuy;
        
        checkTriggerRecursive1.runOnce();
        checkTriggerRecursive1.runOnce();
        test.stoptest();
    }
    static testMethod void myUnitTest1() 
    {
     /*   Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '1233333902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc2468@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Contact testContact1 = new Contact();
        testContact1.Salutation = 'Fr.';
        testContact1.FirstName = 'TestFirstName1';
        testContact1.LastName = 'TestLastName1';
        testContact1.Job_Function__c = 'TestJob';
        testContact1.Title = 'TestTitle';
        testContact1.MailingCity  = 'TestCity';
        testContact1.MailingCountry = 'TestCountry';
        testContact1.MailingState = 'TestState'; 
        testContact1.MailingStreet = 'TestStreet'; 
        testContact1.MailingPostalCode = 'TestPostalCode'; 
        testContact1.Phone = '9999999999';
        testContact1.Email = 'abc1@xyz.com';
        testContact1.MobilePhone = '9999999999';
        testContact1.AccountId = testAccount.Id;
        insert testContact1;        
        
         Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id:12638-commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Houston - Dallas';
        testOppty.Amount = 100;
        //testOppty.Buyer__c= testContact.id;  
        testOppty.CurrencyIsoCode='USD';
        testOppty.Concatenated_Products__c='529 Plan;DB Risk';
        insert testOppty;
        
          
        Buying_Influence__c testBuy = new Buying_Influence__c();
        testBuy.Contact__c = testContact.Id;
        testBuy.Opportunity__c = testOppty.Id;
        insert testBuy;
        
        checkTriggerRecursive1.isStartKCBI1 = false;
        testBuy.Contact__c = testContact1.Id;
        update testBuy;
        
        checkTriggerRecursive1.isStartKCBI2 = false;
        testBuy.Contact__c = testContact.Id;
        update testBuy;
        
        checkTriggerRecursive1.isStartKCBI1 = false;
        delete testBuy;
        
        checkTriggerRecursive1.runOnce();
        checkTriggerRecursive1.runOnce();*/
    }
}