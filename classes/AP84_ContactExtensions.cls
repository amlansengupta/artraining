/*Purpose:  This Apex class acts as Extensions and also contains  methods which are used in Mercer_Contact_New visualforce page 
==============================================================================================================================================
The methods called perform following functionality:

•   Applies business logic for button Save and New

History 
-----------------------------------------------------------------------------------------------------
VERSION     AUTHOR       DATE        DETAIL 
   1.0 -    Ravikiran   1/23/2014  Requirement#3543 (Rearrange fields of Contacts  Page) , part of March2014 release' :
                                    New Contact VF page has been designed and this is controller class for this requirement
   
   2.0 -    Venkatesh   6/10/2015  Requirement #5166
============================================================================================================================================== 
*/
public with sharing class AP84_ContactExtensions {
    private ApexPages.StandardController contactController;
    Contact contact {get; set;}
    List<sObject> duplicateRecords {get; set;}
    public boolean hasDuplicateResult{get;set;}
    
    /*Constructor of the class*/
    Public AP84_ContactExtensions(){
        this.duplicateRecords = new List<sObject>();
        
    }
    
    /*Constructor of the class, standardcontroller's instance is captured in
    this for further use*/
    public AP84_ContactExtensions(ApexPages.StandardController controller) {
        contactController = controller;
        contact = (Contact) contactController.getRecord();
    }
    
    /* Return duplicate records to the Visualforce page for display
    */
    public List<sObject> getDuplicateRecords() {
        return this.duplicateRecords;
    }
    
    /* This Method is created to leverage "Save and New " functionlity
    logic is to use Standard Controller's save and new functionlity.
    */
    public PageReference saveAndNew() {
         PageReference pr =contactController.save();
         if(pr==NULL){
           return null;
         }
         else{
          return Page.Mercer_Contact_New.setRedirect(true);
         }
       }


   /*Added by C. Pon Venkatesh for Req #5166
   */
   public PageReference SaveNonDuplicates(){
       Database.DMLOptions dml = new Database.DMLOptions(); 
       dml.DuplicateRuleHeader.allowSave = false;
       Database.SaveResult saveResult = Database.insert(contact, dml);
    
       if(!saveResult.isSuccess()) {          
           for (Database.Error error : saveResult.getErrors()){
               System.debug('Inside error: '+error);

               if (error instanceof Database.DuplicateError) {
                   Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                   Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();

                   this.duplicateRecords = new List<sObject>();
                
                   // Return only match results of matching rules that find duplicate records
                   Datacloud.MatchResult[] matchResults = duplicateResult.getMatchResults();

                   // First match result (which contains the duplicate record found and other match info)
                   Datacloud.MatchResult matchResult = matchResults[0];

                   Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                   // Add matched record to the duplicate records variable
                   for (Datacloud.MatchRecord matchRecord : matchRecords) {
                       this.duplicateRecords.add(matchRecord.getRecord());
                   }
                   ApexPages.addMessage(
                       new ApexPages.Message(
                           ApexPages.Severity.ERROR, 
                           System.Label.Manual_contact_creation_duplicate
                       )
                   );
                   this.hasDuplicateResult = !this.duplicateRecords.isEmpty();
                }else{
                    ApexPages.addMessage(
                       new ApexPages.Message(
                           ApexPages.Severity.ERROR, 
                           error.getMessage()
                       )
                    );
                }
            }
            return null;
        }else{
            return (new ApexPages.StandardController(contact)).view();
        }
    }
}