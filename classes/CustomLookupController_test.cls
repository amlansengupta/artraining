@isTest

public class CustomLookupController_test{

    static testMethod void customLookupTest(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = 'ABC123';
        //testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active'; 
        testContact.email = 'abc@xyz.com';         
        insert testContact;
        
        Talent_Contract__c testTc = new Talent_Contract__c();
        testTc.Account__c = testAccount.id;
        TestTc.Product__c = 'WIN ePRISM';
        insert TestTc;
        
        System.currentPageReference().getParameters().put('lksrch','003');
        System.currentPageReference().getParameters().put('object','Contact');
        System.currentPageReference().getParameters().put('acctid',testAccount.id);
        System.currentPageReference().getParameters().put('id',TestTc.id);
        
        CustomLookupController objC = new CustomLookupController();
        objC.search();
        //objC.performSearch('Test');
        //objC.performtalenttsmoduleSearch('Test');
        //objC.performclientproductcontactSearch('Test');
        objC.getFormTag();
        objC.getTextBox();
        
        System.currentPageReference().getParameters().put('lksrch','a1d');
        System.currentPageReference().getParameters().put('object','talenttsmodule');
        CustomLookupController objC2 = new CustomLookupController();
        
        //System.currentPageReference().getParameters().put('id',testContact.id);
        System.currentPageReference().getParameters().put('lksrch','003');
        System.currentPageReference().getParameters().put('object','clientproductcontact');
        CustomLookupController objC3 = new CustomLookupController();
        
        
    }

}