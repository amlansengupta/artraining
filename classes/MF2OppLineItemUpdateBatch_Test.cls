/***************************************************************************************************************
Request Id : 18835
Purpose : This Test Class is created for the Apex Class OpportunityValidator
Created by : Archisman Ghosh
Created Date : 22/01/2019
Project : MF2
****************************************************************************************************************/
@isTest
public class MF2OppLineItemUpdateBatch_Test 
{   
    static testMethod void testOpportunityValidator()
    {   
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
       Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        
        testOppty.Buyer__c=testContact.Id;
        testOppty.AccountId=testAccount.Id;
     
        insert testOppty;
        
        ID  opptyId  = testOppty.id;
        
         test.startTest();
       
        //Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
          Id pricebookId = Test.getStandardPricebookId();   
                
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        insert pro;
        
      
       /* PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice; */
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = pro.Id,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice; 
        
       // PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :customPB.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1]; 
        
        
        OpportunityLineItem testOli= new OpportunityLineItem();
        testOli.OpportunityId=testOppty.id;
        testOli.PricebookEntryId = customPrice.Id;
        testOli.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+1);
        testOli.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
        testOli.UnitPrice = 1.00;
        testOli.Sales_Price_USD_calc__c = 1.00;
        testOli.CurrentYearRevenue_edit__c = 0.00;
        testOli.Year2Revenue_edit__c = 0.00;
        testOli.Year3Revenue_edit__c = 1.00;
        testOli.Is_Project_Required__c=true;
        testOli.Project_Linked__c=false;
        insert testOli;
        MF2_OppLineItemUpdateBatch batch1 = new MF2_OppLineItemUpdateBatch('open');
        database.executeBatch(batch1);
        MF2_OppLineItemUpdateBatch batch2 = new MF2_OppLineItemUpdateBatch('closed');
        database.executeBatch(batch2);
        
       
         test.stopTest();
    }
}