/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(SeeAllData=True)

public class Test_AP10_SalesCreditTriggerUtil {

/*Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    }*/

 static testMethod void test_AP10_SalesCreditTriggerUtil() {
Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User tUser = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',EmployeeNumber = '12345678902', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
  insert tUser;
  Colleague__c testColleague = new Colleague__c();
  testColleague.Name = 'TestColleague';
  testColleague.Last_Name__c = 'TestLastName';
  testColleague.EMPLID__c = '12345678902';
  testColleague.LOB__c = 'Health';
  testColleague.Empl_Status__c = 'Active';
  testColleague.Email_Address__c = 'abc@abc.com';
  testColleague.Country__c = 'USA';
  insert testColleague;
  
  Colleague__c testColleague1 = new Colleague__c();
  testColleague1.Name = 'TestColleague1';
  testColleague1.Last_Name__c = 'TestLastName1';
  testColleague1.EMPLID__c = '123456789021';
  testColleague1.LOB__c = 'Health';
  testColleague1.Empl_Status__c = 'Active';
  testColleague1.Email_Address__c = 'abc1@abc.com';
  testColleague1.Country__c = 'USA';
  insert testColleague1;


   
  
  Account testAccount = new Account();
  testAccount.Name = 'TestAccountName';
  testAccount.BillingCity = 'TestCity';
  testAccount.BillingCountry = 'TestCountry';
  testAccount.BillingStreet = 'Test Street';
  testAccount.Relationship_Manager__c = testColleague.Id;
  insert testAccount;
  
  
  // Opportunity testOppty = new Opportunity();
  // System.runAs(user1) {
   /*
   testOppty.Name = 'TestOppty';
   testOppty.Type = 'New Client';
   testOppty.AccountId = testAccount.Id;
   testOppty.Step__c = 'Identified Deal';
   testOppty.StageName = 'Pending Step';
   testOppty.CloseDate = date.Today();
   insert testOppty;
   
  */
  //Test Class Fix
  
  AP44_ChatterFeedReporting.FROMFEED = True;
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;     
        //request id:12638 commenting step   
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'New York - 1166';
     testOppty.Opportunity_region__C='North America';
        insert testOppty;
 
   test.starttest();
 
  List < OpportunityTeamMember > Otm = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id and TeamMemberRole!='Opportunity Owner'];
  delete Otm;

  Sales_Credit__c testSales = new Sales_Credit__c();
  testSales.Opportunity__c = testOppty.Id;
  testSales.EmpName__c = testColleague.Id;
  testSales.Percent_Allocation__c = 10;
  testSales.Role__c ='Sales Professional';
  insert testSales;
  testSales.Percent_Allocation__c = 50;
  update testSales;

  List < OpportunityTeamMember > Otm2 = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id];

  
   User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
  Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Colleague__c = testColleague.id;
        iSalesGoal.OwnerId = tu.Id;
        Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
        iSalesGoal1.Sales_Goals__c = 500;
        iSalesGoal1.Colleague__c = testColleague1.id;
        iSalesGoal1.OwnerId = tu.Id;
        Sales_Credit_Line_Item__c scli = new Sales_Credit_Line_Item__c();
        //Test.startTest();
        System.runAs(tUser){ 
        insert iSalesGoal;      
        insert iSalesGoal1;
        scli.Individual_Sales_Goal__c = iSalesGoal.Id;
        scli.Sales_Credit_lookup__c = testSales.Id;
        insert scli;
        } 
        
        List<Sales_Credit__c> salesCred=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterUpdate(salesCredmap,salesCredmap);
        

       
       
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap,salesCredmap);  
        testSales.EmpName__c = testColleague1.Id;
        update testSales;
        List<Sales_Credit__c> salesCred1=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap1= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred1[0]);
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap1,salesCredmap);  
        Test.stopTest();       
        AP10_SalesCreditTriggerUtil.deleteSalesCreditLineItem(salesCred);          
  
  
   
  
 }
 
 static testMethod void test_AP10_SalesCreditTriggerUtil2() {
Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User tUser = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',EmployeeNumber = '12345678902', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
  insert tUser;
  Colleague__c testColleague = new Colleague__c();
  testColleague.Name = 'TestColleague';
  testColleague.Last_Name__c = 'TestLastName';
  testColleague.EMPLID__c = '12345678902';
  testColleague.LOB__c = 'Health';
  testColleague.Empl_Status__c = 'Active';
  testColleague.Email_Address__c = 'abc@abc.com';
  testColleague.Country__c = 'USA';
  insert testColleague;
  
  Colleague__c testColleague1 = new Colleague__c();
  testColleague1.Name = 'TestColleague1';
  testColleague1.Last_Name__c = 'TestLastName1';
  testColleague1.EMPLID__c = '123456789021';
  testColleague1.LOB__c = 'Health';
  testColleague1.Empl_Status__c = 'Active';
  testColleague1.Email_Address__c = 'abc1@abc.com';
  testColleague1.Country__c = 'USA';
  insert testColleague1;


   
  
  Account testAccount = new Account();
  testAccount.Name = 'TestAccountName';
  testAccount.BillingCity = 'TestCity';
  testAccount.BillingCountry = 'TestCountry';
  testAccount.BillingStreet = 'Test Street';
  testAccount.Relationship_Manager__c = testColleague.Id;
  insert testAccount;
  
  
  // Opportunity testOppty = new Opportunity();
  // System.runAs(user1) {
   /*
   testOppty.Name = 'TestOppty';
   testOppty.Type = 'New Client';
   testOppty.AccountId = testAccount.Id;
   testOppty.Step__c = 'Identified Deal';
   testOppty.StageName = 'Pending Step';
   testOppty.CloseDate = date.Today();
   insert testOppty;
   
  */
  //Test Class Fix
  
  AP44_ChatterFeedReporting.FROMFEED = True;
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;     
        //request id:12638 commenting step   
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'New York - 1166';
     testOppty.Opportunity_region__C='International';
        insert testOppty;
 
   test.starttest();
 
  List < OpportunityTeamMember > Otm = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id and TeamMemberRole!='Opportunity Owner'];
  delete Otm;

  Sales_Credit__c testSales = new Sales_Credit__c();
  testSales.Opportunity__c = testOppty.Id;
  testSales.EmpName__c = testColleague.Id;
  testSales.Percent_Allocation__c = 10;
  testSales.Role__c ='Sales Professional';
  insert testSales;
  testSales.Percent_Allocation__c = 90;
  update testSales;

  List < OpportunityTeamMember > Otm2 = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id];

  
   User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
  Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Colleague__c = testColleague.id;
        iSalesGoal.OwnerId = tu.Id;
        Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
        iSalesGoal1.Sales_Goals__c = 500;
        iSalesGoal1.Colleague__c = testColleague1.id;
        iSalesGoal1.OwnerId = tu.Id;
        Sales_Credit_Line_Item__c scli = new Sales_Credit_Line_Item__c();
        //Test.startTest();
        System.runAs(tUser){ 
        insert iSalesGoal;      
        insert iSalesGoal1;
        scli.Individual_Sales_Goal__c = iSalesGoal.Id;
        scli.Sales_Credit_lookup__c = testSales.Id;
        insert scli;
        } 
        
        List<Sales_Credit__c> salesCred=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterUpdate(salesCredmap,salesCredmap);
        

       
       
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap,salesCredmap);  
        testSales.EmpName__c = testColleague1.Id;
        update testSales;
        List<Sales_Credit__c> salesCred1=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap1= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred1[0]);
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap1,salesCredmap);  
        Test.stopTest();       
        AP10_SalesCreditTriggerUtil.deleteSalesCreditLineItem(salesCred);          
  
  
   
  
 }
  static testMethod void test_AP10_SalesCreditTriggerUtil3() {
Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User tUser = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',EmployeeNumber = '12345678902', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
  insert tUser;
  Colleague__c testColleague = new Colleague__c();
  testColleague.Name = 'TestColleague';
  testColleague.Last_Name__c = 'TestLastName';
  testColleague.EMPLID__c = '12345678902';
  testColleague.LOB__c = 'Health';
  testColleague.Empl_Status__c = 'Active';
  testColleague.Email_Address__c = 'abc@abc.com';
  testColleague.Country__c = 'USA';
  insert testColleague;
  
  Colleague__c testColleague1 = new Colleague__c();
  testColleague1.Name = 'TestColleague1';
  testColleague1.Last_Name__c = 'TestLastName1';
  testColleague1.EMPLID__c = '123456789021';
  testColleague1.LOB__c = 'Health';
  testColleague1.Empl_Status__c = 'Active';
  testColleague1.Email_Address__c = 'abc1@abc.com';
  testColleague1.Country__c = 'USA';
  insert testColleague1;


   
  
  Account testAccount = new Account();
  testAccount.Name = 'TestAccountName';
  testAccount.BillingCity = 'TestCity';
  testAccount.BillingCountry = 'TestCountry';
  testAccount.BillingStreet = 'Test Street';
  testAccount.Relationship_Manager__c = testColleague.Id;
  insert testAccount;
  
  
  // Opportunity testOppty = new Opportunity();
  // System.runAs(user1) {
   /*
   testOppty.Name = 'TestOppty';
   testOppty.Type = 'New Client';
   testOppty.AccountId = testAccount.Id;
   testOppty.Step__c = 'Identified Deal';
   testOppty.StageName = 'Pending Step';
   testOppty.CloseDate = date.Today();
   insert testOppty;
   
  */
  //Test Class Fix
  
  AP44_ChatterFeedReporting.FROMFEED = True;
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;     
        //request id:12638 commenting step   
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'New York - 1166';
     testOppty.Opportunity_region__C='International';
        insert testOppty;
 
   test.starttest();
 
  List < OpportunityTeamMember > Otm = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id and TeamMemberRole!='Opportunity Owner'];
  delete Otm;

  Sales_Credit__c testSales = new Sales_Credit__c();
  testSales.Opportunity__c = testOppty.Id;
  testSales.EmpName__c = testColleague.Id;
  testSales.Percent_Allocation__c = 10;
  testSales.Role__c ='Client Coordinator';
  insert testSales;
  testSales.Percent_Allocation__c = 100;
  update testSales;

  List < OpportunityTeamMember > Otm2 = [SELECT Id, OpportunityId, UserId from OpportunityTeamMember where OpportunityId = : testOppty.id];

  
   User tu = [select Id, EmployeeNumber from User where EmployeeNumber = '12345678902' limit 1];
  Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        iSalesGoal.Colleague__c = testColleague.id;
        iSalesGoal.OwnerId = tu.Id;
        Individual_Sales_Goal__c iSalesGoal1 = new Individual_Sales_Goal__c();
        iSalesGoal1.Sales_Goals__c = 500;
        iSalesGoal1.Colleague__c = testColleague1.id;
        iSalesGoal1.OwnerId = tu.Id;
        Sales_Credit_Line_Item__c scli = new Sales_Credit_Line_Item__c();
        //Test.startTest();
        System.runAs(tUser){ 
        insert iSalesGoal;      
        insert iSalesGoal1;
        scli.Individual_Sales_Goal__c = iSalesGoal.Id;
        scli.Sales_Credit_lookup__c = testSales.Id;
        insert scli;
        } 
        
        List<Sales_Credit__c> salesCred=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterUpdate(salesCredmap,salesCredmap);
        

       
       
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap,salesCredmap);  
        testSales.EmpName__c = testColleague1.Id;
        update testSales;
        List<Sales_Credit__c> salesCred1=[Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales.Id Limit 1];
        Map<Id,Sales_Credit__c> salesCredmap1= new Map<Id,Sales_Credit__c>();
        salesCredmap.put(testSales.Id,salesCred1[0]);
        AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(salesCredmap1,salesCredmap);  
        Test.stopTest();       
        AP10_SalesCreditTriggerUtil.deleteSalesCreditLineItem(salesCred);          
  
  
   
  
 }
static testMethod void test_AP10_SalesCreditTriggerUtil1()
{
    User testUser1= new User();
    User User1 = new User();
  String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
  User1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
  
  
  
        testUser1.alias = 'abc';
        testUser1.email = 'abc007@abc.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'TestLastName2';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = mercerStandardUserProfile;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'abc007@abc.com';
        testUser1.EmployeeNumber = '123456789022';
    testUser1.IsActive =true;
        Database.insert(testUser1);
        
        User testUser2 = new User();
        
        testUser2.alias = 'abc';
        testUser2.email = 'abc007@abc.com';
        testUser2.emailencodingkey='UTF-8';
        testUser2.lastName = 'TestLastName2';
        testUser2.languagelocalekey='en_US';
        testUser2.localesidkey='en_US';
        testUser2.ProfileId = mercerStandardUserProfile;
        testUser2.timezonesidkey='Europe/London';
        testUser2.UserName = 'abc0010@abc.com';
        testUser2.EmployeeNumber = '123456789024';
        Database.insert(testUser2);
        
        
      /*  User testUser3 = new User();
        
        testUser3.alias = 'abc';
        testUser3.email = 'abc007@abc.com';
        testUser3.emailencodingkey='UTF-8';
        testUser3.lastName = 'TestLastName2';
        testUser3.languagelocalekey='en_US';
        testUser3.localesidkey='en_US';
        testUser3.ProfileId = mercerStandardUserProfile;
        testUser3.timezonesidkey='Europe/London';
        testUser3.UserName = 'abc114@abc.com';
        testUser3.EmployeeNumber = '123456789025';
        Database.insert(testUser3);
        
         
        
        Colleague__c testColleague4 = new Colleague__c();
        testColleague4.Name = 'TestColleague4';
        testColleague4.Last_Name__c = 'TestLastName2';
        testColleague4.EMPLID__c = '123456789025';
        testColleague4.LOB__c = 'Region Market Development';
        testColleague4.Empl_Status__c = 'Active';
        testColleague4.Email_Address__c = 'abc007@abc.com';
        testColleague4.Country__c = 'USA';
        insert testColleague4;*/
       
        
      
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.Last_Name__c = 'TestLastName2';
        testColleague2.EMPLID__c = '123456789022';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.Email_Address__c = 'abc007@abc.com';
        testColleague2.Country__c = 'USA';
        insert testColleague2;
        
        Colleague__c testColleague3 = new Colleague__c();
        testColleague3.Name = 'TestColleague3';
        testColleague3.Last_Name__c = 'TestLastName2';
        testColleague3.EMPLID__c = '123456789024';
        testColleague3.LOB__c = 'Health';
        testColleague3.Empl_Status__c = 'Active';
        testColleague3.Email_Address__c = 'abc009@abc.com';
        testColleague3.Country__c = 'USA';
        testColleague3.SP_Flag__c ='Y';
        insert testColleague3;
        
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        testAccount1.Relationship_Manager__c = testColleague2.Id;
        insert testAccount1;
        //Test Class Fix
        
           
        AP44_ChatterFeedReporting.FROMFEED = True;
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOppty1';
        testOppty1.Type = 'New Client';
        testOppty1.AccountId = testAccount1.id;
        testOppty1.OwnerId=testUser2.Id;
        //request id:12638 commenting step        
        //testOppty1.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty1.StageName = 'Identify';
        testOppty1.CloseDate = date.Today();
        testOppty1.CurrencyIsoCode = 'ALL';
        testOppty1.Opportunity_Office__c = 'New York - 1166';
        
        insert testOppty1;
        
  test.starttest();  
        
  Sales_Credit__c testSales1 = new Sales_Credit__c();
  testSales1.Opportunity__c = testOppty1.Id;
  testSales1.EmpName__c = testColleague2.Id;
   //testSales1.Emp_Id__c = 123456789022';
  testSales1.Percent_Allocation__c = 20;
  testSales1.Role__c ='Sales Professional';
  insert testSales1;
  
  Sales_Credit__c testSales2 = new Sales_Credit__c();
  testSales2.Opportunity__c = testOppty1.Id;
  testSales2.EmpName__c = testColleague3.Id;
  testSales2.Percent_Allocation__c = 50;
  testSales2.Role__c ='EH&B Specialist';
  insert testSales2;
  //testSales2.EmpName__c = testColleague2.Id;
  update testSales2;
    
  //AP10_SalesCreditTriggerUtil.preventEditingSalesCredit()
  
  /*Sales_Credit__c testSales3 = new Sales_Credit__c();
  testSales3.Opportunity__c = testOppty1.Id;
  testSales3.EmpName__c = testColleague4.Id;
  testSales3.Percent_Allocation__c = 30;
  testSales3.Role__c ='Sales Professional';
  insert testSales3;*/
    
   User tu1 = [select Id, EmployeeNumber from User where EmployeeNumber = '123456789022' limit 1];
  
   Individual_Sales_Goal__c iSalesGoal2 = new Individual_Sales_Goal__c();
        iSalesGoal2.Sales_Goals__c = 500;
        iSalesGoal2.Colleague__c = testColleague2.id;
        iSalesGoal2.OwnerId = tu1.Id;
        
        Sales_Credit_Line_Item__c scli1 = new Sales_Credit_Line_Item__c();
       //Test.startTest();
        
        insert iSalesGoal2;      
        
        scli1.Individual_Sales_Goal__c = iSalesGoal2.Id;
        scli1.Sales_Credit_lookup__c = testSales1.Id;
        insert scli1;
        
        User tu2 = [select Id, EmployeeNumber from User where EmployeeNumber = '123456789024' limit 1];
        Individual_Sales_Goal__c iSalesGoal3 = new Individual_Sales_Goal__c();
        iSalesGoal3.Sales_Goals__c = 500;
        iSalesGoal3.Colleague__c = testColleague3.id;
        iSalesGoal3.OwnerId = tu2.Id;
        
        insert iSalesGoal3;
        
         
        
        Sales_Credit_Line_Item__c scli2 = new Sales_Credit_Line_Item__c();
        
        scli2.Individual_Sales_Goal__c = iSalesGoal3.Id;
        scli2.Sales_Credit_lookup__c = testSales2.Id;
        insert scli2;
        test.stoptest();
        /*User tu3 = [select Id, EmployeeNumber from User where EmployeeNumber = '123456789025' limit 1];
        Individual_Sales_Goal__c iSalesGoal4 = new Individual_Sales_Goal__c();
        iSalesGoal4.Sales_Goals__c = 500;
        iSalesGoal4.Colleague__c = testColleague3.id;
        iSalesGoal4.OwnerId = tu3.Id;
        insert iSalesGoal4 ;
        
        Sales_Credit_Line_Item__c scli3 = new Sales_Credit_Line_Item__c();
        
        scli3.Individual_Sales_Goal__c = iSalesGoal4.Id;
        scli3.Sales_Credit_lookup__c = testSales3.Id;
        insert scli3;*/
        
        
        
        
      /*  Map<Id,Sales_Credit__c> salesCredmap2= new Map<Id,Sales_Credit__c>();
        List<Sales_Credit__c> salesCred2 = [Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales1.Id Limit 1]; 
        salesCredmap2.put(testSales1.Id,salesCred2[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterInsert(salesCredmap2);
        
        
        Map<Id,Sales_Credit__c> salesCredmap3= new Map<Id,Sales_Credit__c>();
        List<Sales_Credit__c> salesCred3 = [Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales2.Id Limit 1]; 
        salesCredmap3.put(testSales2.Id,salesCred3[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterInsert(salesCredmap3);
         AP10_SalesCreditTriggerUtil.validateSalesCreditAfterUpdate(salesCredmap3,salesCredmap3);
        */
        /*Map<Id,Sales_Credit__c> salesCredmap4= new Map<Id,Sales_Credit__c>();
        List<Sales_Credit__c> salesCred4 = [Select id,Percent_Allocation__c, LOB__c,Employee_Office__c,Role__c,EmpName__c,Opportunity__c,Emp_Id__c from Sales_Credit__c where id =: testSales3.Id Limit 1]; 
        salesCredmap4.put(testSales3.Id,salesCred4[0]);
        AP10_SalesCreditTriggerUtil.validateSalesCreditAfterInsert(salesCredmap4);*/
        
}  

}