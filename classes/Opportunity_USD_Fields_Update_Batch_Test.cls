@isTest(seeAllData = true)
public class Opportunity_USD_Fields_Update_Batch_Test {

    public static testMethod void executeUSDUpdateBatch(){        
        Test.StartTest();
    
        Opportunity_USD_Fields_Update_Sched schedulable = new Opportunity_USD_Fields_Update_Sched();     
        
        String sch = '0 0 23 * * ?'; 
        
        system.schedule('Test Opportunity USD fields', sch, schedulable ); 
        
        Test.stopTest();       
    }   
}