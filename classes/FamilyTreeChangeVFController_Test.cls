@isTest
public class FamilyTreeChangeVFController_Test {

    static testMethod Void method1(){
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
   
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdfd.com',
                           Username = 'jason.test@test123.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
        
         Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            testAccount.My_Global_Special_Terms__c='test';
            insert testAccount;
        
       Account testAccount1 = new Account();        
            testAccount1.Name = 'TestAccountName';
            testAccount1.BillingCity = 'TestCity';
            testAccount1.BillingCountry = 'TestCountry';
            testAccount1.BillingStreet = 'Test Street';
            testAccount1.Relationship_Manager__c = testColleague.Id;
            testAccount1.OwnerId = testUser.Id;
            testAccount1.Account_Region__c='EuroPac';
            testAccount1.RecordTypeId='012E0000000QxxD';
            testAccount1.ParentId=testAccount.id;
            testAccount1.Previous_GU_DUNS__c = '345';
            testAccount1.Previous_GU_DUNS__c = '1234';
            testAccount1.GU_DUNS__c = '567';
            testAccount1.AGU_Flag__c = FALSE;
            insert testAccount1;                  
        
            ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount1);
            FamilyTreeChangeVFController cntrl=new FamilyTreeChangeVFController(stdController);
            cntrl.showFamilyTreeChangeMsg = 'Test Msg';
        
        
    }

}