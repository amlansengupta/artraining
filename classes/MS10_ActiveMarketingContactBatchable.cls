/*
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Arijit Roy    04/01/2013         Batch class for updating Account Status    
============================================================================================================================================== 
*/
global class MS10_ActiveMarketingContactBatchable implements Database.Batchable<sObject>, Database.Stateful{
    
    
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
        'Recent Win'                  => 'Recent Win',
        'High Revenue'                => 'High Revenue',
        'Low Revenue'                 => 'Low Revenue'  
    };
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global static String query = 'Select Id,Accountid,Account.MercerForce_Account_Status__c, FirstName, LastName, (Select Id, ContactId, Campaign.Status FROM CampaignMembers Where Status =' + '\'' + 'Completed' + '\'' + ' OR Status = ' + '\'' + 'Cancelled' + '\'' + '), (Select Id FROM Contact_Distributions__r) FROM Contact Where Contact_Status__c <> \'Inactive\' and Contact_Status__c <> \'Deceased\' and Account.MercerForce_Account_Status__c NOT IN' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()) + ' ORDER BY AccountId';
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
        if(Test.isRunningTest())
        {
            String ContactID = '896754231' + String.valueOf(Date.Today());  
            
            query = 'Select Id,AccountId, FirstName,Account.MercerForce_Account_Status__c, LastName, (Select Id, ContactId, Campaign.Status FROM CampaignMembers), (Select Id FROM Contact_Distributions__r) FROM Contact Where Account.MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()); 
        }
        System.debug('\n Constructed query String :'+query); 
        return Database.getQueryLocator(query); 
    }
    
     /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<String, Account> accountMapForUpdate = new Map<String, Account>();        
        for(Contact contact : (List<Contact>)scope)
        {
            for(CampaignMember cMember : contact.CampaignMembers)
            {
                if(cMember.Campaign.Status <> null)
                {
                    if(!accountMapForUpdate.containsKey(contact.AccountId) && contact.account.MercerForce_Account_Status__c <> 'Active Marketing Contact' )
                    {
                        Account account = new Account(Id = contact.AccountId);
                        account.MercerForce_Account_Status__c = 'Active Marketing Contact';
                        accountMapForUpdate.put(account.Id, account);   
                    }
                    break;  
                    
                }
                    
            }
            
            for(Contact_Distributions__c conDistribution : contact.Contact_Distributions__r)
            {
                if(!accountMapForUpdate.containsKey(contact.AccountId) && contact.account.MercerForce_Account_Status__c <> 'Active Marketing Contact')
                {
                    Account account = new Account(Id = contact.AccountId);
                    account.MercerForce_Account_Status__c = 'Active Marketing Contact';
                    accountMapForUpdate.put(account.Id, account);
                    break;
                }   
            }
            
            if(!accountMapForUpdate.containsKey(contact.AccountId) && contact.account.MercerForce_Account_Status__c <> 'Active Contact')
            {
                Account account = new Account(Id = contact.AccountId);
                account.MercerForce_Account_Status__c = 'Active Contact';
                accountMapForUpdate.put(account.Id, account);
            }

        }
        
        if(!accountMapForUpdate.isEmpty()) 
        {
        for(Database.Saveresult result :Database.update(accountMapForUpdate.values(), false))
        {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }
            }
        
        }
    }
    
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
       try
        {
          Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
          system.debug('\n exception has occured');
        }finally
        {
          // TO DO: Error logging code logic to be added
        MercerAccountStatusBatchHelper.callNextBatch('Has Activities');   
        } 
        
    }
    
}