/*Purpose: Test class to provide test coverage for AP89_CreateScopeItTemplate_Modeling class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Venu   02/20/2014  Created test class 
 
============================================================================================================================================== 
*/
@isTest(seeAlldata=false)
Private class AP89_CreateScopeItTemplate_Modeling_test{
    private Static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtSdata = new Mercer_ScopeItTestData();
    
    /*
     * @Description : Test method for "Save as Template" in Scope Modeling.
     * @ Args       : Null
     * @ Return     : void
     */    
     static testMethod void CreateScopeItTemplate_Modeling(){
         
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        Pricebook2 pb = new PriceBook2();
        pb.Id = Test.getStandardPricebookId();
        update pb;
        
        
            list<id> IdList = new  list<id>();
           String Lob='Benefits Admin';
        Product2 testProduct =mtdata.buildScopeProduct(Lob) ;
        String prodId=testProduct.id;
        Scope_Modeling__c testSProject = mtSdata.buildScopeModelProject(prodId);
        IdList.add(testSProject.id);
        mtSdata.buildScopeModelTask();
      String empBillrate= Mercer_ScopeItTestData.getEmployeeBillRate();
      mtSdata.buildScopeModelEmployee(empBillrate);
       
       test.startTest();
       system.runAs(User1){
       AP89_CreateScopeItTemplate_Modeling.createTscope(IdList);
       }
       test.stopTest(); 
   }

}