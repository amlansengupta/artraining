/*Purpose: This Apex class implements schedulable interface to create Tasks for Talent Contract object.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   10/10/2014  Created class
============================================================================================================================================== 
*/


global class AP112_CreateTalentTaskSchedulable implements Schedulable{

     
    global void execute(SchedulableContext sc)
    {
        Date after60 = System.today()+60;
        system.debug('Date after 60 days...'+after60);
        List<Talent_Contract__c> lstTc = [select id,product__c,Account__c,License_Expiration_Date__c 
                                                    
                                                    from Talent_Contract__c
                                                    where 
                                                    License_Expiration_Date__c = :after60 and 
                                                    Product__c<>'Global Membership'];

        if(lstTc.size()>0){
            List<AccountTeamMember> lstActm = new List<AccountTeamMember>();
            Set<id> accidset = new set<id>();                                                    
            List<Task> lstTask = new list<Task>();
            Map<String,String> acTeamMap = new Map<String,String>();
            
            for(Talent_Contract__c tc: lstTc){
               accidset.add(tc.account__c);
            } 
            
            lstActm = [select id,userid,Accountid,teammemberrole from AccountTeamMember where AccountId in :accidset order by teammemberrole desc];
            
            for(AccountTeamMember atm:lstActm){
                if(atm.teammemberrole == 'Talent Solutions Strategic Account Mgr')
                    acTeamMap.put(atm.accountid,atm.userid);
                else if(!acTeamMap.containsKey(atm.accountid) && atm.teammemberrole == 'Talent Solutions Account Coordinator') 
                    acTeamMap.put(atm.accountid,atm.userid);                 
            }
            
            for(Talent_Contract__c tc: lstTc){
                if(acTeamMap.size()>0 && acTeamMap.containsKey(tc.Account__c)){
                    Task t =  new Task();
                    t.whatid = tc.Id;
                    t.activitydate =  tc.License_Expiration_Date__c;
                    t.ownerid = acTeamMap.get(tc.Account__c);
                    t.subject = 'Renewal - '+tc.Product__c;
                    t.IsReminderSet = true;
                    t.ReminderDateTime = tc.License_Expiration_Date__c-30;
                    lstTask.add(t);
                }
            }
            
            if(lstTask.size()>0)
                database.insert(lstTask);                                          
            
        }
     }
 
 }