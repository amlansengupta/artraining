public class ProductExpansionCreate{
    
    
    Id oppid{set;get;}
    public ProductExpansionCreate() {
        
        oppid = ApexPages.currentPage().getParameters().get('id');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'One or more products which require a project on the opportunity you are expanding are not linked. Please ensure these products are linked.'));        
        
    }
    public pagereference createExpansion(){
        PageReference pageRef = new PageReference('/apex/Mercer_OpportunityClonewithProdExpansion');
        pageRef.getParameters().put('isexpansion', 'True');        
        pageRef.getParameters().put('id', oppid);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public pagereference cancel(){
        PageReference pageRef = new PageReference('/'+oppid);
        pageRef.setRedirect(true);
        return pageRef;
    }
}