/*Purpose: This test class provides daat coverage to AP60_OpportunityLineItemTriggerUtil class 
==============================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR             DATE        DETAIL 
1.0 -    Shashank           06/03/2013  Created test class

============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP60_OpportunityLineItemTriggerUtil {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    /*
* @Description : Test method to provide data coverage to AP60_OpportunityLineItemTriggerUtil class
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void myUnitTest() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        
        system.runAs(User1){ 
            Mercer_TestData mdata = new Mercer_TestData();
            Account acc = mdata.buildAccount();
            acc.one_code__c ='123';
            acc.One_Code_Status__c ='Active';
            Update acc;  
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc12@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = acc.Id;
            insert testContact;
            
            Test.startTest();
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acc.Id;
            //Request Id 12638 Commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'USD';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            testOppty.Amount = 100;
            testOppty.Buyer__c= testContact.id;  
            testOppty.CurrencyIsoCode='USD';
            testOppty.Concatenated_Products__c='529 Plan;DB Risk';
            insert testOppty;
            
            Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            
            Product2 pro = new Product2();
            pro.Name = 'TestPro';
            pro.Family = 'RRF';
            pro.IsActive = True;
            insert pro;
            
            PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'USD' limit 1];
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptyLineItem.Revenue_End_Date__c = date.Today()+30;
            opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
            opptylineItem.UnitPrice = 1.00;
            opptylineItem.CurrentYearRevenue_edit__c = 1.00;
            insert opptylineItem;
            AP60_OpportunityLineItemTriggerUtil ap=new AP60_OpportunityLineItemTriggerUtil();
            opptylineItem.UnitPrice = 2.00;
            
            Test.stoptest();
            
        }
        
    }
    
    /*
* @Description : Test method to provide data coverage to AP60_OpportunityLineItemTriggerUtil class
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void myUnitTest_UpdateTest() {      
        
        
        List<Opportunity> testOppList = new List<Opportunity>(); 
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            
            Mercer_TestData mdata = new Mercer_TestData();
            Account acc = mdata.buildAccount();
            acc.one_code__c ='123';
            acc.One_Code_Status__c ='Active';
            Update acc;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc13@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = acc.Id;
            insert testContact;
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acc.Id;
            //Request Id 12638 Commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            testOppty.Amount = 100;
            testOppty.Buyer__c= testContact.id;  
            testOppty.CurrencyIsoCode='USD';
            testOppty.Concatenated_Products__c='529 Plan;DB Risk';
            Test.startTest();
            insert testOppty;
            testOppList.add(testOppty);
            Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            
            Product2 pro = new Product2();
            pro.Name = 'TestPro';
            pro.Family = 'RRF';
            pro.IsActive = True;
            insert pro;
            
            PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'USD' limit 1];
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptyLineItem.Revenue_End_Date__c = date.Today()+1;
            opptyLineItem.Revenue_Start_Date__c = date.Today()+1; 
            opptylineItem.UnitPrice = 1.00;
            opptylineItem.CurrentYearRevenue_edit__c = 1.00;
            
            insert opptylineItem;
            
            opptylineItem.UnitPrice = 2.00;
            opptylineItem.CurrentYearRevenue_edit__c = 2.00;
            
            //    Test.startTest();
            
            update opptylineItem;
            List<OpportunityLineItem> oliL = new List<OpportunityLineItem>();
            oliL.add(opptylineItem);
            Test.stopTest();
            AP60_OpportunityLineItemTriggerUtil.chatterpost(oliL);
            AP02_OpportunityTriggerUtil.deleteOppSummary(testOppList);
            //   Test.stoptest();
        }
        
    }
    
    /*
* @Description : Test method to provide test coverage for OLI After trigger(Requirement#412,2806)
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void testOLIAfter() 
    {
        
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'Test Colleague';
        testColleague.EMPLID__c = '12345678024';       
        testColleague.LOB__c = '111214';       
        testColleague.Last_Name__c = 'test LastName';      
        testColleague.Empl_Status__c = 'Active';      
        testColleague.Email_Address__c = 'test@test.com';  
        
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'Test Account';
        testAccount.Relationship_Manager__c = testColleague.Id;  
        testAccount.BillingCity = 'TestCity';   
        testAccount.BillingCountry = 'TestCountry'; 
        testAccount.BillingStreet = 'Test Street'; 
        testAccount.One_Code__c = 'ABC123';
        
        insert testAccount;
        
        
        Test.startTest(); 
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            // test.startTest();
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc14@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            
            insert testContact;    
            
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id 12638 Commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'USD';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            testOppty.Amount = 100;
            testOppty.Buyer__c= testContact.id;  
            testOppty.CurrencyIsoCode='USD';
            testOppty.Concatenated_Products__c='529 Plan;DB Risk';
            
            
            
            insert testOppty;
            
            
            Product2 testproduct = new Product2();
            testproduct.Name = 'Test product';
            testproduct.Family = 'H&B';
            testproduct.CurrencyIsoCode = 'USD' ;
            testproduct.LOB__c= 'Talent' ;
            testproduct.IsActive = True;
            
            
            insert testProduct;
            
            Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
            
            
            PricebookEntry testPricebookEntry1= [SELECT Id,Pricebook2Id,Product2Id,CurrencyISOCode FROM PricebookEntry where Pricebook2.isStandard = true and Product2Id=:testproduct.Id  and CurrencyISOCode='USD' ];
            
            
            OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
            testOpportunityLineItem.UnitPrice = 4;
            testOpportunityLineItem.CurrentYearRevenue_edit__c = 4.00;
            testOpportunityLineItem.Duration__c = 'Multi-Year Open';
            testOpportunityLineItem.Revenue_Start_Date__c = Date.Today();
            testOpportunityLineItem.Revenue_End_Date__c = Date.Today() + 2;
            testOpportunityLineItem.OpportunityId = testOppty.Id;
            testOpportunityLineItem.PricebookEntryId = testPricebookEntry1.Id;
            testOpportunityLineItem.DS_Sales_Leader__c = testcolleague.id;
            Test.stopTest();
            insert testOpportunityLineItem;
            
            
            
            
        }
        
        
    }
    /*
* @Description : Test method to provide test coverage for OLI After trigger(Requirement#412,2806)
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void testOLIAfter_DeleteTest() 
    {
        try
        { 
            
            
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'Test Colleague';
            testColleague.EMPLID__c = '12345678024';       
            testColleague.LOB__c = '111214';       
            testColleague.Last_Name__c = 'test LastName';      
            testColleague.Empl_Status__c = 'Active';      
            testColleague.Email_Address__c = 'test@test.com';  
            
            insert testColleague;
            
            
            Account testAccount = new Account();
            testAccount.Name = 'Test Account';
            testAccount.Relationship_Manager__c = testColleague.Id;  
            testAccount.BillingCity = 'TestCity';   
            testAccount.BillingCountry = 'TestCountry'; 
            testAccount.BillingStreet = 'Test Street'; 
            testAccount.one_code__C='TEST';
            
            insert testAccount;
            
               Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc14@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            
            insert testContact;  
            
            Test.startTest();
            
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
            system.runAs(User1){
                Opportunity testOpportunity = new Opportunity();
                testOpportunity.Name = 'Sample Opportunity';
                //Request Id 12638 Commenting step__c
                //testOpportunity.Step__c = 'Closed / Won (SOW Signed)';
                testOpportunity.StageName = 'Closed / Won';
                testOpportunity.CloseDate = Date.Today();
                testOpportunity.AccountId = testAccount.ID;
                testOpportunity.CurrencyIsoCode = 'USD' ;
                testOpportunity.Type = 'New Client';
                testOpportunity.Product_LOBs__c= '';
                testOpportunity.Count_Product_LOBs__c= 0;
                testOpportunity.Opportunity_Office__c='Aarhus - Axel';
                testOpportunity.buyer__C=testContact.Id;
                
                insert testOpportunity;
                
                Product2 testproduct = new Product2();
                testproduct.Name = 'Test product';
                testproduct.Family = 'H&B';
                testproduct.CurrencyIsoCode = 'USD' ;
                testproduct.LOB__c= 'Talent' ;
                testproduct.IsActive = True;
                insert testProduct;
                
                Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
                
                
                PricebookEntry testPricebookEntry1= [SELECT Id,Pricebook2Id,Product2Id,CurrencyISOCode FROM PricebookEntry where Pricebook2.isStandard = true and Product2Id=:testproduct.Id  and CurrencyISOCode='USD' ];
                
                
                OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
                testOpportunityLineItem.UnitPrice = 4;
                testOpportunityLineItem.Duration__c = 'Multi-Year Open';
                testOpportunityLineItem.Revenue_Start_Date__c = Date.Today();
                testOpportunityLineItem.Revenue_End_Date__c = Date.Today() + 2;
                testOpportunityLineItem.OpportunityId = testOpportunity.Id;
                testOpportunityLineItem.PricebookEntryId = testPricebookEntry1.Id;
                
                insert testOpportunityLineItem;
                
                Opportunity op = [Select Id,Product_LOBs__c from Opportunity where Id= :testOpportunity.Id] ;        
                //System.assert(op.Product_LOBs__c == 'Talent');
                //test.startTest();
                //Delete testOpportunityLineItem
                delete testOpportunityLineItem;
                
                
                op = [Select Id,Product_LOBs__c from Opportunity where Id= :testOpportunity.Id] ;       
               // System.assert(op.Product_LOBs__c == 'Talent');
            } 
        }catch(Exception ex)
        {
            //system.assert(true,ex.getMessage().contains('Closed / Won opportunities must have at least one associated product. If you need to delete this opportunity, please contact your Market CRM Team'));                   
            //System.debug('Closed / Won opportunities must have at least one associated product. If you need to delete this opportunity, please contact your Market CRM Team'+Ex.getMessage());
            
        }
        
        
        test.stoptest();
    }
    
    /*
* @Description : Test method to provide test coverage for OLI After delete trigger
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void testOppProdAfter_Delete() 
    {
        Try
        {
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'Test Colleague';
            testColleague.EMPLID__c = '12345678024';       
            testColleague.LOB__c = '111214';       
            testColleague.Last_Name__c = 'test LastName';      
            testColleague.Empl_Status__c = 'Active';      
            testColleague.Email_Address__c = 'test@test.com';  
            
            insert testColleague;
            
            Account testAccount = new Account();
            testAccount.Name = 'Test Account';
            testAccount.One_Code__c = '1234';
            testAccount.Relationship_Manager__c = testColleague.Id;  
            testAccount.BillingCity = 'TestCity';   
            testAccount.BillingCountry = 'TestCountry'; 
            testAccount.BillingStreet = 'Test Street'; 
             insert testAccount;
            ConstantsUtility.STR_ACTIVE = 'Inactive';
             Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc14@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            
            insert testContact;  
            
           
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
            system.runAs(User1){
                Opportunity testOpportunity = new Opportunity();
                testOpportunity.Name = 'Sample Opportunity';
                //Request Id 12638 Commenting step__c
                //testOpportunity.Step__c = 'Closed / Won (SOW Signed)';
                testOpportunity.StageName = 'Closed / Won';
                testOpportunity.CloseDate = Date.Today();
                testOpportunity.AccountId = testAccount.ID;
                testOpportunity.CurrencyIsoCode = 'USD' ;
                testOpportunity.Type = 'New Client';
                testOpportunity.Product_LOBs__c= '';
                testOpportunity.Count_Product_LOBs__c= 0;
                testOpportunity.Opportunity_office__C='Aarhus - Axel';
                testOpportunity.buyer__c=testContact.Id;
                
                insert testOpportunity;
                
                Product2 testproduct = new Product2();
                testproduct.Name = 'Test product';
                testproduct.Family = 'H&B';
                testproduct.CurrencyIsoCode = 'USD' ;
                testproduct.LOB__c= 'Talent' ;
                testproduct.IsActive = True;
                insert testProduct;
                
                Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
                
                
                PricebookEntry testPricebookEntry1= [SELECT Id,Pricebook2Id,Product2Id,CurrencyISOCode FROM PricebookEntry where Pricebook2.isStandard = true and Product2Id=:testproduct.Id  and CurrencyISOCode='USD' ];
                
                
                OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
                testOpportunityLineItem.UnitPrice = 4;
                testOpportunityLineItem.Duration__c = 'Multi-Year Open';
                testOpportunityLineItem.Revenue_Start_Date__c = Date.Today();
                testOpportunityLineItem.Revenue_End_Date__c = Date.Today() + 2;
                testOpportunityLineItem.OpportunityId = testOpportunity.Id;
                testOpportunityLineItem.PricebookEntryId = testPricebookEntry1.Id;
                
                insert testOpportunityLineItem;
                
                test.startTest();
                delete testOpportunityLineItem;
                test.stoptest();
            }
        }catch(Exception ex)
        {
            system.assert(true,ex.getMessage().contains('Closed / Won opportunities must have at least one associated product. If you need to delete this opportunity, please contact your Market CRM Team'));        
            //System.debug('Closed / Won opportunities must have at least one associated product. If you need to delete this opportunity, please contact your Market CRM Team'+Ex.getMessage());
            
        }
        
        
    }
    
    
    /*
* @Description : Test method to provide test coverage for Scope it Delete functionality
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void myUnitTest_DeleteScopeit() {  
        
        
        
        User user1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        
        
        System.runAs(user1) {           
            
            String LOB = 'Retirement';
            String billType = 'Billable';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            Mercer_TestData mdata = new Mercer_TestData();
            Account acc = mdata.buildAccount();
            acc.one_code__c ='123';
            acc.One_Code_Status__c ='Active';
            Update acc; 
            //Opportunity testOppty = mtdata.buildOpportunity();
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty4';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acc.id;
            //Request Id 12638 Commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //testOppty.OwnerId = tUser.Id;
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'USD';
            testOppty.Amount = 100;
            testOppty.Opportunity_Office__c = 'Adelaide - Franklin';
            Test.startTest();   
            insert testOppty;  
            Test.stopTest();
            ID  opptyId  = testOppty.id;
            Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'USD' limit 1];
            ID  pbEntryId = pbEntry.Id;
            
            //OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptylineItem.Duration__c = 'One-Time Project';
            opptylineItem.Revenue_Start_Date__c = Date.Today();
            opptylineItem.Revenue_End_Date__c = Date.Today() ;
            opptyLineItem.UnitPrice = 1.00;
            opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
            opptyLineItem.Year2Revenue_edit__c = 0.00;
            opptyLineItem.Year3Revenue_edit__c = 0.00;

            insert opptylineItem;
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
            
            //     Test.startTest();                
            ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
            try { 
                delete opptylineItem;
            } catch (exception e) {
                system.assert(true,e.getMessage().contains('A Scopeit project is assosiated with this product. Please delete the project first (or) save as template and then delete the product.'));
            }
            
            
        }  
        
    }
   
    
    /*
* @Description : Test method to provide test coverage for DS_Sales_Leader insertion into sales credit team
* @ Args       : Null
* @ Return     : void
*/
    static testMethod void testDS_Sales_Leader() 
    {
        Try
        {
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'Test Colleague';
            testColleague.EMPLID__c = '12345678024';       
            testColleague.LOB__c = '111214';       
            testColleague.Last_Name__c = 'test LastName';      
            testColleague.Empl_Status__c = 'Active';      
            testColleague.Email_Address__c = 'test@test.com';  
            
            insert testColleague;
            
            Account testAccount = new Account();
            testAccount.Name = 'Test Account';
            testAccount.One_Code__c = '1234';
            testAccount.Relationship_Manager__c = testColleague.Id;  
            testAccount.BillingCity = 'TestCity';   
            testAccount.BillingCountry = 'TestCountry'; 
            testAccount.BillingStreet = 'Test Street'; 
            
            insert testAccount;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc12@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            ConstantsUtility.STR_ACTIVE = 'Inactive';
            insert testContact;
            
            Target_Margin__c testTarget = new Target_Margin__c ();
            testTarget.Budgeted_Net_BPs__c = 0.2;     
            testTarget.Market__c = 'TestMarket';
            insert testTarget;
            
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
            system.runAs(User1){
                //test.startTest();
                Opportunity testOpportunity = new Opportunity();
                testOpportunity.Name = 'Sample Opportunity';
                //Request Id 12638 Commenting step__c
                //testOpportunity.Step__c = 'Closed / Won (SOW Signed)';
                testOpportunity.StageName = 'Closed / Won';
                testOpportunity.CloseDate = Date.Today();
                testOpportunity.AccountId = testAccount.ID;
                testOpportunity.CurrencyIsoCode = 'USD' ;
                testOpportunity.Type = 'New Client';
                testOpportunity.Product_LOBs__c= '';
                testOpportunity.Count_Product_LOBs__c= 0;
                testOpportunity.Opportunity_Market__c = 'TestMarket';  
                testOpportunity.Opportunity_Office__c='Urbandale - Meredith';
                testOpportunity.buyer__c=testContact.Id;
                AP44_ChatterFeedReporting.FROMFEED=true;
                insert testOpportunity;
                // test.stopTest();
                Product2 testproduct = new Product2();
                testproduct.Name = 'Test product';
                testproduct.Family = 'H&B';
                testproduct.CurrencyIsoCode = 'USD' ;
                testproduct.LOB__c= 'Talent' ;
                testproduct.IsActive = True;
                insert testProduct;
                
                Pricebook2 standard = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
                
                
                PricebookEntry testPricebookEntry1= [SELECT Id,Pricebook2Id,Product2Id,CurrencyISOCode FROM PricebookEntry where Pricebook2.isStandard = true and Product2Id=:testproduct.Id  and CurrencyISOCode='USD' ];
                
                
                OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
                testOpportunityLineItem.UnitPrice = 4;
                testOpportunityLineItem.Duration__c = 'Multi-Year Open';
                testOpportunityLineItem.Revenue_Start_Date__c = Date.Today();
                testOpportunityLineItem.Revenue_End_Date__c = Date.Today() + 2;
                testOpportunityLineItem.OpportunityId = testOpportunity.Id;
                testOpportunityLineItem.PricebookEntryId = testPricebookEntry1.Id;
                testOpportunityLineItem.DS_Sales_Leader__c = testColleague.Id;
                List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
                lineItems.add(testOpportunityLineItem);
                insert lineItems;
                
                Sales_Credit__c testSales = new Sales_Credit__c();
                testSales.Opportunity__c = testOpportunity.Id;
                testSales.EmpName__c = testColleague.Id;
                testSales.Percent_Allocation__c = 100;
                try{
                    insert testSales;
                }catch(exception e){
                }
                
                
                test.startTest();
                
                AP60_OpportunityLineItemTriggerUtil.processOliAfterTrigger(lineItems, false);          
                delete testOpportunityLineItem;
                test.stoptest();
            }
        }catch(Exception ex)
        {
            
            //System.debug('Closed / Won opportunities must have at least one associated product. If you need to delete this opportunity, please contact your Market CRM Team'+Ex.getMessage());
            
        }
        
        
    }
    
    
}