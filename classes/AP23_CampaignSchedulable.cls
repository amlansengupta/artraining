/*
Purpose: This Apex class implements Schedulable interface to schedule AP23_CampaignSchedulable
==============================================================================================================================================
History ----------------------- VERSION     AUTHOR  DATE        DETAIL    1.0 -    Arijit Roy 03/29/2013  Created Apex Schedulable class
============================================================================================================================================== 
*/
global class AP23_CampaignSchedulable implements schedulable
{
    global final String query = 'Select Id, EndDate, Status from Campaign where EndDate < Today AND IsActive = True AND Status<>\'Completed\'';
    
    global AP23_CampaignSchedulable(){}
    
    global void execute(SchedulableContext SC) 
    {       
       database.executeBatch(new AP24_CampaignBatchable(query), 500); 
    }

    
}