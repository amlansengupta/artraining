/* Copyright ©2016-2017 7Summits Inc. All rights reserved. */

/*
    Name : SVNSUMMITS_TestUtil
    Description: Utility class for creating the test records.
*/
@isTest
global with sharing class SVNSUMMITS_TestUtil {

    /**
    date        5/17/2016
    description: create list of test users
    */

    // MyId
    static final global Id userId = UserInfo.getUserId();
    // Profiles
    static final global String COMPANY_COMMUNITY_PROFILE_NAME = 'Customer Community Plus User';
    static global Id COMPANY_COMMUNITY_PROFILE_Id {
        get {
            if (COMPANY_COMMUNITY_PROFILE_Id == null) {

                List<Profile> profiles = [select Id from Profile where Name = :COMPANY_COMMUNITY_PROFILE_NAME];
                COMPANY_COMMUNITY_PROFILE_Id = profiles[0].id;
            }
            return COMPANY_COMMUNITY_PROFILE_Id;
        }
        set;
    }

    static global String THIS_COMMUNITY_NAME {
        get {
            String commName = '';
            commName = [select Id, Name from Network][0].Name;
            return commName;
        }
    }

    // this is for standard user to know
    static global String NETWORK_ID {
        get {
            if (NETWORK_ID == null) {

                NETWORK_ID = [select Id from Network where Name = :THIS_COMMUNITY_NAME][0].Id;
            }
            return NETWORK_ID;

        }
        set;
    }


    global static List<User> createUsers(Integer howMany, String profileName) {

        Colleague__c colleague = new Colleague__c(Name='Colleague Test');
        insert colleague;

        Account a = new Account(name ='TestAccount123', Relationship_Manager__c=colleague.Id,one_Code__c='Test') ;
        insert a;

        List<Contact> listOfContacts = new List<Contact>();
        Map<Integer, Contact> mapCont = new Map<Integer, Contact>();

        for (Integer i = 0; i < howMany; i++) {
            Contact c = new Contact(LastName = i + 'Test', AccountId = a.Id, MailingCountry='US', Email='eventsCreateUsers' + i + '@eventsCreateUsers.com' );
            listOfContacts.add(c);
            mapCont.put(i, c);
        }
        insert listOfContacts;

        // by default bidders
        Id profileId;
        // to make user unique
        String type;
        if (profileName == COMPANY_COMMUNITY_PROFILE_NAME) {
            profileId = COMPANY_COMMUNITY_PROFILE_Id;
            type = 'com';
        }

        List<User> listOfUsers = new List<User>();
        Map<Integer, User> mapUser = new Map<Integer, User>();
        for (Integer key : mapCont.keySet()) {
            User u = new User(alias = type + key, email = key + 'testtest@test.com', communitynickname = key + mapCont.get(key).LastName,
                    emailencodingkey = 'UTF-8', firstname = key + 'Test', lastname = key + 'Test', languagelocalekey = 'en_US',
                    localesidkey = 'en_US', profileid = profileId, ContactId = mapCont.get(key).Id,
                    timezonesidkey = 'America/Los_Angeles', username = key + type + '@test.com', IsActive = true);

            mapUser.put(key, u);
            u.Empl_Status__c = 'Active';
            u.In_Member_Directory__c=true;
            listOfUsers.add(u);

        }

        insert listOfUsers;


        //To create the Members I follow
        List<EntitySubscription> subscriptions = new List<EntitySubscription>();

        for (Integer i = 1; i < howMany; i++) {

            EntitySubscription e = new EntitySubscription(PARENTID = listOfUsers[i].Id, SUBSCRIBERID = listOfUsers[0].Id, NetworkId = NETWORK_ID);
            subscriptions.add(e);

        }

        insert subscriptions;

        //To create the likes
        List<FeedItem> feedItems = new List<FeedItem>();
        Map<Integer, FeedItem> mapFeedItem = new Map<Integer, FeedItem>();
        for (Integer key : mapUser.keySet()) {

            FeedItem fi = new FeedItem(Body = 'Test' + mapUser.get(key).communitynickname, Type = 'ContentPost', ParentId = a.Id, Visibility = 'AllUsers');

            mapFeedItem.put(key, fi);
            feedItems.add(fi);

        }
        insert feedItems;


        List<FeedLike> feedLikes = new List<FeedLike>();
        for (Integer key : mapFeedItem.keySet()) {

            FeedLike fl = new FeedLike(FeedItemId = mapFeedItem.get(key).Id);

            feedLikes.add(fl);

        }
        insert feedLikes;

        return listOfUsers;
    }

}