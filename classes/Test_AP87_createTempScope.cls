/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================

. create all required data for testing 
.createTscope_test to test createTscope method of AP87_createTempScope class.

----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/24/2014  This class acts as Test class for AP87_createTempScope .
                                
 
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_AP87_createTempScope{
  private static Mercer_TestData mtdata = new Mercer_TestData();
  private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();   
  /*
     * @Description : Test method to provide data coverage to AP87_createTempScope class
     * @ Args       : Null
     * @ Return     : void
     */
  private static testMethod void createTscope_test(){
  User user1 = new User();
      
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
     
     System.runAs(user1) {
          String LOB = 'Retirement';
          String billType = 'Billable';
          Product2 testProduct = mtscopedata.buildProduct(LOB);
      ID prodId = testProduct.id;
         
         AP44_ChatterFeedReporting.FROMFEED=true;
         Opportunity testOppty = mtdata.buildOpportunity();
         //Test.stoptest();
         ID  opptyId  = testOppty.id;
          Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
       ID  pbEntryId = pbEntry.Id;
       
       
       OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
       
       ID oliId= opptylineItem.id;
       String oliCurr = opptylineItem.CurrencyIsoCode;
       Double oliUnitPrice = opptylineItem.unitprice;
       String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
       
         
       list<id> sList= new list<id>();
        Test.startTest();          
       ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
       Test.stopTest();
       sList.add(testScopeitProj.id);
       
       mtscopedata.buildScopeitTask();               
       mtscopedata.buildScopeitEmployee(employeeBillrate);
       AP87_createTempScope.createTscope(sList);
       
  }
  }
  
    
    
    
}