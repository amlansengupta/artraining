@isTest
public class Product2Update_BatchTest {
     static testMethod void myUnitTest3(){
         TriggerSettings__c TrgSetting= new TriggerSettings__c();
         TrgSetting.name='Project Exception Trigger';
         TrgSetting.Object_API_Name__c = 'Project_Exception__c' ; TrgSetting.Trigger_Disabled__c= false;
         insert TrgSetting;
         Project_Exception__c pe7=new Project_Exception__c();
        pe7.Exception_Type__c='Product + Office Code';
        pe7.Product_Code__c='0001';
        pe7.Office__c='Guangzhou - Tianhe';
        insert pe7;
         Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'TEST Bus';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode='5436';
        insert testProduct2;
        //delete pe7;
        Product2Update_Batch batch = new Product2Update_Batch();
            Database.executeBatch(batch);
    }

}