@IsTest
public class CGPSummary_test {
    @isTest static void Tab1Test() {
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()),12,31));
        insert py;
        System.debug('>>>py='+py);
        
        //creating Mercer_Office_Geo_D__c
        List<Mercer_Office_Geo_D__c> MercerOfficeList = new List<Mercer_Office_Geo_D__c>();
        for(Integer i=0;i<5;i++){
           Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
           if(i==0)
             mog.Region__c='International'; 
           if(i==1)
             mog.Region__c='Unspecified'; 
           if(i==2)
             mog.Region__c='North America'; 
            
            
            mog.Name = 'TestGopal';
            mog.Location_Id__c = '20873.000000000000000';
            mog.Mercer_Office__c = 'true';
            mog.Office__c = 'Dallas - Main';
            mog.Status__c = 'A';
            mog.Office_Code__c = 'DLS99' ;
            //mog.Region__c = 'North America';
            mog.Sub_Region__c = 'United States';
            MercerOfficeList.add(mog);
        }
        if(MercerOfficeList!=null && MercerOfficeList.size()>0)
            insert MercerOfficeList;
        
        system.debug('*********' + MercerOfficeList);
        //creating collegeus
        List<Colleague__c> collegeList = new List<Colleague__c>();
        Integer count=0;
        for(Mercer_Office_Geo_D__c mObj:MercerOfficeList){
            system.debug('*****' + mObj);
            Colleague__c testColleague = new Colleague__c();
            count++;
            testColleague.Name = 'TestColleague'+count;
            testColleague.LOB__c = '11111'+count;
            testColleague.Last_Name__c = 'TestLastName'+count;
            testColleague.Empl_Status__c = 'Active';
            testColleague.Level_1_Descr__c = 'Merc';
            testColleague.Email_Address__c = 'abc@abc.com';
            testColleague.EMPLID__c = '101'+count ;
            testColleague.Location__c = mObj.Id;
            collegeList.add(testColleague);
        }
        if(collegeList!=null && collegeList.size()>0)
            insert collegeList;
        
        System.debug('>>>collegeList='+collegeList);        
        
         //creating Accounts
        List<Account> acclist = new List<Account>();
        List<Id> accIdList = new List<Id>();
        for(Colleague__c colObj:collegeList){
            Account Acc = new Account();
            Acc.Name = 'Test Account'+count;
            Acc.BillingCountry='test'; 
            Acc.Country__c='United States - US';
            Acc.One_Code_Status__c = 'Active';
           
            Acc.Relationship_Manager__c = colObj.Id;
            Acc.GU_DUNS__c = '1234567';
            acclist.add(Acc);
            
        }
       
       if(acclist!=null && acclist.size()>0)
           insert acclist;
        
        for(Account acObj:acclist){
            acObj.One_Code_Status__c = 'Active';
            acObj.Account_Region__c = 'North America';
            accIdList.add(acObj.Id);
        }
        if(acclist!=null && acclist.size()>0)
            update acclist;
            
        System.debug('>>>acclist='+acclist);
        
        //creating growth plan 
        List<Growth_Plan__c> gpList = new List<Growth_Plan__c>();
        for(Account accObj:acclist){
            Growth_Plan__c gp = new Growth_Plan__c();
            gp.Account__c = accObj.id;
            gp.fcstPlanning_Year__c = py.id;
            gp.Status__c = 'Active';
            gp.Planning_Year__c='2019';
            gpList.add(gp);
        }
        if(gpList!=null && gpList.size()>0)
             insert gpList;
        
        System.debug('>>>gpList='+gpList);
        for(Growth_Plan__c gpobj:gpList){
          System.debug('>>>gpobj.Status__c='+gpobj.Status__c+'gpobj.Planning_Year__c=='+gpobj.Planning_Year__c+'gpobj.fcstPlanning_Year__c'+gpobj.fcstPlanning_Year__c);  
        }
        
        
       List<Forecast_Model__c> forecastModelList = new List<Forecast_Model__c>();
       forecastModelList =[select Sub_Business__c,LOB__c,Account__r.One_Code_Status__c,Id,Account__c,Growth_Plan__c,Account__r.Account_Region__c,Growth_Plan__r.Planning_Year__c,Growth_Plan__r.Status__c,Growth_Plan__r.fcstPlanning_Year__c from Forecast_Model__c where  Growth_Plan__c=:gpList[0].Id and  Sub_Business__c = ''];
        for(Forecast_Model__c fm:forecastModelList){
             System.debug(' one Growth_Plan__c>>>>'+fm.Growth_Plan__c+'Account__c>>>>'+fm.Account__c+'LOB__c>>>'+fm.LOB__c+'Sub_Business__c>>>'+fm.Sub_Business__c+'fMObj>> Account__r.One_Code_Status__c'+ fm.Account__r.One_Code_Status__c+'==>>fMObj.Account__r.Account_Region__c=='+fm.Account__r.Account_Region__c+'fMObj.Growth_Plan__r.Planning_Year__c>>>'+fm.Growth_Plan__r.Planning_Year__c+'>>Growth_Plan__r.Status__c=='+fm.Growth_Plan__r.Status__c+'Growth_Plan__r.fcstPlanning_Year__c=='+fm.Growth_Plan__r.fcstPlanning_Year__c);
        
            //System.debug('fMObj>> Account__r.One_Code_Status__c'+ fm.Account__r.One_Code_Status__c+'==>>fMObj.Account__r.Account_Region__c=='+fm.Account__r.Account_Region__c+'fMObj.Growth_Plan__r.Planning_Year__c>>>'+fm.Growth_Plan__r.Planning_Year__c+'>>Growth_Plan__r.Status__c=='+fm.Growth_Plan__r.Status__c+'Growth_Plan__r.fcstPlanning_Year__c=='+fm.Growth_Plan__r.fcstPlanning_Year__c);
        }
       List<Forecast_Model__c> fmlist = new List<Forecast_Model__c>();
        fmlist=[Select Account__c,Growth_Plan__c,LOB__c,Growth_Plan__r.Status__c,Growth_Plan__r.Planning_Year__c,Account__r.One_Code_Status__c,Account__r.Account_Region__c,Sub_Business__c,Growth_Plan__r.fcstPlanning_Year__c from Forecast_Model__c  where Sub_Business__c = '' and  Account__c IN:acclist];
        
        for(Forecast_Model__c fmobj:fmlist){
            //fmobj.Growth_Plan__r.Planning_Year__c='2018';
             System.debug('test==Growth_Plan__c>>>>'+fmobj.Growth_Plan__c+'Account__c>>>>'+fmobj.Account__c+'LOB__c>>>'+fmobj.LOB__c+'Sub_Business__c>>>'+fmobj.Sub_Business__c+'fMObj>> Account__r.One_Code_Status__c'+ fmobj.Account__r.One_Code_Status__c+'==>>fMObj.Account__r.Account_Region__c=='+fmobj.Account__r.Account_Region__c+'fMObj.Growth_Plan__r.Planning_Year__c>>>'+fmobj.Growth_Plan__r.Planning_Year__c+'>>Growth_Plan__r.Status__c=='+fmobj.Growth_Plan__r.Status__c+'Growth_Plan__r.fcstPlanning_Year__c=='+fmobj.Growth_Plan__r.fcstPlanning_Year__c);
         //  System.debug('test fmobj.Growth_Plan__r.Status__c>>'+fmobj.Growth_Plan__r.Status__c+'>>fmobj.Growth_Plan__r.Planning_Year__c>>'+'>>fmobj.Account__r.One_Code_Status__c>>>>'+fmobj.Account__r.One_Code_Status__c+'>>>>>Account__r.Account_Region__c=='+fmobj.Account__r.Account_Region__c+'>>>>>>fmobj.Sub_Business__c=='+fmobj.Sub_Business__c+'>>>fmobj.Growth_Plan__r.fcstPlanning_Year__c=='+fmobj.Growth_Plan__r.fcstPlanning_Year__c);
        } 
       // Update fmlist;
        CGPSummaryCtrl cgpCtrl = New CGPSummaryCtrl();
        cgpCtrl.Set_Zeros_RevbyRegion();
        CGPSummaryCtrl.allCGP alCGP =  New CGPSummaryCtrl.allCGP();
        CGPSummaryCtrl.allProjRev aprev= New CGPSummaryCtrl.allProjRev();
        
        AggregateResult[] ars = [Select Account__r.Account_Region__c Region, Account__r.Account_Country__c Country, LOB__c LOB, Account__r.One_Code__c oneCode,
                              SUM(USD_Prior_FY_Projected__c)PrjPrev, SUM(USD_Current_Carry_Forward_Revenue__c)PrjSales,
                              SUM(USD_Revenue_from_In_Year_Sales__c)PrjRecur,
                              Account__r.Relationship_Manager__r.Name RMName from Forecast_Model__c Group By Account__r.Account_Region__c, Account__r.Account_Country__c, LOB__C, Account__r.One_Code__c, Account__r.Relationship_Manager__r.Name ];
       
        System.debug('@@@ Test Ars:' + ars.size());
        if(ars!=null && ars.size()>0){   
             CGPSummaryCtrl.Summary smry =  New CGPSummaryCtrl.Summary(ars[0]);
             CGPSummaryCtrl.Summary_LOB sLob = New CGPSummaryCtrl.Summary_LOB(ars[0]);
             CGPSummaryCtrl.Summary_OneCode sobj = New CGPSummaryCtrl.Summary_OneCode(ars[0]);
       }
       Test.setCurrentPageReference(new PageReference('Page.CGPSummaryReport'));
         System.currentPageReference().getParameters().put('guName', acclist[0].Global_Ultimate_Name__c);
         System.currentPageReference().getParameters().put('guduns', acclist[0].GU_DUNS__c);
         System.currentPageReference().getParameters().put('onecode', acclist[0].One_code__c);
         System.currentPageReference().getParameters().put('planningyear', '2019');
       CGPSummaryCtrl.allCgp allcgoObj= new CGPSummaryCtrl.allCgp();
           
       Test.StartTest();
         cgpCtrl.str_newplanningyear='2019';
         cgpCtrl.Preplanningyear=2019;
         cgpCtrl.sumofactiveonecode=2018;
         cgpCtrl.sumofactiveonecodeCY=2018;        
         cgpCtrl.Display();
         cgpCtrl.Calc_Total_RevByRegion();
         cgpCtrl.projectedRevenueByLOB(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country (accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_LOB(accIdList);
         cgpCtrl.HEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.WEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.Career_ProjRevenue_Country(accIdList);
         cgpCtrl.GBS_ProjRevenue_Country(accIdList);
         cgpCtrl.Other_ProjRevenue_Country(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_OneCode(accIdList);
         cgpCtrl.projectedRevenueByRegion(accIdList);
        
     Test.StopTest(); 
    }
  
  
  @isTest static void Tab2Test() {
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()),12,31));
        insert py;
        System.debug('>>>py='+py);
        
        //creating Mercer_Office_Geo_D__c
        List<Mercer_Office_Geo_D__c> MercerOfficeList = new List<Mercer_Office_Geo_D__c>();
        for(Integer i=0;i<5;i++){
           Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
           if(i==0)
             mog.Region__c='North America'; 
           if(i==1)
             mog.Region__c='Unspecified'; 
           if(i==2)
             mog.Region__c='International'; 
            
            
            mog.Name = 'TestGopal';
            mog.Location_Id__c = '20873.000000000000000';
            mog.Mercer_Office__c = 'true';
            mog.Office__c = 'Dallas - Main';
            mog.Status__c = 'A';
            mog.Office_Code__c = 'DLS99' ;
            //mog.Region__c = 'North America';
            mog.Sub_Region__c = 'United States';
            MercerOfficeList.add(mog);
        }
        if(MercerOfficeList!=null && MercerOfficeList.size()>0)
            insert MercerOfficeList;
        
        system.debug('*********' + MercerOfficeList);
        //creating collegeus
        List<Colleague__c> collegeList = new List<Colleague__c>();
        Integer count=0;
        for(Mercer_Office_Geo_D__c mObj:MercerOfficeList){
            system.debug('*****' + mObj);
            Colleague__c testColleague = new Colleague__c();
            count++;
            testColleague.Name = 'TestColleague'+count;
            testColleague.LOB__c = '11111'+count;
            testColleague.Last_Name__c = 'TestLastName'+count;
            testColleague.Empl_Status__c = 'Active';
            testColleague.Level_1_Descr__c = 'Merc';
            testColleague.Email_Address__c = 'abc@abc.com';
            testColleague.EMPLID__c = '101'+count ;
            testColleague.Location__c = mObj.Id;
            collegeList.add(testColleague);
        }
        if(collegeList!=null && collegeList.size()>0)
            insert collegeList;
        
        System.debug('>>>collegeList='+collegeList);        
        
         //creating Accounts
        List<Account> acclist = new List<Account>();
        List<Id> accIdList = new List<Id>();
        for(Colleague__c colObj:collegeList){
            Account Acc = new Account();
            Acc.Name = 'Test Account'+count;
            Acc.BillingCountry='test'; 
            Acc.Country__c='United States - US';
            Acc.One_Code_Status__c = 'Active';
           
            Acc.Relationship_Manager__c = colObj.Id;
            Acc.GU_DUNS__c = '1234567';
            acclist.add(Acc);
            
        }
       
       if(acclist!=null && acclist.size()>0)
           insert acclist;
        
        for(Account acObj:acclist){
            acObj.One_Code_Status__c = 'Active';
            acObj.Account_Region__c = 'International';
            accIdList.add(acObj.Id);
        }
        if(acclist!=null && acclist.size()>0)
            update acclist;
            
        System.debug('>>>acclist='+acclist);
        
        //creating growth plan 
        List<Growth_Plan__c> gpList = new List<Growth_Plan__c>();
        for(Account accObj:acclist){
            Growth_Plan__c gp = new Growth_Plan__c();
            gp.Account__c = accObj.id;
            gp.fcstPlanning_Year__c = py.id;
            gp.Status__c = 'Active';
            gp.Planning_Year__c='2019';
            gpList.add(gp);
        }
        if(gpList!=null && gpList.size()>0)
             insert gpList;
        
        
       
        CGPSummaryCtrl cgpCtrl = New CGPSummaryCtrl();
        cgpCtrl.Set_Zeros_RevbyRegion();
        CGPSummaryCtrl.allCGP alCGP =  New CGPSummaryCtrl.allCGP();
        CGPSummaryCtrl.allProjRev aprev= New CGPSummaryCtrl.allProjRev();
        
        AggregateResult[] ars = [Select Account__r.Account_Region__c Region, Account__r.Account_Country__c Country, LOB__c LOB, Account__r.One_Code__c oneCode,
                              SUM(USD_Prior_FY_Projected__c)PrjPrev, SUM(USD_Current_Carry_Forward_Revenue__c)PrjSales,
                              SUM(USD_Revenue_from_In_Year_Sales__c)PrjRecur,
                              Account__r.Relationship_Manager__r.Name RMName from Forecast_Model__c Group By Account__r.Account_Region__c, Account__r.Account_Country__c, LOB__C, Account__r.One_Code__c, Account__r.Relationship_Manager__r.Name ];
       
        System.debug('@@@ Test Ars:' + ars.size());
        if(ars!=null && ars.size()>0){   
             CGPSummaryCtrl.Summary smry =  New CGPSummaryCtrl.Summary(ars[0]);
             CGPSummaryCtrl.Summary_LOB sLob = New CGPSummaryCtrl.Summary_LOB(ars[0]);
             CGPSummaryCtrl.Summary_OneCode sobj = New CGPSummaryCtrl.Summary_OneCode(ars[0]);
       }
       Test.setCurrentPageReference(new PageReference('Page.CGPSummaryReport'));
         System.currentPageReference().getParameters().put('guName', acclist[0].Global_Ultimate_Name__c);
         System.currentPageReference().getParameters().put('guduns', acclist[0].GU_DUNS__c);
         System.currentPageReference().getParameters().put('onecode', acclist[0].One_code__c);
         System.currentPageReference().getParameters().put('planningyear', '2019');
       CGPSummaryCtrl.allCgp allcgoObj= new CGPSummaryCtrl.allCgp();
           
       Test.StartTest();
         cgpCtrl.str_newplanningyear='2019';
         cgpCtrl.Preplanningyear=2019;
         cgpCtrl.sumofactiveonecode=2018;
         cgpCtrl.sumofactiveonecodeCY=2018;        
         cgpCtrl.Display();
         cgpCtrl.Calc_Total_RevByRegion();
         cgpCtrl.projectedRevenueByLOB(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country (accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_LOB(accIdList);
         cgpCtrl.HEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.WEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.Career_ProjRevenue_Country(accIdList);
         cgpCtrl.GBS_ProjRevenue_Country(accIdList);
         cgpCtrl.Other_ProjRevenue_Country(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_OneCode(accIdList);
         cgpCtrl.projectedRevenueByRegion(accIdList);
        
     Test.StopTest();
    }
    
    @isTest static void Tab4Test() {
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()),12,31));
        insert py;
        System.debug('>>>py='+py);
        
        //creating Mercer_Office_Geo_D__c
        List<Mercer_Office_Geo_D__c> MercerOfficeList = new List<Mercer_Office_Geo_D__c>();
        for(Integer i=0;i<5;i++){
           Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
           if(i==0)
             mog.Region__c='North America'; 
           if(i==1)
             mog.Region__c='Unspecified'; 
           if(i==2)
             mog.Region__c='International'; 
            
            
            mog.Name = 'TestGopal';
            mog.Location_Id__c = '20873.000000000000000';
            mog.Mercer_Office__c = 'true';
            mog.Office__c = 'Dallas - Main';
            mog.Status__c = 'A';
            mog.Office_Code__c = 'DLS99' ;
            //mog.Region__c = 'North America';
            mog.Sub_Region__c = 'United States';
            MercerOfficeList.add(mog);
        }
        if(MercerOfficeList!=null && MercerOfficeList.size()>0)
            insert MercerOfficeList;
        
        system.debug('*********' + MercerOfficeList);
        //creating collegeus
        List<Colleague__c> collegeList = new List<Colleague__c>();
        Integer count=0;
        for(Mercer_Office_Geo_D__c mObj:MercerOfficeList){
            system.debug('*****' + mObj);
            Colleague__c testColleague = new Colleague__c();
            count++;
            testColleague.Name = 'TestColleague'+count;
            testColleague.LOB__c = '11111'+count;
            testColleague.Last_Name__c = 'TestLastName'+count;
            testColleague.Empl_Status__c = 'Active';
            testColleague.Level_1_Descr__c = 'Merc';
            testColleague.Email_Address__c = 'abc@abc.com';
            testColleague.EMPLID__c = '101'+count ;
            testColleague.Location__c = mObj.Id;
            collegeList.add(testColleague);
        }
        if(collegeList!=null && collegeList.size()>0)
            insert collegeList;
        
        System.debug('>>>collegeList='+collegeList);        
        
         //creating Accounts
        List<Account> acclist = new List<Account>();
        List<Id> accIdList = new List<Id>();
        for(Colleague__c colObj:collegeList){
            Account Acc = new Account();
            Acc.Name = 'Test Account'+count;
            Acc.BillingCountry='test'; 
            Acc.Country__c='United States - US';
            Acc.One_Code_Status__c = 'Active';
           
            Acc.Relationship_Manager__c = colObj.Id;
            Acc.GU_DUNS__c = '1234567';
            acclist.add(Acc);
            
        }
       
       if(acclist!=null && acclist.size()>0)
           insert acclist;
        
        for(Account acObj:acclist){
            acObj.One_Code_Status__c = 'Active';
            acObj.Account_Region__c = 'Unspecified';
            accIdList.add(acObj.Id);
        }
        if(acclist!=null && acclist.size()>0)
            update acclist;
            
        System.debug('>>>acclist='+acclist);
        
        //creating growth plan 
        List<Growth_Plan__c> gpList = new List<Growth_Plan__c>();
        for(Account accObj:acclist){
            Growth_Plan__c gp = new Growth_Plan__c();
            gp.Account__c = accObj.id;
            gp.fcstPlanning_Year__c = py.id;
            gp.Status__c = 'Active';
            gp.Planning_Year__c='2019';
            gpList.add(gp);
        }
        if(gpList!=null && gpList.size()>0)
             insert gpList;
        
        
       
        CGPSummaryCtrl cgpCtrl = New CGPSummaryCtrl();
        cgpCtrl.Set_Zeros_RevbyRegion();
        CGPSummaryCtrl.allCGP alCGP =  New CGPSummaryCtrl.allCGP();
        CGPSummaryCtrl.allProjRev aprev= New CGPSummaryCtrl.allProjRev();
        
        AggregateResult[] ars = [Select Account__r.Account_Region__c Region, Account__r.Account_Country__c Country, LOB__c LOB, Account__r.One_Code__c oneCode,
                              SUM(USD_Prior_FY_Projected__c)PrjPrev, SUM(USD_Current_Carry_Forward_Revenue__c)PrjSales,
                              SUM(USD_Revenue_from_In_Year_Sales__c)PrjRecur,
                              Account__r.Relationship_Manager__r.Name RMName from Forecast_Model__c Group By Account__r.Account_Region__c, Account__r.Account_Country__c, LOB__C, Account__r.One_Code__c, Account__r.Relationship_Manager__r.Name ];
       
        System.debug('@@@ Test Ars:' + ars.size());
        if(ars!=null && ars.size()>0){   
             CGPSummaryCtrl.Summary smry =  New CGPSummaryCtrl.Summary(ars[0]);
             CGPSummaryCtrl.Summary_LOB sLob = New CGPSummaryCtrl.Summary_LOB(ars[0]);
             CGPSummaryCtrl.Summary_OneCode sobj = New CGPSummaryCtrl.Summary_OneCode(ars[0]);
       }
       Test.setCurrentPageReference(new PageReference('Page.CGPSummaryReport'));
         System.currentPageReference().getParameters().put('guName', acclist[0].Global_Ultimate_Name__c);
         System.currentPageReference().getParameters().put('guduns', acclist[0].GU_DUNS__c);
         System.currentPageReference().getParameters().put('onecode', acclist[0].One_code__c);
         System.currentPageReference().getParameters().put('planningyear', '2019');
       CGPSummaryCtrl.allCgp allcgoObj= new CGPSummaryCtrl.allCgp();
           
       Test.StartTest();
         cgpCtrl.str_newplanningyear='2019';
         cgpCtrl.Preplanningyear=2019;
         cgpCtrl.sumofactiveonecode=2018;
         cgpCtrl.sumofactiveonecodeCY=2018;        
         cgpCtrl.Display();
         cgpCtrl.Calc_Total_RevByRegion();
         cgpCtrl.projectedRevenueByLOB(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country (accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_LOB(accIdList);
         cgpCtrl.HEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.WEALTH_ProjRevenue_Country(accIdList);
         cgpCtrl.Career_ProjRevenue_Country(accIdList);
         cgpCtrl.GBS_ProjRevenue_Country(accIdList);
         cgpCtrl.Other_ProjRevenue_Country(accIdList);
         cgpCtrl.ProjectedRevenueBy_Region_Country_OneCode(accIdList);
         cgpCtrl.projectedRevenueByRegion(accIdList);
        
     Test.StopTest();
    }
    
     @isTest static void Tab3Test() {
        //Create FCST Fiscal Year List
        FCST_Fiscal_Year_List__c yList_2018= New FCST_Fiscal_Year_List__c();
        yList_2018.Name=string.valueof(Date.Today().Year()+1);
        yList_2018.StartDate__c = date.parse('1/1/2017');
        yList_2018.EndDate__c= date.parse('12/31/2017');
        Insert yList_2018;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '101' ;
        //testColleague.Location__c = mog.Id;
        insert testColleague;

        // First Account 
        List<Account> accList = new List<Account>();
        //for(Integer i=0;i<10;i++){
            Account eu_acc = New Account();
            eu_acc.Name='Pfizer Inc1';
            eu_acc.Global_Ultimate_Name__c = 'Pfizer Ltd1';
            eu_acc.Account_Region__c='International';
            eu_acc.One_Code__c = '1A2B3C';
            eu_acc.One_Code_Status__c = 'Active';
            eu_acc.GU_DUNS__c = '1234567';
            eu_acc.Total_PY_Revenue__c = 234345.34;
            eu_acc.Total_CY_Revenue__c = 1234567;
            eu_acc.Relationship_Manager__c = testColleague.Id;
            eu_acc.BillingCountry='test';
            eu_acc.Country__c='United States - US';
          //  accList.add(eu_acc);
      //  }
       // if(accList!=null && accList.size()>0)
            Insert eu_acc;
        
        
        Test.setCurrentPageReference(new PageReference('Page.CGPSummaryReport'));
         System.currentPageReference().getParameters().put('guName', eu_acc.Global_Ultimate_Name__c);
         System.currentPageReference().getParameters().put('guduns', eu_acc.GU_DUNS__c);
         System.currentPageReference().getParameters().put('onecode', eu_acc.One_code__c);
         System.currentPageReference().getParameters().put('planningyear',string.valueof(Date.Today().Year()+1));
        
        System.debug('>>>>>eu_acc.....'+eu_acc);
        
        CGPSummaryCtrl testObj = new CGPSummaryCtrl();
        testObj.Display();
     }
}