/*Purpose: Test Class for providing code coverage to TRG11_CompetitorTrigger class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Suchi   02/20/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          17-Jan-2019  12638-Commenting Oppotunity Step
Trisha          22-Jan-2019  17601-Replacing stage Value 'Pending Step' with 'Identify'
=======================================================
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
public class Test_TRG11_CompetitorTrigger
{

/*Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
        }*/

    /*
     * @Description : Test method to provide data coverage to TRG11_CompetitorTrigger class
     * @ Args       : Null
     * @ Return     : void
     */
     static testMethod void myPositiveTest()
     {
     
     //createCS();
         Colleague__c testColleague = new Colleague__c();
         testColleague.Name = 'TestColleague';
         testColleague.EMPLID__c = '12345678901';
         testColleague.LOB__c = '11111';
         testColleague.Empl_Status__c = 'Active';
         insert testColleague;
            
         Account testAccount = new Account();
         testAccount.Name = 'TestAccountName';
         testAccount.BillingCity = 'TestCity';
         testAccount.BillingCountry = 'TestCountry';
         testAccount.BillingStreet = 'Test Street';
         testAccount.Relationship_Manager__c = testColleague.Id;
         testAccount.Competitor_Flag__c = True;
         testAccount.One_Code__c = '1234';
         insert testAccount;
         
         Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)
        //Request Id:12638 Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601;Replacing stage value 'Pending Step with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Noida - Sector 135';
        test.startTest();
        insert testOppty;

         
        //Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Pricebook2 pb = new Pricebook2();
        pb.name = 'pbtest';
        insert pb;
               
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        //pro.Classification__c = 'Consulting Solution Area';
        insert pro;
        
        //PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
         PricebookEntry pbEntry = new PricebookEntry();
       pbEntry.Product2Id = pro.Id;
       pbEntry.UnitPrice = 220.30;
       pbEntry.Pricebook2Id = pb.Id;
       pbEntry.CurrencyIsoCode = 'ALL';
       pbEntry.IsActive = true;
       insert pbEntry;
        test.stopTest();
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+70;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
       //opptyLineItem.CurrentYearRevenue_edit__c = 100.00;
        //opptyLineItem.Year2Revenue_edit__c = 50.00;
        insert opptylineItem;
        testOppty.Name = 'TestOppty';
         testOppty.Type = 'New Client';
         testOppty.AccountId = testAccount.Id;
         //Request Id:12638 Commenting step__c
         //testOppty.Step__c = 'Identified Single Sales Objective(s)';
         testOppty.Close_Stage_Reason__c = 'Other';
         testOppty.StageName = 'Identify';
         testOppty.CloseDate = date.Today();
         testOppty.CurrencyIsoCode = 'ALL';
         testOppty.Opportunity_Office__c='Montreal - McGill';
         
         
         //Updated as part of 5166(july 2015)
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
         /******Request Id:12638 Commenting step__c;adding stageName = 'Closed / Won' START*********/
        //testOppty.Step__c = 'Closed / Won (SOW Signed)';
          testOppty.stageName = 'Closed / Won';
          /******Request Id:12638 Commenting step__c;adding stageName = 'Closed / Won' END*********/
         //testOppty.Close_Stage_Reason__c = 'Other';
         //testOppty.StageName = 'Pending Step';
         try{ update testOppty;}
         catch(Exception ex) {
         }
         //testOppty.Step__c = 'Closed / Won (SOW Signed)';
         //update testOppty;
         
         Competitor__c testComp = new Competitor__c();
         testComp.Competitor__c = testAccount.Id;
         testComp.Opportunity__c = testOppty.Id;
         insert testComp;
         list<Competitor__c> cmplst = new list<Competitor__c>();
         cmplst.add(testComp);
         try
         {
             //delete testComp;
             AP20_CompetitorTriggerUtil.processCompetitorBeforeDelete(cmplst);
         }catch(DMLException e){
          system.assert(true, e.getMessage().contains('You cannot delete this competitor because Opportunity Step is :Closed / Won (SOW Signed)'));
        }
        
     }
     
     
     
}