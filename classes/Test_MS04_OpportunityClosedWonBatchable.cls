/*Purpose: Test Class for providing code coverage to _MS04_OpportunityClosedWonBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData=true)
private class Test_MS04_OpportunityClosedWonBatchable  {
private static Mercer_TestData mtdata =new  Mercer_TestData();
 /*
Public static void createCS(){
       ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    }*/

    /* * @Description : This test method provides data coverage to Closed Won Opportunity functionality         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest()
     {
       // createCS();
          Colleague__c testColleague = new Colleague__c();
         testColleague.Name = 'TestColleague';
         testColleague.EMPLID__c = '12345678901';
         testColleague.LOB__c = '11111';
         testColleague.Empl_Status__c = 'Active';
         insert testColleague;
         
         Account testAccount = new Account();
         testAccount.Name = 'TestAccountName';
         testAccount.BillingCity = 'TestCity';
         testAccount.BillingCountry = 'TestCountry';
         testAccount.BillingStreet = 'Test Street';
         testAccount.Relationship_Manager__c = testColleague.Id;
         testAccount.Competitor_Flag__c = True;
         testAccount.One_Code__c = '1234';
         testAccount.MercerForce_Account_Status__c = 'Low Revenue';
         insert testAccount;
         testAccount.MercerForce_Account_Status__c = 'Recent Win'; 
         update testAccount;
         
         Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Minneapolis - South Seventh';
        Test.startTest();
        insert testOppty;
        Test.stopTest();
         
        //Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Pricebook2 pb = new Pricebook2();
        pb.name = 'pbtest';
        insert pb;
               
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        //pro.Classification__c = 'Consulting Solution Area';
        insert pro;
        
        //PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        
         PricebookEntry pbEntry = new PricebookEntry();
       pbEntry.Product2Id = pro.Id;
       pbEntry.UnitPrice = 220.30;
       pbEntry.Pricebook2Id = pb.Id;
       pbEntry.CurrencyIsoCode = 'ALL';
       pbEntry.IsActive = true;
       insert pbEntry;
        
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+70;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        // opptyLineItem.CurrentYearRevenue_edit__c = 100.00;
        opptyLineItem.Duration__c = 'One-Time Project';
        insert opptylineItem;
        //Test.stopTest();
        testOppty.Name = 'Test Opportunity1' + String.valueOf(Date.Today());
         testOppty.Type = 'New Client';
         testOppty.AccountId = testAccount.Id;
         //request id:12638 commenting step
         //testOppty.Step__c = 'Identified Single Sales Objective(s)';
         testOppty.Close_Stage_Reason__c = 'Other';
         testOppty.StageName = 'Identify';
         testOppty.CloseDate = date.Today();
         testOppty.CurrencyIsoCode = 'ALL';
        // testOppty.Current_Year_Revenue__c = opptyLineItem.CurrentYearRevenue_edit__c;
         //testOppty.year
        
         
         List<String> oppOffices_List = new List<String>();               
         ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
         if(officesToBeExcluded.StrValue__c != null){        
             oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
             testOppty.Opportunity_Office__c = oppOffices_List[0];
         }
        
         //request id:12638 commenting step
         //testOppty.Step__c = 'Closed / Won (SOW Signed)';
         testOppty.Close_Stage_Reason__c = 'Other';
         testOppty.StageName = 'Closed / Won';
         
         try{ update testOppty;}
         catch(Exception ex) {
         }
        
        
        MS04_OpportunityClosedWonBatchable myBatch = new MS04_OpportunityClosedWonBatchable();
        MS04_OpportunityClosedWonBatchable.query = MS04_OpportunityClosedWonBatchable.query + 'LIMIT 100';
        database.executeBatch(myBatch , 500);
         

      }
    
}