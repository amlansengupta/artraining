@isTest
public class MF2_SplitDelAPexTest{
@isTest
public static void test1(){
Account acc = new Account ();
acc.name='test';
insert acc;
Opportunity op1 = new Opportunity();
op1.name='Test Opp1';
op1.AccountId = acc.Id;
op1.stageName='Identify';
op1.closeDate = system.today();
op1.Type='New Client';
Op1.Opportunity_office__c='Aarhus - Axel';
insert op1;

OpportunityTeamMember OTM1 = [Select id, UserId, OpportunityId, Opportunity.OwnerId from OpportunityteamMember where OpportunityId =:op1.id];
 ApexPages.StandardController sc = new ApexPages.StandardController(OTM1);
 MF2_SplitDelAPex ob = new  MF2_SplitDelAPex(sc);
 ob.delSplit();
User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        Op1.OwnerId = user1.Id;
        update op1;
  //OpportunityTeamMember OTM2 = [Select id, UserId, OpportunityId, Opportunity.OwnerId from OpportunityteamMember where OpportunityId =:op1.id and userId Opportunity.OwnerId];      
  ApexPages.StandardController sc1 = new ApexPages.StandardController(OTM1);
 MF2_SplitDelAPex ob1 = new  MF2_SplitDelAPex(sc1);
 ob1.delSplit();       
 }

}