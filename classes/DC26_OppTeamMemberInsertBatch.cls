/*Purpose: One time batch job to check all the existing sales credits and insert the members to opportunity team if it not exist already
==============================================================================================================================================
----------------------- 
VERSION     AUTHOR              DATE        DETAIL 
   1.0 -    Gyan Chandra     10/24/2013      Created Apex Batchable class
   2.0 -    Sarbpreet        7/7/2014        Added Comments.
============================================================================================================================================== 
*/
public class DC26_OppTeamMemberInsertBatch implements Database.Batchable<sObject>,schedulable
{
    //List to Hold Opportunity Team Members which needs to be inserted.
    List<OpportunityTeamMember> lstoppTeamMembersToBeInserted = new List<OpportunityTeamMember>();
    // List variable to store error logs
    public List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    //List to hold Opportunity Team Member results
    List<Database.SaveResult> oppTeamMemberResult = new List<Database.SaveResult>();
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        //Fetch all sales-credit Records already Exist in System.
        String query = 'Select id,Opportunity__c, EmpName__c,Emp_Id__c From Sales_Credit__c '; 
        return Database.getQueryLocator(query);
    }
    
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
   public void execute(Database.BatchableContext bc,List<Sales_Credit__c> lstsalesfromBatch)
    {
        //Integer to hold error count while updating account owner
        Integer errorCount = 0; 
        
        //List to Hold Opportunity Team Members
        List<OpportunityTeamMember> lstoppTeamMembers = new List<OpportunityTeamMember>();
        
        //Map to Hold key - EmpID of collegue + oppId Attached with sales-credit Record and value as whole sales-credit record.
        Map<String,Sales_Credit__c> mapOppWithSalesCredit = new Map<String,Sales_Credit__c>();
        
         //Map to Hold key - EmpNumber of User+ oppId Attached with Opportunity Team Member Record and value as whole Opportunity Team member record.
        Map<String,OpportunityTeamMember> mapOppWithTeamMembers = new Map<String,OpportunityTeamMember>();
        
        // Set to Hold OppID/EmpId attached with sales-credit.
        Set<String> setOppId = new Set<String>();
        Set<String> setEmpId = new Set<String>();
        
        //Iterate through sales-credit list and create Map to Hold key - EmpID of collegue + oppId Attached with sales-credit Record and value as whole sales-credit record.
        for(Sales_Credit__c obj:lstsalesfromBatch)
        {
               setOppId.add(obj.Opportunity__c);
               mapOppWithSalesCredit.put(obj.Emp_Id__c+'_'+obj.Opportunity__c,obj);
               setEmpId.add(obj.Emp_Id__c);
        }
        
        // Fetch all opportunity Team members record for opportunity
        lstoppTeamMembers = [Select o.OppTeamMemID__r.EmployeeNumber, o.OpportunityId,o.OppTeamMemID__c From OpportunityTeamMember o  where OpportunityId IN :setOppId];   
        
        //Iterate through lstoppTeamMembers list and create Map to Hold key - EmpNumber of User+ oppId Attached with Opportunity Team Member Record and value as whole Opportunity Team member record
        for(OpportunityTeamMember otm : lstoppTeamMembers)
        {
            mapOppWithTeamMembers.put(otm.OppTeamMemID__r.EmployeeNumber+'_'+otm.OpportunityId,otm);
        }
        
        //Fetch all user Record those are active in System.
        List<User> userList = [Select Id ,EmployeeNumber From User where EmployeeNumber IN : setEmpId and isActive = True]; 
        Map<String,String> userMap = new Map<String,String>();
        For(User u : userList)
        {
            userMap.put(u.EmployeeNumber,u.Id);
        }
        
        // Check if Opportunity Team Member record not already exist for selected sales-credit record.
        for(String s : mapOppWithSalesCredit.keySet())
        {
                 Integer index = s.indexOf('_');
                 String empNum = s.substring(0,index);
                 String oppId = s.substring(index+1);
            if(mapOppWithTeamMembers.get(s)==Null && userMap.get(empNum)!= Null)
            {
                //create a new instance of opportunity team member
                 OpportunityTeamMember teamMember = new OpportunityTeamMember();
                 
                 //assign the new record's owner as team member
                 teamMember.userId = userMap.get(empNum);
                 //assign new record's opportunity ID as team member records' opportunity ID
                 teamMember.OpportunityId = oppId;                     
                 // add each instance to the list
                 lstoppTeamMembersToBeInserted.add(teamMember);
            }
        }
         // Insert List of New Opportunity Team Members.
        if(!lstoppTeamMembersToBeInserted.isEmpty())
        {
            oppTeamMemberResult = Database.insert(lstoppTeamMembersToBeInserted,false) ; 
            integer i = 0;
         //Iterate for Error Logging
            for(Database.Saveresult result : oppTeamMemberResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC26:' + result.getErrors()[0].getMessage() , errorLogs, lstoppTeamMembersToBeInserted[i].Id);  
                  errorCount++;
                }
                i++;
            }
        }
         
        //Update the status of batch in BatchLogger object
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>(); 
        
         errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('DC26 Status :List of Sales Credit:' + lstsalesfromBatch.size() +
                         ' oppTeamMembers List:' + lstoppTeamMembersToBeInserted.size() +     + ' Total Errorlog Size: '+errorLogs.size() +
                          ' Batch Error Count :'+errorCount  ,  errorLogStatus , lstsalesfromBatch[0].Id);    
         insert     errorLogStatus ;
        

        lstoppTeamMembersToBeInserted.clear();
        mapOppWithSalesCredit.clear();
        mapOppWithTeamMembers.clear();
        lstoppTeamMembers.clear();
        userMap.clear();
    }
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */  
    public void finish(Database.BatchableContext bc)
    {
         if(errorLogs.size()>0)
             database.insert(errorlogs, false);
    

    }
    
    /*
    *   Schedulable's class Execute Method
    */
    public void execute(SchedulableContext sc)
    {
        DC26_OppTeamMemberInsertBatch batch = new DC26_OppTeamMemberInsertBatch();
        database.executeBatch(batch);    
    }
}