global class batchableChatterPostForOpportunity9Days implements Database.Batchable<sObject> {
    String opp;
global Database.QueryLocator start(Database.BatchableContext bc) {
Date countDay9 = Date.Today().AddDays(-9);
    if(Test.isRunningTest()){
        opp = 'select Id, Name, OwnerId from Opportunity Where StageName=\'Pending Project\' and Chatter_Posted_for_5_days__c = False Limit 100';  
    }else{
        opp = 'select Id, Name, OwnerId from Opportunity Where Age_of_Pending_Project_Stage__c=9  and StageName=\'Pending Project\' and Chatter_posted_for_9_days__c = False';
    }
    return Database.getQueryLocator(opp);
}

global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
   system.debug('%%%' +scope );
   chatterPostInOpportunityNinedays.chatterPostMethod((List <Opportunity>)scope);
}

global void finish(Database.BatchableContext bc) {

}
}