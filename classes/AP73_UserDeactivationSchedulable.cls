/* Purpose: Scheduler for Batch
===================================================================================================================================
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          08/23/2013  Schedulable class for deactivating corresponding users for recently deactivated Colleagues
 ======================================================================================================================================*/


global class AP73_UserDeactivationSchedulable implements schedulable{

	global AP73_UserDeactivationSchedulable(){}
	
	global void execute(SchedulableContext SC)    
	 {  
	     //String variable to fetch inactive Colleague records  
	     String  qry = 'Select id, Empl_Status__c,EMPLID__c from Colleague__c where EMPLID__c != null and Empl_Status__c like \'Inactive\'  and Deactivated_On__c = TODAY';        
	     database.executeBatch(new AP74_UserDeactivationBatch(qry), 100);  
	     
	 }   
 }