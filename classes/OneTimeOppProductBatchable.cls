/*Purpose:  This is one time batch class to dummy update opportunity product for project required

 */
global class OneTimeOppProductBatchable implements Database.Batchable<sObject>{
    private String query;       
    List<String> LobList;
    public String typae;
    // List variable to store error logs 
    private List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    /*
    *   Class Constructor 
    */
    global OneTimeOppProductBatchable (){
        LobList=new List<String>();
     

        
         String qry='Select ID,OpportunityId,Opportunity.Opportunity_Office__c,Opportunity.Opportunity_Country__c,Opportunity.OneCode__c,Opportunity.Type,Product_Code_del__c,Is_Project_Required__c,LOB__c,Commission__c from OpportunityLineItem ';
        if(Test.isRunningTest()){
            qry='Select ID from OpportunityLineItem limit 1';
        }
         this.query= qry;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
            System.debug('query***'+query);
        return Database.getQueryLocator(query);
    }
    
  
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope)
    {  
         Integer errorCount = 0 ;
         List<OpportunityLineItem> OpptyProdductListUpdate = new List<OpportunityLineItem>();         
                          
         for(OpportunityLineItem lineItem:scope){ 
               
               lineItem.is_project_Required__c=false;
                OpptyProdductListUpdate.add(lineItem);            

        }
                           
                 
     // List variable to save the result of the Opportunity Line Items which are updated   
     List<Database.SaveResult> srList= Database.Update(OpptyProdductListUpdate, false);  
    
  
     if(srList!=null && srList.size()>0) {
         // Iterate through the saved Opportunity Line Items  
         Integer i = 0;             
         for (Database.SaveResult sr : srList)
         {
                // Check if Oppportunity Product are not updated successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) 
                {
                        errorCount++;
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('OneTimeOppProductBatchable:' +sr.getErrors()[0].getMessage(), errorLogs, OpptyProdductListUpdate[i].Id);  
                }
                i++;
         }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('OneTimeOppProductBatchable Status :scope:' + scope.size() +' OpptyProdductListUpdate:' + OpptyProdductListUpdate.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);
     
      database.insert(errorLogStatus);    
    }
    
    global void finish(Database.BatchableContext BC)
    {
                           
        if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }
    

}