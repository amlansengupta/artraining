/*Purpose: Test class to provide test coverage for New account.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan   06/03/2014  Created test class 
 
============================================================================================================================================== 
*/
public class New_Account_test {
/*
     * @Description : Test method for new account
     * @ Args       : Null
     * @ Return     : void
     */
    public pageReference redirect(){
         return new PageReference('/apex/Mercer_Account_Search?retURL=%2F001%2Fo&save_new=1&sfdc.override=1');  
    }
}