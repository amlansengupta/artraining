/*Purpose:  This Apex class contains  the logic for Mercer Opportunity New Page 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR          DETAIL 
   1.0 -    Arijit          Created a controller Opportunity new page.
   2.0 -    Srishty         Added Chatter functionality for auto follow of record and Chatter Post Mention per requirement # 367
   3.0 -    Sarbpreet       As per March 2014 Release (Request # 2940) changed the url of saveAndProduct method to new custom vf page for adding products.
   4.0 -        Neeraj                  As per September 2015 Release (Request #7695) changed the condition on which opportunity office value for (WFH) is populated from the Employee office.
============================================================================================================================================== 
*/
public class MercVFC06_OpportuntyController {

    public Opportunity opp{get; set;}
    public string stageValue{get;set;}
    public Profile prof = new Profile();
    public Account account = new Account();
    
    Boolean donotRollback = false;
    public MercVFC06_OpportuntyController(ApexPages.StandardController controller) 
    {
        this.opp = (Opportunity)controller.getRecord();
        String CurrUserOffice = [select employee_office__c from user where id=:userinfo.getuserid()].employee_office__c;
        system.debug('Employee office....'+CurrUserOffice);
        
        List<String> officeList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_Office__c.getDescribe();
        for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
            officeList.add(pickListVal.getLabel());
        }     
                        
        if(CurrUserOffice<>null && !CurrUserOffice.containsIgnoreCase('WFH') && officeList.contains(CurrUserOffice))//As per PMO 7695 September Release
        {
           this.opp.opportunity_office__c = CurrUserOffice;
        }
        //else if(CurrUserOffice<>null && CurrUserOffice.containsIgnoreCase('WFH'))//As per PMO 7695 September Release
        else{
            this.opp.opportunity_office__c = null;
        }
        
        //set opportunity stagename to Identify when creating a new Opportunity
        //this.opp.stageName= 'Identify';
        //stageValue= 'Above the Funnel';
        //UAT FIx
        stageValue= 'Identify';
        //set Opportunity Probability to 0 
        //this.opp.Probability = 0;
        this.prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
    }
    //UAT 2670
     public List<SelectOption> getStageVal() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption(System.Label.CL57_OpportunityType,System.Label.None_With_Dash));
        stOptions.add(new SelectOption(System.Label.Stage_Above_the_Funnel,System.Label.Stage_Above_the_Funnel));           
         stOptions.add(new SelectOption(System.Label.Stage_Identify,System.Label.Stage_Identify));            
         //sOptions.add(new SelectOption(System.Label.Stage_Qualify,System.Label.Stage_Qualify));           
        
        return stOptions;
        
    }
    
    //method to create save and add product
    public pageReference saveAndProduct()
    {
         opp.stageName = stageValue;
        SavePoint sp;
        sp = Database.setSavePoint();
        try{
            
            account = getAccountDetail(opp.AccountId);
            
            if(prof.Name<>'System Administrator'&&( account.BillingCity==null|| account.BillingStreet==null) && (account.shippingcity == null || account.shippingstreet == null)&&account<>null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.'));
                return null;
            }
            else if(stageValue == 'None'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, Please set stage value'));
                return null;
            }
            else
            {
           
                //Insert Opportunity
                insert opp;
            }
            
            
            Opportunity opp = getOpportunityDetail(opp);
            
            if(opp.Account.OwnerId <> null)
            {
                if(opp.OwnerId <> opp.Account.OwnerId)
                {
                    
                    String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id;
                    String post = 'New Opportunity created:  '  + opp.Name + '<' + fullRecordURL + '>';
                    
                    if(opp.Account.Owner.isActive)
                    {
                        //call the method to Notify on chatter the Opportunity owner that Opportunity is created
                        AP43_ChatterUtils.chatterPostMention(opp.AccountId, opp.Account.OwnerId, post, UserInfo.getSessionId());
                        System.debug('\The loop starts');
                        //call the method to follow the Opportunity Owner
                        AP43_ChatterUtils.autoFollow(opp.Id, opp.Account.OwnerId);
                    }  
                }   
            }
            
                
        }catch(DMLException dme){throw dme;for (Integer i = 0; i < dme.getNumDml(); i++){if(dme.getDMLMessage(i)<> null && !dme.getDMLMessage(i).contains('subscription')){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, dme.getDMLMessage(i)));}else{donotRollback = true;}} 
            if(donotRollback == false){Database.rollback(sp);return null;}else{new PageReference('/p/opp/SelectSearch?addTo='+opp.Id+'&retURL=%2F006%2Fe');}
        }catch(Exception e){throw e;if(e.getMessage()<> null && !e.getMessage().contains('subscription'))ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));else{donotRollback = true;}                             
         if(donotRollback == false) {Database.rollback(sp);return null;}else {new PageReference('/apex/Mercer_ProductSearch?addTo=' + opp.Id + '&retURL=%2F' + opp.Id + '&sfdc.override=1&id=' + opp.Id); //As per March 2014 Release (Request # 2940) changed the url of saveAndProduct method to new custom vf page for adding products.
                                                                             }
        }
        //return this page when opportunity is created to Add product        
        //As per March 2014 Release (Request # 2940) changed the url of saveAndProduct method to new custom vf page for adding products.
        return new PageReference('/apex/Mercer_ProductSearch?addTo=' + opp.Id + '&retURL=%2F' + opp.Id + '&sfdc.override=1&id=' + opp.Id);
 
    }
    //Method to save opportunity
    public pageReference saveOpportunity()
    {
         opp.stageName = stageValue;
        SavePoint sp;
        try
        {
            sp = Database.setSavePoint();
            
            account = getAccountDetail(opp.AccountId);
             system.debug('account is ' + account);
            system.debug('profile is ' + prof.Name);
            system.debug('StageName is ' + opp.stageName);
            system.debug('probability is ' + opp.probability );
            System.debug('abc'+stageValue);
            if(prof.Name<>'System Administrator'&&(account.BillingCity==null||account.BillingStreet==null) && (account.shippingcity == null || account.shippingstreet == null) &&account<>null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, the Account RM or an Account Team member need to update the \'Street\' and \'City/Locality\' for the Account associated to this Opportunity.'));return null;
            }
            
            else if(stageValue == 'None'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Before saving this Opportunity, Please set stage value'));return null;
            }
            else
            {
                //Insert Opportunity
                insert opp;
            }
            
            Opportunity opp = getOpportunityDetail(opp);
            
            
            if(opp.OwnerId <> opp.Account.OwnerId)
            {
                String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id;
                String post = 'New Opportunity created:  '  + opp.Name + '<' + fullRecordURL + '>';
                
                if(opp.Account.Owner.isActive)
                {
                    //call the method to Notify on chatter the Opportunity owner that Opportunity is created
                    AP43_ChatterUtils.chatterPostMention(opp.AccountId, opp.Account.OwnerId, post, UserInfo.getSessionId());
                    //call the method to follow the Opportunity Owner
                    AP43_ChatterUtils.autoFollow(opp.Id, opp.Account.OwnerId);
                }  
            }
                
        }catch(DMLException dme){throw dme;for (Integer i = 0; i < dme.getNumDml(); i++){if(dme.getDMLMessage(i)<> null && !dme.getDMLMessage(i).contains('subscription')){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, dme.getDMLMessage(i)));}else{donotRollback = true;}} 
           if(donotRollback == false) {Database.rollback(sp);return null;}else{return new PageReference('/'+opp.Id);}
       }catch(Exception e) {throw e;if(e.getMessage()<> null && !e.getMessage().contains('subscription'))ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));else{donotRollback = true;}if(donotRollback == false){Database.rollback(sp);return null;}else{return new PageReference('/'+opp.Id);}
        }
        //return the Saved Opportunity Page
        return new pageReference('/'+opp.Id);       
    }
        
    public Opportunity getOpportunityDetail(Opportunity opp)
    {
        return [Select Id, OwnerId, Name, Account.OwnerId, Account.Name, AccountId, Account.Owner.isActive From opportunity where Id = : opp.Id];
    }
    public Account getAccountDetail(string accId)
    {
        if(accId<>null)
        {
            return [select Id, BillingCity, BillingStreet,shippingcity, shippingstreet from account where Id =:accId];
        }
        else
        {
            return null;
        }
    }
}