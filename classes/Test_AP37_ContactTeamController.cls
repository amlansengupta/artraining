/*
 Purpose: 
 This test class provides data coverage to AP37_ContactTeamController  class       
 ==============================================================================================================================================
 History 
 -----------------------
  VERSION     AUTHOR  DATE        DETAIL    
  1.0 -    Shashank Singhal 05/25/2013  Created Test Class
 ============================================================================================================================================== 
 */
@isTest
private class Test_AP37_ContactTeamController 
{
    /*
     * @Description : Test method for Contact Team functionality 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        
     
         Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
      //  insert testColleague;
        
      
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        User testUser = new User();
        testUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', testUser);
        
          
          system.runAs(User1){
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = '123456';
      //  testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
     
            Contact_Team_Member__c testConTeam = new Contact_Team_Member__c();
            testConTeam.Contact__c = testContact.Id;
            testConTeam.ContactTeamMember__c = testUser.Id;
        
    
            ApexPages.Standardcontroller stdController = new Apexpages.StandardController(testConTeam);
            AP37_ContactTeamController controller = new AP37_ContactTeamController(stdController);
            controller.save();
            
            /*PageReference ref = new PageReference('apex/Test_AP37_ContactTeamController'); 
            Test.setCurrentPage(ref);
            AP37_ContactTeamController controller1 = new AP37_ContactTeamController(null);
            System.assert (ApexPages.getMessages().get(0).getSeverity() == ApexPages.Severity.ERROR); */   
     
            delete testConTeam;
          }
         
    }
    /*
     * @Description : Test method for Contact Team and Save & New functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest1() 
    {
        
         Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
      //  insert testColleague;
        
        User testUser = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        testUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', testUser);
        
        User user1 = new User();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.One_Code__c = '123456';
           // testAccount.Relationship_Manager__c = testColleague.Id;
            insert testAccount;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            insert testContact;
            
            
            
            Contact_Team_Member__c testConTeam = new Contact_Team_Member__c();
            testConTeam.Contact__c = testContact.Id;
            testConTeam.ContactTeamMember__c = testUser.Id;
                
                ApexPages.Standardcontroller stdController = new Apexpages.StandardController(testConTeam);
                AP37_ContactTeamController controller = new AP37_ContactTeamController(stdController);
                controller.savenew();
                controller.cancel();
        }
    }
}