/*Purpose: Test Class for providing code coverage to MercVFC06_OpportunityController class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Arijit   03/29/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
Trisha          21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify' 
=======================================================
============================================================================================================================================== 
*/
@isTest(seeAllData = false)
private class Test_MercVFC06_OpportunityController {
    
    /*
* @Description : This test method provides data coverage for Mercer Opportunity New Page (Scenerio 1)    
* @ Args       : no arguments        
* @ Return     : void       
*/
    public static User user1,user2, user3 = new User();
    
    private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = String.valueOf(Integer.valueOf('1234567890')+ i);
        //testUser.Employee_Office__c='Brisbane - Eagle';
        testUser.Employee_Office__c='Aarhus - Axel';
        insert testUser;
        System.runas(testUser){
        }
        return testUser;
        
    }
    
    public static void insertUsers(){
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1, 5);
        user2 = createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2, 6);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);
        //user1.EmployeeNumber = '123';                  
    }    
    
    
    static testMethod void myUnitTestsave1() 
    {
        /*  List<ApexConstants__c> apcList=new List<ApexConstants__c>();
ApexConstants__c apc=new ApexConstants__c();
apc.name='Above the funnel';
apc.Description__c='test';
apc.RelatedComponent__c='';
apc.StrValue__c='Marketing/Sales Lead;Executing Discovery;';
apcList.add(apc);

ApexConstants__c apc1=new ApexConstants__c();
apc1.name='Active Pursuit';
apc1.Description__c='test';
apc1.RelatedComponent__c='';
apc1.StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
apcList.add(apc1);

ApexConstants__c apc2=new ApexConstants__c();
apc2.name='Finalist';
apc2.Description__c='test';
apc2.RelatedComponent__c='';
apc2.StrValue__c='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
apcList.add(apc2);

ApexConstants__c apc3=new ApexConstants__c();
apc3.name='Identify';
apc3.Description__c='test';
apc3.RelatedComponent__c='';
apc3.StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
apcList.add(apc3);

ApexConstants__c apc4=new ApexConstants__c();
apc4.name='Selected';
apc4.Description__c='test';
apc4.RelatedComponent__c='';
apc4.StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
apcList.add(apc4);

ApexConstants__c apc5=new ApexConstants__c();
apc5.name='AP02_OpportunityTriggerUtil_1';
apc5.Description__c='test';
apc5.RelatedComponent__c='';
apc5.StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;';
apcList.add(apc5);

ApexConstants__c apc6=new ApexConstants__c();
apc6.name='AP02_OpportunityTriggerUtil_2';
apc6.Description__c='test';
apc6.RelatedComponent__c='';
apc6.StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
apcList.add(apc6);

ApexConstants__c apc7=new ApexConstants__c();
apc7.name='ScopeITThresholdsList';
apc7.Description__c='test';
apc7.RelatedComponent__c='';
apc7.StrValue__c='EuroPac:25000;Growth Markets:2500;North America:25000';
apcList.add(apc7);

insert apcList;    */
        //try{
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //testUser.Employee_Office__c='Brisbane - Eagle';
                                 Employee_Office__c='Aarhus - Axel'      
                                );
        
        insert testUser;
        User testUser1 = new User(LastName = 'tetsLIVESTON',
                                  FirstName='JASON',
                                  Alias = 'jliv',
                                  Email = 'jasontest.liveston@asdf.com',
                                  Username = 'jasontest.test@test.com',
                                  ProfileId = profileId.id,
                                  TimeZoneSidKey = 'GMT',
                                  LanguageLocaleKey = 'en_US',
                                  EmailEncodingKey = 'UTF-8',
                                  LocaleSidKey = 'en_US',
                                  Employee_Office__c='Aarhus - Axel'      
                                 );
        insert testUser1;
        
        Account testAccount = new Account();
        System.runAs(testUser1){
            
            
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser1.Id;
            insert testAccount;
            
        }
        System.runAs(testUser){
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty1.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_office__c = 'Aarhus - Axel';
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            
            PageReference pageref = controller.saveAndProduct();
            //PageReference pageref1=   controller.saveOpportunity();
        }
        
        
    }
    static testMethod void myUnitTestsave2() 
    {
        /*    List<ApexConstants__c> apcList=new List<ApexConstants__c>();
ApexConstants__c apc=new ApexConstants__c();
apc.name='Above the funnel';
apc.Description__c='test';
apc.RelatedComponent__c='';
apc.StrValue__c='Marketing/Sales Lead;Executing Discovery;';
apcList.add(apc);

ApexConstants__c apc1=new ApexConstants__c();
apc1.name='Active Pursuit';
apc1.Description__c='test';
apc1.RelatedComponent__c='';
apc1.StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
apcList.add(apc1);

ApexConstants__c apc2=new ApexConstants__c();
apc2.name='Finalist';
apc2.Description__c='test';
apc2.RelatedComponent__c='';
apc2.StrValue__c='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
apcList.add(apc2);

ApexConstants__c apc3=new ApexConstants__c();
apc3.name='Identify';
apc3.Description__c='test';
apc3.RelatedComponent__c='';
apc3.StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
apcList.add(apc3);

ApexConstants__c apc4=new ApexConstants__c();
apc4.name='Selected';
apc4.Description__c='test';
apc4.RelatedComponent__c='';
apc4.StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
apcList.add(apc4);

ApexConstants__c apc5=new ApexConstants__c();
apc5.name='AP02_OpportunityTriggerUtil_1';
apc5.Description__c='test';
apc5.RelatedComponent__c='';
apc5.StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;';
apcList.add(apc5);

ApexConstants__c apc6=new ApexConstants__c();
apc6.name='AP02_OpportunityTriggerUtil_2';
apc6.Description__c='test';
apc6.RelatedComponent__c='';
apc6.StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
apcList.add(apc6);

ApexConstants__c apc7=new ApexConstants__c();
apc7.name='ScopeITThresholdsList';
apc7.Description__c='test';
apc7.RelatedComponent__c='';
apc7.StrValue__c='EuroPac:25000;Growth Markets:2500;North America:25000';
apcList.add(apc7);

insert apcList;        */
        //try{
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_Office__c='Brisbane - Eagle' 
                                 Employee_Office__c='Aarhus - Axel'     
                                );
        
        insert testUser;
        User testUser1 = new User(LastName = 'tetsLIVESTON',
                                  FirstName='JASON',
                                  Alias = 'jliv',
                                  Email = 'jasontest.liveston@asdf.com',
                                  Username = 'jasontest.test@test.com',
                                  ProfileId = profileId.id,
                                  TimeZoneSidKey = 'GMT',
                                  LanguageLocaleKey = 'en_US',
                                  EmailEncodingKey = 'UTF-8',
                                  LocaleSidKey = 'en_US',
                                  //Employee_Office__c='Brisbane - Eagle' 
                                  Employee_Office__c='Aarhus - Axel'      
                                 );
        insert testUser1;
        
        Account testAccount = new Account();
        System.runAs(testUser1){
            
            
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser1.Id;
            insert testAccount;
            
        }
        System.runAs(testUser){
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty1.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Aarhus - Axel';
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            
            //PageReference pageref = controller.saveAndProduct();
            PageReference pageref1=   controller.saveOpportunity();
        }
        
        
    }
    
    /*
* @Description : This test method provides data coverage for Mercer Opportunity New Page (Scenerio 2)    
* @ Args       : no arguments        
* @ Return     : void       
*/
    static testMethod void myUnitTest1() 
    {   
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_Office__c='Brisbane - Eagle' 
                                 Employee_Office__c='Aarhus - Axel'
                                );
        insert testUser;
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.OwnerId = testUser.Id;
        insert testAccount;
        
        System.runAs(testUser){
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_office__c = 'Aarhus - Axel';
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty1';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.Opportunity_office__c = 'Aarhus - Axel';
            
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            PageReference pageref = controller.saveAndProduct();
            
            ApexPages.StandardController stdController1 = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller1 = new MercVFC06_OpportuntyController(stdController1);
            PageReference pageref1 = controller1.saveAndProduct();
            
        } 
        
        
    }
    
    
    /*
* @Description : This test method provides data coverage for Mercer Opportunity New Page (Scenerio 3)    
* @ Args       : no arguments        
* @ Return     : void       
*/
    static testMethod void myUnitNegativeTest() 
    {
        Test.startTest();
        
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_Office__c='Brisbane - Eagle' 
                                 Employee_Office__c='Aarhus - Axel'
                                );
        insert testUser;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        //testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        //testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.OwnerId = testUser.Id;
        insert testAccount;
        System.runAs(testUser){
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.Opportunity_Office__c = 'Aarhus - Axel';
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            PageReference pageref = controller.saveOpportunity();
            
            ApexPages.StandardController stdController1 = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller1 = new MercVFC06_OpportuntyController(stdController1);
            PageReference pageref1 = controller1.saveOpportunity();
        }
        
        Test.stopTest();
    }
    
    
    /*
* @Description : This test method provides data coverage for Mercer Opportunity New Page (Scenerio 4)    
* @ Args       : no arguments        
* @ Return     : void       
*/
    static testMethod void myUniteTest3() 
    {
        
        /*     List<ApexConstants__c> apcList=new List<ApexConstants__c>();
ApexConstants__c apc=new ApexConstants__c();
apc.name='Above the funnel';
apc.Description__c='test';
apc.RelatedComponent__c='';
apc.StrValue__c='Marketing/Sales Lead;Executing Discovery;';
apcList.add(apc);

ApexConstants__c apc1=new ApexConstants__c();
apc1.name='Active Pursuit';
apc1.Description__c='test';
apc1.RelatedComponent__c='';
apc1.StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
apcList.add(apc1);

ApexConstants__c apc2=new ApexConstants__c();
apc2.name='Finalist';
apc2.Description__c='test';
apc2.RelatedComponent__c='';
apc2.StrValue__c='Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
apcList.add(apc2);

ApexConstants__c apc3=new ApexConstants__c();
apc3.name='Identify';
apc3.Description__c='test';
apc3.RelatedComponent__c='';
apc3.StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
apcList.add(apc3);

ApexConstants__c apc4=new ApexConstants__c();
apc4.name='Selected';
apc4.Description__c='test';
apc4.RelatedComponent__c='';
apc4.StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;';
apcList.add(apc4);

ApexConstants__c apc5=new ApexConstants__c();
apc5.name='AP02_OpportunityTriggerUtil_1';
apc5.Description__c='test';
apc5.RelatedComponent__c='';
apc5.StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;';
apcList.add(apc5);

ApexConstants__c apc6=new ApexConstants__c();
apc6.name='AP02_OpportunityTriggerUtil_2';
apc6.Description__c='test';
apc6.RelatedComponent__c='';
apc6.StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;';
apcList.add(apc6);

ApexConstants__c apc7=new ApexConstants__c();
apc7.name='ScopeITThresholdsList';
apc7.Description__c='test';
apc7.RelatedComponent__c='';
apc7.StrValue__c='EuroPac:25000;Growth Markets:2500;North America:25000';
apcList.add(apc7);

insert apcList;
*/   
        
        Test.startTest();
        
        //insertUsers();
        //        System.runAs(user1){
        
        
        /*      User testUser = new User();
String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
testUser = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert', 'usrLstName', 1);
*/  
        Colleague__c testColleague = new Colleague__c();                  
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_Office__c='Brisbane - Eagle' 
                                 Employee_Office__c='Aarhus - Axel'
                                );
        insert testUser;
        
        Account testAccount = new Account();
        
        testAccount.Name = 'TestAccountName';
        //testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        //testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.OwnerId = testUser.Id;        
        testAccount.BillingCity = 'Sacramento';
        testAccount.BillingStreet = '2 Test Street';
        insert testAccount;
        
        system.debug('testAccount###'+testAccount.Id);
        System.runAs(testUser){
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Executing Discovery;';
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'USD';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Executing Discovery;';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'USD';
            testOppty1.Opportunity_Office__c = 'Aarhus - Axel';
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            PageReference pageref = controller.saveAndProduct();
            
            ApexPages.StandardController stdController1 = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller1 = new MercVFC06_OpportuntyController(stdController1);
            PageReference pageref1 = controller1.saveAndProduct();
            
            //}
            
            Test.stopTest();
        }
    }
    
    
    /*
* @Description : This test method provides data coverage for Mercer Opportunity New Page (Scenerio 5)    
* @ Args       : no arguments        
* @ Return     : void       
*/
    static testMethod void myNegativeTest() 
    {
        
        Test.startTest();
        
        // insertUsers();
        // System.runAs(user1){
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        /* User testUser = new User();
String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
testUser = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert', 'usrLstName', 1);    */
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_Office__c='Brisbane - Eagle' 
                                 Employee_Office__c='Aarhus - Axel'
                                );
        insert testUser;
        System.runAs(testUser){
            
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            //testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            //testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.BillingCity = 'Sacramento';
            testAccount.BillingStreet = '2 Test Street';
            insert testAccount;
            
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty';
            testOppty.Type = 'New Client';
            //testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            //testOppty.Opportunity_Office__c = 'Brisbane - Eagle';
            testOppty.Opportunity_Office__c = 'Aarhus - Axel';
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            //testOppty.AccountId = testAccount.Id;
            //Request Id:12638 commenting step__c
            //testOppty.Step__c = 'Identified Deal';
            //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            //testOppty.Opportunity_Office__c = 'Brisbane - Eagle';
            testOppty1.Opportunity_Office__c = 'Aarhus - Axel';
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
            MercVFC06_OpportuntyController controller = new MercVFC06_OpportuntyController(stdController);
            PageReference pageref = controller.saveOpportunity();
            controller.getStageVal();
            
            ApexPages.StandardController stdController1 = new ApexPages.StandardController(testOppty1);
            MercVFC06_OpportuntyController controller1 = new MercVFC06_OpportuntyController(stdController1);
            PageReference pageref1 = controller1.saveOpportunity();
            
            Test.stopTest();
        }
    }
    
}