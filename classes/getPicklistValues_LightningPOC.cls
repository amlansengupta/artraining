public class getPicklistValues_LightningPOC {
    private static final String label = 'all';
    private static final String linkType = 'DETAIL_PAGE_LINK';
    
     /*@AuraEnabled
    public static boolean isRunningUserInParentHierarchy(Id recordId) {
        boolean isNotChild = true;
        try{
            List<Profile> runningUserProfileList = [Select Name from Profile where Id=:userInfo.getProfileId()];
            String runningUserProfileName;
            if(runningUserProfileList != null && runningUserProfileList.size() > 0){
                runningUserProfileName = runningUserProfileList.get(0).Name;
            }
            if(runningUserProfileName != null && runningUserProfileName.equalsIgnoreCase('System Administrator')){
                return true;
            }
            List<Account> accList = [Select Owner.UserRoleId from Account where Id=:recordId];
            Id userRoleId;
            if(accList != null && accList.size() > 0){
                userRoleId = accList.get(0).Owner.UserRoleId;
            }
            Set<Id> ownerRoleIds = new Set<Id>();
            ownerRoleIds.add(userRoleId);
            Set<Id> childRoleIds = getAllSubRoleIds(ownerRoleIds);
            if(childRoleIds != null){
                for(Id childRoleId : childRoleIds){
                    if(childRoleId == UserInfo.getUserRoleId()){
                        isNotChild = false;
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'UserRoleHierarchyUtil', 'isRunningUserInParentHierarchy');
        }
        return isNotChild;
    }
    
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {

        Set<ID> currentRoleIds = new Set<ID>();
    
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
             IN :roleIds AND ParentRoleID != null]) {
            currentRoleIds.add(userRole.Id);
        }
    
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
    
        return currentRoleIds;
    }*/
    
    @AuraEnabled
    public static List<Map<String, String>> getSubMarket(){
        List<Map<String, String>> items = new List<Map<String, String>>();
        Schema.DescribeFieldResult fieldResult = Account.Account_Sub_Market__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        if(pList != null && !pList.isEmpty()){
            items.add(new Map<String, String>{'value' => 'None', 'label' => '--None--'});
        }
        for (Schema.PicklistEntry p: pList) {
            items.add(new Map<String, String>{'value' => p.getValue(), 'label' => p.getLabel()});
        }
        return items;
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getGUCountry(){
        List<Map<String, String>> items = new List<Map<String, String>>();
        Schema.DescribeFieldResult fieldResult = Account.GU_Country__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        if(pList != null && !pList.isEmpty()){
            items.add(new Map<String, String>{'value' => 'None', 'label' => '--None--'});
        }
        for (Schema.PicklistEntry p: pList) {
            items.add(new Map<String, String>{'value' => p.getValue(), 'label' => p.getLabel()});
        }
        return items;
    }
    
    //Request No#:17100:This method fetches custom links from custom setting 
    @AuraEnabled
    public static List<MF2_GlobalRelationshipCustomLinks__c> fetchCustomLinks(String linkLabel){
        List<MF2_GlobalRelationshipCustomLinks__c> listOfQuickLinks = new List<MF2_GlobalRelationshipCustomLinks__c>();
        try{
            if(linkLabel == label){
                for(MF2_GlobalRelationshipCustomLinks__c link : MF2_GlobalRelationshipCustomLinks__c.getAll().values()){
                    if(link.Type__c == linkType){
                        listOfQuickLinks.add(link);
                    }
                }
            }
        }
        catch(Exception ex){
            ExceptionLogger.logException(ex, 'getPicklistValues_LightningPOC', 'fetchCustomLinks');
            new AuraHandledException('Custom Links not found! Please try after sometime.');
        }
        return listOfQuickLinks;
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getOutsourcingStatus(){
        List<Map<String, String>> items = new List<Map<String, String>>();
        Schema.DescribeFieldResult fieldResult = Account.Outsourcing_Status__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        if(pList != null && !pList.isEmpty()){
            items.add(new Map<String, String>{'value' => 'None', 'label' => '--None--'});
        }
        for (Schema.PicklistEntry p: pList) {
            items.add(new Map<String, String>{'value' => p.getValue(), 'label' => p.getLabel()});
        }
        return items;
    }
    
    @AuraEnabled
    public static List<Map<String, String>> getClientsinCommon(){
        List<Map<String, String>> items = new List<Map<String, String>>();
        Schema.DescribeFieldResult fieldResult = Account.Clients_in_Common__c   .getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        if(pList != null && !pList.isEmpty()){
            items.add(new Map<String, String>{'value' => 'None', 'label' => '--None--'});
        }
        for (Schema.PicklistEntry p: pList) {
            items.add(new Map<String, String>{'value' => p.getValue(), 'label' => p.getLabel()});
        }
        return items;
    }
    
    @AuraEnabled
    public static Account getaccList(String accid) {
        Account acc = [Select Id,Name,Duplicate_Flag__c,Managed_Office_Details__c,
                       Duplicate_Identified_Date__c,Surviving_OneCode__c,One_Code__c,Primary_Sales_Professional__c,Primary_Sales_Professional__r.Name,Account_ID1__c,
                       Number_of_CEMs__c,PE_Owner__c,PE_Owner__r.Name,NPS_Survey_Sent_Date__c,Account_Sub_Market__c,
                       Government_Client_Identifier__c,Parent_Growth_Plan__c,Exclude_from_CSAT__c,
                       Exclude_from_marketing__c,Private_Equity__c,Partner_Flag__c,Intermediary_Flag__c,DUNS__c,
                       Competitor_Flag__c,Vendor_Flag__c,New_Trustees__c,D_B_Name__c,HQ_Immediate_Parent_DUNS_Name__c,
                       Domestic_DUNS__c,GU_DUNS__c,GU_DUNS_2__c,Domestic_Ultimate_DUNS_Name__c,Global_Ultimate_Name__c,
                       Domestic_Ultimate_Country__c,GU_Country__c,Tradestyle_Name_Long_Text__c,GU_Flag__c,
                       National_ID__c,AGU_Flag__c,Company_Annual_Revenue_USD__c,DB_Total_Assets__c,US_Health_Lead_Consultant__c,US_Health_Lead_Consultant__r.Name,
                       DC_Total_Assets__c,Outsourcing_Status__c,Client_Description__c,Clients_in_Common__c FROM Account Where Id =: accid limit 1];
        return acc;
    }
    
    @AuraEnabled
    public static Account saveAccount (Account acc) {
        
        update acc;
        return acc;
        
    }
    
}