global class SC_SP_CM_Allocation_Update_Batch implements Database.Batchable<sObject>{

    global String query = Label.SC_SP_CM_Allocation_Update_Batch_Query;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {    
        if(Test.isRunningTest()){
               query = 'Select Id, EmpName__r.EMPLID__c,Opportunity__c,Opportunity__r.Owner.EmployeeNumber,'+
                       'SP_CM_Allocation__c, EmpName__r.SP_Flag__c ,EmpName__r.CM_Flag__c, Opportunity__r.Opportunity_Region__c '+
                       ' From Sales_Credit__c Where Opportunity__r.CloseDate = THIS_MONTH LIMIT 200'; 
        }
       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sales_Credit__c> scope)
    {  
        List<Sales_Credit__c> salesCreditListToUpdate = new List<Sales_Credit__c>();
        for(Sales_Credit__c sc: scope){
            if(sc.Opportunity__r.Opportunity_Region__c=='International'){
                 if(sc.EmpName__r.SP_Flag__c=='Y'|| sc.EmpName__r.CM_Flag__c=='FT' ){
                 system.debug('is A Flagg**********');
                     if(sc.EmpName__r.EMPLID__c==sc.Opportunity__r.Owner.EmployeeNumber){
                         system.debug('is A Owner **********');
                            
                             sc.SP_CM_Allocation__c=100.00; 
                             salesCreditListToUpdate.add(sc);                        
                     }
                     else if(sc.EmpName__r.EMPLID__c!=sc.Opportunity__r.Owner.EmployeeNumber){
                         system.debug('Not A Owner **********');
                             
                             sc.SP_CM_Allocation__c=25.00;
                             salesCreditListToUpdate.add(sc);
                     }
                 }
                 else{
                     sc.SP_CM_Allocation__c=0.00;
                     salesCreditListToUpdate.add(sc);
                 }
              }
              else{
              
                  sc.SP_CM_Allocation__c = 0.00;
                  salesCreditListToUpdate.add(sc);
              }
        }
        
        if(!salesCreditListToUpdate.isEmpty()){
              try{            
                Database.update(salesCreditListToUpdate,false);
            }
        
            catch(DmlException dmlException){
                system.debug('###SCException'+dmlException.getMessage());
            }            
        }

   }
        
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}