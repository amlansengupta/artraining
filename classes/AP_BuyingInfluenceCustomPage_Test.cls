@isTest(SeeAllData = true)
public class AP_BuyingInfluenceCustomPage_Test {
    
    public static testMethod void testBuingInfluenceCustomPage(){
        Mercer_TestData testData = new Mercer_TestData();
        
        Account acc = testData.buildAccount();
        test.starttest();
        Opportunity testOpportunity = testData.buildOpportunity();
        test.stoptest();
        Contact cc = testData.buildContact();
          
        
        Buying_Influence__c bi = new Buying_Influence__c();
        bi.mh_Degree__c='High';
        bi.Opportunity__c=testOpportunity.id;
        bi.contact__c=cc.id;
        insert bi;
        ApexPages.StandardController sc = new ApexPages.StandardController(bi);
        AP_BuyingInfluenceCustomPage_Extension obj = new AP_BuyingInfluenceCustomPage_Extension(sc);
        PageReference Success = obj.save();
        Test.setCurrentPage(Success);
        PageReference savenew = obj.saveAndNew();
        PageReference Can = obj.cancel();
        //PageReference savenew = obj.saveAndNew();
        
        
    }
}