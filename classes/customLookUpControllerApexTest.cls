/*****************************************************************************************************************************
User story : 17673 (epic)
Purpose : This is a test class for customLookUpControllerApex.
Created by : Soumil Dasgupta
Created Date : 29/03/2019
Project : MF2
******************************************************************************************************************************/

@isTest
public class customLookUpControllerApexTest {
    static testMethod void customLookUpTest(){
         Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'testLastName';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Email_Address__c = 'abc@gmail.com';
        insert testColleague; 
        
        Test.startTest();
        customLookUpControllerApex.fetchLookUpValues('Test', 'Account');
        customLookUpControllerApex.fetchLookUpValues('Test', 'Colleague__c');
        customLookUpControllerApex.fetchDefaultLookUp(testAccount.Id, 'Account');
        Test.stopTest();
        
    }
}