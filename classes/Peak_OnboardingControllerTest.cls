/**
 * Created by shashiarrabeli on 9/13/18.
 */
@IsTest
public with sharing class Peak_OnboardingControllerTest {

    @testSetup
    public static void setup() {
        list <network> testnetworkList = [SELECT Id
        FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        //create system admin user
        Profile p = [select Id,name from Profile where name='System Administrator' limit 1];
        List<UserRole> roles= [select id from UserRole where name='United States'];
        
        User systemAdmin = new User(profileId = p.id, username = 'standarduserAdmin@peak.com', email = 'standarduser123@peak.com',
                                emailencodingkey = 'UTF-8', localesidkey = 'en_US', languagelocalekey = 'en_US',
                                timezonesidkey = 'America/Los_Angeles', alias='nuser123', lastname='lastname123', userroleId=roles[0].Id);
        insert systemAdmin;
  
        //create community user
        Account acct;
         Contact cont;
        List<User> newUsers = new List<User>();

        system.runAs(systemAdmin){
            p = [select Id,name from Profile where name='Mercer Premier Community - Client' limit 1];
            acct= new Account(Name='TestAccount',Country__c='USA',one_Code__c='A1B1C1');
            insert acct;
            cont = new Contact(Lastname='Standard',AccountId=acct.Id, MailingCountry='India', email='test@gmail.com');
            insert cont;
            roles= [select id from UserRole where name='Acme Corporation Customer User'];
            newUsers.add(new User(Alias = Peak_TestConstants.STANDARD_ALIAS, Email=Peak_TestConstants.STANDARD_EMAIL,
                               EmailEncodingKey=Peak_TestConstants.ENCODING, FirstName=Peak_TestConstants.FIRSTNAME, 
                               LastName=Peak_TestConstants.LASTNAME, 
                               LanguageLocaleKey=Peak_TestConstants.LOCALE,
                               LocaleSidKey=Peak_TestConstants.LOCALE, ProfileId = p.Id, 
                               TimeZoneSidKey=Peak_TestConstants.TIMEZONE, UserName=Peak_TestConstants.STANDARD_USERNAME,
                              contactId = cont.id, PortalRole='manager', CommunityNickname = 'testUser123', IsActive = true));

            Contact expertContact = new Contact(LastName='MercerExpert',AccountId=acct.Id, MailingCountry='India', Email='mercerExpert@TestClass.com');
            insert expertContact;

            newUsers.add(new User(Alias = Peak_TestConstants.STANDARD_ALIAS, Email='mercerExpert@TestClass.com',
                    EmailEncodingKey=Peak_TestConstants.ENCODING, FirstName=Peak_TestConstants.FIRSTNAME,
                    LastName='MercerExpert',
                    LanguageLocaleKey=Peak_TestConstants.LOCALE,
                    LocaleSidKey=Peak_TestConstants.LOCALE, ProfileId = p.Id,
                    TimeZoneSidKey=Peak_TestConstants.TIMEZONE, Username='mercerExpert@TestClass.com',
                    ContactId = expertContact.Id, PortalRole='manager', CommunityNickname = 'mercerExpert', IsActive = true));


        }
        
        
        insert newUsers;
        System.runAs(systemAdmin) {
           
            list <CollaborationGroup> testGroups = new list <CollaborationGroup>();
            for (integer i=0; i<4; i++){
                CollaborationGroup colGroup = new CollaborationGroup(Name='Test Chatter Group' + i,CollaborationType = 'Public', CanHaveGuests = false,
                        isArchived = false, isAutoArchiveDisabled = false, networkId = networkId );
                testGroups.add(colGroup);
            }
            
            insert testGroups;
    
            list <Topic> testTopics = new list <Topic>();
            for (integer i=0; i<2; i++){
                Topic testTopic = new Topic(Name = 'Test Topic ' + i, Description = 'Test Topic1 Description', networkId = networkId);
                testTopics.add(testTopic);
            }
    
            insert testTopics;
            
                ApexConstants__c setting1 = new ApexConstants__c();
                setting1.Name = 'Selected';
                setting1.StrValue__c = '';
                insert setting1;
                    ApexConstants__c setting = new ApexConstants__c();
                setting.Name = 'LiteProfiles';
                setting.StrValue__c = [Select Id from Profile where Name LIKE '%LITE' LIMIT 1].Id;
                insert setting;

            insert new Mercer_Experts__c(Expert_User_Name__c = newUsers[1].Id, Name = newUsers[1].Name);
         }
    }

    @isTest
    public static void testGetGroup() {
        User testUser = [Select Id, username from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> groupIds = new list <Id>();
        String groupId =  [Select Id from CollaborationGroup where Name = 'Test Chatter Group1' limit 1].Id;
        
        CollaborationGroupMember member = new CollaborationGroupMember(CollaborationGroupId = groupId, MemberId=userinfo.getUserId());
        system.debug('groupId::'+groupId+'testUser::'+testUser.username);
        insert member;
        //system.runAs(testUser) {
            Peak_OnboardingController.OnboardingGroupWrapper groupWrapper = Peak_OnboardingController.getGroup(groupId);
            system.assert(groupWrapper.id == groupId);
        //}
    }
    @isTest
    public static void testGetOnboardingComplete() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        testUser.Onboarding_Complete__c = True;
        update testUser;
        system.runAs(testUser) {
            User testUser1 = Peak_OnboardingController.getUserRecord();
            system.assert(testUser1.Onboarding_Complete__c == True);
        }
    }
    
    @isTest
    public static void testUpdateUserNames() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        testUser.FirstName ='TestChange';
        //system.runAs(testUser) {
            Peak_OnboardingController.updateUserNames(testUser);
            User testUserName = [Select Id, FirstName from User where Email = 'standarduser@peak.com' Limit 1];
            system.assert(testUserName.FirstName == 'TestChange');
        //}
    }
    @isTest
    public static void testUpdatePreferences() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        system.runAs(testUser) {
            Peak_OnboardingController.updatePreferences(False);
            NetworkMember testUserName = [SELECT PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :testUser.Id Limit 1];
            system.assert(testUserName.PreferencesDisableAllFeedsEmail == True);
            Peak_OnboardingController.updatePreferences(True);
            NetworkMember testUserName2 = [SELECT PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE MemberId = :testUser.Id Limit 1];
            system.assert(testUserName2.PreferencesDisableAllFeedsEmail == False);
        }
    }
    @isTest
    public static void testInsertGroupMember() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
        system.debug('testUser:' + testUser.Id);
        list <CollaborationGroup> testGroups = [Select Id from CollaborationGroup where Name = 'Test Chatter Group0'
        OR Name = 'Test Chatter Group1'
        OR Name = 'Test Chatter Group2'
        OR Name = 'Test Chatter Group3'];

        //system.runAs(testUser) {
            Peak_OnboardingController.insertGroupMember(testGroups[0].Id,  True);

            list <CollaborationGroupMember> memberList = [SELECT Id
            FROM CollaborationGroupMember
            WHERE memberId = :userinfo.getUserId()];

            system.assert(memberList.size() == 1);
        //}
    }
    @isTest
    public static void testInsertGroupMemberRemove() {
        User testUser = [Select Id, Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];

        list <CollaborationGroupMember> testMembers = new list <CollaborationGroupMember>();

        for (CollaborationGroup grp : [Select Id from CollaborationGroup where Name = 'Test Chatter Group0'
        OR Name = 'Test Chatter Group1'
        OR Name = 'Test Chatter Group2'
        OR Name = 'Test Chatter Group3']){
            CollaborationGroupMember member = new CollaborationGroupMember(CollaborationGroupId = grp.Id, MemberId=Userinfo.getUserId());
            testMembers.add(member);
        }

        insert testMembers;
        
         list <network> testnetworkList = [SELECT Id
        FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        CollaborationGroup colGroup = new CollaborationGroup(Name='All MercerForce Users',CollaborationType = 'Public', CanHaveGuests = false,
                        isArchived = false, isAutoArchiveDisabled = false, networkId = networkId );
        insert colGroup;     
            

        //system.runAs(testUser) {
            Peak_OnboardingController.insertGroupMember(testMembers[0].CollaborationGroupId, false);

            list <CollaborationGroupMember> memberList = [SELECT Id
            FROM CollaborationGroupMember
            WHERE memberId = :testUser.Id];

            system.assert(memberList.size() == 0);
        //}
    }
    @isTest
    public static void testCompleteSlide() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        system.runAs(testUser) {
            Peak_OnboardingController.completeSlide('Welcome');
            Peak_OnboardingController.completeSlide('Profile');
            test.startTest();
            Peak_OnboardingController.completeSlide('Topic');
            Peak_OnboardingController.completeSlide('Group');
            test.stopTest();
            User testUserName = [Select  Onboarding_Complete__c from User where Email = 'standarduser@peak.com' Limit 1];
            //system.assert(testUserName.Onboarding_Complete__c == True);
        }
    }
    @isTest
    public static void testGetGroups() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        list <Id> groupIds = new list <Id>();
        for (CollaborationGroup grp: [Select Id from CollaborationGroup where Name = 'Test Chatter Group0' OR Name = 'Test Chatter Group1']){
            groupIds.add(grp.Id);
        }

        CollaborationGroupMember member = new CollaborationGroupMember(CollaborationGroupId = groupIds[0], MemberId=Userinfo.getUserId());
        insert member;
        //system.runAs(testUser) {
            list <Peak_OnboardingController.OnboardingGroupWrapper> groupWrapper = Peak_OnboardingController.getGroups(groupIds);
            system.assert(groupWrapper.size() == 2);
        //}
    }
    @isTest
    public static void testFollowTopic() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;

        system.runAs(testUser) {
            Peak_OnboardingController.followTopic(topicId);
            list <EntitySubscription> userFollow = [SELECT Id FROM EntitySubscription WHERE parentId = :topicId AND subscriberId = :testUser.Id limit 1];
            system.assert(userFollow.size() == 1);
        }
    }
    @isTest
    public static void testRemoveGroupMember() {
        User testUser = [Select Id from User where Email = 'standarduser@peak.com' Limit 1];
        CollaborationGroup testGroup = [Select Id from CollaborationGroup where Name = 'Test Chatter Group3' Limit 1];
        CollaborationGroupMember member = new CollaborationGroupMember(CollaborationGroupId = testGroup.Id, MemberId=UserInfo.getUserId());
        insert member;
        
         list <network> testnetworkList = [SELECT Id
        FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        CollaborationGroup colGroup = new CollaborationGroup(Name='All MercerForce Users',CollaborationType = 'Public', CanHaveGuests = false,
                        isArchived = false, isAutoArchiveDisabled = false, networkId = networkId );
        insert colGroup;  

        //system.runAs(testUser) {
            Peak_OnboardingController.removeGroupMember(testGroup.Id);
            list <CollaborationGroupMember> userFollow = [SELECT Id FROM CollaborationGroupMember WHERE MemberId = :testUser.Id];
            system.assert(userFollow.size() == 0);
        //}
    }
    @isTest
    public static void testUnfollowTopic() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Id networkId1 = Network.getNetworkId();
            //[SELECT Id FROM Network limit 1].Id;
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;
        EntitySubscription topicFollow = new EntitySubscription(networkId = networkId1, parentId = topicId, subscriberId = testUser.Id);
        insert topicFollow;

        //system.runAs(testUser) {
            Peak_OnboardingController.unfollowTopic(topicId);
            list <EntitySubscription> userFollow = [SELECT Id FROM EntitySubscription WHERE parentId = :topicId AND subscriberId = :testUser.Id limit 1];
            system.assert(userFollow.size() == 0);
        //}
    }
     @isTest
    public static void testGetFollowersAndPostsForTopic() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Id networkId1 = Network.getNetworkId();
//[SELECT Id FROM Network limit 1].Id;
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;
        EntitySubscription topicFollow = new EntitySubscription(networkId = networkId1, parentId = topicId, subscriberId = testUser.Id);
        insert topicFollow;
            List<Integer> result=Peak_OnboardingController.getFollowersAndPostsForTopic(topicId);
            system.assert(result.size() == 2);
    }
    @isTest
    public static void testTopicsUsersIsActiveIn() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Id networkId1 = Network.getNetworkId();
//[SELECT Id FROM Network limit 1].Id;
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;
        EntitySubscription topicFollow = new EntitySubscription(networkId = networkId1, parentId = topicId, subscriberId = testUser.Id);
        insert topicFollow;

            list <EntitySubscription> lstSubsc=Peak_OnboardingController.topicsUsersIsActiveIn();
            system.assert(lstSubsc.size() >0);
    }
    @isTest
    public static void testUpsertUserProfile() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Peak_OnboardingController.upsertUserProfile('iFirstName','iLastName','Automotive', 'iRole', 'Human Resources',
                                                                                           'North America');
    }
    
     @isTest
    public static void testGetMercerExperts() {
        User testUser = [SELECT Id FROM User WHERE Email = 'mercerExpert@TestClass.com' LIMIT 1];
        Mercer_Experts__c mercerExperts = Peak_OnboardingController.getMercerExperts(testUser.Id);
         System.assertEquals(testUser.Id, mercerExperts.Expert_User_Name__c);
         mercerExperts = Peak_OnboardingController.getMercerExperts(null);
         System.assertEquals(new Mercer_Experts__c(), mercerExperts);
    }
    
      @isTest
    public static void testUpsertMercerExperts() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Peak_OnboardingController.upsertMercerExperts(testUser.Id, true);
    }
      @isTest
    public static void testGetTopics() {
        User testUser = [Select Id from User where id=:userinfo.getUserId() Limit 1];
        Id networkId1 = Network.getNetworkId();
//[SELECT Id FROM Network limit 1].Id;
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;
        EntitySubscription topicFollow = new EntitySubscription(networkId = networkId1, parentId = topicId, subscriberId = testUser.Id);
        insert topicFollow;
        
        Peak_OnboardingController.getTopics(new List<Id>{topicId});
    }

    @IsTest
    public static void followMercerExpertsTest(){
        List<Mercer_Experts__c> mercerExperts = new List<Mercer_Experts__c>([SELECT Id, Expert_User_Name__c FROM Mercer_Experts__c]);
        List<EntitySubscription> entitySubscriptions = new List<EntitySubscription>();

        Test.startTest();
            for (Mercer_Experts__c mercerExpert :mercerExperts) {
                Peak_OnboardingController.followMercerExperts(mercerExpert.Id,true,null,null,null,null,null,null);
                entitySubscriptions = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE
                            SubscriberId = :UserInfo.getUserId() AND   ParentId = :mercerExpert.Id LIMIT 1];
                //System.assertEquals(1,entitySubscriptions.size());

                Peak_OnboardingController.followMercerExperts(mercerExpert.Id,true, mercerExpert.Id,false,mercerExpert.Id,null,null,null);
                entitySubscriptions = [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE
                        SubscriberId = :UserInfo.getUserId() AND   ParentId = :mercerExpert.Id LIMIT 1];
                //System.assertEquals(0,entitySubscriptions.size());

            }
        Test.stopTest();
    }

    @isTest
    public static void testGetPicklistOptions() {
        User testUser = new User();
        testUser = [SELECT Id FROM User WHERE LastName = :Peak_TestConstants.LASTNAME LIMIT 1];
        System.runAs(testUser) {
            List<String> stringList = Peak_OnboardingController.getPicklistOptions('User.Functional_Business_Area__c');
            System.assertNotEquals(null, stringList, 'The collection should be instantiated');
            System.assert(!stringList.isEmpty(), 'The collection should be populated');
        }
    }

    @isTest
    public static void testMercerExpertsByTopic() {
        User testUser = new User();
        testUser = [SELECT Id FROM User WHERE LastName = :Peak_TestConstants.LASTNAME LIMIT 1];
        String topicId =  [Select Id from Topic where Name = 'Test Topic 0' limit 1].Id;

        Test.startTest();
        Peak_Response saveResponse = Peak_OnboardingController.saveMercerExpertsByTopic(topicId,testUser.Id);
        System.assert(saveResponse.success);
        Peak_Response getResponse = Peak_OnboardingController.getMercerExpertsByTopic(topicId);
        System.assert(getResponse.success);
        Mercer_Expert_Topic__c expertTopic = (Mercer_Expert_Topic__c) getResponse.results[0];
        System.assertEquals(1,getResponse.results.size());
        saveResponse = Peak_OnboardingController.saveMercerExpertsByTopic(topicId,testUser.Id);
        System.assert(saveResponse.success);
        Peak_Response deleteResponse = Peak_OnboardingController.deleteMercerExpertsByTopic(topicId,(expertTopic.Mercer_Experts__c));
        System.assert(deleteResponse.success);
        saveResponse = Peak_OnboardingController.saveMercerExpertsByTopic(null,null);
        System.assert(!saveResponse.success);
        Test.stopTest();

    }


}