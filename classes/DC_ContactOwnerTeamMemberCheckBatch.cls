/*
Purpose:  This Apex class implements Batchable to insert the missing contact team member records for contact owners.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   06/7/2014  Added Comments.
============================================================================================================================================== 
*/
 global class DC_ContactOwnerTeamMemberCheckBatch implements Database.Batchable<sObject>, Database.Stateful {
     
    //List to do error Logging
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // this method will return 50 million rows
        return Database.getQueryLocator(Label.DC_contactTeamCheck_query); 
 
    }   
    
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */ 
    global void execute(Database.BatchableContext bc, List<Contact> scope)
    {
        Integer errorCount = 0;
        Integer errorCountTM = 0;
        Set<Id> ConIdSet = new Set<Id>();
        Set<Id> OwneridSet = new Set<Id>();
        Map<Id,set<Id>> conTeamMemMap = new Map<Id,set<Id>>();
        Map<Id,User> Userinfo = new Map<Id,User>();
        List<Contact_Team_Member__c> tempObjList = new List<Contact_Team_Member__c>();
        
        for (Contact con : scope)
        {
            ConIdSet.add(con.id);
            OwneridSet.add(con.ownerid);
            
        }
        
        Set<Id> conShareUsers = new Set<Id>();
        Userinfo = new Map<id,user>([select id,First_Name__c,Last_name__c,isActive,UserRole.Name from User where id in:owneridset]);
        //find out if all
        for(Contact_Team_Member__c cs: [select ContactTeamMember__c,Contact__c from Contact_Team_Member__c where contact__c in :ConIdset])
        {
            if(!conTeamMemMap.containsKey(cs.Contact__c))
                conTeamMemMap.put(cs.Contact__c,new Set<Id>{cs.ContactTeamMember__c});
            else {
                set<Id> conset = conTeamMemMap.get(cs.Contact__c);
                conset.add(cs.ContactTeamMember__c);
                conTeamMemMap.put(cs.Contact__c,conset);
            }
        }
        
        for (Contact con : scope)
        {
            if(!conTeamMemMap.containsKey(con.id) || (conTeamMemMap.containsKey(con.id) && !conTeamMemMap.get(con.id).contains(con.ownerid))){
                if(userInfo.get(con.ownerID).isActive){
                    Contact_Team_Member__c tempObj = new Contact_Team_Member__c();
                    tempObj.Contact__c = con.id;
                    tempObj.ContactTeamMember__c = con.ownerid;
                    tempObj.First_Name__c = userInfo.get(con.ownerID).First_Name__c;
                    tempObj.Last_Name__c = userInfo.get(con.ownerID).Last_Name__c;
                    tempObj.User_ID__c = userInfo.get(con.ownerID).ID;
                    tempObj.Role__c = userInfo.get(con.ownerID).UserRole.Name;
                    tempObjList.add(tempObj); //insert contact team member record in list 
                }
            }
               
        }
        
        if(tempObjList.size()>0){
            integer i = 0;
            for(Database.Saveresult result : database.insert(tempObjList,false))
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                    errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC21:' + result.getErrors()[0].getMessage() , errorLogs, tempObjList[i].Id);  
                    errorCount++;
                }
                i++;
            }
            tempObjList.clear();
        }
        
        
        
    }
    
     

    /*
     *  Method Name: finish(Insert error logs in to Batch Error Logger object after the completion of all records)
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */     
    global void finish(Database.BatchableContext bc)
    {
        //insert error records if any
        if(errorLogs.size()>0) 
            Database.insert(errorLogs,false) ; 
    }     
}