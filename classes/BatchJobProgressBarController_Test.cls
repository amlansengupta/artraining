@isTest
public class BatchJobProgressBarController_Test {
	
    static testMethod void setup(){
        
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
        
       	Test.startTest(); 
       	
	       	List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
			 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		         fc.name = '2018';
			 FCST_Fiscal_Year_List__c fc1 = new FCST_Fiscal_Year_List__c();
		         fc1.name = '2019';
				 
		     fsclYrLst.add(fc);
			 fsclYrLst.add(fc1);
		     insert fsclYrLst;
		     
		     List<Account> accListInsert = new List<Account>();
			 for(integer i=0;i<50;i++){
				 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
				 accListInsert.add(Acc);
			 } 
			
			 insert accListInsert;
			
			 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
			 for(Account accObj:accListInsert){
				 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
				 Growth_Plan__c gpObj1 = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[1].id);
				 gpListInsert.add(gpObj);
				 gpListInsert.add(gpObj1);
			 } 
			
			 insert gpListInsert;
			 
	        classObj.GP_batchStatus= 'finished';
	        classObj.CBI_batchStatus= 'finished';   
	        classObj.CA_batchStatus= 'finished';
	        classObj.ACBS_batchStatus= 'finished';
	        classObj.SO_batchStatus= 'finished';
	        classObj.SS_batchStatus= 'finished';
	        classObj.ST_batchStatus= 'finished';
	        classObj.SW_batchStatus= 'finished';
	        classObj.GandA_batchStatus= 'finished';
	        
	         classObj.Start_CBI_BatchJob();
	        classObj.Start_CA_BatchJob();
	        classObj.Start_ACBS_BatchJob();
	        classObj.Start_SO_BatchJob();
	        classObj.Start_SS_BatchJob();
	        classObj.Start_ST_BatchJob();
	        classObj.Start_SW_BatchJob();
	        classObj.Start_GP_BatchJob();
	        classObj.Start_GAndA_BatchJob();
	       	
	       	BatchToCopyCBIDataToNextYear acc1=new BatchToCopyCBIDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CBI_Id=database.executebatch(acc1,100);
	        
	        BatchToCopyCADataToNextYear acc2=new BatchToCopyCADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CA_Id=database.executebatch(acc2,100); 
	        
	        BatchToCopyACBSDataToNextYear acc3=new BatchToCopyACBSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ACBS_Id=database.executebatch(acc3,100); 
	        
	        BatchToCopySODataToNextYear acc4=new BatchToCopySODataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SO_Id=database.executebatch(acc4,100);
	        
	        BatchToCopySSDataToNextYear acc5=new BatchToCopySSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SS_Id=database.executebatch(acc5,100);
	        
	        BatchToCopySTDataToNextYear acc6=new BatchToCopySTDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ST_Id=database.executebatch(acc6,100);
	        
	        BatchToCopySWDataToNextYear acc7=new BatchToCopySWDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SW_Id=database.executebatch(acc7,100);        
	        
	        BatchToCopyGPDataToNextYear acc8=new BatchToCopyGPDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GP_Id=database.executebatch(acc8,100);
	        
	        BatchToCopyGandADataToNextYear acc9=new BatchToCopyGandADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GandA_Id=database.executebatch(acc9,100);
	                
	                                 
	        classObj.fromSelectedYear='2018';
	        classObj.planningyearchange();
	        classObj.getShowProgressBar();
	        classObj.getFromPlanningYears();
	        classObj.getToPlanningYears();
	        
	        classObj.getCBIJobs();
	        classObj.getCAJobs();
	        classObj.getACBSJobs();
	        classObj.getSOJobs();
	        classObj.getSSJobs();
	        classObj.getSTJobs();
	        classObj.getSWJobs();
	        classObj.getGPJobs();
	        classObj.getGAndAJobs();
	        
	        classObj.update_CBI_Progress();
	        classObj.update_CA_Progress();
	        classObj.update_ACBS_Progress();
	        classObj.update_SO_Progress();
	        classObj.update_SS_Progress();
	        classObj.update_ST_Progress();
	        classObj.update_SW_Progress();
	        classObj.update_GP_Progress();
	        classObj.update_GAndA_Progress();
        
        
        Test.stopTest();
    }
   static testMethod void setup2(){
         
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
       
       	Test.startTest(); 
		 
	        classObj.GP_batchStatus='not_started';
	        classObj.CBI_batchStatus='not_started';    
	        classObj.CA_batchStatus='not_started';
	        classObj.ACBS_batchStatus='not_started';
	        classObj.SO_batchStatus='not_started';
	        classObj.SS_batchStatus='not_started';
	        classObj.ST_batchStatus='not_started';
	        classObj.SW_batchStatus='not_started';
	        classObj.GandA_batchStatus='not_started';
	            
	        classObj.fromSelectedYear='2018';
	        classObj.planningyearchange();
	        classObj.getShowProgressBar();
	        classObj.getFromPlanningYears();
	        classObj.getToPlanningYears();
	        
	        classObj.Start_CBI_BatchJob();
	        classObj.Start_CA_BatchJob();
	        classObj.Start_ACBS_BatchJob();
	        classObj.Start_SO_BatchJob();
	        classObj.Start_SS_BatchJob();
	        classObj.Start_ST_BatchJob();
	        classObj.Start_SW_BatchJob();
	        classObj.Start_GP_BatchJob();
	        classObj.Start_GAndA_BatchJob();
	       	
	        classObj.getCBIJobs();
	        classObj.getCAJobs();
	        classObj.getACBSJobs();
	        classObj.getSOJobs();
	        classObj.getSSJobs();
	        classObj.getSTJobs();
	        classObj.getSWJobs();
	        classObj.getGPJobs();
	        classObj.getGAndAJobs();
	        
	        classObj.update_CBI_Progress();
	        classObj.update_CA_Progress();
	        classObj.update_ACBS_Progress();
	        classObj.update_SO_Progress();
	        classObj.update_SS_Progress();
	        classObj.update_ST_Progress();
	        classObj.update_SW_Progress();
	        classObj.update_GP_Progress();
	        classObj.update_GAndA_Progress();
        
        Test.stopTest();
    }
	static testMethod void setup3(){
         FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
	         fc.name = '2018';
		 insert fc;
		 
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
       	Test.startTest(); 
            
	        classObj.fromSelectedYear='2018';
	        classObj.planningyearchange();
	        classObj.getShowProgressBar();
	        classObj.getFromPlanningYears();
	        classObj.getToPlanningYears();
	        
	        BatchToCopyCBIDataToNextYear acc1=new BatchToCopyCBIDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CBI_Id=database.executebatch(acc1,100);
	        
	        BatchToCopyCADataToNextYear acc2=new BatchToCopyCADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CA_Id=database.executebatch(acc2,100); 
	        
	        BatchToCopyACBSDataToNextYear acc3=new BatchToCopyACBSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ACBS_Id=database.executebatch(acc3,100); 
	        
	        BatchToCopySODataToNextYear acc4=new BatchToCopySODataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SO_Id=database.executebatch(acc4,100);
	        
	        BatchToCopySSDataToNextYear acc5=new BatchToCopySSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SS_Id=database.executebatch(acc5,100);
	        
	        BatchToCopySTDataToNextYear acc6=new BatchToCopySTDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ST_Id=database.executebatch(acc6,100);
	        
	        BatchToCopySWDataToNextYear acc7=new BatchToCopySWDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SW_Id=database.executebatch(acc7,100);        
	        
	        BatchToCopyGPDataToNextYear acc8=new BatchToCopyGPDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GP_Id=database.executebatch(acc8,100);
	        
	        BatchToCopyGandADataToNextYear acc9=new BatchToCopyGandADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GandA_Id=database.executebatch(acc9,100);
	        
	       	classObj.GP_batchStatus= 'finished';
	        classObj.CBI_batchStatus= 'finished';    
	        classObj.CA_batchStatus= 'finished';
	        classObj.ACBS_batchStatus= 'finished';
	        classObj.SO_batchStatus= 'finished';
	        classObj.SS_batchStatus= 'finished';
	        classObj.ST_batchStatus= 'finished';
	        classObj.SW_batchStatus= 'finished';
	        classObj.GandA_batchStatus= 'finished';
	        
	        classObj.getCBIJobs();
	        classObj.getCAJobs();
	        classObj.getACBSJobs();
	        classObj.getSOJobs();
	        classObj.getSSJobs();
	        classObj.getSTJobs();
	        classObj.getSWJobs();
	        classObj.getGPJobs();
	        classObj.getGAndAJobs();
	        
	        classObj.update_CBI_Progress();
	        classObj.update_CA_Progress();
	        classObj.update_ACBS_Progress();
	        classObj.update_SO_Progress();
	        classObj.update_SS_Progress();
	        classObj.update_ST_Progress();
	        classObj.update_SW_Progress();
	        classObj.update_GP_Progress();
	        classObj.update_GAndA_Progress();
        
        Test.stopTest();
    }
    
	static testMethod void testCBI(){
        
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
        List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 List<Client_Business_Imperatives__c> insertList = new List<Client_Business_Imperatives__c>();
		 for(Growth_Plan__c gpObj:gpListInsert){
		 	Client_Business_Imperatives__c obj = new Client_Business_Imperatives__c();
		 	obj.Account__c=gpObj.Account__c;
		 	obj.Client_Business_Imperative__c='test';
		 	obj.Growth_Plan__c=gpObj.Id;
		 	obj.Our_Offering_Area__c='Health';
		 	//obj.Status__c='Not_Relevant';
		 	obj.To_Be_Proposed_Solution__c='testts';
		 	insertList.add(obj);
		 }
        
       	Test.startTest();
       	    insert insertList;
	       	classObj.CBI_batchStatus= 'processing';  
	        classObj.getShowProgressBar();
	        BatchToCopyCBIDataToNextYear acc1=new BatchToCopyCBIDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CBI_Id=database.executebatch(acc1,100);
	        classObj.getCBIJobs();
	        classObj.update_CBI_Progress();
        Test.stopTest();
        classObj.getCBIJobs();
	        classObj.update_CBI_Progress();
    }
    
    static testMethod void testCA(){
         
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
         List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 
		 List<Competitive_Assessment__c> insertNewCAObjList = new List<Competitive_Assessment__c>();

		for(Growth_Plan__c gpObj : gpListInsert){
				Competitive_Assessment__c caNewObj = new Competitive_Assessment__c();
				caNewObj.Account__c = gpObj.Account__c;
				caNewObj.Growth_Plan__c =gpObj.Id;
				caNewObj.Area__c = 'Health';
				caNewObj.Account_Footprint_And_Strengths__c = 'test foot';
				caNewObj.Account_s_Perception_of_Competitor__c = 'test comp';
				caNewObj.Competitor__c = 'test comp';
				caNewObj.Estimated_Wallet_Share__c = '0-25%';
				caNewObj.Their_Key_Executive_Sponsor_s__c= 'test song';
				insertNewCAObjList.add(caNewObj);
		}
		insert insertNewCAObjList;
		 
       	Test.startTest(); 
	        classObj.CA_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopyCADataToNextYear acc2=new BatchToCopyCADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_CA_Id=database.executebatch(acc2,100); 
	        classObj.getCAJobs();
	        classObj.update_CA_Progress();
        Test.stopTest();
        classObj.getCAJobs();
	        classObj.update_CA_Progress();
    }
    
    static testMethod void testACBS(){
    	
    	 List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 
		 List<AccountAlignmentClient__c> insertNeWACBSObjList = new List<AccountAlignmentClient__c>();
		for(Growth_Plan__c gpObj : gpListInsert){
				AccountAlignmentClient__c aCBSNewObj = new AccountAlignmentClient__c();
				aCBSNewObj.Account__c = gpObj.Account__c;
				aCBSNewObj.Growth_Plan__c =gpObj.Id;
				aCBSNewObj.Actions_for_Improving_relationships__c = 'Health';
				aCBSNewObj.Buying_Center__c = 'test foot';
				aCBSNewObj.Buying_Center_Decision_Maker_Customer__c = 'test comp';
				aCBSNewObj.Decision_Criteria_Preferences__c = 'test comp';
				aCBSNewObj.Decision_Maker_Customer__c = '0-25%';
				aCBSNewObj.Our_Alignment__c= 'test song';
				//aCBSNewObj.Relationship_Status__c= 'Sponsor / Very Positive';
				insertNeWACBSObjList.add(aCBSNewObj);
			
		}
		insert insertNeWACBSObjList;
        
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
       	Test.startTest(); 
	        classObj.ACBS_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopyACBSDataToNextYear acc2=new BatchToCopyACBSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ACBS_Id=database.executebatch(acc2,100); 
	        classObj.getACBSJobs();
	        classObj.update_ACBS_Progress();
        Test.stopTest();
        classObj.getACBSJobs();
	        classObj.update_ACBS_Progress();
    }
    
    static testMethod void testSO(){
         
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
        
        
         List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 List<Swot_Opportunity__c> insertNeWSOObjList = new List<Swot_Opportunity__c>();
		 for(Growth_Plan__c gpObj : gpListInsert){
				Swot_Opportunity__c SONewObj = new Swot_Opportunity__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Opportunity__c = 'Health';
				insertNeWSOObjList.add(SONewObj);
		 }
		
		 
        
		 
       	Test.startTest();
       	insert insertNeWSOObjList; 
	        classObj.SO_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopySODataToNextYear acc2=new BatchToCopySODataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SO_Id=database.executebatch(acc2,100); 
	        classObj.getSOJobs();
	        classObj.update_SO_Progress();
        Test.stopTest();
        classObj.getSOJobs();
	        classObj.update_SO_Progress();
    }
    
    static testMethod void testSS(){
         
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
         List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 
		 List<Swot_Strength__c> insertNeWSSObjList = new List<Swot_Strength__c>();
		for(Growth_Plan__c gpObj : gpListInsert){
				Swot_Strength__c SONewObj = new Swot_Strength__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Strength__c = 'Health';
				insertNeWSSObjList.add(SONewObj);
		}
		
        
       	Test.startTest(); 
       	insert insertNeWSSObjList;
	        classObj.SS_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopySSDataToNextYear acc2=new BatchToCopySSDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SS_Id=database.executebatch(acc2,100); 
	        classObj.getSSJobs();
	        classObj.update_SS_Progress();
        Test.stopTest();
        classObj.getSSJobs();
	        classObj.update_SS_Progress();
    }
    
    static testMethod void testST(){
    	BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
    	 List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 
         
        
        
        List<Swot_Threat__c> insertNeWSTObjList = new List<Swot_Threat__c>();
		for(Growth_Plan__c gpObj : gpListInsert){
				Swot_Threat__c SONewObj = new Swot_Threat__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Threat__c = 'Health';
				insertNeWSTObjList.add(SONewObj);
		}
		
       	Test.startTest(); 
       	insert insertNeWSTObjList;
	        classObj.ST_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopySTDataToNextYear acc2=new BatchToCopySTDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_ST_Id=database.executebatch(acc2,100); 
	        classObj.getSTJobs();
	        classObj.update_ST_Progress();
        Test.stopTest();
        classObj.getSTJobs();
	        classObj.update_ST_Progress();
    }	
    
    static testMethod void testSW(){
        
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
         List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		     fc.name = '2018';
		
		 fsclYrLst.add(fc);
		 
		 insert fsclYrLst;
		 
		 List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<5;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 } 
		
		 insert accListInsert;
		
		 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id);
			 gpListInsert.add(gpObj);
			
		 } 
		
		 insert gpListInsert;
		 
		 List<Swot_Weak__c> insertNeWSWObjList = new List<Swot_Weak__c>();
		for(Growth_Plan__c gpObj : gpListInsert){
				Swot_Weak__c SONewObj = new Swot_Weak__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Weakness__c = 'Health';
				insertNeWSWObjList.add(SONewObj);
			
		}
		
        
       	Test.startTest();
       	insert insertNeWSWObjList; 
	        classObj.SW_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopySWDataToNextYear acc2=new BatchToCopySWDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_SW_Id=database.executebatch(acc2,100); 
	        classObj.getSWJobs();
	        classObj.update_SW_Progress();
        Test.stopTest();
         classObj.getSWJobs();
	        classObj.update_SW_Progress();
    }
     static testMethod void testGP(){
    	
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
       	Test.startTest(); 
			 List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
			 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
		         fc.name = '2018';
			 FCST_Fiscal_Year_List__c fc1 = new FCST_Fiscal_Year_List__c();
		         fc1.name = '2019';
				 
		     fsclYrLst.add(fc);
			 fsclYrLst.add(fc1);
		     insert fsclYrLst;
		    
		    List<Account> accListInsert = new List<Account>();
			 for(integer i=0;i<50;i++){
				 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
				 accListInsert.add(Acc);
			 } 
			
			 insert accListInsert;
			
			 List<Growth_Plan__c> gpListInsert = new List<Growth_Plan__c>();
			 for(Account accObj:accListInsert){
				 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id,Planning_Year__c='2018');
				 gpListInsert.add(gpObj);
			 } 
			
			 insert gpListInsert; 
	        classObj.GP_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopyGPDataToNextYear acc2=new BatchToCopyGPDataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GP_Id=database.executebatch(acc2,100); 
	        classObj.getGPJobs();
	        classObj.update_GP_Progress();
        Test.stopTest();
    }
    
  static testMethod void testGA(){
         
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
        
        
       	Test.startTest(); 
	        classObj.GandA_batchStatus= 'processing';
	        classObj.getShowProgressBar();
	        BatchToCopyGandADataToNextYear acc2=new BatchToCopyGandADataToNextYear(fromSelectedYear,toSelectedYear);
	        classObj.batch_GandA_Id=database.executebatch(acc2,100); 
	        classObj.getGAndAJobs();
	        classObj.update_GAndA_Progress();
        Test.stopTest();
    }
    
      static testMethod void testAllClass(){
         
        
        BatchJobProgressBarController classObj = new BatchJobProgressBarController();
        String fromSelectedYear='2018',toSelectedYear='2019';
        
       	Test.startTest(); 
		 List<FCST_Fiscal_Year_List__c> fsclYrLst = new List<FCST_Fiscal_Year_List__c>();
		 FCST_Fiscal_Year_List__c fc = new FCST_Fiscal_Year_List__c();
	         fc.name = '2018';
		 FCST_Fiscal_Year_List__c fc1 = new FCST_Fiscal_Year_List__c();
	         fc1.name = '2019';
			 
	     fsclYrLst.add(fc);
		 fsclYrLst.add(fc1);
	     insert fsclYrLst;
	     
	     List<Account> accListInsert = new List<Account>();
		 for(integer i=0;i<50;i++){
			 Account Acc = new Account(name = 'Test Account'+i,BillingCountry='test');
			 accListInsert.add(Acc);
		 }
		 insert accListInsert;
		 
		 List<Growth_Plan__c> gp18ListInsert = new List<Growth_Plan__c>();
		 List<Growth_Plan__c> gp19ListInsert = new List<Growth_Plan__c>();
		 for(Account accObj:accListInsert){
			 Growth_Plan__c gpObj = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[0].id,Planning_Year__c='2018');
			 Growth_Plan__c gpObj1 = new Growth_Plan__c(Account__c = accObj.id,fcstPlanning_Year__c = fsclYrLst[1].id,Planning_Year__c='2019');
			 gp18ListInsert.add(gpObj);
			 gp19ListInsert.add(gpObj1);
		 }
		 insert gp18ListInsert; 
		 insert gp19ListInsert; 
		 
		 //CBI
		List<Client_Business_Imperatives__c> insertNewCBIObjList = new List<Client_Business_Imperatives__c>();

		for(Growth_Plan__c gpObj: gp18ListInsert){
			Client_Business_Imperatives__c cbiNewObj = new Client_Business_Imperatives__c();
			cbiNewObj.Account__c=gpObj.Account__c;
			cbiNewObj.Client_Business_Imperative__c='Test Client BUsiness';
			cbiNewObj.Growth_Plan__c=gpObj.Id;
			//cbiNewObj.Our_Offering_Area__c='Health';
			cbiNewObj.Status__c='Not_Relevant';
			//cbiNewObj.To_Be_Proposed_Solution__c='Active Exchange';
			insertNewCBIObjList.add(cbiNewObj);
		}
		insert insertNewCBIObjList;
        BatchToCopyCBIDataToNextYear acc1=new BatchToCopyCBIDataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_CBI_Id=database.executebatch(acc1,100);
        
		//CA
		List<Competitive_Assessment__c> insertNewCAObjList = new List<Competitive_Assessment__c>();

		for(Growth_Plan__c gpObj : gp18ListInsert){
				Competitive_Assessment__c caNewObj = new Competitive_Assessment__c();
				caNewObj.Account__c = gpObj.Account__c;
				caNewObj.Growth_Plan__c =gpObj.Id;
				caNewObj.Area__c = 'Health';
				caNewObj.Account_Footprint_And_Strengths__c = 'test foot';
				caNewObj.Account_s_Perception_of_Competitor__c = 'test comp';
				caNewObj.Competitor__c = 'test comp';
				caNewObj.Estimated_Wallet_Share__c = '0-25%';
				caNewObj.Their_Key_Executive_Sponsor_s__c= 'test song';
				insertNewCAObjList.add(caNewObj);
		}
		insert insertNewCAObjList;
        BatchToCopyCADataToNextYear acc2=new BatchToCopyCADataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_CA_Id=database.executebatch(acc2,100); 
        
        //ACBS
        List<AccountAlignmentClient__c> insertNeWACBSObjList = new List<AccountAlignmentClient__c>();
		for(Growth_Plan__c gpObj : gp18ListInsert){
				AccountAlignmentClient__c aCBSNewObj = new AccountAlignmentClient__c();
				aCBSNewObj.Account__c = gpObj.Account__c;
				aCBSNewObj.Growth_Plan__c =gpObj.Id;
				aCBSNewObj.Actions_for_Improving_relationships__c = 'Health';
				aCBSNewObj.Buying_Center__c = 'test foot';
				aCBSNewObj.Buying_Center_Decision_Maker_Customer__c = 'test comp';
				aCBSNewObj.Decision_Criteria_Preferences__c = 'test comp';
				aCBSNewObj.Decision_Maker_Customer__c = '0-25%';
				aCBSNewObj.Our_Alignment__c= 'test song';
				//aCBSNewObj.Relationship_Status__c= 'Sponsor / Very Positive';
				insertNeWACBSObjList.add(aCBSNewObj);
			
		}
		insert insertNeWACBSObjList;
        BatchToCopyACBSDataToNextYear acc3=new BatchToCopyACBSDataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_ACBS_Id=database.executebatch(acc3,100); 
        
        //SO
         List<Swot_Opportunity__c> insertNeWSOObjList = new List<Swot_Opportunity__c>();
		 for(Growth_Plan__c gpObj : gp18ListInsert){
				Swot_Opportunity__c SONewObj = new Swot_Opportunity__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Opportunity__c = 'Health';
				insertNeWSOObjList.add(SONewObj);
		 }
		 insert insertNeWSOObjList;
        BatchToCopySODataToNextYear acc4=new BatchToCopySODataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_SO_Id=database.executebatch(acc4,100); 
        
        //SS
        List<Swot_Strength__c> insertNeWSSObjList = new List<Swot_Strength__c>();
		for(Growth_Plan__c gpObj : gp18ListInsert){
				Swot_Strength__c SONewObj = new Swot_Strength__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Strength__c = 'Health';
				insertNeWSSObjList.add(SONewObj);
		}
		insert insertNeWSSObjList;
        BatchToCopySSDataToNextYear acc5=new BatchToCopySSDataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_SS_Id=database.executebatch(acc5,100);
        
        
        //ST
        List<Swot_Threat__c> insertNeWSTObjList = new List<Swot_Threat__c>();
		for(Growth_Plan__c gpObj : gp18ListInsert){
				Swot_Threat__c SONewObj = new Swot_Threat__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Threat__c = 'Health';
				insertNeWSTObjList.add(SONewObj);
		}
		insert insertNeWSTObjList;
        BatchToCopySTDataToNextYear acc6=new BatchToCopySTDataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_ST_Id=database.executebatch(acc6,100); 
        
        //SW
        List<Swot_Weak__c> insertNeWSWObjList = new List<Swot_Weak__c>();
		for(Growth_Plan__c gpObj : gp18ListInsert){
				Swot_Weak__c SONewObj = new Swot_Weak__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				SONewObj.Weakness__c = 'Health';
				insertNeWSWObjList.add(SONewObj);
			
		}
		insert insertNeWSWObjList;
        BatchToCopySWDataToNextYear acc7=new BatchToCopySWDataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_SW_Id=database.executebatch(acc7,100); 
       
       //G&A 
		 List<Goal_Relationship__c> insertNewGoalObjList = new List<Goal_Relationship__c>();
			for(Growth_Plan__c gpObj : gp18ListInsert){
				Goal_Relationship__c SONewObj = new Goal_Relationship__c();
				SONewObj.Account__c = gpObj.Account__c;
				SONewObj.Growth_Plan__c =gpObj.Id;
				
				SONewObj.Area__c = 'Health';
				SONewObj.Goal_Description__c = 'Health';
				SONewObj.Relationship_Status__c = 'Red';
				
				SONewObj.Risks_Barriers__c = 'Red';
				SONewObj.Executive_Status__c = 'Delayed';
				SONewObj.Executive_Name_And_Role__c = 'Red';
				
				insertNewGoalObjList.add(SONewObj);
			
		}
		insert insertNewGoalObjList;
		
		List<Goal_Action__c> insertNewActionObjList = new List<Goal_Action__c>();
		for(Goal_Relationship__c gpObj : insertNewGoalObjList){
				
				Goal_Action__c actionNewObj = new Goal_Action__c();
				actionNewObj.Action_Name__c = 'test Name';
				actionNewObj.Due_Date__c =date.today();
				actionNewObj.Goal_Relationship__c = gpObj.Id;
				actionNewObj.Priority__c = 'Low';
				actionNewObj.Status__c = 'Open';
				insertNewActionObjList.add(actionNewObj);
			
		}
		insert insertNewActionObjList;
        classObj.GandA_batchStatus= 'processing';
        classObj.getShowProgressBar();
        BatchToCopyGandADataToNextYear acc8=new BatchToCopyGandADataToNextYear(fromSelectedYear,toSelectedYear);
        classObj.batch_GandA_Id=database.executebatch(acc8,100); 
        classObj.getGAndAJobs();
        classObj.update_GAndA_Progress();
        
        Test.stopTest();
    }
}