/*
Purpose: This Apex class implements batchable interface
================================================================================================================================
History
 ------      VERSION     AUTHOR     DATE            DETAIL    
               1.0 -    Shashank    03/29/2013      Created Utility Class.
               1.1 -    Gyan        10/25/2013      Modified Class as per Req-3407
               1.2 -    Sarbpreet   11/19/2013      Dec Release#3512- Prevent “opps closing in the next week” reminders from being included in count of chatter posts dashboard tile
               1.3 -    Madhavi     11/10/2014      As per december Release 2014(Request #5079) ,created apex method "checkChatterPostsOnOpportunity"  to populate  date a user 
                                                    (other than MercerForce) posts chatter to the opportunity record.
               1.4 -   Sarbpreet  5/12/2015    As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.
               1.5 -    Keerthi     31/05/2019      Request 17500 : Using database. for DML operations whereever it is required
================================================================================================================================

*/
public class AP44_ChatterFeedReporting
{
    public static List<ChatterFeedReporting__c> chatterFeedList = new List<chatterFeedReporting__c>();
    public static Map<string , string> objectPrefixMap = new Map<string, string>();    
    private static final String EXCEPTION_STR = 'Exception occured with reason :' ;
    private static final String STR_Chatter_Free = 'Chatter Free' ;
    public static boolean FROMFEED = false;
    
    public AP44_ChatterFeedReporting()
    {
        setPrefixMap();
    }
    
    public static void setPrefixMap()
    {
        schema.DescribeSObjectResult accResult = Account.SObjectType.getDescribe();
        string accPrefix = accResult.getKeyPrefix();
        objectPrefixMap.put('Account', accPrefix);
        schema.DescribeSObjectResult conResult = Contact.SObjectType.getDescribe();
        string conPrefix = conResult.getKeyPrefix();
        objectPrefixMap.put('Contact', conPrefix);
        schema.DescribeSObjectResult oppResult = Opportunity.SObjectType.getDescribe();
        string oppPrefix = oppResult.getKeyPrefix();
        objectPrefixMap.put('Opportunity', oppPrefix);
        schema.DescribeSObjectResult useResult = User.SObjectType.getDescribe();
        string usePrefix = useResult.getKeyPrefix();
        objectPrefixMap.put('User', usePrefix);
        schema.DescribeSObjectResult groupResult = CollaborationGroup.SObjectType.getDescribe();
        string groupPrefix = groupResult.getKeyPrefix();
        objectPrefixMap.put('Group', groupPrefix);
    }
 /*
     *  Method Name: insertChatterFeed
     *  Description: Insert ChatterFeedReporting on insert of Chatter Feed.
     *  Arguements: List
     */    

    public static void insertChatterFeed(List<FeedItem> triggernew)
    {
        // clear cache
        chatterFeedList.clear();
        try
        {
            setPrefixMap();
            set<string> groupIdSet = new set<string>();
            Map<string, string> groupMap = new Map<string, string>();
            //As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.
            Set<ID> CreatedByIDSet = new Set<ID>();
            Set<ID> profileIDSet = new Set<ID>();
            String Userlicense;
            ID Userprofileid;
            
            
            for(FeedItem feed:triggernew)
            {
            	CreatedByIDSet.add(feed.CreatedByID);
            }             
            Map<id, User> userMap = new Map<id, User>([Select id, ProfileID from user where id IN :CreatedByIDSet]);
            
           for(User usr : userMap.values())
           {
           		profileIDSet.add(usr.ProfileID);
           }

			Map<id, Profile> profileMap = new Map<id, Profile>([select id, UserLicenseId, UserLicense.name  from profile where ID IN: profileIDSet]);
            //End of CA Incident #33609319
                                   
            for(FeedItem feed:triggernew)
            {
                string parent = feed.ParentId;
                boolean bypass = false;  
                string feedBody = feed.Body;              
                //As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.                           	
                if(userMap.containsKey(feed.CreatedByID))
                {
                	Userprofileid = userMap.get(feed.CreatedByID).ProfileID;
                	Userlicense = profileMap.get(Userprofileid).UserLicense.name;
                }
             
                
                if(feedBody <> null)
                {
                    //Dec Release#3512- Prevent “opps closing in the next week” reminders from being included in count of chatter posts dashboard tile
                    //As per CA Incident #33609319, modified logic to prevent adding those feeditems  which are created by Chatter Free users in ChatterFeedReporting__c object.     
                    if(feedBody.contains('New Opportunity created:') || feedBody.contains(Label.Opportunity_Close_Date_Reminder_Skip) || Userlicense == STR_Chatter_Free )
                    {
                        bypass = true;  
                    }
                }
                if((parent.substring(0,3)==objectPrefixMap.get('Account')
                   ||parent.substring(0,3)==objectPrefixMap.get('Contact')
                   ||parent.substring(0,3)==objectPrefixMap.get('Opportunity')
                   ||parent.substring(0,3)==objectPrefixMap.get('User')
                   ||parent.substring(0,3)==objectPrefixMap.get('Group'))
                   &&!bypass)
                {
                    if(parent.substring(0,3)==objectPrefixMap.get('Group'))
                    {
                        groupIdSet.add(feed.ParentId);
                    }
                    else
                    {
                        ChatterFeedReporting__c chatterFeed = new chatterFeedReporting__c();
                        chatterFeed.Name = feed.Id;
                        chatterFeed.Posted_By__c = feed.CreatedById;
                        chatterFeed.Post_Type__c = feed.Type;
                        
                        if(feed.Type == 'LinkPost')
                        {
                            chatterFeed.Post__c = feed.LinkUrl;
                        }
                        else
                        {
                            chatterFeed.Post__c = feed.Body;
                        }
                        
                        if(parent.substring(0,3)==objectPrefixMap.get('Account'))
                        {
                            chatterFeed.Account__c = feed.ParentId;
                            //chatterFeed.Chatter_Object__c = 'Account';
                        }
                        else if(parent.substring(0,3)==objectPrefixMap.get('Contact'))
                        {
                            chatterFeed.Contact__c = feed.ParentId;
                            //chatterFeed.Chatter_Object__c = 'Contact';
                        }
                        else if(parent.substring(0,3)==objectPrefixMap.get('Opportunity'))
                        {
                            chatterFeed.Opportunity__c = feed.ParentId;
                            //chatterFeed.Chatter_Object__c = 'Opportunity';
                        }
                        else if(parent.substring(0,3)==objectPrefixMap.get('User'))
                        {
                            chatterFeed.User__c = feed.ParentId;
                            //chatterFeed.Chatter_Object__c = 'User';
                        }
                        
                        chatterFeedList.add(chatterFeed);
                       }
                    }
                }
                
            
            
            for(CollaborationGroup grp:[select Id, Name from CollaborationGroup where ID IN:groupIdSet])
            {
                groupMap.put(grp.Id, grp.Name);
            }
            
            for(FeedItem feed:triggernew)
            {
                if(groupMap.containsKey(feed.ParentId))
                {
                    ChatterFeedReporting__c chatterFeed = new chatterFeedReporting__c();
                    chatterFeed.Name = feed.Id;
                    chatterFeed.Posted_By__c = feed.CreatedById;
                    chatterFeed.Post_Type__c = feed.Type;
                    
                    if(feed.Type == 'LinkPost')
                    {
                        chatterFeed.Post__c = feed.LinkUrl;
                    }
                    else
                    {
                       chatterFeed.Post__c = feed.Body;
                    }
                    
                    chatterFeed.Group__c = groupMap.get(feed.ParentId);
                    //chatterFeed.Chatter_Object__c = 'Group';
                    chatterFeedList.add(chatterFeed);
                }
            }
/*************************Request Id:17500 : Using database. for DML operations where ever it is required START********************************************/                          
Database.SaveResult[] srList = Database.insert(chatterFeedList, false);

// Iterate through each returned result
for (Database.SaveResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully inserted  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}
/*************************Request Id:17500 : Using database. for DML operations where ever it is required END********************************************/              
           // insert chatterFeedList;
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
            
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage()); 
            triggernew[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
          
        }
    }
     /*
     *  Method Name: deleteChatterFeed
     *  Description: Delete ChatterFeedReporting on delete of Chatter Feed.
     *  Arguements: List
     */
    public static void deleteChatterFeed(List<FeedItem> triggerold)
    {
        // clear cache
        chatterFeedList.clear();
        try
        {
            set<string> feedIdSet = new set<string>();
            for(FeedItem feed:triggerold)
            {
                feedIdSet.add(feed.Id);
            }
            
            for(ChatterFeedReporting__c chatterFeed:[select Id from ChatterFeedReporting__c where Name IN:feedIdSet])
            {
                chatterFeedList.add(chatterFeed);
            }
            
Database.deleteResult[] drList = Database.delete(chatterFeedList, false);

// Iterate through each returned result
for (Database.deleteResult dr : drList) {
    if (dr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully deleted  ' + dr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : dr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}

            //delete chatterFeedList;
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage()); 
            triggerold[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.');     
        }
    } 
    
     /*
     *  Method Name: restrictChatterFeedForAllMercerGroup
     *  Description: only below  User can make post to 'AllMercerForce Chatter Group
                     Julio Portalatin, Kerry Sain, Monica Ralli, Brittany Daoust, Tracie Doughty, Darren Clark and MercerForce 

(admin user). 
     *  Arguements: List
     */
    public static void restrictChatterFeedForAllMercerGroup(List<FeedItem> triggernew)
    {
        //Fetch all Users those are member of MercerForceChatterGroupAdmin Public Group.
        List<Group> g = new List<Group>() ;
        
        g = [Select g.Name, g.Id ,g.DeveloperName From Group g where g.DeveloperName = 'MercerForceChatterGroupAdmin' limit 1];
        
        if(g!=null && g.size()>0) { 
            List<GroupMember> lstGroupMember = [Select g.UserOrGroupId, g.GroupId From GroupMember g where g.GroupId =: g

[0].Id];
            
            //Create a Set of all users those having access to this group
            Set<String> setofUsershavingAccessToChatterGrp = new Set<String>();
            For(GroupMember gm:lstGroupMember)
            {
                setofUsershavingAccessToChatterGrp.add(gm.UserOrGroupId);
            }
           
            //Logic for post into Chatter Group.
            for(FeedItem feed:triggernew)
            {
                    if(!setofUsershavingAccessToChatterGrp.contains(feed.CreatedById))
                    {
                        feed.Body.addError(Label.Chatter_Feed_Error);
                    }                
            }
            setofUsershavingAccessToChatterGrp.clear();
            lstGroupMember.clear();
            }
        }
  /*
     *  Method Name: checkChatterPostsOnOpportunity
     *  Description: As per december Release 2014(Request #5079) ,this method  populates  date a user (other than MercerForce) posts chatter to the opportunity record.
     *  Arguements: List
     */
public static void checkChatterPostsOnOpportunity(List<feeditem> triggernew){
    String oppKeyPrefix = Opportunity.sObjectType.getDescribe().getKeyPrefix();
Set<id>  oppIds = new set<id>();
Map<Id, feeditem> feeditemmap =  new Map<id,feeditem>();
Set<Id> createdBy = new set<id>();
List<opportunity> lstopprecstoupdate= new List<opportunity>(); 
    for(feedItem f :triggernew){
        String parentId = f.parentId;
        if((parentId.startswith(oppKeyPrefix) && f.createdById!=Label.MercerForceUserid && f.Body!=null)){
        oppIds.add(f.parentId);
        feeditemmap.put(f.parentId,f);
        }
    }
    
    lstopprecstoupdate =[select id, Date_of_Last_Chatter_Post_by_User__c  from opportunity where id in :oppIds];
    if(!lstopprecstoupdate.isempty()){
        for(opportunity opp:lstopprecstoupdate){
            opp.Date_of_Last_Chatter_Post_by_User__c= feeditemmap.get(opp.id).createdDate.Date();
        }
        try{
            
Database.SaveResult[] srList = Database.update(lstopprecstoupdate, false);

// Iterate through each returned result
for (Database.SaveResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful
        System.debug('Successfully updated  ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('fields that affected this error: ' + err.getFields());
        }
    }
}

       // update lstopprecstoupdate;
        }catch (Exception e){
        	System.debug('---inside Exception--'+e);
            for(Integer i=0;i<triggernew.size();i++){              	
                	triggernew[i].addError(EXCEPTION_STR+e.getMessage());
            }
        }
    }
}
}