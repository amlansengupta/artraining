/*Purpose: This Apex class implements Schedulable interface to schedule AP13_ColleagueBatchable 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   12/28/2012  Created Apex Schedulable class
============================================================================================================================================== 
*/
global class AP12_ColleagueSchedulable implements Schedulable{

//Build the query string for the batch of records    
        global final string baseQuery1 = 'select Account_Market__c, Account_Region__c, Account_Sub_Market__c, Account_Sub_Region__c, Office_Code__c, Account_Country__c,';
        global final string baseQuery2 = ' Relationship_Manager__r.Id, Relationship_Manager__r.Market__c, Relationship_Manager__r.Region__c, Relationship_Manager__r.Sub_Market__c, Relationship_Manager__r.Sub_Region__c, Relationship_Manager__r.Name , Relationship_Manager__r.CountryGeo__c, Relationship_Manager__r.Office__c, Relationship_Manager__c from Account';
        global final string whereClause = ' where Relationship_Manager__r.isUpdated__c = true';
		global final string query;

	global AP12_ColleagueSchedulable()
	{
		this.query = baseQuery1 + baseQuery2 + whereClause;
	}

// schedulable execute method
    global void execute(SchedulableContext SC) {

//Call database execute method to run AP11 batch class        
         database.executebatch(new AP11_ColleagueBatchable(query),Integer.valueOf(system.label.CL52_ColleagueBatchSize));
     }
}