/*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
public class ExcludeUserUtility {
    
    public static boolean isExcludedUser(String userName){
        //String runningUserName = [Select Id, Name from User where Id =: userInfo.getProfileId()].get(0).Name;
        List<Audit_Excluded_Profiles__mdt> listAuditExcludedProfiles = [Select ExcludedProfiles__c from Audit_Excluded_Profiles__mdt];
        boolean isExcludedUser = false;
        if(listAuditExcludedProfiles != null){
            for(Audit_Excluded_Profiles__mdt profMetadata : listAuditExcludedProfiles){
                if(profMetadata.ExcludedProfiles__c != null && profMetadata.ExcludedProfiles__c.equalsIgnoreCase(userName)){
                    isExcludedUser = true;
                    break;
                }
            }
        }
        return isExcludedUser;    
    }
}