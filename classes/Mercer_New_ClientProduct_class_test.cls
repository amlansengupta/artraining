@isTest

public class Mercer_New_ClientProduct_class_test{

    private static testMethod void Mercer_New_ClientProductContact_class_testMethod(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = 'ABCDEF';
        //testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active';  
        testContact.email = 'pqr@xyz.com';        
        insert testContact;
        
        Talent_Contract__c testTc = new Talent_Contract__c();
        testTc.Account__c = testAccount.id;
        TestTc.Product__c = 'WIN ePRISM';
        insert TestTc;
                         
        
        Client_Product_Contact__c Testmod = new Client_Product_Contact__c();
        Testmod.Talent_Contract__c = testTc.id;
        Testmod.Contact__c = testContact.id;
        
        //insert Testmod;
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con = new ApexPages.StandardController(testmod);
        Mercer_New_ClientProductContact_class testM = new Mercer_New_ClientProductContact_class(con);
        
        PageReference pageRef = Page.Mercer_New_ClientProductContact;
        Test.setCurrentPage(pageRef);
        testM.objClient.Contact__c = testContact.id;
        testM.Save();
        
        Client_Product_Contact__c Testmod2 = new Client_Product_Contact__c();
        Testmod2.Talent_Contract__c = testTc.id;
        Testmod2.Contact__c = testContact.id;
        
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con2 = new ApexPages.StandardController(testmod2);
        Mercer_New_ClientProductContact_class testM2 = new Mercer_New_ClientProductContact_class(con2);
        testM2.objClient.Contact__c = testContact.id;
        testM2.SaveNew();
        
    
    }
    
    // Cover exceptions
    private static testMethod void Mercer_New_ClientProductContact_class_testMethod2(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code__c = 'ABCDEF';
        //testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active'; 
        testContact.email = 'pqr@xyz1.com';         
        insert testContact;
        
        Talent_Contract__c testTc = new Talent_Contract__c();
        testTc.Account__c = testAccount.id;
        TestTc.Product__c = 'WIN ePRISM';
        insert TestTc;
                         
        
        Client_Product_Contact__c Testmod = new Client_Product_Contact__c();
        Testmod.Talent_Contract__c = testTc.id;
        Testmod.Contact__c = testContact.id;
        
        //insert Testmod;
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con = new ApexPages.StandardController(testmod);
        Mercer_New_ClientProductContact_class testM = new Mercer_New_ClientProductContact_class(con);
        
        testM.Save();
        
        Client_Product_Contact__c Testmod2 = new Client_Product_Contact__c();
        Testmod2.Talent_Contract__c = testTc.id;
        Testmod2.Contact__c = testContact.id;
        
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con2 = new ApexPages.StandardController(testmod2);
        Mercer_New_ClientProductContact_class testM2 = new Mercer_New_ClientProductContact_class(con2);
        
        testM2.SaveNew();
        
    
    }
    
}