/*
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Arijit Roy     04/01/2013           Batch class for updating Account Status    
2.0         Jagan          05/22/14             Updated class to check if the status is not already same
============================================================================================================================================== 
*/
global class MS14_AccountCompetitorBatchable implements Database.Batchable<sObject>, Database.Stateful{
    
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
        'Recent Win'                  => 'Recent Win',
        'High Revenue'                => 'High Revenue',
        'Low Revenue'                 => 'Low Revenue',
        'Active Marketing Contact'    => 'Active Marketing Contact',
        'Active Contact'              => 'Active Contact',
        'Activities Exists'           => 'Activities Exists'        
    };
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    global static String query = 'Select Id, MercerForce_Account_Status__c From Account Where Competitor_Flag__c = true AND MercerForce_Account_Status__c  NOT IN' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keySet());
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    if(Test.isRunningTest())
        {
            String Name= 'Test Competitior' + String.valueOf(Date.Today());  
            
            query = 'Select Id, MercerForce_Account_Status__c From Account Where Competitor_Flag__c = true AND MercerForce_Account_Status__c  NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(new Set<String>{Name}); 
        }
        return Database.getQueryLocator(query); 
    }
    
     /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<String, Account> accountMapForUpdate = new Map<String, Account>();
        for(Account account :(List<Account>)scope)
        {
            if(account.MercerForce_Account_Status__c <> 'Competitor'){
                account.MercerForce_Account_Status__c = 'Competitor';
                accountMapForUpdate.put(account.Id, account);
            }
        }
        
        if(!accountMapForUpdate.isEmpty()) 
        {
        for(Database.Saveresult result : Database.update(accountMapForUpdate.values(), false))
        {
            
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }
            }
        } 
        }  
    
    
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
    try{
          Database.insert(errorLogs, false);
       }
       catch(DMLException dme)
       {
          system.debug('\n exception has occured');
       }finally
       {
          // TO DO: Error logging code logic to be added
        MercerAccountStatusBatchHelper.callNextBatch('Has No Activities');  
        }
        
        
    }
   
}