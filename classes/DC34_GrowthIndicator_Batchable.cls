/*Purpose:  This batch class will update Growth Plan indicator field for all existing Growth Plan records.
=============================================================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   08/27/2015    As part of September Release 2015(PMO# 6798) created batch class to calculate and update Growth Plan
   									indicator field for all existing Growth Plan records. 
============================================================================================================================================== 
*/
global class DC34_GrowthIndicator_Batchable implements Database.Batchable<sObject>, Database.Stateful {
  private String query; 
  public static final string constErrorStr1 = 'DC34_GrowthIndicator_Batchable: Error with Growth Plan updation : ';
  public static final string constErrorStr2 = 'DC34 Status :scope:';
  public static final string constErrorStr3 = ' updateGrowthPlanList:';
  public static final string constErrorStr4 = ' Total Errorlog Size: ';
  public static final string constErrorStr5 = ' Batch Error Count :';
  private Integer i = 0;

  // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
     /*
    *    Creation of Constructor
    */
    global DC34_GrowthIndicator_Batchable() {       
                
         String qry =  Label.Growth_Indicator_Query;
         this.query= qry; 
    }
    
    
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
    }
    
    
     /*
     *  Method Name: execute
     *  Description: Updating Sales Price USD calc field on existing opportunity products
     */
    global void execute(Database.BatchableContext BC, List<Growth_Plan__c> scope) {
        List<Growth_Plan__c> updateGrowthPlanList = new  List<Growth_Plan__c>();
        //List to hold Save Result 
        List<Database.SaveResult> grwthPlanSaveResult = new List<Database.SaveResult>();
        Integer errorCount = 0; 
        
        for(Growth_Plan__c gp : scope){
            gp.Growth_Indicator__c = gp.TOT_of_Revenue_Growth_Anticipated01__c;
            updateGrowthPlanList.add(gp);
        }
        
        if(updateGrowthPlanList.size()>0)
        {
            grwthPlanSaveResult = Database.update(updateGrowthPlanList,true);
            
             //Iterate for Error Logging
            for(Database.Saveresult result : grwthPlanSaveResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCount++;
                }
                
              } 
        }
        
        //Update the status of batch in BatchLogger object
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>(); 
         errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog(constErrorStr2 + scope.size() +
                         constErrorStr3 + updateGrowthPlanList.size() +     + constErrorStr4+errorLogs.size() +
                          constErrorStr5+errorCount  ,  errorLogStatus , scope[0].Id);    
         insert  errorLogStatus ;
        
        
        
    }
    
    
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC) {               
         
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  

    }
}