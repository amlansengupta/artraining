@isTest(SeeAllData=false) 
public class AccountTrigger_Test{
    public static testmethod void testAccountTrigger(){
        TriggerSettings__c TrgSetting1 = new TriggerSettings__c();
        TrgSetting1.name='Account Trigger';
        TrgSetting1.Object_API_Name__c='Account';
        TrgSetting1.Trigger_Disabled__c=false;
        insert TrgSetting1;
        User us=[select id from User where id=:UserInfo.getUserId()];
        User testUser = new User();
        System.runAs(us){         
        testUser.alias = 'tUser';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'lastName';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = [SELECT Id FROM Profile WHERE Name='System Administrator'][0].Id;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'test@xyz.com.testuser';
        //testUser.EmployeeNumber = Col.EMPLID__c;
        insert testUser;
        }
        
        Mercer_TestData mdata = new Mercer_TestData();
        Account accrec =  mdata.buildAccount();
        accrec.client_Retention__c = 100;
        accrec.Account_Region__c = 'International';
        accrec.ownerId=testUser.Id;
        update accrec;
        accrec.client_Retention__c = 50;
        accrec.Account_Region__c = 'North America';
        update accrec;
    }
    @isTest
    public static void testAccountTriggerHandler()
    {
        AccountTriggerHandler triggerHandler =new AccountTriggerHandler();
        
        TriggerSettings__c TrgSetting1 = new TriggerSettings__c();
        TrgSetting1.name='Account Trigger';
        TrgSetting1.Object_API_Name__c='Account';
        TrgSetting1.Trigger_Disabled__c=false;
        insert TrgSetting1;
        
        triggerHandler.IsDisabled();
        
        Colleague__c Col = new Colleague__c();
        Col.Name = 'TestColleague';
        Col.Last_Name__c = 'TestLastName';
        Col.EMPLID__c = '1233333902';
        Col.LOB__c = 'Health & Benefits';
        Col.Empl_Status__c = 'Active';
        Col.Email_Address__c = 'testcol@abc.com';
        insert Col;

        User testUser = new User();
        testUser.alias = 'tUser';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'lastName';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = [SELECT Id FROM Profile WHERE Name='System Administrator'][0].Id;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'test@xyz.com.testuser';
        testUser.EmployeeNumber = Col.EMPLID__c;
        insert testUser;
              
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName #';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = Col.Id;
        testAccount.One_Code__c='ABC123XYZ';
        testAccount.client_Retention__c = 10;
        testAccount.DUNS__c = '00789654';
        testAccount.GU_DUNS__c = '00789654';
        testAccount.One_Code_Status__c = 'Pending';
       // testAccount.Account_Office__c  = 'New York - 1166';
        testAccount.OwnerId = UserInfo.getUserId();
        testAccount.GDPR_Opt_Out__c = true;
        
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        testAccount1.Relationship_Manager__c = Col.Id;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.client_Retention__c = 15;
        testAccount1.GU_DUNS__c = '00789654';
        testAccount1.GDPR_Opt_Out__c = true;

        
        //SObject object1= testAccount;
        //SObject object2= testAccount1;
        
        List<Account> beforeInsertList=new List<Account>();
        List<Account> listAcc = new list<Account>();
        beforeInsertList.add(testAccount);
        beforeInsertList.add(testAccount1); 
        
        triggerHandler.BeforeInsert(beforeInsertList);
        listAcc.add(testAccount);
        listAcc.add(testAccount1);
        insert listAcc;
        
        
        system.debug('***testAccount1_OLD'+testAccount1.GDPR_Opt_Out__c);    
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.Email = 'test@test.com';
        con.AccountId = listAcc.get(0).Id;        
        insert con;
        
        List<Account> afterInsertList=new List<Account>();
        afterInsertList.add(testAccount);
        afterInsertList.add(testAccount1);  
        
        Map<Id, Account> afterInsertMap=new Map<Id, Account>();
        afterInsertMap.put(testAccount.id,testAccount);
        afterInsertMap.put(testAccount1.id,testAccount1);
        
        try{
            triggerHandler.AfterInsert(afterInsertList,afterInsertMap);
            }
            catch(DmlException e){}
        
        
        Map<Id, Account> beforeUpdateMap=new Map<Id, Account>();
        beforeUpdateMap.put(testAccount.id,testAccount);
        beforeUpdateMap.put(testAccount1.id,testAccount1);
        
        listAcc.get(0).client_Retention__c = 100;
        listAcc.get(0).Account_Region__c = 'International';
        listAcc.get(0).GDPR_Opt_Out__c = false;
        //update testAccount;
        
        listAcc.get(1).client_Retention__c = 50;
        listAcc.get(1).Account_Region__c = 'North America';
        listAcc.get(1).GU_DUNS__c = '00789655';
        listAcc.get(1).GDPR_Opt_Out__c = false;   
        Test.startTest();   
        update listAcc;
        
        system.debug('***testAccount1_NEW'+testAccount1.GDPR_Opt_Out__c);  
        //testAccount1.GDPR_Opt_Out__c = true;    
        //update testAccount1;
        //SObject object3 = testAccount;
        //SObject object4 = testAccount1;
        
        Map<Id, Account> afterUpdateMap=new Map<Id, Account>();
        afterUpdateMap.put(testAccount.id,testAccount);        
        afterUpdateMap.put(testAccount1.id,testAccount1);
        
        //Test.startTest();
        triggerHandler.BeforeUpdate(beforeUpdateMap, afterUpdateMap);
        triggerHandler.AfterUpdate(beforeUpdateMap, afterUpdateMap);
        Test.stopTest();
        triggerHandler.BeforeDelete(beforeUpdateMap);
        triggerHandler.AfterDelete(beforeUpdateMap);
        //Test.stopTest();
        triggerHandler.AfterUndelete(beforeUpdateMap);
    }
}