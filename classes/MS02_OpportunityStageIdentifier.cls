/*
Purpose: This Apex class implements Batchable for Account Status.
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR               DATE                 DETAIL    
1.0 -       Arijit               04/29/13             Created Apex Batch class
2.0 -       Jagan                05/22/14             Updated to check if the status is not same already
==============================================================================================================================================
 */
/*
==============================================================================================================================================
Request Id                                 Date                    Modified By
17601:Removing Pending Step                21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
global class MS02_OpportunityStageIdentifier implements Database.Batchable<sObject>, Database.Stateful{
    
    global Map<String, String> accountStatusMap = new Map<String, String>();	 
    /*********Request Id:17601;Removing Pending Step START*****************/
   /* global Static Map<String, String> oppStageMap = new Map<String, String>{
    	'Above the Funnel' => 'Above the Funnel',
        'Identify'         => 'Identify',
        'Active Pursuit'   => 'Active Pursuit',
        'Finalist'         => 'Finalist',
        'Selected'         => 'Selected',
        'Pending Step'     => 'Pending Step'
    }; */ 
        global Static Map<String, String> oppStageMap = new Map<String, String>{
    	'Above the Funnel' => 'Above the Funnel',
        'Identify'         => 'Identify',
        'Active Pursuit'   => 'Active Pursuit',
        //Request No. 17953 : Removing Finalist START   
        //'Finalist'         => 'Finalist',
        'Selected'         => 'Selected'        
    };
    /*********Request Id:17601;Removing Pending Step END*****************/
    global Set<String> accountUpdatedCache = new Set<String>();
    
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    global static String query = 'Select Id, StageName, AccountId,Account.MercerForce_Account_Status__c From Opportunity Where  StageName IN ' + MercerAccountStatusBatchHelper.quoteKeySet(oppStageMap.keySet()) + ' ORDER BY AccountId ';
    
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.      
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(Test.isRunningTest())
        {
            String oppname = 'Test Opportunity' + String.valueOf(Date.Today());  
            // + String.valueOf(Date.Today());
            query = 'Select Id, StageName, AccountId,Account.MercerForce_Account_Status__c From Opportunity Where Name IN ' + MercerAccountStatusBatchHelper.quoteKeySet(new Set<String>{oppname}); 
        }
        System.debug('\n Constructed query String :'+query);
        return Database.getQueryLocator(query); 
    }
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        List<Opportunity> opportunities = (List<Opportunity>)scope;
        Map<Id, Account> accForUpdateMap = new Map<Id, Account>();
        for(Opportunity opportunity : opportunities)
        {
            /*****************Request Id:17601;Removing Pending Step from the If condition START***************/
            /*if((opportunity.StageName.equalsIgnoreCase('Active Pursuit') 
                || opportunity.StageName.equalsIgnoreCase('Finalist') 
                || opportunity.StageName.equalsIgnoreCase('Selected') 
                || opportunity.StageName.equalsIgnoreCase('Pending Step')) && opportunity.account.MercerForce_Account_Status__c <> 'Open Opportunity - Stage 2+' )
            {
                //if(!accountUpdatedCache.contains(opportunity.AccountId))
                //{
                    Account account = new Account(Id = opportunity.AccountId);
                    account.MercerForce_Account_Status__c = 'Open Opportunity - Stage 2+';
                    accountUpdatedCache.add(account.Id);
                    accForUpdateMap.put(account.Id, account);
                //}       
            } */
            if((opportunity.StageName.equalsIgnoreCase('Active Pursuit') 
                || opportunity.StageName.equalsIgnoreCase('Finalist') 
                || opportunity.StageName.equalsIgnoreCase('Selected')) 
                && opportunity.account.MercerForce_Account_Status__c <> 'Open Opportunity - Stage 2+' )
            {
                //if(!accountUpdatedCache.contains(opportunity.AccountId))
                //{
                    Account account = new Account(Id = opportunity.AccountId);
                    account.MercerForce_Account_Status__c = 'Open Opportunity - Stage 2+';
                    accountUpdatedCache.add(account.Id);
                    accForUpdateMap.put(account.Id, account);
                //}       
            }
            /*****************Request Id:17601;Removing Pending Step from the If condition END***************/
        }
        
        for(Opportunity opportunity : opportunities)
        {
            if(opportunity.StageName.equalsIgnoreCase('Identify') || opportunity.StageName.equalsIgnoreCase('Above the Funnel'))
            {
                if(!accountUpdatedCache.contains(opportunity.AccountId) && opportunity.account.MercerForce_Account_Status__c <> 'Open Opportunity - Stage 1')
                {
                    Account account = new Account(Id = opportunity.AccountId);
                    account.MercerForce_Account_Status__c = 'Open Opportunity - Stage 1';
                    accountUpdatedCache.add(account.Id);
                    accForUpdateMap.put(account.Id, account);   
                }
            }
        }
        
        if(!accForUpdateMap.isEmpty())
        {
            
            for(Database.Saveresult result : Database.update(accForUpdateMap.values(), false))
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                    errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());    
                }
            }
        }
    
    }
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
        try
        {
            Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
            system.debug('\n exception has occured');
        }finally
        {
            MercerAccountStatusBatchHelper.callNextBatch('Recent Win');     
        }
         
    }
    
}