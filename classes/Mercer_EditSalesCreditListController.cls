/* This Class is Used as the Controller for Edit Sales Credit Lightning component
* Created By -TCS
* Date - 4/16/2019 
*/
public with sharing class Mercer_EditSalesCreditListController {
    /*  Request 13745,14070 - To fetch name for opportunity*/
    @AuraEnabled
    public static String fetchOppName(Id recId){
        
        try{
            
            if(recId != null && ((String)recId).startsWith('006'))
            {
                /*To fetch name for opportunity*/
                List<opportunity> listopp = [select name from opportunity where id=:recId];
                if(!listopp.isEmpty()){
                    System.debug('****'+listopp.get(0).name);
                    return listopp.get(0).name;
                }
            }else{
                List<Sales_Credit__c> listSalesCredits = [select Opportunity__r.name from Sales_Credit__c where id=:recId];
                if(!listSalesCredits.isEmpty()){
                    System.debug('****'+listSalesCredits.get(0).Opportunity__r.name);
                    return listSalesCredits.get(0).Opportunity__r.name;
                }
            }
            return null;
        }
        catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'fetchOppName');
            new AuraHandledException('Failed to fetch Opp Name! Please try after sometime.');
            throw ex;
        }
        
        
    }
    
    /* Request 13745,14070 - To fetch sales credit records for opportunity*/
    @AuraEnabled
    public static List<SelectionWrapper> fetchSalesCredit(Id recId){ 
        List<SelectionWrapper> listSCW = new List<SelectionWrapper>();
        
        try{
            System.debug('***'+recId);
             if(recId != null && ((String)recId).startsWith('006'))
            {
                /*To fetch sales credit records for opportunity*/
                List<Sales_Credit__c> listSC = [select id,name,EmpName__c,EmpName__r.name,Role__c,SP_CM_Allocation__c,LOB__c,Percent_Allocation__c,Opportunity__c from Sales_Credit__c where Opportunity__c=:recId];
                if(!listSC.isEmpty()){
                    
                    for(Sales_Credit__c sc :listSC) {
                        SelectionWrapper scw = new SelectionWrapper(sc,false);
                        listSCW.add(scw);
                    }
                    return listSCW;
                }
            }else{
                
                String relatedOppId;
                List<Sales_Credit__c> listSalesCredits = [select Opportunity__c from Sales_Credit__c where id=:recId];
                if(!listSalesCredits.isEmpty()){
                   relatedOppId = listSalesCredits.get(0).Opportunity__c;
                }
                if(relatedOppId != null){
                    List<Sales_Credit__c> listSC = [select id,name,EmpName__c,EmpName__r.name,Role__c,SP_CM_Allocation__c,LOB__c,Percent_Allocation__c,Opportunity__c from Sales_Credit__c where Opportunity__c=:relatedOppId];
                    if(!listSC.isEmpty()){
                        
                        for(Sales_Credit__c sc :listSC) {
                            SelectionWrapper scw = new SelectionWrapper(sc,false);
                            listSCW.add(scw);
                        }
                        return listSCW;
                    }
                }
            }
            return null;
        }
        catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'delSlctRec');
            new AuraHandledException('Failed to fetch Sales Credit! Please try after sometime.');
            throw ex;
        }
        
    }
    /* Request 13745,14070 -  To fetch all enabled picklist values in role*/
    @AuraEnabled
    public static List<String> fetchRole(){
        List<String> options = new List<String>();
        
        Schema.DescribeFieldResult fieldResult =
            Sales_Credit__c.Role__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        try{    
            for( Schema.PicklistEntry f : ple)
            {
                options.add(f.getValue());
            }       
            options.add(System.Label.None_With_Dash);
            options.sort();
        }
        Catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'delSlctRec');
            new AuraHandledException('Failed to fetch Role! Please try after sometime.');
            throw ex; 
        }
        return options;
    } 
    /* Request 13745,14070 - To save edited or newly created records*/
    @AuraEnabled
    public static String save(String scl ){
        List<Sales_Credit__c> toUpdateSalesCreditList= new List<Sales_Credit__c>();
        List<Sales_Credit__c> toInsertSalesCreditList= new List<Sales_Credit__c>();
        String oppId = null;
        try{
            if(scl != null && !String.isEmpty(scl)){
                List<Sales_Credit__c> lstScl = (List<Sales_Credit__c>)JSON.deserialize(scl, List<Sales_Credit__c>.class);
                System.debug('&&&&&'+lstScl);
                for(Sales_Credit__c item : lstScl){
                    if(item != null){
                        if(item.Id != null && !String.isEmpty(item.Id)){
                        	toUpdateSalesCreditList.add(item);
                        }else{
                            toInsertSalesCreditList.add(item);
                        }
                    }
                }
            }
            
            
            if(!toUpdateSalesCreditList.isEmpty()){
                update toUpdateSalesCreditList;
                System.debug('update list:'+toUpdateSalesCreditList);
                oppId = toUpdateSalesCreditList.get(0).opportunity__c;
            }
            if(!toInsertSalesCreditList.isEmpty()){
                insert toInsertSalesCreditList;
                System.debug('update list:'+toInsertSalesCreditList);
                if(oppId != null){
                    oppId = toInsertSalesCreditList.get(0).opportunity__c;
                }
            }
        }
        Catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'save');
            new AuraHandledException('Failed to save Sales Credit! Please try after sometime.');
            throw ex;
        }
        return oppId;
    }
    
    
    /* Request 13745,14070 - To calculate total sales credit allocation on change*/
    /* @AuraEnabled
public static double calculateTotal(List<Sales_Credit__c> scl){
double sum =0;
try{
if(!scl.isEmpty()){
for(Sales_Credit__c sc:scl){
sum =sum+sc.percent_allocation__c;
}
}
}
Catch(Exception ex){
ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'delSlctRec');
new AuraHandledException('Failed to Calculate Sum of Allocation! Please try after sometime.');
throw ex;   
}
return sum;
}*/
    
    /*To fetch the LOB associated to the employeee selected from lookup*/
    @AuraEnabled
    public static string fetchLOB(String colId)
    {
        try{
            /*Request 13745,14070 - To fetch the LOB associated to the employeee selected from lookup*/
            Colleague__c col;        
            col =[select id,LOB__c from Colleague__c where id=:colId];
            return col.LOB__c;
        }
        Catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'delSlctRec');
            new AuraHandledException('Failed to fetch LOB! Please try after sometime.');
            throw ex; 
        }
    }
    /*To delete selected Sales Credit*/
    // @AuraEnabled
    /*  public static boolean del(String scId){
List<Sales_Credit__c> sclist=[select id from Sales_Credit__c where id=:scId];
if(!sclist.isEmpty()){
// delete sclist;
return true;
}
else{
return false;
}
}*/
    /* Request 13745,14070 - To delete mass selected sales credit records*/
    @AuraEnabled
    public static List<SelectionWrapper> delSlctRec(String slctRec){
        List<SelectionWrapper> resultingList = new List<SelectionWrapper>();
        List<Sales_Credit__c> toDeleteList = new List<Sales_Credit__c>();
        try{
            if(slctRec != null && !String.isEmpty(slctRec)){
                List<SelectionWrapper> deserializedSelections = 
                    (List<SelectionWrapper>)JSON.deserialize(slctRec, List<SelectionWrapper>.class);
                for(SelectionWrapper item : deserializedSelections){
                    if(item != null){
                        if(item.isChecked){
                            toDeleteList.add(item.scl);
                        }else{
                            resultingList.add(item);
                        }
                    }
                }
                delete toDeleteList;
            }
        }
        Catch(Exception ex){
            ExceptionLogger.logException(ex, 'Mercer_EditSalesCreditListController', 'delSlctRec');
            new AuraHandledException('Failed to delete Sales Credit! Please try after sometime.');
            throw ex;
        }
        return resultingList;
    }
    public class SelectionWrapper{
        @AuraEnabled public Sales_Credit__c scl;
        @AuraEnabled public boolean isChecked;
        // @AuraEnabled public Integer ind ;
        public SelectionWrapper(){
            
        }
        public SelectionWrapper(Sales_Credit__c scl, Boolean isChecked){
            //this.ind =ind; 
            this.scl = scl;
            this.isChecked = isChecked;
        }
    }
    
}