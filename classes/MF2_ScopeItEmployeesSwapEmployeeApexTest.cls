/***************************************************************************************************************
Req ID : 17453
Purpose : This class is created as the test class for of MF2_ScopeItEmployeesSwapEmployeeApex.
Created by : Soumil Dasgupta
Created Date : 10/01/2018
Project : MF2
****************************************************************************************************************/
@isTest(SeeAllData = False)
public class MF2_ScopeItEmployeesSwapEmployeeApexTest {
    
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    
    public static testmethod void swapEmployeeTestPositive(){
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        //Account test data creation
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        String accID = testAccount1.ID;
        
        //Opportunity test data creation
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  accID; 
        //#12638 sprint 2 dependency
        //testOpportunity.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
         //#12638 sprint 2 dependency
        testOpportunity.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(2018, 1, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        testOpportunity.Close_Stage_Reason__c ='Other';
        insert testOpportunity;
        Id OpptyId = testOpportunity.ID;        
        
        
        //ScopeIt project test data creation
        ScopeIt_Project__c testscopeitProject = new ScopeIt_Project__c();
        testscopeitProject.name = 'testscopeitproj';
        //testscopeitProject.Product__c =  prodId;
        //testscopeitProject.Bill_Type__c = 'Billable';                
        testscopeitProject.opportunity__c = OpptyId;
        //testscopeitProject.OpportunityProductLineItem_Id__c = oliId;
        //testscopeitProject.CurrencyIsoCode = oliCurr;
        //testscopeitProject.Sales_Price__c=oliUnitprice;
        insert testscopeitProject;
        ID scopeitprojectid = testscopeitProject.id;
        
        //ScopeIt Task test data creation
        ScopeIt_Task__c testscopeTask = new ScopeIt_Task__c(); 
        testscopeTask.task_name__c = 'testscopetask';
        testscopeTask.ScopeIt_Project__c = scopeitprojectid;
        //testscopeTask.Net_Price__c = netPrice;       
        testscopeTask.Billable_Expenses__c = 2000;
        testscopeTask.Bill_Rate__c = 20;
        //testscopeTask.Cost_Rate__c = 30;
        insert testscopeTask;
        
        System.runAs(user1) {
            ApexPages.StandardController stdController = new ApexPages.Standardcontroller(testscopeitProject);
            MF2_ScopeItEmployeesSwapEmployeeApex controller = new MF2_ScopeItEmployeesSwapEmployeeApex(stdController);
            system.assertequals(true,controller.taskFound);
        }
    }
    
}