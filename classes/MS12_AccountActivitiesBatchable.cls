/*
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Arijit Roy     04/01/2013             Batch class for updating Account Status    
2.0         Jagan          05/22/14             Updated class to check if the status is not already same
============================================================================================================================================== 
*/
global class MS12_AccountActivitiesBatchable implements Database.Batchable<sObject>, Database.Stateful{
    
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
        'Recent Win'                  => 'Recent Win',
        'High Revenue'                => 'High Revenue',
        'Low Revenue'                 => 'Low Revenue',
        'Active Marketing Contact'    => 'Active Marketing Contact',
        'Active Contact'              => 'Active Contact'   
    };
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    //global static String query = 'Select Id, WhatId From Event where isRelatedToAccount__c = ' + '\'' + 'YES' + '\'' + ' AND What.MercerForce_Account_Status__c NOT IN' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()) +' ORDER BY WhatId';
    global static String query = 'Select Id,MercerForce_Account_Status__c From Account where (MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset())+ ' or MercerForce_Account_Status__c = null)';   
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    if(Test.isRunningTest())
        {
            String AccName= 'TestAccountName123' + String.valueOf(Date.Today());  
            
            query = 'Select Id,MercerForce_Account_Status__c  From Account where MercerForce_Account_Status__c  IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()); 
        }
         System.debug('#########Return#######'+Database.getQueryLocator(query));
        return Database.getQueryLocator(query); 
    }
    
     /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Set<Id> AccountIds = New Set<Id> (); 
        Map<Id,Task[]> MapAccId_Tasks = New Map<Id,Task[]> () ;
        Map<Id,Event[]> MapAccId_Events = New Map<Id,Event[]> () ;
        Map<Id,ActivityHistory[]> MapAccId_Acts = New Map<Id,ActivityHistory[]> () ;
        
        for(Account a :(List<Account>)scope){
            
            AccountIds.add(a.Id); 
        
        }
        
        List<Task> AllTasks = New List<Task> ();
        AllTasks = [ SELECT Id, Accountid 
                         FROM Task
                         WHERE Accountid IN :AccountIds ] ;
            
            Set<Task> AllTasksSet  = New Set<Task> ( AllTasks );
            
            for(Id a :AccountIds ){
                
                List<Task> tmpTask = New List<Task> ();
                
                for( Task t : AllTasksSet  ){
                
                    if ( a == t.AccountId ) {
                        
                        tmpTask.add(t);
                        AllTasksSet.remove(t); 
                        
                    }
            
                }
                
                MapAccId_Tasks.Put(a,tmpTask);
            
            }
                         
        List<Event> AllEvents = New List<Event> ();
        AllEvents = [ SELECT Id, Accountid 
                         FROM Event
                         WHERE Accountid IN :AccountIds ] ;    
                         
                         
            Set<Event> AllEventsSet  = New Set<Event> ( AllEvents );
        
            for(Id a :AccountIds ){
                
                List<Event> tmpEvent = New List<Event> ();
                
                for( Event e : AllEventsSet  ){
                
                    if ( a == e.AccountId ) {
                        
                        tmpEvent.add(e);
                        AllEventsSet.remove(e); 
                        
                    }
            
                }
                
                MapAccId_Events.Put(a,tmpEvent);
            
            }                               
        
        List<Account> AllActivities = New List<Account>() ;
        AllActivities = [Select Id, (SELECT ActivityDate, createddate FROM ActivityHistories) 
                            FROM Account 
                            WHERE Id  IN : AccountIds ];
        
        Set<Account> AllActivitiesSet  = New Set<Account> ( AllActivities );
        
            
            for( Account a : AllActivitiesSet  ){
                
                List<ActivityHistory> tmpAct = New List<ActivityHistory> ();

                    for( ActivityHistory h : a.ActivityHistories  ){
                    
                        tmpAct.add(h);
                    }
                
                if ( ! tmpAct.isEmpty() ){                  
                    MapAccId_Acts.Put(a.Id,tmpAct);
                }
                
                AllActivitiesSet.remove(a);
        
            }
            
           // System.debug('MapAccId_Acts ==> '+ MapAccId_Acts );
            
        Map<String, Account> accountMapForUpdate = new Map<String, Account>();
                
        for(Account account : (List<Account>)scope)
        {
            //List<Task> tasks = account.Tasks;
            //List<Event> events = account.Events;
            //List<ActivityHistory> activities = account.ActivityHistories;
            
            List<Task> tasks   = MapAccId_Tasks.get(account.id) ;
            List<Event> events = MapAccId_Events.get(account.id) ;
            
            List<ActivityHistory> activities = New List<ActivityHistory>() ;
                
                if (  MapAccId_Acts.get(account.id) != null ){                  
                    activities = MapAccId_Acts.get(account.id) ;
                }
                
            if((!tasks.isEmpty() || !events.isEmpty() || !activities.isEmpty()) && account.MercerForce_Account_Status__c <> 'Activities Exists')
            {
                account.MercerForce_Account_Status__c = 'Activities Exists';
                accountMapForUpdate.put(account.Id, account);   
            }
        } 
        
        if(!accountMapForUpdate.values().isEmpty())
        {
            for(Database.SaveResult result : Database.update(accountMapForUpdate.values(), false))
            {
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }   
            }
        }   
    }
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
       try
        {
          Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
          system.debug('\n exception has occured');
        }finally
        {
         // TO DO: Error logging code logic to be added
        MercerAccountStatusBatchHelper.callNextBatch('Is Competitor');  
        } 
        
    }
    
    //convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
    /*global static String quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }*/
}