/**
* @Description
* Test Class for Mercer_New_Module_class. 
**/

@isTest

public class Mercer_New_Module_class_test{

    private static testMethod void Mercer_New_Module_class_testMethod(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active'; 
        testContact.email = 'pqr@xyz3.com';           
        insert testContact;
        
        Talent_Contract__c testTc = new Talent_Contract__c();
        testTc.Account__c = testAccount.id;
        TestTc.Product__c = 'WIN ePRISM';
        insert TestTc;
                         
        
        Module__c Testmod = new Module__c();
        Testmod.Talent_Contract__c = testTc.id;
        //insert Testmod;
        
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con = new ApexPages.StandardController(testmod);
        Mercer_New_Module_class testM = new Mercer_New_Module_class(con);
        testM.Save();
        
        Module__c Testmod2 = new Module__c();
        Testmod2.Talent_Contract__c = testTc.id;
        
        ApexPages.StandardController con2 = new ApexPages.StandardController(testmod2);
        Mercer_New_Module_class testM2 = new Mercer_New_Module_class(con2);
        
        
        testM2.SaveNew();
        
    
    }
    
    //Covering exceptions
    private static testMethod void Mercer_New_Module_class_testMethod2(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active'; 
        testContact.email = 'pqr@xyz2.com';           
        insert testContact;
        
        Talent_Contract__c testTc = new Talent_Contract__c();
        testTc.Account__c = testAccount.id;
        TestTc.Product__c = 'WIN ePRISM';
        insert TestTc;
                         
        
        Module__c Testmod = new Module__c();
        Testmod.Talent_Contract__c = testTc.id;
        //insert Testmod;
        
        System.currentPageReference().getParameters().put('contractId',null);
        ApexPages.StandardController con = new ApexPages.StandardController(testmod);
        Mercer_New_Module_class testM = new Mercer_New_Module_class(con);
        testM.Save();
        
        Module__c Testmod2 = new Module__c();
        //Testmod2.Talent_Contract__c = testTc.id;
        
        System.currentPageReference().getParameters().put('contractId',testTc.id);
        ApexPages.StandardController con2 = new ApexPages.StandardController(testmod2);
        Mercer_New_Module_class testM2 = new Mercer_New_Module_class(con2);
        
        
        testM2.SaveNew();
        
    
    }
    
}