/*Purpose:  This Apex class contains  the logic for Mercer Opportunity New Page 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR          DETAIL 
   1.0 -    Arijit          Created a controller Opportunity new page.
   2.0 -    Srishty         Added Chatter functionality for auto follow of record and Chatter Post Mention per requirement # 367
   3.0 -    Sarbpreet       As per March 2014 Release (Request # 2940) changed the url of saveAndProduct method to new custom vf page for adding products.
   4.0 -    Neeraj          As per September 2015 Release (Request #7695) changed the condition on which opportunity office value for (WFH) is populated from the Employee office.
   5.0 -    Trisha          Request # 12638 Replacing step with stage for opportunity
============================================================================================================================================== 
*/
public class MercVFC06_OpportuntyControllerEdit {



    public Opportunity opportunity{get; set;}

    public string OppID {get;set;}
    
    public MercVFC06_OpportuntyControllerEdit(ApexPages.StandardController controller) 
    {
        this.opportunity = (Opportunity)controller.getRecord();
        OppID =ApexPages.currentPage().getParameters().get('id');

        if(OppID!=Null){
        /*****request id:12638, removing step__c from query START*****/
           /* opportunity=[select id,Name,Step__c,StageName,CloseDate,Type,AccountID,Description,Next_Action__c,CurrencyIsoCode,CampaignID,IsPrivate,Opportunity_Office__c,Growth_Plan_ID__c,Opportunity_Sub_Market__c,
            Opportunity_Country__c,Opportunity_Market__c,Opportunity_Sub_Region__c,Opportunity_Region__c,Managed_Office__c, Final_Office__c, LeadSource,Related_Opportunity__c,Global_Contract__c,
            Global_Client_Strategy__c,Global_Rate_Card__c,Global_Client_Governance__c,Global_Special_Terms__c,Other_Global_Agreement__c,Bundled_Integrated_Offering__c,Delivery_Geography__c,Opportunity_Name_Local__c,Blue_Sheet_Last_Updated__c,mh_Specify_Competitors__c,mh_Adequacy_of_Current_Position__c,
            mh_Managers_Notes__c,Sibling_Company__c,Business_LOB__c,Sibling_Contact_Name__c,Potential_Revenue__c,Sibling_Contact_Office__c,Cross_Sell_ID__c,Referrer_Name__c,
            Referral_to_LOB__c,Referrer_LOB__c,Referrer_Office__c,Total_Scopable_Products_Revenue__c,Profit_Loss__c,Planned_Fee_Adj__c,Calc_Margin__c,Cost2__c,
            WD_Market_Segment__c,Workday_Region__c,ServiceNow_Market_Segment__c,ServiceNow_Region__c
            From Opportunity where Id=:OppID];*/
            opportunity=[select id,Name,StageName,CloseDate,Type,AccountID,Description,Next_Action__c,CurrencyIsoCode,CampaignID,IsPrivate,Opportunity_Office__c,Growth_Plan_ID__c,Opportunity_Sub_Market__c,
            Opportunity_Country__c,Opportunity_Market__c,Opportunity_Sub_Region__c,Opportunity_Region__c,Managed_Office__c, Final_Office__c, LeadSource, Sales_Lead_Source__c, Related_Opportunity__c,Global_Contract__c,
            Global_Client_Strategy__c,Global_Rate_Card__c,Global_Client_Governance__c,Global_Special_Terms__c,Other_Global_Agreement__c,Bundled_Integrated_Offering__c,Delivery_Geography__c,Opportunity_Name_Local__c,Blue_Sheet_Last_Updated__c,mh_Specify_Competitors__c,mh_Adequacy_of_Current_Position__c,
            mh_Managers_Notes__c,Sibling_Company__c,Business_LOB__c,Sibling_Contact_Name__c,Potential_Revenue__c,Sibling_Contact_Office__c,Cross_Sell_ID__c,Referrer_Name__c,
            Referral_to_LOB__c,Referrer_LOB__c,Referrer_Office__c,Total_Scopable_Products_Revenue__c,Profit_Loss__c,Planned_Fee_Adj__c,Calc_Margin__c,Cost2__c,
            WD_Market_Segment__c,Workday_Region__c,ServiceNow_Market_Segment__c,ServiceNow_Region__c,ServiceNow_Region_2__c,Ambassador_Type__c
            From Opportunity where Id=:OppID];
         /*****request id:12638, removing step__c from query END*****/
        }
        
        
    }
    
     public ApexPages.PageReference saveRecord(){
     
     ApexPages.StandardController controller = new ApexPages.StandardController(opportunity);
        try{
            if(opportunity.Cross_Sell_ID__c != null){
                List<Opportunity> oppList  = new List<Opportunity>();
                oppList = [Select Id,Cross_Sell_ID__c,Opportunity_ID__c from Opportunity where Cross_Sell_ID__c=:opportunity.Cross_Sell_ID__c ];
                if(oppList!= null && oppList.size()>0 && oppList[0].Id != opportunity.Id){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'This Cross Sell ID has been used on ' + oppList.get(0).Opportunity_ID__c + ' and can only be used once'));
                    return null;
                }
                else{
                   update opportunity; 
        
                   PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
                   pageRef.getParameters().put('id', OppID);
                   return pageRef;                                                         
                }
            }
            else{
                   update opportunity;         
                   PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
                   pageRef.getParameters().put('id', OppID);
                   return pageRef;
                
            } 
            
                       
            //controller.save();
           // update opportunity;
            
        }
        catch(Exception e){ApexPages.addMessages(e);return null;}
        
       /* PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
        pageRef.getParameters().put('id', OppID);
        return pageRef;  */
        return null;       
    
    }    
    
    public ApexPages.PageReference cancelRecord(){
          PageReference pageRef = new PageReference('/apex/OpportunitySimplifiedScreen');
     pageRef.getParameters().put('id', OppID);

         return pageRef; 
  
    
    }
    public PageReference saveAndReturn(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(opportunity);
        try{
            if(opportunity.Cross_Sell_ID__c != null){
                List<Opportunity> oppList = [Select Id,Cross_Sell_ID__c,Opportunity_ID__c from Opportunity where Cross_Sell_ID__c=:opportunity.Cross_Sell_ID__c ];
                if(oppList!= null && oppList.size()>0 && oppList[0].Id != opportunity.Id){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'This Cross Sell ID has been used on ' + oppList.get(0).Opportunity_ID__c + ' and can only be used once'));
                    return null;
                }  
                else{
                   update opportunity;         
                   PageReference pageRef = new PageReference('/'+opportunity.id);
                   return pageRef;                                                         
                }                                   
           // controller.save();
          //  update opportunity;
          }
          else{
            update opportunity; 
            PageReference pageRef = new PageReference('/'+opportunity.id);
            return pageRef;                                                         
          }          
            
        }
        catch(Exception e){ApexPages.addMessages(e);return null;}
        
   //  PageReference pageRef = new PageReference('/'+opportunity.id);
     //pageRef.getParameters().put('id', String.valueOf(opp.id));

    // return pageRef; 
       return null;

    
    }
}