public with sharing class EditAccount {
    public Account acc{get; set;}
    public string AccId {get;set;}    
    ApexPages.standardController m_sc = null;
    public string RingLeadDataBaseURL{get;set;}
    public string leanDataBaseURL{get;set;}
    public string marketoBaseURL{get;set;}
    public Boolean isShowPSP{get;set;}
    public Boolean isShowHealthLead{get;set;}  
    //14846
    public Boolean isRenderSections{get;set;}
    public Boolean isRequired{get;set;}   
    
    public EditAccount (ApexPages.StandardController controller) {
        this.acc=(Account)controller.getRecord();
        
        AccId =acc.id;
        m_sc=controller;
        
        
        string BaseURL = ApexPages.currentPage().getHeaders().get('Host');
        RingLeadDataBaseURL=BaseURL.replace('--c','--uniqueentry');
        
        marketoBaseURL=BaseURL.replace('--c','--mkto-si');
        
       isShowPSP = false; 
       //14846
       isRenderSections = false;
       //isRequired = false;
       List<AccountTeamMember> atmList = new List<AccountTeamMember>();
       List<Profile> adminProfileList = new List<Profile>();
       List<UserRecordAccess> recordEditAccessList = new List<UserRecordAccess>();
       Id loggedInUser = UserInfo.getUserId();
       atmList = [Select Id,AccountId,UserId,AccountAccessLevel From AccountTeamMember 
                  Where AccountId= :acc.Id AND AccountAccessLevel = 'Edit' AND UserId = :loggedInUser];

       adminProfileList = [Select Id,Name From Profile Where Id = :loggedInUser AND Name = 'System Administrator'];
        
       recordEditAccessList = [Select RecordId From UserRecordAccess 
                               Where HasEditAccess = True AND RecordId = :acc.Id AND UserId = :loggedInUser];

       if(!atmList.isEmpty() || !adminProfileList.isEmpty() || !recordEditAccessList.isEmpty()){
           isShowPSP = true;
       }
       
       if(!adminProfileList.isEmpty() || !recordEditAccessList.isEmpty()){
           isShowHealthLead = true;
       }
        
       //14846
        List<PermissionSetAssignment> inPermissionSetAsgmnt = new List<PermissionSetAssignment>();
        inPermissionSetAsgmnt = [Select Id, PermissionSetId, AssigneeId FROM PermissionSetAssignment where AssigneeId = :loggedInUser];
        List<Id> ids = new List<Id>();
         for(PermissionSetAssignment pma:inPermissionSetAsgmnt){
          String tempvar = pma.PermissionSetId;
          ids.add(tempvar);
         }
        List<PermissionSet> inPermissionSet = new List<PermissionSet>();
        inPermissionSet = [Select Id, Name FROM PermissionSet where id in :ids AND Name = 'Know_Your_Client'];
        if(!inPermissionSet.isEmpty()){
           isRenderSections = true;
        }
       //14846
    }
    
    public PageReference edit(){
    
     PageReference pageRef = new PageReference('/apex/Mercer_Account_EditPage');
     pageRef.getParameters().put('AccId', String.valueOf(acc.id));

     return pageRef; 
    
    }
     public PageReference save(){    
    
     ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        try{
            
            controller.save();
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/apex/AccountSimplifiedScreen');
     pageRef.getParameters().put('AccId', String.valueOf(acc.id));
     return pageRef; 

     
    
    }
    public PageReference saveAndReturn(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        try{
            
            controller.save();
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/'+acc.id);
     //pageRef.getParameters().put('AccId', String.valueOf(acc.id));

     return pageRef; 

     
    
    }
    public PageReference Cancel()
    {
        PageReference pageRef = new PageReference('/apex/AccountSimplifiedScreen');
        pageRef.getParameters().put('AccId', String.valueOf(acc.id));

         return pageRef; 
    }
    public PageReference doCancel()
    {
         return m_sc.cancel(); 
    }
    
    public void makeNone()
    
    {   
        
        if(Acc.Document_Owner__c == null|| Acc.Document_Owner__c == ''){
           Acc.Country_of_Residence_Citizenship__c = null;
           Acc.Role__c = '' ;
           Acc.Comments__c = '' ;
        } else{
           Acc.Role__c = 'Beneficial Owners' ;
        }       
        
    }
    public void makeNone2()
    
    {   
        
        if(Acc.Document_Owner2__c == null|| Acc.Document_Owner2__c == ''){
           Acc.Country_of_Residence_Citizenship2__c = null;
           Acc.Role2__c = '' ;
           Acc.Comments2__c = '' ;
        } else{
           Acc.Role2__c = 'Beneficial Owners' ;
        }       
        
    }
    public void makeNone3()
    
    {   
        
        if(Acc.Document_Owner3__c == null|| Acc.Document_Owner3__c == ''){
           Acc.Country_of_Residence_Citizenship3__c = null;
           Acc.Role3__c = '' ;
           Acc.Comments3__c = '' ;
        } else{
           Acc.Role3__c = 'Beneficial Owners' ;
        }       
        
    }
    public void makeNone4()
    
    {   
        
        if(Acc.Document_Owner4__c == null|| Acc.Document_Owner4__c == ''){
           Acc.Country_of_Residence_Citizenship4__c = null;
           Acc.Role4__c = '' ;
           Acc.Comments4__c = '' ;
        } else{
           Acc.Role4__c = 'Beneficial Owners' ;
        }       
        
    }
    public void makeNone5()
    
    {   
        
        if(Acc.Document_Owner5__c == null|| Acc.Document_Owner5__c == ''){
           Acc.Country_of_Residence_Citizenship5__c = null;
           Acc.Role5__c = '' ;
           Acc.Comments5__c = '' ;
        } else{
           Acc.Role5__c = 'Beneficial Owners' ;
        }       
        
    }
    
}