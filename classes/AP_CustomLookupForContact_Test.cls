@isTest(SeeAllData=true)
public class AP_CustomLookupForContact_Test {
    //TestMethod for Contacts at this account without any wildcards
    public static testmethod void testSearchContactAtAccount(){ 
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        Test.starttest();
        Opportunity testOpportunity = testData.buildOpportunity();
        Test.stoptest(); 
        Contact cc = testData.buildContact();
        
        Test.setCurrentPageReference(new PageReference('Page.VF_CustomLookupForContact'));
        System.currentPageReference().getParameters().put('accid',acc.id);
        System.currentPageReference().getParameters().put('oppid',testOpportunity.id);
        
        Buying_Influence__c bi = new Buying_Influence__c();
        bi.mh_Degree__c='High';
        bi.Opportunity__c=testOpportunity.id;
        bi.contact__c=cc.id;
        insert bi;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(bi);
                
        AP_CustomLookupForContact_Extension obj = new AP_CustomLookupForContact_Extension(sc); 
        obj.customText= 'TestFirstName';
        List<SelectOption> testSelect = obj.getItems();
        
        PageReference searchResults = obj.searchCustomLookUp();
        obj.FirstPage();
        obj.getnxt();
        obj.next();
        obj.getprev();
        obj.previous();
        Integer totalPage = obj.getTotalPages();
        Integer PageNumber = obj.getPageNumber();
        obj.customText='';
        searchResults = obj.searchCustomLookUp();
        obj.customText='ShouldNotMatch';
        searchResults = obj.searchCustomLookUp();
        
    }
    //testmethod for searching contact at current account with wildcards
    private static testmethod void SearchContactAtAccountWildCards(){
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        Test.starttest();
        Opportunity testOpportunity = testData.buildOpportunity();
        Test.stoptest();
        Contact cc = testData.buildContact();
        
        Test.setCurrentPageReference(new PageReference('Page.VF_CustomLookupForContact'));
        System.currentPageReference().getParameters().put('accid',acc.id);
        System.currentPageReference().getParameters().put('oppid',testOpportunity.id);
        
        Buying_Influence__c bi = new Buying_Influence__c();
        bi.mh_Degree__c='High';
        bi.Opportunity__c=testOpportunity.id;
        bi.contact__c=cc.id;
        insert bi;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(bi);
                
        AP_CustomLookupForContact_Extension obj = new AP_CustomLookupForContact_Extension(sc); 
        obj.customText= 'TestFirst*';
        List<SelectOption> testSelect = obj.getItems();
        PageReference searchResults = obj.searchCustomLookUp();
        obj.customText='*First*';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*First';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*Fi*rst';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*Fi*rst*';
        searchResults = obj.searchCustomLookUp();
        obj.customText='Fi*rst';
        searchResults = obj.searchCustomLookUp();
        obj.radioValueChanged();
         
    }
    private static testmethod void searchAllContactsTest(){
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        Test.starttest(); 
        Opportunity testOpportunity = testData.buildOpportunity();
        Test.stoptest();
        Contact cc = testData.buildContact();
        
        Test.setCurrentPageReference(new PageReference('Page.VF_CustomLookupForContact'));
        System.currentPageReference().getParameters().put('accid',acc.id);
        System.currentPageReference().getParameters().put('oppid',testOpportunity.id);
        
        Buying_Influence__c bi = new Buying_Influence__c();
        bi.mh_Degree__c='High';
        bi.Opportunity__c=testOpportunity.id;
        bi.contact__c=cc.id;
        insert bi;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(bi);
                
        AP_CustomLookupForContact_Extension obj = new AP_CustomLookupForContact_Extension(sc); 
        obj.customText= 'TestFirst*';
        obj.defaultRadioVal = 'Search All Contacts';
        List<SelectOption> testSelect = obj.getItems();
        PageReference searchResults = obj.searchCustomLookUp();
        obj.customText='*First*';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*First';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*Fi*rst';
        searchResults = obj.searchCustomLookUp();
        obj.customText='*Fi*rst*';
        searchResults = obj.searchCustomLookUp();
        obj.customText='Fi*rst';
        searchResults = obj.searchCustomLookUp();
        obj.customText='';
        searchResults = obj.searchCustomLookUp();
        obj.customText='ShouldNotMatch';
        searchResults = obj.searchCustomLookUp();
        
    } 
}