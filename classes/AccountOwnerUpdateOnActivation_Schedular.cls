global class AccountOwnerUpdateOnActivation_Schedular implements Schedulable{

    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new AccountOwnerUpdateOnActivation_Batch(),Integer.valueOf(Label.AccountOwnerUpdateOnActivation_BatchSize));
    }
}