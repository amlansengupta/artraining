/*Purpose:  This Apex class contains static methods to process growth Plan records and checks if only one record for each Planned Year is created per Account   
=============================================================================================================================================================================

History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   30/10/2014  (As part of Request # 5367)Created Apex class to support trigger TRG27_GrowthPlanTrigger
   2.0 -    Jagan       13/4/2015   Added method to update revenue fields
   3.0 -    Sarbpreet   20/8/2015   As part of request3 6798(September Release 2015) updated code to include all new BBC fields in the calculation of Revenue Target fields.
   4.0 -    Apoorva/   05/08/2016  As Part of #9790 (August Release 2016) created the After update method.
            Shilpa
   5.0 -    Jigyasu    06/08/2016  As part of #9790 (August Release 2016) updated calclations done in updateRevenuefields method.          
============================================================================================================================================== 
*/
public with sharing class AP115_GrowthPlanTriggerUtil {
    
    private static final String EXCEPTION_STR = 'Exception occured with reason :' ;
    private static final String STR_grERROR = 'Growth Plan for this year already exist' ;

     /*
     * @Description : This method is called on before insertion of Growth Plan records to check if only one record for each Planned Year is created per Account     
     * @ Args       : growthPlanList
     * @ Return     : void
     */
    public static void processGrowthPlanBeforeInsert( List<Growth_Plan__c> growthPlanList)
    {
        try {
        Map<id,Growth_Plan__c> grAccMap = new Map<id,Growth_Plan__c>();
        Map<id,Set<String>> accWithgrMap = new Map<id,Set<String>>();
        
        for(Growth_Plan__c gr : growthPlanList)
        {
            grAccMap.put(gr.Account__c, gr);
        }
        Map<id,account> accmap = new Map<id,account> ([Select id, name , (Select id, Planning_Year__c From Growth_Plans__r) from account where id IN : grAccMap.keySet()]);
        Set<String> grYear;
         for(ID aid : accmap.keyset())
        {
            Account ac = accmap.get(aid);
            grYear = new Set<String>();
            for(Growth_Plan__c gpln : ac.Growth_Plans__r)
            {
                grYear.add(gpln.Planning_Year__c);
            }
            accWithgrMap.put(aid,grYear);
        }
         Set<String> gplnYr;
        for(Growth_Plan__c gp : grAccMap.values())
        {
            
            if(accWithgrMap.containskey(gp.Account__c))
            {             
              gplnYr = new Set<String>();
              gplnYr =(accWithgrMap.get(gp.Account__c));
              if(gplnYr.contains(gp.Planning_Year__c))
              gp.addError(STR_grERROR);
            } 
        }  
        //Call the method to update revenue fields
        AP115_GrowthPlanTriggerUtil.updateRevenuefields(growthPlanList);
        
        } 
        catch(Exception e) {
            for(Integer i =0; i< growthPlanList.size(); i++) {                  
                growthPlanList[i].addError(EXCEPTION_STR+e.getMessage());
            }
        } 
    }
    
     /*
     * @Description : This method is called on before updation of Growth Plan records to check if only one record for each Planned Year is created per Account  
     * @ Args       : triggernewmap, triggeroldmap
     * @ Return     : void
     */
     // Calculation Part is updated to meet the requirement of AUG Release 9790
    public static void processGrowthPlanBeforeUpdate( Map<Id, Growth_Plan__c> triggernewmap, Map<Id, Growth_Plan__c> triggeroldmap)
    {
        try {
        Map<id,Growth_Plan__c> grAccMap = new Map<id,Growth_Plan__c>();
        Map<id,Set<String>> accWithgrMap = new Map<id,Set<String>>();
        List<Growth_Plan__c> growthPlanList = new List<Growth_Plan__c>();
        for(Growth_Plan__c gr : triggernewmap.values())
        {
            grAccMap.put(gr.Account__c, gr);
            growthPlanList.add(gr);
        }
        Map<id,account> accmap = new Map<id,account> ([Select id, name , (Select id, Planning_Year__c From Growth_Plans__r) from account where id IN : grAccMap.keySet()]);
        Set<String> grYear;
         for(ID aid : accmap.keyset())
        {
            Account ac = accmap.get(aid);
            grYear = new Set<String>();
            for(Growth_Plan__c gpln : ac.Growth_Plans__r)
            {
                grYear.add(gpln.Planning_Year__c);
            }
            accWithgrMap.put(aid,grYear);
        }
         Set<String> gplnYr;
        for(Growth_Plan__c gp : grAccMap.values())
        {
            
            if(accWithgrMap.containskey(gp.Account__c))
            {             
              gplnYr = new Set<String>();
              gplnYr =(accWithgrMap.get(gp.Account__c));
              if(gplnYr.contains(gp.Planning_Year__c) && gp.Planning_Year__c <> triggeroldmap.get(gp.id).Planning_Year__c)
              gp.addError(STR_grERROR);
            } 
        } 
       
              
        //Call the method to update revenue fields
        //AP115_GrowthPlanTriggerUtil.updateRevenuefields(growthPlanList);//commented by Forecastera to calculate (TOT Projected Full Year Revenue)
        
        
        } 
        catch(Exception e) {
            
        }  
    }
    
      /*
     * @Description : Method to Update Revenue fields if fields are updated by data loader. 
     * @ Args       : List<Growth_Plan__c>
     * @ Return     : null 
     */
    public static void updateRevenuefields( List<Growth_Plan__c> growthPlanList){
    
        for(Growth_Plan__c gr:growthPlanList) {
        
            if(gr.RET_Projected_Full_Year_Revenue__c==null) gr.RET_Projected_Full_Year_Revenue__c=0;
            if(gr.GBS_Projected_Full_Year_Revenue__c==null) gr.GBS_Projected_Full_Year_Revenue__c=0;
            if(gr.EH_B_Projected_Full_Year_Revenue__c==null) gr.EH_B_Projected_Full_Year_Revenue__c=0;
            if(gr.INTL_Projected_Full_Year_Revenue__c==null) gr.INTL_Projected_Full_Year_Revenue__c=0;
            if(gr.OTH_Projected_Full_Year_Revenue__c==null) gr.OTH_Projected_Full_Year_Revenue__c=0;
            if(gr.TAL_Projected_Full_Year_Revenue__c==null) gr.TAL_Projected_Full_Year_Revenue__c=0;
            
            //As part of request3 6798(September Release 2015) added RET_of_Revenue_Growth_Anticipated01__c in calculation
            if(gr.RET_of_Revenue_Growth_Anticipated01__c==null) gr.RET_of_Revenue_Growth_Anticipated01__c=0;    
            if(gr.GBS_of_Revenue_Growth_Anticipated__c==null) gr.GBS_of_Revenue_Growth_Anticipated__c=0;
            if(gr.TAL_of_Revnue_Growth_Anticipated__c==null) gr.TAL_of_Revnue_Growth_Anticipated__c=0;
            if(gr.EH_B_of_Revenue_Growth_Anticipated01__c==null) gr.EH_B_of_Revenue_Growth_Anticipated01__c=0;
            if(gr.INTL_of_Revenue_Growth_Anticipated01__c==null) gr.INTL_of_Revenue_Growth_Anticipated01__c=0;
            if(gr.OTH_of_Revenue_Growth_Anticipated01__c==null) gr.OTH_of_Revenue_Growth_Anticipated01__c=0;
            
            //As part of request3 6798(September Release 2015) added RET_Projected_Current_Year_Revenue__c in calculation
            if(gr.RET_Projected_Current_Year_Revenue__c == null) gr.RET_Projected_Current_Year_Revenue__c = 0;
            if(gr.GBS_Projected_Current_Year_Revenue__c == null) gr.GBS_Projected_Current_Year_Revenue__c = 0;
            if(gr.EH_B_Projected_Current_Year_Revenue__c == null) gr.EH_B_Projected_Current_Year_Revenue__c = 0;
            if(gr.INTL_Projected_Current_Year_Revenue__c == null) gr.INTL_Projected_Current_Year_Revenue__c = 0;
            if(gr.TAL_Projected_Current_Year_Revenue__c == null) gr.TAL_Projected_Current_Year_Revenue__c = 0;
            if(gr.OTH_Projected_Current_Year_Revenue__c == null) gr.OTH_Projected_Current_Year_Revenue__c = 0;
            
            //As part of request3 6798(September Release 2015) added RET_Recurring_and_Carry_forward_Revenue__c in calculation 
            if(gr.RET_Recurring_and_Carry_forward_Revenue__c == null) gr.RET_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gr.GBS_Recurring_and_Carry_forward_Revenue__c == null) gr.GBS_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gr.EH_B_Recurring_and_Carry_forward_Revenue__c == null) gr.EH_B_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gr.INTL_Recurring_and_Carry_forward_Revenue__c == null) gr.INTL_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gr.TAL_Recurring_and_Carry_forward_Revenue__c == null) gr.TAL_Recurring_and_Carry_forward_Revenue__c = 0;
            if(gr.OTH_Recurring_and_Carry_forward_Revenue__c == null) gr.OTH_Recurring_and_Carry_forward_Revenue__c = 0;
            
            //As part of request3 6798(September Release 2015) added RET_Projected_New_Sales_Revenue__c in calculation 
            if(gr.RET_Projected_New_Sales_Revenue__c == null) gr.RET_Projected_New_Sales_Revenue__c = 0;
            if(gr.GBS_Projected_New_Sales_Revenue__c == null) gr.GBS_Projected_New_Sales_Revenue__c = 0;
            if(gr.EH_B_Projected_New_Sales_Revenue__c == null) gr.EH_B_Projected_New_Sales_Revenue__c = 0;
            if(gr.INTL_Projected_New_Sales_Revenue__c == null) gr.INTL_Projected_New_Sales_Revenue__c = 0;
            if(gr.TAL_Projected_New_Sales_Revenue__c == null) gr.TAL_Projected_New_Sales_Revenue__c = 0;
            if(gr.OTH_Projected_New_Sales_Revenue__c == null) gr.OTH_Projected_New_Sales_Revenue__c = 0;
            
            //As part of request6 9790(AUG Release 2016) Projected full year revenue actuals fields added.
            if(gr.RET_Projected_Full_Year_Revenue_Actual__c==null) gr.RET_Projected_Full_Year_Revenue_Actual__c=0;
            if(gr.GBS_Projected_Full_Year_Revenue_Actual__c==null) gr.GBS_Projected_Full_Year_Revenue_Actual__c=0;
            if(gr.EH_B_Projected_Full_Year_Revenue_Actual__c==null) gr.EH_B_Projected_Full_Year_Revenue_Actual__c=0;
            if(gr.INTL_Projected_Full_Year_Revenue_Actual__c==null) gr.INTL_Projected_Full_Year_Revenue_Actual__c=0;
            if(gr.OTH_Projected_Full_Year_Revenue_Actual__c==null) gr.OTH_Projected_Full_Year_Revenue_Actual__c=0;
            if(gr.TAL_Projected_Full_Year_Revenue_Actual__c==null) gr.TAL_Projected_Full_Year_Revenue_Actual__c=0;
            
            //As part of request6 9790(AUG Release 2016) Planning Yr and Carry Frw Rev% fields added.
            if(gr.RET_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.RET_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.GBS_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.GBS_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.EH_B_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.EH_B_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.INTL_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.INTL_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.OTH_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.OTH_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.TAL_Planning_Yr_and_Carry_Frw_Rev__c==null) gr.TAL_Planning_Yr_and_Carry_Frw_Rev__c=0;
            if(gr.TOT_Projectd_Current_Year_Revenue__c == null) gr.TOT_Projectd_Current_Year_Revenue__c = 0;
            if(gr.TOT_Planning_Yr_and_Carry_Frw_Rev__c == null) gr.TOT_Planning_Yr_and_Carry_Frw_Rev__c = 0;
                 
                  
                  //11475  
                   //if(gr.RET_Projected_Full_Year_Revenue_Actual__c!=null && gr.GBS_Projected_Full_Year_Revenue_Actual__c!=null && gr.TAL_Projected_Full_Year_Revenue_Actual__c!=null && gr.EH_B_Projected_Full_Year_Revenue_Actual__c!=null  && gr.OTH_Projected_Full_Year_Revenue_Actual__c!=null)      
                      gr.TOT_Projected_Full_Year_Revenue_Actual__c= (gr.RET_Projected_Full_Year_Revenue_Actual__c + gr.TAL_Projected_Full_Year_Revenue_Actual__c + gr.EH_B_Projected_Full_Year_Revenue_Actual__c + gr.GBS_Projected_Full_Year_Revenue_Actual__c + gr.OTH_Projected_Full_Year_Revenue_Actual__c);
                      gr.TOT_Projectd_Current_Year_Revenue__c = (gr.RET_Projected_Current_Year_Revenue__c +gr.GBS_Projected_Current_Year_Revenue__c + gr.EH_B_Projected_Current_Year_Revenue__c + gr.TAL_Projected_Current_Year_Revenue__c + gr.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                     // gr.TOT_Projectd_Current_Year_Revenue__c = (gr.RET_Projected_Current_Year_Revenue__c +gr.GBS_Projected_Current_Year_Revenue__c + gr.EH_B_Projected_Current_Year_Revenue__c + gr.TAL_Projected_Current_Year_Revenue__c + gr.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                      
                  
                  //11475
                      gr.RET_Recurring_and_Carry_forward_Revenue__c = ((gr.RET_Projected_Current_Year_Revenue__c * gr.RET_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gr.GBS_Recurring_and_Carry_forward_Revenue__c = ((gr.GBS_Projected_Current_Year_Revenue__c * gr.GBS_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gr.EH_B_Recurring_and_Carry_forward_Revenue__c = ((gr.EH_B_Projected_Current_Year_Revenue__c * gr.EH_B_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gr.TAL_Recurring_and_Carry_forward_Revenue__c = ((gr.TAL_Projected_Current_Year_Revenue__c * gr.TAL_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                      gr.OTH_Recurring_and_Carry_forward_Revenue__c = ((gr.OTH_Projected_Current_Year_Revenue__c * gr.OTH_Planning_Yr_and_Carry_Frw_Rev__c) / 100).setScale(1);
                    
                      gr.TOT_Recurring_Carry_forward_Revenue__c = (gr.RET_Recurring_and_Carry_forward_Revenue__c + gr.GBS_Recurring_and_Carry_forward_Revenue__c + gr.EH_B_Recurring_and_Carry_forward_Revenue__c + gr.TAL_Recurring_and_Carry_forward_Revenue__c + gr.OTH_Recurring_and_Carry_forward_Revenue__c).setScale(1); 
                      
                      // WEALTH Calculations for Column 5 and 6
                     if(gr.RET_Projected_Current_Year_Revenue__c == 0){
                     gr.RET_Projected_New_Sales_Revenue__c = gr.RET_of_Revenue_Growth_Anticipated01__c;
                     gr.RET_Projected_Full_Year_Revenue__c = gr.RET_Projected_New_Sales_Revenue__c;}
                     else{
                     gr.RET_Projected_Full_Year_Revenue__c = (((gr.RET_Projected_Current_Year_Revenue__c * (gr.RET_of_Revenue_Growth_Anticipated01__c/100)) + gr.RET_Projected_Current_Year_Revenue__c)).setScale(1);
                     gr.RET_Projected_New_Sales_Revenue__c = (gr.RET_Projected_Full_Year_Revenue__c - gr.RET_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                      
                      // GBS Calculations for Column 5 and 6
                     if(gr.GBS_Projected_Current_Year_Revenue__c == 0){
                     gr.GBS_Projected_New_Sales_Revenue__c = gr.GBS_of_Revenue_Growth_Anticipated__c;
                     gr.GBS_Projected_Full_Year_Revenue__c = gr.GBS_Projected_New_Sales_Revenue__c;}
                     else{
                     gr.GBS_Projected_Full_Year_Revenue__c = ((gr.GBS_Projected_Current_Year_Revenue__c * (gr.GBS_of_Revenue_Growth_Anticipated__c/100)) + gr.GBS_Projected_Current_Year_Revenue__c).setScale(1);
                     gr.GBS_Projected_New_Sales_Revenue__c = (gr.GBS_Projected_Full_Year_Revenue__c - gr.GBS_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                     
                     //HEALTH Calculations for Column 5 and 6
                     if(gr.EH_B_Projected_Current_Year_Revenue__c == 0){
                     gr.EH_B_Projected_New_Sales_Revenue__c = gr.EH_B_of_Revenue_Growth_Anticipated01__c;
                     gr.EH_B_Projected_Full_Year_Revenue__c = gr.EH_B_Projected_New_Sales_Revenue__c;}
                     else{
                     gr.EH_B_Projected_Full_Year_Revenue__c = ((gr.EH_B_Projected_Current_Year_Revenue__c * (gr.EH_B_of_Revenue_Growth_Anticipated01__c/100)) + gr.EH_B_Projected_Current_Year_Revenue__c).setScale(1);
                     gr.EH_B_Projected_New_Sales_Revenue__c = (gr.EH_B_Projected_Full_Year_Revenue__c - gr.EH_B_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                                         
                     // CAREER Calculations for Column 5 and 6
                     if(gr.TAL_Projected_Current_Year_Revenue__c == 0){
                     gr.TAL_Projected_New_Sales_Revenue__c = gr.TAL_of_Revnue_Growth_Anticipated__c;
                     gr.TAL_Projected_Full_Year_Revenue__c = gr.TAL_Projected_New_Sales_Revenue__c;}
                     else{
                     gr.TAL_Projected_Full_Year_Revenue__c = ((gr.TAL_Projected_Current_Year_Revenue__c * (gr.TAL_of_Revnue_Growth_Anticipated__c/100)) + gr.TAL_Projected_Current_Year_Revenue__c).setScale(1);
                     gr.TAL_Projected_New_Sales_Revenue__c = (gr.TAL_Projected_Full_Year_Revenue__c - gr.TAL_Recurring_and_Carry_forward_Revenue__c).setScale(1);}
                                          
                     // OTHR Calculations for Column 5 and 6
                     if(gr.OTH_Projected_Current_Year_Revenue__c == 0){
                     gr.OTH_Projected_New_Sales_Revenue__c = gr.OTH_of_Revenue_Growth_Anticipated01__c;
                     gr.OTH_Projected_Full_Year_Revenue__c = gr.OTH_Projected_New_Sales_Revenue__c;}
                     else{
                     gr.OTH_Projected_Full_Year_Revenue__c = ((gr.OTH_Projected_Current_Year_Revenue__c * (gr.OTH_of_Revenue_Growth_Anticipated01__c/100)) + gr.OTH_Projected_Current_Year_Revenue__c).setScale(1);
                     gr.OTH_Projected_New_Sales_Revenue__c = (gr.OTH_Projected_Full_Year_Revenue__c - gr.OTH_Recurring_and_Carry_forward_Revenue__c).setScale(1);}                    

                     // Column 6 (TOTAL Calculation)
                         gr.TOT_Projectd_Full_Year_Revenue__c =  (gr.RET_Projected_Full_Year_Revenue__c +   gr.GBS_Projected_Full_Year_Revenue__c + gr.TAL_Projected_Full_Year_Revenue__c + gr.EH_B_Projected_Full_Year_Revenue__c+gr.OTH_Projected_Full_Year_Revenue__c).setScale(1);
                     // Column 5 (TOTAL Calculation)
                         gr.TOT_Projected_Sales_Revenue__c = (gr.RET_Projected_New_Sales_Revenue__c +gr.GBS_Projected_New_Sales_Revenue__c + gr.EH_B_Projected_New_Sales_Revenue__c + gr.TAL_Projected_New_Sales_Revenue__c + gr.OTH_Projected_New_Sales_Revenue__c).setScale(1); 
                     

                     if(gr.TOT_Projected_Full_Year_Revenue_Actual__c <> 0)
                      gr.TOT_Planning_Yr_and_Carry_Frw_Rev__c = ((gr.TOT_Recurring_Carry_forward_Revenue__c/gr.TOT_Projected_Full_Year_Revenue_Actual__c) * 100).setScale(1);       
                       else
                      gr.TOT_Planning_Yr_and_Carry_Frw_Rev__c = 0;
                                                 
                            if(gr.TOT_Projectd_Current_Year_Revenue__c <> 0)
                            gr.TOT_of_Revenue_Growth_Anticipated01__c = (((gr.TOT_Projectd_Full_Year_Revenue__c - gr.TOT_Projectd_Current_Year_Revenue__c )/ gr.TOT_Projectd_Current_Year_Revenue__c)*100).setScale(1);
                            else
                            gr.TOT_of_Revenue_Growth_Anticipated01__c = 0;
                               
                            if(gr.TOT_Projectd_Current_Year_Revenue__c <> 0)   
                            gr.Growth_Indicator__c = (((gr.TOT_Projectd_Full_Year_Revenue__c - gr.TOT_Projectd_Current_Year_Revenue__c )/ gr.TOT_Projectd_Current_Year_Revenue__c)*100).setScale(1);
                            system.debug('The growth indicator'+gr.Growth_Indicator__c);
                           //As part of request # 6798 (September Release 2015), upadted code to check growth indicator field value and highlight it with green, yellow or red
                          /* if(gr.Growth_Indicator__c > 0)
                            gColor = ConstantsUtility.STR_GreenCode;
                            
                           if(gr.Growth_Indicator__c == 0)
                            gColor = ConstantsUtility.STR_AmberCode;
                           
                           if(gr.Growth_Indicator__c < 0)
                            gColor = ConstantsUtility.STR_Red;
                           //End of request # 6798 (September Release 2015)
                           system.debug('The growth indicator 2'+gr.Growth_Indicator__c);*/
          
          
          
        }
    
    }
    
    
public static void processGrowthPlanAfterInsert (Map<Id,Growth_Plan__c> triggernewmap)
{
    Map<id,Growth_Plan__c> grMap = new Map<Id,Growth_Plan__c>();
    
    
    for(Growth_Plan__c gp : triggernewmap.values())
    {
        grMap.put(gp.Parent_Growth_Plan_ID__c,gp);
        system.debug('gp+++'+gp.Parent_Growth_Plan_ID__c);
    }
    
    Map<Id,Growth_Plan__c> grMap1 = new Map<Id,Growth_Plan__c> ([Select id,Parent_Growth_Plan_On_CGP__c,Account__c from Growth_Plan__c where id IN : grMap.keyset() ]);
    Map<Id,Growth_Plan__c> parentAcc = new Map<Id,Growth_Plan__c>();
    
    system.debug('@@@grMap1 value'+grMap1);
    
    for(Growth_Plan__c gpParent : grMap1.values())
    {
        gpParent.Parent_Growth_Plan_On_CGP__c = true;
        parentAcc.put(gpParent.Account__c,gpParent);
    }
    update grMap1.values();
    
    Map<Id,Account> AccMap = new Map<Id,Account> ([Select id,Parent_Growth_Plan__c from Account where id IN : parentAcc.keyset()]);
    
    for(ID aid : AccMap.keyset())
    {
            Account a = accmap.get(aid);
            a.Parent_Growth_Plan__c = true;
    }
    update AccMap.values();
}
    
    public static void processGrowthPlanAfterUpdate(Map<Id, Growth_Plan__c> triggernewmap, Map<Id, Growth_Plan__c> triggeroldmap)
    {
        
        Integer Var = 0;
        Map<Id,Growth_Plan__c> grMapOld = new Map<Id,Growth_Plan__c>();
        System.debug('triggeroldmap value'+triggeroldmap);
        for(Growth_Plan__c gpOld : triggeroldmap.values())
        {
            System.debug('gpOld value'+gpOld);
            grMapOld.put(gpOld.Parent_Growth_Plan_ID__c,gpOld);
            system.debug('gp$$$'+gpOld.Parent_Growth_Plan_ID__c);
            
            
            
        }
        
        Map<Id,Growth_Plan__c> grMapOld1 = new Map<Id,Growth_Plan__c>();
        List<ID> gpOldIds = new List<ID>();
        List<ID> lstPgId = new List<ID>();
        System.debug('triggeroldmap value'+triggeroldmap);
        for(Growth_Plan__c gpOld1 : triggeroldmap.values())
        {
            System.debug('gpOld value'+gpOld1);
            grMapOld1.put(gpOld1.Parent_Growth_Plan_ID__c,gpOld1);
            gpOldIds.add(gpOld1.Id);
            system.debug('gp$$$'+gpOld1.Parent_Growth_Plan_ID__c);
            lstPgId.add(gpOld1.Parent_Growth_Plan_ID__c);
            system.debug('lstpg value'+lstPgId);
            
        }
         
       
        
        List<Growth_Plan__c> updateLstgr = new List<Growth_Plan__c>();
         List<Growth_Plan__c> updateLstgr1 = new List<Growth_Plan__c>();
         Map<id,Growth_Plan__c> pgpOld = new Map<id,Growth_Plan__c> ([Select id,Parent_Growth_Plan_ID__c from Growth_Plan__c where Parent_Growth_Plan_ID__c IN: grMapOld1.keyset() AND id NOT IN :gpOldIds]);
         Map<id,Growth_Plan__c> MappgId = new Map<id,Growth_Plan__c>([select id,Parent_Growth_Plan_On_CGP__c from Growth_Plan__c where id IN: lstPgId]);
        system.debug('MappgId value'+MappgId);
        system.debug('pgpOld value'+pgpOld );
        if(pgpOld.size()==0)
        {
            for(ID id  : grMapOld1.keyset())
                    {
                       Growth_Plan__c pg = MappgId.get(id);
                       Growth_Plan__c pgUpdate = new Growth_Plan__c(Id=pg.Id);
                       system.debug('pgUpdate value'+pgUpdate );
                       system.debug('Initial value'+pg.Parent_Growth_Plan_On_CGP__c);
                       pgUpdate.Parent_Growth_Plan_On_CGP__c = false;
                       system.debug('Updated value'+pg.Parent_Growth_Plan_On_CGP__c);
                       updateLstgr.add(pgUpdate);
                       system.debug('List of pg'+updateLstgr);
                    }
                   update updateLstgr;
            
        }
        
        
        
        Map<id,Growth_Plan__c> parentGrowthPlanOld = new Map<id,Growth_Plan__c> ([Select id,Account__c from Growth_Plan__c where id IN:grMapOld.keyset()]);
        System.debug('parentGrowthPlanOld'+parentGrowthPlanOld);
        Map<id,Growth_Plan__c> parentAccOld = new Map<id,Growth_Plan__c>();
         
        for(Growth_Plan__c gp1Old : parentGrowthPlanOld.values()){
            
            system.debug('gp1 value'+gp1Old.Account__c);
            parentAccOld.put(gp1Old.Account__c,gp1Old);
            
            system.debug('parentAccOld value'+parentAccOld);
        }
        //update parentGrowthPlan.values();
        
        Map<id,Account> accmapOld = new Map<id,Account> ([Select id,Parent_Growth_Plan__c from Account where id IN: parentAccOld.keyset() ]);
        Map<id,Growth_Plan__c> childGrowthPlan = new Map<id,Growth_Plan__c> ([select id,Parent_Growth_Plan_On_CGP__c from Growth_Plan__c where Account__c in: accmapOld.keyset()]);
        
        if(childGrowthPlan.size()<>0)
        {
            for(Growth_Plan__c cgp : ChildGrowthPlan.values())  {
                system.debug('cgp.Parent_Growth_Plan_On_CGP__c'+cgp.Parent_Growth_Plan_On_CGP__c);
                    if((cgp.Parent_Growth_Plan_On_CGP__c) == true)
                    {
                    system.debug('cgp.Parent_Growth_Plan_On_CGP__c in if'+cgp.Parent_Growth_Plan_On_CGP__c);
                    system.debug('var'+var);
                        var=var+1;
                    system.debug('var loop'+var);    
                    }
                }
                if(var>0)
                {
                system.debug('var>0'+var);
                    for(ID aidOld : accmapOld.keyset())  {
                       Account aOld = accmapOld.get(aidOld);
                       system.debug('aOld.Parent_Growth_Plan__c'+aOld.Parent_Growth_Plan__c);
                       aOld.Parent_Growth_Plan__c = true;
                    }
                    
                }
                
                else if(var == 0)
                {
                    for(ID aidOld1 : accmapOld.keyset()) {
                       Account aOld1 = accmapOld.get(aidOld1);
                       aOld1.Parent_Growth_Plan__c = false;
                    }
                    
                }
                update accmapold.values();
                
        }
        
        
        
    
    
        Map<Id,Growth_Plan__c> grMap = new Map<Id,Growth_Plan__c>();
        System.debug('triggernewmap value'+triggernewmap);
        for(Growth_Plan__c gp : triggernewmap.values())
        {
            System.debug('gp value'+gp);
            grMap.put(gp.Parent_Growth_Plan_ID__c,gp);
            system.debug('gp$$$'+gp.Parent_Growth_Plan_ID__c);
            
            
        }
        Map<id,Growth_Plan__c> parentGrowthPlan = new Map<id,Growth_Plan__c> ([Select id,Account__c,Parent_Growth_Plan_On_CGP__c from Growth_Plan__c where id IN:grMap.keyset()]);
        System.debug('parentGrowthPlan'+parentGrowthPlan);
        Map<id,Growth_Plan__c> parentAcc = new Map<id,Growth_Plan__c>();
        for(Growth_Plan__c gp1 : parentGrowthPlan.values())
        {
            system.debug('gp1@@@'+gp1.Parent_Growth_Plan_On_CGP__c);
            gp1.Parent_Growth_Plan_On_CGP__c = true;
            system.debug('gp1###'+gp1.Parent_Growth_Plan_On_CGP__c);
            system.debug('gp1 value'+gp1.Account__c);
            parentAcc.put(gp1.Account__c,gp1);
            system.debug('parentAcc value'+parentAcc);
        }
        update parentGrowthPlan.values();
        
        Map<id,Account> accmap = new Map<id,Account> ([Select id,Parent_Growth_Plan__c from Account where id IN: parentAcc.keyset() ]);
        system.debug('accmap value'+accmap);
        for(ID aid : accmap.keyset())
        {
            Account a = accmap.get(aid);
            a.Parent_Growth_Plan__c = true;
        }   
        update accmap.values();
            
    }
    
    
}