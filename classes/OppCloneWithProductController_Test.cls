@isTest
public class OppCloneWithProductController_Test {
    static testMethod void methodForClone(){
        
        
        Opportunity testOppty1 = new Opportunity();
       Opportunity testOppty2 = new Opportunity();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
        List<User> userList=new List<User>();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 //Employee_office__c='Mercer US Mercer Services - US03'
                                 Employee_office__c='Aarhus - Axel'     
                                );
        
        userList.add(testUser);
        
        User testUser1 = new User(LastName = 'LIVESTON',
                                  FirstName='JAMES',
                                  Alias = 'jim',
                                  Email = 'jason.james12345@asdf.com',
                                  Username = 'jason.james12345@test.com',
                                  ProfileId = profileId.id,
                                  TimeZoneSidKey = 'GMT',
                                  LanguageLocaleKey = 'en_US',
                                  EmailEncodingKey = 'UTF-8',
                                  LocaleSidKey = 'en_US',
                                  //Employee_office__c='Mercer US Mercer Services - US03'
                                 Employee_office__c='Aarhus - Axel'       
                                 );
        
        //userList.add(testUser1);
        insert userList;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            //testAccount.RecordTypeId='012E0000000QxxD';
            testAccount.One_Code__c='Test1';
            insert testAccount;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc11@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            insert testContact;
            
            
            
            
            
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //12638 removing step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today()+1;
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.Probability = 20;
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            testOppty1.Buyer__c=testContact.id;
            insert testOppty1;
          
            
            //Opportunity testOppty2 = new Opportunity();
            testOppty2.Name = 'TestOppty2';
            testOppty2.Type = 'New Client2';
            testOppty2.AccountId = testAccount.Id;
            //12638 removing step
            //testOppty2.Step__c = 'Identified Deal';
            testOppty2.StageName = 'Identify';
            testOppty2.CloseDate = Date.newInstance(Date.today().year()+1, Date.today().month(), Date.today().Day());
            testOppty2.CurrencyIsoCode = 'ALL';
            testOppty2.Probability = 10;
            testOppty2.OwnerId = testUser.Id;
            testOppty2.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty2.Account_Region__c='EuroPac';
            testOppty2.Sibling_Contact_Name__c=testColleague.id;
            testOppty2.Buyer__c=testContact.id;
             testOppty2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Detail Page Assignment').getRecordTypeId();
            }
            
            Test.startTest();
        	 Id pricebookId = Test.getStandardPricebookId();	
		        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        insert pro;
       
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = pro.Id,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice; 
        
       
            List<OpportunityLineItem> listOl = new List<OpportunityLineItem>();
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty1.Id;
            opptylineItem.PricebookEntryId = customPrice.Id;
            opptyLineItem.Revenue_End_Date__c = Date.newInstance(Date.today().year()+1, Date.today().month(), Date.today().Day()+1);
            opptyLineItem.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
            opptyLineItem.UnitPrice = 1.00;
            //opptyLineItem.Cluster__c ='DB delegated solutions';
            opptyLineItem.Sales_Price_USD_calc__c = 1.00;
            opptyLineItem.CurrentYearRevenue_edit__c = 0.00;
            opptyLineItem.Year2Revenue_edit__c = 0.00;
            opptyLineItem.Year3Revenue_edit__c = 1.00;
            listOl.add(opptyLineItem);
            
            OpportunityLineItem opptylineItem1 = new OpportunityLineItem();
            opptylineItem1 .OpportunityId = testOppty1.Id;
            opptylineItem1 .PricebookEntryId = customPrice.Id;
            opptylineItem1 .Revenue_End_Date__c = Date.newInstance(Date.today().year()+2, Date.today().month(), Date.today().Day()+1);
            opptylineItem1 .Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
            opptylineItem1 .UnitPrice = 1.00;
            //opptyLineItem.Cluster__c ='DB delegated solutions';
            opptylineItem1 .Sales_Price_USD_calc__c = 1.00;
            opptylineItem1 .CurrentYearRevenue_edit__c = 0.00;
            opptylineItem1 .Year2Revenue_edit__c = 0.00;
            opptylineItem1 .Year3Revenue_edit__c = 1.00;
            listOl.add(opptyLineItem1);
            
            OpportunityLineItem opptylineItem2 = new OpportunityLineItem();
            opptylineItem2.OpportunityId = testOppty1.Id;
            opptylineItem2.PricebookEntryId = customPrice.Id;
            opptylineItem2.Revenue_End_Date__c = Date.newInstance(Date.today().year()+2, Date.today().month(), Date.today().Day()+1);
            opptylineItem2.Revenue_Start_Date__c = Date.newInstance(Date.today().year()+1, Date.today().month(), Date.today().Day());
            opptylineItem2.UnitPrice = 1.00;
            //opptyLineItem.Cluster__c ='DB delegated solutions';
            opptylineItem2.Sales_Price_USD_calc__c = 1.00;
            opptylineItem2.CurrentYearRevenue_edit__c = 0.00;
            opptylineItem2.Year2Revenue_edit__c = 0.00;
            opptylineItem2.Year3Revenue_edit__c = 1.00;
            listOl.add(opptylineItem2);
            insert listOl;
            
            Sales_Credit__c testSales = new Sales_Credit__c();
            testSales.Opportunity__c = testOppty1.Id;
            testSales.EmpName__c = testColleague.Id;
            testSales.Percent_Allocation__c = 19;
            insert testSales;
            
            OppCloneWithProductLtngController cntrl=new OppCloneWithProductLtngController();
          
            OppCloneWithProductLtngController.getStages(false);
            OppCloneWithProductLtngController.getChargeCodeExepVal();
            OppCloneWithProductLtngController.getOffice();
            OppCloneWithProductLtngController.getType();
            OppCloneWithProductLtngController.getcloseStageReasonVal();
            OppCloneWithProductLtngController.getOpportunityRecordDetails(testOppty1.Id,True,false);
            OppCloneWithProductLtngController.saveClonedOpportunity(JSON.serialize(testOppty2), testOppty1, false);
            
            //OppCloneWithProductLtngController.cloneSalesCredit(testOppty1.id);
            //OppCloneWithProductLtngController.checkExpansion = true;
            OppCloneWithProductLtngController.getOpportunityRecordDetails(testOppty1.Id,false,false);
            OppCloneWithProductLtngController.errorOrId result=OppCloneWithProductLtngController.saveClonedOpportunity(JSON.serialize(testOppty2), testOppty1, false);
            Opportunity opp =[select id, stageName from Opportunity where id=:result.clonedId];
            system.assertEquals(opp.stageName,'Identify');
            
            Test.stopTest();
        
        
        
    }
   static testMethod void methodForExpansion(){
           
         Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
        List<User> userList=new List<User>();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                //Employee_office__c='Mercer US Mercer Services - US03'
                                 Employee_office__c='Aarhus - Axel'      
                                );
        
        userList.add(testUser);
        
        User testUser1 = new User(LastName = 'LIVESTON',
                                  FirstName='JAMES',
                                  Alias = 'jim',
                                  Email = 'jason.james12345@asdf.com',
                                  Username = 'jason.james12345@test.com',
                                  ProfileId = profileId.id,
                                  TimeZoneSidKey = 'GMT',
                                  LanguageLocaleKey = 'en_US',
                                  EmailEncodingKey = 'UTF-8',
                                  LocaleSidKey = 'en_US',
                                 //Employee_office__c='Mercer US Mercer Services - US03'
                                 Employee_office__c='Aarhus - Axel'       
                                 );
        
        //userList.add(testUser1);
        insert userList;
        System.runAs(testUser){
          ConstantsUtility.STR_ACTIVE = 'Inactive';
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
           // testAccount.RecordTypeId='012E0000000QxxD';
            testAccount.One_Code__c='Test1';
            insert testAccount;
           
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc11@xyz.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            insert testContact;
            
            TriggerSettings__c  trg = new TriggerSettings__c ();
            trg.name='Project Trigger';
            trg.Object_API_Name__c='Revenue_Connectivity__c'; trg.Trigger_Disabled__c = true;
            insert trg;
            
            
            Opportunity testOppty1 = new Opportunity();
            
            Id RecordTypeLockedId =[select id from Recordtype where name='Opportunity Locked'].Id;
            
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //12638 removing step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.Probability = 20;
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            testOppty1.Buyer__c=testContact.id;
            testOppty1.recordtypeId = RecordTypeLockedId ;
            insert testOppty1;
          
            
            Opportunity testOppty2 = new Opportunity();
            testOppty2.Name = 'TestOppty2';
            testOppty2.Type = 'New Client2';
            testOppty2.AccountId = testAccount.Id;
            //12638 removing step
            //testOppty2.Step__c = 'Identified Deal';
            testOppty2.StageName = 'Closed / Won';
            testOppty2.CloseDate = Date.newInstance(Date.today().year()+1, Date.today().month(), Date.today().Day());
            testOppty2.CurrencyIsoCode = 'ALL';
            testOppty2.Probability = 10;
            testOppty2.OwnerId = testUser.Id;
            testOppty2.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty2.Account_Region__c='EuroPac';
            testOppty2.Sibling_Contact_Name__c=testColleague.id;
            testOppty2.Buyer__c=testContact.id;
            testOppty2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Product Expansion').getRecordTypeId();
            
           Test.starttest();
            Id pricebookId = Test.getStandardPricebookId();	
		        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        insert pro;
     
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = pro.Id,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice;  
            
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty1.Id;
            opptylineItem.PricebookEntryId = customPrice.Id;
            opptyLineItem.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+1);
            opptyLineItem.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
            opptyLineItem.UnitPrice = 1.00;
            opptyLineItem.Sales_Price_USD_calc__c = 1.00;
            opptyLineItem.CurrentYearRevenue_edit__c = 0.00;
            opptyLineItem.Year2Revenue_edit__c = 0.00;
            opptyLineItem.Year3Revenue_edit__c = 1.00;
            insert opptylineItem;
            
            String IdOppLineItem = opptylineItem.Id;
            Revenue_Connectivity__c rc = new Revenue_Connectivity__c();
            rc.Opportunity_Product_Id__c = IdOppLineItem;
            rc.Opportunity__c = testOppty1.Id;
            insert rc;
            //Test.startTest();
            Sales_Credit__c testSales = new Sales_Credit__c();
            testSales.Opportunity__c = testOppty1.Id;
            testSales.EmpName__c = testColleague.Id;
            testSales.Percent_Allocation__c = 19;
            insert testSales;
            
            testOppty1.stageName = 'Closed / Won';
            update testOppty1;
            
            OppCloneWithProductLtngController cntrl=new OppCloneWithProductLtngController();
            OppCloneWithProductLtngController.getProdStatusCount(testOppty1.id);
            OppCloneWithProductLtngController.getStages(false);
            OppCloneWithProductLtngController.getChargeCodeExepVal();
            OppCloneWithProductLtngController.getcloseStageReasonVal();
            OppCloneWithProductLtngController.getcloseStageReasonVal();
            OppCloneWithProductLtngController.getOptyStage(String.valueOf(testOppty1.id));
            OppCloneWithProductLtngController.getOpportunityRecordDetails(testOppty1.Id,false,true);
            OppCloneWithProductLtngController.errorOrId result=OppCloneWithProductLtngController.saveClonedOpportunity(JSON.serialize(testOppty2), testOppty1, true);
            Opportunity opp = [SELECT id, StageName from Opportunity where id=:result.clonedId];
            system.assertEquals(opp.StageName,testOppty1.stageName);
            
            Test.stopTest();
        }
        
   }
        
    
}