/* Purpose:This test class provides data coverage to AP77_OpportunityReminderShedulable  class.
===================================================================================================================================
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          12/11/2013  Created test class.
 ======================================================================================================================================
 */
@isTest(seeAllData=true)
    private class Test_AP77_OpportunityReminderShedulable 
    {

	   /* 
		* @Description : This test method provides data coverage for AP77_OpportunityReminderShedulable schedulable class             
        * @ Args       : no arguments        
        * @ Return     : void       
        */
       static testMethod void testOpportunitySchedulable()  
       { 
                         
             AP77_OpportunityReminderShedulable oppsch = new AP77_OpportunityReminderShedulable();      
             DateTime r = DateTime.now();     
             String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';       
             system.schedule('AP77_OpportunityReminderShedulable', nextTime, oppsch);           
               
       }
  }