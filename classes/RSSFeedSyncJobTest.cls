@isTest
public class RSSFeedSyncJobTest {

    @IsTest
    static void test_rss_feed_download() {
        
        // The RSS sample was taken from http://www.w3schools.com/xml/xml_rss.asp and modified
        // to only include one item
        String httpResponse = '<?xml version="1.0" encoding="UTF-8" ?> <rss version="2.0"> <channel> <title>W3Schools Home Page</title> <link>http://www.w3schools.com</link> <description>Free web building tutorials</description> <item> <title>RSS Tutorial</title> <link>http://www.w3schools.com/xml/xml_rss.asp</link> <description>New RSS tutorial on W3Schools</description> <guid>1234</guid> </item> </channel> </rss>';
        
        // set http callout mock
        Test.setMock( HttpCalloutMock.class, new RSSFeedMockHttpCallout( httpResponse ) );
    
        Test.startTest();
        
        RSSFeedDownloadService.RSSOptions options = new RSSFeedDownloadService.RSSOptions();
        options.feedType = RSSFeedDownloadService.FEED_TYPE_BRINK;
        System.debug('%%%%% options.feedType: ' + options.feedType);
        RSSFeedDownloadService.RSSFeed feed = RSSFeedDownloadService.downloadRSSFeed( 'link ignored due to mock callout', options);
        
        Test.stopTest();
    }

    @isTest
    static void testRSSFeedSync() {
        
        String httpResponse = '<?xml version="1.0" encoding="UTF-8" ?> <rss version="2.0"> <channel> <title>W3Schools Home Page</title> <link>http://www.w3schools.com</link> <description>Free web building tutorials</description> <item> <title>RSS Tutorial</title> <link>http://www.w3schools.com/xml/xml_rss.asp</link> <description>New RSS tutorial on W3Schools</description> <guid>1234</guid> </item> </channel> </rss>';
        
        // set http callout mock
        Test.setMock( HttpCalloutMock.class, new RSSFeedMockHttpCallout( httpResponse ) );
    
        RSS_Feed__c feed = new RSS_Feed__c(
            Title__c = 'My Test RSS Feed',
            URL__c = 'https://select.mercer.com/ps/82112/rss/',
            Active__c = true,
            Type__c = 'MSI',
            Sync_Schedule__c = '0 0 * * * ?'
        );
        
        insert feed;
        System.debug('%%%%% feed: ' + feed);
            
        Test.startTest();
        
        // sync the feed
        RSSFeedDownloadService.RSSOptions options = new RSSFeedDownloadService.RSSOptions();
        options.feedType = RSSFeedDownloadService.FEED_TYPE_BRINK;
        RSSFeedSyncJob.syncFeeds(options.feedType);
        
        // get count of feed items for feed
        Integer count = [SELECT count() FROM News__c WHERE RSS_Feed__c = :feed.Id];
        System.debug('%%%%% count: ' + count);
        
        Test.stopTest();
    }

    @IsTest
    static void test_RSSFeedSyncSchedulableJobs() {
        
        String httpResponse = '<?xml version="1.0" encoding="UTF-8" ?> <rss version="2.0"> <channel> <title>W3Schools Home Page</title> <link>http://www.w3schools.com</link> <description>Free web building tutorials</description> <item> <title>RSS Tutorial</title> <link>http://www.w3schools.com/xml/xml_rss.asp</link> <description>New RSS tutorial on W3Schools</description> <guid>1234</guid> </item> </channel> </rss>';
        
        // set http callout mock
        Test.setMock( HttpCalloutMock.class, new RSSFeedMockHttpCallout( httpResponse ) );

        RSS_Feed__c msiRssFeed = new RSS_Feed__c();
        msiRssFeed.Active__c = true;
        msiRssFeed.URL__c = 'https://select.mercer.com/ps/82112/rss/';
        msiRssFeed.Type__c = 'MSI';

        insert msiRssFeed;
        
        RSS_Feed__c brinkRssFeed = new RSS_Feed__c();
        brinkRssFeed.Active__c = true;
        brinkRssFeed.URL__c = ' http://www.brinknews.com/feed/';
        brinkRssFeed.Type__c = 'Brink';

        insert brinkRssFeed;
        
        
        Test.startTest();

        String cronSchedule = '0 0 23 * * ?';
            
        RSSFeedMsiSyncSchedulableJob msiJob = new RSSFeedMsiSyncSchedulableJob();
        RSSFeedBrinkSyncSchedulableJob brinkJob = new RSSFeedBrinkSyncSchedulableJob();

        System.schedule('Msi Job', cronSchedule, msiJob);
        System.schedule('Brink Job', cronSchedule, brinkJob);

        Test.stopTest();
    }
    
    @isTest
    static void test_RSSFeedSyncDirectCalls() {
        String httpResponseBrink = '<?xml version="1.0" encoding="UTF-8" ?> <rss version="2.0"> <channel> <title>W3Schools Home Page</title> <link>http://www.w3schools.com</link> <description>Free web building tutorials</description> <item> <title>RSS Tutorial</title> <link>http://www.w3schools.com/xml/xml_rss.asp</link> <pubDate>23 Aug 2017 05:00:19 +0000</pubDate> <description>New RSS tutorial on W3Schools</description> <guid>1234</guid> </item> </channel> </rss>';
        String httpResponseMsi = '<?xml version="1.0" encoding="UTF-8" ?> <rss version="2.0"> <channel> <title>W3Schools Home Page</title> <link>http://www.w3schools.com</link> <description>Free web building tutorials</description> <item> <title>RSS Tutorial</title> <link>http://www.w3schools.com/xml/xml_rss.asp</link> <PubDate>23 Aug 2017</PubDate> <description>New RSS tutorial on W3Schools</description> <guid>1234</guid> </item> </channel> </rss>';
        
        RSS_Feed__c msiRssFeed = new RSS_Feed__c();
        msiRssFeed.Active__c = true;
        msiRssFeed.URL__c = 'https://select.mercer.com/ps/82112/rss/';
        msiRssFeed.Type__c = 'MSI';

        insert msiRssFeed;
        
        RSS_Feed__c brinkRssFeed = new RSS_Feed__c();
        brinkRssFeed.Active__c = true;
        brinkRssFeed.URL__c = ' http://www.brinknews.com/feed/';
        brinkRssFeed.Type__c = 'BRINK';

        insert brinkRssFeed;
        
        
        Test.startTest();

        RSSFeedMsiSyncSchedulableJob msiJob = new RSSFeedMsiSyncSchedulableJob();
        RSSFeedBrinkSyncSchedulableJob brinkJob = new RSSFeedBrinkSyncSchedulableJob();

        // set http callout mock
        Test.setMock( HttpCalloutMock.class, new RSSFeedMockHttpCallout( httpResponseMsi ) );
        msiJob.runExecuteMethod();

        Test.setMock( HttpCalloutMock.class, new RSSFeedMockHttpCallout( httpResponseBrink ) );
        brinkJob.runExecuteMethod();

        Test.stopTest();
    }
}