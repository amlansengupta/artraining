/*
Purpose: This test class provide data coverage to AP25_LockOpportunitySchedulable schedulable class 
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  	DATE        DETAIL    
 1.0 -    	 Shashank 	03/29/2013  Created Tets class
=============================================================================================================================================== 
*/
@isTest
private class Test_AP25_LockOpportunitySchedulable 
{
	/*
     * @Description : Test method to provide data coverage to AP25_LockOpportunitySchedulable schedulable class  
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        AP25_LockOpportunitySchedulable AMsch = new AP25_LockOpportunitySchedulable();
    	String sch = '0 0 23 * * ?';
	    system.schedule('Test AP25_LockOpportunitySchedulable', sch, AMsch);
    }
}