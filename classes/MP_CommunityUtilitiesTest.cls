/**
 * Created by emaleesoddy on 5/10/17.
 */
@isTest
public with sharing class MP_CommunityUtilitiesTest {

    @isTest
    public static void testGetCurrentUser() {
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User user = peakTestUtils.createStandardUser();

        insert user;

        system.runAs(user) {
            system.assertEquals(user.Id, MP_CommunityUtilities.getCurrentUser().Id);
        }
    }

    @isTest
    public static void testGetAccountInfo() {
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User user = peakTestUtils.createStandardUser();

        insert user;

        system.runAs(user) {
            system.assertEquals(user.Id, MP_CommunityUtilities.getAccountInfo().user.Id);
        }
    }

    @isTest
    public static void testGetSitePrefix() {
        system.assert(Peak_Utils.getSitePrefix() != null);
    }


    @isTest
    public static void testGetGroupKeyContact() {
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User testUser = peakTestUtils.createStandardUser();
        CollaborationGroup testGroup = peakTestUtils.createGroup(Peak_TestConstants.TEST_GROUPNAME,'Public');

        insert testGroup;
        insert testUser;

        User queryUser = MP_CommunityUtilities.getGroupKeyContact(testGroup.Id);

    }

    @isTest
    public static void testGetPrivateGroup() {
        // Create user
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        User testUser = peakTestUtils.createStandardUser();

        insert testUser;

        System.runAs(testUser) {
            Peak_Response peakResponse = MP_CommunityUtilities.getPrivateGroup();
        }
    }

}