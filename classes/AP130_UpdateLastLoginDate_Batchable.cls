/*Purpose:  This Batch Apex class is created as a Part of July 2015 Release for Req:6292
==============================================================================================================================================
The below code when executed will perform the following:
- Update the user record's Last Browser Login Date (if user has logged in last 24 Hrs)
- Exclude certain users and profiles from above update. The list of the profiles and users is given in the "UsersAndProfilesList" custom setting.

History 
----------------------------------------------------------------------------------------------------------------------
VERSION - 1.0
AUTHOR - C. Pon Venkatesh
DATE - 05/21/2015
============================================================================================================================================== 
*/
global class AP130_UpdateLastLoginDate_Batchable implements Database.Batchable<sObject>, Database.Stateful{
    
    Public String query;
    public Static Map<Id, Date> testDataPrep = new Map<Id, Date>();
    List<BatchErrorLogger__c> errorLogger = new List<BatchErrorLogger__c>();
    List<String> user_List {get; set;}
    List<String> profile_List {get; set;}
    public static final string STR_Brower = 'Browser';
    public static final string STR_UpdateLastBrowserLoginDate ='UpdateLastBrowserLoginDate_Batchable';
    public static final string STR_colon = ';';
    public static final string STR_limit = ' Limit 100';
    
    /* 
        Constructor for the class 
    */    
    global AP130_UpdateLastLoginDate_Batchable(){
        
        // Query to fetch user records who have logged-in in last 24 hours         
        query = Label.UpdateLastBrowserLoginDate_Query;
        //Date loginDateCheck = Date.newInstance(2016, 4, 1);
        //query = 'Select Application,LoginTime,Status,UserId FROM LoginHistory';
        
    }
    
    /*
        Batch initialization to fetch all the records for exectution   
     */  
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        
        // Limit number of records to be fetched to 100 if batch running in Test mode            
        if(Test.isRunningTest())
        {
            query += STR_limit;
        }
        
        return Database.getQueryLocator(query); 
    }
    
    /*
     *  Method Name: execute
     *  Description: Updating LastBrowerLoginDate on User 
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<LoginHistory> userHistoryList = new List<LoginHistory>();
        Map<Id, User> usersToBeUpdated_Map = new Map<Id, User>();
        
        userHistoryList  = (List<LoginHistory>)scope;
        
        // If running in Test mode, then put the queried data in testDataPrep for testing.         
        if(Test.isRunningTest()){
            for(LoginHistory LH : userHistoryList){
                if(STR_Brower.equals(LH.Application)){
                    LoginHistory reEnterRecord = new LoginHistory();
                    userHistoryList.add(reEnterRecord);
                    Break;
                }
            }
            for(LoginHistory LH : userHistoryList){
                if(testDataPrep.get(LH.UserId)!=null){
                    
                    // Check if user has logged in multiple times through the browser. And if Yes, get the latest login time                  
                    if(STR_Brower.equals(LH.Application) && testDataPrep.get(LH.UserId)<=date.newinstance(LH.LoginTime.year(),LH.LoginTime.month(),LH.LoginTime.day())){
                        testDataPrep.put(LH.UserId, date.newinstance(LH.LoginTime.year(),LH.LoginTime.month(),LH.LoginTime.day()));
                    }
                }else{
                    if(STR_Brower.equals(LH.Application)){
                        testDataPrep.put(LH.UserId, date.newinstance(LH.LoginTime.year(),LH.LoginTime.month(),LH.LoginTime.day()));
                    }
                }
            }
        }
        
        // Get the List of Users and Profiles to be excluded from the batch from UpdateLastBrowserLoginDate_Batchable custom setting
        user_List = new List<String>();
        profile_List  = new List<String>();
        UsersAndProfilesList__c toBeExcluded = UsersAndProfilesList__c.getValues(STR_UpdateLastBrowserLoginDate);
        if(toBeExcluded.Profiles_List__c != null)
        profile_List = toBeExcluded.Profiles_List__c.split(STR_colon);
        if(toBeExcluded.Users_List__c != null)
        user_List = toBeExcluded.Users_List__c.split(STR_colon);
        Map<Id, User> excludedUsersMap = new Map<Id, User>([Select Id from User where Id IN :user_list OR ProfileId IN :profile_List]); 
        
        // Check the below debug log to see if profiles and users being excluded appropriately
        System.debug('Excluded List====>Profiles:' + profile_List + ', Users:' + user_List);
        List<ID> userIds = new List<ID>();
        for(LoginHistory li : userHistoryList){
            userIds.add(li.UserId);
        }
        
         Map<Id,User> existingUsers = new Map<Id, User>([Select Id,Last_Browser_Login_Date__c  from User where Id IN: userIds]);
        // Prepare the list of users to be updated
        for(LoginHistory li : userHistoryList){
            if(excludedUsersMap.get(li.UserId)==null&&existingUsers.get(li.UserId)!=null){
                if(STR_Brower.equals(li.Application)){
                    //if(!excludedUserIds_List.containsKey(li.UserId)){
                        User u = new User();
                        u.id = li.UserId;
                        Date lastLoginDate = date.newinstance(li.LoginTime.year(),li.LoginTime.month(),li.LoginTime.day());
                        u.Last_Browser_Login_Date__c = lastLoginDate;
                        if(usersToBeUpdated_Map.containsKey(li.UserId)){ 
                            
                            // Check if user has logged in multiple times through the browser. And if Yes, get the latest login time                            
                            if(usersToBeUpdated_Map.get(li.UserId).Last_Browser_Login_Date__c <= lastLoginDate)
                                usersToBeUpdated_Map.put(li.UserId, u);
                        }else{
                            if(existingUsers.get(li.UserId).Last_Browser_Login_Date__c == null || existingUsers.get(li.UserId).Last_Browser_Login_Date__c <= lastLoginDate ){
                                usersToBeUpdated_Map.put(li.UserId, u);
                            }
                        }
                    //}
                }
            }
        }
        
        /*
         * Run an update to update the list of users prepared above
         */
        if(!usersToBeUpdated_Map.values().isEmpty()){
            List<Database.SaveResult> sr = Database.update(usersToBeUpdated_Map.values(), false);
            for(Database.SaveResult s : sr){
                if(!s.Success){
                   BatchErrorLogger__c error = new BatchErrorLogger__c ();
                   error.Failed_Reason__c = String.valueOf(s.getErrors().get(0));
                   error.Record_Id__c = s.getId(); 
                   errorLogger.add(error);
                }
            }
            //insert errorLogger;
         }
    }  
    
    
    global void finish(Database.BatchableContext bc){
        If(errorLogger.size()!=0 || !errorLogger.isEmpty())
            insert errorLogger;
    }

}