@isTest(seeAllData = true)
private class NewGrowthPlanController_Test {

    
    @isTest static void UnitTest() {
        Account Acc = new Account(name = 'Test Account',BillingCountry='test',Account_Country__c='USA');
        insert Acc;
        
        Contact con = new Contact(LastName='test',AccountID=Acc.Id,Email='Test1813@gmail.com');
        insert con;
        
        FCST_Fiscal_Year_List__c py = new FCST_Fiscal_Year_List__c(Name = string.valueOf(system.today().year()+1),StartDate__c = date.newinstance(Integer.valueOf(system.today().year()+1),1,1),EndDate__c = date.newinstance(Integer.valueOf(system.today().year()+1),12,31));
        insert py;
        
        Growth_Plan__c gp = new Growth_Plan__c(Account__c = Acc.id,fcstPlanning_Year__c= py.id);
        insert gp;
                
        
        ApexPages.StandardController sc = new ApexPages.StandardController(gp);
        
        PageReference pageRef = Page.New_Growth_Plan;
        pageRef.getParameters().put('Accid', String.valueOf(Acc.Id));        
        Test.setCurrentPage(pageRef);
        
        NewGrowthPlanController contrl = new NewGrowthPlanController(sc);
        
        //Methods
        contrl.onSelectAccount();
        contrl.saveGrowthPlan();
        contrl.cancel();
        
        
    }

    
}