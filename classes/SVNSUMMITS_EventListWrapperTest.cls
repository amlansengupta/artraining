@isTest
public with sharing class SVNSUMMITS_EventListWrapperTest {

    @isTest
    public static void testGetProperties() {
        SVNSUMMITS_EventListWrapper eventListWrapper = new SVNSUMMITS_EventListWrapper(Peak_TestConstants.FIELDSTRING,Peak_TestConstants.TEST_DESCRIPTION);

        system.assert(eventListWrapper.objEventList == null);
        system.assert(eventListWrapper.isEditable == null);
        system.assert(eventListWrapper.topicNameToId == null);
        system.assert(eventListWrapper.topicLst == null);
        system.assert(eventListWrapper.intDate == null);
        system.assert(eventListWrapper.strDay == null);
        system.assert(eventListWrapper.strEndDate == null);
        system.assert(eventListWrapper.strMinute == null);
        system.assert(eventListWrapper.strMonth == null);
        system.assert(eventListWrapper.strStartDate == null);
        system.assert(eventListWrapper.strTimeZone == null);
        system.assert(eventListWrapper.daysOfMultiDaysEvent == null);
        system.assert(eventListWrapper.topicsOfRecord == null);
        system.assert(eventListWrapper.totalResults == null);
        system.assert(eventListWrapper.totalPages == null);
        system.assert(eventListWrapper.dtNow == null);
        system.assert(eventListWrapper.hasNextSet == null);
        system.assert(eventListWrapper.hasPreviousSet == null);
        system.assert(eventListWrapper.listSizeValue == null);
        system.assert(eventListWrapper.eventsToTopicsMap == null);
        system.assert(eventListWrapper.pageNumber == null);
        // Set in constructor, so not null!
        system.assert(eventListWrapper.errorMsg != null);
        system.assert(eventListWrapper.field != null);
    }

    @isTest
    public static void testFullConstructor() {
        Peak_TestUtils peakTestUtils = new Peak_TestUtils();
        Set<String> eventListIDs = new Set<String>();
        Event__c testEvent = peakTestUtils.generateEvent(false);
        insert testEvent;

        Event__c testEvent2 = peakTestUtils.generateEvent(true);
        insert testEvent2;

        eventListIDs.add(testEvent2.id);

        // Full constructor
        SVNSUMMITS_EventListWrapper svnsummitsEventListWrapper = new SVNSUMMITS_EventListWrapper(Peak_TestConstants.EVENTS_QUERY_STRING, 1, Peak_TestConstants.FROMDT, Peak_TestConstants.TODATE, eventListIDs, 'List', false, null);

        // Alternate
        SVNSUMMITS_EventListWrapper svnsummitsEventListWrapperAlt = new SVNSUMMITS_EventListWrapper(Peak_TestConstants.EVENTS_QUERY_STRING, 1, Peak_TestConstants.FROMDT, Peak_TestConstants.TODATE, eventListIDs, 'Calendar', false, null);

        // Alternate with featured
        Map<String,String> featuredEventMap = new Map<String,String>{(String)testEvent.id => 'Huh'}; 
        SVNSUMMITS_EventListWrapper svnsummitsEventListWrapperFeatured = new SVNSUMMITS_EventListWrapper(Peak_TestConstants.EVENTS_QUERY_STRING, 1, Peak_TestConstants.FROMDT, Peak_TestConstants.TODATE, eventListIDs, 'Calendar', true, featuredEventMap);
    }


    @isTest
    public static void testPaging() {
        SVNSUMMITS_EventListWrapper svnsummitsEventListWrapper = new SVNSUMMITS_EventListWrapper(Peak_TestConstants.EVENTS_QUERY_STRING, 1, Peak_TestConstants.FROMDT, Peak_TestConstants.TODATE, Peak_TestConstants.STRINGSET, 'Calendar', false, null);
        svnsummitsEventListWrapper.pageNumber = 1;

        svnsummitsEventListWrapper.nextPage();
        system.assertEquals(0,svnsummitsEventListWrapper.pageNumber);

        svnsummitsEventListWrapper.previousPage();
        system.assertEquals(0,svnsummitsEventListWrapper.pageNumber);

    }

}