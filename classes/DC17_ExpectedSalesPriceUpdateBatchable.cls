/* Purpose: This Batchable class updates respective Opportunity records and populates Expected Sales Price field
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Oportunity as an input and updates Opp record to  populate Expected Sales Price  field.
 
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Arijit          06/08/2013    Created Batch Class to update Expected Sales Price.
 2.0         Sarbpreet       05/06/2014   PMO#3566(June Release) : Updated the code to refer it to current currency rates rather than dated currency 
 3.0         Sarbpreet       7/7/2014     Added Comments.
 ======================================================================================================================================*/
global class DC17_ExpectedSalesPriceUpdateBatchable implements database.Batchable<sObject>
{
    global static final string query = 'select Id, UnitPrice, Expected_Product_Sales_Price_USD__c, OpportunityId from OpportunityLineItem where StageName NOT IN (\'Closed / Won\',\'Closed / Lost\',\'Closed / Declined to Bid\',\'Closed / Client Cancelled\') AND CALENDAR_YEAR(CloseDate) >= 2015';
    global static final string testQuery = 'select Id, UnitPrice, Expected_Product_Sales_Price_USD__c, OpportunityId from OpportunityLineItem where Opportunity.Name = \'TestOpptySalesPriceBatch\' limit 1';
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global database.QueryLocator start(database.batchableContext BC)
    {
        if(!Test.isRunningTest())
        return database.getQueryLocator(query);
        else
        return database.getQueryLocator(testQuery);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(database.BatchableContext BC, List<sObject> scope)
    {
        List<OpportunityLineItem> lineItems = (List<OpportunityLineItem>)scope;
        List<OpportunityLineItem> lineItemsToUpdate = new List<OpportunityLineItem>();
        set<string> opportunityIdSet = new set<string>();
        set<string> ISOCodeSet = new set<string>();
        //Updated as part of PMO#3566(June Release)
        List<CurrencyType> exchangeRates = new List<CurrencyType>();
        Map<string, Opportunity> opportunityMap = new Map<string, Opportunity>();
        
        for(OpportunityLineItem lineItem:lineItems)
        {
            opportunityIdSet.add(lineItem.OpportunityId);
        }   
        for(Opportunity opp:[select Id, CurrencyISOCode, Probability, CloseDate from Opportunity where Id IN :opportunityIdSet])
        {
            ISOCodeSet.add(opp.CurrencyISOCode);
            opportunityMap.put(opp.Id, opp);
        }
        
        if(!IsoCodeSet.isEmpty())               
                //Updated as part of PMO#3566(June Release)
                exchangeRates = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE ISOCode in :IsoCodeSet ];
                
        for(OpportunityLineItem lineItem:lineItems)
        {
            Opportunity opp = opportunityMap.get(lineItem.OpportunityId);
            if(opp.CurrencyIsoCode<>null&&opp.CloseDate<>null)
            {
                //Updated as part of PMO#3566(June Release)
                for(CurrencyType currencyVal : exchangeRates)
                {
                    //if(currencyVal.IsoCode<>null&&currencyVal.StartDate<>null&&currencyVal.NextStartDate<>null)
                    if(currencyVal.IsoCode<>null)                   
                    {
                          if(currencyVal.IsoCode == opp.CurrencyIsoCode)    
                            {
                                if(lineItem.UnitPrice<>null)
                                lineItem.Expected_Product_Sales_Price_USD__c=lineItem.UnitPrice*currencyVal.ConversionRate*opp.Probability/100;
                                lineItemsToUpdate.add(lineItem);
                            }
                    }
                }
            }
        } 
        if(!lineItemsToUpdate.isEmpty())update lineItemsToUpdate;
    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC){}
}