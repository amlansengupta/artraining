/* Purpose: This test class provides dat coverage to AP73_UserDeactivationSchedulable  class
===================================================================================================================================
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          08/23/2013  Created test class
 ======================================================================================================================================*/
@isTest(seeAllData=false)
    private class Test_AP73_UserDeactivationSchedulable 
    {

        /* * @Description : This test method provides data coverage for AP73_UserDeactivationSchedulable schedulable class             
           * @ Args       : no arguments        
           * @ Return     : void       
        */
       static testMethod void testUserDeactivationSchedulable()  
       { 
             Test.StartTest();  
              
             User user1 = new User();
             String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
             user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            
             system.runAs(User1){          
                 AP73_UserDeactivationSchedulable usersch = new AP73_UserDeactivationSchedulable();      
                 DateTime r = DateTime.now();     
                 String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';       
                 system.schedule('AP73_UserDeactivationSchedulable', nextTime, usersch);    
        }       
             Test.StopTest();   
       }
  }