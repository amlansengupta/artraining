/*Handler Class for Project Object*/
/*
====================================================================================
Request Id                       Date                    Modified By
12638:Replacing step with Stage   18-Jan-2109               Trisha
=====================================================================================
*/
public class ProjectTriggerHandler implements ITriggerHandler
{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('Project Trigger');
    
        if (TrgSetting.Object_API_Name__c == 'Revenue_Connectivity__c' && TrgSetting.Trigger_Disabled__c== true) {
            return true;
        }else{
            return TriggerDisabled;
        }
    }
 
    public void BeforeInsert(List<SObject> newItems) {
        /*system.debug('Gopal***' + newItems + ':::' + newItemsMap);
        Map<Id, SObject> newItemsMap1 = new Map<Id, SObject>();
        for(SObject s:newItems){
            newItemsMap1.put(s.id, s);
        }*/
        //updateColleagueandProudct(newItems, newItemsMap1);
        AP125_ProjectTriggerClass.updateColleagueandProudct(newItems);
    }
 
    public void BeforeUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
        AP125_ProjectTriggerClass.updateColleagueandProudct(newItemsMap.values());
    }
  
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterInsert(List<SObject> newItems,Map<Id, SObject> newItemsMap) {
        system.debug('***Project Trigger After Insert');
        CheckOliLinkwithWebcas(newItemsMap);
        closeCheckonProductProject( newItemsMap);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItems) {
        system.debug('***Project Trigger After Update');
        CheckOliLinkwithWebcas(newItemsMap);
        closeCheckonProductProject(newItemsMap);
    }
 
    public void AfterDelete(Map<Id, SObject> oldItems) {}
 
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    
    public void CheckOliLinkwithWebcas(Map<Id, SObject> newMap){
        
        Set<String> oliId=new Set<String>();
        Map<id,Revenue_Connectivity__c> prodProjectMap=new Map<id,Revenue_Connectivity__c>();
        List<OpportunityLineItem> olitobeupdated=new List<OpportunityLineItem>();
        
        Map<id,Revenue_Connectivity__c> ProjectMap=new Map<id,Revenue_Connectivity__c>([select id,Project_LOB_Formula__c,Project_Status__c,Charge_Basis__c,Opportunity_Product_Id__c,Product__c from Revenue_Connectivity__c where Id In:newMap.keyset()]);
        
        if(ProjectMap.size()>0){
        
            for(Revenue_Connectivity__c rc:ProjectMap.values()){
                oliId.add(rc.Opportunity_Product_Id__c);
                prodProjectMap.put(rc.Product__c,rc);
                //System.debug('^^'+prodProjectMap);
            }
            Map<id,OpportunityLineItem> oliMap=new Map<id,OpportunityLineItem>([Select id,LOB__c,Project_Linked__c,Product2Id from OpportunityLineItem where Id IN:oliId]);
            for(OpportunityLineItem oli:oliMap.values()){
                //if(oli.Project_Linked__c==false){
                    Revenue_Connectivity__c rev=prodProjectMap.get(oli.Product2Id);
                   // System.debug('&&'+rev+oli.LOB__c+rev.Project_LOB_Formula__c);
                    if(prodProjectMap.ContainsKey(oli.Product2Id) && oli.LOB__c == rev.Project_LOB_Formula__c && (rev.Charge_Basis__c == 'S'||rev.Charge_Basis__c == 'R'||rev.Charge_Basis__c == 'T'||rev.Charge_Basis__c == 'C') ){                    
                        oli.Project_Linked__c=true;
                        olitobeupdated.add(oli);
                        //System.debug('%%'+olitobeupdated);
                    }else{
                        oli.Project_Linked__c=false;
                        olitobeupdated.add(oli);
                    } 
                //}           
            }
            try{
            update olitobeupdated;
            }
            Catch(Exception e){
            System.debug(e.getMessage()+':'+e.getLineNumber());
            }
        }
    }
    
    /**
     * Method to query Colleague id and Proudct id 
     * based on information from WebCAS and associate them to look up fields in Project object
     */
   /* public void updateColleagueandProudct(List<SObject> triggernew, Map<Id, SObject> triggernewMap){
        
        Set<String> empnoSet = new Set<String>();
        set<String> prodcodeset = new set<String>();
        Map<String,id> collMap = new Map<String,id>();
        Map<String,id> prodMap = new Map<String,id>();
        Map<String,OpportunityLineItem> oppprodMap = new Map<String,OpportunityLineItem>();
        List<Colleague__c> lstColl = new List<Colleague__c>();
        LIst<product2> lstprod = new List<Product2>();
        Set<String> setProdId = new Set<String>();
        List<opportunitylineitem> lstoppprod = new List<opportunitylineitem>();
        List<Id> lstOpp = new List<Id>();
        Map<Id, Id> mapRevOpp = new Map<Id, Id>();
        
        Map<id,Revenue_Connectivity__c> ProjectMap=new Map<id,Revenue_Connectivity__c>([select id,Project_Solution__c,Project_Manager__c,Opportunity_Product_Id__c,Product__c from Revenue_Connectivity__c where Id In:triggernewMap.keyset()]);
        
        try {
            //Loop around trigger.new and take the project manager and project solution values in to two sets
            for (Revenue_Connectivity__c rev:ProjectMap.values()){
        
                if(rev.Project_Manager__c <> null)
                    empnoSet.add(rev.Project_Manager__c);
                
                if(rev.Project_Solution__c <> null)    
                    prodcodeset.add(rev.Project_Solution__c);
                    
                if(rev.Opportunity_Product_Id__c <> null)
                    setProdId.add(rev.Opportunity_Product_Id__c);
                
               
            }     
            
            System.debug('Debug125..1..setProdId..'+setProdId +'.prodcodeset..'+prodcodeset+'.empnoSet..'+empnoSet);
            //Query from colleague and project objects
            if(empnoset.size() > 0)
                lstColl = [select id,emplid__c from Colleague__c where emplid__c in :empnoset];
                
            if(prodcodeset.size()>0)
                lstprod = [select id,isActive, ProductCode from Product2 where isActive = true and productcode in :prodcodeset];
            
            if(setProdId.size()>0)
                lstoppprod = [select id,opportunityid,opportunity.name,opportunity.ownerId from opportunitylineitem where id in:setProdId];
            System.debug('Debug125..1..lstoppprod ..'+lstoppprod +'.lstprod ..'+lstprod +'.lstColl ..'+lstColl );
            //Loop around colleague and product objects and put the values in map    
            for(Colleague__c c:lstColl){ collMap.put(c.emplid__c, c.id); }
            
            for(product2 p: lstprod){
                prodMap.put(p.productcode, p.id);
            }
            
            for(opportunitylineitem oppprod:lstoppprod){
                String oppid = oppprod.id;
                oppprodMap.put(oppid.subString(0,15), oppprod);
            }
             System.debug('Debug125..3..oppprodMap..'+oppprodMap +'.prodMap..'+prodMap+'.collMap..'+collMap);    
             System.debug('Debug125..4..'+mapRevOpp);
            //use the values in map to assign the ids to the look up fields in project object.    
            for (Revenue_Connectivity__c rev:ProjectMap.values()){        

                if(rev.Project_Manager__c <> null && !collMap.isEmpty() && collMap.containsKey(rev.Project_Manager__c)){rev.Colleague__c = collMap.get(rev.Project_Manager__c);}
                
                if(rev.Project_Solution__c <> null && !prodMap.isEmpty() && prodMap.containsKey(rev.Project_Solution__c))    
                    rev.Product__c = prodMap.get(rev.Project_Solution__c);


            }
        }
        catch(Exception e){
            system.debug('Exception '+e.getmessage());
        }
        
    }*/
    Public static void closeCheckonProductProject( Map<Id, sObject> triggernewMap){
        List<Id> lstOpp = new List<Id>();
        Map<Id, Id> mapRevOpp = new Map<Id, Id>();
         string STR_AP125 = 'AP125_ProjectTriggerClass';
         Map<String, String> errorMap = new Map<String, String>();
        Map<id,Revenue_Connectivity__c> ProjectMap=new Map<id,Revenue_Connectivity__c>([select id,Project_Solution__c,Opportunity__c,Project_Manager__c,Opportunity_Product_Id__c,Product__c from Revenue_Connectivity__c where Id In:triggernewMap.keyset()]);
        for (Revenue_Connectivity__c rev:ProjectMap.values()){        
          
            lstOpp.add(rev.Opportunity__c);
            mapRevOpp.put(rev.Opportunity__c, rev.Id);

        }
        /*********requestId:12638 removing step__c from query and adding stageName start*******/
        /*Map<Id, Opportunity> OppQueryMap = new Map<Id, Opportunity>([Select Id, Step__c, Ownerid,Opportunity_Office__c, Name from Opportunity where Id IN: lstOpp]);*/
        Map<Id, Opportunity> OppQueryMap = new Map<Id, Opportunity>([Select Id, stageName, Ownerid,Opportunity_Office__c, Name from Opportunity where Id IN: lstOpp]);
        /*********requestId:12638 removing step__c from query and adding stageName end*******/
        if(lstOpp.size()>0) {
           AP125_ProjectTriggerClass.updateOpportunityStep(OppQueryMap, STR_AP125); 
        }
        for(String error : errorMap.keySet()){ 
            triggernewMap.get(mapRevOpp.get(error)).addError(errorMap.get(error));
        }    
     }
}