public class UpdateGPFYPlanRevenue { 
    
    public static void updateGPValue(List<Forecast_Model__c> forecastModelList){
        Map<Id,List<Forecast_Model__c>> mapGPObjVsFMList = new Map<Id,List<Forecast_Model__c>>();
        Set<Id> growthPlanIdSet = new Set<Id>();
        List<Growth_Plan__c> updateGPList = new List<Growth_Plan__c>();
        
        for(Forecast_Model__c fmObj:forecastModelList){
            growthPlanIdSet.add(fmObj.Growth_Plan__c);
           
          }
        
        List<Growth_Plan__c> growthPlanList=[Select Total_Prior_Revenue_Actual__c,Id,Name,TOT_Projectd_Full_Year_Revenue__c,Total_Pre_Prior_Revenue_Actual__c,Total_Prior_LTM_Revenue__c,Total_Prior_FY_Projected__c,Total_Retained_Revenue__c,Total_New_Revenue__c,Total_Current_FY_Revenue__c from Growth_Plan__c where Id IN:growthPlanIdSet ];
        List<Forecast_Model__c> totalFMList = [select Prior_Revenue_Actual__c,Id,Growth_Plan__c,Current_FY_Revenue__c,Sub_Business__c,Pre_Prior_FY_Actual__c,Prior_LTM_Revenue__c,Prior_FY_Projected__c,Current_Carry_Forward_Revenue__c from Forecast_Model__c where Growth_Plan__c IN:growthPlanIdSet and LOB__c!='Global Planning'];
        for(Forecast_Model__c fmObj3:totalFMList){
            
             if(fmObj3.Sub_Business__c==null){
               if(mapGPObjVsFMList.containsKey(fmObj3.Growth_Plan__c)){
               List<Forecast_Model__c> fModelList1 =mapGPObjVsFMList.get(fmObj3.Growth_Plan__c);
               fModelList1.add(fmObj3);
               mapGPObjVsFMList.put(fmObj3.Growth_Plan__c,fModelList1);
             }
             else{
               List<Forecast_Model__c> newFMList = new List<Forecast_Model__c>();
               newFMList.add(fmObj3);
               mapGPObjVsFMList.put(fmObj3.Growth_Plan__c,newFMList);
             }
                
            }
        }
        
        
        
        for(Growth_Plan__c gPObj:growthPlanList){
            Decimal fYPlanRevenue=0,totalPrePriorRevActual=0,totalLTM=0,totalPriorFyProjected=0,TotalPriorActual=0,totalRetainedRevenue=0;
            
            List<Forecast_Model__c> fModelList2 =mapGPObjVsFMList.get(gPObj.Id);
            for(Forecast_Model__c fmObj2:fModelList2){
                if(fmObj2.Sub_Business__c==null && fmObj2.Current_FY_Revenue__c!=null){
                    fYPlanRevenue+=fmObj2.Current_FY_Revenue__c;
                }
                if(fmObj2.Sub_Business__c==null && fmObj2.Pre_Prior_FY_Actual__c!=null){
                    totalPrePriorRevActual+=fmObj2.Pre_Prior_FY_Actual__c;
                }
                if(fmObj2.Sub_Business__c==null && fmObj2.Prior_LTM_Revenue__c!=null){
                    totalLTM+=fmObj2.Prior_LTM_Revenue__c;
                }
                if(fmObj2.Sub_Business__c==null && fmObj2.Prior_FY_Projected__c!=null){
                    totalPriorFyProjected+=fmObj2.Prior_FY_Projected__c;
                }
                if(fmObj2.Sub_Business__c==null && fmObj2.Current_Carry_Forward_Revenue__c!=null){
                    totalRetainedRevenue+=fmObj2.Current_Carry_Forward_Revenue__c;
                }
                if(fmObj2.Sub_Business__c==null && fmObj2.Prior_Revenue_Actual__c!=null){
                    TotalPriorActual+=fmObj2.Prior_Revenue_Actual__c;
                }
            }
            
            gPObj.TOT_Projectd_Full_Year_Revenue__c=fYPlanRevenue;
            gPObj.Total_Current_FY_Revenue__c=fYPlanRevenue;//1
            
            gPObj.Total_Pre_Prior_Revenue_Actual__c=totalPrePriorRevActual;//1
            gPObj.Total_Prior_LTM_Revenue__c=totalLTM;//1
            gPObj.Total_Prior_FY_Projected__c=totalPriorFyProjected;//1
            gPObj.Total_Prior_Revenue_Actual__c=TotalPriorActual;
            gPObj.Total_Retained_Revenue__c=totalRetainedRevenue;//1
            
            
            System.debug('fYPlanRevenue>>>'+fYPlanRevenue);
            updateGPList.add(gPObj);
        }
       
        if(updateGPList.size()>0){
             System.debug('updateGPList>>>'+updateGPList[0].Id);
            update updateGPList;
        }
    
    }

}