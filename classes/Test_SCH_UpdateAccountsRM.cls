@isTest
private class Test_SCH_UpdateAccountsRM
{
  
    static testMethod void Test_SCH_UpdateAccountsRM() 
    {
        Test.StartTest();
         
        User user1 = new User();
      String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert7', 'usrLstName7', 7);
        
        system.runAs(User1){
            SCH_UpdateAccountsRM bc = new SCH_UpdateAccountsRM();
            String sch = '0 0 23 * * ?';
            system.schedule('Test SCH_UpdateAccountsRM', sch, bc);
        }
        Test.stopTest();
    }
}