/*
* @Description : Test Class for AP112_CreateTalentTaskSchedulable schedule class
*  
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)

Public Class AP112_CreateTalentTaskSchedulable_test {      

    static testMethod void myUnitTest() {
      
        
        insertTalentTsMappingData();
        
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='TAL123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.userid = userinfo.getUserid();
        atm.Accountid= acc.id;
        atm.teammemberrole = 'Talent Solutions Strategic Account Mgr';
        insert atm;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'Rebid';
        testOppty.AccountId = acc.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Dallas - Main';
        Test.startTest();
        insert testOppty; 
       
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'ePRISM Implementation';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        test.stoptest();
        insert opptylineItem;
        
        Date after60 = System.today()+60;
        Talent_Contract__c tc = new Talent_Contract__c();
        tc.Account__c = acc.id;
        tc.Opportunity__c = testOppty.id;
        tc.Product__c = 'WIN ePRISM';
        tc.License_Expiration_Date__c = after60;
        insert tc;
                              
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('testBasicScheduledApex',CRON_EXP, new AP112_CreateTalentTaskSchedulable());
        
    }
    
       Static void insertTalentTsMappingData(){
        
        TalentTSProductMapping__c ts = new TalentTSProductMapping__c();
        ts.name = 'T01';
        ts.Opportunity_Product_Name__c = 'ePRISM Ongoing Subscription';
        ts.Talent_Contract_Product_Name__c = 'WIN ePRISM';
        insert ts;
        
        TalentTSProductMapping__c ts2 = new TalentTSProductMapping__c();
        ts2.name = 'T02';
        ts2.Opportunity_Product_Name__c = 'HC Connect Subscription';
        ts2.Talent_Contract_Product_Name__c = 'Human Capital Connect';
        insert ts2;
        
        TalentTSProductMapping__c ts3 = new TalentTSProductMapping__c();
        ts3.name = 'T03';
        ts3.Opportunity_Product_Name__c = 'ePRISM Implementation';
        ts3.Talent_Contract_Product_Name__c = 'WIN ePRISM';
        insert ts3;
        
        TalentTSProductMapping__c ts4 = new TalentTSProductMapping__c();
        ts4.name = 'T04';
        ts4.Opportunity_Product_Name__c = 'HC Connect Implementation';
        ts4.Talent_Contract_Product_Name__c = 'Human Capital Connect';
        insert ts4;
    }
}