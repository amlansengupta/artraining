/*

==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR        DATE                 DETAIL    
1.0        Arijit Roy    04/12/2013      Created a Schedulable class for MS14_AccountCompetitorBatchable.  
============================================================================================================================================== 
*/

global class MS13_AccountCompetitorSchedulable implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS14_AccountCompetitorBatchable(), Integer.valueOf(System.Label.CL78_MS14BatchSize));
    }
}