global class PastDueOpptyRemindSchedulable implements Schedulable {
    global PastDueOpptyRemindSchedulable(){
        
    }
    
    global void execute(SchedulableContext context)
    {
        AP111_PastDueOpptyRemindBatchable b = new AP111_PastDueOpptyRemindBatchable();     
        Id batchJobId = Database.executeBatch(b,20);
    }
}