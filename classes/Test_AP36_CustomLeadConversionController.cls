/*Purpose:  This test class provide data coverage to AP36_CustomLeadConversionController  class
=================================================================================================================================================
History 
----------------------- 
 VERSION     AUTHOR  DATE        DETAIL   
 1.0 -    Shashank 14/08/2013  Created Test class 
 2.0 -    Venkat   17/07/2015  Modified test class for Req#5166
 ================================================================================================================================================= 
*/
@isTest
private class Test_AP36_CustomLeadConversionController 
{
    /*Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        ApexConstants__c setting1 = new ApexConstants__c();
        setting1.Name = 'AP02_OpportunityTriggerUtil_2';
        setting1.StrValue__c = 'Seoul;Taipei;';
        insert setting1;
        
        ApexConstants__c setting2 = new ApexConstants__c();
        setting2.Name = 'Above the funnel';
        setting2.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting2;
        
        ApexConstants__c setting3 = new ApexConstants__c();
        setting3.Name = 'Identify';
        setting3.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting3;
        
        ApexConstants__c setting4 = new ApexConstants__c();
        setting4.Name = 'Selected';
        setting4.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting4;
        
        ApexConstants__c setting5 = new ApexConstants__c();
        setting5.Name = 'Active Pursuit';
        setting5.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting5;
        
        ApexConstants__c setting6 = new ApexConstants__c();
        setting6.Name = 'Finalist';
        setting6.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting6;
    }*/
    
    /*
     * @Description : Test method to handle Custom Lead Conversion Functioanlity 
     * @ Args       : Null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest2() 
    {
       
           
        //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678909';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Country__c = 'TestCountry';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = 'ABC';
        //insert testAccount;
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.AccountID = testAccount.id;
        con.email = 'test1@email.com';
        //insert con;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);       
        system.runAs(User1){       
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
            lead.Email = 'test3@email.com';
            insert lead;
            
           
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
            controller.check = true;
            controller.lead = lead;
            controller.contact = con;
            //controller.contact.AccountId = testAccount.Id;
            controller.account = testAccount;  
            /* Test Class Modification - TCS 5-Apr-2019--START*/          
              controller.account.Relationship_Manager__c = testColleague.Id;
              controller.account.Country__c = 'Test Country';
              controller.account.Billingcity='Test City';
               controller.account.BillingStreet='Test Street';
            controller.getStageValue();
            controller.convertLead();
           Contact cont = [select id , Email from Contact];
          System.assertEquals(cont.Email,'test3@email.com');
          /* Test Class Modification - TCS 5-Apr-2019--END*/
            Test.stopTest();
            
        }
    }
    /*
     * @Description : Test method to handle Custom Lead Conversion Functioanlity 
     * @ Args       : Null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest3() 
    {
      
       //createCS();
       
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678908';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';  
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = 'ABC';
        insert testAccount;
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.AccountID = testAccount.id;
        con.email = 'test1@email.com';
        //insert con;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);
        
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
            lead.Email = 'test2@email.com';
            insert lead;            
          
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
           // controller.check = false;
            controller.lead = lead;
            controller.contact = con;
            //controller.contact.accountId = testAccount.Id;
             controller.convertLead();
              /* Test Class Modification - TCS 5-Apr-2019--START*/ 
             Contact cont=[select id, Email from Contact];
             System.assertEquals(cont.Email,'test2@email.com');
              /* Test Class Modification - TCS 5-Apr-2019--END*/ 
            Test.stopTest();

    }
  
        
    
    /*
     * @Description : Test method to handle Custom Lead Conversion Functioanlity 
     * @ Args       : Null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest4() 
    {
      
       //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';  
        testAccount.Relationship_Manager__c = testColleague.Id;
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.email = 'test1@email.com';

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);
        
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
            lead.Email = 'test2@email.com';
            insert lead;            
          
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
            controller.check = true;
            controller.lead = lead;
            controller.contact = con;
            controller.account = testAccount;
            /* Test Class Modification - TCS 5-Apr-2019--START*/          
              controller.account.Relationship_Manager__c = testColleague.Id;
              controller.account.Country__c = 'Test Country';
              controller.account.Billingcity='Test City';
               controller.account.BillingStreet='Test Street';
            controller.getStageValue();
            controller.convertLead();
           Contact cont = [select id , Email from Contact];
          System.assertEquals(cont.Email,'test2@email.com');
          /* Test Class Modification - TCS 5-Apr-2019--END*/
            controller.convertLead();
            Test.stopTest();

    }  
    
    /*
     * @Description : Test method to handle Custom Lead Conversion Functioanlity 
     * @ Args       : Null
     * @ Return     : void
     */ 
    static testMethod void myUnitTest5() 
    {
      
       //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678903';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';  
        testAccount.Relationship_Manager__c = testColleague.Id;
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.email = 'test1@email.com';
        
        Lead prelead = new Lead();
        prelead.FirstName =  'Test FirstName';
        prelead.LastName = 'Test LastName';
        prelead.Company = 'Test Company';
        prelead.Title = 'Test Title';
        prelead.Status = 'Open';
        prelead.Email = 'test2@email.com';
        insert prelead ; 

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);
        
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
                        
            insert lead;            
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
            controller.check = true;
            controller.lead = lead;
            controller.lead.Email = 'test2@email.com';
            controller.contact = con;
            controller.lead.Email = 'test3@email.com';
            controller.contact.email =  controller.lead.Email;
            controller.account = testAccount;
            /* Test Class Modification - TCS 5-Apr-2019--START*/          
              controller.account.Relationship_Manager__c = testColleague.Id;
              controller.account.Country__c = 'Test Country';
              controller.account.Billingcity='Test City';
               controller.account.BillingStreet='Test Street';
            controller.getStageValue();
            controller.convertLead();
           Contact cont = [select id , Email from Contact];
          System.assertEquals(cont.Email,'test3@email.com');
          /* Test Class Modification - TCS 5-Apr-2019--END*/
            controller.convertLead();
            Test.stopTest();

    }  

    static testMethod void myUnitTest6() 
    {
       //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678904';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';  
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.Country__c = 'TestCountry';
        testAccount.BillingCity = 'Sample';
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.email = 'test1@email.com';
        
        Lead prelead = new Lead();
        prelead.FirstName =  'Test FirstName';
        prelead.LastName = 'Test LastName';
        prelead.Company = 'Test Company';
        prelead.Title = 'Test Title';
        prelead.Status = 'Open';
        prelead.Email = 'test2@email.com';
        insert prelead ; 

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);
        
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
            
            insert lead;            
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
            controller.check = true;
            controller.accName = 'testName';
            controller.oppName ='TestOPPName';
            controller.lead = lead;
            controller.lead.Email = 'test2@email.com';
            controller.contact = con;
            controller.account = testAccount;
            //controller.checkOpp =true;
            controller.convertLead();
            try{
                controller.accName = null;
                controller.convertLead();
                }
                catch(DmlException e){}
            controller.getDuplicateRecords();
            /* Test Class Modification - TCS 5-Apr-2019--START*/  
           List<Contact> listcont = [select id , Email from Contact];
          System.assertEquals(listcont.size(),0);
          /* Test Class Modification - TCS 5-Apr-2019--END*/    
            Test.stopTest();

    }  
    
    static testMethod void myUnitTest7() 
    {
       //createCS();
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678905';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';  
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.Country__c = 'TestCountry';
        testAccount.BillingCity = 'Sample';
        
        contact con = new contact();
        con.LastName = 'testContact';
        con.email = 'test1@email.com';
        
        Lead prelead = new Lead();
        prelead.FirstName =  'Test FirstName';
        prelead.LastName = 'Test LastName';
        prelead.Company = 'Test Company';
        prelead.Title = 'Test Title';
        prelead.Status = 'Open';
        prelead.Email = 'test2@email.com';
        insert prelead ; 

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile , 'usert2', 'usrLstName2', 2);
        
            Lead lead = new Lead();
            lead.FirstName =  'Test FirstName';
            lead.LastName = 'Test LastName';
            lead.Company = 'Test Company';
            lead.Title = 'Test Title';
            lead.Status = 'Open';
            lead.Email = 'test4@email.com';
            insert lead;   
                     
            PageReference pref = Page.Mercer_CustomLeadConversion;
            pref.getParameters().put('id',lead.id);
            
            Test.startTest();
            
            Test.setCurrentPage(pref);
            ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(lead);
            AP36_CustomLeadConversionController controller = new AP36_CustomLeadConversionController(stdController);
            
             controller.account.Relationship_Manager__c = testColleague.Id;
              controller.account.Country__c = 'Test Country';
              controller.account.Billingcity='Test City';
               controller.account.BillingStreet='Test Street';
            controller.getStageValue();
            controller.convertLead();
            
            controller.check = true;
            controller.accName = 'testName';
            controller.oppName ='TestOPPName';
            controller.check = true;
            controller.lead = lead;
            controller.contact = con;
            //controller.contact.AccountId = testAccount.Id;
            controller.account = testAccount;
            //controller.contact.AccountId = testAccount.Id;
           // controller.account = testAccount;  
            /* Test Class Modification - TCS 5-Apr-2019--START*/          
              controller.account.Relationship_Manager__c = testColleague.Id;
              controller.account.Country__c = 'Test Country';
              controller.account.Billingcity='Test City';
               controller.account.BillingStreet='Test Street';
               controller.stageVal = 'Identify';         
            controller.checkOpp =true;             
            controller.opportunity.opportunity_office__c='Aarhus - Axel';
             controller.opportunity.closeDate= System.today();
              controller.opportunity.Type ='New Client';
              controller.opportunity.CurrencyIsoCode='USD';
              controller.opportunity.OwnerId=UserInfo.getUserId();
            controller.convertLead(); 
            Opportunity opp =[select id,stageName from Opportunity];
            system.assertEquals(opp.stageName,'Identify'); 
               //Contact cont = [select id , Email from Contact]; 
            /* Test Class Modification - TCS 5-Apr-2019--END*/
            
            try{
                controller.accName = null;
                controller.convertLead();
                controller.updateAccountAddress(testAccount.Id);
                }
                catch(DmlException e){}
            controller.getDuplicateRecords();    
            Test.stopTest();

    } 

}