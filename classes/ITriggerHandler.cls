/*used for TriggerDispatcher framework*/
public interface ITriggerHandler 
{
    void BeforeInsert(List<SObject> newItems);
 
    void BeforeUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap);
 
    void BeforeDelete(Map<Id, SObject> newItemsMap);
 
    void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemsMap);
 
    void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap);
 
    void AfterDelete(Map<Id, SObject> oldItemsMap);
 
    void AfterUndelete(Map<Id, SObject> oldItemsMap);
 
    Boolean IsDisabled();
}