/*Purpose:  User Trigger Business Helper 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE          DETAIL 
1.0 -    Reetika   07/19/2013  if User is being made active and was not active previously ,ten set u.Newly_Licenensed__c = true , 
so that batch can process Accounts for the colleage related to user (if there is any)
2.0      Sarbpreet 28/09/203    Added  functionality to automatically remove Supervisor when user is deactivated
3.0 -    Sarbpreet 7/4/2014     Updated Comments/documentation.
4.0 -    Sarbpreet 07/22/2014   As part of PMO Request 4355(September Release) added logic to remove S-Dash Supervisors from inactive users
5.0 -    Jagan     11/17/2014   PMO Request 3480 - added logic to update Employee Status Change Date
6.0 -    Sarbpreet 10/12/2015   As per Request #7445 Dec Release 2015, updated logic to update Goals field on User object on the basis of individual sales goal
7.0 -    Bala      10/15/2015   As per Request #7439 Dec Release 2015, updated the logic to track the changes on Role,Profile and Isactive field.
8.0        Dhanusha  27/01/2016  added login removing MH licenses of inactive and lite users (Request :6605 Relaese: March2016)
9.0 -    Jigyasu     21/04/2016 As per Request #10298 some conditions are modified. 
============================================================================================================================================== 
*/
public class AP67_userTriggerUtil {
    // Added as part of PMO Request 4355(September Release)
    private static final String EXCEPTION_STR = 'Exception occured with reason :' ;
    public Static Boolean Isrecursive = False;
    
    //Mix DML Fix Start
    public static Boolean updatedFromUser = False;
    //Mix DML Fix End
    
    /*
* @Description : This method contains all business logic to be executed before user insert
* @ Args       : List<User> lstNewUser
* @ Return     : void
*/
    public static void processUserBeforeInsert(List<User> lstNewUser) {
        
        integer i = 0;
        
        for(i=0;i<lstNewUser.size();i++) {
            // to enable license of user
            enableUserLicense(lstNewUser[i],null,false);
        }
        // Added as per Request #7445 Dec Release 2015
        updateUserGoal(lstNewUser);
    } 
    
    /*
* @Description : This method contains all business logic to be executed after user insert
* @ Args       : List<User> lstNewUser
* @ Return     : void
*/
    public static void processUserAfterInsert(List<User> lstNewUser)
    {   
        List<Id> lstUserIds = new List<Id>();     
        
        Profile prof = [Select Id,name From Profile p where Name = 'Chatter External User' limit 1];
        //generateChatterMember(lstNewUser);
        For(User u : lstNewUser)
        {
            if(u.ProfileId != prof.Id && u.isActive==True)
            {
                lstUserIds.add(u.Id);
            }
            
        }
        
        if(!lstUserIds.isEmpty())
            createChatterMember(lstUserIds);
        
        
        
    } 
    /*
* @Description : This method contains all business logic to be executed before user update
* @ Args       : List<User> lstNewUser , List<User> lstOldUser
* @ Return     : void
*/
    public static void processUserBeforeUpdate(List<User> lstNewUser , List<User> lstOldUser) {
        
        
        
        integer i = 0;
        for(i=0;i<lstNewUser.size();i++) {
            User usr = lstNewUser[i];
            // As part of PMO Request 4355(September Release) calling method to remove S-Dash Supervisors from inactive users                         
            if(!usr.IsActive && usr.IsActive != lstOldUser[i].IsActive)
            {  
                removeSDashSupervisors(usr);
            }
            
            //6605
            Map<ID,String>  profileMap = new Map<ID,String>();
            
            ApexConstants__c deactivationSetting = ApexConstants__c.getValues('LiteProfiles');
            List<String> deactivationProfiles = deactivationSetting.StrValue__c.split(';');
            
            for(String profile : deactivationProfiles){
                profileMap.put(profile, profile);
            }
            if((!usr.IsActive && usr.IsActive != lstOldUser[i].IsActive ) || (profileMap.get(usr.ProfileId) != null) )
            {  
                RemoveMHLicenses(usr);
            }
            
            // end of 6605
            //Update Employee Status Change Date (December 2014 Release)
            if( lstNewUser[i].Empl_Status__c != lstOldUser[i].Empl_Status__c)
            {  
                usr.Employee_Status_Change_Date__c = system.today();
            }
            
            // to enable license of user
            enableUserLicense(lstNewUser[i],lstOldUser[i],true);
            
        } 
        // Added as per Request #7445 Dec Release 2015
        updateUserGoal(lstNewUser);
        
        
        
    }  
    /*
* @Description : This method contains all business logic to be executed after user update
* @ Args       : Map<Id,User>NewUsermap,Map<Id,User> OldUserMap
* @ Return     : void
*/
    public static void processUserAfterUpdate(Map<Id,User>NewUsermap,Map<Id,User> OldUserMap) 
    {
        List<Id> lstUserIds = new List<Id>();
        
        Profile prof = [Select Id,name From Profile p where Name = 'Chatter External User' limit 1];
        // Below code will be executed only when we have activated any user and Newly Activated User is not having Chatter External User Profile.
        For(ID uid:NewUsermap.KeySet())
        {
            if(NewUsermap.get(uid).isActive == True && OldUserMap.get(uid).isActive == False && NewUsermap.get(uid).ProfileId != prof.Id)
            {
                lstUserIds.add(uid);
            }
            
        }
        if(!lstUserIds.isEmpty())
            createChatterMember(lstUserIds);
        
        
    }    
    
    @future
    /*
* @Description : Future method for creating chatter member
* @ Args       : List<Id> lstIds
* @ Return     : void
*/
    public static void createChatterMember(List<Id> lstIds)
    {
        List<CollaborationGroupMember> lstGroupMem = new List<CollaborationGroupMember>();
        // Logic New user will be part of AllMercerForceUsers Chatter Group
        String grpName = Label.All_MercerForce_Users;
        //Fetch the ID of "All MercerForce Users" Group.
        List<CollaborationGroup> cg = new List<CollaborationGroup>() ;
        cg =  [Select Name,Id From CollaborationGroup where Name = :grpName limit 1]; 
        
        
        // Set of Members of Group
        Set<String> setGrpMembers = new Set<String>();
        if(cg!=null && cg.size()>0) {
            
            List<CollaborationGroupMember> lstcgm = [Select CollaborationGroupId,MemberId,id from CollaborationGroupMember where CollaborationGroupId =: cg[0].Id];
            for (CollaborationGroupMember cgm:lstcgm)
            {
                setGrpMembers.add(cgm.MemberId);
            }
            
            // Create Record of collobration Group Member
            for(Id uId :lstIds) 
            {
                
                // if user is not already part of Group,Then add it to Group
                if(!setGrpMembers.contains(uId) && cg!=Null )
                {
                    CollaborationGroupMember cgm = new CollaborationGroupMember();
                    cgm.CollaborationGroupId = cg[0].Id;
                    cgm.MemberId = uId;
                    cgm.CollaborationRole = 'Standard';
                    lstGroupMem.add(cgm);
                    System.debug('@@@@ Chatter Grp Mamber @@@'+cgm);
                }
            }
        }
        System.debug('@@@@ lstGroupMem @@@@'+lstGroupMem);
        if(!lstGroupMem.isEmpty())
            Database.insert(lstGroupMem);
    }        
    
    /* functionality : when User is being made active and was not active previously ,then set u.Newly_Licenensed__c = true ,
so  that batch can process Accounts for the colleage related to user (if there is any)
*/
    public static void enableUserLicense(User newusr , User oldusr , boolean isUpdate ) {
        
        if((newusr.IsActive && isUpdate==false) || (isUpdate==true && newusr.IsActive && newusr.IsActive!=oldusr.IsActive )) {
            newusr.Newly_Licensed__c = true;
        }
    }
    
    /*
* @Description : As part of PMO Request 4355(September Release): Method to remove S-Dash Supervisors from inactive users
* @ Args       : List<Id> lstIds
* @ Return     : void
*/
    public static void removeSDashSupervisors(User susr) {
        try {
            if(susr.S_Dash_Supervisor__c <> null || susr.S_Dash_Supervisor_2__c<> null ||  susr.S_Dash_Supervisor_3__c<> null || susr.S_Dash_Supervisor_4__c<> null ||  susr.S_Dash_Supervisor_5__c <> null) 
            {
                susr.S_Dash_Supervisor__c = null;
                susr.S_Dash_Supervisor_2__c = null;
                susr.S_Dash_Supervisor_3__c = null;
                susr.S_Dash_Supervisor_4__c = null;
                susr.S_Dash_Supervisor_5__c = null;
            }
        }
        catch (Exception e) {                              
            susr.addError(EXCEPTION_STR+e.getMessage());
            
        }
    }
    
    //6605
    
    
    public static void RemoveMHLicenses(User susr) {
        try {
            if(susr.Blue_Sheet_Managers_License__c == TRUE || susr.Blue_Sheet_Read_Only_License__c == TRUE||  susr.mh_Blue_Sheet_Read_Write_License__c == TRUE || susr.Green_Sheet_Managers_License__c == TRUE ||  susr.Green_Sheet_Read_Only_License__c == TRUE ||  susr.Green_Sheet_Read_Write_License__c == TRUE ||  susr.Gold_Sheet_Managers_License__c == TRUE ||  susr.Gold_Sheet_Read_Only_License__c == TRUE || susr.Gold_Sheet_Read_Write_License__c == TRUE) 
            { 
                susr.Blue_Sheet_Managers_License__c = false;
                susr.Blue_Sheet_Read_Only_License__c = false;
                susr.mh_Blue_Sheet_Read_Write_License__c = false;
                susr.Green_Sheet_Managers_License__c = false;
                susr.Green_Sheet_Read_Only_License__c = false;
                susr.Green_Sheet_Read_Write_License__c = false;
                susr.Gold_Sheet_Managers_License__c = false;
                susr.Gold_Sheet_Read_Only_License__c = false;
                susr.Gold_Sheet_Read_Write_License__c = false;
                //  userlstforupdate.add(u);
            }
        }
        catch (Exception e) {                              
            susr.addError(EXCEPTION_STR+e.getMessage());
            
        }
    }
    
    // end of 6605
    
    /*



/*
* @Description : As part of PMO Request 7439 (December Release): Method to Track who had made the changes on User Record for Email,
Profile, Role or Isactivated.
* @ Args       : Map<id,User> UserMap
* @Returns     : void
*/
    public static void usertracker(Map < Id, User > NewUsermap, Map < Id, User > OldUserMap) {
        
        Map < id, Profile > mapprofilename = new map < id, Profile > ([select id, name from profile]);
        
        Map < id, UserRole > maprolename = new map < id, UserRole > ([Select id, Name from UserRole]);
        
        for (User u: NewUsermap.values()) {
            
            if (u.Track_User_History__c != null) {
                if (u.Track_User_History__c.length() >= 32600) {
                    u.Backup_User_History__c = u.Track_User_History__c;
                    u.Track_User_History__c = null;
                }
            }
            if (OldUserMap.get(u.id).ProfileId != u.ProfileId) {
                
                u.Logged_In_User__c = UserInfo.getUserId();
                
                if (!Isrecursive) {
                    
                    if (u.Track_User_History__c == null) {
                        
                        u.Track_User_History__c = '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Profile :</b>' + mapprofilename.get(OldUserMap.get(u.id).ProfileId).Name + '<div style="page-break-after: always;"></div>' + '<b>New Profile :</b>' + mapprofilename.get(u.ProfileID).Name;
                    } else {
                        u.Track_User_History__c = u.Track_User_History__c + '<div style="page-break-after: always;">====================================</div>' + '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Profile :</b>' + mapprofilename.get(OldUserMap.get(u.id).ProfileId).Name + '<div style="page-break-after: always;"></div>' + '<b>New Profile :</b>' + mapprofilename.get(u.ProfileID).Name;
                    }
                }
            }
            
            if (OldUserMap.get(u.id).Email != u.Email) {
                
                u.Logged_In_User__c = UserInfo.getUserId();
                
                if (!Isrecursive) {
                    
                    if (u.Track_User_History__c == null) {
                        
                        u.Track_User_History__c = '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Email :</b>' + OldUserMap.get(u.id).Email + '<div style="page-break-after: always;"></div>' + '<b>New Email :</b>' + u.Email;
                    } else {
                        u.Track_User_History__c = u.Track_User_History__c + '<div style="page-break-after: always;">====================================</div>' + '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Email :</b>' + OldUserMap.get(u.id).Email + '<div style="page-break-after: always;"></div>' + '<b>New Email :</b>' + u.Email;
                    }
                }
            }
            if (OldUserMap.get(u.id).Isactive != u.Isactive) {
                
                u.Logged_In_User__c = UserInfo.getUserId();
                
                if (!Isrecursive) {
                    
                    if (u.Track_User_History__c == null) {
                        
                        u.Track_User_History__c = '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>IsActive :</b>' + OldUserMap.get(u.id).Isactive + '<div style="page-break-after: always;"></div>' + '<b>IsActive :</b>' + u.Isactive;
                    } else {
                        u.Track_User_History__c = u.Track_User_History__c + '<div style="page-break-after: always;">====================================</div>' + '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>IsActive :</b>' + OldUserMap.get(u.id).Isactive + '<div style="page-break-after: always;"></div>' + '<b>IsActive :</b>' + u.Isactive;
                    }
                }
            }
            if(OldUserMap.get(u.id).UserRoleID==NULL||u.UserRoleID==NULL){
                String OldRole, NewRole;
                if(OldUserMap.get(u.id).UserRoleID == NULL){
                    OldRole = 'Blank';
                }else{
                    OldROle = maprolename.get(OldUserMap.get(u.id).UserRoleID).Name;
                }
                if(u.UserRoleID == NULL){
                    NewRole = 'Blank';
                }else{
                    NewRole = maprolename.get(u.UserRoleID).Name;
                }
                if (!Isrecursive) {
                    if (u.Track_User_History__c == null) {
                        
                        u.Track_User_History__c = '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Role :</b>' + OldRole  + '<div style="page-break-after: always;"></div>' + '<b>New Role :</b>' + NewRole;
                    } else {
                        u.Track_User_History__c = u.Track_User_History__c + '<div style="page-break-after: always;">====================================</div>' + '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Role :</b>' + OldRole  + '<div style="page-break-after: always;"></div>' + '<b>New Role :</b>' + NewRole ;
                    }
                }
            }else{
                if (OldUserMap.get(u.id).UserRoleID != u.UserRoleID) {
                    
                    u.Logged_In_User__c = UserInfo.getUserId();
                    
                    if (!Isrecursive) {
                        
                        if (u.Track_User_History__c == null) {
                            
                            u.Track_User_History__c = '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Role :</b>' + maprolename.get(OldUserMap.get(u.id).UserRoleID).Name + '<div style="page-break-after: always;"></div>' + '<b>New Role :</b>' + maprolename.get(u.UserRoleID).Name;
                        } else {
                            u.Track_User_History__c = u.Track_User_History__c + '<div style="page-break-after: always;">====================================</div>' + '<b>Modified By:</b>' + UserInfo.getName() + '<div style="page-break-after: always;"></div>' + '<b>Modified Date:</b>' + System.now() + '<div style="page-break-after: always;"></div>' + '<b>Old Role :</b>' + maprolename.get(OldUserMap.get(u.id).UserRoleID).Name + '<div style="page-break-after: always;"></div>' + '<b>New Role :</b>' + maprolename.get(u.UserRoleID).Name;
                        }
                    }
                }
            }
        }
        
        Isrecursive = true;
    }
    
    /*
* @Description : As per Request #7445 Dec Release 2015, this method updates Goals field on User object on the basis of individual sales goal
* @ Args       : List<User> userGoalList
* @ Return     : void
*/
    public static void updateUserGoal(List<User> userGoalList) {
        Map<String, User> userEmpIDmap = new Map<String, User>();
        Map<String, Colleague__c> colleagueEmpIDMap = new Map<String, Colleague__c>();
        Map<ID, Colleague__c> colleagueMap = new Map<ID, Colleague__c>();
        List<Individual_Sales_Goal__c> indSalesGoalList = new List<Individual_Sales_Goal__c>();
        Map<ID, Individual_Sales_Goal__c> indviSalesGoalMap = new Map<ID, Individual_Sales_Goal__c>();
        
        For(User u : userGoalList)
        {       
            if(u.EmployeeNumber <> null)     
                userEmpIDmap.put(u.EmployeeNumber,u);
        }   
        System.debug('The User MAP: '+userEmpIDmap);                  
        colleagueMap =  new Map<ID, Colleague__c>([Select ID,name, EMPLID__c from Colleague__c where EMPLID__c IN: userEmpIDmap.keyset()]);
        for(Colleague__c col :colleagueMap.values())
        {
            colleagueEmpIDMap.put(col.EMPLID__c, col);
        }
        System.debug('The Colleague  MAP: '+colleagueEmpIDMap);   
        indSalesGoalList = [select id, name, Colleague__c, Sales_Goals__c, Sales_Goal_CY__c, Sales_Goal_TOR_MMX__c, Revenue_Growth_Target__c,GRM_REV_TARGET_AMT__c,IRM_REV_TARGET_AMT__c,PGM_REV_TARGET_AMT__c,PM_REV_TARGET_AMT__c,RM_REV_TARGET_AMT__c from Individual_Sales_Goal__c where Colleague__c IN :colleagueMap.keyset()];
        System.debug('The Individual Sales Goal List: '+indSalesGoalList);
        for(Individual_Sales_Goal__c indgoal : indSalesGoalList)
        {
            if(indgoal!=null)
                indviSalesGoalMap.put(indgoal.Colleague__c, indgoal);
        }
        System.debug('the individual Salesfoal Map: '+indviSalesGoalMap);
        Individual_Sales_Goal__c indSalesGoal;
        Colleague__c coll;
        for(User usr :userGoalList)
        {
            
            if(colleagueEmpIDMap.containsKey(usr.EmployeeNumber))
            {
                System.debug('Colleague map contains the emp ID');
                coll = new Colleague__c();
                coll = colleagueEmpIDMap.get(usr.EmployeeNumber);
                if(!indSalesGoalList.isEmpty()) {
                    System.debug('Salesgoal List is not empty');
                    if(indviSalesGoalMap.containsKey(coll.id))
                    {
                        System.debug('Salesgoal List Contains the colleague idS');
                        indSalesGoal = new Individual_Sales_Goal__c();
                        indSalesGoal = indviSalesGoalMap.get(coll.id);
                        
                        
                        
                        if(((indSalesGoal.Sales_Goals__c !=null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c !=null && indSalesGoal.Sales_Goal_CY__c > 0)) && ((indSalesGoal.GRM_REV_TARGET_AMT__c == null || indSalesGoal.GRM_REV_TARGET_AMT__c == 0) && (indSalesGoal.IRM_REV_TARGET_AMT__c == null || indSalesGoal.IRM_REV_TARGET_AMT__c == 0) && (indSalesGoal.PGM_REV_TARGET_AMT__c == null || indSalesGoal.PGM_REV_TARGET_AMT__c == 0) && (indSalesGoal.PM_REV_TARGET_AMT__c == null || indSalesGoal.PM_REV_TARGET_AMT__c == 0) && (indSalesGoal.RM_REV_TARGET_AMT__c == null || indSalesGoal.RM_REV_TARGET_AMT__c == 0)))
                        {
                            System.debug('I am in condition 1');
                            usr.goals__c = 'Sales';
                            System.debug('The value of goals in condition 1: '+usr.goals__c);
                        }
                        else if(((indSalesGoal.Sales_Goals__c !=null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c !=null && indSalesGoal.Sales_Goal_CY__c > 0)) && (indSalesGoal.GRM_REV_TARGET_AMT__c >0 || indSalesGoal.IRM_REV_TARGET_AMT__c >0 || indSalesGoal.PGM_REV_TARGET_AMT__c >0 || indSalesGoal.PM_REV_TARGET_AMT__c >0 || indSalesGoal.RM_REV_TARGET_AMT__c >0))
                        {
                            System.debug('I am in condition 2');
                            usr.goals__c = 'Sales & Revenue';
                        }
                        else if(((indSalesGoal.Sales_Goals__c == null || indSalesGoal.Sales_Goals__c == 0) || (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0)) && ((indSalesGoal.GRM_REV_TARGET_AMT__c !=null && indSalesGoal.GRM_REV_TARGET_AMT__c >0) || (indSalesGoal.IRM_REV_TARGET_AMT__c !=null && indSalesGoal.IRM_REV_TARGET_AMT__c >0) || (indSalesGoal.PGM_REV_TARGET_AMT__c !=null && indSalesGoal.PGM_REV_TARGET_AMT__c >0) || (indSalesGoal.PM_REV_TARGET_AMT__c !=null && indSalesGoal.PM_REV_TARGET_AMT__c >0) || (indSalesGoal.RM_REV_TARGET_AMT__c !=null && indSalesGoal.RM_REV_TARGET_AMT__c >0)))
                        {
                            System.debug('I am in condition 3');
                            usr.goals__c = 'Revenue';
                        }
                        else if(((indSalesGoal.Sales_Goals__c == null || indSalesGoal.Sales_Goals__c == 0) || (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0)) && ((indSalesGoal.GRM_REV_TARGET_AMT__c == null || indSalesGoal.GRM_REV_TARGET_AMT__c == 0) && (indSalesGoal.IRM_REV_TARGET_AMT__c == null || indSalesGoal.IRM_REV_TARGET_AMT__c == 0) && (indSalesGoal.PGM_REV_TARGET_AMT__c == null || indSalesGoal.PGM_REV_TARGET_AMT__c == 0) && (indSalesGoal.PM_REV_TARGET_AMT__c == null || indSalesGoal.PM_REV_TARGET_AMT__c == 0) && (indSalesGoal.RM_REV_TARGET_AMT__c == null || indSalesGoal.RM_REV_TARGET_AMT__c == 0)))
                        {
                            System.debug('I am in condition 4');
                            usr.goals__c = '';
                        }
                        
                    }
                    // As per May Release request #10298 conditons have been modified
                    
                    /* if(((indSalesGoal.Sales_Goals__c != null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c != null && indSalesGoal.Sales_Goal_CY__c > 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c != null && indSalesGoal.Sales_Goal_TOR_MMX__c > 0)) && (indSalesGoal.Revenue_Growth_Target__c == 0 || indSalesGoal.Revenue_Growth_Target__c == null)){
usr.goals__c = 'Sales';
System.debug('I am in COndition Sales from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c != null || indSalesGoal.Sales_Goals__c == 0) && (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0) && (indSalesGoal.Sales_Goal_TOR_MMX__c == null || indSalesGoal.Sales_Goal_TOR_MMX__c == 0)) && (indSalesGoal.Revenue_Growth_Target__c != null && indSalesGoal.Revenue_Growth_Target__c >0)){
usr.goals__c = 'Revenue';
System.debug('I am in COndition Revenue from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c != null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c != null && indSalesGoal.Sales_Goal_CY__c > 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c != null && indSalesGoal.Sales_Goal_TOR_MMX__c > 0)) && (indSalesGoal.Revenue_Growth_Target__c != null && indSalesGoal.Revenue_Growth_Target__c >0)){
usr.goals__c = 'Sales and Revenue';
System.debug('I am in COndition Sales and Revenue from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c == null || indSalesGoal.Sales_Goals__c == 0) || (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c == null || indSalesGoal.Sales_Goal_TOR_MMX__c == 0)) && (indSalesGoal.Revenue_Growth_Target__c == null || indSalesGoal.Revenue_Growth_Target__c == 0)){
System.debug('I am in COndition Blank from User trigger');
usr.goals__c = '';
} */
                }
            }
            
            /*if(!indSalesGoalList.isEmpty()) {
if(indviSalesGoalMap.containsKey(coll.id))
{
indSalesGoal = new Individual_Sales_Goal__c();
indSalesGoal = indviSalesGoalMap.get(coll.id);

}
// As per May Release request #10298 conditons have been modified

if(((indSalesGoal.Sales_Goals__c != null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c != null && indSalesGoal.Sales_Goal_CY__c > 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c != null && indSalesGoal.Sales_Goal_TOR_MMX__c > 0)) && (indSalesGoal.Revenue_Growth_Target__c == 0 || indSalesGoal.Revenue_Growth_Target__c == null)){
usr.goals__c = 'Sales';
System.debug('I am in COndition Sales from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c != null || indSalesGoal.Sales_Goals__c == 0) && (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0) && (indSalesGoal.Sales_Goal_TOR_MMX__c == null || indSalesGoal.Sales_Goal_TOR_MMX__c == 0)) && (indSalesGoal.Revenue_Growth_Target__c != null && indSalesGoal.Revenue_Growth_Target__c >0)){
usr.goals__c = 'Revenue';
System.debug('I am in COndition Revenue from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c != null && indSalesGoal.Sales_Goals__c > 0) || (indSalesGoal.Sales_Goal_CY__c != null && indSalesGoal.Sales_Goal_CY__c > 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c != null && indSalesGoal.Sales_Goal_TOR_MMX__c > 0)) && (indSalesGoal.Revenue_Growth_Target__c != null && indSalesGoal.Revenue_Growth_Target__c >0)){
usr.goals__c = 'Sales and Revenue';
System.debug('I am in COndition Sales and Revenue from User trigger');
}


else if(((indSalesGoal.Sales_Goals__c == null || indSalesGoal.Sales_Goals__c == 0) || (indSalesGoal.Sales_Goal_CY__c == null || indSalesGoal.Sales_Goal_CY__c == 0) || (indSalesGoal.Sales_Goal_TOR_MMX__c == null || indSalesGoal.Sales_Goal_TOR_MMX__c == 0)) && (indSalesGoal.Revenue_Growth_Target__c == null || indSalesGoal.Revenue_Growth_Target__c == 0)){
System.debug('I am in COndition Blank from User trigger');
usr.goals__c = '';
}


}*/            
        }
        
    }
    
    public static void processUserFedIDBeforeUpdate(Map <Id, User> NewUsermap, Map <Id, User> OldUserMap) {
        Map<String,String> ClgMap = new Map<String,String>();
        Set<String> empNum = new Set<String>();
        //              List<User> updateUser = new List<User>();
        for(User usr:NewUsermap.values()){  
            if(String.isNotBlank(usr.EmployeeNumber))
                empNum.add(usr.EmployeeNumber);
        }
        List<Colleague__c> clgList = new List<Colleague__c>();
        if(!empNum.isempty())
            clgList = [Select Id,EMPLID__c,Empl_Status__c from Colleague__c where EMPLID__c IN: empNum limit 50000];
        
        for(Colleague__c clg: clgList){
            ClgMap.put(clg.EMPLID__c,clg.Empl_Status__c);
        }
        for(User usrNew:NewUsermap.values()){
            if(!usrNew.IsActive && usrNew.IsActive != (OldUserMap.get(usrNew.Id)).IsActive){
                if(ClgMap.get(usrNew.EmployeeNumber) == 'Inactive'){
                    usrNew.FederationIdentifier = '';
                    // updateUser.add(usrNew);
                }
                
            }
            
            if(usrNew.IsActive && usrNew.IsActive != (OldUserMap.get(usrNew.Id)).IsActive){
                if(ClgMap.get(usrNew.EmployeeNumber) == 'Active'){
                    usrNew.FederationIdentifier = usrNew.Email_Address__c.toUpperCase();
                    //   updateUser.add(usrNew);
                }
                
            }
            
            
        }
        // update updateUser;
    }    
    @future
    public static void updatecolleagueuser(Set<Id> NewUserIdList){
    
        //Mix DML Fix Start
        if(Test.isRunningTest()){
          updatedFromUser = true;
        }
        //Mix DML Fix End
        
        Map<String, User> usrEmpIDmap = new Map<String, User>();
        List<User> NewUserList=[select id,EmployeeNumber from User where Id IN:NewUserIdList];
        System.debug('NewUserList*****************'+NewUserList);
        For(User u : NewUserList)
        {       
            if(u.EmployeeNumber <> null)     
                usrEmpIDmap.put(u.EmployeeNumber,u);
        } 
        System.debug('usrEmpIDmap*****************'+usrEmpIDmap);
        if(usrEmpIDmap.size()>0){
            
            List<Colleague__c> colleagueList = new List<Colleague__c>(); 
            List<Colleague__c> colleagueListToUpdate = new List<Colleague__c>();               
            colleagueList = [Select ID,name, EMPLID__c,User_Record__c from Colleague__c where EMPLID__c IN: usrEmpIDmap.keyset()];
            System.debug('colleagueList *****************'+colleagueList );
            for(Colleague__c col :colleagueList)
            {
                col.User_Record__c=usrEmpIDmap.get(col.EMPLID__c).id;
                colleagueListToUpdate.add(col);
            }
            update colleagueListToUpdate;
            
        }
    }  
    
    //Added for #18495
    public static void trackProfileUpdate(List<User> triggerNew, Map<Id,User> oldMap){
    
        Map<Id,Profile> profileMap = new Map<Id,Profile>([SELECT Id, Name, UserLicenseId, UserLicense.Name FROM Profile]);    
        
        for(User u : triggerNew){
            if(u.IsActive && !oldMap.get(u.Id).IsActive){
                u.Last_Activation_Date__c = System.today();
            }
            
            if(profileMap.get(u.ProfileId).UserLicenseId != profileMap.get(oldMap.get(u.Id).ProfileId).UserLicenseId){
                if(profileMap.get(u.ProfileId).UserLicense.Name == 'Salesforce'){
                    u.Profile_Change_Date__c = System.today();
                }
            }
            
        }
    }
    
}