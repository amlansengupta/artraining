/*Purpose:This test class provides data coverage for the class MercerForce_Account_Status_Batch_Sched
=============================================================================================================
History
---------------
VERSION  AUTHOR  DATE     DETAIL
1.0      Jagam   12/1/2014 Created testmethod for the schedulable class MercerForce_Account_Status_Batch_Sched.
==============================================================================================================
*/
@isTest
private class MercerForce_Acc_Status_Batch_Sched_test{
/*
*Method Name:testSchedulable
*Description:To schedule the class  at specific time.
*/
    static testmethod void testSchedulable(){
       Test.startTest();
            String jobid = System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022',new MercerForce_Account_Status_Batch_Sched());
            CronTrigger ct  = [SELECT Id, CronExpression, TimesTriggered,NextFireTime FROM CronTrigger WHERE id = :jobId];
       Test.stopTest();
   
    System.assertEquals('0 0 0 3 9 ? 2022',ct.CronExpression); 
    System.assertEquals(0, ct.TimesTriggered);
    System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
   }
}