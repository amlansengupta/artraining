/* Purpose: This test class provides data coverage for Contact Cloning             
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       10/30/2013  Created test class for AP71_ContactCloneController controller class
 ==================================================================================================================*/    
@isTest(SeeAllData=true)
private class Test_AP71_ContactCloneController{
    /*
     * @Description : Test method ot provide data coverage for Contact clone functionality
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void testContClone() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678900';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc1@abc.com';
        insert testColleague;

        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        
        Contact testContact = new Contact();
        
        testContact.FirstName = 'TestName';
        testContact.LastName = 'TestContactName';
        testContact.AccountId = testAccount.Id;
        testContact.Contact_Status__c = 'Active'; 
        testContact.email = 'xyzq@abc1.com';         
        insert testContact;
        
        User testUser = new User();
        testUser.alias = 'usert1';
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = 'lastName';
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = 'abc4.aaa@xyz.com';
        testUser.EmployeeNumber = '12545676200';
        testUser.isActive = true;
        insert testUser;
               
        Contact_Team_Member__c contactTeam = new Contact_Team_Member__c();
        contactTeam.ContactTeamMember__c = testUser.id; 
        contactTeam.Contact__c =testContact.id;
        insert contactTeam;
        
        Contact testContact1 = new Contact();
        
        testContact1.FirstName = 'TestName1';
        testContact1.LastName = 'TestContactName1';
        testContact1.AccountId = testAccount.Id;
        testContact1.Contact_Status__c = 'Active';          
        testContact1.email = 'xyzq@abc12.com'; 
        insert testContact1;
        
        Contact_Team_Member__c contactTeam1 = new Contact_Team_Member__c();
        contactTeam1.ContactTeamMember__c = testUser.id; 
        contactTeam1.Contact__c =testContact1.id; 
        insert contactTeam1;
        User user1 = new User();
        String AdminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(AdminUserprofile , 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        
        test.startTest();            
        ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
        AP71_ContactCloneController controller = new AP71_ContactCloneController(stdController);
        
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(testContact1);
        AP71_ContactCloneController controller1 = new AP71_ContactCloneController(stdController1);

        
        controller.contactsWithContactTeam();
        controller.newCon.LastName = 'newName';
        controller.saveClone();
        
        controller1.contactsWithContactTeam();
        controller1.newCon.LastName = 'newName1';
        controller1.savenewClone();
        controller.cancelClone();
        test.stopTest(); 
         }
    }
}