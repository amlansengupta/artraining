/***************************************************************************************************************
User Story : US504/US505(17441/17442)
Purpose : This apex class is created to support the 'Lock Opportunity (Admin Only)' and 'Lock Opportunity Expansion 
		(Admin Only)' Custom link on Opportunity.
Created by : Soumil Dasgupta
Created Date : 3/1/2018
Project : MF2
****************************************************************************************************************/

Global class MF2_LockOpportunityCustomLinkApex {
    webservice static String fetchStageOpp(String OppId){
        String Stage;
        Opportunity opp = [Select ID, Stagename from Opportunity where id =: OppID];
        Stage = opp.StageName;
        return Stage;
    }
    webservice static Boolean fetchStageMatch(String oppStage){
        Boolean stageMatch =  FALSE;
        String validStages = Label.OpportunityStageName;
        List<String> validStageList = validStages.split(';');
        if(!validStageList.isEmpty() && validStageList != null && validStageList.contains(oppStage)){
			stageMatch = TRUE;            
        }
        return stageMatch;
    }
}