/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class AP95_ProductSearchController_Test {
    Public Static String LOB = 'Retirement';
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    /**
     *  Search Test Screnario 1
     */
    static testMethod void myUnitTest() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
               
       
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
         
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
       
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
          
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c='Active' limit 1].id;
        //Request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c = 'Houston - Dallas';
        Test.startTest();
        insert testOppty;
        
        Test.stopTest();
       
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
        ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
        ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
        controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
        controller.runSearch();
        
    
        controller.Selected = (String)pbEntry.id;
        
        controller.PR3 = 'Solution Segment';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Segment__c;
        controller.runSearch();
        
         
        controller.PR3 = 'Cluster';
        controller.PR1 = 'Equals';
        controller.PR2 =  pro1.Cluster__c;
        controller.runSearch(); 
         
        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Equals';
        controller.PR2 =  pro1.LOB__c;
        controller.runSearch();
         
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Contains';
        controller.PR2 = pro1.Name;
        controller.runSearch(); 
         
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.Name;
        controller.runSearch(); 
     
        controller.PR3 = 'Solution Segment';
        controller.PR1 = 'Contains';
        controller.PR2 = pro1.Segment__c;
        controller.runSearch();
         
        controller.PR3 = 'Solution Segment';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.Segment__c;
         
        controller.runSearch(); 
       
        
        controller.PR3 = 'Cluster';
        controller.PR1 = 'Contains';
        controller.PR2 = pro1.Cluster__c;
        controller.runSearch();
         
        controller.PR3 = 'Cluster';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.Cluster__c;
        controller.runSearch();
         
        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Equals';
        controller.PR2 =  pro1.LOB__c;
        controller.runSearch();
         
        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Contains';
        controller.PR2 =  pro1.LOB__c;
        controller.runSearch(); 
         
        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
        controller.runSearch();
        Pagereference pageref = controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today()+750;
        controller.OliObj.Revenue_Start_Date__c = date.Today();
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
        
        
        
    }
    

     
    /**
     *  Search Test Screnario & Reallocate Revenue functionality testing
     */
    static testMethod void myUnitTest1() {

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
         
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c ='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c ='Houston - Dallas';
        Test.startTest();
        insert testOppty;
       Test.stopTest();
        
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
       controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
         controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
        controller.runSearch();
        Pagereference pageref = controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today()+750;
        controller.OliObj.Revenue_Start_Date__c = date.Today()+750;
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
     
     
          
      
    /**
     *  Negative Test Screnario for exception
     */  
     static testMethod void myNegativeUnitTest() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
           
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOppty1';
        testOppty1.Type = 'New Client';
        testOppty1.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty1.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty1.StageName = 'Identify';
        testOppty1.CloseDate = date.Today();
        testOppty1.CurrencyIsoCode = 'ALL';
        testOppty1.Pricebook2Id = pb.id;
        testOppty1.Opportunity_Office__c = 'Houston - Dallas';
        Test.startTest();
        insert testOppty1;
        Test.stopTest();
           
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty1);
         ApexPages.CurrentPage().getParameters().put('id',testOppty1.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc11';
        controller.sortField = 'abc';
        controller.PR4= pro1.Name;

         controller.runQuery();
        controller.runSearch();
        }
         
    
     }
 
    /**
     *  Search Test Screnario & Reallocate Revenue Scenario 2
     */
      static testMethod void myUnitTest2() {

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
          
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c ='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c ='Honolulu - Fort';
         Test.startTest();
        insert testOppty;
       Test.stopTest();
       
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
        controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
        controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
        controller.runSearch();
        controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today()+369;
        controller.OliObj.Revenue_Start_Date__c = date.Today();
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
     
    /**
     *  Search Test Screnario & Reallocate Revenue Scenario 3
     */ 
       static testMethod void myUnitTest3() {

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
          
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c ='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c ='Honolulu - Fort';
        Test.startTest();
        insert testOppty;
       Test.stopTest();
        
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
        controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
        controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
        controller.runSearch();
        controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today()+369;
        controller.OliObj.Revenue_Start_Date__c = date.Today()+369;
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
        /**
         *  Search Test Screnario & Reallocate Revenue Scenario 4
         */   
      static testMethod void myUnitTest4() {

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
       
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
          
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c='Hoboken - River';
        Test.startTest();
        insert testOppty;
       Test.stopTest();
        
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
        controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
        controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
        controller.runSearch();
        controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today();
        controller.OliObj.Revenue_Start_Date__c = date.Today();
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
    
   
       
        /**
         *  Neagative test scenario 2
         */ 
       static testMethod void myUnitNegativeTest6() {


        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);
  
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/
          
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null  and One_Code_Status__c ='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c = 'Hoboken - River';
        Test.startTest();
        insert testOppty;
       Test.stopTest();
        
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
       controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
         controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
       controller.runSearch();
        controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = null;
        controller.OliObj.Revenue_Start_Date__c = date.Today()+365;
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
     
       
        /**
         *  Search Test Screnario & Reallocate Revenue Scenario 4
         */ 
        static testMethod void myUnitTest6() {

        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Product2 pro1 = mtscopedata.buildProduct(LOB);

        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.pricebook2Id = pb.Id;
        pbEntry.product2id = pro1.id;
        pbEntry.unitprice = 1249.0;
        pbEntry.isactive = true;
        pbEntry.CurrencyIsoCode = 'ALL';
        insert pbEntry;*/ 
        Test.startTest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = [select id from Account where one_code__c <>null and One_Code_Status__c ='Active' limit 1].id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
        testOppty.Opportunity_Office__c = 'Guangzhou - Tianhe';
        insert testOppty;
Test.stopTest();
       
        
        system.runAs(User1){
        PageReference pageRef5 = Page.Mercer_ProductSearch;
        test.setCurrentPageReference(pageRef5);
         ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
         ApexPages.CurrentPage().getParameters().put('id',testOppty.Id);    
        AP95_ProductSearchController  controller = new AP95_ProductSearchController(stdController);
        
        controller.isSearchHaveValue = true;
        controller.sortDir='asc';
        controller.sortField = 'p.Product2.Name';
        controller.PR4= pro1.Name;
       controller.runSearch();
       
        controller.PR3 = 'Product Name';
        controller.PR1 = 'Equals';
        controller.PR2 = pro1.Name;
  
        controller.runSearch();        
    
        controller.Selected = (String)pbEntry.id;

        controller.PR3 = 'Product LOB';
        controller.PR1 = 'Starts With';
        controller.PR2 = pro1.LOB__c;      
                 
        controller.toggleSort();
       controller.runSearch();
        controller.selectPBE();
        
        controller.clearSearch();
        
        controller.OliObj.Revenue_End_Date__c = date.Today()+750;
        controller.OliObj.Revenue_Start_Date__c = date.Today()+365;
        controller.OliObj.UnitPrice = 120.00;
      
        controller.reallocateRevenue();
        controller.Savebutton();
        controller.SaveMorebutton();
        controller.Cancelbutton();
        }
         
        
        
    }
       
     
}