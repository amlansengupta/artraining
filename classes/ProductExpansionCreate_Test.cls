/*
==============================================================================================================================================
Request Id                 					 Date                    Modified By
12638:Removing Step 						 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest
private class ProductExpansionCreate_Test {

    static testMethod void method1(){
        Test.startTest();
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
        
        
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
       User testUser = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason.test@test.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Employee_office__c='Mercer US Mercer Services - US03'      
                           );
       
         insert testUser;
         
        Switch_Settings__c ss=new Switch_Settings__c();
        ss.SetupOwnerId=testUser.id;
        ss.IsOpportunityProductTriggerActive__c=false;
        ss.IsOpportunityProductValidationActive__c=false;
        ss.IsOpportunityTriggerActive__c=false;
        ss.IsOpportunityValidationActive__c=false;      
        insert ss;
        
        System.runAs(testUser){
            
        
        
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            insert testAccount;
                
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //request id:12638 commenting step
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            insert testOppty1;
            
            ProductExpansionCreate pec=new ProductExpansionCreate();
            ApexPages.currentPage().getParameters().put('id', testOppty1.Id);
            pec.createExpansion();
            pec.cancel();
            }
         Test.stopTest();
    }
}