/*Purpose: This Controller is an extension controller for the Account Hierarchy Page. 
           It gets the child account and intermediate account for the parent account record based on Global ID 
           and also the opportunities and contacts related to those accounts.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL 
   1.0 -    Arijit    12/26/2012  Created Controller Class
   2.0 -    Sarbpreet 7/2/2014    Updated Comments/documentation.
============================================================================================================================================== 
*/
public with sharing class AP08_AccountHierarchyController {
    
     // list variable to store a tree model
     public transient List<TreeModel> nodeList {get;set;}
     
     // string variable to store GlobalId
     private String globalid;
     
     // string variable to store account type
     private String AcctType;
     
     // string variable to store OpportunityId mapped to VF page
     Public String opportunityId{get;set;}
     
     // boolean variable 
     Public boolean opprender{get;set;}
     
     // string variable to store Wrapper object mapped to JSON variable in VF page
     public string JSONSopporstring{get;set;}
     
     // Account object
     private Account accountObj;
     
     // map variable of string and list of parent account records
     Map<String, List<Account>> parentAccMap;
     
     // map variable of string and list of child account records
     transient Map <String, List<Account>> childAccMap;
     
     // ID variable to Id
     Public Id id {get;set;}
     
     // map variable of string and list of grand child account records
     Map<String, List<Account>> grandChildMap;
     
     public boolean hasNoGlobalId{get; private set;}
     
    /* 
     * constructor for the class  
     */
     public AP08_AccountHierarchyController(ApexPages.StandardController controller)
     {
     try{
        opprender = false;
        this.hasNoGlobalId = false;
          
        this.accountObj = (Account)controller.getRecord();  
      
        // check whether records exist
        if(accountObj <> null)
        {
            System.debug('\n print accountObj :'+accountObj);
            System.debug('\n print global id value :'+accountObj.GBL_ID__c); 
            this.AcctType = accountObj.Type__c;
            this.globalid = accountObj.GBL_ID__c;
        }      
            
        // variable initialisation
        nodeList = new List<TreeModel>();  
        TreeModel tm1 = new TreeModel();
        this.parentAccMap = new Map<String, List<Account>>();
        this.childAccMap = new Map<String, List<Account>>();
        this.grandChildMap = new Map<String, List<Account>>();
            
        System.debug('\n print global id value :'+globalid);
        // function call to create account hierarchy 
        if(accountObj.GBL_ID__c <> null)
        {
            createAccountHeirarchy(globalid);
                  
        }else
        {
            this.hasNoGlobalId = true;
        }
                
     }
     catch(Exception ex)
     {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
     }
     }
     
     // string variable to store account Id mapped to VF page
     public String AccId
     {
        get{
            return ApexPages.currentPage().getParameters().get('Id');
        }
        set;
     }
     
      /*
     * @Description : This method is called in the class costructor 
     * @ Args       : Global Id
     * @ Return     : Pagereference
     */
      Public pagereference createAccountHeirarchy( String GlobID)
      {                     
            try
            {
                // function call to retrieve parent accounts
                this.parentAccMap = AP09_AccountHierarchyUtil.getParentAccounts(GlobID);
                
                // function call to retrieve intermediate accounts
                this.childAccMap =  AP09_AccountHierarchyUtil.getIntermediateAccounts(GlobID);
                
                // function call to retrieve child accounts
                this.grandChildMap = AP09_AccountHierarchyUtil.getChildAccounts(GlobID);  
                
                System.debug('\n parent account map :'+parentAccMap);
                
                System.debug('\n Intermediate account map :'+childAccMap);
                
                System.debug('\n child account map :'+grandChildMap); 
                    
                // iterate through parent account map keyset
                for(String gblId : parentAccMap.keySet())
                {
                    // iterate through records in parent account map
                    for(Account acc : parentAccMap.get(gblId))
                    {
                        TreeModel parentNode = new TreeModel();
                        parentNode.id = acc.Id; 
                        parentNode.Name = acc.Name; 
    
                        // if child account exist
                        if(childAccMap.get(acc.GBL_ID__c) <> null)
                        {
                            // iterate through records in child account map
                            for(Account acct : childAccMap.get(acc.GBL_ID__c))
                            {
                                TreeModel childNode = new TreeModel();
                                childNode.Id = acct.Id;     
                                childNode.Name = acct.Name;
                                parentNode.children.add(childNode);
    
                                // if grand child account exist
                                if(grandChildMap.get(acct.Int_ID__c) <> null)
                                {
                                    // iterate through records in grand child acoount map
                                    for(Account accct : grandChildMap.get(acct.Int_ID__c))
                                    {
                                        TreeModel grandchildNode = new TreeModel(); 
                                        grandchildNode.Id = accct.Id;
                                        grandchildNode.Name = accct.Name;
                                        childNode.children.add(grandchildNode);
                                    }
                                }
                                
                            }           
                            
                        }
                        // adding the node to the tree model
                        nodeList.add(parentNode);
                        
                    }
                    
                }
                System.debug('\n created node list :'+nodeList);    
            }catch(Exception ex)
            {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage())); 
            }
            
            return null;
    }

    
    /* 
     *  Opportunity wrapper class
    */
    private class OpportunityWrapper
    {
        String Id, Name, AccName, Amount, OneCode, Stage, currencyiso; 
        string associatedGoldSheet, closeDate;  
        
        /*
        *   Constructor for Opportunity wrapper class
        */
        OpportunityWrapper(String Id, String Name, String AccName, String Amount, String OneCode, String Stage, String currencyiso, string associatedGoldSheet, string CloseDate)
        {
            this.Id = Id;
            this.Name = Name;
            this.AccName = AccName;
            this.Amount = Amount;
            this.OneCode = OneCode;
            this.Stage = Stage;
            this.currencyiso = currencyiso;
            this.associatedGoldSheet = associatedGoldSheet;
            this.closeDate = CloseDate;
        }
    }
        

   /* 
    *   Contact wrapper class
    */
    private class ContactWrapper
    {
        String Id, Name, AccName, Mailingcountry , Title , Phone , Email ;
        
        /*
        * Constructor for Contact wrapper class
        */
        ContactWrapper(String Id, String Name, String AccName, String Mailingcountry , String Title , String Phone , String Email )
        {
            this.Id = Id;
            this.Name = Name;
            this.AccName = AccName;
            this.MailingCountry = MailingCountry;
            this.Title = Title;
            this.Phone = Phone;
            this.Email = Email;
            
        }
        
    }
    
   /*
    *   wrapper class for opportunity, contact and account wrapper objects
    */
    private class WrapperObject
    {
        public List<OpportunityWrapper> opps;
        public List<ContactWrapper> cons;
        public List<AccountWrap> accounts;
        
       /*
        *   Constructor for WrapperObject class
        */
        WrapperObject(List<OpportunityWrapper> opps, List<ContactWrapper> cons, List<AccountWrap> accounts)
        {
            this.opps = opps;
            this.cons = cons;
            this.accounts = accounts;
        }
    }
    
   
   /*
    *   Account wrapper class
    */
    private class AccountWrap
    {
        String Id, name, onecode, onecodestatus, relmanagername, region;
        String associatedGoldSheet;
        
        /* 
        * Constructor for AccountWrap class
        */
        AccountWrap(String Id, String name, String onecode, String onecodestatus, String relmanagername, String region, String associatedGoldSheet)
        {
            this.name = name;
            this.onecode = onecode;
            this.onecodestatus = onecodestatus;
            this.relmanagername = relmanagername;
            this.region = region;
            this.Id = Id;  
            this.associatedGoldSheet = associatedGoldSheet;                     
        }   
    }
    

   /*
     * @Description : This method is used to retrieve data (mapped to the VF page)
     * @ Args       : none
     * @ Return     : pagereference
     */
    public pageReference getData()
    {  
    try{ 
        opprender = true;
        Set<String> accIds = new Set<String>();
        List<OpportunityWrapper> opportunities = new List<OpportunityWrapper>();
        List<ContactWrapper> contacts = new List<ContactWrapper>();
        List<AccountWrap> accounts = new List<AccountWrap>();
        Account account = [Select Name, Type__c,GBL_ID__c,Int_Id__c,GOC_ID__c, Account_Region__c, Account_Market__c from Account where Id = :opportunityId];
        String AccType = account.Type__c;
        String GBLID = account.GBL_ID__c;
        String INTID = account.Int_Id__c;
        String GOCID = account.GOC_ID__c;
        
                
        if(account <> null)
        {
            
            // get parent accounts                        
            if(AccType == 'Parent' )
            {                
                
                for(Account acc : [Select Id, mh_Associated_Gold_Sheet__c,MH_Sheet_Attached__c, Name, One_Code__c, One_Code_Status__c, /*Relationship_Manager__r.First_Name__c, Relationship_Manager__r.Last_Name__c,*/ Relationship_Manager__r.Name, Account_Region__c FROM Account where GBL_ID__c = : GBLID])
                { 
                    String mangerName = acc.Relationship_Manager__r.Name;
                    /*if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c + ' ' + acc.Relationship_Manager__r.Last_Name__c;    
                    }else if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c == null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c; 
                    }else if(acc.Relationship_Manager__r.First_Name__c == null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.Last_Name__c;  
                    }else
                    {
                        mangerName = null;  
                    }*/                    
                    
                    AccountWrap wrap = new AccountWrap(acc.Id, acc.Name.replace('\'', ' '), acc.One_Code__c, acc.One_Code_Status__c, mangerName, acc.Account_Region__c, acc.MH_Sheet_Attached__c);
                    accounts.add(wrap);   
                    accIds.add(acc.Id);
                }
                
                // get related opportunities and contacts
                if(!accIds.isEmpty()) 
                {
                   
                    for(Opportunity opp : AP09_AccountHierarchyUtil.getOpportunities(accIds))
                    {  
                        OpportunityWrapper wrap = new OpportunityWrapper(opp.Id, opp.Name.replace('\'', ' '), opp.Account.Name.replace('\'', ' '), String.valueOf(opp.Total_Opportunity_Revenue_USD__c), opp.OneCode__c, opp.StageName, opp.CurrencyIsoCode, opp.MH_Sheet_Attached__c, String.valueOf(opp.CloseDate));                        
                        opportunities.add(wrap);                        
                    }
                    
                    for(Contact con : AP09_AccountHierarchyUtil.getContacts(accIds))
                    {  
                        ContactWrapper wrapcon = new ContactWrapper(con.Id, con.Name.replace('\'', ' '), con.Account.Name.replace('\'', ' '), con.MailingCountry , con.Title , con.Phone , con.Email );                        
                        contacts.add(wrapcon);                        
                    }                
                    System.debug('\n Associated contacts for mailing country check :'+contacts);
                    System.debug('\n No of Opportunities returned from the Account query :'+opportunities.size());
                }                 
                
                
            }
        
            // get intermediate accounts
            else if(AccType == 'Intermediate')
            {
                for(Account acc : [Select Id, mh_Associated_Gold_Sheet__c, MH_Sheet_Attached__c, Name, One_Code__c, One_Code_Status__c, /*Relationship_Manager__r.First_Name__c, Relationship_Manager__r.Last_Name__c,*/Relationship_Manager__r.Name, Account_Region__c FROM Account where INT_ID__c =:INTID])
                { 
                    String mangerName = acc.Relationship_Manager__r.Name;
                    /*if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c + ' ' + acc.Relationship_Manager__r.Last_Name__c;    
                    }else if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c == null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c; 
                    }else if(acc.Relationship_Manager__r.First_Name__c == null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.Last_Name__c;  
                    }else
                    {
                        mangerName = null;  
                    }*/
                    AccountWrap wrap = new AccountWrap(acc.Id, acc.Name.replace('\'', ' '), acc.One_Code__c, acc.One_Code_Status__c, mangerName, acc.Account_Region__c, acc.MH_Sheet_Attached__c);
                    accounts.add(wrap);   
                    accIds.add(acc.Id);
                }
            
                // get related opportunities and contacts
                if(!accIds.isEmpty())
                {
                    for(Opportunity opp : AP09_AccountHierarchyUtil.getOpportunities(accIds))
                    {
                        
                        
                       OpportunityWrapper wrap = new OpportunityWrapper(opp.Id, opp.Name.replace('\'', ' '), opp.Account.Name.replace('\'', ' '), String.valueOf(opp.Total_Opportunity_Revenue_USD__c), opp.OneCode__c, opp.StageName, opp.CurrencyIsoCode, opp.MH_Sheet_Attached__c, String.valueOf(opp.CloseDate));                        
                        opportunities.add(wrap);
                    }
                    
                    for(Contact con : AP09_AccountHierarchyUtil.getContacts(accIds))
                    {
                        
                        ContactWrapper wrapcon = new ContactWrapper(con.Id, con.Name.replace('\'', ' '), con.Account.Name.replace('\'', ' '), con.MailingCountry , con.Title , con.Phone , con.Email );
                        contacts.add(wrapcon);
                    }
                    System.debug('\n No of Opportunities returned from the Account query :'+opportunities.size());
                    
                }   
            }
            
            // get child accounts
            else if(AccType == 'Child')
            {
                for(Account acc : [Select Id, Name, mh_Associated_Gold_Sheet__c, MH_Sheet_Attached__c, One_Code__c, One_Code_Status__c, /*Relationship_Manager__r.First_Name__c, Relationship_Manager__r.Last_Name__c,*/ Relationship_Manager__r.Name, Account_Region__c FROM Account where GOC_ID__c =:GOCID ])
                {  
                    String mangerName = acc.Relationship_Manager__r.Name;
                    /*if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c + ' ' + acc.Relationship_Manager__r.Last_Name__c;    
                    }else if(acc.Relationship_Manager__r.First_Name__c <> null && acc.Relationship_Manager__r.Last_Name__c == null)
                    {
                        mangerName = acc.Relationship_Manager__r.First_Name__c; 
                    }else if(acc.Relationship_Manager__r.First_Name__c == null && acc.Relationship_Manager__r.Last_Name__c <> null)
                    {
                        mangerName = acc.Relationship_Manager__r.Last_Name__c;  
                    }else
                    {
                        mangerName = null;  
                    }*/
                    AccountWrap wrap = new AccountWrap(acc.Id, acc.Name.replace('\'', ' '), acc.One_Code__c, acc.One_Code_Status__c, mangerName, acc.Account_Region__c, acc.MH_Sheet_Attached__c);
                    accounts.add(wrap);  
                    accIds.add(acc.Id);
                }
            
                // get related opportunities and contacts
                if(!accIds.isEmpty())
                {
                    for(Opportunity opp : AP09_AccountHierarchyUtil.getOpportunities(accIds))
                    {
                        
                        OpportunityWrapper wrap = new OpportunityWrapper(opp.Id, opp.Name.replace('\'', ' '), opp.Account.Name.replace('\'', ' '), String.valueOf(opp.Total_Opportunity_Revenue_USD__c), opp.OneCode__c, opp.StageName, opp.CurrencyIsoCode, opp.MH_Sheet_Attached__c, String.valueOf(opp.CloseDate));                        
                        opportunities.add(wrap);
                    }
                    
                    for(Contact con : AP09_AccountHierarchyUtil.getContacts(accIds))
                    {
                        
                        ContactWrapper wrapcon = new ContactWrapper(con.Id, con.Name.replace('\'', ' '), con.Account.Name.replace('\'', ' '), con.MailingCountry , con.Title , con.Phone , con.Email );
                        contacts.add(wrapcon);
                    }
                    System.debug('\n No of Opportunities returned from the Account query :'+opportunities.size());
                    
                    
                }
            }
        }
        
        // adding accounts, opportunities and contacts wrapper list to wrapperobject  
        WrapperObject wObject = new WrapperObject(opportunities, contacts, accounts);
        JSONSopporstring = JSON.serialize(wObject).replace('\'', ' ');
        System.debug('\n constructed Json string :'+JSONSopporstring);
        }
        catch(Exception ex)
            {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage())); 
            }
        return null;
        
    }
    
  
    

}