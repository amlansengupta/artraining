/* Purpose: This test class provides data coverage for updating the Concatenated Industry Category associated with 
            an Account 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       08/25/2013  Created test method for Test_DC24_AccountBatchable batch class
 ==================================================================================================================*/    
@isTest(seeAllData=true)
public class Test_DC24_AccountBatchable {
      
    /* * @Description : This test method provides data coverage for Test_DC24_AccountBatchable batch class 
                        for updating the Concatenated Industry Category associated with an Account              
       * @ Args       : no arguments        
       * @ Return     : void       
    */        
    static testMethod void Test_DC24_AccountBatchable() 
    {
         Test.startTest() ;
       
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        insert testAccount;

        String query='Select id, Name, Primary_Industry_Category__c, Industry_Category__c, Industry_Category_2__c, Concatenated_Industry_Category__c  from Account where Id = \''+ testAccount.Id  + '\'';
        database.executeBatch(new DC24_AccountBatchable(query),200);
        
        Test.stopTest() ;
    }
}