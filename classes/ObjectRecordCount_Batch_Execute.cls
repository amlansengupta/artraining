global class ObjectRecordCount_Batch_Execute{
    @InvocableMethod 
    global static void executeORCBatch(){
        Database.executeBatch(new ObjectRecordCount_Batch(),2);
    }
}