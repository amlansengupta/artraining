public class Mercer_Request_Class {
    
    public String recTypeid;
    public Mercer_Request_Class(ApexPages.StandardController controller) {
        
        recTypeid = System.CurrentPageReference().getParameters().get('RecordType');
    }
    
    public PageReference redirect(){
        if(recTypeid == Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId())
            return new PageReference('/a08/e?retURL=%2Fa08%2Fo&RecordType='+recTypeid+'&ent=01IE0000000XC6I&nooverride=1');
        else if(recTypeid == Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Defect').getRecordTypeId())       
            return new PageReference('/apex/Mercer_PMO_NewDefect');             
        else
            return null;
    }

}