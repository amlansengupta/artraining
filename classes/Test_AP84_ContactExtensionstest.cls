/*Purpose:  This Apex class contains Test methods for Test Coverage of  AP84_ContactExtensions
==============================================================================================================================================History 
---------------------------------------------------------------------------------
VERSION     AUTHOR        DATE        DETAIL 
   1.0 -    Ravikiran    02/20/2014  Created as a test class for  Apex class AP84_ContactExtensions
   2.0      Ravikiran    03/10/2014  Changes made to Enchance Quality 
============================================================================================================================================== 
*/
@istest
Private class Test_AP84_ContactExtensionstest {
    private static Mercer_TestData mtdata = new Mercer_TestData();
    /*This method is used to test functionality of Mercer_Contact_New page 
     * 
     */
    static testMethod void saveandnewMethod() {
        User user1 = new User(id = UserInfo.getUserId());
        system.runAs(user1) {


            contact conn = new contact();
            Colleague__c testColleague = mtdata.buildColleague();
            Account testAccount = mtdata.buildAccount();

            pageReference cntNew = page.Mercer_Contact_New;

            ApexPages.StandardController controller = new ApexPages.StandardController(conn);
            AP84_ContactExtensions cont = new AP84_ContactExtensions(controller);

            conn.Accountid = testAccount.Id;
            conn.LastName = 'Test';

            test.startTest();

            
                cont.saveAndNew();
                System.assertEquals(conn.LastName, 'Test');
           
            test.stopTest();

        }

    }
    
    
    /*This method is used to test functionality of duplicates on the Mercer_Contact_New page 
     * 
     */
    static testMethod void SaveNonDuplicates() {
        User user1 = new User(id = UserInfo.getUserId());
        system.runAs(user1) {


            Contact conn = new contact();
            Colleague__c testColleague = mtdata.buildColleague();
            Account testAccount = mtdata.buildAccount();
            conn.Accountid = testAccount.Id;
            conn.LastName = 'Test';
            conn.Email = 'test@duplicates.com';
            Insert conn; 
            
            Contact con_dupe = new contact();
            pageReference cntNew = page.Mercer_Contact_New;

            ApexPages.StandardController controller = new ApexPages.StandardController(con_dupe);
            AP84_ContactExtensions cont = new AP84_ContactExtensions(controller);

            con_dupe.Accountid = testAccount.Id;
            con_dupe.LastName = 'Test Duplicate';
            con_dupe.Email = 'test@duplicates.com';
            try{            
                cont.SaveNonDuplicates();
            }catch(Exception e){
                System.debug(e.getMessage());
                Boolean expectedExceptionThrown =  e.getMessage().contains('A contact with the same email address already exists. Click Cancel and search for the contact using the email address to locate the existing record.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        }

    }
    
    /*This method is used to test general exceptions on the Mercer_Contact_New page 
     * 
     */
    static testMethod void SaveNonDuplicates2() {
        User user1 = new User(id = UserInfo.getUserId());
        system.runAs(user1) {


            Contact conn = new contact();
            Colleague__c testColleague = mtdata.buildColleague();
            Account testAccount = mtdata.buildAccount();
            conn.Accountid = testAccount.Id;
            conn.LastName = 'Test';
            conn.Email = 'test@duplicates.com';
            Insert conn; 
            
            Contact con_dupe = new contact();
            pageReference cntNew = page.Mercer_Contact_New;

            ApexPages.StandardController controller = new ApexPages.StandardController(con_dupe);
            AP84_ContactExtensions cont = new AP84_ContactExtensions(controller);

            con_dupe.Accountid = testAccount.Id;
            con_dupe.LastName = 'Test Duplicate';
            con_dupe.Email = 'test@duplicates';
            try{            
                cont.SaveNonDuplicates();
            }catch(Exception e){
                Boolean flag = false;
                System.debug(e.getMessage());
                if(e.getMessage()!= '')
                flag = true;
                System.AssertEquals(flag, true);
            }
        }

    }
}