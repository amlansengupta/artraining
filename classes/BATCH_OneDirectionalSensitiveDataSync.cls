global class BATCH_OneDirectionalSensitiveDataSync implements Database.Batchable<sObject>{

  global set<Id> setRiskIds;
global BATCH_OneDirectionalSensitiveDataSync(){}
        global BATCH_OneDirectionalSensitiveDataSync(set<Id> rids){
    setRiskIds = rids;
  }
  global Database.QueryLocator start(Database.BatchableContext BC)
  {
    string soql = 'select Id,Account__c,At_Risk_Type__c,Date_Opened__c,CreatedBy.Name, Status__c,LastModifiedBy.Name, Why_at_Risk__c,LOB_Status__c,LOB__c from Sensitve_At_Risk__c';
    if(setRiskIds <> null && setRiskIds.size() > 0)
            soql += ' Where Id IN: setRiskIds';
    return Database.getQueryLocator(soql);
  }
  global void execute(Database.BatchableContext BC, List<Sensitve_At_Risk__c> scope)
  {
    System.debug('scope'+scope);
    map<Id,Client_Relationship__c> mapCustomSensitive_At_Risk = new map<Id,Client_Relationship__c>();
    for(Client_Relationship__c c : [select Id,At_Risk_Type__c ,Date_Opened__c,CreatedBy.Name, LastModifiedBy.Name,Why_at_Risk__c,LOB_Status__c,LOB__c,Sensitive_At_Risk__c from Client_Relationship__c where Sensitive_At_Risk__c IN: scope]){
      mapCustomSensitive_At_Risk.put(c.Sensitive_At_Risk__c, c);
    }
    List<Client_Relationship__c> lstClientRelationship = new list<Client_Relationship__c>();
    for(Sensitve_At_Risk__c s: scope)
    {
      Client_Relationship__c cr = new Client_Relationship__c();
      if(mapCustomSensitive_At_Risk.containsKey(s.id)){
        cr = mapCustomSensitive_At_Risk.get(s.id);
      }else{
        cr.Account__c = s.Account__c;
      }
      cr.Why_at_Risk__c = s.Why_at_Risk__c;
      cr.LOB_Status__c = s.Status__c;
      cr.LOB__c = s.LOB__c;
      cr.Sensitive_At_Risk__c = s.id;
      cr.At_Risk_Type__c = s.At_Risk_Type__c;
      cr.Date_Opened__c = s.Date_Opened__c;      
      cr.CreatedBy = s.CreatedBy;
      cr.LastModifiedBy = s.LastModifiedBy;
      lstClientRelationship.add(cr);  
    }
    database.upsert(lstClientRelationship,false);
  }
  global void finish(Database.BatchableContext BC)
  {
  }
}