/*Purpose: This test class provides data coverage to AP05_AccountMergeSchedulable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit 02/05/2013  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_AP05_AccountMergeSchedulable 
{
/*
     * @Description : Test method to provide data coverage to AP05_AccountMergeSchedulable class 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
    	AP05_AccountMergeSchedulable AMsch = new AP05_AccountMergeSchedulable();
    	String sch = '0 0 23 * * ?';
	    system.schedule('Test MercerOfficeSchedulable', sch, AMsch);
    	
    }

}