/***************************************************************************************************************
Request# : 17444
Purpose : This class is created as the server-side controller class of MF2_OpportunityRefreshBillRates lightning component
Created by : Harsh Vats
Created Date : 27/12/2018
Project : MF2
****************************************************************************************************************/
public class RefreshBillRatesControllerApex
{
    /* This method accepts the Opportunity record id and refreshes all related 
* scopeIt employee records' bill rates according to the currency code
* Made this method AuraEnabled to comply with Lightning Component MF2_OpportunityRefreshBillRates  */
    @AuraEnabled
    public static boolean refreshBillRates(Id oppId)
    {
        boolean stageError = false;
        String thisRecordCode = null;
        try
        {
            //Checks if there is some opportunityId
            if(oppId != null)
            {
                //Created map to store conversion rate values against the conversion Names
                Map<String, Double> isoConversionMap = new Map<String, Double>();
                Opportunity opp = [select Id,StageName from Opportunity where Id =: oppId];
                String stage = opp.StageName;
                //System.debug('debug1: '+stage);
                //To fetch values from custom label
                String stageName = System.Label.OpportunityStageName;
                List<String> listStageName = stageName.split(';');

				//To fetch values from custom settings
                //List<OpportunityStageName__c> listStageName = OpportunityStageName__c.getall().values();
                for(String oppStage : listStageName)
                {
                    //To check if the opportunity stage Open;if not, an error message will be prompted
                    if(stage == oppStage)
                    {
                        stageError = true;
                    }
                    else
                    {
                        //To fetch all scopeIt employee records related to Opportunity record. 
                        List<ScopeIt_Employee__c > listScopeItEmpRecs = [select Id, Bill_Rate_Opp__c,CurrencyIsoCode, Employee_Bill_Rate__c,Bill_Rate__c,Employee_Bill_Rate__r.CurrencyIsoCode, Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c from ScopeIt_Employee__c where ScopeIt_Task__r.ScopeIt_Project__r.Opportunity__c =:oppId];
                        //To fetch all currency Name and conversion rate from system.
                        List<Current_Exchange_Rate__c> listExchangeRates = [select Name,Conversion_Rate__c from Current_Exchange_Rate__c];
                        // To check if the list contains a value
                        if(listExchangeRates!=null)
                        {
                            //To keep a map of currency code and corresponding conversion rate
                            for(Current_Exchange_Rate__c rate: listExchangeRates)
                            {
                                isoConversionMap.put(rate.Name, rate.Conversion_Rate__c);
                            }
                        }
                        // Created a new list to store scopeIt Employees which needs to update
                        List<ScopeIt_Employee__c> listScopeEmpToUpdate = new List<ScopeIt_Employee__c>();
                        //To ensure system has scopeIt employee record(s) for the provided Opportunity record
                        if(listScopeItEmpRecs!=null && !listScopeItEmpRecs.isEmpty())
                        {
                            thisRecordCode = listScopeItEmpRecs.get(0).CurrencyIsoCode;
                            //To re-calculate bill rate for every scope-it employee
                            for(ScopeIt_Employee__c scopeEmp:listScopeItEmpRecs)
                            {
                                scopeEmp.Bill_Rate_Opp__c = ((scopeEmp.Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c * isoConversionMap.get(thisRecordCode))/isoConversionMap.get(scopeEmp.Employee_Bill_Rate__r.CurrencyIsoCode));
                                listScopeEmpToUpdate.add(scopeEmp);
                            }
                        }
                        //To update refreshed bill rates
                        if(!listScopeEmpToUpdate.isEmpty())
                        {
                            update listScopeEmpToUpdate;
                        }
                    }
                }
                
            }
        }
        catch(Exception ex)
        {
            ExceptionLogger.logException(ex, 'RefreshBillRatesControllerApex', 'refreshBillRates');
            throw new AuraHandledException('Could not refresh Bill Rates! Please try after sometime.');
        }
        return stageError;
    }
}