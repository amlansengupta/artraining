/**
 * Created by 7Summits on 5/12/17.
 */

@isTest
public with sharing class SVNSUMMITS_RecordTypeUtilityTest {

    @isTest
    public static void getRecordTypeIdsByDeveloperName_news_test() {

        // get record types available
        Schema.SObjectType token = News__c.getSObjectType();
        Map<String, Id> result = SVNSUMMITS_RecordTypeUtility.getRecordTypeIdsByDeveloperName(token);

        // get the actual recordType
        List<RecordType> expectedResult = [SELECT id, DeveloperName FROM RecordType WHERE sObjectType='News__c' AND IsActive=true];

        // iterate actual record types an make sure they were found
        for(RecordType recordType : expectedResult){
            Object containsRecordType = result.containsKey(recordType.developerName);
            System.assertEquals(true, containsRecordType);
        }
    }

}