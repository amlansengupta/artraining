global class SC_SP_CM_Allocation_Update_Schedulable implements Schedulable{

     
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new SC_SP_CM_Allocation_Update_Batch(),Integer.valueOf(Label.SC_SP_CM_Allocation_Update_Batch_Size));
    }
    
    
}