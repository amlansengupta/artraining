/*Purpose:  This test class provides data coverage to AP59_PricebookentryTriggerUtil class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Srishty   05/16/2013  Created test class 
   
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP59_PricebookentryTriggerUtil {

    private static Product2 testProduct = new Product2();
    private static Mercer_TestData mtdata = new Mercer_TestData();
    /*
     * @Description : Test method to provide data coverage to AP59_PricebookentryTriggerUtil class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        
        Product_Sub_Bus_Classification__c psb = new Product_Sub_Bus_Classification__c();
        psb.Business__c = 'TEST Bus';
        psb.Solution_Segment__c = 'TEST seg';
        psb.Classification__c = 'TEST class';
        psb.Sub_Busines__c = 'TEST sub bus';
        psb.SHORT_Sub_Business__c = 'TES';
        insert psb;   
       
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.LOB__c = 'TEST Bus';
        testProduct.Segment__c = 'TEST seg';
        insert testProduct;
        
        testProduct.IsActive = False;
        
        test.startTest();
        update testProduct;
        test.stoptest();
        
        }
        
    }
    
     static testMethod void myUnitTest1()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){ 
            
        Account acct = mtdata.buildAccount();
        
      
        //test.startTest();           
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'TEST Bus';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
              
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :testProduct2.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acct.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
            testOppty.Opportunity_Office__c = 'Minneapolis - South Seventh';
        insert testOppty;
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+30;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
        opptylineItem.UnitPrice = 1.00;
        insert opptylineItem;
        
        test.startTest(); 
        testProduct2.Classification__c ='Consulting Solution Area';
        update testProduct2;
        test.stoptest();
        /*Product2 testProduct1 = new Product2();
        testProduct1.Name = 'TestPro1';
        testProduct1.Family = 'RRF';
        testProduct1.IsActive = True;
        testProduct1.LOB__c = 'TEST Bus';
        testProduct1.Segment__c = 'TEST seg';
        testProduct1.Classification__c ='Non Consulting Solution Area';
        insert testProduct1;
        
        testProduct1.Classification__c ='Consulting Solution Area';
        update testProduct1;*/
        
        }
        
    }
    
     static testMethod void myUnitTest2()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){ 
            
        Account acct = mtdata.buildAccount();
        
      
        //test.starttest();           
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'TEST Bus';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Consulting Solution Area';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
              
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :testProduct2.Id and CurrencyIsoCode = 'ALL' limit 1];
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acct.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Pricebook2Id = pb.id;
            testOppty.Opportunity_Office__c = 'Minneapolis - South Seventh';
            test.starttest();
        insert testOppty;
            test.stoptest();
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+30;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
        opptylineItem.UnitPrice = 1.00;
        insert opptylineItem;
        
        
        testProduct2.Classification__c ='Non Consulting Solution Area';
        update testProduct2;
        
        
        }
        
    }
}