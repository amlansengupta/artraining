/*Purpose:  This Apex class contains method to insert contact role when a buying influence is created 
=================================================================================================================================================
History 
----------------------- 
VERSION   AUTHOR      DATE        DETAIL   
 1.0 -    Sarbpreet   10/07/2015  Created Apex class to insert contact role when a buying influence is created
 ================================================================================================================================================= 
*/
global without sharing class AP131_insertContactRoleBuyingInfluence {
     private static List<OpportunityContactRole> oppContactList = new List<OpportunityContactRole>();

 /*
     *  Method Name: insertContactRole
     *  Description: Insert Contact Role on insert of Buying Influence.
     *  Arguements: Map
     */
     public static void insertContactRole(Map<Id, Buying_Influence__c> triggernewMap)
    {
        try
        {
            oppContactList.clear();
            OpportunityContactRole oppContact;
            for(Buying_Influence__c buy : triggernewMap.values())
            {
                oppContact = new OpportunityContactRole();
                oppContact.ContactId = buy.Contact__c;
                oppContact.OpportunityId = buy.Opportunity__c;
                oppContact.Role = buy.mh_Role__c;
                oppContactList.add(oppContact);
            }
            
            insert oppContactList;
        }catch(Exception e) {
                for(Integer i =0; i< oppContactList.size(); i++) {                  
                    oppContactList[i].addError(ConstantsUtility.STR_ERROR+e.getMessage());
                } 
              } 
    }
}