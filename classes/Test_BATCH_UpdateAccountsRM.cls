@isTest
public class Test_BATCH_UpdateAccountsRM {

    
    static testMethod void test_BATCH_UpdateAccountsRM() {
        
       User user1 = new User();
       String adminprofile= Mercer_TestData.getsystemAdminUserProfile();      
       user1 = Mercer_TestData.createUser1(adminprofile,'usert7', 'usrLstName7', 7);
       system.runAs(User1){
       string s = 'select Account_Market__c, Account_Region__c, Account_Sub_Market__c, Account_Sub_Region__c, Office_Code__c, Relationship_Manager__r.Office_Record_Name__c,Account_Country__c, Relationship_Manager__r.Id, Relationship_Manager__r.Market__c, Relationship_Manager__r.Region__c, Relationship_Manager__r.Sub_Market__c, Relationship_Manager__r.Sub_Region__c, Relationship_Manager__r.Name , Relationship_Manager__r.CountryGeo__c,Relationship_Manager__r.Office__c, Relationship_Manager__c from Account where Relationship_Manager__r.isUpdated__c = true';  
        
        Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
         testMog.Name = 'ABC';
         testMog.Market__c = 'United States';
         testMog.Sub_Market__c = 'Canada – National';
         testMog.Region__c = 'Manhattan';
         testMog.Sub_Region__c = 'Canada';
         testMog.Country__c = 'Canada';
         testMog.Isupdated__c = true;
         testMog.Office_Code__c = '123';
         testMog.Office__c = 'US';
         insert testMog;
         
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.Location__c = testMog.Id;
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        //insert testColleague;
        
        Account testAcc = new Account();
        testAcc.Name = 'TestAccountName';
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingCountry = 'TestCountry';
        testAcc.BillingStreet = 'Test Street';
        //testAcc.Relationship_Manager__c = testColleague.Id;
        insert testAcc;
        
       
        Mercer_Office_Geo_D__c testMog1 = new Mercer_Office_Geo_D__c();
         testMog1.Name = 'ABC2';
         testMog1.Market__c = 'United States';
         insert testMog1; 
             
        //testColleague.Location__c = testMog1.Id;
       // update testColleague;
        
           
             
        
        database.executeBatch(new BATCH_UpdateAccountsRM(s), 200);  
       
    }
    }
}