/*Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History -----------------------
                                                              VERSION     AUTHOR  DATE        DETAIL                   
                                                                   1.0 -    Arijit Roy 03/29/2013  Created Schedulable class for Account
=============================================================================================================================================== 
*/
global class MS01_AccountMDriveStatusSchedulable implements Schedulable{

    global void execute(SchedulableContext sc)
    {
       Database.executeBatch(new MS17_ResetMercerAccountStatus(), Integer.valueOf(System.Label.CL80_MS17BatchSize)); 
    }
}