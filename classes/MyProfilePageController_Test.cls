@IsTest
public class MyProfilePageController_Test {

     /* Test class for providing coverage to MyProfilePageController class*/
     
    @IsTest(SeeAllData=true) static void testSave() {         
        // Modify the test to query for a portal user that exists in your org
        List<User> existingPortalUsers = [SELECT id, profileId, userRoleId FROM User WHERE UserRoleId <> null AND UserType='CustomerSuccess'];

        if (existingPortalUsers.isEmpty()) {
            User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                                FROM User WHERE id =: UserInfo.getUserId()];
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            //System.assert(controller.isEdit == false, 'isEdit should default to false');
            controller.edit();
            //System.assert(controller.isEdit == true);
            controller.cancel();
            //System.assert(controller.isEdit == false);
            
            //Test Data for Collegue
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.EMPLID__c = '12345678902';
            testColleague.LOB__c = '11111';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            
            //Test Data for Account
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '1234';
            insert testAccount;
            
            //Test Data for Contact
            Contact c = new Contact();
            c.LastName = 'TestContact';
            c.AccountId = testAccount.Id;
            c.email = 'abc1@xyz.com';
            insert c;
            
            controller.setContactFields(c, currentUser);
            controller.save();
            System.assert(Page.ChangePassword.getUrl().equals(controller.changePassword().getUrl()));
        } else {
            User existingPortalUser = existingPortalUsers[0];
            String randFax = Math.rint(Math.random() * 1000) + '5551234';
            
            System.runAs(existingPortalUser) {
                MyProfilePageController controller = new MyProfilePageController();
                System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
                System.assert(controller.getIsEdit() == false, 'isEdit should default to false');
                controller.edit();
                System.assert(controller.getIsEdit()== true);
                
                controller.cancel();
                System.assert(controller.getIsEdit() == false);
                
                controller.getUser().Fax = randFax;
                controller.save();
                System.assert(controller.getIsEdit() == false);
            }
            
            // verify that the user and contact were updated
            existingPortalUser = [Select id, fax, Contact.Fax from User where id =: existingPortalUser.Id];
            System.assert(existingPortalUser.fax == randFax);
            System.assert(existingPortalUser.Contact.fax == randFax);
        }
    }
}