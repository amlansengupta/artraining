global class Opportunity_USD_Fields_Update_Sched implements Schedulable{

     
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new Opportunity_USD_Fields_Update_Batch(),Integer.valueOf(Label.Opportunity_USD_Fields_Update_Query_Size_Label));
    }
    
    
}