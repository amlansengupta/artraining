public with sharing class ConstantsUtility {
    
    public static boolean oppLineItemUpdateSTOP = true;
    public static boolean oppLineItemUpdateSTOP1 = true;
    public static string STR_ACTIVE = 'Active' ;
    public static final string STR_YES = 'CAPITAL_YES';
    public static final string STR_NO = 'CAPITAL_NO';
    public static boolean skipTestClass = false;
    
    // Constants for AP83_ScopeItProjectUtil
    public static final string STR_BenefitsAdmin = 'Benefits Admin' ;
    public static final string STR_CrossBusinessOfferingsCBO = 'Cross Business Offerings (CBO)' ;
    public static final string STR_EmployeeHealthBenefits = 'Health' ;
    public static final string STR_Investments = 'Investments' ;
    public static final string STR_Retirement = 'Wealth' ;
    public static final string STR_Talent = 'Career';
    public static final string STR_GlobalBusinessSolutions = 'Global Business Solutions';
    public static final string STR_Other = 'Other';
    
    //Constants for AP87_createTempScope
    public static final string STR_Message = 'When saving as a template, the template name is defaulted to "ScopeIt Project Name - template".  A template already exists with this name.  Please update the ScopeIt Project Name field to make it unique.';
    public static final string STR_Template = '-Template';
    public static final string STR_TemplateMessage = 'Scope Project Template has been created. Template Name = "';
    public static final string STR_Blank = '';
    public static final string STR_SINGLESPACE = ' ';
    
    //Constants for AP96_scopeit_Add_Task_Employees
    public static final string STR_BackSlash = '/';
    public static final string STR_EditSubTitle = 'Edit Task and Employees';
    public static final string STR_AddSubTitle = 'Add Task and Employees';
    public static final string STR_Task =  'Task ';
    public static final string STR_ProjID =  'projid';
    public static final string STR_ErrorMessage =  'Please Enter Task Name!!';
    public static final string STR_PickVal ='Billable';
    
    //Constants for AP101_Scopeit_swap_employees
    public static final string STR_ObjID = 'id';
    public static final string STR_Object = 'object';
    
    //Constants for Mercer_profitDetails_class
    public static final string STR_Amber = 'Amber';
    public static final string STR_Red = 'Red';
    public static final string STR_Green = 'Green';
    public static final string STR_GreenCode = '#9ACD32';
    public static final string STR_AmberCode = '#FFBF00';
    public static final string STR_INV = 'INV';
    public static final string STR_CBO = 'CBO';
    public static final string STR_RET = 'WEA';
    public static final string STR_TAL = 'CAR';
    public static final string STR_HB = 'HEA';
    public static final string STR_GBS = 'GBS';
    public static final string STR_OTH = 'OTH';
    public static final string STR_BA = 'BA';
    
    //Constants for AP02_OpportunityTriggerUtil 
    public static final string STR_Closed_Won = 'Closed / Won';
    public static final string STR_Closed_Lost = 'Closed / Lost';
    //12638 merging Closed / Client Cancelled and Closed / Declined to Bid to Closed / Cancelled
   // public static final string STR_Closed_Declined_to_Bid = 'Closed / Declined to Bid';
    //public static final string STR_Closed_Client_Cancelled = 'Closed / Client Cancelled';
    public static final string STR_Closed_Cancelled = 'Closed / Cancelled';
    
    //Constants for AP04_ColleagueTriggerUtil
    public static final string STR_At = '@';
    public static final string STR_Two = '2';
     
    //Constants for AP108_ScopeModellingCopyTaskandEmployees
    public static final String STR_OBJ = 'Scope_Modeling__c';
    public static final String STR_COPY = 'Copy';
    public static final string STR_SYM = '_';
    public static final string STR_EXM = 'Copy Failed :';
    public static final String STR_TEMPOBJ = 'Temp_ScopeIt_Project__c';
    public static final String STR_BLNK = '';
    
    //Constants for AP110_ScopeItTemplate_Edit
    public static final String STR_MSG ='Please select a product before saving.';
    public static final String STR_URL ='/';
    public static final String STR_FILTER = 'Consulting Solution Area';
    
    //Constants for AP113_Mercer_Growth_Plan
    public static final String STR_ACID ='acid';
    public static final String STR_ERROR ='Operation Failed due to:';
    
    //constanst for AP122_CEMTriggerUtil and AP105_Mercer_CEM
     public static final String STR_STATUS ='Completed';
     public static final String STR_ACCID ='accid';
     public static final string STR_RENDER ='renderAs';
     public static final String STR_STATS ='In Progress';
     public static final String STR_SUB = 'Client Experience Measurement';
     public static final String STR_TASKSTATUS = 'Not Started';
     public static final String STR_PRIORITY = 'Normal';
     public static final String STR_RATING ='6';
     
     public static String STR_BYPASS_BUYINGINFLUENCE_FORBATCH ='false';  
     public static String STR_TRUE = 'True';
   
   
   

  
    
    
    
}