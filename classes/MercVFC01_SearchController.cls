/*Purpose:  This Apex class contains  the logic for searching Accounts 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Joban      1/21/2013   Created a controller Account search page.
   2.0 -    Sarbpreet  7/3/2014    Updated Comments/documentation.
  
============================================================================================================================================== 
*/
public class MercVFC01_SearchController 
{
    /*
    *  Creation of constructor 
    */
    public MercVFC01_SearchController(ApexPages.StandardController controller) {

    }


 public String nameQuery {get; set;}
 public transient List<Account> accounts {get; set;}
 public boolean results {get;set;}
 public boolean searchAcct{get;set;}
 public boolean noresults {get;set;}
 
  private String sortDirection = 'ASC';
   private String sortExp = 'name';

   public String sortExpression
   {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
       sortExp = value;
     }
   }
 
/*
 * @Description : This method fetches value for sorting the direction
 * @ Args       : null
 * @ Return     : String
 */  
public String getSortDirection()
 {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
      return 'ASC';
    else
     return sortDirection;
 }

/*
 * @Description : This method sets the  value for sorting the direction
 * @ Args       : null
 * @ Return     : String
 */
 public void setSortDirection(String value)
 {  
   sortDirection = value;
 }
 /*
  *     Getter method for getting accounts
  */
   public List<Account> getAccounts() {
       return accounts;
   }

 /*
 * @Description : This method executes the search functionality of accounts
 * @ Args       : null
 * @ Return     : PageReference
 */
 public PageReference executeSearch()
 {
  //String searchtext = nameQuery;   
 
  results = false;
  searchAcct = false;
  
  String queryStr='%' + nameQuery + '%';
  String sortExpressionAsterix;
  
    nameQuery = nameQuery.replace('*','');
    String searchtext = '%' + nameQuery.trim() + '%'; 
     
    //searchtext = searchtext.replaceAll('\\s','');
    //searchtext = searchtext.replace('*','');
    
     searchtext = searchtext.trim();
    

    String sortFullExp = sortExpression  + ' ' + sortDirection;
    String query = 'SELECT id,Account.Name,Account.Account_Name_Local__c,One_Code__c,One_Code_Status__c,Relationship_Manager__c,Relationship_Manager__r.Name,BillingCity,BillingCountry,BillingState,BillingStreet,ShippingCity,ShippingCountry,ShippingState,ShippingStreet,Duplicate_Flag__c,Surviving_OneCode__c   from Account WHERE ( Account.Name LIKE :searchtext OR Account.Account_Name_Local__c LIKE :searchtext OR BillingCity LIKE :searchtext OR BillingCountry LIKE :searchtext ) order by ';
     query += sortFullExp;
     query =String.escapeSingleQuotes(query);
    accounts =  Database.query(query);
      if(accounts.size()>1000)
      {
          ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Too Many Results Returned.');
          ApexPages.addMessage(errorMsg);
          return null;
      }
  else
  {
  
  
/*  accounts=[select id, Name from Account 
            where Account.Name = :searchtext]; */
            Integer size = accounts.size(); 

    if (size> 0)
    {
        results = true;
        noresults = false;
    
    }
    else
    {
        noresults = true;
        results = false;
    
    }
     
  searchAcct= true;             
  return null;
  }
 }
 
/*
 * @Description : Method for creating new Account
 * @ Args       : null
 * @ Return     : PageReference
 */
Public PageReference createnew(){

string url;


if (namequery<>'') 
{
    String encoded = EncodingUtil.urlEncode(nameQuery, 'UTF-8');
    //url = ('/001/e?retURL=%2F001%2Fo&nooverride=1&acc2='+nameQuery);
    url = '/apex/Mercer_Account_New?acc2=' + encoded + '&nooverride=1&retURL=%2F001%2Fo';
    }
    else
    {
    
        //url = '/001/e?retURL=%2F001%2Fo&nooverride=1';
        url = '/apex/Mercer_Account_New?retURL=%2F001%2Fo&nooverride=1';
    }
    
    PageReference pageRef = new PageReference(url);


        
    pageRef.setRedirect(true);

 return pageRef;
 
 }
/*
 *  Creation of constructor 
 */
 public MercVFC01_SearchController()
 {
  // if query appears in URL, execute it
  String urlQuery=ApexPages.currentPage().getParameters().get('query');
  
  if ((null!=urlQuery) && (0!=urlQuery.length()))
  {
   nameQuery=urlQuery;
   executeSearch();
  }
 }
 
}