/*Purpose: Test Class for providing code coverage to functionality related to Opportunity Lite
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Saravanan   07/13/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
Trisha          21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify' 
=======================================================
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_Opportunity_lite {
    /*
     * @Description : Test method to provide data coverage to functionality related to Opportunity Lite
     * @ Args       : Null
     * @ Return     : void
     */
    private static testMethod void testOppliteclass(){
          
        Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
        
        
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague1';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Location__c = mogd.Id;
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
       // testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;  
        
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '12345892';
        testColleague1.LOB__c = '1234';
        testColleague1.Location__c = mogd.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        testColleague1.Empl_Status__c = 'Active';
      //  testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
      
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
        
        

        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id 12638-Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //testOppty.OwnerId = tUser.Id;
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c='Hong Kong - Harbour';
        Test.startTest() ; 
        insert testOppty; 
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
               
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague1.Id;
        testSales.Percent_Allocation__c = 10;
         Test.stopTest() ;
        insert testSales;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
      
       
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+90;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+60; 
       
        insert opptylineItem;
        
        Opportunity_lite__c testol = new opportunity_lite__c();
        testol.opportunity__c = testoppty.id;
        testol.account__c = testaccount.id;
        insert testol;
        
        ApexPages.StandardController con = new ApexPages.StandardController(testol);
        opportunity_lite_related_list ol = new opportunity_lite_related_list(con);
        ol.getOppProducts(); 
        
       } 
    }
}