@isTest(seeAllData = true)
private class Test_ClientProductContactTriggerUtil1 {
  static testMethod void myUnitTest(){
        User testUser1 = new User();
        testUser1.alias = 'user1k2';
        testUser1.email = 'tek2@bbcm.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'TestUserCPC';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = [Select id from Profile where name = 'System Administrator' limit 1].id;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'bbb24@bbcm.com';
        testUser1.EmployeeNumber = '123391';
        testUser1.isActive = true;
        testUser1.S_Dash_Supervisor__c = null;
        testUser1.S_Dash_Supervisor_2__c = null;  
        testUser1.S_Dash_Supervisor_3__c = null;
        testUser1.S_Dash_Supervisor_4__c = null;  
        testUser1.S_Dash_Supervisor_5__c = null;    
        insert testUser1;
        
       System.runAs(testUser1){
        User testUser2 = new User();
        testUser2.alias = 'user1k2';
        testUser2.email = 'tek3@bbcm.com';
        testUser2.emailencodingkey='UTF-8';
        testUser2.lastName = 'TestUserCPC1';
        testUser2.languagelocalekey='en_US';
        testUser2.localesidkey='en_US';
        testUser2.ProfileId = [Select id from Profile where name = 'Mercer Standard' limit 1].id;
        testUser2.timezonesidkey='Europe/London';
        testUser2.UserName = 'bbb241@bbcm.com';
        testUser2.EmployeeNumber = '1233911';
        testUser2.isActive = true;
        testUser2.S_Dash_Supervisor__c = null;
        testUser2.S_Dash_Supervisor_2__c = null;  
        testUser2.S_Dash_Supervisor_3__c = null;
        testUser2.S_Dash_Supervisor_4__c = null;  
        testUser2.S_Dash_Supervisor_5__c = null;    
        insert testUser2;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        Account testAccount1 = new Account();  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = testColleague1.Id;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = testUser2.ID;
        testAccount1.GU_DUNS__c = '6666';
        testAccount1.DUNS__c = '123';
        testAccount1.HQ_Immediate_Parent_DUNS__c = '444';
        testAccount1.Domestic_DUNS__c = '555';
        testAccount1.One_Code__c = '123456';
        //testAccount1.GDPR_Opt_Out__c = false;
        insert testAccount1;
        
        Account testAccount2 = new Account();  
        testAccount2.Name = 'TestAccoun2t';     
        testAccount2.Relationship_Manager__c = testColleague1.Id;
        testAccount2.One_Code__c='123456';
        //testAccount1.BillingCity = 'TestCity';
        //testAccount1.ShippingCity = 'TestCity1';
        //testAccount1.ShippingStreet =  'TestStreet1';
        //testAccount1.BillingCountry = 'TestCountry';
        //testAccount1.BillingStreet = 'TestStreet';
        testAccount2.OwnerId = testUser2.ID;
        //testAccount1.GU_DUNS__c = '6666';
        //testAccount1.DUNS__c = '123';
        //testAccount1.HQ_Immediate_Parent_DUNS__c = '444';
        //testAccount1.Domestic_DUNS__c = '555';
        //testAccount1.GDPR_Opt_Out__c = false;
        insert testAccount2;
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Testcpc';
        con.Email = 'testcpc@test.com';
        con.AccountId = testAccount1.Id;        
        insert con;
        
        Talent_Contract__c tc = new Talent_Contract__c();
        tc.Account__c = testAccount1.Id;
        tc.Product__c = 'Rewards Membership';
        tc.Product_Status__c = 'Active';
        tc.Agreement_Type__c = 'SOW';
        tc.CurrencyIsoCode = 'INR';
        tc.License_Start_Date__c = system.today();
        tc.License_Expiration_Date__c = system.today()+30;
        tc.Term_in_Months__c = 1;
        tc.Billing_Currency__c = 'USD';
        tc.Service_License_Fee_Freq__c = 'Monthly';
        insert tc;
        
        Talent_Contract__c tc1 = new Talent_Contract__c();
        tc1.Account__c = testAccount2.Id;
        tc1.Product__c = 'Rewards Membership';
        tc1.Product_Status__c = 'Active';
        tc1.Agreement_Type__c = 'SOW';
        tc1.CurrencyIsoCode = 'INR';
        tc1.License_Start_Date__c = system.today();
        tc1.License_Expiration_Date__c = system.today()+30;
        tc1.Term_in_Months__c = 1;
        tc1.Billing_Currency__c = 'USD';
        tc1.Service_License_Fee_Freq__c = 'Monthly';
        insert tc1;
        
        Client_Product_Contact__c cpc = new Client_Product_Contact__c();
        cpc.Talent_Contract__c = tc.Id;
        cpc.Role__c = 'Billing Contact';
        cpc.Contact__c = con.Id;
        //insert cpc;
        
        Client_Product_Contact__c cpc1 = new Client_Product_Contact__c();
        cpc1.Talent_Contract__c = tc1.Id;
        cpc1.Contact__c = con.Id;
        insert cpc1;
        delete cpc1;
        insert cpc;
        delete cpc;        
       }
 
  }
}