public class ExceptionLogger {

    public static void logException(Exception ex, String originatingClass, String originatingMethod){
        try{
            String QueryLimit = '1. SOQL Queries used / SOQL Queries allowed: ' + Limits.getQueries() + '/' + Limits.getLimitQueries();
			String DMLimit = '2. Number of records queried so far /  Number allowed: ' + Limits.getDmlRows() + '/' + Limits.getLimitDmlRows();
			String DMLStat = '3. Number of DML statements used so far / Number allowed: ' +  Limits.getDmlStatements() + '/' + Limits.getLimitDmlStatements();   
			String CPUT = '4. Amount of CPU time (in ms) used so far / CPU usage time (in ms) allowed: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime();
            MFException__c error = new MFException__c();
            error.Exception_Type__c = ex.getTypeName();
            error.Error_Message__c = ex.getMessage();
            error.Stack_Trace__c = ex.getStackTraceString();
            error.Originating_Class__c = originatingClass;
            error.Originating_Method__c = originatingMethod;
            error.Line_Number__c = ex.getLineNumber();
            error.Gov_Limits_in_Executed_Context__c = String.format('{0}\n{1}\n{2}\n{3}',new List<String>{QueryLimit, DMLimit,DMLStat,CPUT});
            error.Log_Time__c = system.now();
            error.RunningUser__c = UserInfo.getUserId();
            insert error;
            //system.debug(error);
        }catch(Exception e){
            system.debug(e.getMessage());
        }
    }
}