/* =============================================================================================
Request Id : 17100 
This class is used as the test class for ClientRevenueController
Date : 25-MAR-2019 
Created By : Keerthi Ramalingam
===============================================================================================*/

@isTest
public class ClientRevenueController_Test { 
    
    private static testMethod void clientRevenueController_Test_testMethod(){
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        insert testAccount;
         Account testAccount2 = new Account();

        ClientRevenueController.fetchRevenueDetails(testAccount.Id);
        try{
        	ClientRevenueController.fetchRevenueDetails(testAccount2.Id);
        }catch(Exception ex){
            system.debug(ex.getMessage());
        }
       
}
}