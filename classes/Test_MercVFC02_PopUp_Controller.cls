/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_MercVFC02_PopUp_Controller {
      Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'LiteProfiles';
        setting.StrValue__c = [Select Id from Profile where Name LIKE '%LITE' LIMIT 1].Id;
        insert setting;
    }

    /* * @Description : This test method provides data coverage for  Mercer Login Popup Page        
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest() {
        
         createCS();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        PageReference pageRef = Page.Mercer_Login_Popup;
        Test.setCurrentPage(pageRef);
        MercVFC02_PopUp_Controller controller = new MercVFC02_PopUp_Controller();
        MercVFC02_PopUp_Controller.HasUserChecked();
        controller.updateUserDisclaimerViewed();
        }
        
    }
}