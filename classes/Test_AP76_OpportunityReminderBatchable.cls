/* Purpose:This test class provides dat acoverage to  mass update Opportunities with a chatter post which closes in next week.
===================================================================================================================================
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          12/11/2013  Created test class
 ======================================================================================================================================
 */
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP76_OpportunityReminderBatchable {
    /*
     * @Description : Test method to  mass update Opportunities with a chatter post which closes in next week
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
         
         Colleague__c testColleague = new Colleague__c();
         testColleague.Name = 'Test Colleague';
         testColleague.EMPLID__c = '12345678024';       
         testColleague.LOB__c = '111214';       
         testColleague.Last_Name__c = 'test LastName';      
         testColleague.Empl_Status__c = 'Active';      
         testColleague.Email_Address__c = 'test@test.com';  
     
         insert testColleague;

         Account testAccount = new Account();
         testAccount.Name = 'Test Account';
         testAccount.Relationship_Manager__c = testColleague.Id;  
         testAccount.BillingCity = 'TestCity';   
         testAccount.BillingCountry = 'TestCountry'; 
         testAccount.BillingStreet = 'Test Street'; 
        
         insert testAccount;
           
         Opportunity testOpportunity = new Opportunity();
         testOpportunity.Name = 'Sample Opportunity';
         //request id:12638 commenting step
         //testOpportunity.Step__c = 'Identified Deal';
         testOpportunity.StageName = 'Identify';
         testOpportunity.CloseDate = Date.Today()+ 7;
         testOpportunity.AccountId = testAccount.ID;
         testOpportunity.CurrencyIsoCode = 'USD' ;
         testOpportunity.Type = 'New Client';
         testOpportunity.Product_LOBs__c= '';
         testOpportunity.Count_Product_LOBs__c= 0;
         testOpportunity.Opportunity_Office__c = 'Aarhus - Axel';
         Test.startTest();     
         insert testOpportunity;
         
         String  qry = 'Select id,Name, CloseDate, Reminder__c, Accountid from Opportunity where  CloseDate <> null and  CloseDate = NEXT_WEEK LIMIT 10 ';
         database.executeBatch(new AP76_OpportunityReminderBatchable(qry), 100); 
         Test.stopTest();
                 
    }
  }