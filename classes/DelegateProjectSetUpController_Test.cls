@isTest(SeeAllData=True)
public class DelegateProjectSetUpController_Test{
    
    static testMethod void method1(){
        Test.startTest();
        ConstantsUtility.skipTestClass = true;
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c='Oliver Wyman Group';
        insert testColleague;  
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = new User(LastName = 'LIVESTON',
                                 FirstName='JASON',
                                 Alias = 'jliv',
                                 Email = 'jason.liveston@asdf.com',
                                 Username = 'jason.test@test.com',
                                 ProfileId = profileId.id,
                                 TimeZoneSidKey = 'GMT',
                                 LanguageLocaleKey = 'en_US',
                                 EmailEncodingKey = 'UTF-8',
                                 LocaleSidKey = 'en_US',
                                 Employee_office__c='Mercer US Mercer Services - US03'      
                                );
        
        insert testUser;
        System.runAs(testUser){
            Account testAccount = new Account();        
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.OwnerId = testUser.Id;
            testAccount.Account_Region__c='EuroPac';
            testAccount.RecordTypeId='012E0000000QxxD';
            testAccount.One_Code__c = 'abc';
            insert testAccount;
            
            Contact testContact = new Contact();
            testContact.Salutation = 'Fr.';
            testContact.FirstName = 'TestFirstName';
            testContact.LastName = 'TestLastName';
            testContact.Job_Function__c = 'TestJob';
            testContact.Title = 'TestTitle';
            testContact.MailingCity  = 'TestCity';
            testContact.MailingCountry = 'TestCountry';
            testContact.MailingState = 'TestState'; 
            testContact.MailingStreet = 'TestStreet'; 
            testContact.MailingPostalCode = 'TestPostalCode'; 
            testContact.Phone = '9999999999';
            testContact.Email = 'abc@xyz777.com';
            testContact.MobilePhone = '9999999999';
            testContact.AccountId = testAccount.Id;
            insert testContact;
            
            
            Opportunity testOppty1 = new Opportunity();
            testOppty1.Name = 'TestOppty';
            testOppty1.Type = 'New Client';
            testOppty1.AccountId = testAccount.Id;
            //removing step #12638 sprint 2 dependency
            //testOppty1.Step__c = 'Identified Deal';
            testOppty1.StageName = 'Identify';
            testOppty1.CloseDate = date.Today();
            testOppty1.CurrencyIsoCode = 'ALL';
            testOppty1.OwnerId = testUser.Id;
            testOppty1.Opportunity_Office__c = 'Shanghai - Huai Hai';
            testOppty1.Account_Region__c='EuroPac';
            testOppty1.Sibling_Contact_Name__c=testColleague.id;
            insert testOppty1;
            
            Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
            
            Product2 pro = new Product2();
            pro.Name = 'TestPro';
            pro.Family = 'RRF';
            pro.IsActive = True;
            insert pro;
            
            PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty1.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptyLineItem.Revenue_End_Date__c = date.Today()+3;
            opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
            opptyLineItem.UnitPrice = 1.00;
            opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
            opptyLineItem.Year2Revenue_edit__c = null;
            opptyLineItem.Year3Revenue_edit__c = null;
            opptyLineItem.Project_Manager__c=testColleague.Id;
            opptyLineItem.Sales_Price_USD_calc__c = 1.00;
            opptyLineItem.CurrentYearRevenue_edit__c = 0.00;
            opptyLineItem.Is_Project_Required__c=true;
            opptyLineItem.Project_Linked__c=false;
            insert opptylineItem;
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=testOppty1.id;
            insert attach;
            
            DelegateProjectSetUpController.getOpportunity(testOppty1.Id);
            String var=DelegateProjectSetUpController.pushMailBodyAsHTML(testOppty1.Id);
            System.debug('my value:'+var); 
            String varTest='{\"To\":\"sankar.mitra@mercer.com\",\"Cc\":\"sankar.mitra@mercer.com\",\"Bcc\":\"sankar.mitra@mercer.com\",\"Attachments\":[{\"name\":\"Unit Test Attachment\",\"documentId\":\"'+attach.Id+'\"}],\"Body\":\"<p>Opportunity Owners,</p><p><br></p><p>Before sending this email to your delegate:</p><p><br></p><p>1. Complete <b>REQUIRED</b> missing details below.</p><p><br></p><p>2. <b>Attach the relevant client agreement and/or note any existing client documents </b>that should be linked to the project(s).</p><p><br></p><p>_____________________</p><p><br></p><p>Click the link(s) below to set up chargeable project code(s) in WebCAS for the following MercerForce opportunity:</p><p><br></p><p><b>Opportunity Name: </b>TestOppProdExpansion2</p><p><br></p><p><b>Account: </b>Test Account 5 (123456)</p><p><br></p><p><b>Total Opportunity Revenue: </b>1 USD</p><p><br></p><p><b>Product: </b>Absence</p><p><b>Project Required: </b>Y</p><p><a href=\\"http://webcas-goc.mercer.com:8000/?end_form=gocprojsf~123456~O-01114256~00k4D000002f7INQAY\\" target=\\"_blank\\">http://webcas-goc.mercer.com:8000/?end_form=gocprojsf~123456~O-01114256~00k4D000002f7INQAY</a></p><p><br></p><p><b>Charge Basis:</b> <b>REQUIRED</b></p><p><br></p><p><b>Payment Terms:</b> <b>REQUIRED in US</b></p><p><br></p><p><b>Engagement: REQUIRED - enter existing engagement details or request new</b></p><p><br></p><p><b>Project Manager: </b> </p><p><br></p><p><b>Project Fee: </b>1 USD</p><p><br></p><p><b>Program / Program Manager: </b>Enter a value if required</p><p><br></p><p><b>Opp Prod Id: </b>00k4D000002f7INQAY</p><p><br></p>\"}';
            DelegateProjectSetUpController.sendEmail(varTest);
            try{
            DelegateProjectSetUpController.deleteUploadedDocument(attach.Id);           
            }catch(Exception ex){}
        }
    }
}