@isTest
private class Test_SCH_UpdateOpportunityFields
{
  
    static testMethod void Test_SCH_UpdateOpportunityFields() 
    {
        Test.StartTest();
        User user1 = new User();
      String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert7', 'usrLstName7', 7);
        
        
        system.runAs(User1){
            SCH_UpdateOpportunityFields bc = new SCH_UpdateOpportunityFields();
            String sch = '0 1 23 * * ?';
            system.schedule('Test SCH_UpdateOpportunityFields', sch, bc);
        }
        Test.stopTest();
    }
}