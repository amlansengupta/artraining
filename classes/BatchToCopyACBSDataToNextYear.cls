global class BatchToCopyACBSDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopyACBSDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String header = 'Account,Actions for Improving relationships,Buying Center,Buying Center & Decision Maker,Decision Criteria / Preferences,Decision Maker(Customer),Growth Plan,Our Alignment,Relationship Status,Record Status \n';
        string hearder2= 'Id,Account,Actions for Improving relationships,Buying Center,Buying Center & Decision Maker,Decision Criteria / Preferences,Decision Maker(Customer),Growth Plan,Our Alignment,Relationship Status,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear  ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>();
        System.debug('Alignment with Client Buying Structure foldGPList>>'+oldGPList.size());
        try { 
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
		}
			
		Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
		List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
		for(Account a : accList){
			for(Growth_Plan__c gg : a.Growth_Plans__r){
				accIdVsNewGp.put(gg.Account__c,gg);
			}  
		}
        
        System.debug('Alignment with Client Buying Structure accList>>'+accList.size());
        List<AccountAlignmentClient__c> insertNewAACObjList = new List<AccountAlignmentClient__c>();			
            List<AccountAlignmentClient__c> aacObjectList = [select id,name,Account__c,Actions_for_Improving_relationships__c,Buying_Center__c,Buying_Center_Decision_Maker_Customer__c,Colleague__c,Contact__c,Decision_Criteria_Preferences__c,Decision_Maker_Customer__c,Growth_Plan__c,Our_Alignment__c,Relationship_Status__c,LastModifiedBy.Name,lastModifiedDate from AccountAlignmentClient__c where Growth_Plan__c IN :oldGPList];
		 System.debug('Alignment with Client Buying Structure aacObjectList>>'+aacObjectList.size());
            if(aacObjectList != null && aacObjectList.size()>0){
                for(AccountAlignmentClient__c aacOldObj : aacObjectList){
                    if(accIdVsNewGp.containsKey(aacOldObj.Account__c)){
                        AccountAlignmentClient__c aacNewObj = new AccountAlignmentClient__c();
                        
                        if(aacOldObj.Account__c != null)
                            aacNewObj.Account__c = aacOldObj.Account__c;
                        aacNewObj.Growth_Plan__c = accIdVsNewGp.get(aacOldObj.Account__c).Id;
                        
                        if(aacOldObj.Actions_for_Improving_relationships__c != null)
                            aacNewObj.Actions_for_Improving_relationships__c = aacOldObj.Actions_for_Improving_relationships__c;
                        if(aacOldObj.Buying_Center__c != null)
                            aacNewObj.Buying_Center__c = aacOldObj.Buying_Center__c;
                        if(aacOldObj.Buying_Center_Decision_Maker_Customer__c != null)
                            aacNewObj.Buying_Center_Decision_Maker_Customer__c = aacOldObj.Buying_Center_Decision_Maker_Customer__c;
                        if(aacOldObj.Colleague__c != null)
                            aacNewObj.Colleague__c = aacOldObj.Colleague__c;
                        if(aacOldObj.Contact__c != null)
                            aacNewObj.Contact__c = aacOldObj.Contact__c;
                        if(aacOldObj.Decision_Criteria_Preferences__c != null)
                            aacNewObj.Decision_Criteria_Preferences__c = aacOldObj.Decision_Criteria_Preferences__c;
                        if(aacOldObj.Decision_Maker_Customer__c != null)
                            aacNewObj.Decision_Maker_Customer__c = aacOldObj.Decision_Maker_Customer__c;
                        if(aacOldObj.Our_Alignment__c != null)
                            aacNewObj.Our_Alignment__c = aacOldObj.Our_Alignment__c;
                        if(aacOldObj.Relationship_Status__c != null)
                            aacNewObj.Relationship_Status__c = aacOldObj.Relationship_Status__c;
                        
                        aacNewObj.BuyingStructure_LastModified_By__c = aacOldObj.LastModifiedBy.Name;
                        DateTime dt = aacOldObj.LastModifiedDate;
                        aacNewObj.BuyingStructure_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
                        
                        insertNewAACObjList.add(aacNewObj);
                    }
                }
            }
            System.debug('Alignment with Client Buying Structure insertNewAACObjList>>'+insertNewAACObjList.size());
            if(insertNewAACObjList!=null && insertNewAACObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewAACObjList,false);
	    		if(srList!=null && srList.size()>0){
    				for(Integer i=0;i<srList.size();i++){
    					String actionForIMproving='',buyingcenterAndDecision='',decisionCriteria='',decisionMaker='',ourAlingment='',realtionShipStatus='';
    					
    					if(insertNewAACObjList[i].Actions_for_Improving_relationships__c!=null)
    						actionForIMproving=insertNewAACObjList[i].Actions_for_Improving_relationships__c;
    						
    					if(insertNewAACObjList[i].Buying_Center_Decision_Maker_Customer__c!=null)
    						buyingcenterAndDecision=insertNewAACObjList[i].Buying_Center_Decision_Maker_Customer__c;
    						
    				    	
    						
    				    if(insertNewAACObjList[i].Buying_Center_Decision_Maker_Customer__c!=null)
    						decisionMaker=insertNewAACObjList[i].Buying_Center_Decision_Maker_Customer__c;
    						
    						
    					if(insertNewAACObjList[i].Decision_Criteria_Preferences__c!=null)
    						decisionCriteria=insertNewAACObjList[i].Decision_Criteria_Preferences__c;
    						
    						
    					if(insertNewAACObjList[i].Our_Alignment__c!=null)
    						ourAlingment=insertNewAACObjList[i].Our_Alignment__c;
    						
    					if(insertNewAACObjList[i].Relationship_Status__c!=null)
    						realtionShipStatus=insertNewAACObjList[i].Relationship_Status__c;
    						
    						
    					
    						
    						
    						
    						
    					if(srList.get(i).isSuccess()){
    						finalSuccessStr+=srList.get(i).getId()+','+insertNewAACObjList[i].Account__c+','+actionForIMproving.replace(',', '')+','+buyingcenterAndDecision.replace(',', '')+','+decisionCriteria.replace(',', '')+','+decisionMaker.replace(',', '')+','+insertNewAACObjList[i].Growth_Plan__c+','+ourAlingment.replace(',', '')+','+realtionShipStatus.replace(',', '')+', Successfully created \n';
    					}
    					if(!srList.get(i).isSuccess()){
    						Database.Error error = srList.get(i).getErrors().get(0);
    						finalFailStr+=insertNewAACObjList[i].Account__c+','+actionForIMproving.replace(',', '')+','+buyingcenterAndDecision.replace(',', '')+','+decisionCriteria.replace(',', '')+','+decisionMaker.replace(',', '')+','+insertNewAACObjList[i].Growth_Plan__c+','+ourAlingment.replace(',', '')+','+realtionShipStatus.replace(',', '')+','+error.getMessage()+' \n';
    					}
    				}
    			}
	    	}
	    	
	    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }
    }
    
    global void finish(Database.BatchableContext bc){
		System.debug('Alignment with Client Buying Structure finish finalSuccessStr>>>>'+finalSuccessStr);
		System.debug('Alignment with Client Buying Structure finish finalFailStr>>>> '+finalFailStr);
		    
		    
		 AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Alignment with Client Buying Structure Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Alignment with Client Buying Structure Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Alignment with Client Buying Structure Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Alignment with Client Buying Structure copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}