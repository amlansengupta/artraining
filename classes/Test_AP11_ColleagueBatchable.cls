/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*Purpose: This Test Class tests for Coverage and test cases of AP11_ColleagueBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   01/02/2013  Test Class
============================================================================================================================================== 
*/
@isTest
public class Test_AP11_ColleagueBatchable {

    /*
     * @Description : Test method to update account data when account's RM office is changed 
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void test_AP11_ColleagueBatchable() {
        
       User user1 = new User();
       String adminprofile= Mercer_TestData.getsystemAdminUserProfile();      
       user1 = Mercer_TestData.createUser1(adminprofile,'usert2', 'usrLstName2', 2);
       system.runAs(User1){
       string s = 'select Account_Market__c, Account_Region__c, Account_Sub_Market__c, Account_Sub_Region__c, Office_Code__c, Account_Country__c, Relationship_Manager__r.Id, Relationship_Manager__r.Market__c, Relationship_Manager__r.Region__c, Relationship_Manager__r.Sub_Market__c, Relationship_Manager__r.Sub_Region__c, Relationship_Manager__r.Name , Relationship_Manager__r.CountryGeo__c,Relationship_Manager__r.Office__c, Relationship_Manager__c from Account where Relationship_Manager__r.isUpdated__c = true';  
        
        Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
         testMog.Name = 'ABC';
         testMog.Market__c = 'United States';
         testMog.Sub_Market__c = 'Canada – National';
         testMog.Region__c = 'Manhattan';
         testMog.Sub_Region__c = 'Canada';
         testMog.Country__c = 'Canada';
         testMog.Isupdated__c = true;
         testMog.Office_Code__c = '123';
         testMog.Office__c = 'US';
         insert testMog;
         
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.Location__c = testMog.Id;
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        Account testAcc = new Account();
        testAcc.Name = 'TestAccountName';
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingCountry = 'TestCountry';
        testAcc.BillingStreet = 'Test Street';
        testAcc.Relationship_Manager__c = testColleague.Id;
        insert testAcc;
        
       
        Mercer_Office_Geo_D__c testMog1 = new Mercer_Office_Geo_D__c();
         testMog1.Name = 'ABC2';
         testMog1.Market__c = 'United States';
         insert testMog1; 
             
        testColleague.Location__c = testMog1.Id;
        update testColleague;
        
            
        //List<Account> aList = [select Account_Market__c, Account_Region__c, Account_Sub_Market__c, Account_Sub_Region__c, Office_Code__c, Account_Country__c, Relationship_Manager__r.Id, Relationship_Manager__r.Market__c, Relationship_Manager__r.Region__c, Relationship_Manager__r.Sub_Market__c, Relationship_Manager__r.Sub_Region__c, Relationship_Manager__r.Name , Relationship_Manager__r.CountryGeo__c, Relationship_Manager__c from Account LIMIT 2];
            
          for (Account account : [select Account_Market__c, Account_Region__c, Account_Sub_Market__c, Account_Sub_Region__c, Office_Code__c, Account_Country__c, Relationship_Manager__r.Id, Relationship_Manager__r.Market__c, Relationship_Manager__r.Region__c, Relationship_Manager__r.Sub_Market__c, Relationship_Manager__r.Sub_Region__c, Relationship_Manager__r.Name , Relationship_Manager__r.CountryGeo__c, Relationship_Manager__c from Account LIMIT 2])
          {
             account.Account_Country__c = account.Relationship_Manager__r.CountryGeo__c;
             account.Market_Segment__c = account.Relationship_Manager__r.Market__c;
             account.Account_Sub_Market__c = account.Relationship_Manager__r.Sub_Market__c;
             account.Account_Region__c = account.Relationship_Manager__r.Region__c;
             account.Account_Sub_Region__c = account.Relationship_Manager__r.Sub_Region__c;

          }
            // update aList;
             
        
        database.executeBatch(new AP11_ColleagueBatchable(s), 500);  
       
    }
    }
}