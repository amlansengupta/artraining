/***************************************************************************************************************
Request# : 17449
Purpose : This class is created as the test class of MF2_CopyTaskNewController class.
Created by : Harsh Vats
Created Date : 11/01/2019
Project : MF2
****************************************************************************************************************/
@isTest
public class MF2_CopyTaskNewControllerTest {
    /*Request# : 17449: test method to test the copy task button functionality*/
    public static testmethod void copyTaskTest()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        //Account test data creation
        Account testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        //testAccount1.Relationship_Manager__c = collId;
        testAccount1.One_Code__c='ABC123XYZ1';
        testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
        testAccount1.Integration_Status__c = 'Error';
        insert testAccount1;
        String accID = testAccount1.ID;
        
        //Opportunity test data creation
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  accID; 
        //Removing step #12638, sprint 2 dependency
        //testOpportunity.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        //Replacing 'Pending Step' with 'Identify' #12638, sprint 2 dependency
        testOpportunity.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(2018, 1, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        testOpportunity.Close_Stage_Reason__c ='Other';
        insert testOpportunity;
        Id OpptyId = testOpportunity.ID;        
        
        //Scope Modeling test data creation
        Scope_Modeling__c   testScopeModeling = new Scope_Modeling__c();
        testScopeModeling.Name = 'testModel1';
        testScopeModeling.CurrencyIsoCode = 'USD';
        testScopeModeling.Sales_Price__c = 100;
        insert testScopeModeling;
        Id scopeModelId = testScopeModeling.Id;
        
        
        //Scope Modeling Task test data creation
        Scope_Modeling_Task__c testScopeModelTask = new Scope_Modeling_Task__c();   
        testScopeModelTask.Task_Name__c = 'testTask';
        testScopeModelTask.Scope_Modeling__c = scopeModelId;
        List<Scope_Modeling_Task__c> listScopeModelTask = new List<Scope_Modeling_Task__c>();
        listScopeModelTask.add(testScopeModelTask);
        
        System.runAs(user1)
        {
            ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(listScopeModelTask);
            MF2_CopyTaskNewController controller = new MF2_CopyTaskNewController(stdController);
            controller.redirectToScope();
            system.assertequals(testScopeModelTask.Scope_Modeling__c,scopeModelId);
        }
        
    }
    
}