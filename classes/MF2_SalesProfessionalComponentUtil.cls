public without sharing class MF2_SalesProfessionalComponentUtil {
	
    @AuraEnabled
    public static boolean getOpportunityStatus(Id opportunityId){
        try{
        	Opportunity oppty = [Select StageName from Opportunity where Id =: opportunityId];    
            if(oppty != null && oppty.StageName.equalsIgnoreCase('Closed / Won')){
                return true;
            }
        }catch(Exception ex){
            system.debug('Error:'+ex.getMessage());
         	ExceptionLogger.logException(ex, 'MF2_SalesProfessionalComponentUtil', 'getOpportunityStatus');
        }
        return false;
    }
}