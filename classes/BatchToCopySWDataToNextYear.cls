global class BatchToCopySWDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;
	
    global BatchToCopySWDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String header = 'Account,Growth Plan,Weakness,Record Status \n';
        string hearder2= 'Id,Account,Growth Plan,Weakness,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear  ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>(); 
         System.debug('swot weak>>oldGPList>> '+oldGPList.size());
        try {
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
				
		}
		
		Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
		List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
		for(Account a : accList){
			for(Growth_Plan__c gg : a.Growth_Plans__r){
				accIdVsNewGp.put(gg.Account__c,gg);
			}  
		}
        System.debug('swot weak>>accList>> '+accList.size());
        
        List<Swot_Weak__c> insertNewSWObjList = new List<Swot_Weak__c>();	 
        List<Swot_Weak__c> swObjectList = [select id,name,Account__c,Growth_Plan__c,Weakness__c,LastModifiedBy.Name,lastModifiedDate from Swot_Weak__c where Growth_Plan__c IN :OldGPList];
         System.debug('swot weak>>swObjectList>> '+swObjectList.size());
        if(swObjectList != null && swObjectList.size()> 0){
            for(Swot_Weak__c swOldObj : swObjectList){
                if(accIdVsNewGp.containsKey(swOldObj.Account__c)){
                    Swot_Weak__c swNewObj = new Swot_Weak__c();
                    
                    if(swOldObj.Account__c != null)
                        swNewObj.Account__c = swOldObj.Account__c;
                    swNewObj.Growth_Plan__c = accIdVsNewGp.get(swOldObj.Account__c).Id;
                    
                    if(swOldObj.Weakness__c != null)
                        swNewObj.Weakness__c = swOldObj.Weakness__c;
                    
                    swNewObj.Weak_LastModified_By__c = swOldObj.LastModifiedBy.Name;
                    DateTime dt = swOldObj.LastModifiedDate;
                    swNewObj.Weak_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
                   
                   insertNewSWObjList.add(swNewObj);
                }
            }
        }
         System.debug('swot weak>>insertNewSWObjList>> '+insertNewSWObjList.size());
        if(insertNewSWObjList!=null && insertNewSWObjList.size()>0){
    		Database.SaveResult[] srList = Database.insert(insertNewSWObjList,false);
    		if(srList!=null && srList.size()>0){
				for(Integer i=0;i<srList.size();i++){
					
					String weak='';
					if(insertNewSWObjList[i].Weakness__c!=null)
						weak=insertNewSWObjList[i].Weakness__c;
					
					if(srList.get(i).isSuccess()){
						finalSuccessStr+=srList.get(i).getId()+','+insertNewSWObjList[i].Account__c+','+insertNewSWObjList[i].Growth_Plan__c+','+weak.replace(',', '')+', Successfully created \n';
					}
					if(!srList.get(i).isSuccess()){
						Database.Error error = srList.get(i).getErrors().get(0);
						finalFailStr+=insertNewSWObjList[i].Account__c+','+insertNewSWObjList[i].Growth_Plan__c+','+weak.replace(',', '')+','+error.getMessage()+' \n';
					}
				}
			}
    	}
    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }
    }
    
    global void finish(Database.BatchableContext bc){
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Swot Weak Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Swot Weak Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Swot Weak Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Swot Weak copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}