/*Purpose: Test Class for providing code coverage to Mercer_Request_Class class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   01/10/2014  Created test class
============================================================================================================================================== 
*/
@isTest
public class test_Mercer_Request_Class{
    
    /* * @Description : This test method provides data coverage to Mercer_Request_Class class         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void test_Mercer_Request(){
    
         
        User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
	        String inqRecTypId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
	        String defRecTypId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Defect').getRecordTypeId(); 
	        String reqRecTypId = Schema.SObjectType.Request__c.getRecordTypeInfosByName().get('Request').getRecordTypeId(); 
	
	        Request__c testR = new Request__c();
	        testR.recordTypeId = inqRecTypId;
	        testR.Requirement_Title__c = 'test rec';
	        insert testR;
	
	        System.CurrentPageReference().getParameters().put('RecordType',inqRecTypId);
	        ApexPages.StandardController con = new ApexPages.StandardController(testR);
	        Mercer_Request_Class mrc = new Mercer_Request_Class(con);
	        mrc.redirect();
	        
	        testR.recordTypeId = defRecTypId;      
	        update testR;
	        
	        System.CurrentPageReference().getParameters().put('RecordType',defRecTypId);
	        ApexPages.StandardController con1 = new ApexPages.StandardController(testR);
	        Mercer_Request_Class mrc1 = new Mercer_Request_Class(con1);
	        mrc1.redirect();
	        
	        testR.recordTypeId = reqRecTypId;      
	        update testR;
	        
	        System.CurrentPageReference().getParameters().put('RecordType',reqRecTypId);
	        ApexPages.StandardController con2 = new ApexPages.StandardController(testR);
	        Mercer_Request_Class mrc2 = new Mercer_Request_Class(con2);
	        mrc2.redirect();
        }
    
    }
    

}