/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(SeeAllData=true)
public class Mercer_DelegatedCodeSetUpController_Test {
    
     private static Product2 testProduct = new Product2();
    private static Mercer_TestData mtdata = new Mercer_TestData();
     static testMethod void myUnitTest1()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){ 
            
            Account acct = mtdata.buildAccount();
            
          
            //test.startTest();           
            Product2 testProduct2 = new Product2();
            testProduct2.Name = 'TestPro';
            testProduct2.Family = 'RRF';
            testProduct2.IsActive = True;
            testProduct2.LOB__c = 'TEST Bus';
            testProduct2.Segment__c = 'TEST seg';
            testProduct2.Classification__c ='Non Consulting Solution Area';
            insert testProduct2;
            
            Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
                  
            
            PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :testProduct2.Id and CurrencyIsoCode = 'ALL' limit 1];
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty1';
            testOppty.Type = 'New Client';
            testOppty.AccountId = acct.id;
            //request id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Pricebook2Id = pb.id;
                testOppty.Opportunity_Office__c = 'Dallas - Main';
            test.startTest(); 
            insert testOppty;
            
            OpportunityLineItem opptylineItem = new OpportunityLineItem();
            opptylineItem.OpportunityId = testOppty.Id;
            opptylineItem.PricebookEntryId = pbEntry.Id;
            opptyLineItem.Revenue_End_Date__c = date.Today()+30;
            opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
            opptylineItem.UnitPrice = 1.00;
            insert opptylineItem;
            
            
            
            
            
            test.stoptest();
            
            ApexPages.StandardController controller =new ApexPages.StandardController(testOppty);
            Mercer_DelegatedCodeSetUpController delegate = New Mercer_DelegatedCodeSetUpController(controller);
            Mercer_DelegatedCodeSetUpController.getOppDetail(testOppty.id);
            Mercer_DelegatedCodeSetUpController.getOppLineItemDetail(testOppty.id);    
            
            
        }
    }
}