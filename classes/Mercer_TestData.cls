/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
 * The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest
public class Mercer_TestData {

    private static User requester = new User();
    private static User testUser1 = new User();
    private static Account testAccount = new Account();
    private static Account testAccount1 = new Account();
    private static AccountTeamMember testAccountTeamMember = new AccountTeamMember();
    private static Opportunity testOpportunity = new Opportunity();
    private static Opportunity testOpportunity1 = new Opportunity();
    private static OpportunityTeamMember testOpportunityTeamMember;
    private static Contact testContact = new Contact();
    private static ContactShare testContactShare = new ContactShare();
    private static Contact_Team_Member__c testContactTeamMember = new Contact_Team_Member__c();
    private static Colleague__c testColleague = new Colleague__c();
    private static Colleague__c testColleague1 = new Colleague__c();
    private static Sales_Credit__c testSalesCredit = new Sales_Credit__c();
    private static Sales_Credit__c testSalesCredit1 = new Sales_Credit__c();
    private static Product2 testProduct = new Product2();
    private static OpportunityLineItem opptylineItem = new OpportunityLineItem();   
    private static Product2 testScopeProduct = new Product2();
    private static Growth_Plan__c testGrowthPlan = new Growth_Plan__c();
    private static Revenue_Connectivity__c testProject = new Revenue_Connectivity__c();
    private static Activity_Archive__c testActArchive = new Activity_Archive__c();
   
    
    private static List<OpportunityTeamMember> testOpportunityTeamMemberList  = new List<OpportunityTeamMember>();
    private static List<Sales_Credit__c> testSalesCreditList = new List<Sales_Credit__c>();
    private static List<Sales_Credit__c> testSalesCreditList1 = new List<Sales_Credit__c>();
    
    private static String mercerStandardUserProfile;
    private static Id collId; 
    private static Id collId1;
    private static Id acctId; 
    private static Id acctId1;
    private static Id OpptyId;
    private static Id OpptyId1; 
    private static Id oppId;
    private static Id contId;
    private static Id conTMember;
    private static Id scproid;
    private static Id gplanid;
    private static Id opptyLineItemId;
    private static Id mogId;
    private static Id mogId1;    
    private static Integer month = System.Today().month();
    private static Integer year = System.Today().year();
    
    /*
     * @Description : Test data for creating Standard User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    static
    {
        mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
    }


   
    /*
     * @Description : Test data for creating test user
     * @ Args       : Null
     * @ Return     : void
     */     
    public static User createUser(String profileId, String alias, String lastName, User testUser)
    {   
        UserRole roleId = [SELECT Id FROM UserRole WHERE Name = 'Canada - Wealth User' LIMIT 1];
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890';  
        testUser.UserRoleId = roleId.Id;
        insert testUser;         
        return testUser;
    }
    
     /*
     * @Description : Test data for creating test user
     * @ Args       : Null
     * @ Return     : void
     */ 
    public static User createUser1(String profileId, String alias, String lastName, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser1.alias = alias;
        testUser1.email = 'test@xyz.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = lastName;
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = profileId;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser1.EmployeeNumber = '1234567890'+i;
        Database.insert(testUser1);
        return testUser1;
    }
    
         /*
     * @Description : Test data for creating test Account
     * @ Args       : Null
     * @ Return     : void
     */
    public Account buildAccount()
    {   
              
        testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = collId;
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount;
        acctId = testAccount.ID;    
        return testAccount;
    }
    
    public Account buildAccount1()
    {   
              
        testAccount1 = new Account();
        testAccount1.Name = 'TestAccountName1';
        testAccount1.BillingCity = 'TestCity1';
        testAccount1.BillingCountry = 'TestCountry1';
        testAccount1.BillingStreet = 'Test Street1';
        testAccount1.Relationship_Manager__c = collId1;
        testAccount1.One_Code__c='ABC127XYZ';
        insert testAccount1;
        acctId1 = testAccount1.ID;    
        return testAccount1;
    }
    
     /*
     * @Description : Test data for creating test Opportunity
     * @ Args       : Null
     * @ Return     : void
     */
    public Opportunity buildOpportunity()
    {
        testOpportunity.Name = 'test oppty';
        testOpportunity.Type = 'New Client';
        testOpportunity.AccountId =  acctId; 
        //request id:12638 commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity.CloseDate = date.newInstance(year, month, 1);
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        //Test Class Fix
        //testOpportunity.Close_Stage_Reason__c ='Other';
       
        insert testOpportunity;
        OpptyId = testOpportunity.ID;        
        return testOpportunity;
    }
    
     public Opportunity buildOpportunity1()
    {
        testOpportunity1.Name = 'test oppty';
        testOpportunity1.Type = 'New Client';
        testOpportunity1.AccountId =  acctId1; 
        //request id:12638 commenting step
        //testOpportunity1.Step__c = 'Identified Deal';
        //testOpportunity.Step__c = 'Pending Chargeable Code';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity1.StageName = 'Identify';
        //testOpportunity.CloseDate = date.parse('1/1/2015'); 
        testOpportunity1.CloseDate = date.newInstance(year, month, 1);
        testOpportunity1.CurrencyIsoCode = 'ALL';        
        testOpportunity1.Opportunity_Office__c = 'Urbandale - Meredith';
        testOpportunity1.Opportunity_Country__c = 'CANADA';
        testOpportunity1.currencyisocode = 'ALL';
        testOpportunity1.Close_Stage_Reason__c ='Other';
       
        insert testOpportunity1;
        OpptyId1 = testOpportunity1.ID;        
        return testOpportunity1;
    }
    
    
     /*
     * @Description : Test data for creating test Product
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildProduct()
    {
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        insert testProduct;
        }
        
    
     /*
     * @Description : Test data for creating test Product
     * @ Args       : Null
     * @ Return     : void
     */
       public Product2 buildScopeProduct(String productLOB)
    {
        testScopeProduct.Name = 'TestPro';
        testScopeProduct.Family = 'RRF';
        testScopeProduct.IsActive = True;
        testScopeProduct.Classification__c = 'Non-Consulting Solution Area';
        testScopeProduct.LOB__c = productLOB;
        insert testScopeProduct;
        scproid = testScopeProduct.id;
        return testScopeProduct;
    }
     /*
     * @Description : Test data for creating test OpportunityLineItem
     * @ Args       : Null
     * @ Return     : void
     */
    public static OpportunityLineItem createOpptylineItem(String opptId, String pbentryId)   
    {
        opptylineItem.OpportunityId = opptId;
        opptylineItem.PricebookEntryId = pbentryId;
        //Updated start date and end dates by Jagan  
        /*opptyLineItem.Revenue_Start_Date__c = date.parse('1/1/2015');
        opptyLineItem.Revenue_End_Date__c = date.parse('1/10/2015'); */
        //Updated start date and end dates by Jyotsna
        opptyLineItem.Revenue_Start_Date__c = date.newInstance(year, month, 1);
        opptyLineItem.Revenue_End_Date__c = date.newInstance(year, month, 10);
        opptylineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1; 
        //opptyLineItem.Year2Revenue_edit__c = 1;    
        Database.insert(opptylineItem);
        opptyLineItemId = opptylineItem.Id;
        return  opptylineItem;
    } 
       
     /*
     * @Description : Test data for creating test OpportunityLineItem
     * @ Args       : Null
     * @ Return     : void
     */   
    public List<OpportunityTeamMember> buildOpportunityTeamMember()
    {
        User opportunityMember = new User();
        opportunityMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName1', opportunityMember);      
        
        testOpportunityTeamMember = new OpportunityTeamMember();
        testOpportunityTeamMember.OpportunityId = OpptyId;
        testOpportunityTeamMember.UserId = opportunityMember.ID;
        System.debug('\n Opportunity team member for insert :'+testOpportunityTeamMember);
        insert testOpportunityTeamMember;
        testOpportunityTeamMemberList.add(testOpportunityTeamMember);
        
        oppId = testOpportunityTeamMember.OpportunityId;
       
        return testOpportunityTeamMemberList;
    }
    
   /*
     * @Description : Test data for creating test Sales credit
     * @ Args       : Null
     * @ Return     : void
     */
    public List<Sales_Credit__c> buildSalesCredit()
    {
        testSalesCredit.Opportunity__c = oppId;
        testSalesCredit.EmpName__c = collId;
        testSalesCredit.Percent_Allocation__c = 0;
        testSalesCredit.CurrencyIsoCode = 'USD';
        testSalesCredit.Role__c = null;
        insert testSalesCredit;
        testSalesCreditList.add(testSalesCredit);
        
        return testSalesCreditList;
    }
    
       /*
     * @Description : Test data for creating test Sales credit
     * @ Args       : Null
     * @ Return     : void
     */
    public List<Sales_Credit__c> buildSalesCredit1(ID opptId)
    {
        testSalesCredit1.Opportunity__c = opptId;
        testSalesCredit1.EmpName__c = collId;
        testSalesCredit1.Percent_Allocation__c = 0;
        testSalesCredit1.CurrencyIsoCode = 'USD';
        testSalesCredit1.Role__c = null;
        insert testSalesCredit1;
        testSalesCreditList1.add(testSalesCredit1);
        
        return testSalesCreditList1;
    }
    
     /*
     * @Description : Test data for creating test Contact
     * @ Args       : Null
     * @ Return     : void
     */
    public Contact buildContact()
    {
        
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'test123@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId =  acctId;
        insert testContact;
        contId = testContact.Id;
        
        return testContact;
    } 
    
     /*
     * @Description : Test data for creating test Contact Team Member
     * @ Args       : Null
     * @ Return     : void
     */
    public Contact_Team_Member__c buildContactTeamMember()
    {
        User contactMember = new User();
        contactMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName2', contactMember);      
        
        testContactTeamMember = new Contact_Team_Member__c();
        testContactTeamMember.Contact__c = contId;
        testContactTeamMember.ContactTeamMember__c = contactMember.ID;
        insert testContactTeamMember;
        conTMember = testContactTeamMember.ContactTeamMember__c;
        
        return testContactTeamMember;
    }
    
     /*
     * @Description : Test data for creating test Contact Share
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildContactShare()
    {
         testContactShare.ContactId = contId;                    
         testContactShare.UserOrGroupId = conTMember;                   
         testContactShare.ContactAccessLevel = 'Edit';
         insert  testContactShare;
    }
    
     /*
     * @Description : Test data for creating test Colleague
     * @ Args       : Null
     * @ Return     : void
     */
    public Colleague__c buildColleague()
    {
        buildMercerOffice();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.Level_1_Descr__c = 'Mercer';
        testColleague.Location__c = mogId;
        insert testColleague;
        collId = testcolleague.Id;
      
        return testColleague;        
    }
    
        public Colleague__c buildColleague1()
    {
        buildMercerOffice1();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678910';
        testColleague1.LOB__c = '111112';
        testColleague1.Last_Name__c = 'TestLastNam1e';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc1@abc.com';
        testColleague1.Level_1_Descr__c = 'Mercer';
        testColleague1.Location__c = mogId1;
        insert testColleague1;
        collId1 = testcolleague1.Id;
      
        return testColleague1;        
    }
    
     /*
     * @Description : Test data for creating test Colleague Search
     * @ Args       : Null
     * @ Return     : void
     */
    public String buildColleaguesearch ()
    {
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        return testColleague.Id;     
    }
    
     /*
     * @Description : Test data for fetching mercer Standard User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    public static String getmercerStandardUserProfile()
    {
        return [Select Id, Name FROM Profile where Name = 'Mercer Standard' LIMIT 1].Id;
    }
    
    public static String getmercerSystemAdminUserProfile()
    {
        return [Select Id, Name FROM Profile where Name = 'Mercer System Admin Lite' LIMIT 1].Id;
    }
    
    /*
     * @Description : Test data for fetching Marketo Sync User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    public static String getMarketoSyncUserProfile()
    {
        return [Select Id, Name FROM Profile where Name = 'Marketo Sync' LIMIT 1].Id;
    }
    
       /*
     * @Description : Test data for fetching System Admin User Profile
     * @ Args       : Null
     * @ Return     : void
     */
    public static String getsystemAdminUserProfile()
    {
        return [Select Id, Name FROM Profile where Name = 'System Administrator' LIMIT 1].Id;
    }
    
    /*
     * @Description : Test data for creating test Requester
     * @ Args       : Null
     * @ Return     : void
     */
        public void createRequester()
    {
        requester = createUser(mercerStandardUserProfile, 'testUser', 'testLastName3', requester);    
    }
      
    
     /*
     * @Description : Test data for creating test Scopeit project
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildScopeProject()
     {
        
    }
    
     /*
     * @Description : Test data for creating test Account Team Member
     * @ Args       : Null
     * @ Return     : void
     */
    public void buildAccountTeamMember()
    {
        User accountMember = new User();
        accountMember = createUser(mercerStandardUserProfile, 'tUser', 'tLastName4', accountMember);
        //opportunityMember = createUser(mercerStandardUserProfile, 'testUser', 'testLastName', opportunityMember);      
        
        testAccountTeamMember = new AccountTeamMember();
        //testOpportunityTeamMember = new OpportunityTeamMember();
        testAccountTeamMember.AccountId = acctId;
        //testOpportunityTeamMember.OpportunityId = opptyId;
         testAccountTeamMember.UserId = accountMember.Id;
        insert testAccountTeamMember;
        delete testAccountTeamMember;
        
 
    }
     /*
     * @Description : Test data for creating test Intermediate Account
     * @ Args       : Null
     * @ Return     : void
     */ 
    public static Account buildIntermediateAccount(String globalId)
    {
        Account intermediateAccount = new Account();
        intermediateAccount.Name = 'Parent Account';    
        intermediateAccount.BillingCity = 'TestCity';
        intermediateAccount.BillingCountry = 'TestCountry';
        intermediateAccount.BillingStreet = 'Test Street';
        intermediateAccount.Relationship_Manager__c = collId;
        intermediateAccount.GBL_ID__c = globalId;
        intermediateAccount.INT_ID__c = 'Test787';
        return intermediateAccount;
    }
    
     /*
     * @Description : Test data for creating test Child Account
     * @ Args       : Null
     * @ Return     : void
     */
    public static Account buildChildAccount(String globalId, String intId)
    {
        Account childAccount = new Account();
        childAccount.Name = 'Parent Account';   
        childAccount.BillingCity = 'TestCity';
        childAccount.BillingCountry = 'TestCountry';
        childAccount.BillingStreet = 'Test Street';
        childAccount.Relationship_Manager__c = collId;
        childAccount.GBL_ID__c = globalId;
        childAccount.INT_ID__c = intId;
        childAccount.GOC_ID__c = 'Test788';
        return childAccount;
        
    }
     /*
     * @Description : Test data for creating test Mercer Office data
     * @ Args       : Null
     * @ Return     : void
     */
    public static void buildMercerOffice()
    
    {
            Mercer_Office_Geo_D__c  mogd = new Mercer_Office_Geo_D__c ();

            mogd.Office__c = 'New Delhi';
            mogd.Region__c = 'EuroPac';
            mogd.Sub_Market__c = 'India';
            mogd.Sub_Region__c = 'Europe';
            mogd.Country__c = 'Japan';
            mogd.Market__c = 'Ireland';
            mogd.Status__c = 'A';
            insert mogd ;
            mogId = mogd.Id;
    }
    
     public static void buildMercerOffice1()
    
    {
            Mercer_Office_Geo_D__c  mogd1 = new Mercer_Office_Geo_D__c ();

            mogd1.Office__c = 'Bangalore';
            mogd1.Region__c = 'EuroPac';
            mogd1.Sub_Market__c = 'India';
            mogd1.Sub_Region__c = 'Europe';
            mogd1.Country__c = 'Japan';
            mogd1.Market__c = 'Ireland';
            mogd1.Status__c = 'A';
            insert mogd1 ;
            mogId1 = mogd1.Id;
    }
    
    public Growth_Plan__c buildGrowthPlan(String planningYear, ID accid)
    {
        //testGrowthPlan.name = 'test gplan';
        testGrowthPlan.Account__c = accid;
        testGrowthPlan.Planning_Year__c = planningYear;
        testGrowthPlan.Status__c = 'In Progress';
        insert testGrowthPlan;
        
        return testGrowthPlan;
    }
  
       /*
     * @Description : Test data to convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
     * @ Args       : Null
     * @ Return     : void
     */
    public static string quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
        {
            newSetStr += '\'' + str + '\',';
        }
            
        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }
    public Client_Engagement_Measurement_Interviews__c buildCEM(){
      Client_Engagement_Measurement_Interviews__c  cem = new Client_Engagement_Measurement_Interviews__c ();
      cem.Date_of_Review__c = system.today();
      return cem;
    
    }
    
    public Revenue_Connectivity__c buildProject(){
        
        testProject.CEP_Code__c = 'WEB002-CEP-CODE';
        testProject.Charge_Basis__c = '100';
        testProject.Project_Amount__c = 20000.00;
        testProject.Colleague__c = collId;
        testProject.Opportunity__c = OpptyId;
        testProject.Opportunity_Product_Id__c = opptyLineItemId;
        testProject.Project_End_Date__c = date.newInstance(year, month, 1);
        testProject.Project_LOB__c = 'Benefits Admin';
        testProject.Project_Manager__c = '866608';
        //testProject.Project_Name__c = 'Absence Management - PROJ01';
        testProject.Project_Solution__c = '3051';
        testProject.Project_Start_Date__c = date.newInstance(year, month, 10);
        testProject.Project_Status__c = 'Approved';
        testProject.Project_Type__c = 'Project';
        testProject.WebCAS_Project_ID__c = 'WEB001';
        insert testProject;
        return testProject;
    }
   
 
   public Activity_Archive__c buildActArchive(){
    
        testActArchive.Account_ID__c = '001E000000Z9uXOIAZ';
        testActArchive.Activity_ID__c = '00TE0000016LsBRMA0';
        testActArchive.Activity_Owner__c = '005E0000002ABz8';
        testActArchive.mh_Additional_Notes_Exist__c = FALSE;
        testActArchive.mh_Associated_Green_Sheet__c = FALSE;
        testActArchive.Description__c = 'From: Mercer marketing.reply@mercer.com. Click https://mercer.my.salesforce.com/00XE0000000e7u2MAA for email details';
        testActArchive.Completion_Date__c = '8/21/2014';
        testActArchive.CreatedByUser__c = '005E0000002ABz8';
        testActArchive.IsArchived__c = FALSE;
        testActArchive.IsClosed__c = TRUE;
        testActArchive.IsDeleted__c = FALSE;
        testActArchive.IsHighPriority__c = FALSE;
        testActArchive.IsRecurrence__c = FALSE;
        testActArchive.isRelatedToAccount__c = 'NO';
        testActArchive.IsReminderSet__c = FALSE;
        testActArchive.Last_Modified_By_User__c = '005E0000002ABz8';
        testActArchive.mh_Managers_Notes_Exist__c = FALSE;
        testActArchive.MH_Action__c = FALSE;
        testActArchive.Notify_User__c = FALSE;
        testActArchive.mh_Post_Call_Assessment_Exists__c = FALSE;
        testActArchive.Priority__c = 'Normal';
        testActArchive.ROW_Flag__c = FALSE;
        testActArchive.Status__c = 'Completed';
        testActArchive.Subject__c = 'Was Sent Email: Making_Accountability_Work HB_Health Care Reform Website Promo US_HCR Blog_Solicitation for signup_Aug20.HCR ';
        testActArchive.WhatCount__c = 0;
        testActArchive.Whocount__c = 1;
        testActArchive.WhoId__c = '003E000000aNWu4IAG';
        insert testActArchive;
        return testActArchive;
   }
}