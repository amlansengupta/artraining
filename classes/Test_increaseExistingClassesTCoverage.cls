/* This  class works as Test class for increasing Test Coverage of existing Classes with Zero %
 =================================================================================================
Created by : Ravikiran Nadella Deployed as a Part of March Realease.
 =================================================================================================
*/
@isTest
Private class Test_increaseExistingClassesTCoverage{
  private static Mercer_TestData mtdata = new Mercer_TestData();
  
    /*
     * @Description : Test method for increasing Test Coverage of existing Classes with Zero %
     * @ Args       : Null
     * @ Return     : void
     */
   static testMethod void unitTest(){
    
    User user1 = [Select id, name from User where name = 'MercerForce'limit 1];
    system.runas(user1){
    Colleague__c testColleague = mtdata.buildColleague();
    Account testAccount = mtdata.buildAccount();
    String sch = '0 0 23 * * ?';
     list<Account> aList = new list<Account>();
     aList=[select id from account limit 1 ] ;
      
    test.startTest();
    if(aList.size()> 0)
    try{
     throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
     }catch (TriggerException tEx){
     
     system.debug('Exception' +tEx.getMessage());
     }
    AP41_RMOwnershipSchedulable bc = new AP41_RMOwnershipSchedulable();
    system.schedule('RMOwnership', sch, bc);
   /* Map<String, User>empIdUserMapFromSchedulable= new Map<String, User>();
    AP42_RMOwnershipBatchable  batch = new AP42_RMOwnershipBatchable(empIdUserMapFromSchedulable);
     Id batchid = database.executeBatch(batch);*/
     MS01_AccountMDriveStatusSchedulable bc1 = new MS01_AccountMDriveStatusSchedulable();
     system.schedule('AccountMDriveStatus', sch, bc1);
     MS07_OpportunityRevenueSchedulable bc2= new MS07_OpportunityRevenueSchedulable();
     system.schedule('OpportunityRevenue', sch, bc2);
     MS20_NoActivityObsoleteSchedulable bccls = new MS20_NoActivityObsoleteSchedulable();
     System.schedule('obsoleteSchedulable',sch,bccls );
    test.stopTest();
   }
   
   
   }
}