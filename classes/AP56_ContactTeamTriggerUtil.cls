/*
Purpose: This class is being used by the contact team trigger, this is created to have contact team functionality similar to account/oppty team
         Whenever a contact is created the contact owner is added to the contact team with read/write access
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR       DATE          DETAIL    
                            1.0 -    Shashank     03/29/2013    Created Utility Class for TRG_05ContactTeam Trigger
                            2.0 -    Arijit       05/19/2013    Added logic for Duplicate Contact Team Member check and bypassed logic execution
                                                                for Contact AccountOwner  
                            3.0 -    Gyan         18/12/2013    Updated As per Req-3584,To populate ConcatenatedContactTeam Field 
                            4.0 -    Jagan        3/26/2014     Corrected logic related to contact share for contact team load
                            5.0 -    Sarbpreet    7/4/2014     Updated Comments/documentation.
                            6.0 -    Dhanusha     2/8/2017    Updated As per Req-10977,To populate ConcatenatedContactTeamID Field on contact 
                           7.0 -    Dhanusha     2/15/2017     As per Req-10977,Added logic to restrict contact team member deletion. 
                                                                               Removed logic which prevent users in adding contact team members
                                                 
=============================================================================================================================================== 
*/
public class AP56_ContactTeamTriggerUtil 
{
    // Initailize contact share list
    private static List<ContactShare> contactShareList = new List<ContactShare>();
    private static List<ContactShare> delContactShareList = new List<ContactShare>();
    
    private static Map<Id,set<id>> contactShareInfo = new Map<Id, Set<id>>();
    private static Map<String,Contact> contactInfo = new Map<String,Contact>();
    private static Map<ID,User> usrInfo = new Map<ID,User>();
    private static Set<ID> delcontID = new Set<ID>();
    private static Set<ID> delTeamMembers = new Set<ID>();
    private static List<Contact_Team_Member__c> lstContTeamToBeFutureUpdate = new List<Contact_Team_Member__c>();
    private static final String SYS_ADMIN = 'System Administrator';
    private static final String SYS_ADMIN_LITE = 'Mercer System Admin Lite';
    
     
    /*
     * @Description : This method is called on before insertion of contact team records 
     * @ Args       : List<Contact_Team_Member__c> triggernew
     * @ Return     : void
     */
    public static void processContactTeambeforeInsert(List<Contact_Team_Member__c> triggernew)
    {
        
        
        Set<Id> conIds = new Set<Id>();
        Map<Id, List<Contact_Team_Member__c>> contactTeamMemberMap = new Map<Id, List<Contact_Team_Member__c>>();
        //Map<Id, Contact_Team_Member__c> contactTeamMemberMap = new Map<Id, Contact_Team_Member__c>();
        Map<String, Contact> contactIdAccountOwnerIdMap = new Map<String, Contact>();
        Set<ID> newMemberIDs = new Set<ID>();
        //Set<id> triggerNewID = new Set<id>();//used for debugging. delete after use.
        //List<Contact_Team_Member__c> relatedCTMtoContact = new List<Contact_Team_Member__c>();
        Profile prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
        
        // clear cache (to avoid re-insertion of same IDs).
        contactShareList.clear();
        contactShareInfo.clear();
        contactInfo.clear();
        usrInfo.clear();
        conIds.clear();
        contactTeamMemberMap.clear();
        newMemberIDs.clear();
        try
        {
            System.debug('printing trigger.new____'+Triggernew);
            for(Contact_Team_Member__c rec :  Triggernew)
            {
                System.debug('printing rec '+rec);
                system.debug('inside loop');
                conIds.add(rec.Contact__c);
                //triggerNewID.add(rec.id);
                newMemberIDs.add(rec.ContactTeamMember__c);
            }
            System.debug('conIds: '+conIds);
            System.debug('newMemberIDs: '+newMemberIDs);
            if(!conIds.isEmpty())
            {
                //Added the below three line for debugging. delete after use
                List<Contact> cons = new List<Contact>();
                cons = [Select Id, Name, OwnerId, Account.OwnerId,(Select Id, Contact__c, ContactTeamMember__c FROM Contact_Team_Members__r) FROM Contact WHERE Id IN :conIds];
                System.debug('the value of cons is:+++ '+cons);
                //delete till this point
                for(Contact contact : [Select Id, Name, OwnerId, Account.OwnerId,(Select Id, Contact__c, ContactTeamMember__c FROM Contact_Team_Members__r) FROM Contact WHERE Id IN :conIds])
                {
                    System.debug('Contact name being created:____ '+contact.name+' contact id is '+contact.id);
                    contactTeamMemberMap.put(contact.Id, contact.Contact_Team_Members__r);
                    contactIdAccountOwnerIdMap.put(contact.Id, contact);  
                }

            }
            System.debug('contactTeamMemberMap###: '+contactTeamMemberMap);
            if(!newMemberIDs.isEmpty())
            {
                for(User usr : [Select EmployeeNumber,ID,First_Name__c,Last_Name__c,Country__c,LOB__c,Employee_Office__c,Empl_Status__c,People_Directory__c,Email_Address__c,UserRole.Name from User where Id IN : newMemberIDs])
                {
                    usrInfo.put(usr.ID,usr);
                }
            }
            
            for(Contact con : [Select ID,ownerID from Contact where ID IN : conIds])
            {
                contactInfo.put(con.ownerID,con);
            }
            
            for(ContactShare cShare : [Select ContactId,UserOrGroupId from ContactShare where ContactId IN : conIds])
            {
                if(!contactShareInfo.containsKey(cShare.UserOrGroupId))
                    contactShareInfo.put(cShare.UserOrGroupId,new Set<id>{cShare.contactId});
                else{
                    Set<id> tempSet = contactShareInfo.get(cShare.UserOrGroupId);
                    tempSet.add(cShare.contactId);
                    contactShareInfo.put(cShare.UserOrGroupId,tempSet);
                }
            }
            
            for(Contact_Team_Member__c rec: triggernew)
            {
                boolean hasError = false;
                boolean notAMember = true;
                rec.First_Name__c = usrInfo.get(rec.ContactTeamMember__c).First_Name__c;                 
                rec.Last_Name__c = usrInfo.get(rec.ContactTeamMember__c).Last_Name__c;
                rec.User_ID__c = usrInfo.get(rec.ContactTeamMember__c).ID;
                rec.Role__c =  usrInfo.get(rec.ContactTeamMember__c).UserRole.Name;
                //Check if the inserting user is System admin or Admin lite or Contact or Account owner or contact team member
                if (!prof.Name.equalsIgnoreCase(SYS_ADMIN) &&!prof.Name.equalsIgnoreCase(SYS_ADMIN_LITE) && UserInfo.getUserId() <> contactIdAccountOwnerIdMap.get(rec.Contact__c).Account.OwnerId  && UserInfo.getUserId() <> contactIdAccountOwnerIdMap.get(rec.Contact__c).OwnerId)
                 { 
                 if((Userinfo.getuserID() == rec.ContactTeamMember__c) && (!contactInfo.containsKey(Userinfo.getuserID())))
                 {
                  hasError = true; 
                  rec.addError ('You cannot add yourself as member in this list because of insufficient access levels');  
                  } 
                  if(!hasError)
                   {
                   if(contactTeamMemberMap.get(rec.Contact__c).size()>0)
                    {
                        for(Contact_Team_Member__c contactTeam : contactTeamMemberMap.get(rec.Contact__c))
                        { 
                                                  
                            //Check if the member to be added already exists in the Team.
                            if(rec.ContactTeamMember__c  == contactTeam.ContactTeamMember__c)
                            {
                                hasError = true;
                                System.debug('this member is already existing in the team');
                                rec.addError('This member already exists in the Team');
                                
                            }
                            
                            if(contactTeam.ContactTeamMember__c == UserInfo.getUserId() && !hasError)
                            {
                                
                                notAMember = false;
                                System.debug('the value of notaMember here is: '+notAMember);
                                
                            }                                            
                        }
                        }
                        else{
                            hasError = true;
                        }   
                    }
                    // removed the below  if condition per 10977 - Contact team members should be created/edited by all the nonlite users
                  /*  if(notAMember && !hasError)
                    {
                        System.debug('I have added access level error here!!!_____');
                        rec.addError('You dont have required level of access to perform the operation');
                    }
                    
                    else
                    { */
                        if(rec.ContactTeamMember__c <> contactIdAccountOwnerIdMap.get(rec.Contact__c).OwnerId)
                        {
                            ContactShare share=new ContactShare ();        
                            share.UserOrGroupId=rec.ContactTeamMember__c;
                            share.ContactId=rec.Contact__c;
                            share.ContactAccessLevel='Edit';
                            contactShareList.add(share); 
                            System.debug('there was no access level error added here. value f contactsharelist is '+contactShareList);
                        }   
                  //  }
                        
                }
                else
                {
                    System.debug('I AM IN ELSE CONDITION_______');
                    for(Contact_Team_Member__c contactTeam : contactTeamMemberMap.get(rec.Contact__c))
                    {
                       
                        //Check if the member to be added already exists in the Team.
                        if(rec.ContactTeamMember__c  == contactTeam.ContactTeamMember__c){rec.addError('This member already exists in the Team');}                       
                        
                    }      
                   System.debug('trying to enter final if!');
                   //Fetch ContactShare records corresponding to conIds.
                    if(!contactShareInfo.containsKey(rec.ContactTeamMember__c) || (contactShareInfo.containsKey(rec.ContactTeamMember__c) && !contactShareInfo.get(rec.ContactTeamMember__c).contains(rec.Contact__c)  ) )
                    {
                        System.debug('entered first!____');
                      if(rec.ContactTeamMember__c <> contactIdAccountOwnerIdMap.get(rec.Contact__c).OwnerId)
                        {    
                            System.debug('adding values to contactsharelist!!!!');
                            ContactShare share=new ContactShare ();        
                            share.UserOrGroupId=rec.ContactTeamMember__c;
                            share.ContactId=rec.Contact__c;
                            share.ContactAccessLevel='Edit';
                            contactShareList.add(share); 
                        }  
                    }
                } 
            }
            
            System.debug('FINALLY PRINTING contactShareList!'+contactShareList);
        if(!contactShareList.isEmpty())
            insert contactShareList;    // insert the list
        }
        catch(TriggerException tEx)
        {
            System.debug('Exception occured at ContactTeamMember Trigger BeforeInsert with reason :'+tEx.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
        
        }
        catch(Exception ex)
        {
            System.debug('Exception occured at ContactTeamMember Trigger BeforeInsert with reason :'+ex.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
        
        }
    }
    /*
     * @Description : This method is called on after deletion of contact team records 
     * @ Args       : List<Contact_Team_Member__c> triggerold
     * @ Return     : void
     */
    public static void processContactTeambeforeDelete(List<Contact_Team_Member__c> triggerold)
    {
    
        Set<Id> contIds = new Set<Id>();
        Map<Id, List<Contact_Team_Member__c>> contTeamMemberMap = new Map<Id, List<Contact_Team_Member__c>>();
        Map<String, Contact> contactandAccountOwnerIdMap = new Map<String, Contact>();
        Set<ID> oldMemberIDs = new Set<ID>();
        
        Profile prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
        
       
        // clear cache (to avoid re-insertion of same IDs).
        contactShareList.clear();
        contactShareInfo.clear();
        contactInfo.clear();
        usrInfo.clear();
        contIds .clear();
        contTeamMemberMap.clear();
        oldMemberIDs.clear();
        
        try
        {
           
         for(Contact_Team_Member__c rec :  triggerold)
            {
                
                contIds.add(rec.Contact__c);
                oldMemberIDs.add(rec.ContactTeamMember__c);
            }
            System.debug('conIds: '+contIds);
            System.debug('oldMemberIDs : '+oldMemberIDs);
            if(!contIds.isEmpty())
            {
                
                for(Contact contact : [Select Id, Name, OwnerId, Account.OwnerId,(Select Id, Contact__c, ContactTeamMember__c FROM Contact_Team_Members__r) FROM Contact WHERE Id IN :contIds])
                {
                    System.debug('Contact name being created:____ '+contact.name+' contact id is '+contact.id);
                    contTeamMemberMap.put(contact.Id, contact.Contact_Team_Members__r);
                    contactandAccountOwnerIdMap.put(contact.Id, contact);  
                }

            }
            System.debug('contactTeamMemberMap###: '+contTeamMemberMap);
            if(!oldMemberIDs.isEmpty())
            {
                for(User usr : [Select EmployeeNumber,ID,First_Name__c,Last_Name__c,Country__c,LOB__c,Employee_Office__c,Empl_Status__c,People_Directory__c,Email_Address__c,UserRole.Name from User where Id IN : oldMemberIDs])
                {
                    usrInfo.put(usr.ID,usr);
                }
            }
            
            for(Contact con : [Select ID,ownerID from Contact where ID IN : contIds])
            {
                contactInfo.put(con.ownerID,con);
            }
          // 10977 : Added the below if else loop to restrict users from deleting the contact members
            for(Contact_Team_Member__c rec : triggerold)
            {                
                if (!prof.Name.equalsIgnoreCase(SYS_ADMIN) &&!prof.Name.equalsIgnoreCase(SYS_ADMIN_LITE) && UserInfo.getUserId() <> contactandAccountOwnerIdMap.get(rec.Contact__c).Account.OwnerId  && UserInfo.getUserId() <> contactandAccountOwnerIdMap.get(rec.Contact__c).OwnerId)
               {
                rec.addError('You do not have access to delete the team Members of this contact');
                }
                else
                {
                
                delcontID.add(rec.Contact__c);
                delTeamMembers.add(rec.ContactTeamMember__c);
                }          
          
            }   
            
            //Fetch ContactShare records corresponding to deleted Contact team member.
            for(ContactShare  cShare : [Select ContactId,UserOrGroupId,ContactAccessLevel from ContactShare where ContactId IN : delcontID and UserOrGroupId IN : delTeamMembers])
            {
                //Delete the corresponding record from ContactShare.
                delContactShareList.add(cShare);      
            }
            
            
            if(!delContactShareList.isEmpty())
            {
                try
                {
                    delete delContactShareList;
                }
                catch(Exception e)
                {
                    if( e.getMessage().contains('cannot delete owner or rule share rows'))
                    {
                        for(Contact_Team_Member__c rec : triggerold)
                        {
                            rec.addError('You cannot delete the current Contact Owner from the Contact Team'); 
                        }

                    }

                }
            }
        }
        catch(TriggerException tEx)
        {
            System.debug('Exception occured at ContactTeamMember Trigger BeforeDelete with reason :'+tEx.getMessage());
            triggerold[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
        
        }
        catch(Exception ex)
        {
            System.debug('Exception occured at ContactTeamMember Trigger BeforeDelete with reason :'+ex.getMessage());
            triggerold[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
        
        }
    }
    
    /*
   * @Description : This method is called on after insertion, after updation and after deletion of an Contact Team record
                    and concatenates all the contact Team associated with an Contact and stores them in a field                 
   * @ Args       : List<Contact_Team_Member__c>   
   * @ Return     : void
   */
   public static void concatenatedContactTeam(List<Contact_Team_Member__c> ctmList)
   {
          Set<Id> ConTeamMemberIdSet = new Set<Id>();
          // Iterate through Contact Team Members and fetch the Contact IDs and add them in a set.
           try { 
          for(Contact_Team_Member__c ctm : ctmList)
          {
              ConTeamMemberIdSet.add(ctm.Contact__c);   
          }
         
          if(AP19_ContactTriggerUtil.isMergeProcess == True){ futureUpdateContactTeam(ConTeamMemberIdSet);}
          else
          {
                updateConcatenatedTeam(ConTeamMemberIdSet);
          }
      }  catch(TriggerException tEx){ctmList[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.');}
        catch(Exception ex){ctmList[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); }
 
          
   }
   
 
   @future
    /*
     * @Description : Future Method for updating contact team
     * @ Args       : null
     * @ Return     : void
     */
   public static void futureUpdateContactTeam(Set<ID> ConTeamMemberIdSet){
   updateConcatenatedTeam(ConTeamMemberIdSet); }
    /*
     * @Description : This method updates the concatenated contact team field
     * @ Args       : Set<ID> ConTeamMemberIdSet
     * @ Return     : void
     */
   public static void updateConcatenatedTeam(Set<ID> ConTeamMemberIdSet) 
   {
          //Map vriable for storing ID and Name Contact
          List<Contact> lstConUpdate = new List<Contact>() ;
          Map<Id,Contact> conMap =new Map<ID,Contact>([Select c.Concatenated_Contact_Team__c, (Select ContactTeamMember__c From Contact_Team_Members__r) From Contact c where ID IN : ConTeamMemberIdSet]);
          List<Id> contactTeamMemberIds = new List<Id>();
            if(conMap != null){
                for(Contact contactObj : conMap.values()){
                    if(contactObj.Contact_Team_Members__r != null){
                        for(Contact_Team_Member__c contactTeamMem : contactObj.Contact_Team_Members__r){
                            contactTeamMemberIds.add(contactTeamMem.ContactTeamMember__c);
                        }
                    }
                }
            }
       		Map<ID,User> usrInfoMap = new Map<ID,User>([Select Id,Name,UserName from User where Id IN :contactTeamMemberIds]); 
            
           try {  
          // Iterate through Contact map and fetch the Contact IDs
          for(Id cid:conMap.keyset())
          {
             Contact con =  conMap.get(cid);
             
             if(con.Contact_Team_Members__r!=null && con.Contact_Team_Members__r.size()>0) 
              {
                     con.Concatenated_Contact_Team__c = '';   
                     con.Concatenated_Contact_Team_ID__c = '';  
                     // Iterate through Contact Team Members associated with the Contact and concatenate Contact Team
                     for(Contact_Team_Member__c ctm : con.Contact_Team_Members__r)
                     {    
                        String strConTeamMembrname = usrInfoMap.get(ctm.ContactTeamMember__c).Name;
                        String strConTeamMembruname = usrInfoMap.get(ctm.ContactTeamMember__c).UserName;
                         // Check if newly added Contact Team Member already exist in list or Not?
                         if(strConTeamMembrname <>null && !con.Concatenated_Contact_Team__c .contains(strConTeamMembrname)) 
                          {
                            //Logic to Not add ; in last of String if it contains only one value.
                           if(con.Concatenated_Contact_Team__c.length()!= 0)
                            con.Concatenated_Contact_Team__c += ';'+strConTeamMembrname;
                           else
                              con.Concatenated_Contact_Team__c += strConTeamMembrname;
                          }
                          lstContTeamToBeFutureUpdate.add(ctm);
                          //10977 - Update the Contact team Member ID field on contact
                          if(strConTeamMembruname <>null && !con.Concatenated_Contact_Team_ID__c .contains(strConTeamMembruname )) 
                          {
                            //Logic to Not add ; in last of String if it contains only one value.
                           if(con.Concatenated_Contact_Team_ID__c .length()!= 0)
                            con.Concatenated_Contact_Team_ID__c += ';'+strConTeamMembruname;
                           else
                              con.Concatenated_Contact_Team_ID__c += strConTeamMembruname;
                          }
                          lstContTeamToBeFutureUpdate.add(ctm);  //end of 10977
                     }
                     
                     
                     
                    
                 }
                 lstConUpdate.add(con);      
        }

        if(!lstConUpdate.isEmpty())
        Database.update(lstConUpdate) ;
   }  catch(TriggerException tEx){lstConUpdate[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); }
        catch(Exception ex){lstConUpdate[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); }

   }
}