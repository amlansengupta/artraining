/*Purpose: This test class provides data coverage to Account's related functionality.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit 12/18/2012  Created test class
============================================================================================================================================== 
*/
@isTest(seeAllData = false)
private class Test_AccountTrigger  {
    
    private static Mercer_TestData mtdata = new Mercer_TestData();
    public static User user1,user2, user3 = new User();
    /*
    * Test data for user creation
    */
    private static User createUser(String profileId, String alias, String lastName, User testUser, integer i)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = String.valueOf(Integer.valueOf('1234567890')+ i);
        insert testUser;
         
        System.runas(testUser){
         
         Switch_Settings__c switchSetting = new Switch_Settings__c();
         switchSetting.SetupOwnerId = testUser.Id;
         switchSetting.IsAccountTriggerActive__c = true;
         insert switchSetting;
         
     /*   AccountTriggerHandler triggerHandler =new AccountTriggerHandler();
        
        TriggerSettings__c TrgSetting1 = new TriggerSettings__c();
        TrgSetting1.name='Account Trigger';
        TrgSetting1.Object_API_Name__c='Account';
        TrgSetting1.Trigger_Disabled__c=false;
        insert TrgSetting1;
        
        triggerHandler.IsDisabled();    */
        }
        
        return testUser;

    }
   
    public static void insertUsers(){
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();
        user1 = createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1, 5);
        user2 = createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2, 6);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);
        //user1.EmployeeNumber = '123';                  
    }

    public static void createDMGroupMember(){
        List<Group> dmGroupList = [Select Name, Id FROM Group Where Name ='Data Migration Group' LIMIT 1]; 
        GroupMember gpm = new GroupMember();
        gpm.UserOrGroupId = user1.id;        
        gpm.GroupId = dmGroupList[0].Id;
        insert gpm;
        
        GroupMember gpm1 = new GroupMember();
        gpm1.UserOrGroupId = user2.id;        
        gpm1.GroupId = dmGroupList[0].Id;
        insert gpm1;
    }
    
    /*
     * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 1)
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myAccountTest1()
    {
        
        //User user1,user2, user3 = new User();
        
        insertUsers();
        
        
        //String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
                
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
       
        Colleague__c  testColleague = mtdata.buildColleague(); 
       
     /*   user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
       // user1.EmployeeNumber = '123';    
       //update user1;
       //system.runAs(user1){
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
                     
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        //Mercer_TestData.testAccount.GU_DUNS__c = '0123';
        testAccount.DUNS__c = '444';
        insert testAccount;
        
        Account testAccount1 = new Account();
        //update Relationship Manager  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '6666';
        testAccount1.DUNS__c = '123';
        testAccount1.HQ_Immediate_Parent_DUNS__c = '444';
        testAccount1.Domestic_DUNS__c = '555';
        testAccount1.GDPR_Opt_Out__c = false;
        testAccount1.One_Code__c = 'ABC123';
        insert testAccount1;
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.Email = 'test@test.com';
        con.AccountId = testAccount1.Id;
        
        insert con;
        testAccount1.GDPR_Opt_Out__c = true;
        update testAccount1;
        Test.startTest();
        Account testAccount2 = [select Id, Name, BillingCity, BillingStreet, OwnerId from Account where Name = 'TestAccount' limit 1];
        system.runAs(User2){
            
            system.debug('*******city***'+testAccount2.BillingCity+'*******Street***'+testAccount2.BillingStreet);
            testAccount2.OwnerId = user1.ID;
            update testAccount2;
        }
        
        
        //industry dummy data
        Industry__c ind = new Industry__c();
        ind.SIC_Code__c = '001';
        ind.Industry_Category__c = 'Agri';
        ind.SIC_Description__c = 'Agriculture';        
        testAccount1.Sic = '57340';
        testAccount1.Primary_SIC_Code__c = '001weff';
        testAccount1.SIC_Code_2__c = '001';
        testAccount1.Industry_Category__c = ind.Industry_Category__c;
        testAccount1.Industry_Category_2__c = 'Agri';
        testAccount1.SIC_Description_2__c = 'sdfj';       
        testAccount1.SicDesc = ind.SIC_Description__c;
        testAccount1.Account_Market__c = testColleague.Market__c;
        testAccount1.Account_Office__c = testColleague.Office__c;
        testAccount1.Account_Region__c = testColleague.Region__c;
        testAccount1.Relationship_Manager__c = testColleague1.Id;
        System.debug('\n Account before update :'+ testAccount1.Id);
        update testAccount1;
        
        
        Test.stopTest();
  }

/*
     * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 2)
     * @ Args       : Null
     * @ Return     : void
     */
 static testMethod void myAccountTest2()
    {
        Test.startTest();
    /*    User user1,user2, user3 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();*/
        //instantiate TestData class
        
        insertUsers();
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        Colleague__c  testColleague = mtdata.buildColleague();
        
      /*  user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
   //     user1.EmployeeNumber = '123';    
       //update user1;
       //system.runAs(user1){
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
                     
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        //testAccount1.GU_DUNS__c = '0123';
        testAccount.DUNS__c = '111';
        insert testAccount;
        
        Account testAccount1 = new Account();
        //update Relationship Manager  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '111';
        testAccount1.DUNS__c = '222';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        testAccount1.HQ_Immediate_Parent_DUNS__c = '222';
        testAccount1.Domestic_DUNS__c = '222';
        insert testAccount1;
        system.runAs(User2){
            testAccount1.OwnerId = user1.ID;
            update testAccount1;
        }
    
        Test.stopTest();    
}
/*
 * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 3)
 * @ Args       : Null
 * @ Return     : void
 */
static testMethod void myAccountTest3()
    {
        Test.startTest();
      //  User user1,user2, user3 = new User();
      //  String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        
        
        insertUsers();
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        Colleague__c testColleague =  mtdata.buildColleague();              

     /*   user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
   //     user1.EmployeeNumber = '123';    
       //update user1;
       //system.runAs(user1){
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
        
                   
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        //testAccount1.GU_DUNS__c = '0123';
        testAccount.DUNS__c = '555';
        insert testAccount;
        
        Account testAccount1 = new Account();
        //update Relationship Manager  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '6666';
        testAccount1.DUNS__c = '123';
        testAccount1.HQ_Immediate_Parent_DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        testAccount1.Domestic_DUNS__c = '555';
        insert testAccount1;
        system.runAs(User2){
            testAccount1.OwnerId = user1.ID;
            update testAccount1;
        }
  
        Test.stopTest();      
}
  /*
   * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 4)
   * @ Args       : Null
   * @ Return     : void
   */
  static testMethod void myAccountTest4()
    {
        Test.startTest();
     //   User user1,user2, user3 = new User();
     //   String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        insertUsers();
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        
        Colleague__c testColleague =  mtdata.buildColleague();
       /* user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
    //    user1.EmployeeNumber = '123';    
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
        
                    
                      
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        testAccount.DUNS__c = '111';
        insert testAccount;
        
        Account testAccount1 = new Account();
        //update Relationship Manager  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '123';
        testAccount1.DUNS__c = '123';
        testAccount1.Domestic_DUNS__c = '124';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        insert testAccount1;
        system.runAs(User2){
            testAccount1.OwnerId = user1.ID;
            update testAccount1;
        }
 
        Test.stopTest();       
}
/*
 * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 5)
 * @ Args       : Null
 * @ Return     : void
 */
static testMethod void myAccountTest5()
    {
        Test.startTest();
      //  User user1,user2, user3 = new User();
      //  String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        insertUsers();
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;
        Colleague__c testColleague = mtdata.buildColleague();  

      /*  user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
   //     user1.EmployeeNumber = '123';    
        
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();
        
                    
                   
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        testAccount.DUNS__c = '111';
        insert testAccount;
        
        Account testAccount1 = new Account();
        //update Relationship Manager  
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '124';
        testAccount1.DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        insert testAccount1;
        system.runAs(User2){
            testAccount1.OwnerId = user1.ID;
            update testAccount1;
        }
        
        Test.stopTest();     
}
  /*
   * @Description : Test method to provide data coverage to Account's related functionality(Scenerio 6)
   * @ Args       : Null
   * @ Return     : void
   */
  Static testMethod void myAccountTest6()
    {
        Test.startTest();
     //   User user1,user2, user3 = new User();
     //   String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        
        insertUsers();
                
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;

        Colleague__c  testColleague = mtdata.buildColleague();     
     /*   user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
  //      user1.EmployeeNumber = '123';    
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();                           
                
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        testAccount.DUNS__c = '111';
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Automobile';
        insert testAccount;
                                                                   
        Account testAccount1 = new Account();          
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '124';
        testAccount1.DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        insert testAccount1;
        system.runAs(User2){
            testAccount1.OwnerId = user1.ID;
            update testAccount1;
        }

        Test.stopTest();
}

 /*
   * @Description : Test method to provide data coverage to Account's Zipcode based Managed Office(Scenerio 7)
   * @ Args       : Null
   * @ Return     : void
   */

    Static testMethod void myAccountTest7()
    {
        
     //   User user1,user2, user3 = new User();
     //   String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]){
            insertUsers();   
            createDMGroupMember();     
        }
                
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;

        Colleague__c  testColleague = mtdata.buildColleague();     
     /*   user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
  //      user1.EmployeeNumber = '123';    
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();                           
                
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        
        US_Office__c usOffice = new US_Office__c();
        usOffice.Name = 'Test US Office';
        usOffice.Zip_Code_Initial__c = '001';
        insert usOffice;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        testAccount.DUNS__c = '111';
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Automobile';
        testAccount.Sic = '444';
        testAccount.Primary_SIC_Code__c = '1111';
        testAccount.SIC_Code_2__c= '2222';
        testAccount.One_Code_Status__c = 'Active';
        testAccount.Account_Country__c = 'United States' ;
        testAccount.BillingCountry = 'United States' ;
        testAccount.Managed_Office__c = null;
        testAccount.ShippingPostalCode = String.valueOf('001034');
        insert testAccount;
                                                                   
        Account testAccount1 = new Account();          
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '124';
        testAccount1.DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        
        testAccount1.One_Code_Status__c = 'Inactive';
        
        testAccount1.Account_Country__c = 'United States' ;
        testAccount1.BillingCountry = 'United States' ;
        testAccount1.Managed_Office__c = null;
        //testAccount1.ShippingPostalCode = null;
        testAccount1.BillingPostalCode = String.valueOf('001038');
                
        insert testAccount1;                        
        
        system.runAs(User2){
        
            testAccount.One_Code_Status__c = null;
            testAccount.Relationship_Manager__c = testColleague1.Id;                    
            update testAccount;
            Test.startTest();
            testAccount.One_Code_Status__c = 'Active';            
            testAccount.Sic = '445';
            testAccount.Primary_SIC_Code__c = '1112';
            testAccount.SIC_Code_2__c = '2223';
            testAccount.OwnerId = User1.Id;
            update testAccount;
            Test.stopTest();            
            testAccount1.OwnerId = user2.ID;            
            update testAccount1;                                            
        }

        
    }
    
    /*******************************************************
    *
    *****************************************/
    
    static testMethod void myAccountTest8()
    {
        Test.startTest();
     //   User user1,user2, user3 = new User();
     //   String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]){
            insertUsers();
        }
                
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '1234567893';        
     //   testColleague1.EMPLID__c = user1.EmployeeNumber;
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;

        Colleague__c  testColleague = mtdata.buildColleague();     
    /*    user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    
        user1.EmployeeNumber = '123';    */
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();                           
                
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';    
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague1.Id;
        testAccount.DUNS__c = '111';
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Automobile';
        insert testAccount;
                                                                   
        Account testAccount1 = new Account();          
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = testColleague1.Id;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '124';
        testAccount1.DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        testAccount1.client_Retention__c=78;       
        insert testAccount1;
        
        Map<String,String> dunsNoMap = new Map<String,String>();
        dunsNoMap.put('12345',testAccount.Id);
        
        
        system.runAs(User2){
            
            testAccount1.OwnerId = user1.ID;
            
            update testAccount1;
            
            AP01_AccountTriggerUtil.updateParentwithDuns(dunsNoMap,testAccount1,'12345');                    
        }

        Test.stopTest();
}


    static testMethod void myAccountTest9()
    {
        
     //   User user1,user2, user3 = new User();
     //   String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        //instantiate TestData class
        System.runAs([Select Id From User where Id = :UserInfo.getUserId()][0]){
            insertUsers();   
            createDMGroupMember();     
        }
                
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.EMPLID__c = '12345678902';
        testColleague1.LOB__c = '111114';
        testColleague1.Last_Name__c = 'testLastName';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        insert testColleague1;

        Colleague__c  testColleague = mtdata.buildColleague();     
     /*   user1 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert1', 'usrLstName', user1);
        user2 = Mercer_TestData.createUser(mercerStandardUserProfile, 'usert2', 'usrLstName2', user2);
        user3 = createUser(mercerStandardUserProfile, 'usert3', 'usrLstName3', user3, 2);    */
  //      user1.EmployeeNumber = '123';    
        Mercer_Office_Geo_D__c mog = new Mercer_Office_Geo_D__c();                           
                
        String collID = testColleague.Id;
        testColleague.EMPLID__c = user1.EmployeeNumber;
        testColleague.Location__c = mog.Id;
        testColleague.Empl_Status__c = 'Active';
        
        
        US_Office__c usOffice = new US_Office__c();
        usOffice.Name = 'Test US Office';
        usOffice.Zip_Code_Initial__c = '001';
        insert usOffice;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = CollId;
        testAccount.DUNS__c = '111';
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Automobile';
        testAccount.Sic = '444';
        testAccount.Primary_SIC_Code__c = '1111';
        testAccount.SIC_Code_2__c= '2222';
        testAccount.One_Code_Status__c = 'Active';
        testAccount.Account_Country__c = 'United States' ;
        testAccount.BillingCountry = 'United States' ;
        testAccount.Managed_Office__c = null;
        testAccount.ShippingPostalCode = String.valueOf('001034');
        insert testAccount;
                                                                   
        Account testAccount1 = new Account();          
        testAccount1.Name = 'TestAccount';     
        testAccount1.Relationship_Manager__c = collID;
        testAccount1.BillingCity = 'TestCity';
        testAccount1.BillingCountry = 'TestCountry';
        testAccount1.BillingStreet = 'TestStreet';
        testAccount1.OwnerId = user1.ID;
        testAccount1.GU_DUNS__c = '124';
        testAccount1.DUNS__c = '123';
        testAccount1.ShippingCity = 'TestCity1';
        testAccount1.ShippingStreet =  'TestStreet1';
        
        testAccount1.One_Code_Status__c = 'Active';
        
        testAccount1.Account_Country__c = 'United States' ;
        testAccount1.BillingCountry = 'United States' ;
        testAccount1.Managed_Office__c = null;
        //testAccount1.ShippingPostalCode = null;
        testAccount1.BillingPostalCode = String.valueOf('001038');
                
        insert testAccount1;                        
        
        system.runAs(User2){
        
            testAccount.One_Code_Status__c = null;
            testAccount.Relationship_Manager__c = testColleague1.Id;                    
            update testAccount;
            Test.startTest();
            testAccount.One_Code_Status__c = 'Active';            
            testAccount.Sic = '445';
            testAccount.Primary_SIC_Code__c = '1112';
            testAccount.SIC_Code_2__c = '2223';
            testAccount.OwnerId = User1.Id;            
            update testAccount;
            
            Test.stopTest();    
            testAccount1.DUNS__c = '124';        
            testAccount1.OwnerId = user2.ID;            
            update testAccount1;                                            
        }
            
        
    }
}