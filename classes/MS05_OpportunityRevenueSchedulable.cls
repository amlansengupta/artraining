/*Purpose: This Apex class implements schedulable interface to schedule MS06_OpportunityRevenueBatchable class.
==============================================================================================================================================
History ----------------------- 
                                                                 VERSION     AUTHOR       DATE                    DETAIL 
                                                                    1.0 -    Arijit     04/29/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS05_OpportunityRevenueSchedulable implements Schedulable{
    
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS06_OpportunityRevenueBatchable(), Integer.valueOf(System.Label.CL74_MS06BatchSize));
    }

}