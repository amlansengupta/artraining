/*
===================================================================
Request                             Date        Modified By
12638-Replacing step wth stage      1/18/2019   Trisha
===================================================================
*/

public with sharing class AP007_OppValidations
{
    public static void validationcheckBeforeInsert(List<Opportunity> triggernew)
    {
        String Label = System.Label.opp_validation_switch;
        Id profileId = userinfo.getProfileId();
        String profileName =[Select Id,Name from Profile where Id=:profileId].Name;
        List<ID> accIdList = new List<Id>();
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        for(Opportunity opp :triggernew){  
            oppList.add(opp);
            accIdList.add(opp.AccountId);
        } 
        Map<Id,Account> oppAccMap = new Map<Id,Account>();
        accList = [Select ID,One_Code_Status__c,One_Code__c from Account where ID IN: accIdList]; 
        for(Account acc:accList)
        {
            oppAccMap.put(acc.Id,acc);
        }
        
        for(Opportunity opp :triggernew){   
            system.debug('^^^'+opp.AccountId); 
            system.debug('%%%'+opp.Account.One_Code_Status__c);  
            String statusAcc = oppAccMap.get(opp.AccountId).One_Code_Status__c;
            if (statusAcc== 'Inactive' && profileName != 'System Administrator' && Label == 'TRUE'){ opp.addError(System.Label.VR04_ErrorMessage);}
        }
    }
    
    public static void validationcheckBeforeUpdate(List<Opportunity> triggernew)
    {
        String Label1 = System.Label.opp_validation_switch;
        List<Account> accList1 = new List<Account>();
        Map<Id,Account> oppAccMap1 =new Map<Id,Account>();
        List<ID> accIdList1 = new List<Id>();
        List<Opportunity> oppList1 = new List<Opportunity>();
        for(Opportunity opp :triggernew){  
            oppList1.add(opp);
            accIdList1.add(opp.AccountId);
        } 
        accList1 = [Select ID,One_Code_Status__c,One_Code__c from Account where ID IN: accIdList1];
        for(Account acc1:accList1)
        {
            oppAccMap1.put(acc1.Id,acc1);
        }
        
        for(Opportunity opp1 :triggernew){   
            system.debug('&&&');
            system.debug('^^^'+opp1.AccountId); 
            system.debug('%%%'+opp1.Account.One_Code_Status__c);  
            system.debug('--->oppmap'+oppAccMap1);
            system.debug('--->acclist'+accList1);
            String statusAcc1 = oppAccMap1.get(opp1.AccountId).One_Code__c;
            /****request id:12638 replacing below conditional block from step to stage , VR08_ErrorMessage label value changed accordingly START ****/
            // if((statusAcc1 == '' || statusAcc1 == null) && (opp1.Step__c.Contains('Finalizing EL') || opp1.Step__c.Contains('SOW Signed')) && Label1 == 'TRUE'){ opp1.addError(System.Label.VR08_ErrorMessage); }                                  
            if((statusAcc1 == '' || statusAcc1 == null) && (opp1.stageName.Contains(System.Label.Stage_Selected) || opp1.stageName.Contains(System.Label.Stage_Pending_Project) || opp1.stageName.Contains(System.Label.CL48_OppStageClosedWon)) && Label1 == 'TRUE'){ opp1.addError(System.Label.VR08_ErrorMessage); }                                  
            /****request id:12638 replacing below conditional block from step to stage , VR08_ErrorMessage label value changed accordingly END ****/
        }
    }
    
    public static void validationcheckBeforeUpdateChanges(Map<Id,Opportunity> triggernew,Map<Id,Opportunity> triggerold)
    {
        system.debug('-->Entering 3 method');
        String Label1 = System.Label.opp_validation_switch;
        //RecordType RecType = [Select Id,Name From RecordType  Where SobjectType = 'Opportunity' and DeveloperName = 'Opportunity_Locked'];
        /*************************Request Id:17497 : Replacing the query with Schema.SObjectType to fetch the RecordType ID START********************************************/
        Id RecType;
        try{
          RecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Product Expansion').getRecordTypeId();
           }
        catch(Exception e)
           {
            RecType = [Select Id,Name From RecordType  Where SobjectType = 'Opportunity' and DeveloperName = 'Opportunity_Locked'].Id;
          }
        /*************************Request Id:17497 : Replacing the query with Schema.SObjectType to fetch the RecordType ID END********************************************/
        Id profileId = userinfo.getProfileId();
        String profileName =[Select Id,Name from Profile where Id=:profileId].Name;
        for(Opportunity opp2 :triggernew.values())
        {
            system.debug('-->profilename'+profileName );
            system.debug('-->RecTypeName2'+opp2.RecordTypeId);
            /****request id:12638, removing step part from if condition START *****/
            //if(Label1 == 'True' && (profileName != 'System Administrator' && profileName != 'Mercer System Admin Lite' && opp2.RecordTypeId == RecType.Id && (opp2.CloseDate != triggerold.get(opp2.Id).CloseDate || opp2.StageName != triggerold.get(opp2.Id).StageName || opp2.OwnerId != triggerold.get(opp2.Id).OwnerId || opp2.Step__c != triggerold.get(opp2.Id).Step__c) )) { opp2.addError(System.Label.VR11_ErrorMessage); }
            //if(Label1 == 'True' && (profileName != 'System Administrator' && profileName != 'Mercer System Admin Lite' && opp2.RecordTypeId == RecType.Id && (opp2.CloseDate != triggerold.get(opp2.Id).CloseDate || opp2.StageName != triggerold.get(opp2.Id).StageName || opp2.OwnerId != triggerold.get(opp2.Id).OwnerId) )) { opp2.addError(System.Label.VR11_ErrorMessage); }
            if(Label1 == 'True' && (profileName != 'System Administrator' && profileName != 'Mercer System Admin Lite' && opp2.RecordTypeId == RecType && (opp2.CloseDate != triggerold.get(opp2.Id).CloseDate || opp2.StageName != triggerold.get(opp2.Id).StageName || opp2.OwnerId != triggerold.get(opp2.Id).OwnerId) )) { opp2.addError(System.Label.VR11_ErrorMessage); }
            /****request id:12638, removing step part from if condition END *****/
        }
    }
    
    public static void updateFieldsAfterInsert(List<Opportunity> triggernew)
    {
        
        futureOppUpdate(JSON.serialize(triggernew)); 
        
    }
    
    public static void updateFieldsAfterUpdate(List<Opportunity> triggernew)
    {
        system.debug('--->Inside Loop MP');
        system.debug('-->beforeserailize'+triggernew);
        futureOppUpdate(JSON.serialize(triggernew));
        
    }
    
    public static void updateFieldsAfterUpdateOpp(List<User> triggernew){
        futureOppUpdateUser(JSON.serialize(triggernew));
    }
    
    @future
    public static void futureOppUpdateUser(String oppListSerialized1)
    {
        List<User> triggernew1 = (List<User>)JSON.deserialize(oppListSerialized1, List<User>.class);
        
        List<Id> usrList = new List<Id>();
        List<User> usrList1 = new List<User>();
        Map<Id,User> mapUser = new Map<Id,User>();
        
        Map<Id,Profile> profMap = new Map<Id,Profile>();
        usrList1 = [Select Id,Name,Profile.Name,Level_1_Descr__c from User where Id IN: triggernew1];
        for(User usr: usrList1){
            usrList.add(usr.Id);
            mapUser.put(usr.Id,usr);
        }
        
        system.debug('-->entered user loop');
        List<Opportunity> oppList =new List<Opportunity>();
        List<Opportunity> oppListUpdate =new List<Opportunity>();
        oppList = [Select ID,Owner_Profile_Name__c,Opp_Owner_Operating_Company__c,OwnerId from Opportunity where OwnerId IN: usrList];
        system.debug('-->opplist'+oppList);
        system.debug('-->usermap'+mapUser);
        //   String pfrName;
        for(Opportunity opp:oppList){
            //pfrName = profMap.get(opp.Owner.Profile).Name;
            opp.Owner_Profile_Name__c = mapUser.get(opp.OwnerId).Profile.Name ;
            opp.Opp_Owner_Operating_Company__c = mapUser.get(opp.OwnerId).Level_1_Descr__c ;
            oppListUpdate.add(opp);
        }
        system.debug('-->oppupdt'+oppListUpdate);
        update oppListUpdate; 
    }             
    
    @future
    public static void futureOppUpdate(String oppListSerialized)
    {
        List<Opportunity> triggernew = (List<Opportunity>)JSON.deserialize(oppListSerialized, List<Opportunity>.class);
        system.debug('-->afterserailize'+triggernew);
        List<Account> accList4 = new List<Account>();
        List<Id> accIdList4 = new List<Id>();
        List<User> ownerDetList4= new List<User>();
        List<Id> usrIdList4 = new List<Id>();
        List<Colleague__c> clgDetList= new List<Colleague__c>();
        List<Id> clgIdList5 = new List<Id>();
        for(Opportunity opp :triggernew){  
            usrIdList4.add(opp.OwnerId);
            accIdList4.add(opp.AccountId);
            clgIdList5.add(opp.Sibling_Contact_Name__c);
        } 
        Map<Id,Account> oppAccMap4 = new Map<Id,Account>();
        accList4 = [Select ID,Name,Account_Region__c from Account where ID IN: accIdList4]; 
        Map<Id,User> oppUserMap4 = new Map<Id,User>();
        ownerDetList4 = [Select ID,Level_1_Descr__c,Profile.Name from User where ID IN: usrIdList4]; 
        Map<Id,Colleague__c> oppClgMap5 = new Map<Id,Colleague__c>();
        clgDetList = [Select ID,Location_Descr__c from Colleague__c where ID IN: clgIdList5]; 
        system.debug('--->updateLoop1'+accList4);
        system.debug('--->updateLoop2'+ownerDetList4);
        system.debug('--->updateLoop2'+clgDetList);
        
        for(Account acc:accList4)
        {
            oppAccMap4.put(acc.Id,acc);
        }
        for(User usr:ownerDetList4)
        {
            oppUserMap4.put(usr.Id,usr);
        }
        for(Colleague__c clg : clgDetList)     {  oppClgMap5.put(clg.Id,clg); }
        
        List<Opportunity> oppUpdateList = new List<Opportunity>();
        for(Opportunity opp4:triggernew){
            
            system.debug('--->Inside Loop MP');
            opp4.Account_Region__c = oppAccMap4.get(opp4.AccountId).Account_Region__c;
            opp4.Account_Name_LDASH__c = oppAccMap4.get(opp4.AccountId).Name;
            system.debug('--->region'+oppAccMap4.get(opp4.AccountId).Account_Region__c);
            system.debug('--->ldash'+oppAccMap4.get(opp4.AccountId).Name);
            //system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
            system.debug('--->sibling2'+opp4.Sibling_Contact_Name__c);
            if(opp4.Sibling_Contact_Name__c != null )
            {
                opp4.Sibling_Contact_Office_Name__c = oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c;
                system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
            }
            else{
                opp4.Sibling_Contact_Office_Name__c = '';
            }
            
            opp4.Owner_Profile_Name__c = oppUserMap4.get(opp4.OwnerId).Profile.Name;
            opp4.Opp_Owner_Operating_Company__c = oppUserMap4.get(opp4.OwnerId).Level_1_Descr__c;
            
            oppUpdateList.add(opp4);
        }
        system.debug('-->updateList'+oppUpdateList);
        
        update oppUpdateList;
    }
    
    //Method for Account Update
    
    public static void updateFieldsAfterAccountUpdate(List<Account> triggernew)
    {
        futureupdateFieldsAfterAccountUpdate(JSON.serialize(triggernew));
    }
    
    @future
    public static void  futureupdateFieldsAfterAccountUpdate(String oppListSerialized2)
    {
        List<Account> triggernew1 = (List<Account>)JSON.deserialize(oppListSerialized2, List<Account>.class);
        
        List<Id> AccList1 = new List<Id>();
        List<Account> AcctList = new List<Account>();
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        
        AcctList = [Select id,Account_Region__c,Name from Account where id IN:triggernew1];
        
        for(Account acc : AcctList)
        {
            AccList1.add(acc.id); 
            mapAccount.put(acc.id,acc); 
        }
        
        List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        
        oppList = [Select id,Name,Account_Region__c,Account_Name_LDASH__c,AccountId from Opportunity where AccountId IN: AccList1];
        
        for(Opportunity opp : oppList)
        {
            if(!test.isrunningtest()){
                opp.Account_Region__c = mapAccount.get(opp.AccountId).Account_Region__c ;
                opp.Account_Name_LDASH__c = mapAccount.get(opp.AccountId).Name;
            }
            oppListToUpdate.add(opp);
        } 
        update oppListToUpdate;   
        
        
    }      
    
    //Method for Colleague Update
    
    public static void updateFieldsAfterColleagueUpdate(List<Colleague__c> triggernew)
    {
        futureupdateFieldsAfterColleagueUpdate(JSON.serialize(triggernew));
    }    
    
    @future
    public static void futureupdateFieldsAfterColleagueUpdate(String OppListSerialize3)
    {
        List<Colleague__c> triggernew2 = (List<Colleague__c>)JSON.deserialize(OppListSerialize3, List<Colleague__c>.class);
        
        List<Id> CollList1 = new List<Id>();
        List<Colleague__c> CollList = new List<Colleague__c>();
        Map<Id,Colleague__c> mapColleague = new Map<Id,Colleague__c>();
        
        CollList = [Select id,Location_Descr__c from Colleague__c where id IN:triggernew2];
        
        for(Colleague__c c : CollList)
        {
            CollList1.add(c.id);
            mapColleague.put(c.id,c);
            
        }
        
        List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        
        oppList = [Select id,Name,Sibling_Contact_Office_Name__c,Sibling_Contact_Name__r.Location_Descr__c,Sibling_Contact_Name__r.id from Opportunity where Sibling_Contact_Name__r.id IN: CollList1];
        system.debug('>>>'+oppList);
        
        for(Opportunity o : oppList)
        {
            o.Sibling_Contact_Office_Name__c = mapColleague.get(o.Sibling_Contact_Name__r.id).Location_Descr__c;
            oppListToUpdate.add(o);
        }
        update oppListToUpdate;
        
    }
    public static void futureOppUpdate123(List<Opportunity> triggernew)
    {
        //List<Opportunity> triggernew = (List<Opportunity>)JSON.deserialize(oppListSerialized, List<Opportunity>.class);
        //system.debug('-->afterserailize'+triggernew);
        system.debug('I am in futureOppUpdate123');
        List<Account> accList4 = new List<Account>();
        List<Id> accIdList4 = new List<Id>();
        List<User> ownerDetList4= new List<User>();
        List<Id> usrIdList4 = new List<Id>();
        List<Colleague__c> clgDetList= new List<Colleague__c>();
        List<Id> clgIdList5 = new List<Id>();
        for(Opportunity opp :triggernew){  
            usrIdList4.add(opp.OwnerId);
            accIdList4.add(opp.AccountId);
            clgIdList5.add(opp.Sibling_Contact_Name__c);
        } 
        Map<Id,Account> oppAccMap4 = new Map<Id,Account>();
        accList4 = [Select ID,Name,Account_Region__c from Account where ID IN: accIdList4]; 
        Map<Id,User> oppUserMap4 = new Map<Id,User>();
        ownerDetList4 = [Select ID,Level_1_Descr__c,Profile.Name from User where ID IN: usrIdList4]; 
        Map<Id,Colleague__c> oppClgMap5 = new Map<Id,Colleague__c>();
        clgDetList = [Select ID,Location_Descr__c from Colleague__c where ID IN: clgIdList5]; 
        system.debug('--->updateLoop1'+accList4);
        system.debug('--->updateLoop2'+ownerDetList4);
        system.debug('--->updateLoop2'+clgDetList);
        
        for(Account acc:accList4)
        {
            oppAccMap4.put(acc.Id,acc);
        }
        for(User usr:ownerDetList4)
        {
            oppUserMap4.put(usr.Id,usr);
        }
        for(Colleague__c clg : clgDetList)
        {
            oppClgMap5.put(clg.Id,clg);
        }
        
        List<Opportunity> oppUpdateList = new List<Opportunity>();
        for(Opportunity opp4:triggernew){
            if(oppAccMap4.containskey(opp4.AccountId)){
                
                system.debug('--->Inside Loop MP');
                if(oppAccMap4.get(opp4.AccountId).Account_Region__c!=null)
                    opp4.Account_Region__c = oppAccMap4.get(opp4.AccountId).Account_Region__c;
                if(oppAccMap4.get(opp4.AccountId).Name!=null)
                    opp4.Account_Name_LDASH__c = oppAccMap4.get(opp4.AccountId).Name;
            }
            //system.debug('--->region'+oppAccMap4.get(opp4.AccountId).Account_Region__c);
            //system.debug('--->ldash'+oppAccMap4.get(opp4.AccountId).Name);
            //system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
            system.debug('--->sibling2'+opp4.Sibling_Contact_Name__c);
            
            if(opp4.Sibling_Contact_Name__c != null )
            {
                if(oppClgMap5.containskey(opp4.Sibling_Contact_Name__c)){
                    opp4.Sibling_Contact_Office_Name__c = oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c;
                    system.debug('--->sibling'+oppClgMap5.get(opp4.Sibling_Contact_Name__c).Location_Descr__c);
                }
            }
            else{
                opp4.Sibling_Contact_Office_Name__c = '';
            }
            //}
            if(oppUserMap4.containskey(opp4.OwnerId)){
                opp4.Owner_Profile_Name__c = oppUserMap4.get(opp4.OwnerId).Profile.Name;
                if(oppUserMap4.get(opp4.OwnerId).Level_1_Descr__c!=null)
                    opp4.Opp_Owner_Operating_Company__c = oppUserMap4.get(opp4.OwnerId).Level_1_Descr__c;
            }
            oppUpdateList.add(opp4);
        }
        system.debug('-->updateList'+oppUpdateList);
        
        //update oppUpdateList;
    }
    
}