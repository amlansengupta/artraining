/*Purpose:  This is a controller class for growth plan PDF generator
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/11/2014  (As part of Request # 5367) Created Controller class for generating PDF of growth plan 
   2.0 -    Jagan       26/11/2014  Fixed the null pointer exception error.  
   3.0 -    Sarbpreet   05/26/2015  As part of July Release(PMO# 6798) updated the class to spilt the picklist values of   
                                    Past_Buying_Centers__c and Proposed_Mercer_Solutions_01__c fields on Growth Plan object.
   4.0 -    Neeraj      07/29/2015  As part of September Release (PMO# 6798) updated the code to add the newly created fields and calcualte TOR and CYR of the opportunities associated with the growth plan's account.
   5.0 -    Sarbpreet   08/12/2015  As part of September Release (PMO# 6798) updated the code to query all the contacts related to the account of growth plan.
   6.0 -    Neeraj      08/26/2015  As part of September Release (PMO# 6798) updated the code to add Incumbent service provider and Survey responses Related List .
============================================================================================================================================== 
*/
public with sharing class AP114_GrowthPlan_pdfgenerator {


 public Growth_Plan__c growth { get; set; }
 public integer int1 {get;set;}
 public List<Task> tList{get;set;}
 public List<AccountTeamMember> AccTeamList{get;set;}
 public List<Contracts_Renewals__c> ContRenewList{get;set;}
 public List<Sensitve_At_Risk__c> SensRiskList{get;set;}
 public List<Opportunity> opptyList{get;set;}
 public List<Opportunity> openOpptyList{get;set;}
 public List<Contact> conList{get;set;}
 //creating for september 2015 release
    public List<Incumbent__c> incList {get;set;}
    public decimal OppTOR {get;set;}   
    public decimal OppCYR {get;set;}
    public integer OppCount {get;set;}
    public List<Survey_Response__c> SurvResList{get;set;}
 
 public String tempGBS_Projected_Current_Year_Revenue { get; set; }
 public List<String> lstPastBuyCentersValues = new List<String>();
 
 public static final string STR_CURR = 'USD';
 public static final string STR_CLOSED = 'Closed%';
 public static final string STR_STATUS = 'Active';

 
 /*,
  * @Description : Constructor of Controller class 
  * @ Args       : controller
  * @ Return     : null 
 */
 public AP114_GrowthPlan_pdfgenerator(ApexPages.StandardController controller){
    // As part of September Release (PMO# 6798) updated the query to add the newly created fields.
     // As part of request #9791(AUG 2016),Is_this_Client_a_Key_Account__c field has been add(New field).
    growth = [select id,Account__c,Account__r.name, Planning_Year__c, Status__c, Business_Objective__c, key_Buying_Centers_Identified__c, key_relationships_entered__c,Parent_Growth_Plan_On_CGP__c, 
                        RET_Projected_Current_Year_Revenue__c,RET_Recurring_and_Carry_forward_Revenue__c,RET_Projected_New_Sales_Revenue__c,RET_Projected_Full_Year_Revenue__c,RET_of_Revenue_Growth_Anticipated01__c,
                        EH_B_Projected_Current_Year_Revenue__c ,Current_Mercer_Solutions__c, Proposed_Mercer_Solutions__c,EH_B_Recurring_and_Carry_forward_Revenue__c,EH_B_Projected_New_Sales_Revenue__c,EH_B_Projected_Full_Year_Revenue__c, 
                        Is_this_Client_a_Key_Account__c,OTH_Planning_Yr_and_Carry_Frw_Rev__c,OTH_Projected_Full_Year_Revenue_Actual__c,
                        RET_Projected_Full_Year_Revenue_Actual__c,RET_Planning_Yr_and_Carry_Frw_Rev__c,GBS_Planning_Yr_and_Carry_Frw_Rev__c,GBS_Projected_Full_Year_Revenue_Actual__c,
                        EH_B_Planning_Yr_and_Carry_Frw_Rev__c,EH_B_Projected_Full_Year_Revenue_Actual__c,
                        TAL_Projected_Current_Year_Revenue__c,TAL_Recurring_and_Carry_forward_Revenue__c,TAL_Projected_New_Sales_Revenue__c,TAL_Projected_Full_Year_Revenue__c, GBS_Projected_Current_Year_Revenue__c,GBS_Recurring_and_Carry_forward_Revenue__c,GBS_Projected_New_Sales_Revenue__c,
                        GBS_Projected_Full_Year_Revenue__c,INTL_Projected_Current_Year_Revenue__c,INTL_Recurring_and_Carry_forward_Revenue__c,INTL_Planning_Yr_and_Carry_Frw_Rev__c,INTL_Projected_Full_Year_Revenue_Actual__c,
                        INTL_Projected_New_Sales_Revenue__c,INTL_Projected_Full_Year_Revenue__c,OTH_Projected_Current_Year_Revenue__c,Proposed_Solution_10_Check__c,Proposed_Solution_1_Check__c,Proposed_Solution_2_Check__c,Proposed_Solution_3_Check__c,Proposed_Solution_4_Check__c,Proposed_Solution_5_Check__c,
                        Proposed_Solution_6_Check__c,Proposed_Solution_7_Check__c,Proposed_Solution_8_Check__c,Proposed_Solution_9_Check__c,TAL_Planning_Yr_and_Carry_Frw_Rev__c,TAL_Projected_Full_Year_Revenue_Actual__c,TOT_Planning_Yr_and_Carry_Frw_Rev__c,TOT_Projected_Full_Year_Revenue_Actual__c,
                        OTH_Recurring_and_Carry_forward_Revenue__c,OTH_Projected_New_Sales_Revenue__c,OTH_Projected_Full_Year_Revenue__c,OTH_of_Revenue_Growth_Anticipated01__c,                                                 
                        Current_Mercer_Solutions_01__c,Proposed_Mercer_Solutions_01__c,Primary_Client_Buying_Center__c,Next_actions_to_achieve_next_year_s_rev__c,
                        Client_s_strengths__c,Client_s_weaknesses__c,Client_s_opportunities__c,Client_s_threats__c,Key_Client_Dates__c,no_revenue_for_Plan_year_Explanation__c,
                        EH_B_of_Revenue_Growth_Anticipated01__c,TOT_Recurring_Carry_forward_Revenue__c,TOT_Projected_Sales_Revenue__c,TOT_Projectd_Current_Year_Revenue__c,
                        TOT_Projectd_Full_Year_Revenue__c,TAL_of_Revnue_Growth_Anticipated__c, Client_Business_Objectives__c ,Client_Business_Needs__c,GBS_of_Revenue_Growth_Anticipated__c ,
                        INTL_of_Revenue_Growth_Anticipated01__c, TOT_of_Revenue_Growth_Anticipated01__c,account__r.One_Code_Status__c, account__r.Relationship_Manager__r.Name,account__r.One_Code__c,
                        Proposed_Solution1__c,Proposed_Solution2__c,Proposed_Solution3__c,Proposed_Solution4__c,Proposed_Solution5__c,Proposed_Solution6__c,Proposed_Solution7__c,
                        Proposed_Solution8__c,Proposed_Solution9__c,Proposed_Solution10__c, Current_Solution_1__c,  Current_Solution_2__c,Current_Solution_3__c, Current_Solution_4__c, Current_Solution_5__c, Current_Solution_6__c, Current_Solution_7__c, Current_Solution_8__c, Current_Solution_9__c, Current_Solution_10__c,            
                        account__r.Relationship_Manager__r.Market__c,Account__r.RM_LOB__c,Account__r.Date_of_Last_CEM_Interview__c,Account__r.Account_Tier__c,account__r.Relationship_Manager__r.Sub_Market__c,account__r.Account_Country__c,account__r.Account_Sub_Market__c,
                        Actions__c,Past_Buying_Centers__c,account__r.Relationship_Manager__r.Office__c,Rebid_in_Planning_Year__c,account__r.Type_of_Organization__c
                        ,Growth_Indicator__c, Share_of_Wallet__c, Parent_Growth_Plan_ID__c,Parent_Growth_Plan_ID__r.Name, account__r.NPS_Survey_Sent_Date__c, account__r.Average_NPS_Score__c,
                        When_was_the_last_RFP_for_Retirement__c, Overall_rating_in_the_last_CEM__c, When_was_the_last_RFP_for_Investments__c, Change_in_Decision_maker_last_year__c, When_was_the_last_RFP_for_EH_B__c,Relationship_risk_as_assessed_by_team__c,When_was_the_last_RFP_for_Talent__c,
                        LOB_s_for_service_issues__c, When_is_our_contract_expiring__c, LOB_s_change_in_delivery_last_year__c, H_B_Yield_Initiative_target_carrier__c, H_B_Yield_Initiative_target_higher_comm__c,Theme_EP_Employee_Engagement__c,Theme_EP_Future_Workforce__c,Theme_EP_Pension_Risk__c,Theme_EP_Global_Environment__c,Theme_GM_Career__c,Theme_GM_Health__c,Theme_GM_Wealth__c,Theme_NA_Financial_Wellness__c,Theme_NA_Healthcare__c,Theme_NA_Pension__c,Theme_NA_Talent__c  from Growth_Plan__c  where id=:apexpages.currentpage().getparameters().get(ConstantsUtility.STR_ObjID)];
    
    
         
      tempGBS_Projected_Current_Year_Revenue = String.valueOf(growth.GBS_Projected_Current_Year_Revenue__c);
       if(tempGBS_Projected_Current_Year_Revenue<>null && tempGBS_Projected_Current_Year_Revenue.contains(STR_CURR))
         tempGBS_Projected_Current_Year_Revenue = tempGBS_Projected_Current_Year_Revenue.replace(STR_CURR, ConstantsUtility.STR_Blank);
         growth.GBS_Projected_Current_Year_Revenue__c = Integer.valueOf(tempGBS_Projected_Current_Year_Revenue);
          
              
     if(growth != null){
                        
     tList = [Select id, subject, ActivityDate, Status, Priority, Description, ownerid, whatid,owner.name from Task where whatid=: growth.id ]; 
     AccTeamList = [Select id, user.name, TeamMemberRole from AccountTeamMember where AccountId =: growth.Account__c];
     ContRenewList = [Select id,LOB__c, Product_Service__c, Contract_Start_Date__c, Contract_Expiration_Date__c from  Contracts_Renewals__c where Account__c =: growth.Account__c];           
     SensRiskList = [Select id, LOB__c, At_Risk_Type__c,At_Risk_Reason__c, Impact__c,Date_Opened__c,Date_Closed__c from Sensitve_At_Risk__c where Account__c =: growth.Account__c and  Date_Closed__c>=TODAY];
     opptyList = [Select id, name, Stagename, Total_Opportunity_Revenue_USD__c,Current_Year_Revenue_USD__c,CloseDate, Owner.name from opportunity where AccountID =: growth.Account__c AND (NOT Stagename like :STR_CLOSED) ORDER BY Total_Opportunity_Revenue_USD__c DESC NULLS LAST LIMIT 5 ];
     incList = [select id,Incumbent_Service_Provider__r.name,Account__c,Type_of_Service__c,LOB__c from Incumbent__c where Account__c =:growth.Account__c];
     // As part of September Release (PMO# 6798) queried all the contacts related to the account of growth plan.
     conList = [Select id, name, Job_Level__c, Title, Contact_Owner_Name__c, Net_Promoter_Score__c from Contact where AccountID =: growth.Account__c AND (Contact_Status__c like: STR_STATUS) order by name];
     // As part of September Release (PMO# 6798) queried all the Survey Responses related to the account of growth plan.
     SurvResList = [Select id, name,Contact__r.name,Question_1__c,Question_2__c,CreatedDate from Survey_Response__c where Account__c =: growth.Account__c];
     openOpptyList = [Select id, name, Stagename, Total_Opportunity_Revenue_USD__c,Current_Year_Revenue_USD__c,CloseDate, Owner.name from opportunity where AccountID =: growth.Account__c AND (NOT Stagename like :STR_CLOSED) ];
     }
     // As part of September Release (PMO# 6798) updated code to calculate TOR and CYR of all the open opportunities 
     OppCYR = 0;
     OppTOR = 0;
     OppCount =0;
     

     if(openOpptyList.size()>0 ){
     for(integer i=0;i<openOpptyList.size();i++){
         
         if(openOpptyList[i].Current_Year_Revenue_USD__c == null)
             openOpptyList[i].Current_Year_Revenue_USD__c = 0;
         if(openOpptyList[i].Total_Opportunity_Revenue_USD__c == null)
             openOpptyList[i].Total_Opportunity_Revenue_USD__c = 0;
         
         OppCYR += openOpptyList[i].Current_Year_Revenue_USD__c;
         OppTOR += openOpptyList[i].Total_Opportunity_Revenue_USD__c;
         OppCount ++;

     }
     }
     // End of September Release (PMO# 6798)  
     
        //As part of July Release(PMO# 6798) updated the class to fetch  the picklist values of Past_Buying_Centers__c field on Growth Plan object.
        Schema.DescribeFieldResult fieldResult =
        Growth_Plan__c.Past_Buying_Centers__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
          lstPastBuyCentersValues.add(f.getLabel());
        }
        
        int1 = Integer.valueof((growth.Planning_Year__c).trim()) - 1;
               
         
 }
 

    /*
     * @Description : Getter for VF 
     * @ Args       : null
     * @ Return     : list of picklist values in Past_Buying_Centers__c field( As part of July Release(PMO# 6798))
    */
    public List<String> getProcuredServices1()
    {
        List<String> lstValues = new List<String>();
        try {           
            Integer i = 0;
            Integer scope = lstPastBuyCentersValues.size()/4;
            for(i=0; i<scope;i++)
            {
                lstValues.add(lstPastBuyCentersValues[i]);
            }
             } catch (Exception ex) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        return lstValues;       
    }
    
     /*
     * @Description : Getter for VF 
     * @ Args       : null
     * @ Return     : list of picklist values in Past_Buying_Centers__c field (As part of July Release(PMO# 6798))
    */
       public List<String> getProcuredServices2()
        {
            List<String> lstValues = new List<String>();
            try {
                Integer i = 0;
                Integer temp = lstPastBuyCentersValues.size()/4;
                Integer scope = lstPastBuyCentersValues.size()/2; 
        
                for(i=temp;i<scope;i++)
                {
                    lstValues.add(lstPastBuyCentersValues[i]);
                }
                } catch (Exception ex) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

            }
            return lstValues;       
        }
     /*
     * @Description : Getter for VF 
     * @ Args       : null
     * @ Return     : list of picklist values in Past_Buying_Centers__c field (As part of July Release(PMO# 6798))
    */
         public List<String> getProcuredServices3()
        {
            List<String> lstValues = new List<String>();
            try {
                Integer i = 0;
                Integer temp = lstPastBuyCentersValues.size()/2;
                Integer scope = lstPastBuyCentersValues.size()-2; 
        
                for(i=temp;i<scope;i++)
                {
                    lstValues.add(lstPastBuyCentersValues[i]);
                }
                } catch (Exception ex) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

            }
            return lstValues;       
        }
     /*
     * @Description : Getter for VF 
     * @ Args       : null
     * @ Return     : list of picklist values in Past_Buying_Centers__c field (As part of July Release(PMO# 6798))
    */
    public List<String> getProcuredServices4()
        {
            List<String> lstValues = new List<String>();
            try {
                Integer i = 0;
                Integer temp = lstPastBuyCentersValues.size()-2;
                Integer scope = lstPastBuyCentersValues.size(); 
        
                for(i=temp;i<scope;i++)
                {
                    lstValues.add(lstPastBuyCentersValues[i]);
                }
                } catch (Exception ex) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

            }
            return lstValues;       
        }

 
    
    
    
 
}