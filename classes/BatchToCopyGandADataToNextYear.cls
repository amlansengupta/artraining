global class BatchToCopyGandADataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalGSuccessStr;
	private string finalGFailStr;
	private  string finalASuccessStr;
	private string finalAFailStr;
	
    global BatchToCopyGandADataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String headerG = 'Account,Goal,Goal Description,Growth Plan,Previous Year Goal Relationship,Relationship Status,Risks & Barriers,Status,Success Metric,Record Status \n';
        string hearder2G= 'Id,Account,Goal,Goal Description,Growth Plan,Previous Year Goal Relationship,Relationship Status,Risks & Barriers,Status,Success Metric,Record Status \n';
        finalGSuccessStr=hearder2G;
        finalGFailStr=headerG;
        
        String headerA = 'Action Name,Action Owner,Due Date,Goal Detail,Priority,Status,Record Status \n';
        string hearder2A= 'Id,Action Name,Action Owner,Due Date,Goal Detail,Priority,Status,Record Status \n';
        finalASuccessStr=hearder2A;
        finalAFailStr=headerA;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
    	System.debug('Goal Relationship  oldGPList>>>>'+oldGPList.size());
        Set<Id> accountIdSet = new Set<Id>(); 
        try {
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
				
		}
		
			
		// Goal_Action__c start
            Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
        	List<Growth_Plan__c> newGpList=[Select Id,Name,Account__c from Growth_Plan__c where fcstPlanning_Year__r.name =:toSelectedYear and Account__c IN:accountIdSet];
            if(newGpList!=null && newGpList.size()>0){
                for(Growth_Plan__c gpObjNew:newGpList){
                    accIdVsNewGp.put(gpObjNew.Account__c,gpObjNew);
                }
            }
            
            System.debug('Goal Relationship  newGpList>>>>'+newGpList.size());
            List<Goal_Relationship__c> insertNewGRObjList = new List<Goal_Relationship__c>();
            List<Goal_Relationship__c> grObjectList = [select id,name,Account__c,Area__c,Goal_Description__c,Growth_Plan__c,Relationship_Status__c,Risks_Barriers__c,Executive_Status__c,Executive_Name_And_Role__c,LastModifiedBy.Name,lastModifiedDate from Goal_Relationship__c where Growth_Plan__c IN :oldGPList];
             System.debug('Goal Relationship  grObjectList>>>>'+grObjectList.size());
            if(grObjectList != null && grObjectList.size()>0){
                for(Goal_Relationship__c grOldObj : grObjectList){
                    if(accIdVsNewGp.containsKey(grOldObj.Account__c)){
                        Goal_Relationship__c grNewObj = new Goal_Relationship__c();
                        
                        if(grOldObj.Account__c != null)
                            grNewObj.Account__c = grOldObj.Account__c;
                            
                        grNewObj.Growth_Plan__c = accIdVsNewGp.get(grOldObj.Account__c).Id;
                        
                        if(grOldObj.Goal_Description__c != null)
                            grNewObj.Goal_Description__c = grOldObj.Goal_Description__c;
                        if(grOldObj.Relationship_Status__c != null)
                            grNewObj.Relationship_Status__c = grOldObj.Relationship_Status__c;
                        if(grOldObj.Risks_Barriers__c != null)
                            grNewObj.Risks_Barriers__c = grOldObj.Risks_Barriers__c;
                        if(grOldObj.Executive_Status__c != null)
                            grNewObj.Executive_Status__c = grOldObj.Executive_Status__c;
                        if(grOldObj.Executive_Name_And_Role__c != null)
                            grNewObj.Executive_Name_And_Role__c = grOldObj.Executive_Name_And_Role__c;
                        if(grOldObj.Id != null)
                            grNewObj.Previous_Year_Goal_Relationship__c = grOldObj.Id;
                        if(grOldObj.Area__c != null)
                            grNewObj.Area__c = grOldObj.Area__c;
                        
                        grNewObj.Goal_LastModified_By__c = grOldObj.LastModifiedBy.Name;
                        DateTime dt = grOldObj.LastModifiedDate;
                        grNewObj.Goal_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
						
                        insertNewGRObjList.add(grNewObj);
                    }
                }
            }
            System.debug('Goal Relationship  insertNewGRObjList>>>>'+insertNewGRObjList.size());
            if(insertNewGRObjList!=null && insertNewGRObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewGRObjList,false);
	    		if(srList!=null && srList.size()>0){
    				for(Integer i=0;i<srList.size();i++){
    					String area='',goalDescription='',relationshipStatus='',riskBarrirer='',statusStr='',scessMatrix='';
    					
    					if(insertNewGRObjList[i].Area__c!=null)
    						area=insertNewGRObjList[i].Area__c;
    					if(insertNewGRObjList[i].Goal_Description__c!=null)
    						goalDescription=insertNewGRObjList[i].Goal_Description__c;	
    					if(insertNewGRObjList[i].Relationship_Status__c!=null)
    						relationshipStatus=insertNewGRObjList[i].Relationship_Status__c;	
    					if(insertNewGRObjList[i].Risks_Barriers__c!=null)
    						riskBarrirer=insertNewGRObjList[i].Risks_Barriers__c;
    						
    				    if(insertNewGRObjList[i].Executive_Status__c!=null)
    						statusStr=insertNewGRObjList[i].Executive_Status__c;
    					if(insertNewGRObjList[i].Executive_Name_And_Role__c!=null)
    						scessMatrix=insertNewGRObjList[i].Executive_Name_And_Role__c;	
    						
    					if(srList.get(i).isSuccess()){
    						finalGSuccessStr+=srList.get(i).getId()+','+insertNewGRObjList[i].Account__c+','+area.replace(',', '')+','+goalDescription.replace(',', '')+','+insertNewGRObjList[i].Growth_Plan__c+','+insertNewGRObjList[i].Previous_Year_Goal_Relationship__c+','+relationshipStatus.replace(',', '')+','+riskBarrirer.replace(',', '')+','+statusStr.replace(',', '')+','+scessMatrix.replace(',', '')+', Successfully created \n';
    					}
    					if(!srList.get(i).isSuccess()){
    						Database.Error error = srList.get(i).getErrors().get(0);
    						finalGFailStr+=insertNewGRObjList[i].Account__c+','+area.replace(',', '')+','+goalDescription.replace(',', '')+','+insertNewGRObjList[i].Growth_Plan__c+','+insertNewGRObjList[i].Previous_Year_Goal_Relationship__c+','+relationshipStatus.replace(',', '')+','+riskBarrirer.replace(',', '')+','+statusStr.replace(',', '')+','+scessMatrix.replace(',', '')+','+error.getMessage()+' \n';
    					}
    				}
    			}
	    	}
	    	
                
            // Goal_Action__c start
            Map<Id,Id> mapPreviousGRIdVsCurrentGRID = new map<Id,Id>();
            
            for(Goal_Relationship__c grObj:insertNewGRObjList){
            	if(grObj.Previous_Year_Goal_Relationship__c!=null){
            		mapPreviousGRIdVsCurrentGRID.put(grObj.Previous_Year_Goal_Relationship__c,grObj.Id);
            	}
            	
            }
           List<Goal_Action__c> insertNewGAObjList = new List<Goal_Action__c>();
            List<Goal_Action__c> gaObjectList = [select id,name,Goal_Relationship__c,Goal_Relationship__r.Growth_Plan__c, Goal_Relationship__r.Account__c,Action_Name__c,Owner__c,Due_Date__c,Priority__c,Status__c,LastModifiedBy.Name,lastModifiedDate from Goal_Action__c where Goal_Relationship__r.Growth_Plan__c IN :oldGPList];
            System.debug('Goal Relationship  gaObjectList>>>>'+gaObjectList.size());
            if(gaObjectList != null && gaObjectList.size()>0){
                for(Goal_Action__c gaOldObj : gaObjectList){
                    if(mapPreviousGRIdVsCurrentGRID.containsKey(gaOldObj.Goal_Relationship__c)){
                        Goal_Action__c gaNewObj = new Goal_Action__c();
                        
                        if(gaOldObj.Goal_Relationship__c != null)
                            gaNewObj.Goal_Relationship__c = mapPreviousGRIdVsCurrentGRID.get(gaOldObj.Goal_Relationship__c);
                        if(gaOldObj.Action_Name__c != null)
                            gaNewObj.Action_Name__c = gaOldObj.Action_Name__c;
                        if(gaOldObj.Due_Date__c != null)
                            gaNewObj.Due_Date__c = gaOldObj.Due_Date__c;
                        if(gaOldObj.Priority__c != null)
                            gaNewObj.Priority__c = gaOldObj.Priority__c;
                        if(gaOldObj.Status__c != null)
                            gaNewObj.Status__c = gaOldObj.Status__c;
                        if(gaOldObj.Owner__c != null)
                            gaNewObj.Owner__c = gaOldObj.Owner__c;    
                         
                        gaNewObj.Action_LastModified_By__c = gaOldObj.LastModifiedBy.Name;
                        DateTime adt = gaOldObj.LastModifiedDate;
                        gaNewObj.Action_LastModified_Date__c = date.newInstance(adt.year(), adt.month(), adt.day());
                        
                        insertNewGAObjList.add(gaNewObj);
                    }
                }
            }
            System.debug('Goal Relationship  insertNewGAObjList>>>>'+insertNewGAObjList.size());
            if(insertNewGAObjList!=null && insertNewGAObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewGAObjList,false);
	    		if(srList!=null && srList.size()>0){
    				for(Integer i=0;i<srList.size();i++){
    					String actionName='',priority='',statusStr='';
    					if(insertNewGAObjList[i].Action_Name__c!=null)
    						actionName=insertNewGAObjList[i].Action_Name__c;
    					if(insertNewGAObjList[i].Priority__c!=null)
    						priority=insertNewGAObjList[i].Priority__c;
    					if(insertNewGAObjList[i].Status__c!=null)
    						statusStr=insertNewGAObjList[i].Status__c;
    					
    					if(srList.get(i).isSuccess()){
    						finalASuccessStr+=srList.get(i).getId()+','+actionName.replace(',', '')+','+insertNewGAObjList[i].Owner__c+','+insertNewGAObjList[i].Due_Date__c+','+insertNewGAObjList[i].Goal_Relationship__c+','+priority.replace(',', '')+','+statusStr.replace(',', '')+', Successfully created \n';
    					}
    					if(!srList.get(i).isSuccess()){
    						Database.Error error = srList.get(i).getErrors().get(0);
    						finalAFailStr+=actionName.replace(',', '')+','+insertNewGAObjList[i].Owner__c+','+insertNewGAObjList[i].Due_Date__c+','+insertNewGAObjList[i].Goal_Relationship__c+','+priority.replace(',', '')+','+statusStr.replace(',', '')+','+error.getMessage()+' \n';
    					}
    				}
    			}
	    	} 
	    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }
            
    }
    
    global void finish(Database.BatchableContext bc){
		System.debug('Goal Relationship finish finalSuccessStr>>>>'+finalGSuccessStr);
		System.debug('Goal Relationship finish finalFailStr>>>> '+finalGFailStr);
		    
		    
		 AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
    	Messaging.EmailFileAttachment csvAttachment2 = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment3 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalGSuccessStr);
		String csvSuccessName = 'Goal Relationship Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalGFailStr);
		String csvFailName = 'Goal Relationship Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		Blob csvBlob2 = blob.valueOf(finalASuccessStr);
		String csvASuccessName = 'Goals Actions Success File.csv';
		csvAttachment2.setFileName(csvASuccessName);
		csvAttachment2.setBody(csvBlob2);
		
		Blob csvBlob3 = blob.valueOf(finalAFailStr);
		String csvAFailName = 'Goals Actions Fail File.csv';
		csvAttachment3.setFileName(csvAFailName);
		csvAttachment3.setBody(csvBlob3);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = '"Goal Relationship" and "Goals Actions" Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('"Goal Relationship" and "Goals Actions" copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1,csvAttachment2,csvAttachment3});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}