@isTest(seeAllData = true)

public class SC_SP_CM_Allocation_Update_Test{

    public static testMethod void testExecute()
    {
        Test.StartTest();
    
        SC_SP_CM_Allocation_Update_Schedulable schedulable = new SC_SP_CM_Allocation_Update_Schedulable();     
        
        String sch = '0 0 23 * * ?'; 
        
        system.schedule('Test SP CM Allocation', sch, schedulable ); 
        
        Test.stopTest();
    
    }
}