/* Purpose: This test class provides data coverage for updating the Contact Address associated with 
            an Account 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Gyan       12/19/2013  Created test method for Test_AP80_ContactAddrUpdateBatchable batch class
 ==================================================================================================================*/    
@isTest
public class Test_AP80_ContactAddrUpdateBatchable {
      
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */        
    static testMethod void Test_AP80_ContactAddrUpdateBatchable() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
                
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingState = 'Teststate';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
            AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
            DateTime r = DateTime.now();
            String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
            system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
    
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */
     static testMethod void Test_AP80_ContactAddrUpdateBatchable1() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
         
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        //testAccount.BillingCountry = '';
        testAccount.BillingState = 'Taiwan';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
        AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
        DateTime r = DateTime.now();
        String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
        system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
    
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */
    static testMethod void Test_AP80_ContactAddrUpdateBatchable2() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        //testAccount.BillingState = '';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
        AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
        DateTime r = DateTime.now();
        String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
        system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
    
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */
     static testMethod void Test_AP80_ContactAddrUpdateBatchable3() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        //testAccount.BillingCountry = 'Testcountry';
        testAccount.BillingState = 'TestState';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
        AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
        DateTime r = DateTime.now();
        String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
        system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */
    static testMethod void Test_AP80_ContactAddrUpdateBatchable4() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'Taiwan';
        testAccount.BillingState = 'Taiwan';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
            AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
            DateTime r = DateTime.now();
            String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
            system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
   /* 
    * @Description : This test method provides data coverage for Test_AP80_ContactAddrUpdateBatchable batch class 
                        for updating the Contact Address associated with an Account              
    * @ Args       : no arguments        
    * @ Return     : void       
    */
    static testMethod void Test_AP80_ContactAddrUpdateBatchable5() 
    {
         
       
       // Test Data for Collegue Record
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.EMPLID__c = '12345678902' ;
        insert testColleague;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        // Test data for Account Record
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName' ;
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'Taiwan';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        //testAccount.Owner = testUser.Id;
        testAccount.One_Code__c = '111';
        testAccount.Competitor_Flag__c = True;
        testAccount.Primary_Industry_Category__c = 'Agriculture';
        testAccount.Industry_Category__c = 'Banking';
        testAccount.Industry_Category_2__c = 'Investment';
        testAccount.Account_Address_Updated_date__c = System.Today();
        insert testAccount;
        
        //Test data for Contact Record.
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;

        //Update Account
        testAccount.BillingCity = 'TestCity1';
        update testAccount;
        //Test batch
        AP80_ContactAddrUpdateBatchable batch = new AP80_ContactAddrUpdateBatchable();
        database.executeBatch(batch);   
        
        //Test Execute Method
        Test.StartTest();
        system.runAs(User1){
            AP80_ContactAddrUpdateBatchable ac = new AP80_ContactAddrUpdateBatchable();
            DateTime r = DateTime.now();
            String nextTime = String.valueOf(r.second()) + ' ' + String.valueOf(r.minute()) + ' ' + String.valueOf(r.hour() ) + ' * * ?';   
            system.schedule('AP80_ContactAddrUpdateBatchable', nextTime, ac);
        }
        Test.stopTest();
        
       
    }
}