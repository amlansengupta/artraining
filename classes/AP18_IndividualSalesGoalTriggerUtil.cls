/*Purpose: This class contains static methods which are used by trgger TRG10
==============================================================================================================================================
The methods called perform following functionality:

•   On Insertion of Individual Sales Goal Sales Credit Line Item gets added for that Employee

•   On Deletion of Individual Sales Goal Sales Credit Line Item gets deleted

•   On Updation of Individual Sales Goal if the owner is Changed the Sales Credit Line Item for the new owner gets added  

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Shashank   1/23/2013  Created this util class
   2.0 -    Shashank   04/10/2013 Commented the Condition to check Opportunity Stage for req #647
   3.0 -    Gyan       11/21/2013 Implementing requirement#3477 , replace Owner with Colleague
   4.0 -    Sarbpreet 10/12/2015   As per Request #7445 Dec Release 2015, updated logic to update Goals field on User object on the basis of individual sales goal
   5.0 -    Jigyasu   07/04/2016   As per Request #10298 May Release 2016, code has been include to update Goals field on User object when individual sales goal is deleted.
============================================================================================================================================== 
*/
public class AP18_IndividualSalesGoalTriggerUtil
{
    //set variable to store owner Ids
    private static set<string> ownerIds = new set<string>();
    // map variable to owner Id and sales Goal Id
    private static Map<string, string> ownerIdSalesGoalIdMap = new Map<string, string>();
               
    //set variable to store colleague Ids
    private static set<string> collIds = new set<string>();
    // map variable to colleague Id and sales Goal Id
    private static Map<string, string> collIdSalesGoalIdMap = new Map<string, string>();
    
    // map variable to store emp id and user id
    private static Map<string, string> empIdUserIdMap = new Map<string, string>();
    // list variable to store sales credit line items
    private static List<Sales_Credit_Line_Item__c> salesCreditLineItemInsert = new List<Sales_Credit_Line_Item__c>();
    // list variable to store sales credit line items
    private static List<Sales_Credit_Line_Item__c> salesCreditLineItemUpdate = new List<Sales_Credit_Line_Item__c>();
    // list variable to store sales credit line items
    private static List<Sales_Credit_Line_Item__c> salesCreditLineItemDelete = new List<Sales_Credit_Line_Item__c>();
    // set variable to store sales goal id
    private static set<string> salesGoalIdSet = new set<string>();
    
    /*
     * @Description : This method is called on after insertion of Sales Goal records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    
    public static void salesGoalValidationInsert (List<Individual_Sales_Goal__c> triggerNew)
    {
        
        //clear cache
        ownerIds.clear();       
        //map variable to store owner id and sales goal
        Map<String, Individual_Sales_Goal__c> ownerIdSalesGoalMap = new Map<String, Individual_Sales_Goal__c>();
        Map<String, Individual_Sales_Goal__c> ownerIdSalesGoalMapForNoCollegue = new Map<String, Individual_Sales_Goal__c>();
        List<Individual_Sales_Goal__c> lstSalesGoalWithNoCollegue = new List<Individual_Sales_Goal__c>();
        List<Individual_Sales_Goal__c> lstSalesGoalToUpdateOwner = new List<Individual_Sales_Goal__c>();
        
      
        Set<ID> setCreatedByUserIDs = new Set<ID>();
        lstSalesGoalWithNoCollegue.clear();
        setCreatedByUserIDs.clear();
        lstSalesGoalToUpdateOwner.clear();
        
        try
        {
            //iterate over the trigger.new
            for(Individual_Sales_Goal__c salesGoal : triggerNew)
            {
                //add to the set
                if(salesGoal.Colleague__c != Null)
                {
                    System.debug('@@@@ salesGoal.Colleague__c @@@@'+salesGoal.Colleague__c);
                    ownerIds.add(salesGoal.Colleague__c);
                    lstSalesGoalToUpdateOwner.add(salesGoal); 
                }
                else //If-Collegue is Not Specified.
                {
                    lstSalesGoalWithNoCollegue.add(salesGoal);
                    setCreatedByUserIDs.add(salesGoal.OwnerId);
                } 
                
                
                
         }
             //Setting the Owner For Those Sales Goal who are created Directly through UI
                if(!setCreatedByUserIDs.isEmpty())
                {
                    List<User> lstUser = [select id,EmployeeNumber from User Where ID IN:setCreatedByUserIDs];
                    Set<String> setEmpNo = new Set<String>();
                    Map<ID,String> userMap = new Map<ID,String>();
                    Map<String,Colleague__c> collMap = new Map<String,Colleague__c>();
                    
                    setEmpNo.clear();
                    userMap.clear();
                    For(User u :lstUser)
                    {
                        if(u.EmployeeNumber != '' && u.EmployeeNumber != Null)
                        {
                        setEmpNo.add(u.EmployeeNumber);
                        userMap.put(u.Id,u.EmployeeNumber);
                        }
                    }
                    List<Colleague__c> lstCollegue = [Select ID,EMPLID__c From Colleague__c where EMPLID__c IN:setEmpNo];
                    For(Colleague__c col : lstCollegue)
                    {
                            if(col.EMPLID__c != Null)
                            collMap.put(col.EMPLID__c,col);
                    }

                    For(Individual_Sales_Goal__c isg : lstSalesGoalWithNoCollegue)
                    {
                        if(userMap.get(isg.OwnerId)!= Null)
                        {
                        isg.Colleague__c = collMap.get(userMap.get(isg.OwnerId)).Id;
                        }
                       // ownerIdSalesGoalMap.put(isg.Colleague__c, isg);
                    }
                    for(Individual_Sales_Goal__c salesGoal : [Select Id, Colleague__c,CreatedByID,OwnerId From Individual_Sales_Goal__c Where OwnerId IN: setCreatedByUserIDs])          
                    {
                    //add to the map
                        ownerIdSalesGoalMapForNoCollegue.put(salesGoal.OwnerId, salesGoal);      
                    }
                    if(!ownerIdSalesGoalMapForNoCollegue.isEmpty())
                    {
                    //iterate over the trigger.new
                    for(Individual_Sales_Goal__c salesGoal : triggerNew)
                    {
                        //check the map
                        if(ownerIdSalesGoalMapForNoCollegue.containskey(salesGoal.OwnerId) && salesGoal.OwnerId<>Label.MercerForceUserid)
                        {
                            //display error
                            salesGoal.adderror(system.Label.CL45_SalesGoalErrMsg);  
                        }
                    }   
                    }
                }
                
            if(!ownerIds.isEmpty())
            {
                
                //iterate over the sales goal 
              for(Individual_Sales_Goal__c salesGoal : [Select Id, Colleague__c,CreatedByID,OwnerId From Individual_Sales_Goal__c Where Colleague__c IN: ownerIds])          
                {
                    //add to the map
                    ownerIdSalesGoalMap.put(salesGoal.Colleague__c, salesGoal);      
                }
               
                if(!ownerIdSalesGoalMap.isEmpty())
                {
                    //iterate over the trigger.new
                    for(Individual_Sales_Goal__c salesGoal : triggerNew)
                    {
                        //check the map
                        if(ownerIdSalesGoalMap.containskey(salesGoal.Colleague__c))
                        {
                            //display error
                            salesGoal.adderror(system.Label.CL45_SalesGoalErrMsg);  
                        }
                    }   
                }
            }   
           
            setOwner(lstSalesGoalToUpdateOwner);
            
        }   
        catch(Exception ex)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalValidationInsert with reason :'+ex.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        } 
        
        
    }
    
    /*
     * @Description : This method is called on after updation of Sales Goal records
     * @ Args       : Trigger.newmap, Trigger.Oldmap
     * @ Return     : void
     */
   public static void salesGoalValidationUpdate (Map<Id, Individual_Sales_Goal__c> triggerNewMap, Map<Id, Individual_Sales_Goal__c> triggerOldMap)
    {
        // clear cache
        ownerIds.clear();       
        //map variable to store owner id and sales goal
        Map<String, Individual_Sales_Goal__c> ownerIdSalesGoalMap = new Map<String, Individual_Sales_Goal__c>();
        List<Individual_Sales_Goal__c> lstSalesGoalOwnerToBeUpdated = New List<Individual_Sales_Goal__c>();
        try
        {
             //iterate over trigger.newmap
             for(Individual_Sales_Goal__c salesGoal : triggerNewMap.values())
            {
                if(salesGoal.Colleague__c <> triggerOldMap.get(salesGoal.Id).Colleague__c)
                { 
                    ownerIds.add(salesGoal.Colleague__c); 
                    lstSalesGoalOwnerToBeUpdated.add(salesGoal);
                }  
            }
            
            if(!ownerIds.isEmpty())
            {
                //iterate over sales goal
                for(Individual_Sales_Goal__c salesGoal : [Select Id, Colleague__c,OwnerId From Individual_Sales_Goal__c Where Colleague__c IN: ownerIds])
                {
                    //add to the map
                    ownerIdSalesGoalMap.put(salesGoal.Colleague__c, salesGoal);  
                }
                
                if(!ownerIdSalesGoalMap.isEmpty())
                {
                    //iterate over trigger.newmap
                    for(Individual_Sales_Goal__c salesGoal : triggerNewMap.values())
                    {
                        //check the map
                        if(ownerIdSalesGoalMap.containskey(salesGoal.Colleague__c))
                        {
                            //display error
                            salesGoal.adderror(system.Label.CL45_SalesGoalErrMsg);  
                        }
                    }   
                }
            } 
            setOwner(lstSalesGoalOwnerToBeUpdated);              
        }catch(Exception ex)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalValidationUpdate with reason :'+ex.getMessage());
            triggernewMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        }
       
    }
    
    /*
     * @Description : This method is called on after insertion of Sales Goal records
     * @ Args       : Trigger.new
     * @ Return     : void
     */
    
    public static void salesGoalAterInsert (List<Individual_Sales_Goal__c> triggerNew)
    {
        
        //clear cache
        ownerIdSalesGoalIdMap.clear();
        empIdUserIdMap.clear();
        salesCreditLineItemInsert.clear();
        // Added as per Request #7445 Dec Release 2015 
        List<Individual_Sales_Goal__c> lstIndividUserGoals = new List<Individual_Sales_Goal__c>();
        try
        {
            //ierate over trigger.new
            for(Individual_Sales_Goal__c individualSalesGoal: triggerNew)
            {
                //add to map
                ownerIdSalesGoalIdMap.put(individualSalesGoal.Colleague__c, individualSalesGoal.Id);//Modified By Gyan As Per Req-3477                
                  
                // Added as per Request #7445 Dec Release 2015           
                lstIndividUserGoals.add(individualSalesGoal);
                  
            }
           
            //iterate over sales credit
            for(Sales_Credit__c salesCredit : [select Id, Name,EmpName__c,Opportunity_Stage__c from Sales_Credit__c where EmpName__c IN:ownerIdSalesGoalIdMap.Keyset() AND Opportunity_Type__c != :system.Label.CL57_OpportunityType])
            {
                if(salesCredit.EmpName__c <> Null)
                {
                    //create new sales credit line item
                    Sales_Credit_Line_Item__c salesCreditLineItem = new Sales_Credit_Line_Item__c();
                    salesCreditLineItem.Name = salesCredit.Name;
                    salesCreditLineItem.Individual_Sales_Goal__c = ownerIdSalesGoalIdMap.get(salesCredit.EmpName__c);
                    salesCreditLineItem.Sales_Credit_lookup__c = salesCredit.Id;
                    salesCreditLineItemInsert.add(salesCreditLineItem);
                }
            }
           
            system.debug('size***'+salesCreditLineItemInsert.size() + Limits.getDMLRows()+Limits.getLimitDMLRows());
            if (salesCreditLineItemInsert.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
            if(!salesCreditLineItemInsert.isEmpty())insert salesCreditLineItemInsert;   
            
            // Added as per Request #7445 Dec Release 2015
            updateIndividualGoals(lstIndividUserGoals);
        }
        catch(TriggerException tEx)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalAterInsert with reason :'+tEx.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
        
        }catch(Exception ex)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalAterInsert with reason :'+ex.getMessage());
            triggernew[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
        
        }
    }
    
    /*
     * @Description : This method is called on after updation of Sales Goal records
     * @ Args       : Trigger.newmap, Trigger.OldMap
     * @ Return     : void
     */
    public static void salesGoalAterUpdate (Map<Id, Individual_Sales_Goal__c> triggerNewMap, Map<Id, Individual_Sales_Goal__c> triggerOldMap)
    {
        
        //clear cache
        salesCreditLineItemUpdate.clear();
        salesCreditLineItemDelete.clear();
        ownerIdSalesGoalIdMap.clear();
        // Added as per Request #7445 Dec Release 2015
        List<Individual_Sales_Goal__c> lstIndividUserGoals = new List<Individual_Sales_Goal__c>();
        
        try
        {
            //iterate over trigger.newmap
            for(Individual_Sales_Goal__c individualSalesGoal: triggerNewMap.values())
            {
                //if owner is changed
                if(triggerNewMap.get(individualSalesGoal.Id).Colleague__c <> triggerOldMap.get(individualSalesGoal.Id).Colleague__c)
                {
                    //add to map
                    ownerIdSalesGoalIdMap.put(individualSalesGoal.Colleague__c, individualSalesGoal.Id);//Updated By gyan As per 3477
                }
                // Added as per Request #7445 Dec Release 2015
                 if((triggerNewMap.get(individualSalesGoal.Id).Sales_Goals__c <> triggerOldMap.get(individualSalesGoal.Id).Sales_Goals__c) || (triggerNewMap.get(individualSalesGoal.Id).Sales_Goal_CY__c <> triggerOldMap.get(individualSalesGoal.Id).Sales_Goal_CY__c) || (triggerNewMap.get(individualSalesGoal.Id).Revenue_Growth_Target__c <> triggerOldMap.get(individualSalesGoal.Id).Revenue_Growth_Target__c) || (triggerNewMap.get(individualSalesGoal.Id).Sales_Goal_TOR_MMX__c <> triggerOldMap.get(individualSalesGoal.Id).Sales_Goal_TOR_MMX__c) )                
                {
                    lstIndividUserGoals.add(individualSalesGoal);
                }
                // End of Request #7445 Dec Release 2015
                
            }
            //iterate over sales credit line item
            for(Sales_Credit_Line_Item__c salesCreditLineItem: [select Id, Name, Individual_Sales_Goal__c from Sales_Credit_Line_Item__c where Individual_Sales_Goal__c IN :ownerIdSalesGoalIdMap.values()])
            {
                //add to list 
                salesCreditLineItemDelete.add(salesCreditLineItem);
            }
            
            //delete sales credit line item
            if(!salesCreditLineItemDelete.isEmpty())delete salesCreditLineItemDelete;
            
            //iterate over sales credit
            for(Sales_Credit__c salesCredit : [select Id, Name, EmpName__c, Opportunity_Stage__c from Sales_Credit__c where EmpName__c IN:ownerIdSalesGoalIdMap.Keyset() AND Opportunity_Type__c != :system.Label.CL57_OpportunityType])
            {
                if(salesCredit.EmpName__c <> Null)
                {
                    //create sales credit line item
                    Sales_Credit_Line_Item__c salesCreditLineItem = new Sales_Credit_Line_Item__c();
                    salesCreditLineItem.Name = salesCredit.Name;
                    salesCreditLineItem.Individual_Sales_Goal__c = ownerIdSalesGoalIdMap.get(salesCredit.EmpName__c);
                    salesCreditLineItem.Sales_Credit_lookup__c = salesCredit.Id;
                    salesCreditLineItemUpdate.add(salesCreditLineItem);
                }
            }
    
            if (salesCreditLineItemUpdate.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
            if(!salesCreditLineItemUpdate.isEmpty())insert salesCreditLineItemUpdate;   
            // Added as per Request #7445 Dec Release 2015
            updateIndividualGoals(lstIndividUserGoals);
            
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalAterUpdate with reason :'+tEx.getMessage());
            triggernewMap.values()[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
        
        }catch(Exception ex)
        {
            System.debug('Exception occured at Individual Sales Goal Trigger salesGoalAterUpdate with reason :'+ex.getMessage());
            triggernewMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
          
        }
    }
    
     /*
     * @Description : This method is called on after insertion/updation of Sales Goal records Which 
                       sets the owner of Individual Sales Goal Record.
                       • If colleague is a user, he/she will be the goal owner
                       • If colleague is not a user, the goal owner is MercerForce

     * @ Args       : List<Individual_Sales_Goal__c>
     * @ Return     : void
     */
    public static void setOwner(List<Individual_Sales_Goal__c> lstSalesGoal)
     {
            Set<String> setCollegueID = new Set<String>();
            Map<ID,String> collMap = new Map<ID,String>();
            Map<String,User> userMap = new Map<String,User>();

    
            for(Individual_Sales_Goal__c salesGoal : lstSalesGoal)
            {
                //add to the set
                setCollegueID.add(salesGoal.Colleague__c);    
            }          
            List<Colleague__c> lstCollegue = [Select Id,EMPLID__c From Colleague__c where ID IN : setCollegueID];
            For(Colleague__c col : lstCollegue)
            {
                if(col.EMPLID__c != Null)
                collMap.put(col.Id,col.EMPLID__c);
            }
            List<User> lstUser = [select EmployeeNumber,IsActive from User where EmployeeNumber IN:collMap.values()];
            For(User u : lstUser)
            {
                userMap.put(u.EmployeeNumber,u);
            }
            for(Individual_Sales_Goal__c individualSalesGoal: lstSalesGoal)
            {
                   
                            String empIDForCollegue = collMap.get(individualSalesGoal.Colleague__c);
                            if(userMap.containsKey(empIDForCollegue)== True && userMap.get(empIDForCollegue).isActive == True)// To Check if Collegue has corrresponding Active User in System
                            {
                                //If Collegue has Active User in System
                                individualSalesGoal.OwnerId = userMap.get(empIDForCollegue).Id;
                            }
                            else
                            {
                                  // if Collegue has Not corrresponding Active User in System and Owner ID is not specified During Load.
                                    individualSalesGoal.OwnerId = System.Label.MercerForceUserid;
                            } 
                        
                   
            }
        }
       
    /*
     * @Description : As per Request #7445 Dec Release 2015, this method updates Goals field on User object on the basis of individual sales goal
     * @ Args       : List<Individual_Sales_Goal__c> lstSalesGoal
     * @ Return     : void
     */  
      public static void updateIndividualGoals(List<Individual_Sales_Goal__c> lstSalesGoal)
     {
         system.debug('**** Inside updategoals');
        Set<ID> colleagueIDSet = new Set<ID>(); 
        Map<ID,String> colleagueMap = new Map<ID,String>();
        Map<String,User> userMap = new Map<String,User>();
        List<User> userGoalUpdateList = new List<User>(); 
        
        for(Individual_Sales_Goal__c sgoal : lstSalesGoal)
        {
            colleagueIDSet.add(sgoal.Colleague__c);
        }
        List<Colleague__c> lstColleague = [Select Id,EMPLID__c From Colleague__c where ID IN : colleagueIDSet];
            For(Colleague__c col : lstColleague)
            {
                if(col.EMPLID__c != Null)
                colleagueMap.put(col.Id,col.EMPLID__c);
            }
            List<User> lstUser = [select EmployeeNumber,IsActive,goals__c from User where EmployeeNumber IN:colleagueMap.values()];
            if(!lstUser.isEmpty())
            { 
            For(User u : lstUser)
            {
                userMap.put(u.EmployeeNumber,u);
            }           
             for(Individual_Sales_Goal__c indiSalesGoal: lstSalesGoal)
            {        
                    User usr;       
                    String collEmpID = colleagueMap.get(indiSalesGoal.Colleague__c);
                    if(userMap.containsKey(collEmpID))
                    {
                        usr = userMap.get(collEmpID);
                                            
                    
         //   As per Request #10298 May Release 2016 conditions are modified
               system.debug('**** before if cond..');
                if(((indiSalesGoal.Sales_Goals__c !=null && indiSalesGoal.Sales_Goals__c > 0) || (indiSalesGoal.Sales_Goal_CY__c !=null && indiSalesGoal.Sales_Goal_CY__c > 0)) && ((indiSalesGoal.GRM_REV_TARGET_AMT__c == null || indiSalesGoal.GRM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.IRM_REV_TARGET_AMT__c == null || indiSalesGoal.IRM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.PGM_REV_TARGET_AMT__c == null || indiSalesGoal.PGM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.PM_REV_TARGET_AMT__c == null || indiSalesGoal.PM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.RM_REV_TARGET_AMT__c == null || indiSalesGoal.RM_REV_TARGET_AMT__c == 0)))
                {
                    System.debug('I am in condition 1');
                    usr.goals__c = 'Sales';
                    System.debug('The value of goals in condition 1: '+usr.goals__c);
                }
                else if(((indiSalesGoal.Sales_Goals__c !=null && indiSalesGoal.Sales_Goals__c > 0) || (indiSalesGoal.Sales_Goal_CY__c !=null && indiSalesGoal.Sales_Goal_CY__c > 0)) && (indiSalesGoal.GRM_REV_TARGET_AMT__c >0 || indiSalesGoal.IRM_REV_TARGET_AMT__c >0 || indiSalesGoal.PGM_REV_TARGET_AMT__c >0 || indiSalesGoal.PM_REV_TARGET_AMT__c >0 || indiSalesGoal.RM_REV_TARGET_AMT__c >0))
                {
                    System.debug('I am in condition 2');
                    usr.goals__c = 'Sales & Revenue';
                }
                else if(((indiSalesGoal.Sales_Goals__c == null || indiSalesGoal.Sales_Goals__c == 0) || (indiSalesGoal.Sales_Goal_CY__c == null || indiSalesGoal.Sales_Goal_CY__c == 0)) && ((indiSalesGoal.GRM_REV_TARGET_AMT__c !=null && indiSalesGoal.GRM_REV_TARGET_AMT__c >0) || (indiSalesGoal.IRM_REV_TARGET_AMT__c !=null && indiSalesGoal.IRM_REV_TARGET_AMT__c >0) || (indiSalesGoal.PGM_REV_TARGET_AMT__c !=null && indiSalesGoal.PGM_REV_TARGET_AMT__c >0) || (indiSalesGoal.PM_REV_TARGET_AMT__c !=null && indiSalesGoal.PM_REV_TARGET_AMT__c >0) || (indiSalesGoal.RM_REV_TARGET_AMT__c !=null && indiSalesGoal.RM_REV_TARGET_AMT__c >0)))
                {
                    System.debug('I am in condition 3');
                    usr.goals__c = 'Revenue';
                }
                else if(((indiSalesGoal.Sales_Goals__c == null || indiSalesGoal.Sales_Goals__c == 0) || (indiSalesGoal.Sales_Goal_CY__c == null || indiSalesGoal.Sales_Goal_CY__c == 0)) && ((indiSalesGoal.GRM_REV_TARGET_AMT__c == null || indiSalesGoal.GRM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.IRM_REV_TARGET_AMT__c == null || indiSalesGoal.IRM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.PGM_REV_TARGET_AMT__c == null || indiSalesGoal.PGM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.PM_REV_TARGET_AMT__c == null || indiSalesGoal.PM_REV_TARGET_AMT__c == 0) && (indiSalesGoal.RM_REV_TARGET_AMT__c == null || indiSalesGoal.RM_REV_TARGET_AMT__c == 0)))
                {
                    System.debug('I am in condition 4');
                    usr.goals__c = '';
                }
                 userGoalUpdateList.add(usr);
                 }
            }
            }
             if(!userGoalUpdateList.isEmpty()) 
             {
                update userGoalUpdateList;
             }   
             else{}
             
     }
      /*
     * @Description : As per Request #10298 May Release 2016, this method updates Goals field on User object when individual sales goal is deleted.
     * @ Args       : List<Individual_Sales_Goal__c> lstSalesGoal
     * @ Return     : void
     */ 
     public static void deleteIndividualGoal(List<Individual_Sales_Goal__c> lstSalesGoal)
{
  Set<ID> colleagueIDSet = new Set<ID>();
  Map<ID,String> colleagueMap = new Map<ID,String>();
  Map<String,User> userMap = new Map<String,User>();
  List<User> userGoalUpdateList = new List<User>();
   for(Individual_Sales_Goal__c sg: lstSalesGoal)
   {
   colleagueIDSet.add(sg.Colleague__c);
   }
   List<Colleague__c> lstColleague = [Select ID, EMPLID__c From Colleague__c where ID IN : colleagueIDSet];
   for(Colleague__c col : lstColleague)
   {
     if(col.EMPLID__c != Null)
     colleagueMap.put(col.Id,col.EMPLID__c);
   }
  List<User> lstUser = [select EmployeeNumber,IsActive,goals__c from User where EmployeeNumber IN:colleagueMap.values()];
   if(!lstUser.isEmpty())
   {
   for(User u : lstUser)
            {
                userMap.put(u.EmployeeNumber,u);
            }          
   for(Individual_Sales_Goal__c indiSalesGoal: lstSalesGoal)
        {        
                    User usr;       
                    String collEmpID = colleagueMap.get(indiSalesGoal.Colleague__c);
                    if(userMap.containsKey(collEmpID))
                    {
                        usr = userMap.get(collEmpID);
                    usr.Goals__c = '';                  
                   userGoalUpdateList.add(usr);                     
                    }
                        
        }
                if(!userGoalUpdateList.isEmpty()) {
                update userGoalUpdateList;
             } 
             }  
}
}