/*Handler Class for SensitveAtRisk Object*/
global class SensitveAtRiskTriggerHandler implements ITriggerHandler{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    public Boolean IsDisabled()
    {
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('SensitveAtRisk Trigger');
        
        if (TrgSetting.Trigger_Disabled__c== true) {
            return true;
        }else{
            return TriggerDisabled;
        }
    } 
    
    public void BeforeInsert(List<SObject> newItems) {}
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void BeforeDelete(Map<Id, SObject> newItemsMap) {
        delete [select Id,At_Risk_Type__c ,Why_at_Risk__c,LOB_Status__c,LOB__c,Sensitive_At_Risk__c from Client_Relationship__c where Sensitive_At_Risk__c IN: newItemsMap.keyset()];
    }
    
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemsMap) {
        database.executeBatch(new BATCH_OneDirectionalSensitiveDataSync(newItemsMap.keyset()),2000);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap) {
        database.executeBatch(new BATCH_OneDirectionalSensitiveDataSync(newItemsMap.keyset()),2000);
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}