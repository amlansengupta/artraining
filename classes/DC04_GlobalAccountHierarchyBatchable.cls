/*Purpose: This Apex class implements batchable interface and update those accounts  Account records.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Srishty     05/09/2013  Created Apex Batch class
   2.0 -    Sarbpreet   7/7/2014    Added Comments.
============================================================================================================================================== 
*/
global class DC04_GlobalAccountHierarchyBatchable implements Database.Batchable<sObject>{ 
    
    global static String accQuery = 'Select Id,GU_DUNS__c,DUNS__c,One_Code_Status__c, Domestic_DUNS__c, HQ_Immediate_Parent_DUNS__c, parentId FROM Account where one_code_status__c <>\'Inactive\'' ;
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
  
   

 /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
  global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('\n constructed query:'+accQuery);
        //execute the query and fetch the records  
        return Database.getQueryLocator(accQuery);
    
    }
  
   
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {    
        
         Map<String, String> dunsNoAccountIdMap = new Map<String, String>();
         Set<String> dunsNoSet = new Set<String>();
         //list to hold the account      
         List<Account> accountList = new List<Account>();
         
         for(Account account : (List<Account>)scope)
         {
                if((account.One_Code_Status__c <> null && account.One_Code_Status__c <> 'Inactive') || account.One_Code_Status__c == null)
                {
                    if(account.Domestic_DUNS__c <> null) 
                        dunsNoSet.add(account.Domestic_DUNS__c);
                    if(account.HQ_Immediate_Parent_DUNS__c <> null) 
                        dunsNoSet.add(account.HQ_Immediate_Parent_DUNS__c);
                    if(account.GU_DUNS__c <> null) 
                        dunsNoSet.add(account.GU_DUNS__c); 
                } 
        }
        
        // if set not empty
        if(!dunsNoSet.isEmpty()){ 
            // iterate through soql query on Accounts object
            for(Account acct : [Select Id,GU_DUNS__c,DUNS__c  FROM Account WHERE  DUNS__c in : dunsNoSet and One_Code_Status__c in ('Active', 'Pending', 'Pending - Workflow Wizard')]){                   
                dunsNoAccountIdMap.put(acct.DUNS__c,acct.Id);
            }
        }
        
        for(Account account : (List<Account>)scope){        
            String prevParent = account.ParentId;
                if(dunsNoAccountIdMap.size()>0){
                    account = updateParentwithDuns(dunsNoAccountIdMap, account, account.HQ_Immediate_Parent_DUNS__c );
                    if(account.parentid == null)
                        account = updateParentwithDuns(dunsNoAccountIdMap, account, account.Domestic_DUNS__c);
                    if(account.parentid == null)
                        account = updateParentwithDuns(dunsNoAccountIdMap, account, account.GU_DUNS__c);
                    if(account.parentid <> prevParent)
                        accountList.add(account);
                }
            
            
         }
           
           system.debug('accountList to update:'+accountList);
         //update the list of accounts
        if(!accountList.isEmpty())
        {
             for(Database.Saveresult result : Database.update(accountList, false))
             {
                 // To Do Error logging
                if(!result.isSuccess())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }   
            }   
        }
        
        if(!errorLogs.isEmpty()) Database.insert(errorLogs, false);
        
        
  }
  /*
   * @Description : Method for updating Parent Account with Duns
   * @ Args       : Null
   * @ Return     : account
   */
    public account updateParentwithDuns(Map<String,String> dunsNoMap, Account acc, String dunsNo){
    
        if(dunsNo<>null && dunsNo<>acc.DUNS__c && dunsNoMap.get(dunsNo) <> null)
            {
                // check for circular dependency
                if(acc.Id <> dunsNoMap.get(dunsNo)){
                    acc.parentId = dunsNoMap.get(dunsNo);
                    
                }
            }
        return acc;

    }
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */    
    global void finish(Database.BatchableContext BC)
    {
    
    }
}