/*Purpose: Test Class for providing code coverage to Distribution Trigger 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   03/29/2013  Created test class
============================================================================================================================================== 
*/
@istest
private class Test_DistributionTrigger
{
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 1)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */ 
    private static testmethod void testDistribution1() 
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1)
        {
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Distribution';
        distribution.Scope__c = 'Office';        
        distribution.Office__c = 'New Delhi';        
        insert distribution; 
        
        Distribution__c distest = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(distest.Office__c, 'New Delhi');
        System.assert(distest.Market__c <> null);
        System.assert(distest.Region__c <> null);
        System.assert(distest.Sub_Market__c <> null);
        System.assert(distest.Sub_Region__c <> null);
        System.assert(distest.Country__c <> null);
    
        distribution.Scope__c = 'Region';
        distribution.Region__c ='EuroPac';        
        update Distribution; 
        Distribution__c distest1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(distest1.Office__c,'All' );
        System.assertEquals(distest1.Region__c, 'EuroPac');
        System.assertEquals(distest1.Sub_Region__c,'All');
        System.assertEquals(distest1.Sub_Market__c,'All');
        System.assertEquals(distest1.Market__c,'All');
        System.assertEquals(distest1.Country__c,'All');

       
        Distribution.scope__c = 'Market*';        
        Distribution.Sub_Market__c = 'India'; 
        update Distribution;
        Distribution__c distest2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(distest2.Office__c,'All' );
        System.assert(distest2.Region__c <> null);
        System.assert(distest2.Market__c <> null);
        System.assert(distest2.Country__c <> null);
        System.assert(distest2.Sub_Market__c <> null);
        System.assert(distest2.Sub_Region__c <> null);
        
        Distribution.scope__c = 'Market';
        Distribution.Market__c = 'Ireland';
        update Distribution;
        Distribution__c distest3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(distest3.Office__c ,'All');
        System.assertEquals(distest3.Sub_Market__c ,'All');
        //System.assert(distest3.Sub_Region__c <> null);
        //System.assert(distest3.Region__c <> null);
        //System.assertEquals(distest3.Country__c,'All');
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c distest4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(distest4.Region__c <> null);
        System.assertEquals(distest4.Office__c ,'All');
        System.assertEquals(distest4.Sub_Market__c ,'All');
        System.assertEquals(distest4.Country__c,'All');
        System.assertEquals(distest4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Country';       
        Distribution.country__c = 'Japan';  
        update Distribution; 
        Distribution__c distest5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(distest5.Sub_Market__c ,'All');
        System.assertEquals(distest5.Office__c ,'All');
        System.assert(distest5.Region__c <> null);
        System.assert(distest5.Sub_Region__c <> null);
        System.assert(distest5.Market__c <> null);
        
        Distribution.scope__c = 'Global'; 
        update Distribution; 
        Distribution__c distest6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(distest6.Sub_Market__c ,'All');
        System.assertEquals(distest6.Office__c ,'All');
        System.assertEquals(distest6.Region__c ,'All');
        System.assertEquals(distest6.Sub_Region__c ,'All');
        System.assertEquals(distest6.Market__c ,'All');
        System.assertEquals(distest6.Country__c,'All');
        }
        }
    
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 2)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution2() 
    {   
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){   
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Region';
        distribution.Scope__c = 'Region';        
        distribution.Region__c = 'EuroPac';        
        insert distribution; 
        Distribution__c istest = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(istest.Region__c, 'EuroPac');
        System.assertEquals(istest.Office__c,'All' );
        System.assertEquals(istest.Sub_Region__c,'All');
        System.assertEquals(istest.Sub_Market__c,'All');
        System.assertEquals(istest.Market__c,'All');
        System.assertEquals(istest.Country__c,'All');

            
        distribution.Scope__c = 'Office';
        distribution.Office__c ='New Delhi';        
        update Distribution; 
        Distribution__c istest1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(istest1.Office__c, 'New Delhi');
        System.assert(istest1.Market__c<> null );
        System.assert(istest1.Region__c <> null);
        System.assert(istest1.Sub_Market__c <> null);
        System.assert(istest1.Sub_Region__c <> null);
        System.assert(istest1.Country__c <> null);

               
        Distribution.scope__c = 'Market*';        
        Distribution.Sub_Market__c = 'India'; 
        update Distribution;
        Distribution__c istest2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];        
        System.assertEquals(istest2.Office__c,'All' );
        System.assert(istest2.Region__c <> null);
        System.assert(istest2.Market__c <> null);
        System.assert(istest2.Country__c <> null);
        System.assert(istest2.Sub_Market__c <> null);
        System.assert(istest2.Sub_Region__c <> null);
        
        Distribution.scope__c = 'Market';
        Distribution.Market__c = 'Ireland';
        update Distribution;
        Distribution__c istest3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];        
        System.assertEquals(istest3.Office__c ,'All');
        System.assertEquals(istest3.Sub_Market__c ,'All');
        //System.assert(istest3.Sub_Region__c <> null);
       // System.assert(istest3.Region__c <> null);
        //System.assertEquals(istest3.Country__c,'All');
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c istest4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(istest4.Region__c <> null);
        System.assertEquals(istest4.Office__c ,'All');
        System.assertEquals(istest4.Sub_Market__c ,'All');
        System.assertEquals(istest4.Country__c,'All');
        System.assertEquals(istest4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Country';       
        Distribution.country__c = 'Japan';  
        update Distribution; 
        Distribution__c istest5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(istest5.Sub_Market__c ,'All');
        System.assertEquals(istest5.Office__c ,'All');
        System.assert(istest5.Region__c <> null);
        System.assert(istest5.Sub_Region__c <> null);
        System.assert(istest5.Market__c <> null);
        
        Distribution.scope__c = 'Global'; 
        update Distribution; 
        Distribution__c istest6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(istest6.Sub_Market__c ,'All');
        System.assertEquals(istest6.Office__c ,'All');
        System.assertEquals(istest6.Region__c ,'All');
        System.assertEquals(istest6.Sub_Region__c ,'All');
        System.assertEquals(istest6.Market__c ,'All');
        System.assertEquals(istest6.Country__c,'All');
        }
    }
    
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 3)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */ 
    private static testmethod void testDistribution3() 
    {   User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){  
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Market';
        distribution.Scope__c = 'Market';        
        distribution.Market__c = 'Ireland';        
        insert distribution; 
        Distribution__c test = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(test.Office__c ,'All');
        System.assertEquals(test.Sub_Market__c ,'All');
        //System.assert(test.Sub_Region__c <> null);
       // System.assert(test.Region__c <> null);
        //System.assertEquals(test.Country__c,'All');
    
        distribution.Scope__c = 'Region';
        distribution.Region__c ='EuroPac';        
        update Distribution; 
        Distribution__c test1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(test1.Office__c,'All' );
        System.assertEquals(test1.Region__c, 'EuroPac');
        System.assertEquals(test1.Sub_Region__c,'All');
        System.assertEquals(test1.Sub_Market__c,'All');
        System.assertEquals(test1.Market__c,'All');
        System.assertEquals(test1.Country__c,'All');

       
        Distribution.scope__c = 'Sub-Market';        
        Distribution.Sub_Market__c = 'India'; 
        update Distribution;
        Distribution__c test2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(test2.Office__c,'All' );
        System.assert(test2.Region__c <> null);
        System.assert(test2.Market__c <> null);
        System.assert(test2.Country__c <> null);
        System.assert(test2.Sub_Market__c <> null);
        System.assert(test2.Sub_Region__c <> null);
        
        Distribution.scope__c = 'Office';
        Distribution.Office__c = 'New Delhi';
        update Distribution;
        Distribution__c test3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(test3.Office__c, 'New Delhi');
        System.assert(test3.Market__c <> null);
        System.assert(test3.Region__c <> null);
        System.assert(test3.Sub_Market__c <> null);
        System.assert(test3.Sub_Region__c <> null);
        System.assert(test3.Country__c <> null);
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c test4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(test4.Region__c <> null);
        System.assertEquals(test4.Office__c ,'All');
        System.assertEquals(test4.Sub_Market__c ,'All');
        System.assertEquals(test4.Country__c,'All');
        System.assertEquals(test4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Country';       
        Distribution.country__c = 'Japan';  
        update Distribution; 
        Distribution__c test5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(test5.Sub_Market__c ,'All');
        System.assertEquals(test5.Office__c ,'All');
        System.assert(test5.Region__c <> null);
        System.assert(test5.Sub_Region__c <> null);
        System.assert(test5.Market__c <> null);
        
        Distribution.scope__c = 'Global'; 
        update Distribution; 
        Distribution__c test6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(test6.Sub_Market__c ,'All');
        System.assertEquals(test6.Office__c ,'All');
        System.assertEquals(test6.Region__c ,'All');
        System.assertEquals(test6.Sub_Region__c ,'All');
        System.assertEquals(test6.Market__c ,'All');
        System.assertEquals(test6.Country__c,'All');
        }
    }
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 4)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution4() 
    {   
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Sub-Market';
        distribution.Scope__c = 'Market*';        
        distribution.Sub_Market__c = 'India';        
        insert distribution; 
        Distribution__c Dtest2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Dtest2.Office__c,'All' );
        System.assert(Dtest2.Region__c <> null);
        System.assert(Dtest2.Market__c <> null);
        System.assert(Dtest2.Country__c <> null);
        System.assert(Dtest2.Sub_Market__c <> null);
        System.assert(Dtest2.Sub_Region__c <> null);
    
        distribution.Scope__c = 'Region';
        distribution.Region__c ='EuroPac';        
        update Distribution; 
        Distribution__c Dtest1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Dtest1.Office__c,'All' );
        System.assertEquals(Dtest1.Region__c, 'EuroPac');
        System.assertEquals(Dtest1.Sub_Region__c,'All');
        System.assertEquals(Dtest1.Sub_Market__c,'All');
        System.assertEquals(Dtest1.Market__c,'All');
        System.assertEquals(Dtest1.Country__c,'All');

       
        Distribution.scope__c = 'Market';        
        Distribution.Market__c = 'Ireland'; 
        update Distribution;
        Distribution__c Dtest = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(Dtest.Office__c ,'All');
        System.assertEquals(Dtest.Sub_Market__c ,'All');
        //System.assert(Dtest.Sub_Region__c <> null);
       // System.assert(Dtest.Region__c <> null);
        //System.assertEquals(Dtest.Country__c,'All');
        
        Distribution.scope__c = 'Office';
        Distribution.Office__c = 'New Delhi';
        update Distribution;
        Distribution__c Dtest3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(Dtest3.Office__c, 'New Delhi');
        System.assert(Dtest3.Market__c <> null);
        System.assert(Dtest3.Region__c <> null);
        System.assert(Dtest3.Sub_Market__c <> null);
        System.assert(Dtest3.Sub_Region__c <> null);
        System.assert(Dtest3.Country__c <> null);
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c Dtest4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(Dtest4.Region__c <> null);
        System.assertEquals(Dtest4.Office__c ,'All');
        System.assertEquals(Dtest4.Sub_Market__c ,'All');
        System.assertEquals(Dtest4.Country__c,'All');
        System.assertEquals(Dtest4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Country';       
        Distribution.country__c = 'Japan';  
        update Distribution; 
        Distribution__c Dtest5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Dtest5.Sub_Market__c ,'All');
        System.assertEquals(Dtest5.Office__c ,'All');
        System.assert(Dtest5.Region__c <> null);
        System.assert(Dtest5.Sub_Region__c <> null);
        System.assert(Dtest5.Market__c <> null);
        
        Distribution.scope__c = 'Global'; 
        update Distribution; 
        Distribution__c Dtest6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Dtest6.Sub_Market__c ,'All');
        System.assertEquals(Dtest6.Office__c ,'All');
        System.assertEquals(Dtest6.Region__c ,'All');
        System.assertEquals(Dtest6.Sub_Region__c ,'All');
        System.assertEquals(Dtest6.Market__c ,'All');
        System.assertEquals(Dtest6.Country__c,'All');
        }
    }
    
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 5)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution5() 
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Country';
        distribution.Scope__c = 'Country';        
        distribution.Country__c = 'Japan';        
        insert distribution; 
        Distribution__c retest5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(retest5.Sub_Market__c ,'All');
        System.assertEquals(retest5.Office__c ,'All');
        System.assert(retest5.Region__c <> null);
        System.assert(retest5.Sub_Region__c <> null);
        System.assert(retest5.Market__c <> null);
        
        distribution.Scope__c = 'Region';
        distribution.Region__c ='EuroPac';        
        update Distribution; 
        Distribution__c retest1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(retest1.Office__c,'All' );
        System.assertEquals(retest1.Region__c, 'EuroPac');
        System.assertEquals(retest1.Sub_Region__c,'All');
        System.assertEquals(retest1.Sub_Market__c,'All');
        System.assertEquals(retest1.Market__c,'All');
        System.assertEquals(retest1.Country__c,'All');

       
        Distribution.scope__c = 'Sub-Market';        
        Distribution.Sub_Market__c = 'India'; 
        update Distribution;
        Distribution__c retest2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(retest2.Office__c,'All' );
        System.assert(retest2.Region__c <> null);
        System.assert(retest2.Market__c <> null);
        System.assert(retest2.Country__c <> null);
        System.assert(retest2.Sub_Market__c <> null);
        System.assert(retest2.Sub_Region__c <> null);
        
        Distribution.scope__c = 'Market';
        Distribution.Market__c = 'Ireland';
        update Distribution;
        Distribution__c retest3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(retest3.Office__c ,'All');
        System.assertEquals(retest3.Sub_Market__c ,'All');
        //System.assert(retest3.Sub_Region__c <> null);
        //System.assert(retest3.Region__c <> null);
        //System.assertEquals(retest3.Country__c,'All');
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c retest4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(retest4.Region__c <> null);
        System.assertEquals(retest4.Office__c ,'All');
        System.assertEquals(retest4.Sub_Market__c ,'All');
        System.assertEquals(retest4.Country__c,'All');
        System.assertEquals(retest4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Office';       
        Distribution.Office__c = 'New Delhi';  
        update Distribution; 
        Distribution__c retest = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(retest.Office__c, 'New Delhi');
        System.assert(retest.Market__c <> null);
        System.assert(retest.Region__c <> null);
        System.assert(retest.Sub_Market__c <> null);
        System.assert(retest.Sub_Region__c <> null);
        System.assert(retest.Country__c <> null);
    
        
        Distribution.scope__c = 'Global'; 
        update Distribution; 
        Distribution__c retest6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(retest6.Sub_Market__c ,'All');
        System.assertEquals(retest6.Office__c ,'All');
        System.assertEquals(retest6.Region__c ,'All');
        System.assertEquals(retest6.Sub_Region__c ,'All');
        System.assertEquals(retest6.Market__c ,'All');
        System.assertEquals(retest6.Country__c,'All');
        }
    }
    
    /* * @Description : This test method provides data coverage to Distribution trigger (Scenerio 6)          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution6() 
    {   
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){
        Mercer_TestData.buildMercerOffice();
        distribution__c distribution = new distribution__c();  
        distribution.Name = 'Test Global';
        Distribution.scope__c = 'Global'; 
        insert distribution; 
        Distribution__c Mtest6 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Mtest6.Sub_Market__c ,'All');
        System.assertEquals(Mtest6.Office__c ,'All');
        System.assertEquals(Mtest6.Region__c ,'All');
        System.assertEquals(Mtest6.Sub_Region__c ,'All');
        System.assertEquals(Mtest6.Market__c ,'All');
        System.assertEquals(Mtest6.Country__c,'All');
        
        distribution.Scope__c = 'Region';
        distribution.Region__c ='EuroPac';        
        update Distribution; 
        Distribution__c Mtest1 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Mtest1.Office__c,'All' );
        System.assertEquals(Mtest1.Region__c, 'EuroPac');
        System.assertEquals(Mtest1.Sub_Region__c,'All');
        System.assertEquals(Mtest1.Sub_Market__c,'All');
        System.assertEquals(Mtest1.Market__c,'All');
        System.assertEquals(Mtest1.Country__c,'All');

       
        Distribution.scope__c = 'Sub-Market';        
        Distribution.Sub_Market__c = 'India'; 
        update Distribution;
        Distribution__c Mtest2 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(Mtest2.Office__c,'All' );
        System.assert(Mtest2.Region__c <> null);
        System.assert(Mtest2.Market__c <> null);
        System.assert(Mtest2.Country__c <> null);
        System.assert(Mtest2.Sub_Market__c <> null);
        System.assert(Mtest2.Sub_Region__c <> null);
        
        Distribution.scope__c = 'Market';
        Distribution.Market__c = 'Ireland';
        update Distribution;
        Distribution__c Mtest3 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];       
        System.assertEquals(Mtest3.Office__c ,'All');
        System.assertEquals(Mtest3.Sub_Market__c ,'All');
        //System.assert(Mtest3.Sub_Region__c <> null);
        //System.assert(Mtest3.Region__c <> null);
        //System.assertEquals(Mtest3.Country__c,'All');
        
        
        Distribution.scope__c = 'Sub-Region';
        Distribution.Sub_Region__c = 'Europe';   
        update Distribution;   
        Distribution__c Mtest4 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assert(Mtest4.Region__c <> null);
        System.assertEquals(Mtest4.Office__c ,'All');
        System.assertEquals(Mtest4.Sub_Market__c ,'All');
        System.assertEquals(Mtest4.Country__c,'All');
        System.assertEquals(Mtest4.Market__c ,'All');
        
        
        Distribution.scope__c = 'Office';       
        Distribution.Office__c = 'New Delhi';  
        update Distribution; 
        Distribution__c Mtest = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Mtest.Office__c, 'New Delhi');
        System.assert(Mtest.Market__c <> null);
        System.assert(Mtest.Region__c <> null);
        System.assert(Mtest.Sub_Market__c <> null);
        System.assert(Mtest.Sub_Region__c <> null);
        System.assert(Mtest.Country__c <> null);
    
        
        distribution.Scope__c = 'Country'; 
        distribution.Country__c = 'Japan';      
        update Distribution; 
        Distribution__c Mtest5 = [Select Id, office__c, Market__c, Region__c,Sub_Market__c,Sub_Region__c,Country__c From Distribution__c where Id = : distribution.Id];
        System.assertEquals(Mtest5.Sub_Market__c ,'All');
        System.assertEquals(Mtest5.Office__c ,'All');
        System.assert(Mtest5.Region__c <> null);
        System.assert(Mtest5.Sub_Region__c <> null);
        System.assert(Mtest5.Market__c <> null);
        }
    }
    /* * @Description : This test method to validate if Country is entered          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution7() 
        {
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runas(User1){      
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name ='Test Country1';
            distribution.scope__c = 'Country';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter a Country'),e.getMessage() );
                
                
            }
            }
            }

    /* * @Description : This test method to validate if Market is entered          
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution8() 
        {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){       
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name = 'Test Market1';
            distribution.scope__c = 'Market';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter a Market'),e.getMessage() );
                
                
            }
            }
            }
 
    /* * @Description : This test method to validate if Region is entered          
       * @ Args       : no arguments        
       * @ Return     : void       
    */           
    private static testmethod void testDistribution9() 
        {
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runas(User1){
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name ='Test Region1';
            distribution.scope__c = 'Region';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter a Region'),e.getMessage() );
                
                
            }
            }
            } 
    /* * @Description : This test method to validate if Sub-Market is entered          
       * @ Args       : no arguments        
       * @ Return     : void       
    */  
    private static testmethod void testDistribution10() 
        {   
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runas(User1){
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name ='Test Sub-Market1';
            distribution.scope__c = 'Sub-Market';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter Sub-Market'),e.getMessage() );
                
                
            }
            }
            }   
    /* * @Description : This test method to validate if Sub-Region is entered         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testmethod void testDistribution11() 
        {
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runas(User1){
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name = 'Test Sub-Region1';
            distribution.scope__c = 'Sub-Region';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter Sub-Region'),e.getMessage() );
                
                
            }
            }
            } 

    /* * @Description : This test method to validate if Office is entered          
       * @ Args       : no arguments        
       * @ Return     : void       
    */              
    private static testmethod void testDistribution12() 
        {
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runas(User1){
            try
            {
            Mercer_TestData.buildMercerOffice();
            distribution__c distribution = new distribution__c();  
            distribution.Name =  'Test Office1';
            distribution.scope__c = 'Office';
             insert distribution; 
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter Office'),e.getMessage() );
                
                
            }
            }
            }           



 
}