/* =============================================================================================
Request Id : 17100 
This class is used as the controller class for MF2_CLientRevenueUSDComponent
Date : 22-MAR-2019 
Created By : Sankar Mitra
===============================================================================================*/

public class ClientRevenueController {
    
    @AuraEnabled
    public static Account fetchRevenueDetails(Id accId){
        try{
            String query = 'SELECT ';
            for(Schema.FieldSetMember fieldSetMember : getFields()) {
                query += fieldSetMember.getFieldPath() + ', ';
            }
            query += 'Id, Name FROM Account where Id=\''+String.escapeSingleQuotes(accId)+'\'';
            return Database.query(query);
        }catch(Exception ex){
            //system.debug(ex);
            ExceptionLogger.logException(ex, 'ClientRevenueController', 'fetchRevenueDetails');
            throw new AuraHandledException('Could not fetch revenue info! Please try after sometime.');
        }
    }
    private static List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.Client_Revenue.getFields();
    }
}