/*
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR         DATE                 DETAIL    
1.0         Arijit Roy     04/01/2013           Batch class for updating Account Status    
2.0         Jagan          05/22/14             Updated class to check if the status is not already same
============================================================================================================================================== 
*/
global class MS16_AccountNoActivityBatchable implements Database.Batchable<sObject>, Database.Stateful{
    global Date stDate = system.today() - 731;
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1',
        'Recent Win'                  => 'Recent Win',
        'High Revenue'                => 'High Revenue',
        'Low Revenue'                 => 'Low Revenue',
        'Active Marketing Contact'    => 'Active Marketing Contact',
        'Active Contact'              => 'Active Contact',
        'Activities Exists'           => 'Activities Exists',
        'Competitor'                  => 'Competitor'
        
    };
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global static String query = 'Select Id, createdDate, MercerForce_Account_Status__c FROM Account Where ( MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keySet()) + 'OR MercerForce_Account_Status__c = null)';
    //001M000000QJ1lw
    //global static String query = 'Select Id, MercerForce_Account_Status__c FROM Account Where MercerForce_Account_Status__c=null';
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(Test.isRunningTest())
        {
            String accname= 'TestAccount' + String.valueOf(Date.Today());  
            
            query = 'Select Id,createdDate, MercerForce_Account_Status__c FROM Account Where MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(new Set<String>{accname}); 
        }
        return Database.getQueryLocator(query); 
    }
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        List<Account> accForUpdate = new List<Account>();
        
        Set<Id> AccountIds = New Set<Id> (); 
        Map<Id,Opportunity[]> MapAccId_Opptys = New Map<Id,Opportunity[]> () ;
        Map<Id,Contact[]> MapAccId_Conts = New Map<Id,Contact[]> () ;
        Map<Id,ActivityHistory[]> MapAccId_Acts = New Map<Id,ActivityHistory[]> () ;
        
        for(Account a :(List<Account>)scope){
            
            AccountIds.add(a.Id); 
        
        }
                
        List<Opportunity> AllOpportunities  = New List<Opportunity> ();
        AllOpportunities = [ SELECT Id, createdDate, Accountid, CloseDate, OpportunityCreatedAge__c, ClosedOpportunityAge__c 
                                 FROM Opportunity 
                                 WHERE Accountid IN :AccountIds ] ;
                                 
        
        Set<Opportunity> AllOpportunitiesSet  = New Set<Opportunity> ( AllOpportunities  );
        
        for(Id a :AccountIds ){
            
            List<Opportunity> tmpOppty = New List<Opportunity> ();
            
            for( Opportunity O : AllOpportunitiesSet ){
            
                if ( a == O.AccountId ) {
                    
                    tmpOppty.add(O);
                    AllOpportunitiesSet.remove(O); 
                    
                }
        
            }
            
            MapAccId_Opptys.Put(a,tmpOppty);
        
        }
        
        //System.debug( ' MapAccId_Opptys ==> '  + MapAccId_Opptys );
        
        
        List<Contact> AllContacts  = New List<Contact> ();
        AllContacts = [ SELECT Id, AccountId, createdDate, ContactCreatedAge__c  
                                 FROM Contact
                                 WHERE Accountid IN :AccountIds ] ;
                                 
        Set<Contact> AllContactsSet  = New Set<Contact> ( AllContacts );
        
        for(Id a :AccountIds ){
            
            List<Contact> tmpCont = New List<Contact> ();
            
            for( Contact C : AllContactsSet  ){
            
                if ( a == c.AccountId ) {
                    
                    tmpCont.add(c);
                    AllContactsSet.remove(c); 
                    
                }
        
            }
            
            MapAccId_Conts.Put(a,tmpCont);
        
        }
        
        //System.debug( ' MapAccId_Conts ==> '  + MapAccId_Conts  );
        
        List<Account> AllActivities = New List<Account>() ;
        AllActivities = [Select Id, (SELECT ActivityDate, createddate FROM ActivityHistories) FROM Account where Id  IN : AccountIds ];
        
        Set<Account> AllActivitiesSet  = New Set<Account> ( AllActivities );
        
            
            for( Account a : AllActivitiesSet  ){
                
                List<ActivityHistory> tmpAct = New List<ActivityHistory> ();

                    for( ActivityHistory h : a.ActivityHistories  ){
                    
                        tmpAct.add(h);
                    }
                
                if ( ! tmpAct.isEmpty() ){                  
                    MapAccId_Acts.Put(a.Id,tmpAct);
                }
                
                AllActivitiesSet.remove(a);
        
            }
            
            System.debug('MapAccId_Acts ==> '+ MapAccId_Acts );
         
        for(Account account : (List<Account>)scope)
        {
            boolean isObsolete = true;
            boolean oppMaxCreated = true;
            boolean oppMaxClosed = true;
            boolean conMaxCreated = true;
            boolean ActMaxCreated = true;
            List<Opportunity> opps = MapAccId_Opptys.get(account.id) ;
            List<Contact> cons = MapAccId_Conts.get(account.id) ;
            
            
            List<ActivityHistory> Acthistories = New List<ActivityHistory>() ;
                
                if (  MapAccId_Acts.get(account.id) != null ){                  
                    Acthistories = MapAccId_Acts.get(account.id) ;
                }
            
            
            System.debug('account  ==> '+ account  );
            System.debug('Acthistories ==> '+ Acthistories );
            
             
            if(!opps.isEmpty())
            {
                for(Opportunity opp : opps)
                {
                    if(opp.OpportunityCreatedAge__c < 1095)
                    {
                        oppMaxCreated = false;
                        
                    }
                    
                    if(opp.ClosedOpportunityAge__c < 1095)
                    {
                        oppMaxClosed = false;
                        
                    }
                    if(oppMaxCreated && oppMaxClosed) break;
                }
            }
            
            if(!cons.isEmpty())
            {
                for(Contact contact : cons)
                {
                    if(contact.ContactCreatedAge__c < 1095)
                    {
                        conMaxCreated = false;
                        break;
                    }
                }
            }
            if(!Acthistories.isEmpty()){
                for(ActivityHistory act:Acthistories){
                    integer actage = system.today().daysBetween(date.valueof(act.createddate));
                    if(actage < 1095){
                        ActMaxCreated = false;
                    }
                }
            }
            
            if(account.createdDate < stDate && oppMaxCreated && oppMaxClosed && conMaxCreated && ActMaxCreated)
            {
                if(account.MercerForce_Account_Status__c <> 'No Activity - OBSOLETE'){
                    account.MercerForce_Account_Status__c = 'No Activity - OBSOLETE';
                    accForUpdate.add(account);
                }
                
            }
            else{
                if(account.MercerForce_Account_Status__c <> 'No Activity'){
                    account.MercerForce_Account_Status__c = 'No Activity';
                    accForUpdate.add(account);
                }
            }
            
        }
        
        if(!accForUpdate.isEmpty())  
        {
             for(Database.Saveresult result :  Database.update(accForUpdate, false))
             {
            
                    if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                    {
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                    }
             }          
        }
    }
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {     
       try
        {
            Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
            system.debug('\n exception has occured');
        }finally
        {
            MercerAccountStatusBatchHelper.abortAllJobs();
        }
        
    }
    
}