/* Purpose: This test class provides data coverage for New Account Creation
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Arijit Roy         5/18/2013   Created test method for MercVFC09_AccountController class
 ==================================================================================================================*/  
@isTest
private class Test_MercVFC09_AccountController
{

    /* * @Description : This test method provides data coverage to check  new Account creation.       
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    private static testMethod void testAccountCreate()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        Mercer_TestData testData = new Mercer_TestData();
        Account acc = testData.buildAccount();
        system.runAs(User1){
        Test.StartTest();
        PageReference pageRef = Page.Mercer_Account_New;
        pageref.getparameters().put('acc2',acc.name);
        Test.setCurrentPage(pageRef);   
        ApexPages.standardController controller = new ApexPages.StandardController(acc);
        MercVFC09_AccountController accountController= new MercVFC09_AccountController(controller);
        accountController.account = acc;
        accountController.saveAndnew();
        Test.StopTest();
        system.assert(String.valueOf(accountController.saveAndnew()).equalsIgnorecase(String.valueOf(new Pagereference('/apex/Mercer_Account_New?retURL=%2F001%2Fo&nooverride=1'))));}
    }
}