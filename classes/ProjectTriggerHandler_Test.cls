/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step from query 											 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
/*Test Class for Project Trigger*/
@isTest()
public class ProjectTriggerHandler_Test {
  private static Integer month = System.Today().month();
     private static Integer year = System.Today().year();

     @testSetup static void setup(){
        Mercer_TestData mdata = new Mercer_TestData();

        Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
         /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */
        
        Insert listofvalues;
        
        // insert Account 
        
        Account accrec =  mdata.buildAccount();
        Colleague__c col = mdata.buildColleague();
        //insert Opportunity
        Opportunity opprec = mdata.buildOpportunity();
        
        Product2 testProduct = new Product2();
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.LOB__c = 'Talent';
        testProduct.ProductCode = '3051';
        testProduct.Exception__c=False;
        insert testProduct;  
                

        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Product2Id = :testProduct.Id  and CurrencyIsoCode ='ALL' limit 1];      
                
        Test.startTest();
        OpportunityLineItem oppLineItem = Mercer_TestData.createOpptylineItem(opprec.Id, pbEntry.Id);
               
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Test First Name';
        testContact.LastName = 'Test Last Name';
        testContact.AccountId = accrec.Id;
        testContact.email = 'xyzq2@abc04.com';
        testContact.Job_Function__c = 'Board';
        testContact.Job_Level__c = 'Director';
        Insert testContact;  
        Test.StopTest();
        Buying_Influence__c testBI = new Buying_Influence__c();
        testBI.Contact__c = testContact.Id;
        testBI.Opportunity__c = opprec.Id;
        Insert testBI;

    }


     static testmethod void ProjectTriggerClassTest(){
     
         
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        String recipientid=user1.id;
        //Request id:12638 removing step from query        
        Opportunity opprec = [Select Id, Name, Type, AccountId, StageName, CloseDate, Opportunity_Office__c, Opportunity_Country__c, currencyisocode, Close_Stage_Reason__c From Opportunity Where Name = 'test oppty'];
        
        TriggerSettings__c TrgSetting= new TriggerSettings__c();
         TrgSetting.name='Project Trigger';
        TrgSetting.Object_API_Name__c='Revenue_Connectivity__c';
        TrgSetting.Trigger_Disabled__c=false;
         insert TrgSetting;
        
        String OppName= opprec.Name;
        String OppID=opprec.Id;
        
        Id collId = [Select Id from Colleague__c where Name = 'TestColleague'].Id;
        Id opptyLineItemId = [Select Id from OpportunityLineItem where OpportunityId =: opprec.Id LIMIT 1].Id;
        //insert Project 
        Revenue_Connectivity__c testProject = new Revenue_Connectivity__c();
        testProject.CEP_Code__c = 'WEB002-CEP-CODE';
        testProject.Charge_Basis__c = 'S';
        testProject.Project_Amount__c = 20000.00;
        testProject.Colleague__c = collId;
        testProject.Opportunity__c = opprec.Id;
        testProject.Opportunity_Product_Id__c = opptyLineItemId;
        testProject.Project_End_Date__c = date.newInstance(year, month, 1);
        testProject.Project_LOB__c = 'Benefits Admin';
        testProject.Project_Manager__c = '866608';
        testProject.Project_Name__c = 'Absence Management - PROJ01';
        testProject.Project_Solution__c = '3051';
        testProject.Project_Start_Date__c = date.newInstance(year, month, 10);
        testProject.Project_Status__c = 'Active';
        testProject.Project_Type__c = 'Project';
        testProject.WebCAS_Project_ID__c = 'WEB001';
        Test.startTest();
        insert testProject;
        
        List<Revenue_Connectivity__c> rvList=new List<Revenue_Connectivity__c>();
        rvList.add(testProject);
        testProject.Charge_Basis__c = 'T';
        testProject.Project_Status__c = 'Active';
        update testProject;
        Map<id,Revenue_Connectivity__c> ProjectMap=new Map<id,Revenue_Connectivity__c>([select id,Project_LOB_Formula__c,Project_Status__c,Charge_Basis__c,Opportunity_Product_Id__c,Product__c from Revenue_Connectivity__c where Id =:testProject.id]);
        
         ProjectTriggerHandler pth=new ProjectTriggerHandler();
         pth.AfterInsert(rvList, ProjectMap);
         pth.AfterUndelete(ProjectMap);
         pth.BeforeDelete(ProjectMap);
         pth.AfterUpdate(ProjectMap, ProjectMap);
         pth.BeforeInsert(rvList);
         pth.BeforeUpdate(ProjectMap, ProjectMap);
         pth.IsDisabled();
         ProjectTriggerHandler.TriggerDisabled=false;
        pth.CheckOliLinkwithWebcas(ProjectMap);
         ProjectTriggerHandler.closeCheckonProductProject(ProjectMap);
        Test.stopTest();  
        
        
        }
    
}