global class RSSFeedSyncJob {
    
    
    @Future(callout=true)
    public static void syncFeeds (String feedType) {
        // Get the network id. Note: we are under the assumption that there is only one community/network id
        List<Network> networkIdList = [SELECT Id 
                FROM Network];

        String networkId;
        if (!Peak_Utils.isNullOrEmpty(networkIdList)) {
            networkId = networkIdList[0].Id;
        }

        List<RSS_Feed__c> rssFeedList = [SELECT Id,
                Active__c,
                URL__c,
                Sync_Schedule__c,
                Type__c,
                Feed_Title_Key__c,
                Feed_Description_Key__c,
                Feed_Link_Key__c,
                Item_ID_Key__c,
                Item_Title_Key__c,
                Item_Description_Key__c,
                Item_Link_Key__c
            FROM
                RSS_Feed__c
            WHERE
                Type__c = :feedType];

        List<News__c> feedItemList = new List<News__c>();
        RecordType newsRecordType;
                            List<RecordType> newsRecordTypeList = [SELECT Id
                                    FROM RecordType
                                    WHERE Name = 'News'
                                        AND SobjectType = 'News__c'];
        if (!Peak_Utils.isNullOrEmpty(rssFeedList)) {
            for (RSS_Feed__c currentRssFeed : rssFeedList) {
                if( currentRssFeed.Active__c ) {
        
                    RSSFeedDownloadService.RSSOptions options = new RSSFeedDownloadService.RSSOptions();
                    
                    options.feedType = currentRssFeed.Type__c;
                    System.debug('%%%%% options: ' + options);
                    
                    // download the RSS feed
                    RSSFeedDownloadService.RSSFeed downloadedFeed = RSSFeedDownloadService.downloadRSSFeed( currentRssFeed.URL__c, options );
                    System.debug('%%%%% downloadedFeed: ' + downloadedFeed);
                    if( downloadedFeed != null ) {
                        
                        // build items
                        System.debug('%%%%% downloadedFeed.items: ' + downloadedFeed.items);
                        for( RSSFeedDownloadService.RSSFeedItem downloadedItem : downloadedFeed.items ) {
                            
                            System.debug('%%%%% downloadedItem: ' + downloadedItem);
                            // Concatenate all fields and hash to make a unique guid
                            // This is a hack since not all RSS feeds provide a valid GUID
                            String uniqueId = downloadedItem.name + downloadedItem.details + downloadedItem.link + downloadedItem.id;
                            String hashedId = EncodingUtil.convertToHex( Crypto.generateDigest( 'SHA1', Blob.valueOf( uniqueId ) ) );                               
                            
                            // certain RSS feeds have an &apos; (Apostrophe) as the first character 
                            // in the url, i.e. status.salesforce.com
                            // This logic is being put in place to replace the first character if it is an '
                            String link = downloadedItem.link;
                            
                            if( String.isNotBlank( link ) ) {
                                
                                if( link.indexOf( '\'' ) == 0 ) {
                                    link = link.substring( 1, link.length() );
                                }
                                
                                // replace characters with proper encoding for link                   
                                // this list should be expanded, but right now it handles
                                // random issues that were encountered in testing
                                
                                link = link.replaceAll( ' ', '%20' );
                                link = link.replaceAll( '®', '%C2%AE' );
                                link = link.replaceAll( '‘', '%E2%80%98' );
                                link = link.replaceAll( '’', '%E2%80%99' ); 
                            }              

                            

                            if (!Peak_Utils.isNullOrEmpty(newsRecordTypeList)) {
                                System.debug('%%%%% newsRecordTypeList: ' + newsRecordTypeList);
                                newsRecordType = newsRecordTypeList[0];
                            }

                            System.debug('%%%%% newsRecordType: ' + newsRecordType);

                            String articleSource;
                            String details = downloadedItem.details;

                            // Trim down any items that might be longer than their field limits in SF
                            String trimmedName;
                            String trimmedLink;
                            String trimmedDetails;
                            String trimmedSummary;

                            System.debug('%%%%% currentRssFeed: ' + currentRssFeed);
                            String currentFeedType = currentRssFeed.Type__c;
                            if (currentFeedType == RSSFeedDownloadService.FEED_TYPE_MSI) {
                                articleSource = 'Mercer Select Intelligence';
                                details = details + '\n' + '\n' + link;
                            } else if (currentFeedType == RSSFeedDownloadService.FEED_TYPE_BRINK) {
                                articleSource = 'BRINK News';
                                details = details + '\n' + '\n' + link;
                            }

                            System.debug('%%%%% downloadedItem: ' + downloadedItem);
                            // Set defaults for publish date and time in case values do not exist
                            DateTime publishDateTime;
                            Integer day = 1;
                            Integer month = 1;
                            Integer year = System.Today().year();
                            Integer hour = 0;
                            Integer minute = 0;
                            Integer second = 0;

                            System.debug('%%%%% downloadedItem.publishDateTime: ' + downloadedItem.publishDateTime);
                            if (!Peak_Utils.isNullOrEmpty(downloadedItem.publishDateTime)) {
                                String fullDateString = downloadedItem.publishDateTime;
                                Map <String, Integer> months = new Map<String, Integer> {'jan'=>1, 'feb'=>2, 'mar'=>3, 'apr'=>4, 'may'=>5, 'jun'=>6, 'jul'=>7, 'aug'=>8, 'sep'=>9, 'oct'=>10, 'nov'=>11, 'dec'=>12};
                                // Separates out the day of the week, which we don't need. 
                                List<String> dateTimeStrings = new List<String>();
                                
                                // Example pubdate format for MSI: 21 Jul 2017
                                // Example pubdate format for Brink: 24 Jul 2017 05:00:10 +0000
                                if (currentFeedType == RSSFeedDownloadService.FEED_TYPE_BRINK) { 
                                    dateTimeStrings = fullDateString.split(', ');
                                } else {
                                    // add a generic string in the 0 position to make the rest of the formatting work
                                    datetimeStrings.add('filler string');
                                    datetimestrings.add(fullDateString);
                                }

                                // size/length checks to avoid index out of bounds errors.
                                // size() for lists and length() for strings
                                    if (dateTimeStrings.size() > 1) {
                                    String[] dateTimeParts = dateTimeStrings[1].split(' ');

                                    // Split the datetime parts back up
                                    if (!Peak_Utils.isNullOrEmpty(dateTimeParts)) {
                                        day = Integer.valueOf(dateTimeParts[0]);
                                        System.debug('%%%%% day: ' + day);

                                        if (dateTimeParts.size() > 1) {
                                            month = months.get(dateTimeParts[1].toLowerCase());
                                            System.debug('%%%%% month: ' + month);
                                            
                                            if (dateTimeParts.size() > 2) {
                                                year = Integer.valueOf(dateTimeParts[2]);
                                                System.debug('%%%%% year: ' + year);
                                                        
                                                if (dateTimeParts.size() > 3 && !Peak_Utils.isNullOrEmpty(dateTimeParts[3])) {
                                                    hour = Integer.valueOf(dateTimeParts[3].split(':')[0]);
                                                    System.debug('%%%%% hour: ' + hour);
                                                
                                                    if (dateTimeParts[3].length() > 1) {    

                                                        minute = Integer.valueOf(dateTimeParts[3].split(':')[1]);
                                                        System.debug('%%%%% minute: ' + minute);

                                                        if (dateTimeParts[3].length() > 2) {
                                                            second = Integer.valueOf(dateTimeParts[3].split(':')[2]);
                                                            System.debug('%%%%% second: ' + second);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            publishDateTime = Datetime.newInstance(year, month, day, hour, minute, second);                             

                            System.debug('%%%%% publishDateTime: ' + publishDateTime);

                            
                            if (downloadedItem != null) {
                                if (!Peak_Utils.isNullOrEmpty(downloadedItem.name)) {
                                    trimmedName = Peak_Utils.trimString(downloadedItem.name, 80);
                                } else {
                                    trimmedName = 'None';
                                }

                                if (!Peak_Utils.isNullOrEmpty(downloadedItem.summary)) {
                                    trimmedSummary = Peak_Utils.trimString(downloadedItem.summary, 255);
                                } else {
                                    trimmedSummary = 'None';
                                }
                            } else {
                                trimmedName = 'None';
                            }

                            if (!Peak_Utils.isNullOrEmpty(link)) {
                                trimmedLink = Peak_Utils.trimString(link, 255);
                            }

                            if (!Peak_Utils.isNullOrEmpty(details)) {
                                trimmedDetails = Peak_Utils.trimString(details, 50000);
                            }

                            if (currentRssFeed.Type__c == RSSFeedDownloadService.FEED_TYPE_BRINK) {
                                // hardcoding the Brink feed summary to "See Below"
                                trimmedSummary = 'See Below';
                            }

                            News__c feedItem = new News__c(
                                RSS_Feed__c = currentRssFeed.Id,
                                Name = trimmedName,
                                Link__c = trimmedLink,
                                GUID__c = hashedId,
                                RecordType = newsRecordType,
                                Archive_DateTime__c = null, // waiting on this
                                Article_Source__c = articleSource,
                                Details__c = trimmedDetails,
                                Publish_DateTime__c = publishDateTime,
                                NetworkId__c = networkId,
                                Summary__c = trimmedSummary
                                    
                            );
                            
                            feedItemList.add( feedItem );
                            System.debug('%%%%% feedItem: ' + feedItem);
                        }                       
                        
                    }
                }
            }
        }
        
        System.debug('%%%%% feedItemList: ' + feedItemList);            
        if (!Peak_Utils.isNullOrEmpty(feedItemList)) {
            upsert feedItemList GUID__c;
        }
    }
}