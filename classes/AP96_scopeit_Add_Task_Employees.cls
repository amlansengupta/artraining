/*Purpose:  This Apex class is used for adding Task and Employees to a Scope Project from a single page.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   03/25/2014  Created the controller class for adding Task and Employees
   2.0 -    Sarbpreet 08/01/2014 As part of PMO Request 4880(September Release) added logic to sort generic employee
   3.0 -    Madhavi   7/30/2014  As part of PMO Request 4258(Sep Release) added Sub_Business__c fields  to the query filter.
                                 Updated vf page "Mercer_Add_Task_employees" for the requirement.
   4.0      Madhavi   7/30/2014  As part of PMO Request 4338(Sep 2014 Release) added Employee_Status__c fields to the query filter.
                                 
   5.0      Madhavi   10/22/2014 Updated class as part of december 2014 release(#4881) .
                                 Updated vf page "Mercer_Add_Task_employees" to include IC charges and  IC charge Type fields.                                                   
============================================================================================================================================== 
*/
public without sharing class AP96_scopeit_Add_Task_Employees {
    
    public String namesearch {get;set;}
    public String levelsearch {get;set;}    
    public Employee_Bill_Rate__c emp {get;set;}
    private List<EmpWrapper> empWrpList = new List<EmpWrapper>();
    public String subTitle {get;set;}
    public List<Selempwrapper> selempWrpList {get;set;}
    //public ScopeIt_Employee__c empHr {get;set;}    
    
    
    private Set<Id> selset = new Set<Id>();
    private String projectid;
    public String oppCurrCodename {get;set;}    
    private Set<Id> selectedEmpsinPage = new Set<Id>();

    
    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    private integer total_size; //used to show user the total size of the list
 
    //Integer pagesize = 20;
    private Integer page = 100;
    //Integer counter = (page - 1) * list_size; 
        
    public ScopeIt_Task__c objScpTsk {get;set;}
    public Scope_Modeling_Task__c objModTsk {get;set;}
    public Temp_ScopeIt_Task__c objTempTsk {get;set;}
    
    private id taskid;     
    public String objectType{get;set;}
   
    /*
     *  Creation of Constructor
     */
    public AP96_scopeit_Add_Task_Employees() {
    
        emp = new Employee_Bill_Rate__c();
        selempWrpList = new List<Selempwrapper>();
        
        projectid = system.currentPageReference().getParameters().get(ConstantsUtility.STR_ProjID);
        taskid = system.currentPageReference().getParameters().get(ConstantsUtility.STR_ObjID);
        objectType = system.currentPageReference().getParameters().get(ConstantsUtility.STR_Object);
        if(taskid<>null){
            subtitle = ConstantsUtility.STR_EditSubTitle;
            //Added  IC_Charge_Type__c,IC_Charges__c fields to below query as part of december2014 release (request 4881)
            objScpTsk = [select id,task_name__c,name,ScopeIt_Project__c,Bill_Type01__c, IC_Charge_Type__c,IC_Charges__c,Billable_Expenses__c,Bill_Est_of_Increases__c,Bill_Rate__c from ScopeIt_Task__c where id=:taskid];
            projectid = objScpTsk.ScopeIt_Project__c;
        }
        else {
            subtitle =ConstantsUtility.STR_AddSubTitle;
            
        }
        
        total_size = Integer.valueof(Label.ScopeIt_Emplyee_Count);//[select count() from Employee_Bill_Rate__c]; 
        getEmployees();
        getSelectedEmployees();
        if(projectid <> null){
            
            if (objectType == null || ConstantsUtility.STR_Blank.equals(objectType)){
                objScpTsk = new ScopeIt_Task__c();
                ScopeIt_Project__c objProject = [select id,name,CurrencyIsoCode from ScopeIt_Project__c where id=:projectid];
                if(taskid==null){
                    objScpTsk.Bill_Type01__c = ConstantsUtility.STR_PickVal;//Added as part of december 2014 release(#4881)
                    objScpTsk.ScopeIt_Project__c = objProject.id;
                    objScpTsk.CurrencyIsoCode = objProject.currencyIsocode;
                    Integer totalTaskCnt = [select count() from ScopeIt_Task__c where ScopeIt_Project__c=:objProject.id];
                    objScpTsk.Task_Name__c = ConstantsUtility.STR_Task +(totalTaskCnt+1);
                }
                oppCurrCodename = objProject.currencyisocode;
            }
            else {              
                objModTsk = new Scope_Modeling_Task__c();
                objModTsk.Bill_Type01__c = ConstantsUtility.STR_PickVal;//Added as part of december 2014 release(#4881)
                Scope_Modeling__c objProject = [select id,name,CurrencyIsoCode from Scope_Modeling__c where id=:projectid];
                Integer totalTaskCnt = [select count() from Scope_Modeling_Task__c where Scope_Modeling__c=:projectid];
                objModTsk.Task_Name__c = ConstantsUtility.STR_Task +(totalTaskCnt+1);
                objModTsk.Scope_Modeling__c = objProject.id;
                oppCurrCodename = objProject.currencyisocode;
            }
           
            oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].Conversion_Rate__c;
            OpportunityCode =  [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :oppCurrCodename].name;
        }
    }
    private Decimal oppcurrcode;
    public String OpportunityCode {get;set;}
    
    /*
     * @Description : This method cancels the event 
     * @ Args       : null  
     * @ Return     : void
     */
    public pageReference cancel(){
        PageReference pageReference = null;
        try {
             pageReference = new PageReference(ConstantsUtility.STR_BackSlash + projectid);
        
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
        }
         return pageReference;
    }
    
    /*
     * @Description : This method clears the search fields 
     * @ Args       : null  
     * @ Return     : void
     */
    public void clearFields(){
        namesearch = null;
        levelsearch = null;
        //emp.Market_Country__c =  '--None--';
        //emp.business__c = '--None--';
    }
    
    /*
     * @Description : This method removes the employess from the list 
     * @ Args       : null  
     * @ Return     : void
     */
    public void removeEmployees(){
        List<Selempwrapper> newempwrapper = new list<Selempwrapper>();
        integer selcnt =0;
        try {
        for(Selempwrapper selemp:selempWrpList){
            if(selemp.isSelected) {
             selcnt++;
            }
            else {
             newempwrapper.add(selemp);
            }
        }
        if(newempwrapper.size()>0){
            selempWrpList.clear();
            selempWrpList.addAll(newempwrapper);
        }
        else if(selcnt ==selempWrpList.size()){
            selempWrpList.clear();
        }
        
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
        } 
    }
    
    /*
     * @Description : This method saves the employees records 
     * @ Args       : null  
     * @ Return     : void
     */
    public pageReference saveEmployees(){
        try{
            if(objectType == null || ConstantsUtility.STR_Blank.equals(objectType)) {
                if(objScpTsk.task_name__c == null){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ConstantsUtility.STR_ErrorMessage);
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                else {
                    if(taskid == null) {
                    
                        insert objScpTsk;
                    }
                    else { 
                           database.update(objScpTsk);
                    }
                    List<ScopeIt_Employee__c> lstScopEmp = new list<ScopeIt_Employee__c>();
                    ScopeIt_Employee__c objemp;
                    for(Selempwrapper selemp:selempWrpList){
                        objemp = new ScopeIt_Employee__c();
                        objemp.Employee_Bill_Rate__c = selemp.recid;
                        objemp.bill_rate_opp__c = selemp.billrate;
                        objemp.ScopeIt_Task__c = objScpTsk.id;
                        objemp.ScopeIt_Project__c = projectid;
                        objemp.Hours__c = selemp.hours;
                        lstScopEmp.add(objemp);
                    }
                     
                    database.insert(lstScopEmp);               
                    return new PageReference(ConstantsUtility.STR_BackSlash + projectid);    
                }    
            }
            else{
                if(objModTsk.Task_name__c == null){
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ConstantsUtility.STR_ErrorMessage);
                ApexPages.addMessage(myMsg);
                return null;
                }
                else {
                      
                      insert objModTsk;
                  
                    List<Scope_Modeling_Employee__c> lstScopEmp = new list<Scope_Modeling_Employee__c>();
                    Scope_Modeling_Employee__c objemp;
                    for(Selempwrapper selemp:selempWrpList){
                        objemp = new Scope_Modeling_Employee__c();
                        objemp.Employee_Bill_Rate__c = selemp.recid;
                        objemp.bill_rate_opp__c = selemp.billrate;
                        objemp.Scope_Modeling_Task__c = objModTsk.id;
                        objemp.Scope_Modeling__c = projectid;
                        objemp.Hours__c = selemp.hours;
                        lstScopEmp.add(objemp);
                    }
                    
                    database.insert(lstScopEmp);
                    
                    return new PageReference(ConstantsUtility.STR_BackSlash + projectid);    
                }
            }
        }
        catch (Exception ex) {
            MercerUtility.addErrorMessage( ex.getMessage());
          //  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
            return null;
        }
    
    }
    private map <String,Decimal> empbillratemap = new Map<String,Decimal>();
    
    /* 
     * @Description : Getter method for getting Employees.
     * @ Args       : null   
     * @ Return     :List<EmpWrapper>
     */
    public List<EmpWrapper> getEmployees(){
        String nm =ConstantsUtility.STR_BLNK;
        String etype=ConstantsUtility.STR_BLNK;
        String emar=ConstantsUtility.STR_BLNK;
        String ebus=ConstantsUtility.STR_BLNK;
        String ls=ConstantsUtility.STR_BLNK;
        String sub =ConstantsUtility.STR_BLNK;
        
        try {
            if(namesearch <>null) { 
                nm = '%' +namesearch+'%';
            }
            if(levelsearch <>null) {
                ls = '%' +levelsearch+'%';
            }
                
            empWrpList.clear();
            //As part of PMO request 4258(Sep 2014 Release) added Sub_Business__c fields  to the query filter.
            //As part of PMO request 4338(Sep 2014 Release) added Employee_Status__c fields  to the query filter.
            String val =ConstantsUtility.STR_ACTIVE;
            String query ='select currencyisocode,name,Type__c,Business__c,Market_Country__c,Employee_Status__c,Sub_Business__c,Bill_Cost_Ratio__c, Bill_Rate_Local_Currency__c,level__c from Employee_Bill_Rate__c ';
            String whercond ='where ';
          
            
            if(emp.type__c != null) {
                    etype=emp.type__c;
                    whercond += ' Type__c = \''+etype+'\' and';
            }
            if(emp.Market_Country__c != null){
                    emar = emp.Market_Country__c;
                    whercond += ' Market_Country__c = \''+emar+'\' and';
            }
            if(emp.Business__c != null){
                    ebus = emp.Business__c;
                    whercond += ' Business__c = \''+ebus+'\' and';
            }
            //Added as part of request 4258
            if(emp.Sub_Business__c!= null){
                    sub= emp.Sub_Business__c;
                    whercond += ' Sub_Business__c = \''+sub+'\' and';
            }            
            if(string.isNotBlank(nm) && nm.length()>3) {
                    whercond += ' name like :nm and';
            }
            
            if(string.isNotBlank(ls)) {
                    whercond += ' level__c like :ls and ';
            }    
            query +=whercond;
            //As part of PMO Request 4880(September Release 2014) added logic to sort generic employee
            query += ' Employee_Status__c=\''+val+'\' order by Business__c, Market_Country__c , Level__c limit :list_size offset :counter';           
            for(Employee_Bill_Rate__c ebrc: database.query(query)){
                Boolean isSelected = true;
                if(!selectedEmpsinPage.contains(ebrc.id)) {
                    isSelected = false;
                }
                else {
                    isSelected = true;
                }
                empWrpList.add(new EmpWrapper(isSelected,ebrc.id,ebrc.name,ebrc.type__c, ebrc.Business__c,ebrc.Market_Country__c,ebrc.id,ebrc.Bill_Rate_Local_Currency__c,ebrc.currencyisocode,ebrc.level__c,ebrc.Sub_Business__c));
            }
            
            Map<String,Decimal> exchMap = new Map <String,Decimal>();
            
            List<String> ratelist = new List<String>();
                    
            for(EmpWrapper ebrc:empWrpList)
                ratelist.add(ebrc.isocode);
                  
            for(Current_Exchange_Rate__c er:[select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :ratelist]) {
                exchMap.put(er.name,er.Conversion_Rate__c);          
            }
            
            for(EmpWrapper em: empWrpList){
                if(em.billrate <> null && exchMap.containsKey(em.isocode) && oppcurrcode<>null) {
                    em.convertedrate = ((em.billrate * oppcurrcode) / exchMap.get(em.isocode)).round();
                }
                else {
                    em.convertedrate = 0;
                }
                    
                empbillratemap.put(em.recid, em.convertedrate);
            }
        }catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
        }
       return empWrpList;
    }
    /*
     * @Description : This method retrieves the selected employees record
     * @ Args       : null  
     * @ Return     : void
     */
    public void getSelectedEmployees(){
        //selempWrpList.clear();
        selset.clear();
        set<id> recidSet = new set<id>();
        try {
            if(selempWrpList.size()>0)           
                  for(Selempwrapper s:selempWrpList)
                      recidSet.add(s.recid);
                    
            for(EmpWrapper ew: empWrpList){
                if(ew.isSelected && !recidSet.contains(ew.recid)) {
                    selset.add(ew.recid);
                }
            }
            
            
            for(Id sid: selectedEmpsinPage){
                if(!recidSet.contains(sid)) {
                    selset.add(sid);
                }
            }
            if(selset.size()>0){
               //As part of PMO request 4258(Sep 2014 Release) added Sub_Business__c  to query            
               for(Employee_Bill_Rate__c ebrc:[select currencyisocode, id,name,Level__c,Type__c,Business__c,Sub_Business__c,Market_Country__c, Bill_Cost_Ratio__c,  Bill_Rate_Local_Currency__c from Employee_Bill_Rate__c where id in:selset]) {
                    selempWrpList.add(new Selempwrapper(ebrc.id,ebrc.name,ebrc.Level__c,ebrc.business__c, ebrc.market_country__c,ebrc.Sub_Business__c,empbillratemap.get(ebrc.id)));
               }
               
            }
        }catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
        }
    }
    /*
     * @Description : This method updates the selected employee record 
     * @ Args       : null  
     * @ Return     : void
     */
    public void updateselectedemployees(){
        try {
        for(EmpWrapper ew: empWrpList){
            if(ew.isSelected) {
                selectedEmpsinPage.add(ew.recid);
            }
        }
        }catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
        }
        
    }
    
    /*
     * Wrapper class for employees records 
     */
    public class EmpWrapper{
        public boolean isSelected {get;set;}
        public String isoCode {get;set;}
        public String recid{get;set;}
        public String name {get;set;}
        public String type {get;set;}
        public String business {get;set;}
        public String market {get;set;}  
        public Decimal billrate {get;set;} 
        public Decimal convertedrate {get;set;} 
        public String level {get;set;}
        public String subbusiness{set;get;}//Added as part of Request 4258(Sep 2014 Release)
        /*
         *  Method for getting details of an Employee
         */
        public EmpWrapper(Boolean sel,String id,String n, String t, String b, String m, String rid, Decimal billRat,string iso, string level,String Sub){
            this.isSelected = sel;
            this.name = n;
            this.recid = id;
            this.type = t;
            this.business = b;
            this.market = m;
            this.recid = rid;
            this.billrate = billRat;
            this.isoCode = iso;
            this.level=level;
            this.subbusiness= Sub;//Added as part of Request 4258(Sep 2014 Release)
        }
    }
    
     
    /*
     * Wrapper class for selected employees records 
     */
     public class Selempwrapper{
        public boolean isSelected {get;set;}
        public String recid{get;set;}
        public String business{get;set;}
        public String market{get;set;}
        public String name{get;set;}
        public String level{get;set;} 
        public String subbus{get;set;}//As part of PMO request 4258(Sep 2014 Release) added Sub_Business__c    
        public Decimal hours {get;set;}   
        public Decimal billrate {get;set;}  
        /*
         *  Method for getting details of Selected Employee
         */
        public Selempwrapper(String rid,String n,String l,String busi, String mar,String subbus, Decimal rt){
            this.recid = rid;
            this.name = n;
            this.level = l;
            this.business = busi;
            this.billrate = rt;
            this.market = mar;
            this.subbus = subbus;//As part of PMO request 4258(Sep 2014 Release) added Sub_Business__c 
            hours = 0;
        }
    }
    
     /*
      * @Description : This method navigates to the beginning of the page 
      * @ Args       : null  
      * @ Return     : void
      */
    public PageReference beginning() {
    try {
      counter = 0;
    } catch (Exception ex) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
    }
      return null;
    }
 
    /*
     * @Description : This method navigates to the previous page 
     * @ Args       : null  
     * @ Return     : void
     */
    public PageReference previous() {
    try {
         updateselectedemployees();   
         counter -= list_size;
    } catch (Exception ex) {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
    }
      return null;
   }
 
    /*
     * @Description : This method navigates to the next page 
     * @ Args       : null  
     * @ Return     : void
     */
   public PageReference next() {
       try {
      updateselectedemployees();
      counter += list_size;
       } catch (Exception ex) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
       }
      return null;
   }
 
   /*
    * @Description : This method navigates to the end of the page 
    * @ Args       : null  
    * @ Return     : void
    */
   public PageReference end() { 
       try {
      counter = total_size - math.mod(total_size, list_size);
       } catch (Exception ex) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));           
       }
      return null;
   }
 
   /* 
    * @Description : Method for enabling and disabling Previous button
    * @ Args       : null   
    * @ Return     : Boolean
    */
   public Boolean getDisablePrevious() {
      
      if (counter>0) {
          return false;
      }else {
          return true;
      }
   }
   
   /* 
    * @Description : Method for enabling and disabling Next button
    * @ Args       : null   
    * @ Return     : Boolean
    */
   public Boolean getDisableNext() { 
      if (counter + list_size < total_size) {
        return false;
      }else {
        return true;
      }
   }
 
   /* 
    * @Description : Method for getting Total size
    * @ Args       : null   
    * @ Return     : total_size
    */
   public Integer getTotal_size() {
      return total_size;
   }
 
   /* 
    * @Description : Method for getting Page Number
    * @ Args       : null   
    * @ Return     : counter/list_size + 1
    */
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   /* 
    * @Description : Method for getting Total number of Pages
    * @ Args       : null   
    * @ Return     : total_size/list_size
    */
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }

}