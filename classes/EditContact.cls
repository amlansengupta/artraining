public class EditContact {
    public Contact Con{get; set;}
    public string ConId {get;set;}    
    ApexPages.standardController m_sc = null;
    public string leanDataBaseURL{get;set;}
    public string RingLeadDataBaseURL{get;set;}
    
    public EditContact (ApexPages.StandardController controller) {
        this.Con=(Contact)controller.getRecord();
        
        ConId =Con.id;
        m_sc=controller;
        
        string BaseURL = ApexPages.currentPage().getHeaders().get('Host');
        RingLeadDataBaseURL=BaseURL.replace('--c','--uniqueentry');
    }
    
    public PageReference edit(){
    
     PageReference pageRef = new PageReference('/apex/Mercer_Contact_EditPage');
     pageRef.getParameters().put('ConId', String.valueOf(Con.id));

     return pageRef; 
    
    }
     public PageReference save(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(Con);
        try{
            
            controller.save();
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/apex/ContactSimplifiedScreen');
     pageRef.getParameters().put('ConId', String.valueOf(Con.id));

     return pageRef; 

     
    
    }
    public PageReference saveAndReturn(){
    
     ApexPages.StandardController controller = new ApexPages.StandardController(Con);
        try{
            
            controller.save();
        }
        catch(Exception e){
            return null;
        }
        
     PageReference pageRef = new PageReference('/'+Con.id);
     //pageRef.getParameters().put('ConId', String.valueOf(Con.id));

     return pageRef; 

     
    
    }
    public PageReference Cancel()
    {
        PageReference pageRef = new PageReference('/apex/ContactSimplifiedScreen');
        pageRef.getParameters().put('ConId', String.valueOf(Con.id));

         return pageRef; 
    }
    public PageReference doCancel()
    {
         return m_sc.cancel(); 
    }
    
}