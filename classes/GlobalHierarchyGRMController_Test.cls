@isTest
public class GlobalHierarchyGRMController_Test {
    public static testmethod void getProfileNameTest()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1)
        {
            GlobalHierarchyGRMAccountApexController.getProfileName();
        }
    }
}