/*Purpose:  This Apex class is created as a Part of March Release for Req:2877
==============================================================================================================================================
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Ravi    02/24/2014  This class acts as Test class for AP85_postChatterwithReportinfo .
                                
 
============================================================================================================================================== 
*/
@isTest(seeAlldata=true)
public class Test_AP85_postChatterwithReportinfo{
  
  /*
   * @Description : Batch file execution test method for Req#2877
   * @ Args       : Null
   * @ Return     : void
   */
  private static testMethod void postChatter_test(){
  
  test.startTest();
  AP85_postChatterwithReportinfo postChatter= new AP85_postChatterwithReportinfo();
  ID batchprocessid = Database.executeBatch(postChatter);
  test.stoptest();
    System.abortJob(batchprocessid);
  }

  
}