/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Arijit Roy 03/29/2013  Created Schedulable class for Account RM Ownership
=============================================================================================================================================== 
*/
global class AP41_RMOwnershipSchedulable implements Schedulable
 {
  
    global Map<String,Colleague__c> ColleagueMap = new Map<String,Colleague__c>();
    global Map<String,User> empIdUserMap = new Map<String,User>();
    global AP41_RMOwnershipSchedulable()
    {
        init();    
    }
    
    // initialization method
    global void init()
    {
        for(Colleague__c colleague :[SELECT Id, EMPLID__c FROM Colleague__c])
        {
        	ColleagueMap.put(colleague.EMPLID__c, colleague);
        }
        
        
        for(User user : [select Id, EmployeeNumber From User where EmployeeNumber IN : ColleagueMap.keyset() and isActive = true])
        {
            empIdUserMap.put(user.EmployeeNumber, user);
        }
                      
                
    }
    
    global void execute(SchedulableContext SC)
    {
    
    }
    

}