/*Purpose: Test class to provide test coverage for AP108_ScopeModellingCopyTaskandEmployees.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   08/28/2014    Created test class to provide test coverage for AP108_ScopeModellingCopyTaskandEmployees class
 
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP108_ScopeModlingCopyTaskEmployee_Test{
    /*
        * @Description : Testmethod to test copy functionality on  scopemodelling
    */
    static testmethod void copymodellingTaskandEmployee(){
    	User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);        
        System.runAs(user1){
	        Mercer_ScopeItTestData  mtscopedata = new Mercer_ScopeItTestData();
	        //Insert Product
	        String LOB = 'Retirement';
	        List<Pricebook2> prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
	        Product2 testProduct = mtscopedata.buildProduct(LOB);
	        //insert Modelling Record
	        Scope_Modeling__c moddata =mtscopedata.buildScopeModelProject(testProduct.id);
	        //insert ModellingTask
	        mtscopedata.buildScopeModelTask();
	        //insert EmployeebillRate
	        String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
	        //insert Employees
	        mtscopedata.buildScopeModelEmployee(employeeBillrate);
	        Test.startTest();
	           AP108_ScopeModellingCopyTaskandEmployees.CopyModellingTaskEmployee(moddata.id);
	        Test.stopTest();
	        System.assertequals(1,prcbk.size());
        }
    }
    /*
      * @Description : Testmethod to test copy functionality on  scopeitTemplate
    */
    static testmethod void  CopyTemplateTaskEmployee(){
    	User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1){
	        Mercer_ScopeItTestData  mtscopedata = new Mercer_ScopeItTestData();
	        //Insert Product
	        String LOB = 'Retirement';
	        Product2 testProduct = mtscopedata.buildProduct(LOB);
	        Temp_ScopeIt_Project__c  temprec =Mercer_ScopeItTestData.createScopeitTemplate('Retirement',testProduct.id,testProduct.name);
	        insert temprec;
	        //insert scopeitTask
	        Temp_ScopeIt_Task__c  taskrec =Mercer_ScopeItTestData.createScopeitTask(temprec.id);       
	        insert taskrec;
	        //insert EmployeebillRate
	        String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
	        Temp_ScopeIt_Employee__c employeerec =Mercer_ScopeItTestData.createScopeitEmployee(temprec.id,taskrec.id,employeeBillrate);
	        insert employeerec;
	        Test.startTest();
	            AP108_ScopeModellingCopyTaskandEmployees.CopyTemplateTaskEmployee(temprec.id);
	        Test.stopTest();
        }
        
    }
}