/*
Purpose: This test class provides data coverage to AP23_CampaignSchedulable schedulable class
==============================================================================================================================================
History
-----------------------
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    	 Arijit  03/29/2013  Created Test class
============================================================================================================================================== 
*/
@isTest
private class Test_AP23_CampaignSchedulable {
	/*
     * @Description : Test method to provide data coverage for AP23_CampaignSchedulable schedulable class
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
    	AP23_CampaignSchedulable AMsch = new AP23_CampaignSchedulable();
    	String sch = '0 0 23 * * ?';
	    system.schedule('Test AP23_CampaignSchedulable', sch, AMsch);
    	
    }
}