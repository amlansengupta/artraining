/*************************************************************************************************************
User Story : US512
Purpose : This apex class is created to support the 'Import From Template' Custom link on ScopeIt Project.
Created by : Akash Shroff
Created Date : 08/01/2019
Project : MF2
****************************************************************************************************************/
Global class MF2_ScopeItImportFromTemplateApex {
 public String ScopeOpportunityId {get;set;}
    //public String ScopeItrecord {get;set;}
    
    public MF2_ScopeItImportFromTemplateApex(ApexPages.StandardSetController controller){
    //List<String> Scopeitrecords = ApexPages.currentPage().getParameters().get('ScopeitRecords');
    //ScopeItrecords = Apexpages.currentPage().getParameters().get('ScopeItrecords');
    //List<Scopeit_project__c> scopeItRecords = controller.getrecords();
    /*ScopeItrecords = new List<String>();
	System.debug('ScopeItRecords '+ ScopeItrecords);*/
    ScopeOpportunityId = ApexPages.currentPage().getParameters().get('OppId');
        System.debug('opp '+ScopeOpportunityId);
    
    }
    
    public pagereference redirectToOpp(){
        Pagereference pageRef = new Pagereference('/'+ScopeOpportunityId);
        return pageRef;
    }
    
 
}