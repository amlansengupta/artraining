/*
Purpose: This Controller is an extension controller for the Mercer Login Popup Page. 
           
==============================================================================================================================================
History ----------------------- 

                                                        VERSION     AUTHOR  DATE        DETAIL 
                                                            1.0 -              Arijit   12/18/2012  Created Controller Class
============================================================================================================================================== 
*/
global class MercVFC02_PopUp_Controller {

	//Public void MercVFC02_PopUp_Controller () {}
	
	webService static Boolean HasUserChecked(){
		List<user> UserList = New List<User>([Select Id,HasUserChecked__c from user where Id = :Userinfo.getuserId() ]);
		//Userlist.HasUserChecked__c = true;
		for ( User uservar : UserList )
		{  
			  uservar.HasUserChecked__c = true;
		}
		
		update userlist;
		system.debug('-------------------------------------------method invoked');
		return true;
	}
	
	public void updateUserDisclaimerViewed()
	{
		User user = new User(Id = UserInfo.getUserId());
		user.HasUserChecked__c = true;
		update user;
	}

}