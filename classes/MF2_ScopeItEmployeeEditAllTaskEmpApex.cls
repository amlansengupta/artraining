/***************************************************************************************************************
User story : 17451
Purpose : This apex class is created as the controller of MF2_ScopeItEmployeeEditAllTaskEmp
		VF page.
Created by : Soumil Dasgupta
Created Date : 06/01/2018
Project : MF2
****************************************************************************************************************/

public class MF2_ScopeItEmployeeEditAllTaskEmpApex {
    
    public boolean taskPresentFlag {get; set;}
    public String scopeItProjectId {get; set;}
    
    public MF2_ScopeItEmployeeEditAllTaskEmpApex(ApexPages.StandardController controller){
        taskPresentFlag = false;
        //Get the ScopeIt Project Id from Controller.
        scopeItProjectId = controller.getId();
        
        //Fetch all the ScopeIt Task records under the particular ScopeIt Project record.
        List<ScopeIt_Task__c> listScopeItTask = [select Id from ScopeIt_Task__c where ScopeIt_Project__c =: scopeItProjectId];
        if(!listScopeItTask.isempty() && listScopeItTask != null){
            taskPresentFlag = true;
        }
        
    }
    
}