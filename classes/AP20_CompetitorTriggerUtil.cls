/*Purpose:  This Apex class contains static methods to process Competitor records on their various stages of insertion/updation/deletion
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Sujata   02/14/2013  Created Apex to support TRG11
   2.0 -    Trisha	 01/18/2019  12638-Replacing step with stage
============================================================================================================================================== 
*/

public class AP20_CompetitorTriggerUtil {

    //Set to contain Opportunity ID and Competitor ID as Key-Value.
    public static Map<ID,ID> oppIdCompIdMap = new Map<ID,ID>();
    
    //Set to contain Competitor ID and Opportunity Step as Key-Value.
    public static Map<ID,String> compIdOppStepMap = new Map<ID,String>();
    
    
    /*
     * @Description : This method is called on before insertion of Contact records
     * @ Args       : Trigger.new
     * @ Return     : void
     */   
    public static void processCompetitorBeforeDelete(List<Competitor__c> triggerold)
    {
        for(Competitor__c comp : triggerold)
        {
            oppIdCompIdMap.put(comp.Opportunity__c,comp.Id);
        }
        /*****request id:12638 removing step and adding stageName in query START*****/ 
       /* for(Opportunity opp : [Select Id,Name,Step__c FROM Opportunity WHERE Id IN : oppIdCompIdMap.Keyset()])
        {
            compIdOppStepMap.put(oppIdCompIdMap.get(opp.Id),opp.Step__c);
        } */ 
         for(Opportunity opp : [Select Id,Name,stageName FROM Opportunity WHERE Id IN : oppIdCompIdMap.Keyset()])
        {
            compIdOppStepMap.put(oppIdCompIdMap.get(opp.Id),opp.stageName);
        } 
        /*****request id:12638 removing step and adding stageName in query END*****/ 
        /*****requestId:12638, comparison of step are replaced with that of stage in below block START *******/
       /* for(Competitor__c comp : triggerold)
        {
            if((compIdOppStepMap.get(comp.Id) == 'Closed / Won (SOW Signed)') || (compIdOppStepMap.get(comp.Id) == 'Closed / Lost'))
            {
                comp.addError('You cannot delete this competitor because Opportunity Step is :'+compIdOppStepMap.get(comp.Id)); 
            }
        }*/
        for(Competitor__c comp : triggerold)
        {
            if((compIdOppStepMap.get(comp.Id) == System.Label.CL48_OppStageClosedWon ) || (compIdOppStepMap.get(comp.Id) == System.Label.CL60_ClosedLost))
            {
                comp.addError(System.Label.ERR_Can_Not_Del_Comp+compIdOppStepMap.get(comp.Id)); 
            }
        }
        /*****requestId:12638, comparison of step are replaced with that of stage in below block END *******/
    }   
}