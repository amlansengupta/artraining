/*Purpose: This Apex class implements Batchable to update ConcatenatedRanking Field on Account if a new Ranking has been added or Removed From Account.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Gyan   12/17/2013  Created Apex Batchable class under Req-3401
============================================================================================================================================== 
*/

global class DC32_ConcatenatedRankingBatchable implements Database.Batchable<sObject>, schedulable {
    
    global String query;
     // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
     
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'Select Concatenated_Ranking__c, (Select Ranking_Description__c From Ranking__r) From Account where ID IN (select Account__c From Ranking__c )  AND One_Code_Status__c IN (\'Active\', \'Pending\', \'Pending - Workflow Wizard\') AND Duplicate_Flag__c = False'; 
        return Database.getQueryLocator(query);
    }
    
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the Account records which don't have correct value in ConcatenatedRanking Field.
     *  Note: execute method is a mandatory method to be extended in this batch class.     
    */
    global void execute(Database.BatchableContext bc, List<Account> lstAccount)
    {
         Integer errorCount = 0 ;
        List<Account> lstAccToUpdate = new List<Account>();
        List<Ranking__c> lstRank = new List<Ranking__c>();
         For(Account acc : lstAccount)
         {
                 acc.Concatenated_Ranking__c = ''; 
                  // Iterate through Ranking associated with the Account 
                     for(Ranking__c ctm : acc.Ranking__r)
                     {   
                         lstRank.add(ctm);
                         break;
                     }   
         }
       
    
// List variable to save the result of the Ranking Object which are updated   
     List<Database.SaveResult> srList= Database.Update(lstRank, false);  

  
     if(srList!=null && srList.size()>0) {
       // Iterate through the saved Ranking object 
       Integer i = 0;             
       for (Database.SaveResult sr : srList)
       {
              // Check if Ranking object are not updated successfully.If not, then store those failure in error log. 
              if (!sr.isSuccess()) 
              {
                      errorCount++;
                      System.debug('Error DC32_ConcatenatedRankingBatchable '+sr.getErrors()[0].getMessage());
                      errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC32:' +sr.getErrors()[0].getMessage(), errorLogs, lstRank[i].Id);  
              }
              i++;
       }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('DC32 Status :lstAccount:' + lstAccount.size() +' lstRank:' + lstRank.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , lstAccount[0].Id);
     
     insert errorLogStatus;      
    }
    
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records . 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
         // Check if errorLogs contains some failure records. If so, then insert error logs into database.
         if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }
    
    /*
     *  Method Name: execute
     *  Note: execute method is a mandatory method to be extended in this batch class for Scheduling Purpose.     
    */
    public void execute(SchedulableContext sc)
    {
        DC32_ConcatenatedRankingBatchable batch = new DC32_ConcatenatedRankingBatchable();
        database.executeBatch(batch,2000);    
    }

}