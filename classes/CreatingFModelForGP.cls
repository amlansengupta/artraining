public class CreatingFModelForGP {
    public static void creatingForcasTModel( Map<ID,Growth_Plan__c> newMap){
        String CurrentAccountId,CurrentYear,CurrentGPId,PreviousYear;
        Integer currentYearInt;
        List<Growth_Plan__c> growthPlanList = newMap.values();
        List<Forecast_Model__c> forecastModelNewList = new List<Forecast_Model__c>();
        try{
            if(growthPlanList.size()>0){
                
                for(Growth_Plan__c gobj:growthPlanList){
                   System.debug('>>>year>>>>'+gobj.Planning_Year__c);
                   if(gobj.Planning_Year__c == '2019'){
                       for(Integer i=1;i<48;i++){
                        Forecast_Model__c fObjHealth= new Forecast_Model__c();
                        
                        //for Health LOb
                        
                        if(i==1){
                            fObjHealth.LOB__c='Health';
                         }
                        if(i==2){
                            fObjHealth.Sub_Business__c='Health Unassigned';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==3){
                            fObjHealth.Sub_Business__c='Associations';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==4){
                            fObjHealth.Sub_Business__c='B2B';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==5){
                            fObjHealth.Sub_Business__c='Government';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        if(i==6){
                            fObjHealth.Sub_Business__c='Health - Large Market Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==7){
                            fObjHealth.Sub_Business__c='Marketplace 365';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        if(i==8){
                            fObjHealth.Sub_Business__c='Mercer Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==9){
                            fObjHealth.Sub_Business__c='Voluntary Benefits';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        //for Wealth LOb
                       
                        if(i==10){
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==11){
                            fObjHealth.Sub_Business__c='Wealth Unassigned';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==12){
                            fObjHealth.Sub_Business__c='Administration';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==13){
                            fObjHealth.Sub_Business__c='Defined Benefits';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==14){
                            fObjHealth.Sub_Business__c='Delegated Solutions';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==15){
                            fObjHealth.Sub_Business__c='DCs & Financial Wellness - Consumer';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==16){
                            fObjHealth.Sub_Business__c='DCs & Financial Wellness - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==17){
                            fObjHealth.Sub_Business__c='Inv CIO';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==18){
                            fObjHealth.Sub_Business__c='Leadership & Operations - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==19){
                            fObjHealth.Sub_Business__c='Non-Pension and Specialties';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==20){
                            fObjHealth.Sub_Business__c='Wealth - Large Market Admin';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==21){
                            fObjHealth.Sub_Business__c='Wealth Administration';
                            fObjHealth.LOB__c='Wealth';
                        }
                        //for Career LOB
                        
                        if(i==22){
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==23){
                            fObjHealth.Sub_Business__c='Career Unassigned';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==24){
                            fObjHealth.Sub_Business__c='Career - Career';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==25){
                            fObjHealth.Sub_Business__c='Career - Digital';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==26){
                            fObjHealth.Sub_Business__c='Career - Products';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==27){
                            fObjHealth.Sub_Business__c='Career - Services';
                            fObjHealth.LOB__c='Career';
                        }
                        
                        
                         //for Career GBS
                         
                         if(i==28){
                            fObjHealth.LOB__c='GBS';
                         }
                         if(i==29){
                            fObjHealth.Sub_Business__c='GBS Unassigned';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==30){
                            fObjHealth.Sub_Business__c='Intellectual Capital Solutions';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==31){
                            fObjHealth.Sub_Business__c='Global Consulting';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==32){
                            fObjHealth.Sub_Business__c='GBS Health';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==33){
                            fObjHealth.Sub_Business__c='Global Wealth';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==34){
                            fObjHealth.Sub_Business__c='Global Career';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==35){
                            fObjHealth.Sub_Business__c='Thomsons';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==36){
                            fObjHealth.Sub_Business__c='Innovation & M&A Community';
                            fObjHealth.LOB__c='GBS';
                         }
                        if(i==37){
                            fObjHealth.Sub_Business__c='MCG Leadership/GCM';
                            fObjHealth.LOB__c='GBS';
                         }
                         if(i==38){
                            fObjHealth.Sub_Business__c='WRG';
                            fObjHealth.LOB__c='GBS';
                         }
                        
                        //for Global Planning
                        if(i==39){
                            fObjHealth.LOB__c='Global Planning';
                         }
                         
                        if(i==40){
                            fObjHealth.LOB__c='Global Planning';
                            fObjHealth.Sub_Business__c='Global Planning Health';
                         }
                         if(i==41){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Wealth';
                           }
                         if(i==42){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Career';
                          }
                         if(i==43){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Consulting';
                           }
                         if(i==44){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Mercer Powered By Darwin';
                           }
                          if(i==45){
                              fObjHealth.LOB__c='Global Planning';
                              fObjHealth.Sub_Business__c='Cross OpCo Solutions - Excl from Rollups';
                         }
                         if(i==46){
                              fObjHealth.LOB__c='Other';
                         }
                         if(i==47){
                              fObjHealth.LOB__c='Other';
                              fObjHealth.Sub_Business__c='Other Unassigned';
                         }
                          
                        fObjHealth.Current_Carry_Forward_Revenue__c=0;
                        fObjHealth.Current_FY_Revenue__c=0;
                        fObjHealth.Post1_FY_Revenue__c=0;
                        fObjHealth.Post_FY_Revenue__c=0;
                        fObjHealth.Pre_Prior_FY_Actual__c=0;
                        fObjHealth.Prior_FY_Projected__c=0;
                        fObjHealth.Prior_Revenue_Actual__c=0;
                        fObjHealth.Prior_LTM_Revenue__c=0;
                        fObjHealth.Account__c=gobj.Account__c;
                        if(gobj.fcstPlanning_Year__c!=null){
                        fObjHealth.Planning_Year__c=gobj.fcstPlanning_Year__c;
                        fObjHealth.Growth_Plan__c=gobj.Id;
                        forecastModelNewList.add(fObjHealth);  
                        }
                      }
                   }
                   else if(gobj.Planning_Year__c == '2018'){
                            for(Integer i=1;i<34;i++){
                                Forecast_Model__c fObjHealth= new Forecast_Model__c();
                                
                                //for Health LOb
                                
                                if(i==1){
                                  fObjHealth.LOB__c='Health';
                                 }
                                if(i==2){
                                    fObjHealth.Sub_Business__c='Associations';
                                    fObjHealth.LOB__c='Health';
                                }
                                if(i==3){
                                    fObjHealth.Sub_Business__c='B2B';
                                    fObjHealth.LOB__c='Health';
                                }
                                if(i==4){
                                    fObjHealth.Sub_Business__c='Government';
                                    fObjHealth.LOB__c='Health';
                                }
                                if(i==5){
                                    fObjHealth.Sub_Business__c='Marketplace 365';
                                    fObjHealth.LOB__c='Health';
                                }
                                if(i==6){
                                    fObjHealth.Sub_Business__c='Mercer Admin';
                                    fObjHealth.LOB__c='Health';
                                }
                                if(i==7){
                                    fObjHealth.Sub_Business__c='Voluntary Benefits';
                                    fObjHealth.LOB__c='Health';
                                }
                                
                                if(i==8){
                                    fObjHealth.Sub_Business__c='Health - Large Market Admin';
                                    fObjHealth.LOB__c='Health';
                                }
                                
                                //for Wealth LOb
                               
                                if(i==9){
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==10){
                                    fObjHealth.Sub_Business__c='Administration';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==11){
                                    fObjHealth.Sub_Business__c='Defined Benefits';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==12){
                                    fObjHealth.Sub_Business__c='Defined Contribution & Financial Wellness - Consumer';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==13){
                                    fObjHealth.Sub_Business__c='Defined Contribution & Financial Wellness - Institutional';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==14){
                                    fObjHealth.Sub_Business__c='Leadership & Operations - Institutional';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==15){
                                    fObjHealth.Sub_Business__c='Non-Pension and Specialties';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==16){
                                    fObjHealth.Sub_Business__c='Inv CIO';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==17){
                                    fObjHealth.Sub_Business__c='Wealth - Large Market Admin';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                if(i==18){
                                    fObjHealth.Sub_Business__c='Wealth Administration';
                                    //fObjHealth.Sub_Business__c='Delegated Solutions';
                                    fObjHealth.LOB__c='Wealth';
                                }
                                
                                
                                //for Career LOB
                                
                                if(i==19){
                                    fObjHealth.LOB__c='Career';
                                }
                                if(i==20){
                                    fObjHealth.Sub_Business__c='Career-Products';
                                    fObjHealth.LOB__c='Career';
                                }
                                if(i==21){
                                    fObjHealth.Sub_Business__c='Career-Services';
                                    fObjHealth.LOB__c='Career';
                                }
                                if(i==22){
                                    fObjHealth.Sub_Business__c='Career-Workday';
                                    fObjHealth.LOB__c='Career';
                                }
                                if(i==23){
                                    fObjHealth.Sub_Business__c='Career - Career';
                                    fObjHealth.LOB__c='Career';
                                }
                                
                                 //for Career GBS
                                 
                                 if(i==24){
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==25){
                                    fObjHealth.Sub_Business__c='Intellectual Capital Solutions';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==26){
                                    fObjHealth.Sub_Business__c='Global Consulting';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==27){
                                    fObjHealth.Sub_Business__c='Global Career';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==28){
                                    fObjHealth.Sub_Business__c='Global Health';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==29){
                                    fObjHealth.Sub_Business__c='Global Wealth';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==30){
                                    //fObjHealth.Sub_Business__c='Innovation & M&A Community';
                                    fObjHealth.Sub_Business__c='Mercer Powered By Darwin';
                                   fObjHealth.LOB__c='GBS';
                                 }
                                if(i==31){
                                    fObjHealth.Sub_Business__c='WRG';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                if(i==32){
                                    fObjHealth.Sub_Business__c='MCG Leadership/GCM';
                                    fObjHealth.LOB__c='GBS';
                                 }
                                
                                //for Other LOB
                                if(i==33){
                                    fObjHealth.LOB__c='Other';
                                 }
                                
                                fObjHealth.Current_Carry_Forward_Revenue__c=0;
                                fObjHealth.Current_FY_Revenue__c=0;
                                fObjHealth.Post1_FY_Revenue__c=0;
                                fObjHealth.Post_FY_Revenue__c=0;
                                fObjHealth.Pre_Prior_FY_Actual__c=0;
                                fObjHealth.Prior_FY_Projected__c=0;
                                fObjHealth.Prior_Revenue_Actual__c=0;
                                fObjHealth.Prior_LTM_Revenue__c=0;
                                fObjHealth.Account__c=gobj.Account__c;
                                if(gobj.fcstPlanning_Year__c!=null){
                                fObjHealth.Planning_Year__c=gobj.fcstPlanning_Year__c;
                                fObjHealth.Growth_Plan__c=gobj.Id;
                                forecastModelNewList.add(fObjHealth);  
                                }
                              }
                      
                      }
                   else if(gobj.Planning_Year__c == '2020'){
                      for(Integer i=1; i<49; i++){
                        Forecast_Model__c fObjHealth= new Forecast_Model__c();
                        
                        //for Health LOb
                        
                        if(i==1){
                            fObjHealth.LOB__c='Health';
                         }
                        if(i==2){
                            fObjHealth.Sub_Business__c='Health Unassigned';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==3){
                            fObjHealth.Sub_Business__c='Associations';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==4){
                            fObjHealth.Sub_Business__c='B2B';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==5){
                            fObjHealth.Sub_Business__c='GBM';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==6){
                            fObjHealth.Sub_Business__c='Government';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        if(i==7){
                            fObjHealth.Sub_Business__c='Health - Large Market Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==8){
                            fObjHealth.Sub_Business__c='Marketplace 365';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        if(i==9){
                            fObjHealth.Sub_Business__c='Mercer Admin';
                            fObjHealth.LOB__c='Health';
                        }
                        if(i==10){
                            fObjHealth.Sub_Business__c='Voluntary Benefits';
                            fObjHealth.LOB__c='Health';
                        }
                        
                        
                        //for Wealth LOb
                       
                        if(i==11){
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==12){
                            fObjHealth.Sub_Business__c='Wealth Unassigned';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==13){
                            fObjHealth.Sub_Business__c='Administration';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==14){
                            fObjHealth.Sub_Business__c='DCs & Financial Wellness - Consumer';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==15){
                            fObjHealth.Sub_Business__c='DCs & Financial Wellness - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==16){
                            fObjHealth.Sub_Business__c='Defined Benefits';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==17){
                            fObjHealth.Sub_Business__c='Delegated Solutions';
                            fObjHealth.LOB__c='Wealth';
                        }
                        
                        if(i==18){
                            fObjHealth.Sub_Business__c='Inv CIO';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==19){
                            fObjHealth.Sub_Business__c='Leadership & Operations - Institutional';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==20){
                            fObjHealth.Sub_Business__c='Non-Pension and Specialties';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==21){
                            fObjHealth.Sub_Business__c='Summit';
                            fObjHealth.LOB__c='Wealth';
                        }
                        if(i==22){
                            fObjHealth.Sub_Business__c='Wealth - Large Market Admin';
                            fObjHealth.LOB__c='Wealth';
                        }
                        
                        //for Career LOB
                        
                        if(i==23){
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==24){
                            fObjHealth.Sub_Business__c='Career Unassigned';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==25){
                            fObjHealth.Sub_Business__c='Career - Career';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==26){
                            fObjHealth.Sub_Business__c='Career - Digital';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==27){
                            fObjHealth.Sub_Business__c='Career - Products';
                            fObjHealth.LOB__c='Career';
                        }
                        if(i==28){
                            fObjHealth.Sub_Business__c='Career - Services';
                            fObjHealth.LOB__c='Career';
                        }
                        
                        
                         //for Career GBS
                         
                         if(i==29){
                            fObjHealth.LOB__c='GBS';
                         }
                         if(i==30){
                            fObjHealth.Sub_Business__c='GBS Unassigned';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==31){
                            fObjHealth.Sub_Business__c='GBS Health';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==32){
                            fObjHealth.Sub_Business__c='Global Career';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==33){
                            fObjHealth.Sub_Business__c='Global Consulting';
                            fObjHealth.LOB__c='GBS';
                        }
                        
                        if(i==34){
                            fObjHealth.Sub_Business__c='Global Wealth';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==35){
                            fObjHealth.Sub_Business__c='Innovation & M&A Community';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==36){
                            fObjHealth.Sub_Business__c='Intellectual Capital Solutions';
                            fObjHealth.LOB__c='GBS';
                        }
                        
                        if(i==37){
                            fObjHealth.Sub_Business__c='MCG Leadership/GCM';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==38){
                            fObjHealth.Sub_Business__c='Thomsons';
                            fObjHealth.LOB__c='GBS';
                        }
                        if(i==39){
                             fObjHealth.Sub_Business__c='WRG';
                             fObjHealth.LOB__c='GBS';
                         }
                        
                        //for Global Planning
                        if(i==40){
                            fObjHealth.LOB__c='Global Planning';
                         }
                         
                        if(i==41){
                            fObjHealth.LOB__c='Global Planning';
                            fObjHealth.Sub_Business__c='Global Planning Health';
                         }
                         if(i==42){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Wealth';
                           }
                         if(i==43){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Career';
                          }
                         if(i==44){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Global Planning Consulting';
                           }
                         if(i==45){
                             fObjHealth.LOB__c='Global Planning';
                             fObjHealth.Sub_Business__c='Mercer Powered By Darwin';
                           }
                          if(i==46){
                              fObjHealth.LOB__c='Global Planning';
                              fObjHealth.Sub_Business__c='Cross OpCo Solutions - Excl from Rollups';
                         }
                         if(i==47){
                              fObjHealth.LOB__c='Other';
                         }
                         if(i==48){
                              fObjHealth.LOB__c='Other';
                              fObjHealth.Sub_Business__c='Other Unassigned';
                         }
                          
                        fObjHealth.Current_Carry_Forward_Revenue__c=0;
                        fObjHealth.Current_FY_Revenue__c=0;
                        fObjHealth.Post1_FY_Revenue__c=0;
                        fObjHealth.Post_FY_Revenue__c=0;
                        fObjHealth.Pre_Prior_FY_Actual__c=0;
                        fObjHealth.Prior_FY_Projected__c=0;
                        fObjHealth.Prior_Revenue_Actual__c=0;
                        fObjHealth.Prior_LTM_Revenue__c=0;
                        fObjHealth.Account__c=gobj.Account__c;
                        if(gobj.fcstPlanning_Year__c!=null){
                        fObjHealth.Planning_Year__c=gobj.fcstPlanning_Year__c;
                        fObjHealth.Growth_Plan__c=gobj.Id;
                        forecastModelNewList.add(fObjHealth);  
                        }
                      } 
                   }
                }
            }
        }
        catch(Exception e){
            System.debug('EXCEPTION: ' + e);
        }
        if(forecastModelNewList.size()>0)
            insert forecastModelNewList;
    }
}