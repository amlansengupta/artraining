/***************************************************************************************************************
Request Id : 17560
Purpose : This apex class is used to display Marketo Lead Scores on the Marketing Engagement Tab in Contacts.
Created by : Archisman Ghosh
Created Date : 04/03/2019
Project : MF2
****************************************************************************************************************/
public class ContactAdditionalDetailsController
{
    @AuraEnabled
    public static boolean isRunningUserInParentHierarchyCon(Id recordId) {
        boolean isNotChild = true;
        try{
            List<Profile> runningUserProfileList = [Select Name from Profile where Id=:userInfo.getProfileId()];
            String runningUserProfileName;
            if(runningUserProfileList != null && runningUserProfileList.size() > 0){
                runningUserProfileName = runningUserProfileList.get(0).Name;
            }
            if(runningUserProfileName != null && runningUserProfileName.equalsIgnoreCase('System Administrator')){
                return true;
            }
            List<Contact> conList = [Select Owner.UserRoleId from Contact where Id=:recordId];
            Id userRoleId;
            if(conList != null && conList.size() > 0){
                userRoleId = conList.get(0).Owner.UserRoleId;
            }
            Set<Id> ownerRoleIds = new Set<Id>();
            ownerRoleIds.add(userRoleId);
            Set<Id> childRoleIds = getAllSubRoleIds(ownerRoleIds);
            if(childRoleIds != null){
                for(Id childRoleId : childRoleIds){
                    if(childRoleId == UserInfo.getUserRoleId()){
                        isNotChild = false;
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'UserRoleHierarchyUtil', 'isRunningUserInParentHierarchy');
        }
        return isNotChild;
    }
    
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        
        Set<ID> currentRoleIds = new Set<ID>();
        
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
                                IN :roleIds AND ParentRoleID != null]) {
                                    currentRoleIds.add(userRole.Id);
                                }
        
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0) {
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        }
        
        return currentRoleIds;
    }
    
    /*
*  @Method Name: getContact
*  @Description: Method gets Contact fields
*  @Args : String 
*/
    @AuraEnabled
    public static Contact getContact(String conid) 
    {
        Contact cnct = [select id,M_A__c,
                        Benefits_Advisory__c,HR_Transformation__c,Core_Brokerage__c,Defined_Benefit_Pension_Risk__c,
                        Mobility_Talent_IS__c,Defined_Contribution__c,Non_Medical_Voluntary_Benefits__c,
                        Endowment_Foundations_Mgmt__c,Private_Exchange__c,Executive_Rewards__c,Talent_Strategy__c,
                        Financial_Wellness__c,Wealth_Management__c,Global_Benefits__c,Workforce_Rewards__c
                        From Contact Where Id =: conid limit 1];
        return cnct;
    }
    
    /*
*  @Method Name: saveContact
*  @Description: Method to save Contact fields
*  @Args : String 
*/
    @AuraEnabled
    public static String saveContact (String cnct) {
        
        Contact updateRecord=new Contact();
        Map<String,Object> deserializedData = (Map<String,Object>)JSON.deserializeUntyped(cnct);
        
        updateRecord.Id = (Id)deserializedData.get('Id');
        
        if(deserializedData.get('Mac')!=null){
            updateRecord.M_A__c = Decimal.valueOf((String)deserializedData.get('Mac'));
        } 
        if(deserializedData.get('Bac')!=null){
            updateRecord.Benefits_Advisory__c = Decimal.valueOf((String)deserializedData.get('Bac'));
        }
        if(deserializedData.get('Hr')!=null){
            updateRecord.HR_Transformation__c = Decimal.valueOf((String)deserializedData.get('Hr'));
        }
        if(deserializedData.get('Cb')!=null){
            updateRecord.Core_Brokerage__c = Decimal.valueOf((String)deserializedData.get('Cb'));
        }
        if(deserializedData.get('DefinedBenefit')!=null){
            updateRecord.Defined_Benefit_Pension_Risk__c = Decimal.valueOf((String)deserializedData.get('DefinedBenefit'));
        }
        if(deserializedData.get('MobilityTalent')!=null){
            updateRecord.Mobility_Talent_IS__c = Decimal.valueOf((String)deserializedData.get('MobilityTalent'));
        }
        if(deserializedData.get('DefinedContribution')!=null){
            updateRecord.Defined_Contribution__c = Decimal.valueOf((String)deserializedData.get('DefinedContribution'));
        }
        if(deserializedData.get('NonMedical')!=null){
            updateRecord.Non_Medical_Voluntary_Benefits__c = Decimal.valueOf((String)deserializedData.get('NonMedical'));
        }
        if(deserializedData.get('EndowmentFoundations')!=null){
            updateRecord.Endowment_Foundations_Mgmt__c = Decimal.valueOf((String)deserializedData.get('EndowmentFoundations'));
        }
        if(deserializedData.get('Private_Exchange__c')!=null){
            updateRecord.Private_Exchange__c = Decimal.valueOf((String)deserializedData.get('Private_Exchange__c'));
        }
        if(deserializedData.get('Executive_Rewards__c')!=null){
            updateRecord.Executive_Rewards__c = Decimal.valueOf((String)deserializedData.get('Executive_Rewards__c'));
        }
        if(deserializedData.get('Talent_Strategy__c')!=null){
            updateRecord.Talent_Strategy__c = Decimal.valueOf((String)deserializedData.get('Talent_Strategy__c'));
        }
        if(deserializedData.get('Financial_Wellness__c')!=null){
            updateRecord.Financial_Wellness__c = Decimal.valueOf((String)deserializedData.get('Financial_Wellness__c'));
        }
        if(deserializedData.get('Wealth_Management__c')!=null){
            updateRecord.Wealth_Management__c = Decimal.valueOf((String)deserializedData.get('Wealth_Management__c'));
        }
        if(deserializedData.get('Global_Benefits__c')!=null){
            updateRecord.Global_Benefits__c = Decimal.valueOf((String)deserializedData.get('Global_Benefits__c'));
        }
        if(deserializedData.get('Workforce_Rewards__c')!=null){
            updateRecord.Workforce_Rewards__c = Decimal.valueOf((String)deserializedData.get('Workforce_Rewards__c'));
        }
        
        
        update updateRecord;
        //System.debug('bang:'+updateRecord.Id);
        return updateRecord.Id;
    }
}