/*Purpose:  This is a Controller Class from Product Search and Add Product Page . Where we search,Add product, Reallocate Revenue, Save, Save & More and also Cancel the event.
 ==============================================================================================================================================
The methods called perform following functionality:

•   Searches for a Product.
•   Adds the selected product in Product entry section.
•   Reallocates the revenue.
•   Saves Opportunity Product and again opens the product selection page to add more Products.
  
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   01/14/2014  As per March 2014 Release(Request # 2940)Created Controller Class
   2.0 -    Sarbpreet   01/12/2015  As part of PMO Request 5341 (February 2015 Release) calculated Annualized Revenue (calculated) field on click of Allocate Revenue button.
   3.0 -    Sarbpreet   08/03/2015  As part of PMO Request# 5422(September 2015 Release) added DS:Sales Leader field in selectPBE function
============================================================================================================================================= 
*/
public with sharing class AP95_ProductSearchController implements AP92_ObjectPaginatorListener {

    private ApexPages.StandardController controller;
    
    public String solSegment {get;set;}
    public String cluster {get;set;}
    public String subBusiness {get;set;}
	
    
    public Opportunity opp {
        get;
        set;
    }
    private OpportunityLineItem opptyLineItemObj;
    
    private OpportunityLineItem oli;
    
    public Id oppId {
        get;
        set;
    }
    private String soql;

    // the soql without the order and limit
    public transient string PR4 {
        get;
        set;
    }
    public transient String PR3 {
        get;
        set;
    }
    public transient string PR2 {
        get;
        set;
    }
    public transient string PR1 {
        get;
        set;
    }

    public string pricebookName {
        get;
        set;
    }
    public string currencyISO {
        get;
        set;
    }
    public String searchString {
        get;
        set;
    }

    public String Selected {
        get;
        set;
    }
    public Boolean isSearchHaveValue {
        get;
        set;
    }
    
    public Decimal annualizedRevenuecalculated {
      get;
      set;      
    }
    
    public AP94_ObjectPaginator paginator {
        get;
        private set;
    }



    private string recordIds = '';
    private string pricebookId = '';
    public Opportunity theOpp {
        get;
        set;
    }
    public OpportunityLineItem OliObj {
        get;
        set;
    }

    public List < PricebookEntry > ListpbeObj {
        get;
        set;
    }
    public List < PriceBookEntry > priceBookEntry {
        get {
            if (priceBookEntry == null) {
                priceBookEntry = new List < PriceBookEntry > ();
            }
            return priceBookEntry;
        }
        set;
    }

    public List < PriceBookEntry > priceBookEntryAll {
        get {
            if (priceBookEntryAll == null) {
                priceBookEntryAll = new List < PriceBookEntry > ();
            }
            return priceBookEntryAll;
        }
        set;
    }



    // the current sort direction. defaults to asc
    public String sortDir {
        get {
            if (sortDir == null) {
                sortDir = 'asc';
            }
            return sortDir;
        }
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField {
        get {
            if (sortField == null) {
                sortField = 'p.Product2.Name';
            }
            return sortField;
        }
        set;
    }

    // format the soql for display on the visualforce page
    public String debugSoql {
        get {
            return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20';
        }
        set;
    }


   /*
    * @Description : This method toggles the sorting of query from asc<-->desc     
    * @ Args       : null     
    * @ Return     : void
    */ 
     public void toggleSort() {
        try {
            // simply toggle the direction
            sortDir = sortDir.equals('asc') ? 'desc' : 'asc';           
            // run the query again

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
    }

    /*
     * @Description : This method clears the search filters.     
     * @ Args       : null     
     * @ Return     : void
     */
    public void clearSearch() {
        try {
            if (PR4 != null) {
                PR4 = '';
            }
            if (PR3 != null) {
                PR3 = '';
            }
            if (PR2 != null) {
                PR2 = '';
            }
            if (PR1 != null) {
                PR1 = '';
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
    }


    /*
     * @Description : This method runs the actual query.
     * @ Args       : null
     * @ Return     : void
     */
    public void runQuery() {

        try {
            priceBookEntry.clear();
            ListpbeObj = Database.query(soql + ' order by ' + sortField + ' ' + sortDir);
            String test = soql + ' order by ' + sortField + ' ' + sortDir;

            List < PriceBookEntry > paginatePBEntry = new List < PriceBookEntry > ();
            if (ListpbeObj != null && ListpbeObj.size() > 0) {
                isSearchHaveValue = false;
                for (PricebookEntry pbe: ListpbeObj) {
                    paginatePBEntry.add(pbe);
                }

                //5 is pageSize, this refers to this class which acts as listener to paginator
                paginator = new AP94_ObjectPaginator(10, this);
                paginator.setRecords(paginatePBEntry);
            } else {
                isSearchHaveValue = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Parameter entered for search did not return any product'));
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }

    }
    /*
     * @Description : This method is used for pagination.
     * @ Args       : null
     * @ Return     : void 
     */
    public void handlePageChange(List < Object > newPage) {
        try {
            priceBookEntry.clear();
            if (newPage != null) {
                for (Object pbEntrey: newPage) {
                    priceBookEntry.add((PriceBookEntry) pbEntrey);
                    priceBookEntryAll.add((PriceBookEntry) pbEntrey);
                }
            }
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
    }
    
    /*
     * @Description : Constructor of Controller class 
     * @ Args       : controller
     * @ Return     : void 
     */
    public AP95_ProductSearchController(ApexPages.StandardController controller) {
        this.controller = controller;
        oppId = ApexPages.currentPage().getParameters().get('id');
        isSearchHaveValue = true;           
        if (ApexPages.currentPage().getParameters().get('id') != null) {
            isSearchHaveValue = true;
            recordIds = ApexPages.currentPage().getParameters().get('id');
            theOpp = database.query('select Id,name,Pricebook2Id,closeDate, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
            Id pb = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
            list < opportunity > listOpptyObj = [select id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from opportunity where id = : recordIds LIMIT 1];
            if (listOpptyObj.size() > 0) {
                for (Opportunity op: listOpptyObj) {
                    op.Pricebook2Id = pb;
                    pricebookId = op.Pricebook2Id;
                    pricebookName = op.Pricebook2.Name;
                    currencyISO = op.CurrencyIsoCode;

                }
                if (PricebookId != null) {
                    soql = 'Select p.UnitPrice, p.ProductCode, p.Product2.Family, p.Product2.Description, p.Product2.Name,p.Product2.Sub_Business__c,p.Product2.Segment__c,p.Product2.Cluster__c, p.Product2.LOB__c,p.Product2Id, p.Pricebook2Id, p.Name, p.IsActive, p.Id From PricebookEntry p where p.Pricebook2Id=\'' + pricebookId + '\'and  p.CurrencyIsoCode =\'' + currencyISO + '\' and p.Product2.IsActive=true and p.IsActive=true ';
                } else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.'));
                }

                // run the query again
                runQuery();
                listOpptyObj.clear();
                
               

            }
        }
    }


    /*
     * @Description : This method filters and searches for a product    
     * @ Args       : null   
     * @ Return     : PageReference 
     */
    public PageReference runSearch() {
        try {
            if (PricebookId != null) {

                soql = 'Select p.UnitPrice, p.ProductCode, p.Product2.Family, p.Product2.Description, p.Product2.Name,p.Product2.Segment__c,p.Product2.Cluster__c,p.Product2.Sub_Business__c,p.Product2.LOB__c, p.Product2Id, p.Pricebook2Id, p.Name, p.IsActive, p.Id From PricebookEntry p where p.Pricebook2Id=\'' + pricebookId + '\'and p.CurrencyIsoCode =\'' + currencyISO + '\'  and p.Product2.IsActive=true and p.IsActive=true ';

                if (PR4 != null) {
                    soql += ' and  (p.Product2.Name LIKE \'%' + String.escapeSingleQuotes(PR4) + '%\' or p.Product2.Segment__c LIKE \'%' + String.escapeSingleQuotes(PR4) + '%\'or p.Product2.Cluster__c LIKE \'%' + String.escapeSingleQuotes(PR4) + '%\' or p.Product2.LOB__c LIKE \'%' + String.escapeSingleQuotes(PR4) + '%\'or p.Product2.Sub_Business__c LIKE \'%' + String.escapeSingleQuotes(PR4) + '%\')';
                }


                if (PR3.equals('Product Name') && PR1.equals('Equals') && PR2 != null) {
                    soql += ' and  (p.Product2.Name = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (PR3.equals('Sub Business') && PR1.equals('Equals') && PR2 != null) {
                    soql += ' and  (p.Product2.Sub_Business__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }
                
                if (PR3.equals('Solution Segment') && PR1.equals('Equals') && PR2 != null) {
                    soql += ' and  (p.Product2.Segment__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (PR3.equals('Cluster') && PR1.equals('Equals') && PR2 != null) {
                    soql += ' and  (p.Product2.Cluster__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (PR3.equals('Product LOB') && PR1.equals('Equals') && PR2 != null) {
                    soql += ' and  (p.Product2.LOB__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (PR3.equals('Product Name') && PR1.equals('Contains') & PR2 != null) {
                    soql += 'and (p.Product2.Name LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }
                
                if (PR3.equals('Sub Business') && PR1.equals('Contains') && PR2 != null) {
                    soql += ' and  (p.Product2.Sub_Business__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }
		
                if (PR3.equals('Solution Segment') && PR1.equals('Contains') & PR2 != null) {
                    soql += 'and (p.Product2.Segment__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Cluster') && PR1.equals('Contains') & PR2 != null) {
                    soql += 'and (p.Product2.Cluster__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Product LOB') && PR1.equals('Contains') & PR2 != null) {
                    soql += 'and (p.Product2.LOB__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Product Name') && PR1.equals('Starts With') & PR2 != null) {
                    soql += 'and (p.Product2.Name LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }
                
                if (PR3.equals('Sub Business') && PR1.equals('Starts With') && PR2 != null) {
                    soql += ' and  (p.Product2.Sub_Business__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Solution Segment') && PR1.equals('Starts With') & PR2 != null) {
                    soql += 'and (p.Product2.Segment__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Cluster') && PR1.equals('Starts With') & PR2 != null) {
                    soql += 'and (p.Product2.Cluster__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (PR3.equals('Product LOB') && PR1.equals('Starts With') & PR2 != null) {
                    soql += 'and (p.Product2.LOB__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }
                
                // run the query again
                runQuery();

            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No pricebook is associated with the Opportunity. Please update Pricebook of the Opportunity.'));
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        return null;
    }


    /*
   * @Description : This method created a new Opportunityline item when a product is selected and displayes it in 
                    Product entry section.
   * @ Args       : null     
   * @ Return     : PageReference     
  */
    public PageReference selectPBE() {

        try {
            for (PriceBookEntry pb: priceBookEntryAll) {
                
                if ((String) pb.Id == Selected) {
                    //As part of PMO Request# 5422(September 2015 Release) added DS:Sales Leader field
                    opptyLineItemObj = new OpportunityLineItem( OpportunityId = recordIds, PriceBookEntry = pb, PricebookEntryId = pb.Id, UnitPrice = pb.UnitPrice, 
                                        CurrentYearRevenue_edit__c = 0.0, Year2Revenue_edit__c = 0.0, Year3Revenue_edit__c = 0.0, LOB_Old__c = pb.Product2.LOB__c, 
                                        Revenue_Start_Date__c = null, Revenue_End_Date__c = null, Duration__c = null, Number_Of_Lives__c = 0.0, Assets_Under_Mgmt__c = 0.0, 
                                        Inv_Segment__c = null, Inv_Client_Type__c = '', Inv_Revenue_Type__c = '', Net_Revenue__c = 0.0, DS_Sales_Leader__c=null);
                    
                    solSegment = pb.Product2.Segment__c;
                    cluster = pb.Product2.Cluster__c;
                    subBusiness = pb.Product2.Sub_Business__c;
                    OliObj = opptyLineItemObj;

                    break;
                }
            }

            PageReference pageRef = ApexPages.currentPage();

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
        return null;
    }


    /*
     * @Description : This method calculates and allocates revenue to Opportunity Product
     * @ Args       : null   
     * @ Return     : void
     */
    public void reallocateRevenue() {
        try {
            oli = opptyLineItemObj;
            Date rsdt = oli.revenue_start_date__c;
            Date redt = oli.revenue_end_date__c;
            Integer cyr = system.today().year();
            Double dbstend = (rsdt.daysbetween(redt) + 1);
            
            //As part of PMO Request 5341 (February 2015 Release) calculated Annualized Revenue (calculated) field on click of Allocate Revenue button.
            Integer noOfDays = rsdt.daysBetween(redt);
            Integer rdt = noOfDays + 1;
            Decimal tempTotal = (rdt / 365.242199);
            Decimal total = tempTotal.setscale(1);
			total = total.scale();
          
            if (rsdt.year() <> cyr) {
                oli.CurrentYearRevenue_edit__c = 0;
            }            else if(redt.year() - rsdt.year() == 0) {
                oli.CurrentYearRevenue_edit__c = oli.UnitPrice;
            }  else {
                Double dbtw = rsdt.daysbetween(date.newInstance(system.today().year(), 12, 31)); //days between revenue start date and end of current year
                Double dbtw2 = rsdt.daysbetween(oli.Revenue_End_Date__c); // days between revenue start date and revenue end date
                Double divn = ((dbtw + 1) / (dbtw2 + 1));
                Decimal cyrev = divn * oli.UnitPrice;               
                oli.CurrentYearRevenue_edit__c = Math.round(divn * oli.UnitPrice); // cyrev;
            }

            if (redt.year() == rsdt.year()) {
                if (redt.year() == cyr + 2) {
                    oli.Year2Revenue_edit__c = 0;
                } else if (redt.year() == cyr + 1) {
                    oli.Year2Revenue_edit__c = oli.UnitPrice;
                } else {
                    oli.Year2Revenue_edit__c = 0;
                }
            } else if (rsdt.year() == cyr) {
                if (redt.year() == cyr + 1) {
                    Double dbtw = date.newInstance(cyr + 1, 1, 1).daysbetween(redt) + 1;
                    oli.Year2Revenue_edit__c = Math.round((dbtw / dbstend) * oli.UnitPrice);
                } else {
                    Double calc = 365.0 / dbstend;
                    oli.Year2Revenue_edit__c = Math.round(calc * oli.UnitPrice);
                }
            } else if (rsdt.year() == cyr + 1) {
                Double dbtw = rsdt.daysbetween(date.newInstance(cyr + 1, 12, 31)) + 1;
                oli.Year2Revenue_edit__c = Math.round((dbtw / (dbstend)) * oli.UnitPrice);
            } else {
                oli.Year2Revenue_edit__c = 0;
            }

            oli.Year3Revenue_edit__c = oli.unitprice - (Math.round(oli.Year2Revenue_edit__c)) - Math.round(oli.CurrentYearRevenue_edit__c);
           
           //As part of PMO Request 5341 (February 2015 Release) calculated Annualized Revenue (calculated) field on click of Allocate Revenue button.
            If( Math.round(total) < 1) {
                annualizedRevenuecalculated = oli.UnitPrice; 
            }
            else {        
                annualizedRevenuecalculated =Math.round( oli.UnitPrice / (total));
            }
           
            
            
            
        
        }catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }

    }



    /*
     * @Description : This method is called when Cancel button is clicked.
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public PageReference cancelButton() {
        PageReference pageReference;
        try {
            pageReference = new PageReference('/' + recordIds);
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            pageReference = null;
        }
        return pageReference;
    }


    /*
     * @Description : This method saves the Opportunity Product and returns to Opportunity page.  
     * @ Args       : null 
     * @ Return     : PageReference 
     */

       public PageReference saveButton() {
        PageReference pageReference;
        try {
         
            Database.insert(OliObj);
            pageReference = new PageReference('/' + recordIds);
        } catch (System.DmlException e) {
            if(e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id')) 
            {
            e.setMessage('You do not have the level of access necessary to add a product. Please contact the owner of the opportunity.');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            }
            else if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            //e.setMessage('');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
            }
            else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            }
            pageReference = null;
        }
        return pageReference;
    }

    /*
     * @Description : This method saves the Opportunity Product and again opens Opportunity Product Selection page to add more Products.  
     * @ Args       : null 
     * @ Return     : PageReference 
     */
    public PageReference saveMorebutton() {
        PageReference pageReference;
        try {
            Database.insert(OliObj);
            pageReference = new PageReference('/apex/Mercer_ProductSearch?addTo=' + recordIds + '&retURL=%2F' + recordIds + '&sfdc.override=1&id=' + recordIds);
            pageReference.setRedirect(true);
            controller.save();
            
        }
       
        catch (System.DmlException e) {
            if(e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id')) 
            {
            e.setMessage('You do not have the level of access necessary to add a product. Please contact the owner of the opportunity.');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            }
            else if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            e.setMessage('');
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            }
            else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
            }
            pageReference = null;
        }
            return pageReference; 
        
    }

}