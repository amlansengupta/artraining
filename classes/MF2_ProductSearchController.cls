/*****************************************************************************************************************************
User story : 17673 (epic)
Purpose : This apex class is created for the product search page.
Created by : Soumil Dasgupta
Created Date : 29/03/2019
Project : MF2
******************************************************************************************************************************/
global class MF2_ProductSearchController {
    
    //This list of PridcebookWrapper is used to hold the price book entry records and the indexes for each.
    public static List<PricebookWrapper> listIndxPBEnry= new List<PricebookWrapper>();
    
    
    // Thgis method is used to fetch the picklist fields values of Product Attribute fields which are used in the search criteria.
    @AuraEnabled
    public static prodFieldsWrapper productFieldsInitialize(String oppId){
        try{
            Map<String, Set<String>> mapSolSegCluster = new Map<String, Set<String>>();
            Map<String, Set<String>> mapSubBussSolSeg = new Map<String, Set<String>>();
            Map<String, Set<String>> mapLOBSubBuss = new Map<String, Set<String>>();
            
            if(oppId != null && oppId != ''){
                String currencyISOCode = [Select ID, Name, Pricebook2Id, CurrencyIsoCode From Opportunity where ID =: oppID].CurrencyIsoCode;
                List<PricebookEntry> lstProductFieldsValue = [Select Product2.Segment__c, Product2.Cluster__c, Product2.Sub_Business__c, Product2.LOB__c From PricebookEntry where IsActive=true and Product2.IsActive=true and CurrencyIsoCode =: currencyISOCode];
                
                
                if(!lstProductFieldsValue.isEmpty() && lstProductFieldsValue != null){
                    for(PricebookEntry p : lstProductFieldsValue){
                        String sscKey = p.Product2.LOB__c + '_' + p.Product2.Sub_Business__c + '_' + p.Product2.Segment__c;
                        if(p.Product2.Segment__c != null && p.Product2.Cluster__c != null){
                            if(mapSolSegCluster.keySet().contains(sscKey)){
                                mapSolSegCluster.get(sscKey).add(p.Product2.Cluster__c); 
                            }
                            else{
                                Set<String> tempSet = new Set<String>();
                                tempSet.add(p.Product2.Cluster__c);
                                mapSolSegCluster.put(sscKey,tempSet);
                            }
                        }   
                    }
                    for(PricebookEntry p : lstProductFieldsValue){
                        String sbssKey = p.Product2.LOB__c + '_' + p.Product2.Sub_Business__c;
                        if(p.Product2.Sub_Business__c != null && p.Product2.Segment__c != null){
                            if(mapSubBussSolSeg.keySet().contains(sbssKey)){
                                mapSubBussSolSeg.get(sbssKey).add(p.Product2.Segment__c); 
                            }
                            else{
                                Set<String> tempSet = new Set<String>();
                                tempSet.add(p.Product2.Segment__c);
                                mapSubBussSolSeg.put(sbssKey,tempSet);
                            }
                        }
                    }
                    for(PricebookEntry p : lstProductFieldsValue){
                        if(p.Product2.LOB__c != null && p.Product2.Sub_Business__c != null){    
                            if(mapLOBSubBuss.keySet().contains(p.Product2.LOB__c)){
                                mapLOBSubBuss.get(p.Product2.LOB__c).add(p.Product2.Sub_Business__c); 
                            }
                            else{
                                Set<String> tempSet = new Set<String>();
                                tempSet.add(p.Product2.Sub_Business__c);
                                mapLOBSubBuss.put(p.Product2.LOB__c,tempSet);
                            }
                        }
                    }
                 }
            }
            
            prodFieldsWrapper prodFieldsWrapper= new prodFieldsWrapper(mapSolSegCluster,mapSubBussSolSeg,mapLOBSubBuss);
            return prodFieldsWrapper;
        }
        catch(Exception ex){
            system.debug(ex);
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'productFieldsInitialize');
            throw new AuraHandledException('Could not fetch Product attribute fields! Please try again later.');
        }
    }
    
    //This method is used to get the searchresult of pricebook Entry (and Products).
    @AuraEnabled
    public static List<PricebookWrapper> fetchPbEntry(String searchKeyWord, String pscCode, String oppID, String lob, String subBuss, String solSeg, String cluster) {
        try{
            Opportunity opp = [Select ID, Name, Pricebook2Id, CurrencyIsoCode From Opportunity where ID =: oppID];
            Id pbSt;
            if(!Test.isRunningTest()){
                pbSt = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
            }
            else{
               pbSt = Test.getStandardPricebookId(); 
            }
            String pricebookID = opp.Pricebook2Id;
            String currencyIsoCode = opp.CurrencyIsoCode;
            if(pbSt != null){
                String genQuery = 'Select UnitPrice, ProductCode, Product2.ProductCode, Product2.Family, Product2.Description, Product2.Name,Product2.Segment__c,Product2.Cluster__c,Product2.Sub_Business__c,Product2.LOB__c, Product2Id, Pricebook2Id, Name, IsActive, Id From PricebookEntry  where Pricebook2Id =: pbSt and CurrencyIsoCode =: currencyISOCode and Product2.IsActive=true and IsActive=true ';
                if(searchKeyWord != null && searchKeyWord != ''){
                    String searchKeyinter = '%' + searchKeyWord + '%';
                    String searchKey = String.escapeSingleQuotes(searchKeyinter);
                    genQuery = genQuery + 'AND Product2.Name LIKE \''+ searchKey +'\' ';
                }
                if(pscCode != null && pscCode != ''){
                    String searchpscCode = String.escapeSingleQuotes(pscCode);
                    genQuery = genQuery + 'AND Product2.ProductCode LIKE \''+ searchpscCode +'\' ';
                }
                if(lob != null && lob != '' && lob != '--None--'){
                    genQuery = genQuery+'AND Product2.LOB__c = \''+lob+'\' ';
                }
                if(subBuss != null && subBuss != '' && subBuss != '--None--'){
                    genQuery = genQuery+'AND Product2.Sub_Business__c = \''+subBuss+'\' ';
                }
                if(solSeg != null && solSeg != '' && solSeg != '--None--'){
                    genQuery = genQuery+'AND Product2.Segment__c = \''+solSeg+'\' ';
                }
                if(cluster != null && cluster != '' && cluster != '--None--'){
                    genQuery = genQuery+'AND Product2.Cluster__c = \''+cluster+'\' ';
                }
                genQuery = genQuery + ' Order By Product2.Name';
                system.debug('****'+genQuery);
                //List <PricebookEntry> lstOfPbEntry = [(Product2.Name LIKE: searchKey or Product2.Segment__c LIKE: searchKey or Product2.Cluster__c LIKE: searchKey or Product2.LOB__c LIKE: searchKey or Product2.Sub_Business__c LIKE: searchKey)];
                try{
                    List <PricebookEntry> lstOfPbEntry = Database.query(genQuery);
                    
                    if(!lstOfPbEntry.isEmpty() && lstOfPbEntry != null){
                        for(PricebookEntry pb : lstOfPbEntry){
                            PricebookWrapper pbWrap = new PricebookWrapper(pb,lstOfPbEntry.indexOf(pb));
                            listIndxPBEnry.add(pbWrap);
                        }
                    }
                    
                }
                catch(exception e){
                    System.debug('Error : '+e);
                }
            }
            system.debug('result> '+listIndxPBEnry);
            return listIndxPBEnry;
        }
        catch(Exception ex){
            system.debug('>>>>> '+ex);
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchPbEntry');
            throw new AuraHandledException('Could not fetch Products! Please try after sometime.');
        }
    }
    
    //This method is used to fetch the Opportunity details.
    @AuraEnabled
    public static Opportunity fetchOpportunity(String oppId) {
        try{
            Opportunity opp =[Select ID, Name, Pricebook2Id, CurrencyIsoCode, Closedate From Opportunity where ID =: oppId];
            return opp;
        }
        catch(Exception ex){
            //system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchOpportunity');
            throw new AuraHandledException('Could not fetch Opportunity data! Please try after sometime.');
        }
    }
    
    //This method is used to fetch the Currency Exchange Rate details.
    @AuraEnabled
    public static List<Current_Exchange_Rate__c> fetchCurrEx() {
        try{
            List<Current_Exchange_Rate__c> currEx =[select Name,Conversion_Rate__c from Current_Exchange_Rate__c];
            return currEx;
        }
        catch(Exception ex){
            //system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchCurrEx');
            throw new AuraHandledException('Could not fetch Current Exchange Rate info! Please try after sometime.');
        }
    }
    
    @AuraEnabled
    public static OpportunityLineItem fetchOLI(String oliId) {
        try{
            OpportunityLineItem oli;
            List<OpportunityLineItem> lstOli =[select Name,OpportunityId,Opportunity.CurrencyIsoCode,PriceBookEntry.Product2Id,PriceBookEntry.Product2.Name,
                                               PriceBookEntry.Product2.Segment__c,PriceBookEntry.Product2.LOB__c,Sales_Professional__C,Project_Manager__c,
                                               CurrentYearRevenue_edit__c,Year2Revenue_edit__c,Year3Revenue_edit__c,DS_Sales_Leader__c,
                                               WD_Market_Segment__c,Workday_Region__c,ServiceNow_Market_Segment__c,
                                               Mercer_Influence__c,ServiceNow_Region__c,UnitPrice,Annualized_Revenue_editable__c,Assets_Under_Mgmt__c from OpportunityLineItem where id =: oliId];
            if(!lstOli.isEmpty() && lstOli != null){
                oli = lstOli[0];
            }
            return oli;
        }
        catch(Exception ex){
            //system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchOli');
            throw new AuraHandledException('Could not fetch Opportunity Product info! Please try after sometime.');
        }
    }
    
    //This method is used to update the Product Data.
    //Currently not in use.
    /*@AuraEnabled
public static List<Product2> editProduct(List<Product2> editProduct){
try{
database.update(editProduct);
return editProduct;
}
catch(Exception ex){
//system.debug(ex.getStackTraceString());
ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'editProduct');
throw new AuraHandledException('Could not update Product! Please try after sometime.');
}
}*/
    
    //This method is used to fetch the Favourite Products.
    
    @AuraEnabled
    public static List<PricebookWrapper> fetchFavourites(String oppID, String activeSessionId){
        
        try{
            Set<Id> productList = new Set<Id>();
            system.debug('oppID:'+oppID);
            Id pbSt;
            Opportunity opp = [Select ID, Name, Pricebook2Id, CurrencyIsoCode From Opportunity where ID =: oppID];
            String pricebookID = opp.Pricebook2Id;
            if(!Test.isRunningTest()){
                pbSt = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
                
            }
            else{
                pbSt = Test.getStandardPricebookId();
            }
            String currencyIsoCode = opp.CurrencyIsoCode;
            List<FavoriteUtility.FavoriteEntity> favourites = new List<FavoriteUtility.FavoriteEntity>();
            //if(!Test.isRunningTest()){
            system.debug('activeSessionId :'+activeSessionId);
            favourites = FavoriteUtility.fetchFavorites(activeSessionId);
            //}
            system.debug('**** '+favourites);
            if(favourites != null){
                for(FavoriteUtility.FavoriteEntity f : favourites){
                    if(f.objectType == 'Product2'){
                        productList.add(f.target);
                    }
                }
            }
            system.debug('productList : '+productList);
            if(!productList.isEmpty() && productList != null && pbSt != null){
                List<PricebookEntry> lstOfPbEntry = [Select UnitPrice, ProductCode, Product2.ProductCode, Product2.Family, Product2.Description, Product2.Name,Product2.Segment__c,Product2.Cluster__c,Product2.Sub_Business__c,Product2.LOB__c, Product2Id, Pricebook2Id, Name, IsActive, Id From PricebookEntry where Pricebook2Id =: pbSt and CurrencyIsoCode =: currencyISOCode and Product2.IsActive=true and IsActive=true and Product2Id in: productList];
                system.debug('lstOfPbEntry : '+lstOfPbEntry);
                for(PricebookEntry pb : lstOfPbEntry){
                    PricebookWrapper pbWrap = new PricebookWrapper(pb,lstOfPbEntry.indexOf(pb));
                    listIndxPBEnry.add(pbWrap);
                }
            }
            system.debug('listIndxPBEnry : '+listIndxPBEnry);
            return listIndxPBEnry;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchFavourites');
            throw new AuraHandledException('Could not fetch Favourites info! Please try after sometime.');
        }
    }
    
    //This method is used to fetch the picklist fields values of Digital fields.
    @AuraEnabled
    public static DigitalPksWrapper fetchDigitalPicklist(){
        try{
            List<String> wdMarketList= new List<String>();
            Schema.DescribeFieldResult fieldResult = OpportunityLineItem.WD_Market_Segment__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                wdMarketList.add(pickListVal.getLabel());
            } 
            
            List<String> snMarketList= new List<String>();
            Schema.DescribeFieldResult fieldResult2 = OpportunityLineItem.ServiceNow_Market_Segment__c.getDescribe();
            List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple2){
                snMarketList.add(pickListVal.getLabel());
            }
            
            List<String> wdRegList= new List<String>();
            Schema.DescribeFieldResult fieldResult3 = OpportunityLineItem.Workday_Region__c.getDescribe();
            List<Schema.PicklistEntry> ple3 = fieldResult3.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple3){
                wdRegList.add(pickListVal.getLabel());
            }
            
            List<String> merInfList= new List<String>();
            Schema.DescribeFieldResult fieldResult4 = OpportunityLineItem.Mercer_Influence__c.getDescribe();
            List<Schema.PicklistEntry> ple4 = fieldResult4.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple4){
                merInfList.add(pickListVal.getLabel());
            }
            List<String> serviceNowRegionList= new List<String>();
            Schema.DescribeFieldResult fieldResult5 = OpportunityLineItem.ServiceNow_Region__c.getDescribe();
            List<Schema.PicklistEntry> ple5 = fieldResult5.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple5){
                serviceNowRegionList.add(pickListVal.getLabel());
            }
            DigitalPksWrapper listDigitalPK = new DigitalPksWrapper(wdMarketList, snMarketList, wdRegList, merInfList,serviceNowRegionList); 
            return listDigitalPK;
        }
        catch(Exception ex){
            //system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'fetchDigitalPicklist');
            throw new AuraHandledException('Could not fetch Digital Fields info! Please try after sometime.');
        }   
    }
    
    //This method is used to calculate the revenue allocation.
    @AuraEnabled
    global static AllocateRevWrapper reallocateRevenue(Decimal unitPrice, Date RevStart, Date RevEnd){
        try{
            /*oli = opptyLineItemObj;
Date rsdt = oli.revenue_start_date__c;
Date redt = oli.revenue_end_date__c;*/
            Integer cyr = system.today().year();
            Decimal dbstend = (RevStart.daysbetween(RevEnd) + 1);
            Decimal CurrentYearRevenue;
            Decimal Year2Revenue;
            Decimal Year3Revenue;
            Decimal annualizedRevenuecalculated;
            
            //As part of PMO Request 5341 (February 2015 Release) calculated Annualized Revenue (calculated) field on click of Allocate Revenue button.
            Integer noOfDays = RevStart.daysBetween(RevEnd);
            Integer rdt = noOfDays + 1;
            Decimal tempTotal = (rdt / 365.242199);
            Decimal total = tempTotal.setscale(1);
            total = total.scale();
            
            if (RevStart.year() <> cyr) {
                CurrentYearRevenue = 0;
            }            else if(RevEnd.year() - RevStart.year() == 0) {
                CurrentYearRevenue = unitPrice;
            }  else {
                Double dbtw = RevStart.daysbetween(date.newInstance(system.today().year(), 12, 31)); //days between revenue start date and end of current year
                Double dbtw2 = RevStart.daysbetween(RevEnd); // days between revenue start date and revenue end date
                Double divn = ((dbtw + 1) / (dbtw2 + 1));
                Decimal cyrev = divn * UnitPrice;               
                CurrentYearRevenue = Math.round(divn * UnitPrice); // cyrev;
            }
            
            if (RevEnd.year() == RevStart.year()) {
                if (RevEnd.year() == cyr + 2) {
                    Year2Revenue = 0;
                } else if (RevEnd.year() == cyr + 1) {
                    Year2Revenue = UnitPrice;
                } else {
                    Year2Revenue = 0;
                }
            } else if (RevStart.year() == cyr) {
                if (RevEnd.year() == cyr + 1) {
                    Double dbtw = date.newInstance(cyr + 1, 1, 1).daysbetween(RevEnd) + 1;
                    Year2Revenue = Math.round((dbtw / dbstend) * UnitPrice);
                } else {
                    Double calc = 365.0 / dbstend;
                    Year2Revenue = Math.round(calc * UnitPrice);
                }
            } else if (RevStart.year() == cyr + 1) {
                Double dbtw = RevStart.daysbetween(date.newInstance(cyr + 1, 12, 31)) + 1;
                Year2Revenue = Math.round((dbtw / (dbstend)) * UnitPrice);
            } else {
                Year2Revenue = 0;
            }
            
            Year3Revenue = unitprice - (Math.round(Year2Revenue)) - Math.round(CurrentYearRevenue);
            
            //As part of PMO Request 5341 (February 2015 Release) calculated Annualized Revenue (calculated) field on click of Allocate Revenue button.
            If( Math.round(total) < 1) {
                annualizedRevenuecalculated = UnitPrice; 
            }
            else {        
                annualizedRevenuecalculated = Math.round( UnitPrice / (total));
            }
            system.debug('total:'+total);
            AllocateRevWrapper allRev = new AllocateRevWrapper(CurrentYearRevenue, Year2Revenue, Year3Revenue, annualizedRevenuecalculated);
            return allRev;
        }
        catch(Exception ex){
            //system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_ProductSearchController', 'reallocateRevenue');
            throw new AuraHandledException('Could not calculate revenue data! Please try after sometime.');
        }
    }
    
    Public class PricebookWrapper{
        @AuraEnabled
        public PricebookEntry pbEntry {get;set;}
        @AuraEnabled
        public integer indexPB{get;set;}
        
        public PricebookWrapper(PricebookEntry pbEntry,integer indexPB){
            this.pbEntry = pbEntry;
            this.indexPB = indexPB;
            
        }
    }
    
    Public class ProdFieldsWrapper{
        @AuraEnabled
        public Map<String,Set<String>> mapSolSegCluster {get;set;}
        @AuraEnabled
        public Map<String,Set<String>>  mapSubBussSolSeg{get;set;}
        @AuraEnabled
        public Map<String,Set<String>>  mapLOBSubBuss{get;set;}
        
        public prodFieldsWrapper(Map<String,Set<String>> mapSolSegCluster,Map<String,Set<String>>  mapSubBussSolSeg,Map<String,Set<String>>  mapLOBSubBuss){
            this.mapSolSegCluster = mapSolSegCluster;
            this.mapSubBussSolSeg = mapSubBussSolSeg;
            this.mapLOBSubBuss = mapLOBSubBuss;
            
        }
    }
    
    Public class DigitalPksWrapper{
        @AuraEnabled
        List<String> wdMarketList {get;set;}
        @AuraEnabled
        List<String> snMarketList {get;set;}
        @AuraEnabled
        List<String> wdRegList {get;set;}
        @AuraEnabled
        List<String> merInfList {get;set;}
        @AuraEnabled
        List<String> serviceNowRegionList {get;set;}
        
        public DigitalPksWrapper(List<String> wdMarketList, List<String> snMarketList, List<String> wdRegList, List<String> merInfList,List<String> serviceNowRegionList){
            this.wdMarketList = wdMarketList;
            this.snMarketList = snMarketList;
            this.wdRegList = wdRegList;
            this.merInfList = merInfList;
            this.serviceNowRegionList = serviceNowRegionList;
        }
    }   
    
    global class AllocateRevWrapper{
        @AuraEnabled
        global Decimal cYRev {get;set;}
        @AuraEnabled
        global Decimal secYRev {get;set;}
        @AuraEnabled
        global Decimal thrdYRev {get;set;}
        @AuraEnabled
        global Decimal calcRev {get;set;}
        
        global AllocateRevWrapper(Decimal cYRev, Decimal secYRev, Decimal thrdYRev, Decimal calcRev){
            this.cYRev = cYRev;
            this.secYRev = secYRev;
            this.thrdYRev = thrdYRev;
            this.calcRev = calcRev;
            
        }
    }
}