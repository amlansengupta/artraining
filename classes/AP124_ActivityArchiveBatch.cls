/* Purpose: Batch for deactivating corresponding users for recently deactivated Colleagues
===================================================================================================================================
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Reetika            3/25/2015   PMO Req#5632 ,2015April Release : Batch for moving partial data from Task object to 
                                            'Activity Archive' , delete from 'Task' and 'Emptying it from Recycle Bin'
 ======================================================================================================================================*/
global class AP124_ActivityArchiveBatch implements Database.Batchable<sObject> {
    
    public String query; 
    public static final string constErrorStr1 = 'AP124_ActivityArchiveBatch Exception occured:' ;
    public static final string constErrorStr2 = 'AP124_ActivityArchiveBatch: Error with Task insertion : ';
    public static final string constErrorStr3 = 'AP124_ActivityArchiveBatch Exception at DELETION : ' ;
    public static final string constErrorStr4 = 'AP124_ActivityArchiveBatch :scope:' ;   
    public static final string constErrorStr5 =  ' archiveList:' ;
    public static final string constErrorStr6 = ' Total Errorlog: ' ;
    public static final string constErrorStr7 =  ' Batch Error Count errorCount: ' ;
    public static final string constErrorStr8 =  ' Deleted Count : ' ; 

    
     // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();


    /*
    *    Creation of Constructor
    */
    global AP124_ActivityArchiveBatch() {       
        
         String qry='';
         qry =  'Select Id, WhoId, WhatId, WhoCount, WhatCount, Subject, ActivityDate, Status, Priority, OwnerId, Description, CurrencyIsoCode, IsDeleted, AccountId, IsClosed, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, IsArchived, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet, RecurrenceActivityId, IsRecurrence, RecurrenceStartDateOnly, RecurrenceEndDateOnly, RecurrenceTimeZoneSidKey, RecurrenceType, RecurrenceInterval, RecurrenceDayOfWeekMask, RecurrenceDayOfMonth, RecurrenceInstance, RecurrenceMonthOfYear, MH_Action__c, mh_Additional_Notes_Exist__c, mh_Associated_Green_Sheet__c, mh_Attitude_Questions__c, mh_Basic_Issues__c, mh_Best_Action_Commitment__c, mh_Buying_Influence_Concept__c, mh_Commitment_Questions__c, mh_Confirmation_Questions__c, mh_Credibility__c, mh_Last_Updated_Green_Sheet__c, mh_Managers_Notes_Exist__c, mh_Managers_Notes__c, mh_Managers_Review_Date__c, mh_Minimum_Acceptable_Action__c, mh_New_Information_Questions__c, mh_Possible_Basic_Issues__c, mh_Post_Call_Assessment_Exists__c, mh_Post_Call_Assessment__c, mh_Sheet_Section__c, mh_Unique_Strengths__c, mh_Valid_Business_Reason__c, MDrive_Activity_Id__c, Completion_Date__c, isRelatedToAccount__c, ROW_Flag__c, Notify_User__c, Prospecting_Stage__c FROM Task '+Label.CL_AP124_Query;
         this.query= qry;
    }
    
    global AP124_ActivityArchiveBatch(String qStr)  {        
         this.query= qStr;
    }
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    /*
     *  Method Name: execute
     *  Description: Put Tasks records in Activity Archive DB , and then delete from Task
     *  And delete from recycle bin as well.
     *  Records to be processed = Owner = Marketo
     *                            Status = Completed
     *                            Subject starts with 'Was Sent Email'  or  'Opened Email' or   'Clicked Link in Email'   
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        List<Task> taskList= scope ;  

        List<Activity_Archive__c> archiveList = new List<Activity_Archive__c>() ;
        //List to hold Save Result 
        List<Database.SaveResult> archSaveResult = new List<Database.SaveResult>(); 
        Integer errorCount = 0;
        try {
            for (Task tsk : taskList) {
                   
                    Activity_Archive__c  archObj= new Activity_Archive__c() ;
                    archObj.IsRecurrence__c=tsk.ISRECURRENCE ;
                    archObj.Activity_SystemModStamp__c=tsk.SYSTEMMODSTAMP ;
                    archObj.Activity_Owner__c=tsk.OWNERID ;
                    archObj.RecurrenceEndDateOnly__c=tsk.RecurrenceEndDateOnly ;
                    archObj.Subject__c=tsk.Subject ;
                    archObj.Description__c=tsk.DESCRIPTION ;
                    archObj.WhoId__c=tsk.WHOID ;
                    archObj.Status__c=tsk.Status ;
                    archObj.CallObject__c=tsk.CALLOBJECT ;
                    archObj.RecurrenceTimeZoneSidKey__c=tsk.RecurrenceTimeZoneSidKey ;
                    archObj.RecurrenceMonthOfYear__c=tsk.RECURRENCEMONTHOFYEAR ;
                    archObj.RecurrenceActivityId__c=tsk.RecurrenceActivityId ;
                    archObj.mh_New_Information_Questions__c=tsk.mh_New_Information_Questions__c ;
                    archObj.mh_Last_Updated_Green_Sheet__c=tsk.mh_Last_Updated_Green_Sheet__c ;
                    archObj.RecurrenceDayOfWeekMask__c=tsk.RecurrenceDayOfWeekMask ;
                    archObj.mh_Commitment_Questions__c=tsk.mh_Commitment_Questions__c ;
                    archObj.Whocount__c=tsk.Whocount ;
                    archObj.Call_Type__c=tsk.CALLTYPE ;
                    archObj.mh_Additional_Notes_Exist__c=tsk.mh_Additional_Notes_Exist__c ;
                    archObj.mh_Post_Call_Assessment_Exists__c=tsk.mh_Post_Call_Assessment_Exists__c ;
                    archObj.mh_Managers_Review_Date__c=tsk.mh_Managers_Review_Date__c ;
                    archObj.mh_Confirmation_Questions__c=tsk.mh_Confirmation_Questions__c ;
                    archObj.CreatedByUser__c=tsk.CREATEDBYID ;
                    archObj.Created_Date__c=tsk.CREATEDDATE ;
                    archObj.Completion_Date__c = String.valueOf(tsk.Completion_Date__c) ;
                    archObj.IsClosed__c=tsk.ISCLOSED ;
                    archObj.MH_Action__c=tsk.MH_Action__c ;
                    archObj.RecurranceInstance__c=tsk.RECURRENCEINSTANCE ;
                    archObj.ROW_Flag__c=tsk.ROW_Flag__c ;
                    archObj.mh_Possible_Basic_Issues__c=tsk.mh_Possible_Basic_Issues__c ;
                    archObj.Last_Modified_By_User__c=tsk.LASTMODIFIEDBYID ;
                    archObj.Last_Modified_Date__c=tsk.LASTMODIFIEDDATE ;
                    archObj.CurrencyIsoCode=tsk.CurrencyIsoCode ;
                    archObj.Account_ID__c=tsk.ACCOUNTID ;
                    archObj.mh_Sheet_Section__c=tsk.mh_Sheet_Section__c ;
                    archObj.Activity_ID__c=tsk.ID ;
                    archObj.IsArchived__c=tsk.ISARCHIVED ;
                    archObj.CallDurationInSeconds__c=tsk.CALLDURATIONINSECONDS ;
                    archObj.mh_Basic_Issues__c=tsk.mh_Basic_Issues__c ;
                    archObj.RecurrenceInterval__c=tsk.RECURRENCEINTERVAL ;
                    archObj.CallDisposition__c=tsk.CALLDISPOSITION ;
                    archObj.IsReminderSet__c=tsk.ISREMINDERSET ;
                    archObj.MDrive_Activity_Id__c=tsk.MDrive_Activity_Id__c ;
                    archObj.WhatId__c=tsk.WHATID ;
                    archObj.Prospecting_Stage__c=tsk.Prospecting_Stage__c ;
                    archObj.ReminderDateTime__c=tsk.ReminderDateTime ;
                    archObj.mh_Unique_Strengths__c=tsk.mh_Unique_Strengths__c ;
                    archObj.RecurrenceStartDateTime__c=tsk.RECURRENCESTARTDATEONLY ;
                    archObj.mh_Associated_Green_Sheet__c=tsk.mh_Associated_Green_Sheet__c ;
                    archObj.mh_Valid_Business_Reason__c=tsk.mh_Valid_Business_Reason__c ;
                    archObj.mh_Attitude_Questions__c=tsk.mh_Attitude_Questions__c ;
                    archObj.mh_Buying_Influence_Concept__c=tsk.mh_Buying_Influence_Concept__c ;
                    archObj.mh_Minimum_Acceptable_Action__c=tsk.mh_Minimum_Acceptable_Action__c ;
                    archObj.RecurrenceType__c=tsk.RecurrenceType ;
                    archObj.mh_Managers_Notes_Exist__c=tsk.mh_Managers_Notes_Exist__c ;
                    archObj.RecurrenceDayOfMonth__c=tsk.RecurrenceDayOfMonth ;
                    archObj.WhatCount__c=tsk.WhatCount ;
                    archObj.Priority__c=tsk.Priority ;
                    archObj.Notify_User__c=tsk.Notify_User__c ;
                    archObj.mh_Post_Call_Assessment__c=tsk.mh_Post_Call_Assessment__c ;
                    archObj.IsDeleted__c=tsk.ISDELETED ;
                    archObj.isRelatedToAccount__c=tsk.isRelatedToAccount__c ;
                    archObj.mh_Credibility__c=tsk.mh_Credibility__c ;
                    archObj.mh_Managers_Notes__c=tsk.mh_Managers_Notes__c ;
                    archObj.ActivityDate__c=tsk.ACTIVITYDATE ;
                    archObj.mh_Best_Action_Commitment__c=tsk.mh_Best_Action_Commitment__c ;
    
                    archiveList.add(archObj) ; 
                    system.debug('archiveList is '+ archiveList);
    
            }
       } catch(Exception e)
         {
            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + e.getMessage(), errorLogs, scope[0].Id);  
            System.debug(constErrorStr1 +e.getMessage());
         } 
      
        //Insert Tasks into Activity Archive
        if(archiveList.size()>0)
        {
        
           archSaveResult = Database.insert(archiveList,true) ;
          
           //Iterate for Error Logging
            for(Database.Saveresult result : archSaveResult)
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr2 + result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                  errorCount++;
                }
                
              }           
        }
        
        integer deletedCount = 0 ; 
        try {
            if(errorCount==0) {
                  Database.delete(scope) ;
                  Database.emptyRecycleBin(scope);
                  deletedCount = scope.size() ; 
            }
        }catch(Exception e)
         {
            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog( constErrorStr3 + e.getMessage(), errorLogs, scope[0].Id);  
            System.debug(constErrorStr3 +e.getMessage());
         } 
         
         //List to hold Batch Status
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();
         //Values that will be stored while Error Logging
         
         errorLogStatus  = MercerAccountStatusBatchHelper.addToErrorLog(constErrorStr4  + scope.size() +constErrorStr5  + archiveList.size() + constErrorStr6 +errorLogs.size() + constErrorStr7 +errorCount + constErrorStr8 + deletedCount ,  errorLogStatus  , scope[0].Id);
         //Insert to ErrorLogStatus
         insert  errorLogStatus ;
         
        
    }
    
    

    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC) {
        
        //If Error Logs exist
        if(errorLogs.size()>0) {
            //Insert to ErrorLogs
           Database.insert(errorLogs,false) ;                        
        }  

    }
    
}