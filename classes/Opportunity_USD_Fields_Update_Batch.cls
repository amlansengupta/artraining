global class Opportunity_USD_Fields_Update_Batch implements Database.Batchable<sObject>{

    global String query = '';    
    /*'Select Id,CurrencyIsoCode,Probability,Total_Opportunity_Revenue__c, Total_Opportunity_Revenue_USD__c,'+
                          'Annualized_Revenue__c, Annualized_Revenue_USD__c From Opportunity '+
                          ' Where CALENDAR_YEAR(CreatedDate) >= 2014';
    */    
    List<CurrencyType> exchangeRates = [SELECT Id,ISOCode, ConversionRate,IsActive FROM CurrencyType];
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(Test.isRunningTest()){
            query = 'Select Id,CurrencyIsoCode,Probability,Total_Opportunity_Revenue__c, Total_Opportunity_Revenue_USD__c,'+
                              'Annualized_Revenue__c, Annualized_Revenue_USD__c ,'+
                              'Current_Year_Revenue__c,Current_Year_Revenue_USD__c,Child_Opp_TOR__c,Child_Opp_TOR_USD__c,'+ 
                              'Total_Scopable_Products_Revenue__c, Total_Scopable_Products_Revenue_USD__c '+
                              'From Opportunity '+
                              ' Where CALENDAR_YEAR(CreatedDate) >= 2014 LIMIT 200';
        }
        else{        
               query = Label.Opportunity_USD_Fields_Update_Query_Label;               
        }
        system.debug('###query'+query);
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {    
        /* Opportunity USD Correction */
        Set<Id> setOfOppIds = new Set<Id>();
        List<Opportunity> listOfOppsToUpdate = new List<Opportunity>();
        Boolean isTORToUpdate = false;
        Boolean isARToUpdate = false;
        Boolean isCYRToUpdate = false;
        Boolean isChildTORToUpdate = false;
        Boolean isTSPToUpdate = false;                

        for(Opportunity op : scope ){                        
        
            setOfOppIds.add(op.Id);
            
           // if(op.Total_Opportunity_Revenue_USD__c != convertToUSD(op.CurrencyIsoCode,op.Total_Opportunity_Revenue__c).setScale(0))
           // {
                op.Total_Opportunity_Revenue_USD__c = convertToUSD(op.CurrencyIsoCode,op.Total_Opportunity_Revenue__c).setScale(2);            
                isTORToUpdate = true;
           // }
            
           // if(op.Annualized_Revenue_USD__c != convertToUSD(op.CurrencyIsoCode,op.Annualized_Revenue__c).setScale(2))
           // {
                op.Annualized_Revenue_USD__c = convertToUSD(op.CurrencyIsoCode,op.Annualized_Revenue__c).setScale(2);
                isARToUpdate = true;
            //}
            
           // if(op.Current_Year_Revenue_USD__c != convertToUSD(op.CurrencyIsoCode,op.Current_Year_Revenue__c).setScale(0))
          //  {
                op.Current_Year_Revenue_USD__c = convertToUSD(op.CurrencyIsoCode,op.Current_Year_Revenue__c).setScale(0);
                isCYRToUpdate = true;
           // }
            
           // if(op.Child_Opp_TOR_USD__c != convertToUSD(op.CurrencyIsoCode,op.Child_Opp_TOR__c).setScale(2))
           // {
                op.Child_Opp_TOR_USD__c = convertToUSD(op.CurrencyIsoCode,op.Child_Opp_TOR__c).setScale(2);
                isChildTORToUpdate = true;
           // }
            
           // if(op.Total_Scopable_Products_Revenue_USD__c != convertToUSD(op.CurrencyIsoCode,op.Total_Scopable_Products_Revenue__c).setScale(0))
          //  {
                op.Total_Scopable_Products_Revenue_USD__c = convertToUSD(op.CurrencyIsoCode,op.Total_Scopable_Products_Revenue__c).setScale(0);
                isTSPToUpdate = true;
          //  }                                
                
            
           // if(isTORToUpdate || isARToUpdate || isCYRToUpdate || isChildTORToUpdate || isTSPToUpdate){
                listOfOppsToUpdate.add(op);
          //  }
            
          //  listOfOppsToUpdate.add(op);
            
            isTORToUpdate = false;
            isARToUpdate = false;    
            isCYRToUpdate = false;
            isChildTORToUpdate = false;
            isTSPToUpdate = false;                     
        }
                
        if(!listOfOppsToUpdate.isEmpty()){
            try{            
                Database.update(listOfOppsToUpdate,false);
            }
        
            catch(DmlException dmlException){
                system.debug('###opportunityException'+dmlException.getMessage());
            }
        }
        
        /* OppProduct USD Correction */
        

        List<OpportunityLineItem> scopedProductList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> listOfOppProduct = new List<OpportunityLineItem>(); 
        
        scopedProductList = [Select Id,OpportunityId,Opportunity.CurrencyIsoCode,UnitPrice,Sales_Price_USD__c,
                             Sales_Price_USD_calc__c, Expected_Product_Sales_Price_USD__c, Opportunity.Probability,
                             CurrentYearRevenue_edit__c, Current_Year_Revenue_Product_USD__c, Year2Revenue_edit__c,
                             Year_2_Revenue_Product_USD__c, Year3Revenue_edit__c, Year_3_Revenue_Product_USD__c,
                             Annualized_Revenue__c, Annualized_Revenue_Product_USD__c 
                             From OpportunityLineItem Where OpportunityId IN :setOfOppIds];
        
                
        Boolean isSPToUpdate = false;
        Boolean isExpectedSPToUpdate = false;        
        for(OpportunityLineItem ol : scopedProductList )
        {
           // if( ol.Sales_Price_USD__c != convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.UnitPrice) )
           // {
                ol.Sales_Price_USD_calc__c = convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.UnitPrice).setScale(2);                                
                isSPToUpdate = true;
           // }
          //  if( ol.Expected_Product_Sales_Price_USD__c !=
             //                convertToUSD(ol.Opportunity.CurrencyIsoCode , ol.UnitPrice*ol.Opportunity.Probability/100 ) )
           // {
                ol.Expected_Product_Sales_Price_USD__c = (convertToUSD(ol.Opportunity.CurrencyIsoCode , ol.UnitPrice) * (ol.Opportunity.Probability/100)).setScale(2);
                isExpectedSPToUpdate = true;
            //}
                ol.Current_Year_Revenue_Product_USD__c  = convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.CurrentYearRevenue_edit__c).setScale(2);
                ol.Year_2_Revenue_Product_USD__c = convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.Year2Revenue_edit__c).setScale(2);
                ol.Year_3_Revenue_Product_USD__c = convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.Year3Revenue_edit__c).setScale(2);
                ol.Annualized_Revenue_Product_USD__c = convertToUSD(ol.Opportunity.CurrencyIsoCode,ol.Annualized_Revenue__c).setScale(2);
                
            
            //if(isSPToUpdate  || isExpectedSPToUpdate ){
             listOfOppProduct.add(ol);
            //}
            isSPToUpdate  = false;
            isExpectedSPToUpdate = false;
        } 
        
        system.debug('###scopeSize'+scope.size());
        system.debug('###listOfOppProduct'+listOfOppProduct.size());
        
         if(!listOfOppProduct.isEmpty()){
            try{
                Database.update(listOfOppProduct,false);
            }
            catch(DmlException dmlException){
                system.debug('###productException'+dmlException.getMessage());
            }
        }    
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    global Decimal convertToUSD(String currencyISOCode,Decimal amount){        
        Decimal convertedUSDValue = null;
        if(amount == null){
            amount = 0.00;
        }
        for(CurrencyType currencyVal : exchangeRates)
        {                                   
            if(currencyVal.IsoCode == currencyIsoCode)   
            {
                convertedUSDValue = amount/currencyVal.ConversionRate;                
            }
        }        
        convertedUSDValue = convertedUSDValue.setScale(2);
        return convertedUSDValue;
    }
}