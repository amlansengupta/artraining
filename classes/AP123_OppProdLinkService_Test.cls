/*Purpose: Apex test class to to provide coverage for AP123_OppProdLinkService
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jyotsna     3/4/2015    Provide coverage for AP123_OppProdLinkService class
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP123_OppProdLinkService_Test {
/*
    * @Description : Test method to provide  coverage to AP123_OppProdLinkService 
    */
    
    
    static testmethod void OppProdlinkServiceTest(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            
        // insert Account 
        Account accrec =  mdata.buildAccount();
        
        //insert Opportunity
        Opportunity opprec =mdata.buildOpportunity();
        
        //Insert opportunityLineItem
        Product2 pro1 = [Select p.Name, p.Id,p.Segment__c,  p.LOB__c, p.Cluster__c, IsActive From Product2 p where p.IsActive=true  Limit 1];
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        OpportunityLineItem oppLineItem = Mercer_TestData.createOpptylineItem(opprec.Id, pbEntry.Id);
        
        String oppProdId = oppLineItem.Id;
        Test.startTest();
        //insert Project 
        Revenue_Connectivity__c prorec =mdata.buildProject();
        
        prorec.Charge_Basis__c = '200';
        prorec.Project_Solution__c = '3052';
        update prorec;
        
        String projStartDate = string.valueOf(prorec.Project_Start_Date__c);
        String projEndDate = string.valueOf(prorec.Project_End_Date__c);
        String projAmount = string.valueOf(prorec.Project_Amount__c);
		string projSalesProf = '866608';
        
        AP123_OppProdLinkService.pushProjectDetailsToMFOppProduct(prorec.WebCAS_Project_ID__c,oppProdId,prorec.Project_Status__c,prorec.Project_Manager__c, projSalesProf,projStartDate, projEndDate,prorec.Project_LOB__c, prorec.Project_Solution__c, projAmount, prorec.CEP_Code__c, prorec.Charge_Basis__c, prorec.Project_Type__c, 'USD');
        Test.StopTest();
        }
    }
    
    static testmethod void OppProdlinkServiceTest1(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            
        // insert Account 
        Account accrec =  mdata.buildAccount();
        
        //insert Opportunity
        Opportunity opprec =mdata.buildOpportunity();
        
        //Insert opportunityLineItem
        Product2 pro1 = [Select p.Name, p.Id,p.Segment__c,  p.LOB__c, p.Cluster__c, IsActive From Product2 p where p.IsActive=true  Limit 1];
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        OpportunityLineItem oppLineItem = Mercer_TestData.createOpptylineItem(opprec.Id, pbEntry.Id);
        
        String oppProdId = oppLineItem.Id;
        Test.startTest();
        //insert Project 
        Revenue_Connectivity__c prorec =mdata.buildProject();
        
        prorec.Charge_Basis__c = '200';
        prorec.Project_Solution__c = '3052';
        update prorec;
        
        String projStartDate = string.valueOf(prorec.Project_Start_Date__c);
        String projEndDate = string.valueOf(prorec.Project_End_Date__c);
        String projAmount = string.valueOf(prorec.Project_Amount__c);

        
        try{
            string projSalesProf = '866608';
            AP123_OppProdLinkService.pushProjectDetailsToMFOppProduct(prorec.WebCAS_Project_ID__c,'',prorec.Project_Status__c,prorec.Project_Manager__c,projSalesProf, projStartDate, projEndDate,prorec.Project_LOB__c, prorec.Project_Solution__c, projAmount, prorec.CEP_Code__c, prorec.Charge_Basis__c, prorec.Project_Type__c, 'USD');
        }catch(Exception e){
            System.Assert(e.getMessage().contains('Please specify mandatory inputs : Parameter 1 - WebCAS Project ID, 2 - Opportunity Product ID , 3 - Project Name'));
        }
        Test.StopTest();
      }  
    }  
    
    static testmethod void OppProdlinkServiceTest2(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            
        // insert Account 
        Account accrec =  mdata.buildAccount();
        
        //insert Opportunity
        Opportunity opprec =mdata.buildOpportunity();
        
        //Insert opportunityLineItem
        Product2 pro1 = [Select p.Name, p.Id,p.Segment__c,  p.LOB__c, p.Cluster__c, IsActive From Product2 p where p.IsActive=true  Limit 1];
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        OpportunityLineItem oppLineItem = Mercer_TestData.createOpptylineItem(opprec.Id, pbEntry.Id);
        
        String oppProdId = oppLineItem.Id;
        Test.startTest();
        //insert Project 
        Revenue_Connectivity__c prorec =mdata.buildProject();
        
        prorec.Charge_Basis__c = '200';
        prorec.Project_Solution__c = '3052';
        update prorec;
        
        String projStartDate = string.valueOf(prorec.Project_Start_Date__c);
        String projEndDate = string.valueOf(prorec.Project_End_Date__c);
        String projAmount = string.valueOf(prorec.Project_Amount__c);
        string projSalesProf = '866608';
        try{
            AP123_OppProdLinkService.pushProjectDetailsToMFOppProduct('',oppProdId,prorec.Project_Status__c,prorec.Project_Manager__c,projSalesProf, projStartDate, projEndDate,prorec.Project_LOB__c, prorec.Project_Solution__c, projAmount, prorec.CEP_Code__c, prorec.Charge_Basis__c, prorec.Project_Type__c, 'USD');
        }catch(Exception e){
            System.Assert(e.getMessage().contains('Please specify mandatory inputs : Parameter 1 - WebCAS Project ID, 2 - Opportunity Product ID , 3 - Project Name'));
        }
        Test.StopTest();
        }
    } 
    
    static testmethod void OppProdlinkServiceTest3(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            
        // insert Account 
        Account accrec =  mdata.buildAccount();
        
        //insert Opportunity
        Opportunity opprec =mdata.buildOpportunity();
        
        //Insert opportunityLineItem
        Product2 pro1 = [Select p.Name, p.Id,p.Segment__c,  p.LOB__c, p.Cluster__c, IsActive From Product2 p where p.IsActive=true  Limit 1];
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
      
        OpportunityLineItem oppLineItem = Mercer_TestData.createOpptylineItem(opprec.Id, pbEntry.Id);
        
        String oppProdId = oppLineItem.Id;
         Test.startTest();
        //insert Project 
        Revenue_Connectivity__c prorec =mdata.buildProject();
        
        prorec.Charge_Basis__c = '200';
        prorec.Project_Solution__c = '3052';
        update prorec;
        
        String projStartDate = string.valueOf(prorec.Project_Start_Date__c);
        String projEndDate = string.valueOf(prorec.Project_End_Date__c);
        String projAmount = string.valueOf(prorec.Project_Amount__c);
        String  projSalesProf = '866608';
        try{
            AP123_OppProdLinkService.pushProjectDetailsToMFOppProduct(prorec.WebCAS_Project_ID__c,oppProdId,prorec.Project_Status__c,prorec.Project_Manager__c, projSalesProf, projStartDate, projEndDate,prorec.Project_LOB__c, prorec.Project_Solution__c, projAmount, prorec.CEP_Code__c, prorec.Charge_Basis__c, prorec.Project_Type__c, 'USD');
        }catch(Exception e){
            System.Assert(e.getMessage().contains('Please specify mandatory inputs : Parameter 1 - WebCAS Project ID, 2 - Opportunity Product ID , 3 - Project Name'));
        }
        Test.StopTest();
        }
    }  
  
  static testmethod void WebServiceExceptionTest(){
        WebServiceException webServiceEx = new WebServiceException();
  }
}