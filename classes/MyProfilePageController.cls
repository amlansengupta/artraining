/*Purpose:  This Apex class keeps updates of a portal user in sync with its corresponding contact.
   Guest users are never able to access this page.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Venu       5/22/2013   Created apex class.
   2.0 -    Sarbpreet  7/3/2014    Updated Comments/documentation.
  
============================================================================================================================================== 
*/
public with sharing class MyProfilePageController {

    private User user;
    private boolean isEdit = false;
    
    /*
    *  Method for getting the user
    */
    public User getUser() {
        return user;
    }

   /*
    *  Creation of constructor 
    */
    public MyProfilePageController() {
        user = [SELECT id, email, username, usertype, communitynickname, timezonesidkey, languagelocalekey, firstname, lastname, phone, title,
                street, city, country, postalcode, state, localesidkey, mobilephone, extension, fax, contact.email
                FROM User
                WHERE id = :UserInfo.getUserId()];
        // guest users should never be able to access this page
        if (user.usertype == 'GUEST') {
            throw new NoAccessException();
        }
    }
    
    /*
     * Boolean variable to check if the page is editable or not
     */ 
    public Boolean getIsEdit() {
        return isEdit;
    }
    
   /*
    * @Description : Method for setting the value of boolean variable to true
    * @ Args       : null
    * @ Return     : void
    */ 
    public void edit() {
        isEdit=true;
    }    
    
   /*
    * @Description : Method for saving the record
    * @ Args       : null
    * @ Return     : String
    */
    public void save() {
        if (user.contact != null) {              
            setContactFields(user.contact, user);
        }
        
        try {
            update user;
            if (user.contact != null) { 
                update user.contact;
            }
            isEdit=false;
        } catch(DmlException e) {
            ApexPages.addMessages(e);
        }
    }
    
   /*
    * @Description : Method for changing the password
    * @ Args       : null
    * @ Return     : PageReference
    */
    public PageReference changePassword() {
        return Page.ChangePassword;
    }
     /*
    * @Description : Method to cancel the current page and redirect to previous page
    * @ Args       : null
    * @ Return     : void
    */
    public void cancel() {
        isEdit=false;
        user = [SELECT id, email, username, communitynickname, timezonesidkey, languagelocalekey, firstname, lastname, phone, title,
                street, city, country, postalcode, state, localesidkey, mobilephone, extension, fax, contact.email
                FROM User
                WHERE id = :UserInfo.getUserId()];
    }
    
   /*
    * @Description : Method for mapping the portal user details with corresponding contact details.
    * @ Args       : null
    * @ Return     : PageReference
    */
    public void setContactFields(Contact c, User u) {
        c.title = u.title;
        c.firstname = u.firstname;
        c.lastname = u.lastname;
        c.email = u.email;
        c.phone = u.phone;
        c.mobilephone = u.mobilephone;
        c.fax = u.fax;
        c.mailingstreet = u.street;
        c.mailingcity = u.city;
        c.mailingstate = u.state;
        c.mailingpostalcode = u.postalcode;
        c.mailingcountry = u.country;
    }

    /*
     * Test class for providing coverage to MyProfilePageController class
     */ 
    /*@IsTest(SeeAllData=true) static void testSave() {         
        // Modify the test to query for a portal user that exists in your org
        List<User> existingPortalUsers = [SELECT id, profileId, userRoleId FROM User WHERE UserRoleId <> null AND UserType='CustomerSuccess'];

        if (existingPortalUsers.isEmpty()) {
            User currentUser = [select id, title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                                FROM User WHERE id =: UserInfo.getUserId()];
            MyProfilePageController controller = new MyProfilePageController();
            System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
            System.assert(controller.isEdit == false, 'isEdit should default to false');
            controller.edit();
            System.assert(controller.isEdit == true);
            controller.cancel();
            System.assert(controller.isEdit == false);
            
            //Test Data for Collegue
            Colleague__c testColleague = new Colleague__c();
            testColleague.Name = 'TestColleague';
            testColleague.EMPLID__c = '12345678902';
            testColleague.LOB__c = '11111';
            testColleague.Last_Name__c = 'TestLastName';
            testColleague.Empl_Status__c = 'Active';
            testColleague.Email_Address__c = 'abc@abc.com';
            insert testColleague;
            
            //Test Data for Account
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague.Id;
            testAccount.One_Code__c = '1234';
            insert testAccount;
            
            //Test Data for Contact
            Contact c = new Contact();
            c.LastName = 'TestContact';
            c.AccountId = testAccount.Id;
            c.email = 'abc1@xyz.com';
            insert c;
            
            setContactFields(c, currentUser);
            controller.save();
            System.assert(Page.ChangePassword.getUrl().equals(controller.changePassword().getUrl()));
        } else {
            User existingPortalUser = existingPortalUsers[0];
            String randFax = Math.rint(Math.random() * 1000) + '5551234';
            
            System.runAs(existingPortalUser) {
                MyProfilePageController controller = new MyProfilePageController();
                System.assertEquals(existingPortalUser.Id, controller.getUser().Id, 'Did not successfully load the current user');
                System.assert(controller.isEdit == false, 'isEdit should default to false');
                controller.edit();
                System.assert(controller.isEdit == true);
                
                controller.cancel();
                System.assert(controller.isEdit == false);
                
                controller.getUser().Fax = randFax;
                controller.save();
                System.assert(controller.isEdit == false);
            }
            
            // verify that the user and contact were updated
            existingPortalUser = [Select id, fax, Contact.Fax from User where id =: existingPortalUser.Id];
            System.assert(existingPortalUser.fax == randFax);
            System.assert(existingPortalUser.Contact.fax == randFax);
        }
    }*/

}