@isTest
public class customLookUpProdControllerApexTest {
@isTest
    public static void test1(){
   
        Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
       Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';        
        testOppty.Buyer__c=testContact.Id;
        testOppty.AccountId=testAccount.Id;     
        insert testOppty;
        
        colleague__c testColleague = new colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.Level_1_Descr__c = 'Mercer';        
        insert testColleague;
        
        customLookUpProdControllerApex.fetchDefaultLookUp(testOppty.id,'Opportunity');
        customLookUpProdControllerApex.fetchLookUpValues('TestAccountName','Account','');
        customLookUpProdControllerApex.fetchLookUpValues('TestColleague','Colleague__c','Empl_Status__c = /Active/ AND Level_1_Descr__c = /Mercer/');
        
    }

}