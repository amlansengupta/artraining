/*Purpose: Apex class to expose Webservice method for WEBCAS 
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Reetika     26/2/2015  2015SApril Release/Req#5568 - Exposed webservice to be able to update opportunity products with WebCAS project details\
   2.0      Reetika     12/5/2015  2015July release/Req#5568 - Added more parameters in pushProjectDetailsToMFOppProduct input parameters 
   3.0      Bala        6/9/2016   2016June Release/Req# 8676 - Updated Webservice to remove the field 'Project Name' and also updated webservice 
                                                                                           status from 'Approved' to 'Active'.
============================================================================================================================================== 
*/
global without sharing class  AP123_OppProdLinkService {
    public static final string STR_WEBSERVICEEXCEPTION = 'Please specify mandatory inputs : Parameter 1 - WebCAS Project ID, 2 - Opportunity Product ID';
    public static final string STR_WEBSERVICEEXCEPTIONA = 'Could not push Project Setup details in Mercerforce.';
    public static final string STR_WEBSERVICEEXCEPTIONB = 'Could not push Project Setup details in Mercerforce because Opportunity Product Id not found in Mercerforce.';
    public static final string STR_WEBSERVICEEXCEPTIONC = 'Could not push Project Setup details in Mercerforce because Project Status is not Active.';
    public static final string PROJ_STATUS = 'Active';
    public static final string OUTPUT_MSG_SUCCESS = 'Record created for Revenue connectivity with Id ' ;
    public static final string PROJMANAGEROUTPUT_MSG_SUCCESS = 'Project manager updated successfully' ;
    public static boolean prodLinkFlag = false;    
    //public static final string CHARGE_FEE_TYPE_S = 'S';
    
    /**
     * Webservice exposed to be consumed by WEBCAS to  update opportunity products 
     * with WebCAS project details
     * MercerForce gets the request with input parameters
     * 
     */
    webservice static void pushProjectDetailsToMFOppProduct(String webcasProjectId, String oppProductId,String projStatus,String projMgr, String projStartDt, String projEndDt,
     String projLOB, String projSol, String projAmt, String cepCode, String chargeBasis, String projectType, String CurrencyIsoCode,string projSalesProfessional) {
        
            if(string.isBlank(webcasProjectId)|| string.isBlank(webcasProjectId.trim())) {
                callLogger(webcasProjectId,oppProductId,projStatus,projMgr, projSalesProfessional, projStartDt, projEndDt, projLOB, projSol, projAmt,
                cepCode, chargeBasis, projectType, CurrencyIsoCode, STR_WEBSERVICEEXCEPTION ) ; 
                return ;
            }
           
            List<OpportunityLineItem> oppProduct = [Select  OpportunityId, Id ,Is_Project_Required__c From OpportunityLineItem where Id = :oppProductId  ] ;
            System.debug('&&&1'+oppProduct);
            List<Revenue_Connectivity__c> revConnect = [Select Project_Status__c, Project_Start_Date__c, Project_Solution__c,Sales_Professional__c,Project_Manager__c,
                 Project_LOB__c, Project_Amount__c, Project_End_Date__c, Opportunity__c, Opportunity_Product_Id__c, Id, CEP_Code__c, Charge_Basis__c, Project_Type__c, WebCAS_Project_ID__c, CurrencyIsoCode From Revenue_Connectivity__c 
                 where WebCAS_Project_ID__c = :webcasProjectId] ;
            
            if(string.isBlank(oppProductId) && revConnect!=null){
            System.debug('>>>>Delete');
            Revenue_Connectivity__c deleteRevConnect;
            //Fix Index Out of Bounds Error - 20191023 - Start
            String unLinkOppProdId;
            if(!revConnect.isEmpty()){
            //Fix Index Out of Bounds Error - 20191023 - End
                deleteRevConnect = revConnect[0];
                unLinkOppProdId = deleteRevConnect.Opportunity_Product_Id__c;
                Database.delete(deleteRevConnect );
            }//Fix Index Out of Bounds Error - 20191023
            
            List<OpportunityLineItem> oppProdList = [Select  OpportunityId, Id ,Project_Linked__c,Is_Project_Required__c From OpportunityLineItem where Id = :unLinkOppProdId  ] ;
                //Fix Index Out of Bounds Error - 20191023 - Start
                //if(oppProdList!=null || oppProdList.size()>0){
                if(oppProdList!=null && oppProdList.size()>0){
                //Fix Index Out of Bounds Error - 20191023 - End
                    OpportunityLineItem oppLineItem = oppProdList.get(0);
                    oppLineItem.Project_Linked__c = false;
                    
                    Database.update(oppLineItem, false);
                }
                
            }else{
                System.debug('>>>>Insert');
                if(oppProduct!=null && oppProduct.size()==1)  {
                
                System.debug('>>>>Update');
                
                    try {
                        OpportunityLineItem oppLineItem=new OpportunityLineItem();
                        List<OpportunityLineItem> oppLineItemList=new List<OpportunityLineItem>();
                        
                        Map<id,Boolean> projectReqMap=new Map<id,Boolean>();
                         
                        List<Colleague__c> listcs=[select id,name,EMPLID__c from Colleague__c where EMPLID__c =: projMgr];
                        System.debug('&&&**'+projMgr+projSalesProfessional);
                        List<Colleague__c> listCol =[Select id, Name , EMPLID__c from Colleague__c where EMPLID__c =: projSalesProfessional];
                        for(OpportunityLineItem oli:oppProduct){
                            
                            oppLineItem.id=oli.id;
                            //#req-18551
                            if(!listcs.isEmpty())
                            oppLineItem.Project_Manager__c=listcs.get(0).id; 
                            if(!listCol.isEmpty()){
                            oppLineItem.Sales_Professional__c =listCol.get(0).id;
                            }
                            if(projSalesProfessional ==null || string.isBlank(projSalesProfessional)){
                              oppLineItem.Sales_Professional__c=null;  
                            }
                            /*else{
                             oppLineItem.Sales_Professional__c=null;   
                            }*/
                             
                            projectReqMap.put(oli.id,oli.Is_Project_Required__c);
                            oppLineItemList.add(oppLineItem);
                        }                        
                        
                        Revenue_Connectivity__c saveRevConnect ;
                        if (revConnect!=null && revConnect.size()==1) {
                        System.debug('>>>>Update if satisfy');
                            saveRevConnect = revConnect[0] ;                
                            saveRevConnect.Opportunity__c = oppProduct[0].OpportunityId ;
                            saveRevConnect.Opportunity_Product_Id__c = oppProductId ;
                        } else {
                            saveRevConnect = new Revenue_Connectivity__c();
                            saveRevConnect.Opportunity__c = oppProduct[0].OpportunityId ;
                            saveRevConnect.Opportunity_Product_Id__c = oppProductId ;
                        }
                        if(projectReqMap.get(oppProductId)){
                            saveRevConnect.Project_Required__c = True ;
                        }
                        saveRevConnect.Project_Status__c = projStatus ;
                        saveRevConnect.WebCAS_Project_ID__c = webcasProjectId;
                        saveRevConnect.Project_Solution__c = projSol ;
                        //saveRevConnect.Project_Name__c = projName ;
                        saveRevConnect.Project_Manager__c = projMgr ;
                        //#Req18551
                        if(!listCol.isEmpty()){
                           saveRevConnect.Sales_Professional__c =listCol.get(0).id;
                            }
                          if(projSalesProfessional ==null || string.isBlank(projSalesProfessional)){
                              saveRevConnect.Sales_Professional__c=null;  
                            }
                        /*else{
                           saveRevConnect.Sales_Professional__c =null; 
                        }*/
                        
                        saveRevConnect.Project_LOB__c = projLOB ;
                        saveRevConnect.CEP_Code__c= cepCode;
                        saveRevConnect.Charge_Basis__c = chargeBasis;
                        saveRevConnect.Project_Type__c = projectType;
                        saveRevConnect.CurrencyIsoCode = CurrencyIsoCode;
                        
                        Date nwStDt = null;
                        Date nwEndDt = null;
                        
                        Integer endMonth = null;
                        Date tmpEndDt = null;
                        Date newTmpEndDt = null;
                        
                        
                        //if(saveRevConnect.Charge_Basis__c==CHARGE_FEE_TYPE_S ) {
                             if (string.isNotBlank(projStartDt) && projStartDt.length()==6) {
                                    nwStDt = Date.newInstance(Integer.valueOf(projStartDt.substring(0,4)) ,Integer.valueOf(projStartDt.substring(4))  , 01) ;
                             }
                             if (string.isNotBlank(projEndDt) && projEndDt.length()==6) { 
                                
                                    newTmpEndDt =  Date.newInstance(Integer.valueOf(projEndDt.substring(0,4)) ,Integer.valueOf(projEndDt.substring(4))  , 01) ;
                                    endMonth = newTmpEndDt.month() + 1;
                                    tmpEndDt = Date.newInstance(Integer.valueOf(projEndDt.substring(0,4)) ,endMonth  , 01) ;
                                    nwEndDt  = tmpEndDt - 1;
                                    
                             }
                        //} 
                        if (string.isNotBlank(projStartDt) && projStartDt.length()==10) nwStDt = Date.valueOf( projStartDt) ;
                        if (string.isNotBlank(projEndDt) && projEndDt.length()==10) nwEndDt = Date.valueOf(projEndDt) ;
                        saveRevConnect.Project_Start_Date__c = nwStDt ;
                        saveRevConnect.Project_End_Date__c = nwEndDt;
                        if (string.isNotBlank(projAmt)) 
                            saveRevConnect.Project_Amount__c = Decimal.valueOf(projAmt) ;
                        
                        String outpt = null ;
                        Boolean projMgrStatus=false;
                        Database.SaveResult[] srList = null;
                        Database.UpsertResult dbRes = null;
                        try{
                        dbRes = Database.upsert(saveRevConnect,Revenue_Connectivity__c.Fields.WebCAS_Project_ID__c, false) ;
                        srList =Database.update(oppLineItemList, false);
                        }catch(Exception ex){
                        System.debug('*****' + ex.getMessage());
                        }
                        // Iterate through each returned result
                        for (Database.SaveResult sr : srList) {
                            if (sr.isSuccess()) {
                               projMgrStatus = true;
                            }                            
                        }
                        
                        if(dbRes.isSuccess() && projMgrStatus) {
                            outpt =  OUTPUT_MSG_SUCCESS + dbRes.getId() +'--'+PROJMANAGEROUTPUT_MSG_SUCCESS + srList[0].getId();
                            
                        } else {
                             outpt = dbRes.getErrors()[0].getMessage() +'--'+srList[0].getErrors()[0].getMessage();
                        }                                     
                        callLogger(webcasProjectId,oppProductId,projStatus,projMgr,projSalesProfessional, projStartDt, projEndDt, projLOB, projSol, projAmt,
                                cepCode, chargeBasis, projectType, CurrencyIsoCode, outpt) ;
                    } catch(Exception e) {
                         callLogger(webcasProjectId,oppProductId,projStatus,projMgr,projSalesProfessional, projStartDt, projEndDt, projLOB, projSol, projAmt,
                                cepCode, chargeBasis, projectType, CurrencyIsoCode, STR_WEBSERVICEEXCEPTIONA+ constantsutility.STR_SINGLESPACE + e.getMessage()) ; 
                     }
                } else {
                         callLogger(webcasProjectId,oppProductId,projStatus,projMgr,projSalesProfessional, projStartDt, projEndDt, projLOB, projSol, projAmt,
                         cepCode, chargeBasis, projectType, CurrencyIsoCode, STR_WEBSERVICEEXCEPTIONB) ; 
                }
        }     
    }
    
    /**
    * Method to call log record
    **/
    public static void callLogger(String webcasProjectId, String oppProductId,String projStatus,String projMgr,string projSalesProfessional, String projStartDt, String projEndDt,
     String projLOB, String projSol, String projAmt, String cepCode, String chargeBasis, String projectType, String CurrencyIsoCode , String op) {
        
        logRecord('webcasProjectId = '+webcasProjectId , 'oppProductId = '+oppProductId,'projStatus = '+projStatus,'projMgr = '+projMgr,'projSalesProfessional ='+projSalesProfessional,
                     'projStartDt = '+projStartDt, 'projEndDt = '+projEndDt,'projLOB = '+projLOB, 'projSol = '+projSol, 'projAmt = '+projAmt, 
                     'cepCode = '+cepCode, 'chargeBasis = '+chargeBasis, 'projectType = '+projectType , 'CurrencyIsoCode = '+CurrencyIsoCode , op) ;
    }    

    /**
    * Method for persisting data in WebServiceLogger__c
    **/
    public static void logRecord(String param1 ,String param2 , String param4 , String param15, String param5 , String param6 , String param7 , 
     String param8 , String param9 , String param10 , String param11 ,String param12,String param13 ,String param14, String op ) {
        WebServiceLogger__c wsLog = new WebServiceLogger__c() ;
        wsLog.Param1__c  = param1;
        wsLog.Param2__c  = param2;
        //wsLog.Param3__c  = param3;
        wsLog.Param4__c  = param4;
        wsLog.Param15__c = param15;
        wsLog.Param5__c  = param5;
        wsLog.Param6__c  = param6;
        wsLog.Param7__c  = param7;
        wsLog.Param8__c  = param8;
        wsLog.Param9__c  = param9;
        wsLog.Param10__c  = param10;
        wsLog.Param11__c  = param11;
        wsLog.Param12__c  = param12;
        wsLog.Param13__c  = param13;
        wsLog.Param14__c  = param14; 
        wsLog.Output__c  = op;
        try { 
            Database.insert(wsLog,false) ;        
            
        } catch(DMLException e) {
            System.debug(e.getMessage()) ; 
        }
    }    
}