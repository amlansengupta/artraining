/* Purpose: This test class provides data coverage for mass editing of Contact Distribution records associated with 
            a Contact 
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Sarbpreet       08/21/2013  Created test class for AP64_ContactDistributionController controller class
 ==================================================================================================================*/    
@isTest
private class Test_AP64_ContactDistributionController{
    /*
     * @Description : test method to provide data coverage for mass editing of Contact Distribution records 
                      associated with a Contact
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void testContDist() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;

        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = 'ABC123';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'TestConatctName';
        testContact.AccountId = testAccount.Id;
        testContact.email = 'xyzq@abc.com';
        insert testContact;
        
        Distribution__c testDistribution = new Distribution__c();
        testDistribution.Name = 'testDistributionName';
        testDistribution.Scope__c = 'Global';
        insert testDistribution;
        
        Contact_Distributions__c testContactDist = new Contact_Distributions__c();
        testContactDist.Distribution__c = testDistribution.Id ;
        testContactDist.Distribution_Preference__c = 'Yes';
        testContactDist.Contact__c = testContact.Id;
        insert testContactDist;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            ApexPages.StandardController stdController = new ApexPages.StandardController(testContact);
            AP64_ContactDistributionListController controller = new AP64_ContactDistributionListController(stdController);
            controller.save();
            controller.cancel();
        }

    }
}