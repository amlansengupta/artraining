public class FamilyTreeChangeVFController {
    public Account thisAccount;
    public String showFamilyTreeChangeMsg{get;set;}
    
	public FamilyTreeChangeVFController(ApexPages.StandardController controller) {
    thisAccount=(Account)controller.getRecord();
    Account acc = new Account();
    acc = [Select Id,Name,Previous_Global_Ultimate_Name__c,Global_Ultimate_Name__c, Previous_HQ_Immediate_Parent_DUNS_Name__c,HQ_Immediate_Parent_DUNS_Name__c,AGU_Flag__c, D_B_Last_Update__c, Previous_GU_DUNS__c, GU_DUNS__c
           From Account Where Id = :thisAccount.Id ];
	
   // if(((String.isNotBlank(acc.Previous_Global_Ultimate_Name__c) && acc.Previous_Global_Ultimate_Name__c != acc.Global_Ultimate_Name__c) || (String.isNotBlank(acc.Previous_HQ_Immediate_Parent_DUNS_Name__c) && acc.Previous_HQ_Immediate_Parent_DUNS_Name__c != acc.HQ_Immediate_Parent_DUNS_Name__c)) && !acc.AGU_Flag__c){
 //if(((String.isNotBlank(acc.Previous_HQ_Immediate_Parent_DUNS_Name__c) && acc.Previous_HQ_Immediate_Parent_DUNS_Name__c != acc.HQ_Immediate_Parent_DUNS_Name__c)) && !acc.AGU_Flag__c){
	if(((String.isNotBlank(acc.Previous_GU_DUNS__c) && acc.Previous_GU_DUNS__c != acc.GU_DUNS__c)) && !acc.AGU_Flag__c){
        showFamilyTreeChangeMsg = 'Checked';   
     }
    
    }
}