/*
Purpose: This Controller is an extension controller for the New Sales Credit Page. 
           
==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Shashank 03/29/2013  Created Controller Class for New Sales Credit Page
=============================================================================================================================================== 
*/
public with sharing class AP38_MultipleSalesCreditController 
{
    public List<wrapSales> wrapSalesList {get; set;}
    public integer lastIndex{get; set;}
    public Sales_Credit__c salesCredit = new Sales_Credit__c();
    public Sales_Credit__c salesCreditObj{get; set;}
    public string opptyId {get;set;} 
    public integer removeIndex{get; set;}
    public double spAllocation{get; set;}
    //public double teamAllocation{get; set;}
    public double sum = 0;
    //public double sum1 = 0;
    
    public class wrapSales
    {
        public Sales_Credit__c salesCredit{get; set;}
        public integer recordIndex{get; set;}
        public wrapSales(Sales_Credit__c salesCredit, integer rIndex)
        {
            this.salesCredit = salesCredit;
            this.recordIndex = rIndex;
        }
    }
     // constructor for the class  
    public AP38_MultipleSalesCreditController(ApexPages.StandardController controller) 
    {
        wrapSalesList = new List<wrapSales>();
        wrapSalesList.add(new wrapSales(new Sales_Credit__c(), 0));
        salesCreditObj = new Sales_Credit__c();
        lastIndex = 1;
        salesCredit = (Sales_Credit__c)controller.getRecord();
        opptyId = SalesCredit.Opportunity__c;
        if(opptyId == null)
        {
            opptyId = ApexPages.currentPage().getParameters().get('retURL');
            System.debug('Opportunity Id :'+opptyId);
           if(opptyId.indexOf('/') <> -1)
           {
               opptyId = opptyId.subString(1, opptyId.length());
            }
        }
        salesCreditObj.Opportunity__c = opptyId;
        for(Opportunity opp : [select Id, (select Id, Role__c, EmpName__r.LOB__c, Percent_Allocation__c, EmpName__r.Country__c from Sales_Creditings__r) from Opportunity where Id = :opptyId])
        {
            if(opp.Sales_Creditings__r.size()>0)
            {
                for(Sales_Credit__c salesCredit : opp.Sales_Creditings__r)
                {
                    sum = sum + salesCredit.Percent_Allocation__c;
                    
                }
            }
        }
        spAllocation = sum;
       
        if(spAllocation < 0 )
        {
            spAllocation = 0;
        }
        
    }
     /*
     * @Description : This method is used to remove SalesCredit
     * @ Args       : none
     * @ Return     : void
     */
    public void removeSalesCredit()
    {
        for(Integer i = 0; i < wrapSalesList.size(); i++) {
            if(wrapSalesList[i].recordIndex == removeIndex) {
                wrapSalesList.remove(i);
            }
        }
        calculate();
    }
    /*
     * @Description : This method is used to add SalesCredit Row
     * @ Args       : none
     * @ Return     : void
     */
    public void addSalesRow() {
        
        if(wrapSalesList .size() == 100) {
            ApexPages.Message errorMaxLinesMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Limit for Sales Credit reached.');
            ApexPages.addMessage(errorMaxLinesMsg);
        }
        else {
            calculate();
            wrapSalesList.add(new wrapSales(new Sales_Credit__c(), lastIndex++));
        }
    }
    /*
     * @Description : This method is used to calculate SalesCredit 
     * @ Args       : none
     * @ Return     : void
     */
    public void calculate()
    {
        spAllocation = sum;
        //teamAllocation = 200 - sum1; 
        set<string> empSet = new set<string>();
        Map<string, Colleague__c> collMap = new Map<string, Colleague__c>();
        double sum2 = 0;
        //double sum3 = 0;
                
        for(wrapSales wSales : wrapSalesList)
        {
            if(wSales.salesCredit.EmpName__c <> null)
            {empSet.add(wSales.salesCredit.EmpName__c);}
        }
        
        for(Colleague__c coll : [select Id, LOB__c, Country__c from Colleague__c where Id IN : empSet])
        {
            collMap.put(coll.Id, coll);
        }
        
        for(wrapSales wSales: wrapSalesList)
        {
            sum2 = sum2 + wSales.salesCredit.Percent_Allocation__c;
        }
        
        spAllocation = spAllocation + sum2;
        //teamAllocation = teamAllocation - sum3;
        if(spAllocation < 0 )
        {
            spAllocation = 0;
        }
        
    }
    /*
     * @Description : This method is used to save data (mapped to the VF page)
     * @ Args       : none
     * @ Return     : pagereference
     */
    public pageReference save()
    {
        List<Sales_Credit__c> salesCreditList = new List<Sales_Credit__c>();
        pageReference pageRef = null;
        
        for(wrapSales wSales : wrapSalesList)
        {
            Sales_Credit__c salesCredit = new Sales_Credit__c();
            salesCredit.EmpName__c = wSales.salesCredit.EmpName__c;
            salesCredit.Role__c = wSales.salesCredit.Role__c;
            salesCredit.Percent_Allocation__c = wSales.salesCredit.Percent_Allocation__c;
            salesCredit.MDrive_Sales_Credit_ID__c = wSales.salesCredit.MDrive_Sales_Credit_ID__c;
            salesCredit.Opportunity__c = opptyId;
            salesCreditList.add(salesCredit);
        }
        try
        {
            insert salesCreditList;
            pageRef = new pageReference('/'+opptyId);
            pageRef.setRedirect(false);
            return pageRef;
        }
        catch(Exception ex){
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error occured!!')); 
            return null;
        }
        
        
    }
    /*
     * @Description : This method is used to cancel data
     * @ Args       : none
     * @ Return     : pagereference
     */
    public pageReference cancel()
    {
        pageReference pageRef = new pageReference('/'+opptyId);
        return pageRef;
    }

}