/*Purpose: Test Class for providing code coverage to MercVFC07_RevenueAutoAdjustmentCont class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   03/29/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
Archisman       21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify'
=======================================================
   
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_MercVFC07_RevenueAutoAdjustmentCont 
{
    /* * @Description : This test method provides data coverage for displaying prompt for Revenue auto adjustment         
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.LOB__c = '1234';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;  
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);

        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        // Request Id :12638 - Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        test.starttest();
        insert testOppty;
        
          system.runAs(User1){
        ApexPages.StandardController stdController = new ApexPages.StandardController(testOppty);
        MercVFC07_RevenueAutoAdjustmentCont controller = new MercVFC07_RevenueAutoAdjustmentCont(stdController);
          }
        test.stoptest();
        //controller.updateOpportunity();
    }
}