/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@Istest(SeeAllData=true)
public class OneTimeOppProductBatchableNew_Test {
    public static testmethod void TestMethod1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User tUser = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        insert tUser;    
        
       
        
         TriggerSettings__c TrgSetting= new TriggerSettings__c();
         TrgSetting.name='Project Trigger1';
        TrgSetting.Object_API_Name__c='Project_Exception__c';
        TrgSetting.Trigger_Disabled__c=True;
         insert TrgSetting;
        
        
        List<Project_Exception__c> peList = new List<Project_Exception__c>();
        
        Project_Exception__c peOffice = new Project_Exception__c();
        peOffice.Exception_Type__c = 'Office';
        peOffice.Office__c = 'Aarhus';
        peList.add(peOffice);
        
        Project_Exception__c peOffice1 = new Project_Exception__c();
        peOffice1.Exception_Type__c = 'Office';
        peOffice1.Office__c = 'Aarhus';
        peList.add(peOffice1);                
        
        Project_Exception__c peOneCode = new Project_Exception__c();
        peOneCode.Exception_Type__c = 'One Code';
        peOneCode.One_Code__c = 'ABC123XYZ';
        peList.add(peOneCode);
        
        Project_Exception__c peOneCode2 = new Project_Exception__c();
        peOneCode2.Exception_Type__c = 'One Code';
        peOneCode2.One_Code__c = 'ABC123XYZ';
        peList.add(peOneCode2);
               
        
        Project_Exception__c peOpportunityType = new Project_Exception__c();
        peOpportunityType.Exception_Type__c = 'Opportunity Type';
        peOpportunityType.Opportunity_Type__c = 'Rebid';
        peList.add(peOpportunityType);
        
        Project_Exception__c peProductCode_Office = new Project_Exception__c();
        peProductCode_Office.Exception_Type__c = 'Product + Office Code';
        peProductCode_Office.Product_Code__c = '4126';
        peProductCode_Office.Office__c = 'Test Office 2';
        peList.add(peProductCode_Office);
        
        Project_Exception__c peProductCode = new Project_Exception__c();
        peProductCode.Exception_Type__c = 'Product Code';
        peProductCode.Product_Code__c = '9999';
        peList.add(peProductCode);
        
        Project_Exception__c peLOB = new Project_Exception__c();
        peLOB.Exception_Type__c = 'Product LOB';
        peLOB.LOB__c = 'Career';
        peList.add(peLOB);
        
        Project_Exception__c peRecordTypeName = new Project_Exception__c();
        peRecordTypeName.Exception_Type__c = 'RecordType Name';
        peRecordTypeName.Record_Type_Name__c = 'Opportunity_Detail_Page_Assignment';
        peList.add(peRecordTypeName);
        
        Project_Exception__c peRecordTypeName1 = new Project_Exception__c();
        peRecordTypeName1.Exception_Type__c = 'RecordType Name';
        peRecordTypeName1.Record_Type_Name__c = 'Opportunity_Detail_Page_Assignment';
        peList.add(peRecordTypeName1);
        
        Project_Exception__c peProdOff = new Project_Exception__c();
        peProdOff.Exception_Type__c = 'Product + Office Code';
        peProdOff.Product_Code__c = '5642';
        peProdOff.Office__c = 'Shanghai - Huai Hai';
        peList.add(peProdOff);
        
        Project_Exception__c peTypeCounloncomName = new Project_Exception__c();
        peTypeCounloncomName.Exception_Type__c = 'Opportunity Type + Opportunity Country + Product LoB + Commission Flag';
        peTypeCounloncomName.Opportunity_Type__c = 'Existing Client - New LOB';
        peTypeCounloncomName.Opportunity_Country__c = 'Canada';
        peTypeCounloncomName.LOB__c = 'Health';
        peTypeCounloncomName.Commision__c= True;
        peList.add(peTypeCounloncomName);
        
        Project_Exception__c peTypeconProd = new Project_Exception__c();
        peTypeconProd.Exception_Type__c = 'Opportunity Type + Opp Country + Product Code';
        peTypeconProd.Opportunity_Type__c = 'Rebid';
        peTypeconProd.Opportunity_Country__c = 'Canada';
        peTypeconProd.Product_Code__c = '5642';
        peList.add(peTypeconProd);
        
        
        Project_Exception__c peTypeConComm = new Project_Exception__c();
        peTypeConComm.Exception_Type__c = 'Opportunity Country + LoB + Commission Flag';
        peTypeConComm.Opportunity_Type__c = 'Rebid';
        peTypeConComm.Opportunity_Country__c = 'Canada';
         peTypeConComm.Commision__c= True;
        peList.add(peTypeConComm);
        
        Project_Exception__c peConProd = new Project_Exception__c();
        peConProd.Exception_Type__c = 'Opportunity Country + Product code';
        peConProd.Product_Code__c = '5642';
        peConProd.Opportunity_Country__c = 'Canada';
        peList.add(peConProd);
        
        Database.insert(peList, false);
        
         Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount; 
      
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity 1';
        testOpportunity.Type = 'Rebid';
        testOpportunity.AccountId =  testAccount.Id; 
        //request id:12638;commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate =  Date.Today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Shanghai - Huai Hai';
        testOpportunity.Opportunity_Country__c = 'CANADA';
        testOpportunity.currencyisocode = 'ALL';
        //testOpportunity.Close_Stage_Reason__c ='Other';  
        
        Test.startTest();  
        insert testOpportunity;
        
       Test.stopTest();
        //PriceBookEntry info
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9999';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;  
        //Test.startTest();   
        Database.insert(opptylineItem);
        
        
     
     Id batchJobId = Database.executeBatch(new OneTimeOppProductBatchableNew(), 200);
    }
    static testmethod void method2(){
         TriggerSettings__c TrgSetting= new TriggerSettings__c();
         TrgSetting.name='Project Trigger1';
        TrgSetting.Object_API_Name__c='Project_Exception__c';
        TrgSetting.Trigger_Disabled__c=True;
         insert TrgSetting;
        
        
        List<Project_Exception__c> peList = new List<Project_Exception__c>();
                          
        
        Project_Exception__c peOneCode = new Project_Exception__c();
        peOneCode.Exception_Type__c = 'One Code';
        peOneCode.One_Code__c = 'ABC123XYZ';
        peList.add(peOneCode);                   
        insert peList;
        ConstantsUtility.STR_ACTIVE='Inactive';
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';       
        testAccount.One_Code__c='ABC123XYZ';
        insert testAccount; 
      
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity 1';
        testOpportunity.Type = 'Rebid';
        testOpportunity.AccountId =  testAccount.Id; 
        //request id:12638;commenting step
        //testOpportunity.Step__c = 'Identified Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOpportunity.StageName = 'Identify';
        testOpportunity.CloseDate =  Date.Today();
        testOpportunity.CurrencyIsoCode = 'ALL';        
        testOpportunity.Opportunity_Office__c = 'Zurich - Tessinerplatz';
        testOpportunity.currencyisocode = 'ALL';
       // testOpportunity.Close_Stage_Reason__c ='Other'; 
          Test.startTest();    
        insert testOpportunity;
       
        Product2 testProduct2 = new Product2();
        testProduct2.Name = 'TestPro';
        testProduct2.Family = 'RRF';
        testProduct2.IsActive = True;
        testProduct2.LOB__c = 'Career';
        testProduct2.Segment__c = 'TEST seg';
        testProduct2.Classification__c ='Non Consulting Solution Area';
        testProduct2.ProductCode = '9923';
        insert testProduct2;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, 
                                  CurrencyIsoCode From PricebookEntry 
                                  Where Pricebook2Id = :pb.Id And Product2Id = :testProduct2.Id
                                  And CurrencyIsoCode = 'ALL' Limit 1];
        
        //Creating Opportunity product
        OpportunityLineItem opptylineItem = new OpportunityLineItem ();
        opptylineItem.OpportunityId = testOpportunity.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;        
        opptyLineItem.Revenue_Start_Date__c =  Date.Today();
        opptyLineItem.Revenue_End_Date__c =  Date.Today();
        opptylineItem.UnitPrice = 100.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 100; 
        //opptyLineItem.Year2Revenue_edit__c = 1;   
        AP44_ChatterFeedReporting.FROMFEED = true;  
        Database.insert(opptylineItem);
         Test.stopTest();
        Id batchJobId = Database.executeBatch(new OneTimeOppProductBatchableNew(), 200);
       
     
     
    }
}