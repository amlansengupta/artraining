/*Purpose: This Apex class implements batchable interface and update those accounts whos RM's office has changed.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   12/28/2012  Created Apex Batch class
   2.0 -    Malini Prakash  07/07/2016  Modified as part of Req 10846
============================================================================================================================================== 
*/

global class AP11_ColleagueBatchable implements Database.Batchable<sObject>,Database.stateful{

    //final string to hold the query
    global final String Query;

    //List to store the records to be udpated
    global static transient List<Account> accountListFromScope = new List<Account>();
    
    global Set<String> colleagueSet;
    
    
    // Constructor to assign the query string to a final variable     
    global AP11_ColleagueBatchable (String q)
    {       
            Query=q;
            this.colleagueSet = new Set<String>(); 
    }
       
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    
        //execute the query and fetch the records    
        return Database.getQueryLocator(query);
    }
     
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
    //Iterate through the scope of records for the batch to Create a map to store colleague name and colleague record    
        List<Account> accountListForUpdate = new List<Account>();
        // set variable to store Relationship manager Ids (reference to colleague) Req 10846
        Set<Id> managerIdSet = new Set<Id>();   
        // Map variable of Colleague Id and Collegue record Req 10846
        Map<Id, Colleague__c> idColleagueMap = new Map<Id, Colleague__c>(); 
        for(sObject s: scope){
            Account accountObj = (Account)s;
            accountListFromScope.add(accountObj);
            // If Account relationship manager is not blank Req 10846
            if(accountObj.Relationship_Manager__c <> null){   
                // add to set Req 10846
                managerIdSet.add(accountObj.Relationship_Manager__c);
            }
        }
        // if set not empty Req 10846
                if(!managerIdSet.isEmpty())
                {
                    for(Colleague__c coll : [Select Id,Office__c,Market__c,Region__c,EMPLID__c,Location__r.Name,Sub_Market__c,CountryGeo__c,Sub_Region__c FROM Colleague__c WHERE Id in : managerIdSet])
                    {
                        // add to map Req 10846
                        idColleagueMap.put(coll.Id, coll);              
                    }
                }   
        
        for(Account account : accountListFromScope)
        {
            account.Account_Market__c = account.Relationship_Manager__r.Market__c;
            account.Account_Office__c = account.Relationship_Manager__r.Office__c;
            account.Account_Region__c = account.Relationship_Manager__r.Region__c;
            account.Account_Sub_Market__c = account.Relationship_Manager__r.Sub_Market__c;
            account.Account_Sub_Region__c = account.Relationship_Manager__r.Sub_Region__c;
           //Commented as part of Req 10846
           // account.Office_Code__c = account.Relationship_Manager__r.Name;
            account.Account_Country__c = account.Relationship_Manager__r.CountryGeo__c;
            //Added as part of Req 10846
            if(idColleagueMap.get(account.Relationship_Manager__c) <> null)
                    {
                        Colleague__c colleague = idColleagueMap.get(account.Relationship_Manager__c);
                        account.Office_Code__c = colleague.Location__r.Name;
                        
                    }
                    else
                    {
                        account.Office_Code__c = '';
                    }
            colleagueSet.add(account.Relationship_Manager__c);            
            //Add the record to a list          
            accountListForUpdate.add(account);
        }
        
        //Insert the list into the database    
            if(!accountListForUpdate.isEmpty())
            database.update(accountListForUpdate,false);
    }

     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC){    
        
        List<Colleague__c> colleagueListForUpdate = new List<Colleague__c>(); 
        
        System.debug('\n colleague set size :'+colleagueSet.size());
        if(!colleagueSet.isEmpty())
        {
            for(Colleague__c coll : [select Id, isUpdated__c from Colleague__c where Id IN: colleagueSet])
            {
                coll.Isupdated__c = false;
                colleagueListForUpdate.add(coll);
            }   
        }
        
        //Insert the list into the database    
        if(!colleagueListForUpdate.isEmpty())
            database.update(colleagueListForUpdate,false);        
        
    }
 
}