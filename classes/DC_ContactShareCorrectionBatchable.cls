/*
Purpose:  This Apex class implements Batchable to correct the contact share records for all contact team members 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   7/7/2014    Added Comments.
==============================================================================================================================================
*/
 global class DC_ContactShareCorrectionBatchable implements Database.Batchable<sObject>, Database.Stateful {
     
    //List to do error Logging
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */  
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // this method will return 50 million rows
        return Database.getQueryLocator(Label.DC_contactShare_query); 
 
    }   
    
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */ 
    global void execute(Database.BatchableContext bc, List<Contact_Team_Member__c> scope)
    {
        Integer errorCount = 0;
        Integer errorCountTM = 0;
        Set<Id> ConIdSet = new Set<Id>();
        Set<Id> ConTeamMemSet = new Set<Id>();
        Map<Id,set<Id>> conShareMap = new Map<Id,set<Id>>();
        
        //Take all the contact team members in to a set
        for (Contact_Team_Member__c conTeam : scope)
        {
            ConIdSet.add(conTeam.Contact__c);
            ConTeamMemSet.add(conTeam.ContactTeamMember__c);
        }
        
        Set<Id> conShareUsers = new Set<Id>();
        //find out if all
        for(ContactShare cs: [select userorgroupid,contactid from contactShare where contactid in :ConIdset])
        {
            conShareUsers.add(cs.userorgroupid);
            if(!conShareMap.containsKey(cs.userorgroupid))
                conShareMap.put(cs.userorgroupid,new Set<Id>{cs.ContactId});
            else {
                set<Id> conset = conShareMap.get(cs.userorgroupid);
                conset.add(cs.contactid);
                conShareMap.put(cs.userorgroupid,conset);
            }
        }
        
        List<ContactShare> contactShareList = new List<ContactShare>();
        for (Contact_Team_Member__c conTeam : scope)
        {
            if(!conShareMap.containsKey(conTeam.ContactTeamMember__c) || (conShareMap.containsKey(conTeam.ContactTeamMember__c) && !conShareMap.get(conTeam.ContactTeamMember__c).contains(conTeam.contact__c))){
                ContactShare share=new ContactShare ();        
                share.UserOrGroupId=conTeam.ContactTeamMember__c;
                share.ContactId=conTeam.Contact__c;
                share.ContactAccessLevel='Edit';
                contactShareList.add(share); 
            }
               
        }
        
        if(contactShareList.size()>0){
            integer i = 0;
            for(Database.Saveresult result : database.insert(contactShareList,false))
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                    errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC21:' + result.getErrors()[0].getMessage() , errorLogs, contactShareList[i].Id);  
                    errorCount++;
                }
                i++;
            }
            contactShareList.clear();
        }
        
        
        
    }
    

    /*
     *  Method Name: finish(Insert error logs in to Batch Error Logger object after the completion of all records )
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */     
    global void finish(Database.BatchableContext bc)
    {
        //insert error records if any
        if(errorLogs.size()>0) 
            Database.insert(errorLogs,false) ; 
    }     
}