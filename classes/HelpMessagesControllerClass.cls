public with sharing class HelpMessagesControllerClass {
    
   
     public list<FCST_Help_Message__c> helpMsg {get;set;}
    public list<FCST_Help_Message__c> msglist {get;set;}
    public list<FCST_Help_Message__c> helpMsgslist {get;set;}
    
        public HelpMessagesControllerClass(){
          msglist= new list<FCST_Help_Message__c>();  
        helpMsg= new list<FCST_Help_Message__c>();
                                               
         helpMsg = [Select CAssesmentMessage__c,Account_Information_Message__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ];  
        }
    
    public void AccountInformationsMessagesave(){
        list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
          msglist = [Select Account_Information_Message__c  from FCST_Help_Message__c  ];  
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0  )
                h.Account_Information_Message__c = helpMsg[0].Account_Information_Message__c;
                msglistupdate.add(h);                
            }
            update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if( helpMsg.size() > 0 )
            fc.Account_Information_Message__c = helpMsg[0].Account_Information_Message__c;
            insert fc;
        }
      
    } 
    public void CBIHelpMessagesave(){
        list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
          msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ];  
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0  )
                h.CBI_Help_Message__c = helpMsg[0].CBI_Help_Message__c;
                msglistupdate.add(h);                
            }
            update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if( helpMsg.size() > 0 )
            fc.CBI_Help_Message__c = helpMsg[0].CBI_Help_Message__c;
            insert fc;
        }
      
    }
    public void ClandscapeHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
    	  list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                 if( helpMsg.size() > 0 )
                h.CLandscapeMessage__c = helpMsg[0].CLandscapeMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
                if(helpMsg.size() > 0 )
            	fc.CLandscapeMessage__c = helpMsg[0].CLandscapeMessage__c;
            insert fc;
        }
    
    }
    public void CAssesmentMessageHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0 )
                h.CAssesmentMessage__c = helpMsg[0].CAssesmentMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if( helpMsg.size() > 0 )    
            fc.CAssesmentMessage__c = helpMsg[0].CAssesmentMessage__c;
            insert fc;
        }
     
    }
    public void CRelationshipHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                 if( helpMsg.size() > 0 )
                h.CRelationshipMessage__c = helpMsg[0].CRelationshipMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
                if( helpMsg.size() > 0  )
            	fc.CRelationshipMessage__c = helpMsg[0].CRelationshipMessage__c;
            insert fc;
        }
     
    }
     public void GoalsActionsHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0 )
                h.GoalsActionsMessage__c = helpMsg[0].GoalsActionsMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
                 if( helpMsg.size() > 0 )
            	fc.GoalsActionsMessage__c = helpMsg[0].GoalsActionsMessage__c;
            insert fc;
        }
      
    }
    public void GURollUpHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0 )
                h.GU_RollUpMessages__c = helpMsg[0].GU_RollUpMessages__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
                 if(helpMsg.size() > 0 )
            	fc.GU_RollUpMessages__c = helpMsg[0].GU_RollUpMessages__c;
            insert fc;
        }
   
    }
    public void SAnalysysHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0  )
                	h.SAnalysysMessage__c = helpMsg[0].SAnalysysMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if( helpMsg.size() > 0  )   
            fc.SAnalysysMessage__c = helpMsg[0].SAnalysysMessage__c;
            insert fc;
        }
     
    }
    public void LOBHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0 )
                h.LOBMessage__c = helpMsg[0].LOBMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if(helpMsg.size() > 0 )    
            fc.LOBMessage__c = helpMsg[0].LOBMessage__c;
            insert fc;
        }
     
    }
    public void MFRoollUpHelpMessagesave(){
         list<FCST_Help_Message__c>  msglistupdate = new list<FCST_Help_Message__c>();
        list<FCST_Help_Message__c>  msglist = [Select CAssesmentMessage__c,CBI_Help_Message__c,CLandscapeMessage__c,CRelationshipMessage__c,GoalsActionsMessage__c,
                                               GU_RollUpMessages__c,LOBMessage__c,MF_RoollUpMessage__c,SAnalysysMessage__c from FCST_Help_Message__c  ]; 
        if(msglist.size() > 0){
            for(FCST_Help_Message__c h:msglist ){
                if( helpMsg.size() > 0  )
                h.MF_RoollUpMessage__c = helpMsg[0].MF_RoollUpMessage__c;
                 msglistupdate.add(h);                
            }
           update msglistupdate;
        }
        else {
            FCST_Help_Message__c fc = new FCST_Help_Message__c();
             if( helpMsg.size() > 0  )    
            	fc.MF_RoollUpMessage__c = helpMsg[0].MF_RoollUpMessage__c;
            insert fc;
        }
      
    }
}