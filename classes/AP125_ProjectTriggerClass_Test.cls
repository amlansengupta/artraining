/*Purpose: Apex test class to to provide coverage for AP125_ProjectTriggerClass
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Bala     9/4/2015    Provide coverage for AP125_ProjectTriggerClass
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                   Date                    Modified By
12638:Removing Step                          17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@IsTest(SeeAllData=False)
private class AP125_ProjectTriggerClass_Test{
     private static Integer month = System.Today().month();
     private static Integer year = System.Today().year();

     @testSetup static void setup(){
        Mercer_TestData mdata = new Mercer_TestData();

        Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
        setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
         /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */
        
        Insert listofvalues;
        
        // insert Account 
        
    }


     static testmethod void ProjectTriggerClassTest(){
                Mercer_TestData mdata = new Mercer_TestData();
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        String recipientid=user1.id;  
         Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
         Account accrec =  mdata.buildAccount();
         Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
           
        insert testContact;
          Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '12345678903';
        testColleague1.LOB__c = '1234';
        testColleague1.Location__c = mogd.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        testColleague1.Empl_Status__c = 'Active';
        insert testColleague1;
         
         Opportunity opprec = new Opportunity();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        //request id:12638 commenting step
        //opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Pending Project';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        
        Test.startTest();   
        insert opprec;
         
            
        TriggerSettings__c TrgSetting= new TriggerSettings__c();
         TrgSetting.name='Project Trigger';
        TrgSetting.Object_API_Name__c='Revenue_Connectivity__c';
        TrgSetting.Trigger_Disabled__c=false;
         insert TrgSetting;
        
        String OppName= opprec.Name;
        String OppID=opprec.Id;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
    
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = opprec.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptylineItem.product2Id=pro.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+3;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+2;
        opptyLineItem.UnitPrice = 1.00;
        opptyLineItem.CurrentYearRevenue_edit__c = 1.00;
        opptyLineItem.Year2Revenue_edit__c = null;
        opptyLineItem.Year3Revenue_edit__c = null;
        insert opptylineItem;
        Revenue_Connectivity__c testProject = new Revenue_Connectivity__c();
        testProject.Product__c=pro.Id;
        testProject.CEP_Code__c = 'WEB002-CEP-CODE';
        testProject.Charge_Basis__c = 'S';
        testProject.Project_Amount__c = 20000.00;
        testProject.Colleague__c = testColleague1.Id;
        testProject.Opportunity__c = opprec.Id;
        testProject.Opportunity_Product_Id__c = opptylineItem.id;
        testProject.Project_End_Date__c = date.newInstance(year, month, 1);
        testProject.Project_LOB__c = 'Benefits Admin';
        testProject.Project_Manager__c = '866608';
        testProject.Project_Name__c = 'Absence Management - PROJ01';
        testProject.Project_Solution__c = '3051';
        testProject.Project_Start_Date__c = date.newInstance(year, month, 10);
        testProject.Project_Status__c = 'Active';
        testProject.Project_Type__c = 'Project';
        testProject.WebCAS_Project_ID__c = 'WEB001';
        
        insert testProject;
        
        
          opprec.Buyer__c=testContact.id;
          //request id:12638 commenting step
        opprec.stageName = 'Pending Project';
        //opprec.Step__c='Pending Chargeable Code';
        opprec.Close_Stage_Reason__c ='Other';
        
        //update opprec;
         Test.stopTest(); 
        testProject.Charge_Basis__c = 'T';
        testProject.Project_Status__c = 'InActive';
        update testProject;
               
       

        
        Map<id,Opportunity> mapop= New Map<id,Opportunity>(); 
        mapop.put(opprec.id,opprec);
        
        List<Revenue_Connectivity__c> lstrev= New List<Revenue_Connectivity__c>();
        
        lstrev.add(testProject);
       
        Map<id,Revenue_Connectivity__c> maprev= New Map<id,Revenue_Connectivity__c>(); 
        maprev.put(opprec.id,testProject);
        
       
         
        String calledFrom= 'AP125_ProjectTriggerClass'; 
        //String calledFrom1='AP02_OpportunityTriggerUtil';
        
        System.runAs(user1) {
        
        AP125_ProjectTriggerClass.updateOpportunityStep(mapop,calledFrom);
        //AP125_ProjectTriggerClass.updateOpportunityStep(mapop,calledFrom1);
        //AP125_ProjectTriggerClass.chatterPost(recipientid,OppName,'Closed / Won (SOW Signed)',OppID);
       // AP125_ProjectTriggerClass.updateColleagueandProudct(lstrev,maprev);
       AP125_ProjectTriggerClass.chatterPost(recipientid,OppName,'Active',OppID,'AP02_OpportunityTriggerUtil');
       
        
        }
     
 }

}