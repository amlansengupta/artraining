/*
Purpose: This Apex class implements Batchable for account status.
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR               DATE                 DETAIL    
1.0 -       Arijit               04/29/13             Created Apex Batch class
2.0 -       Jagan                05/22/14             Updated to check if the status is not already same
==============================================================================================================================================
 */

global class MS04_OpportunityClosedWonBatchable implements Database.Batchable<sObject>, Database.Stateful{
    
    global static Map<String, String> filteredMap = new Map<String, String>{
        'Open Opportunity - Stage 2+' => 'Open Opportunity - Stage 2+',
        'Open Opportunity - Stage 1'  => 'Open Opportunity - Stage 1'       
    };
    Date d = system.today() - 180;
    global static List<Schema.PicklistEntry> statusPickVal = Account.MercerForce_Account_Status__c.getDescribe().getPicklistValues();
    global static List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    global static String query = 'Select Id, StageName, AccountId,Account.MercerForce_Account_Status__c From Opportunity Where StageName = ' + '\'' + 'Closed / Won' + '\'' + ' AND Account.MercerForce_Account_Status__c NOT IN ' + MercerAccountStatusBatchHelper.quoteKeySet(filteredMap.keyset()) + ' AND CloseDate >:d'; 
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         if(Test.isRunningTest())
        {
            String oppname = 'Test Opportunity1' + String.valueOf(Date.Today());  
            
            query = 'Select Id, StageName, AccountId,Account.MercerForce_Account_Status__c From Opportunity Where Name IN ' + MercerAccountStatusBatchHelper.quoteKeySet(new Set<String>{oppname}); 
        }
        System.debug('\n Constructed query String :'+query);
        return Database.getQueryLocator(query);  
    }
    
      /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        Map<String, Account> accountMapForUpdate = new Map<String, Account>();
        System.debug('No of Records in scope :'+scope.size());
        for(Opportunity opportunity : (List<Opportunity>) scope)
        {
            if(!accountMapForUpdate.containsKey(opportunity.AccountId) && opportunity.account.MercerForce_Account_Status__c <> 'Recent Win')
            {
                Account account  = new Account(Id = opportunity.AccountId);
                account.MercerForce_Account_Status__c = 'Recent Win';
                accountMapForUpdate.put(account.Id, Account);   
            }
                
        }
        
        if (!accountMapForUpdate.isempty())
        {
        
        for(Database.Saveresult result : Database.update(accountMapForUpdate.values(), false))
            {
                // To Do Error logging
                if(!result.isSuccess() && MercerAccountStatusBatchHelper.createErrorLog())
                {
                  errorLogs = MercerAccountStatusBatchHelper.addToErrorLog(result.getErrors()[0].getMessage(), errorLogs, result.getId());  
                }
            }
        }
    }
    
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
        try
        {
            Database.insert(errorLogs, false);
        }catch(DMLException dme)
        {
            System.debug('\n exception has occured' +dme.getDMLMessage(0));
        }finally
        {
            MercerAccountStatusBatchHelper.callNextBatch('High Revenue');
        }
        
    }
    
   
}