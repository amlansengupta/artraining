/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class AP111_PastDueOpptyRemindBatchable_Test {
    private static Mercer_TestData mtdata =new  Mercer_TestData();
  
   /*
     * @Description : Test method for AP111_PastDueOpptyRemindBatchable batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
  
         

         Colleague__c col = mtdata.buildColleague();
         
         User user1 = new User();
         String adminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
         user1 = Mercer_TestData.createUser1(adminUserprofile , 'usert2', 'usrLstName2', 2);
                 
         Account acc = mtdata.buildAccount();        
         Test.startTest(); 
         Opportunity opp = mtdata.buildOpportunity();
         //Request Id:12638 commenting step
         //opp.step__c = 'Identified Single Sales Objective(s)';
         opp.stagename = 'Identify';
         opp.CloseDate = system.today();
         opp.Email_Sent__c = false;
         opp.ownerid = user1.id;
         Test.stopTest();
         update opp;
                
         system.runAs(User1){
          
          
         String  qry = 'Select Id, Name, CloseDate, isClosed, OwnerID,Owner.FirstName, Owner.LastName, Email_Sent__c, Total_Opportunity_Revenue__c, Account.name, AccountID,CurrencyIsoCode from Opportunity where Email_Sent__c = false and isClosed = false and Closedate <= TODAY and ownerid = \''+ opp.ownerid  + '\'';
         database.executeBatch(new AP111_PastDueOpptyRemindBatchable(qry), 100); 
         
        }    
        
    }
    /*
     * @Description : Test method schedulable method
     * @ Args       : Null
     * @ Return     : void
     */
    /* static testMethod void testOpportunitySchedulable()  
       { 
        
    
             User user1 = new User();
             String adminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
             user1 = Mercer_TestData.createUser1(adminUserprofile , 'usert2', 'usrLstName2', 2);
             system.runAs(User1){       
             AP111_PastDueOpptyRemindBatchable oppsch = new AP111_PastDueOpptyRemindBatchable();      
             DateTime rst = DateTime.now();     
             String nextTime = String.valueOf(rst.second()) + ' ' + String.valueOf(rst.minute()) + ' ' + String.valueOf(rst.hour() ) + ' * * ?';       
             system.schedule('AP111_PastDueOpptyRemindBatchable', nextTime, oppsch);          
            }  
       } */
    
}