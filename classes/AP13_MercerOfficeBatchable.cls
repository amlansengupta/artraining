/*Purpose: This Apex class implements batchable interface and update those accounts whos RM's office records have changed.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Joban      12/28/2012  Created Apex Batch class
   2.0 -    Sarbpreet  7/3/2014    Updated Comments/documentation.
============================================================================================================================================== 
*/
global class AP13_MercerOfficeBatchable implements Database.Batchable<sObject>,Database.stateful{ 
    
    //final string to hold the query
    global final String accQuery;
    
    global Map<String, Mercer_Office_Geo_D__c> officeMap = new Map<String, Mercer_Office_Geo_D__c>();
    
    /*
    *       Constructor to assign the query string to a final variable  
    */ 
    global AP13_MercerOfficeBatchable(String q)
    {
         accQuery = q; 
         
         for(Mercer_Office_Geo_D__c officeGeo : [Select Id, Market__c, Office__c, Region__c, Sub_Market__c, Name, Sub_Region__c, Country__c FROM Mercer_Office_Geo_D__c Where isUpdated__c = true])
         {
            officeMap.put(officeGeo.Name, officeGeo);   
         }
            
    }

    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
     global Database.QueryLocator start(Database.BatchableContext BC)
    {

        //execute the query and fetch the records   
        return Database.getQueryLocator(accQuery);
    }

/***************************************************************************************************************
     
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {

        //Transient lists to hold the account, Opportunity and User records
        transient List<Account> accountList = new List<Account>();
        
                 
        //Query accounts and assign the values of changed Office Fields to corresponding Account fields     
        for(Account account : (List<Account>)scope)
        {
            account.ByPassVR__c = true; 
            if(officeMap.get(account.Office_Code__c) <> null)
            {
                account.Account_Market__c = officeMap.get(account.Office_Code__c).Market__c;
                account.Account_Office__c = officeMap.get(account.Office_Code__c).Office__c;
                account.Account_Region__c = officeMap.get(account.Office_Code__c).Region__c;
                account.Account_Sub_Market__c = officeMap.get(account.Office_Code__c).Sub_Market__c;
                account.Account_Sub_Region__c = officeMap.get(account.Office_Code__c).Sub_Region__c;
                account.Office_Code__c = officeMap.get(account.Office_Code__c).Name;
                account.Account_Country__c = officeMap.get(account.Office_Code__c).Country__c;
                //Add the record to a list        
                accountList.add(account);   
            }
            
            
        }

            //update the list of accounts
            if(!accountList.isEmpty())
                database.update(accountList,false);
                         
    }

    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */    
    global void finish(Database.BatchableContext BC)
    {
        DateTime sysTime = System.now();
        sysTime = sysTime.addSeconds(360);
        String CRON_EXP = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        AP30_OpportunitySyncSchedulable oppSchedule = new AP30_OpportunitySyncSchedulable();
        System.schedule('Sync Opportunity From Geo ' + String.valueOf(Date.Today()), CRON_EXP, oppSchedule);   
    }
 
 }