public class AP_CustomLookupForContact_Extension {
    PRIVATE FINAL STRING  SEARCHCONTACTINACCOUNT ='Search Contacts in Current Account';
    PRIVATE FINAL STRING SEARCHALLCONTACT='Search All Contacts';
    
    Buying_Influence__c biobj;
    ID currentOpp;
    ID currentAcc;
    ID currentBI;
	
	private integer totalRecs = 0;
	private integer OffsetSize = 0;
	private integer LimitSize= 10;
    
	private id parentOpportunity;
    private id parentAccount;
    
    private string search;
    private string tempString;
    public string customText {get;set;}
    
    public string defaultRadioVal {get;set;}
    public List<Contact> showContacts {get;set;}
    
    public AP_CustomLookupForContact_Extension(ApexPages.StandardController controller){
        
        defaultRadioVal = 'Search Contacts in Current Account';
        if(ApexPages.currentPage().getParameters().get('oppid') !='' || ApexPages.currentPage().getParameters().get('accid')!=''){
            parentOpportunity = ApexPages.currentPage().getParameters().get('oppid');
        	parentAccount = ApexPages.currentPage().getParameters().get('accid');
            showContacts = [SELECT name,id,account.name,Contact_Status__c,Title,Phone, Email, Owner.FirstName, Owner.LastName FROM Contact WHERE AccountID =: parentAccount AND Contact_Status__c = 'Active' ORDER BY LastName ASC LIMIT : LimitSize OFFSET: OffsetSize];
            	totalRecs = [SELECT count() FROM Contact WHERE AccountID =: parentAccount AND Contact_Status__c = 'Active' ];
                if(showContacts.isEmpty() || showContacts.size()==0){
                	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no Contacts in this Account. Please \'Search All Contacts\' or add Contacts to this Account.'));
            	}
        } else{
            this.currentBI = controller.getID();
            System.debug('Buying Influence: '+currentBI);
            
        }
       
        
    }
    
    
    public list<SelectOption> getItems(){
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('Search Contacts in Current Account','Search Contacts at this Account'));
        option.add(new SelectOption('Search All Contacts','Search All Contacts'));
        return option;
    } 
    
    public PageReference searchCustomLookUp(){
        system.debug('---searchCustomLookUp called---');
        System.debug('CustomString: '+customText);
        /*if(customText.equals('') || customText==NULL){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Enter Text in \'Search Contact\' Field'));
        }*/
         if(SEARCHCONTACTINACCOUNT.equals(defaultRadioVal)){
            showContacts = new List<Contact>();
            if(customText.containsAny('*')){
                System.debug('I am containng some wild cards');
                if(customText.countMatches('*') == 1){
                    System.debug('I have one wild card');
                    if(customText.trim().startsWith('*')){
                        System.debug('I am seaching with wildcard at the start of the strng');
                        tempString =  customText.trim().substring(1);
                        search = '%'+tempString;
                        System.debug('My search string is: '+search);
                    }
                    else if(customText.trim().endsWith('*')){
                        System.debug('I am seaching with wildcard at the end of the strng');
                        tempString =  customText.trim().substring(0,customText.length()-1);
                        search = tempString+'%';
                        System.debug('My search string is: '+search);
                    }
                    else if(!customText.trim().startsWith('*') && !customText.trim().endsWith('*')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                        search = '';
                    }
                } else if(customText.countMatches('*')==2){
                  	if(customText.trim().startsWith('*') && customText.trim().endsWith('*')){
                        System.debug('I am seaching with wildcard at both end of the string');
                        tempString = customText.trim().substring(1,customText.length()-1);
                        search = '%'+tempString+'%';
                    } else if(!customText.trim().startsWith('*') && !customText.trim().endsWith('*')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                        search = '';
                    }
                } else if(customText.countMatches('*')>2){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only two wildcards(\'*\') can be added in the search string at a time!Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                    search = '';
                }
            }else{
                search = '%' + customText + '%';
            }
            
            if((parentAccount!=null || !parentAccount.equals('')) && (!String.isEmpty(customText) && !customText.equals('') && customText!=NULL )){
                System.debug('I am searching is search Name');
                System.debug('Search String '+search);
                showContacts = [SELECT name,id,account.name,Contact_Status__c,Title,Phone, Email, Owner.FirstName, Owner.LastName FROM Contact WHERE Name Like: search AND AccountID =: parentAccount AND Contact_Status__c = 'Active' ORDER BY LastName ASC LIMIT :LimitSize OFFSET :OffsetSize];
                totalRecs = [SELECT count() FROM Contact WHERE Name Like: search AND AccountID =: parentAccount AND Contact_Status__c = 'Active' ];
                if(showContacts.isEmpty() || showContacts.size()==0){
                	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no Contacts in this Account. Please \'Search All Contacts\' or add Contacts to this Account.'));
            	}
            }
            else if(String.isEmpty(customText) || customText.equals('') || customText==NULL){
                System.debug('I am searching without search Name');
                showContacts = [SELECT name,id,account.name,Contact_Status__c,Title,Phone, Email, Owner.FirstName, Owner.LastName FROM Contact WHERE AccountID =: parentAccount AND Contact_Status__c = 'Active' ORDER BY LastName ASC LIMIT :LimitSize OFFSET :OffsetSize];
                totalRecs = [SELECT count() FROM Contact WHERE AccountID =: parentAccount AND Contact_Status__c = 'Active' ];
                if(showContacts.isEmpty() || showContacts.size()==0){
                	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no Contacts in this Account. Please \'Search All Contacts\' or add Contacts to this Account.'));
            	}
            }
            
        }
        else if(SEARCHALLCONTACT.equals(defaultRadioVal)){
            showContacts = new List<Contact>();
                if(customText.containsAny('*')){
                System.debug('I am containng some wild cards');
                if(customText.countMatches('*') == 1){
                    System.debug('I have one wild card');
                    if(customText.trim().startsWith('*')){
                        System.debug('I am seaching with wildcard at the start of the strng');
                        tempString =  customText.trim().substring(1);
                        search = '%'+tempString;
                        System.debug('My search string is: '+search);
                    }
                    else if(customText.trim().endsWith('*')){
                        System.debug('I am seaching with wildcard at the end of the strng');
                        tempString =  customText.trim().substring(0,customText.length()-1);
                        search = tempString+'%';
                        System.debug('My search string is: '+search);
                    }
                    else if(!customText.trim().startsWith('*') && !customText.trim().endsWith('*')){
                        
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                        search = '';
                    }
                } else if(customText.countMatches('*')==2){
                  	if(customText.trim().startsWith('*') && customText.trim().endsWith('*')){
                        System.debug('I am seaching with wildcard at both end of the string');
                        tempString = customText.trim().substring(1,customText.length()-1);
                        search = '%'+tempString+'%';
                    } else if(!customText.trim().startsWith('*') && !customText.trim().endsWith('*')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                    }
                } else if(customText.countMatches('*')>2){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Only two wildcards(\'*\') can be added in the search string at a time!Please add wild cards(\'*\') either at the start or/and at the end of the search string!'));
                    search = '';
                }
            } 
            else if(String.isEmpty(customText) || customText.equals('') || customText == NULL){
                System.debug('CustomText here is null!');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Enter Text in \'Search Contact\' Field '));
                search = '';
            }else{
                search = '%' + customText + '%';
            }
            showContacts = [SELECT name,id,account.name,Contact_Status__c,Title,Phone, Email, Owner.FirstName, Owner.LastName FROM Contact WHERE Name Like: search AND Contact_Status__c = 'Active' ORDER BY LastName ASC LIMIT :LimitSize OFFSET :OffsetSize];
            System.debug('Search String = '+search);
            System.debug('the list contains following item: '+showContacts);
            totalRecs = [SELECT count() FROM Contact WHERE Name Like: search AND Contact_Status__c = 'Active'];
            if(showContacts.isEmpty() || showContacts.size()==0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Contact not found. The contact you searched for is not Active or does not exist.'));
            }
        }
        RETURN NULL;
    }
	
	public void FirstPage()
	{
		OffsetSize = 0;
        searchCustomLookUp();
	}
	public void previous()
	{
		OffsetSize = OffsetSize - LimitSize;
        System.debug('OffsetSize: '+OffsetSize);
        searchCustomLookUp();
	}
    public void next()
	{
		OffsetSize = OffsetSize + LimitSize;
        System.debug('OffsetSize: '+OffsetSize);
		searchCustomLookUp();
	}public void LastPage()
	{
		OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
		searchCustomLookUp();
	}
	public boolean getprev()
	{
		if(OffsetSize == 0)
		return true;
		else
		return false;
	}
	public boolean getnxt()
	{
		if((OffsetSize + LimitSize) > totalRecs)
			return true;
		else
			return false;
	}
    public Integer getPageNumber() {
        if(showContacts!=NULL){
            if(showContacts.size()==0 || showContacts.isEmpty())
            return 1;
        else
      		return OffsetSize/LimitSize + 1;
        }
        else
            return 1;
	}
    public Integer getTotalPages() {
        if(showContacts!=NULL){
        	if(showContacts.size()==0 || showContacts.isEmpty() )
            	return 1;
			else if (math.mod(totalRecs, LimitSize) > 0) {
                System.debug('I am HERE 1');
                System.debug('THe total Recs: '+totalRecs+' LimitSize: '+LimitSize+' return Value: '+(totalRecs/LimitSize));
         		return totalRecs/LimitSize + 1;
			} 
            else {
                System.debug('I am HERE 2');
            	if(totalRecs/LimitSize == 0){
                    System.debug('I am HERE 3');
                	return 1;
            	}
                else{
                    System.debug('I am HERE 4');
                   return (totalRecs/LimitSize); 
                }
         		
			}  
        }
        else{
            System.debug('I am HERE 5');
            return 1;
        }
        
	}
    public void radioValueChanged(){
        totalRecs = 0;
		OffsetSize = 0;
		LimitSize= 10;
    }
    // used by the visualforce page to send the link to the right dom element
	public string getFormTag() {
		return System.currentPageReference().getParameters().get('frm');
	}

	// used by the visualforce page to send the link to the right dom element for the text box
	public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
	}
    
}