global class BatchToCopyGPDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private string header;
	private string finalSuccessStr;
	private string finalFailStr;
	
    global BatchToCopyGPDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        header = 'Account Id,Planning Year,FCST Planning Year,Status,Account Forecast Type,CGP Form Type,Economic Factors,Political Factors,Regulatory Factors,Client Challenges,Organization Changes,Organization Changes Show/Hide ,Record Status \n';
        string hearder2= 'Id,Account Id,Planning Year,FCST Planning Year,Status,Account Forecast Type,CGP Form Type,Economic Factors,Political Factors,Regulatory Factors,Client Challenges,Organization Changes,Organization Changes Show/Hide ,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        set<Id> gpIds = new set<Id>();
		if(fromSelectedYear!=null && fromSelectedYear!=''){
            return Database.getQueryLocator('select Actions__c,Are_Organization_Changes__c,Economic_Factors__c,Political_Factors__c,Regulatory_Factors__c,Client_Challenges__c,id,Planning_Year__c,GP_Account_Type__c,GP_Account_Forecast_Type__c,Status__c,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear');
		}
		else{
			return null;
		}	
	} 
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
    	System.debug('>>>>oldGPList...'+oldGPList.size());
        
    	List<FCST_Fiscal_Year_List__c>  fsYrList = new List<FCST_Fiscal_Year_List__c>();
    	List<Growth_Plan__c>  newGPList = new List<Growth_Plan__c>();
    	set<Id>  alreadyExistingGpAccountSet = new  Set<Id>();
    	List<Growth_Plan__c>  alreadyExistingGPList =[select Economic_Factors__c,Political_Factors__c,Regulatory_Factors__c,Client_Challenges__c,id,Planning_Year__c,GP_Account_Type__c,GP_Account_Forecast_Type__c,Status__c,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:toSelectedYear];
    	
    	if(alreadyExistingGPList!=null && alreadyExistingGPList.size()>0){
    		for(Growth_Plan__c gpObj:alreadyExistingGPList){
    			alreadyExistingGpAccountSet.add(gpObj.Account__c);
    		}
    	}
    	System.debug('>>>>alreadyExistingGPList...'+alreadyExistingGPList.size());
    	try {
	    	if(toSelectedYear!=null){
	    		fsYrList =[Select Name from FCST_Fiscal_Year_List__c where Name=:toSelectedYear];
	    		System.debug('>>>>toSelectedYear...'+toSelectedYear);
	    		System.debug('>>>>fsYrList...'+fsYrList);
	    		if(fsYrList!=null && fsYrList.size()>0 && oldGPList!=null && oldGPList.size()>0){
	    			
	    			for(Growth_Plan__c gpOldObj:oldGPList){
	    				if(!alreadyExistingGpAccountSet.contains(gpOldObj.Account__c)){
		    				Growth_Plan__c gpNewObj = new Growth_Plan__c();
		    				gpNewObj.Account__c=gpOldObj.Account__c;
		    				gpNewObj.fcstPlanning_Year__c=fsYrList[0].Id;
		    				gpNewObj.Planning_Year__c=toSelectedYear;
		    				
		    				if(gpOldObj.Status__c!=null)
		    					gpNewObj.Status__c=gpOldObj.Status__c;
		    					
		    			    if(gpOldObj.Actions__c!=null)
		    					gpNewObj.Actions__c=gpOldObj.Actions__c;
		    					
		    			    if(gpOldObj.Are_Organization_Changes__c!=null)
		    					gpNewObj.Are_Organization_Changes__c=gpOldObj.Are_Organization_Changes__c;
		    				
		    				if(gpOldObj.GP_Account_Forecast_Type__c!=null)
		    					gpNewObj.GP_Account_Forecast_Type__c=gpOldObj.GP_Account_Forecast_Type__c;
		    				
		    				if(gpOldObj.GP_Account_Type__c!=null)
		    					gpNewObj.GP_Account_Type__c=gpOldObj.GP_Account_Type__c;
		    					
		    			    if(gpOldObj.Economic_Factors__c!=null)
		    					gpNewObj.Economic_Factors__c=gpOldObj.Economic_Factors__c;
		    					
		    				if(gpOldObj.Political_Factors__c!=null)
		    					gpNewObj.Political_Factors__c=gpOldObj.Political_Factors__c;
		    					
		    				if(gpOldObj.Regulatory_Factors__c!=null)
		    					gpNewObj.Regulatory_Factors__c=gpOldObj.Regulatory_Factors__c;
		    				
		    				if(gpOldObj.Client_Challenges__c!=null)
		    					gpNewObj.Client_Challenges__c=gpOldObj.Client_Challenges__c;
		    				
		    				newGPList.add(gpNewObj);
	    				}
	    			}
	    			 System.debug('>>>>newGPList...'+newGPList.size());
	    			if(newGPList!=null && newGPList.size()>0){
	    				Database.SaveResult[] srList = Database.insert(newGPList,false);
	    			
	    			
		    			if(srList!=null && srList.size()>0){
		    				for(Integer i=0;i<srList.size();i++){
		    					
		    					String action='',clientChallenges='';
		    					if(newGPList[i].Actions__c!=null)
		    						action=newGPList[i].Actions__c;
		    					if(newGPList[i].Client_Challenges__c!=null)
		    						clientChallenges=newGPList[i].Client_Challenges__c;
		    						
		    						
		    					if(srList.get(i).isSuccess()){
		    						finalSuccessStr+=srList.get(i).getId()+','+newGPList[i].Account__c+','+newGPList[i].Planning_Year__c+','+newGPList[i].fcstPlanning_Year__c+','+newGPList[i].Status__c+','+newGPList[i].GP_Account_Forecast_Type__c+','+newGPList[i].GP_Account_Type__c+','+newGPList[i].Economic_Factors__c+','+newGPList[i].Political_Factors__c+','+newGPList[i].Regulatory_Factors__c+','+clientChallenges.replace(',', '')+','+action.replace(',', '')+','+newGPList[i].Are_Organization_Changes__c+', Successfully created \n';	}
		    					if(!srList.get(i).isSuccess()){
		    						Database.Error error = srList.get(i).getErrors().get(0);
		    						finalFailStr+=newGPList[i].Account__c+','+newGPList[i].Planning_Year__c+','+newGPList[i].fcstPlanning_Year__c+','+newGPList[i].Status__c+','+newGPList[i].GP_Account_Forecast_Type__c+','+newGPList[i].GP_Account_Type__c+','+newGPList[i].Economic_Factors__c+','+newGPList[i].Political_Factors__c+','+newGPList[i].Regulatory_Factors__c+','+clientChallenges.replace(',', '')+','+action.replace(',', '')+','+newGPList[i].Are_Organization_Changes__c+','+error.getMessage()+' \n';
		    					}
		    				}
		    			}
	    			}
	    				
	    		 System.debug(' GrowthPlan finalSuccessStr..... '+finalSuccessStr);
	    		 System.debug(' GrowthPlan finalFailStr..... '+finalFailStr);
	    		    
	    		}
    		}
    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }	
    }
    
    global void finish(Database.BatchableContext bc){
    	
    	System.debug(' GrowthPlan finish finalSuccessStr..... '+finalSuccessStr);
	    System.debug(' GrowthPlan finish finalFailStr..... '+finalFailStr);
	    
		 AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'GrowthPlan Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'GrowthPlan Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Growth Plan Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Growth Plan copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
    	
	}
}