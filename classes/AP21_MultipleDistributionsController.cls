/*Purpose: This Controller is an extension controller for the Multiple Distributions Page. 
           It adds multiple rows of Contact Distributions for the user to enter.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE             DETAIL 
   1.0 -             Arijit            3/4/2013      Created Controller Class for Multiple Distributions Page
============================================================================================================================================== 
*/

public with sharing class AP21_MultipleDistributionsController {
    
    //string variable to store related contact Id
    public string contactId {get; set; }
    //string variable to store header 
    public string sectionHeaderSubTitle {get;set;}
    //list variable to store Contact Distributions
    public List<Contact_Distributions__c> insertDistribution = new List<Contact_Distributions__c>();
    //integer variable to store last index
    public integer lastIndex {get;set;}
    //integer variable to store index to be removed
    public Integer removeIndex {get; set;}
    //list to store wrapper object
    public list<wrapDistribution> distributionList {get;set;}
    //variable to store Contact Distribution
    public Contact_Distributions__c contDis {get;set;}
    public Id DistId;
         
   
    //wrapper class to store Contact Distribution and Integer value
    public class wrapDistribution {
        
        public Contact_Distributions__c Distribution {get;set;}
        
        public integer recordIndex {get;set;}
        public boolean bool {get; set;}
        
        //constructor for wrapper
        public wrapDistribution(Contact_Distributions__c conDistribution, integer rIndex, boolean b) {
            this.Distribution = conDistribution;
            this.Distribution.Distribution_Preference__c = 'Yes';
            this.bool = b;
            this.recordIndex = rIndex;
        }
    }

    //constructor for extension controller
    public AP21_MultipleDistributionsController(ApexPages.StandardController controller) 
    {
        //variable initialisation
        sectionHeaderSubTitle = 'New Contact Distributions';
        contDis =  new Contact_Distributions__c();
        distributionList = new List<wrapDistribution>();
         Distribution__c dist;
        
       
        lastIndex = 1;
        //Added code to find if this page is coming from a save and new button
        String SavendNew = system.currentPageReference().getParameters().get('save_new');
        system.debug('Value for save and new..'+SavendNew);
        if(SavendNew != null && SavendNew == '1'){
            String retur = system.currentPageReference().getParameters().get('retURL'); String testString = ApexPages.currentPage().getUrl();
            //String retur = system.currentPageReference().getParameters().get('inContextOfRef');
            
           System.debug('&&&&&'+testString);            
            if(retur!=null && retur.contains('inContextOfRef')){ Integer index = retur.indexOf('inContextOfRef'); String sr = 'inContextOfRef'; Integer len = sr.length();
            System.debug('&&&&&'+index);
            System.debug('retur '+retur);
            Integer index1 = retur.indexOf('%'); if(index1==-1){ String str = retur.substring(index+len+3);string conId=EncodingUtil.base64Decode(str).toString();
            
             system.debug('***'+str);  
             //str = str.substring(0,str.indexOf('&'));              
             
                system.debug('***'+conId);
                Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(conId);  Map<String, Object>  m2=(Map<String, Object>)meta.get('attributes');
                
                System.debug(m2.get('recordId'));
				if(m2.get('objectApiName').toString().equalsIgnoreCase('Contact')){
               		contactId =(string) m2.get('recordId');contDis.Contact__c = contactId;
                }
            }
            else{
               String str = retur.substring(index+len+3,index1); system.debug('***'+str); string conId=EncodingUtil.base64Decode(str).toString(); 
                system.debug('***'+conId);
                Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(conId);  Map<String, Object>  m2=(Map<String, Object>)meta.get('attributes');
                System.debug(m2.get('recordId'));
                if(m2.get('objectApiName').toString().equalsIgnoreCase('Distribution__c')){DistId = (String)m2.get('recordId');contDis.distribution__c = DistId;
               		
                }dist =[select id from Distribution__c where id=:DistId ];
                
            }
            
            }
            else{ contactId = retur.substring(1,retur.length());  contDis.Contact__c = contactId;
               
            //contactId = retur.substring(2,retur.length());
            //System.debug('Hi '+contactId);
            
            system.debug('Contact id for save and new...'+contactId);
            }
        }
        else{
            contDis = (Contact_Distributions__c)controller.getRecord();
            system.debug('COntact distribution record....'+contDis );
            contactId = contDis.Contact__c;    
        }
        if(dist!=null){
            Contact_Distributions__c cd= new Contact_Distributions__c();cd.distribution__c = dist.id;distributionList.add(new wrapDistribution(cd, 0, false));
 }
        else{
        distributionList.add(new wrapDistribution(new Contact_Distributions__c(), 0, false));
        }
        
    }
    
    /*
     * @Description : This method is used to add mulptiple rows (mapped to the VF page)
     * @ Args       : none
     * @ Return     : void
     */
    public void addDistributionRow() {
        
        //max limit for rows is 100
        if(distributionList.size() == 100) {
            ApexPages.Message errorMaxLinesMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Limit for Distributions reached.');ApexPages.addMessage(errorMaxLinesMsg);
            
        }
        else {
            //calling new wrapper object and adding to list
            distributionList.add(new wrapDistribution(new Contact_Distributions__c(), lastIndex++, false));
        }
    }
    
    /*
     * @Description : This method is used to remove a row (mapped to the VF page)
     * @ Args       : none
     * @ Return     : void
     */
    public void removeDistribution()
    {
        //iterate on wrapper list
        for(Integer i = 0; i < distributionList.size(); i++) {
            //if index matches
            if(distributionList[i].recordIndex == removeIndex) {
                distributionList.remove(i);
            }
        }
    }
    
    /*
     * @Description : This method is used to save data (mapped to the VF page)
     * @ Args       : none
     * @ Return     : pagereference
     */
    public PageReference save()
    {
        try
        {
            pageReference pageRef = null; 
            set<string> checkDistributionSet = new set<string>();
            List<Contact_Distributions__c> conDisToInsert = new List<Contact_Distributions__c>();
            for(Contact_Distributions__c cDis: [select Id, Distribution__c from Contact_Distributions__c where Contact__c = :contactId]) {checkDistributionSet.add(cDis.Distribution__c);}
            
            for(wrapDistribution wDis : distributionList)
            {
                if(wDis.Distribution.Distribution__c <> null )
                {
                    List<wrapDistribution> tempList1 = new List<wrapDistribution>();
                    for(integer i = 0; i<distributionList.size(); i++)
                        {
                            if(wDis.Distribution.Distribution__c == distributionList[i].Distribution.Distribution__c)
                            {
                                tempList1.add(distributionList[i]);
                            }
                        }
                    if(tempList1.Size()>1)
                    {
                        for(integer i =1; i<tempList1.Size(); i++) {tempList1[i].bool = true; break;}
                        
                    }
                    else if(checkDistributionSet.contains(wDis.Distribution.Distribution__c))
                    {wDis.bool = true; break;}                    
                    else
                    {
                            Contact_Distributions__c conDis = new Contact_Distributions__c();
                            conDis = wDis.Distribution;
                            conDis.Contact__c = contDis.Contact__c;
                            conDisToInsert.add(conDis);
                        if(contactId!=null)
                        pageref = new pageReference('/'+contactId);
                        else
                        pageref = new pageReference('/'+contDis.Contact__c);    
                    }
                }
            }
            insert conDisToInsert ;
            return pageref;
        }catch(DMLException e){ ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getDMLMessage(0)); ApexPages.addMessage(errorMsg); return null;            
        }catch(Exception ex){ ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()); ApexPages.addMessage(errorMsg); return null;
            }
    }

}