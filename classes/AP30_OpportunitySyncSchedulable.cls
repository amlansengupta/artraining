/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================History ----------------------- 
                                        VERSION     AUTHOR  DATE        DETAIL                  
                                         1.0 -    Arijit Roy 03/29/2013  Created Schedulable class for Opportunity

=========================================================================================================================================
 */
global class AP30_OpportunitySyncSchedulable implements Schedulable{
    global Set<String> mogdSet = new Set<String>();
    
    global String oppQuery;
    
    global void execute(SchedulableContext sc)
    {
        for(Mercer_Office_Geo_D__c officeGeo : [select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True])
        {
            mogdSet.add(officeGeo.Name);
        }
        if(mogdSet.size()>0){
            this.oppQuery = 'Select Opportunity_Market__c, Opportunity_Region__c, Opportunity_Sub_Market__c, Opportunity_Sub_Region__c, Office_Code__c, Opportunity_Country__c, Opportunity_Office__c  from Opportunity where Office_Code__c IN ' + quoteKeySet(mogdSet) + ' AND StageName NOT IN (\'Closed / Won\',\'Closed / Lost\',\'Closed / Declined to Bid\',\'Closed / Client Cancelled\') AND CALENDAR_YEAR(CloseDate) >= 2015 ';
            Database.executeBatch(new AP31_OpportunitySyncBatchable(oppQuery), Integer.valueOf(system.label.CL51_OfficeBatchSize)); 
        }
    }
    
    //convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
    global String quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }   
}