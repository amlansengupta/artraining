/*Purpose:  This Apex class contains static methods to process Distributions Scope records on their various stages of insertion/updation.
This class also contains validation rule to prompt user to select Market/Region/office based on the scope selected by the user.
This class contains method to populate country, Market, Office, Region, Sub Market, Sub Region based on the Scope selected by the user. 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL 
   1.0 -    Srishty   18/10/2013  Created Apex class to support trigger TRG13_DistributionTrigger 

============================================================================================================================================== 
*/

public class AP27_DistributionClassUtil{

    //set variable to store Country
    public static set <String> NewCountry = new Set <string>();
    //set variable to store Market
    public static set <String> NewMarket = new Set <string>();
    //set variable to store region
    public static set <String> NewRegion = new Set<String>();
    //set variable to store SubRegion
    public static Set<String> NewSubRegion = new Set<String>();
    //set variable to store Office
    public static Set<String> NewOffice = new Set<String>();
    //set variable to store SubMarket
    public static Set<String> NewSubMarket = new Set<String>();

    //Map variable of Scope and List of Distributions
    public static Map <String,List<Distribution__c>>scopeDistributionMap = new Map <String,List<Distribution__c>>();
        /*
     * @Description : This method is called on before insertion of Distribution records
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
    public static void processDistribution (List<distribution__c> triggernew)
    {
        // clear cache
        NewCountry.clear();
        NewMarket.clear();
        NewRegion.clear();
        NewSubRegion.clear();
        NewOffice.clear();
        NewSubMarket.clear();
        try
        {
            for (distribution__c distribution: triggernew)
                {
                    //if Scope is Country and Country field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL86_Scope_Country) && (distribution.Country__c == null || distribution.Country__c == 'All'))
                    {
                        //Show Error
                        distribution.Country__c.addError('Please enter a Country');
                        break;
                    } 
                    
                    //if Scope is Office and Office field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL88_Scope_Office) && (distribution.Office__c == null || distribution.Office__c == 'All'))
                    {
                        //Show Error
                        distribution.Office__c.addError('Please enter Office');
                        break;
                    }
                    //if Scope is Market and Market field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL85_Scope_Market) && (distribution.Market__c == null || distribution.Market__c == 'All')) 
                    
                    {
                        //Show Error
                        distribution.Market__c.addError('Please enter a Market');
                        break;
                    
                    }
                    //if Scope is Region and Region field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL83_Scope_Region) && (distribution.Region__c == null || distribution.Region__c == 'All'))
                    
                    {
                        //Show Error
                        distribution.Region__c.addError('Please enter a Region');
                        break;
                    }
                    //if Scope is Sub-Market and Sub-Market field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL87_Scope_SubMarket) && (distribution.Sub_Market__c == null || distribution.Sub_Market__c == 'All'))
                    {
                        //Show Error
                        distribution.Sub_Market__c.addError('Please enter Market*');
                        break;
                    }
                    //if Scope is Sub-Region and Sub-Region field is Null
                    if(distribution.Scope__c.equalsIgnoreCase(System.Label.CL84_Scope_SubRegion) && (distribution.Sub_Region__c == null || distribution.Sub_Region__c == 'All'))
                    {
                        //Show Error
                        distribution.Sub_Region__c.addError('Please enter Sub-Region');
                        break;
                    }
                    
                    
                    if(distribution.scope__c <> null)
                    {
                        //If Scope is selected as Country
                        if ( distribution.scope__c.equalsIgnoreCase(System.Label.CL86_Scope_Country))
                        {
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                            NewCountry.add(distribution.Country__c);
                        }
                        
                        //If Scope is selected as Office
                        else if ( distribution.Scope__c.equalsIgnoreCase(System.Label.CL88_Scope_Office))
                        {
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                            NewOffice.add(distribution.Office__c);
        
                        }
                        
                        //If Scope is selected as Region
                        else if ( distribution.Scope__c.equalsIgnoreCase(System.Label.CL83_Scope_Region))
                        {
                           scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                           NewRegion.add(distribution.Region__c);
                        }
                        
        
                        
                        //If Scope is selected as Sub-Market
                        else if ( distribution.Scope__c.equalsIgnoreCase(System.Label.CL87_Scope_SubMarket))
                        {
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                            NewSubMarket.add(distribution.Sub_Market__c);
                        }
                        
                        //If Scope is selected as Sub-Region
                        else if ( distribution.Scope__c.equalsIgnoreCase(System.Label.CL84_Scope_SubRegion))
                        {
                           scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                           NewSubRegion.add(distribution.Sub_Region__c);
                        }
                        
                        //If Scope is selected as Global
                        else if ( distribution.scope__c.equalsIgnoreCase(System.Label.CL82_Scope_Global))
                        {       
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                        }
                        
                        //If Scope is selected as Multi-Market
                        else if ( distribution.scope__c.equalsIgnoreCase(System.Label.CL89_Scope_MultiMarket))
                        {       
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                        }
                        
                        //If Scope is selected as Multi-Zone
                        else if ( distribution.scope__c.equalsIgnoreCase(System.Label.CL90_Scope_MultiZone))
                        {       
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                        }
                        
                        //If Scope is selected as Market
                        else if ( distribution.Scope__c.equalsIgnoreCase(System.Label.CL85_Scope_Market))
                        {
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                            NewMarket.add(distribution.Market__c);
                        }
                        
                        //If Scope is selected as Zone
                        else if ( distribution.scope__c.equalsIgnoreCase(System.Label.CL92_Scope_Zone))
                        {       
                            scopeDistributionMap = addToDistributionMap(scopeDistributionMap, distribution);
                        }
                            
                    }
                }   
        List<Mercer_Office_Geo_D__c> MercerGeoofficeForCountry = [select Region__c,Sub_Region__c,Market__c,Country__c from Mercer_Office_Geo_D__c 
                                                                         where Country__c IN: NewCountry limit 1];  
                List<Mercer_Office_Geo_D__c> MercerGeoofficeForOffice = [select Office__c,Region__c,Sub_Region__c,Market__c,Country__c,Sub_Market__c  from Mercer_Office_Geo_D__c
                                                                        where Office__c IN: NewOffice limit 1];
                List<Mercer_Office_Geo_D__c> MercerGeoofficeForMarket = [select Region__c,Sub_Region__c,Market__c from Mercer_Office_Geo_D__c 
                                                                        where Market__c IN: NewMarket limit 1];
                List<Mercer_Office_Geo_D__c> MercerGeoofficeForSubMarket = [select Region__c,Sub_Region__c,Market__c,Country__c,Sub_Market__c from Mercer_Office_Geo_D__c 
                                                                            where Sub_Market__c IN: NewSubMarket limit 1];
                List<Mercer_Office_Geo_D__c> MercerGeoofficeForSubRegion = [select Sub_Region__c,Region__c  from Mercer_Office_Geo_D__c 
                                                                            where Sub_Region__c  IN: NewSubRegion limit 1];
                
                for (String scope : scopeDistributionMap.keySet()) 
                {
                    if (scope== System.Label.CL86_Scope_Country)
                    {
                        
                        //Query GeoTable for records
                        
                            
                        //Set Corresponding values
                        for(Distribution__c Distribution : scopeDistributionMap.get(scope))                 
                        {
                            if (!MercerGeoofficeForCountry.isempty())
                            {               
                    
                                Distribution.Market__c = MercerGeoofficeForCountry[0].Market__c; 
                                Distribution.Region__c = MercerGeoofficeForCountry[0].Region__c;
                                Distribution.Sub_Region__c =MercerGeoofficeForCountry[0].Sub_Region__c;
                                
                            }
                            else
                            {
                                Distribution.Market__c = NULL; 
                                Distribution.Region__c = NULL;
                                Distribution.Sub_Region__c =NULL; 
                                    
                            }
                            Distribution.Sub_Market__c ='All';
                            Distribution.Office__c ='All';
                        }
                    }
                    
                    //if value of scope equals Office
                    else if   (scope==System.Label.CL88_Scope_Office)
                    {
                        //Query GeoTable for records        
                        
                        //If Geo table is empty
                        
                        //Set Corresponding values
                        for(Distribution__c Distribution1 : scopeDistributionMap.get(scope))
                        {
                            if (!MercerGeoofficeForOffice.isempty())
                            {
                                Distribution1.Region__c =MercerGeoofficeForOffice[0].Region__c;  
                                Distribution1.Sub_Region__c =MercerGeoofficeForOffice[0].Sub_Region__c; 
                                Distribution1.Market__c =MercerGeoofficeForOffice[0].Market__c; 
                                Distribution1.Country__c =MercerGeoofficeForOffice[0].Country__c;
                                Distribution1.Sub_Market__c =MercerGeoofficeForOffice[0].Sub_Market__c;
                                 
                            }
                            else 
                            {
                                Distribution1.Region__c =NULL;  
                                Distribution1.Sub_Region__c =NULL; 
                                Distribution1.Market__c =NULL; 
                                Distribution1.Country__c =NULL;
                                Distribution1.Sub_Market__c =NULL;
                            }
                        
                        }
                    }
                    
                    else if (scope== System.Label.CL85_Scope_Market)                
                    {
                                        
                        //Query GeoTable for records
                        
                        //If Geo table is empty                 
                        //Set Corresponding values
                        for(Distribution__c Distribution : scopeDistributionMap.get(scope))
                        { 
                            if (!MercerGeoofficeForMarket.isempty())
                            {   
                                Distribution.Region__c = MercerGeoofficeForMarket[0].Region__c;
                                Distribution.Sub_Region__c =MercerGeoofficeForMarket[0].Sub_Region__c;                        
                        
                            }   
                            else
                            {
                                Distribution.Region__c = NULL;
                                Distribution.Sub_Region__c =NULL; 
                            }
                            
                            Distribution.Country__c ='All';
                            Distribution.Sub_Market__c ='All';
                            Distribution.Office__c ='All';
                        }
                    }
                    
                    //if value of scope equals Region
                    else if   (scope==System.Label.CL83_Scope_Region)
                    {
                        //Set Country,Sub-Market,Office,Sub-Region,Market to All
                        for (Distribution__c Distribution: scopeDistributionMap.get(scope) )
                        {
                                    Distribution.Sub_Region__c ='All'; 
                                    Distribution.Market__c = 'All';
                                    Distribution.Country__c = 'All';
                                    Distribution.Sub_Market__c = 'All';
                                    Distribution.Office__c= 'All'; 
                        }
                    
                    }
                    else if   (scope== System.Label.CL87_Scope_SubMarket)
                    {
                        
                        //Query GeoTable for records    
                        
                        //If Geo table is empty
                        
                        //Set Corresponding values
                        for(Distribution__c Distribution : scopeDistributionMap.get(scope))
                        {
                            if (!MercerGeoofficeForSubMarket.isempty())
                            {
                                    
                                Distribution.Region__c =MercerGeoofficeForSubMarket[0].Region__c;  
                                Distribution.Sub_Region__c =MercerGeoofficeForSubMarket[0].Sub_Region__c; 
                                Distribution.Market__c =MercerGeoofficeForSubMarket[0].Market__c; 
                                Distribution.Country__c =MercerGeoofficeForSubMarket[0].Country__c;                     
                                
                            }
                            else 
                            {
                                Distribution.Region__c =NULL;  
                                Distribution.Sub_Region__c =NULL; 
                                Distribution.Market__c =NULL; 
                                Distribution.Country__c =NULL;
                            }
                            Distribution.Office__c ='All';
                        }
                    }
                    
                    //if value of scope equals Sub-Region
                    else if   (scope== System.Label.CL84_Scope_SubRegion)
                    {
                        
                        //Query GeoTable for records    
                        
                        for(Distribution__c Distribution : scopeDistributionMap.get(scope))
                        {
                            if (!MercerGeoofficeForSubRegion.isempty())
                            {
                                Distribution.Region__c =MercerGeoofficeForSubRegion[0].Region__c;
                            }
                            else
                            {
                                Distribution.Region__c =NULL;
                            }
                            
                            Distribution.Market__c = 'All';
                            Distribution.Country__c = 'All';
                            Distribution.Sub_Market__c = 'All';
                            Distribution.Office__c = 'All';
                    
                        }
                    }
                    else if   (scope== System.Label.CL82_Scope_Global || scope== System.Label.CL89_Scope_MultiMarket || scope== System.Label.CL90_Scope_MultiZone || scope==System.label.CL92_Scope_Zone)
                    {
                        //Set Region,Market,Country,Office,Sub-Market,Sub-Region to All
                        for(Distribution__c Distribution1 : scopeDistributionMap.get(scope))
                        {
                            Distribution1.Region__c ='All';  
                            Distribution1.Market__c ='All'; 
                            Distribution1.Country__c ='All';
                            Distribution1.Office__c = 'All';
                            Distribution1.Sub_Market__c = 'All';
                            Distribution1.Sub_Region__c = 'All';
                        }
                    
                
                    }
                    
                }
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());    
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());   
        }
    }
    
    public static void distributionBeforeInsert(List<Distribution__c> distForInsert)
    {
        try
        {
            processDistribution(distForInsert);
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());    
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());   
        }     
    } 
    
    public static void distributionBeforeUpdate(Map<Id, Distribution__c> newDisMap, Map<Id, Distribution__c> oldDisMap)
    {
        List<Distribution__c> distributionForProcess = new List<Distribution__c>();     
        try
        {
            for(Distribution__c distribution : newDisMap.values())
            {
                //Compare the previous values of the Fileds to new values 
                if(distribution.scope__c <> oldDisMap.get(distribution.Id).Scope__c
                ||distribution.Office__c <> oldDisMap.get(distribution.Id).Office__c
                ||distribution.Region__c <> oldDisMap.get(distribution.Id).Region__c
                ||distribution.Sub_Region__c <> oldDisMap.get(distribution.Id).Sub_Region__c
                ||distribution.Country__c <> oldDisMap.get(distribution.Id).Country__c
                ||distribution.Market__c <> oldDisMap.get(distribution.Id).Market__c
                ||distribution.Sub_Market__c <> oldDisMap.get(distribution.Id).Sub_Market__c)
                {
                    distributionForProcess.add(distribution);
                    
                }   
            }
            
            if(!distributionForProcess.isEmpty())
                processDistribution(distributionForProcess);
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured with reason :'+tEx.getMessage());    
        }catch(Exception ex)
        {
            System.debug('Exception occured with reason :'+Ex.getMessage());   
        }
    }
    
    public static Map<String, List<Distribution__c>> addToDistributionMap(Map<String, List<Distribution__c>> sDistributionMap, Distribution__c distribution)
    {
        //if Map key contains that value of scope
        if (sDistributionMap.containskey(distribution.scope__c))
        {
            //Add to Distribution list that particular distribution
            List<Distribution__c> DistributionList =new List <Distribution__c>(); 
            DistributionList.add(distribution);
            sDistributionMap.put(distribution.scope__c, DistributionList);
        }
        else 
        {
            //Add to Scope and Distribution list
            List<Distribution__c> DistributionList =new List <Distribution__c>(); 
            DistributionList.add(distribution);
            sDistributionMap.put(distribution.Scope__c, DistributionList);
            //Add to NewOffice Set
            
        }
            return sDistributionMap;            
    }  
}