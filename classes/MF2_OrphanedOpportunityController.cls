/***************************************************************************************************************
Request Id : 15559
Purpose : This apex class is created to fetch total no of records and custom link 
Created by : Archisman Ghosh
Created Date : 28/03/2019
Project : MF2
****************************************************************************************************************/
public class MF2_OrphanedOpportunityController {
    
    /*
*  @Method Name: fetchCustomLinks
*  @Description: Method gets UI label and Custom Link from Custom Setting 
*/
    @AuraEnabled
    public static List<DailyCleanse__mdt> fetchDailyAlertDetails(){
        List<DailyCleanse__mdt> dailyCleansConfigs = null;
        try{	
            String oppInactiveDuration=System.label.OpportunityInactivityDuration;
            dailyCleansConfigs = [Select DeveloperName,UI_Label__c,IsActive__c,Link__c,Display_In_New_Window__c,
                                  Threshold__c,Query__c from DailyCleanse__mdt where IsActive__c = true];
            for(DailyCleanse__mdt config : dailyCleansConfigs){
                config.Link__c = replacePlaceholders(config.Link__c, config.Threshold__c);
                config.UI_Label__c = replacePlaceholders(config.UI_Label__c, config.Threshold__c);
                config.Query__c = replacePlaceholders(config.Query__c, config.Threshold__c);
            }
        }catch(Exception ex){
            ExceptionLogger.logException(ex, 'MF2_OrphanedOpportunityController', 'fetchDailyAlertDetails');
            throw new AuraHandledException(System.Label.OrphanedOppDaily);
        }
        return dailyCleansConfigs;
    }
    
    @AuraEnabled
    public static Integer fetchRecordCount(String developerName){
        List<DailyCleanse__mdt> dailyCleansConfigs = null;
        Integer recordCount = 0;
        try{
            dailyCleansConfigs = [Select Threshold__c,Query__c from DailyCleanse__mdt where DeveloperName =:developerName];
            if(dailyCleansConfigs != null && !dailyCleansConfigs.isEmpty()){
                String queryString = replacePlaceholders(dailyCleansConfigs.get(0).Query__c, dailyCleansConfigs.get(0).Threshold__c);
                system.debug('query %%%%%%'+queryString);
                recordCount = Database.countQuery(queryString+' AND OwnerId=\''+UserInfo.getUserId()+'\'');
            }
        }catch(Exception ex){
            system.debug(ex.getStackTraceString());
            ExceptionLogger.logException(ex, 'MF2_OrphanedOpportunityController', 'fetchRecordCount');
            throw new AuraHandledException(System.Label.OrphanedOppException);
        }
        return recordCount;
    }
    
    private static String replacePlaceholders(String input, String replacement){
        if(input == null){
            return input;
        }
        if(replacement == null || String.isEmpty(replacement)){
            return input;
        }
        if(input.indexOf('{1}') > -1){
            input = input.replace('{1}', replacement);
        }
        return input;
    }
    
}