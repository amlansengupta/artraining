/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AP126_ContactTeamBatchable_Test { 
    private static Mercer_TestData mtdata =new  Mercer_TestData();
    
    static testMethod void myUnitTest() {
        ContactShare testContactShare = new ContactShare();
         Colleague__c col = mtdata.buildColleague();
           
         User user1 = new User();
         User user2 = new User();
         String adminUserprofile = Mercer_TestData.getsystemAdminUserProfile(); 
         //String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();     
         user1 = Mercer_TestData.createUser1(adminUserprofile , 'usert2', 'usrLstName2', 2);             
         //user2 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert22', 'usrLstName22', 1);
         
         Account acc = mtdata.buildAccount();
         Contact con = mtdata.buildContact();
         system.runAs(user1){
         Contact_Team_Member__c conteam =  mtdata.buildContactTeamMember();
         
         testContactShare.ContactId = con.Id;                    
         testContactShare.UserOrGroupId = conteam.ContactTeamMember__c;                   
         testContactShare.ContactAccessLevel = 'Edit';
         insert  testContactShare;
         
        // testContactShare.ContactAccessLevel = 'Read';
         //update testContactShare;
        delete  testContactShare;
        }
       
         Test.startTest(); 
          String testquery1 = 'Select id, name, (select id, ContactTeamMember__c, Contact__c from Contact_Team_Members__r) from contact where id = \''+ con.Id+'\'';
         database.executeBatch(new AP126_ContactTeamBatchable(testquery1), 100); 
         
         Test.stopTest();
         
    }
}