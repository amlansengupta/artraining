/*Purpose: Test Class for providing code coverage to MS12_AccountActivitiesBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
@istest
private class Test_MS12_AccountActivitiesBatchable  {
	/*
     * @Description : Test method to provide data coverage to MS12_AccountActivitiesBatchable batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
         User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
       
        Account Acc = new Account();
        Acc.Name = 'TestAccountName123';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.MercerForce_Account_Status__c = 'Active Contact';
        insert Acc;
         
       
        Contact Con= new Contact();
        Con.LastName ='TestContactLastName';
        Con.AccountId = Acc.id;
        Con.FirstName ='TestContactFirstName';
        Con.email = 'xyzq1@abc14.com';
        insert Con;
        
        Event event = new Event();
        event.subject= 'Email';
        event.Whatid = acc.id;
        event.StartDateTime = system.now()-1;
        event.EndDateTime = system.now();
        event.CurrencyIsoCode ='AED';
        insert event;
        //query folderId
        Folder foldrec = new Folder();
        foldrec =[select id,name from Folder  where IsReadonly = False and Type = 'Document' limit 1];
        
        //insert docuemntrecord
        Document doc = new Document();
        doc.name ='MercerChatter Logo';
        doc.developerName ='Test_Doc';
        doc.FolderId = foldrec.Id;
        Insert doc;
        
        Task task = new Task();
        task.whoid = Con.Id;
        task.subject= 'Email';
        task.Status = 'Completed';
        task.Priority = 'High';
        task.WhatId = acc.id;
        insert task;
        
        system.runAs(User1){
        database.executeBatch(new MS12_AccountActivitiesBatchable(), 500);
        }
        }
        
         
    }