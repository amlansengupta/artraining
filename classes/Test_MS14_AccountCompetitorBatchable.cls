/*Purpose: Test Class for providing code coverage to MS14_AccountCompetitorBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
   2.0 -    Madhavi  12/3/2014   created test method checkAccountstatus
============================================================================================================================================== 
*/
@istest
private class Test_MS14_AccountCompetitorBatchable  {
    /*
     * @Description : Test method to provide data coverage to MS14_AccountCompetitorBatchable batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '987654321';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
                
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        Acc.MercerForce_Account_Status__c = 'Competitor';
        Acc.Competitor_Flag__c = true;
        insert Acc;
        
       
        
        
        
        Competitor__c Competitor = new Competitor__c();
        //Competitor.Name = 'Test Competitor';
        Competitor.CurrencyIsoCode = 'ALL';
        
        system.runAs(User1){
        database.executeBatch(new MS14_AccountCompetitorBatchable(), 500);
        }
        
       }
        /*
     * @Description : Test method to provide data coverage when MercerForce_Account_Status__c  not equal to competitor.
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void checkAccountStatus()
     {
     
        Mercer_TestData mtdata = new Mercer_TestData();
        mtdata.buildColleague();
        Account acc = mtdata.buildAccount();
        acc.MercerForce_Account_Status__c ='Contacts Exist';
        acc.Competitor_Flag__c = true;
        update acc;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);

        
        system.runAs(User1){
        database.executeBatch(new MS14_AccountCompetitorBatchable(), 500);
        }
        
       }
    }