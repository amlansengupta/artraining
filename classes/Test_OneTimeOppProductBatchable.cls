/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(SeeAllData = true)
public class Test_OneTimeOppProductBatchable {
    static testMethod void test_BATCH_OneTimeOppProduct() {
        User user1 = new User();
        Mercer_TestData mdata = new Mercer_TestData();
        
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12567899';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;  
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc2468@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = acc.Id;
        insert testContact;
        
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.Id;
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c = 'Hong Kong - Harbour';
        testOppty.Amount = 100;
        testOppty.Buyer__c= testContact.id;  
        testOppty.CurrencyIsoCode='USD';
        testOppty.Concatenated_Products__c='529 Plan;DB Risk';
        Test.startTest();
        insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'USD' limit 1];
        
        Test.stopTest();      
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+30;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+20; 
        opptylineItem.UnitPrice = 1.00;
        insert opptylineItem;        
        
        
        
        database.executeBatch(new OneTimeOppProductBatchable(), 200);  
    
    }
}