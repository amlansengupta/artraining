/*Purpose: Test class to provide test coverage for Mercer_New_Modelling_class class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan   05/02/2014  Created test class 
 
============================================================================================================================================== 
*/
@isTest(SeeAllData = True)
public class Mercer_New_Modelling_class_test{
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();   
     
    /*
     * @Description : Test method for searching, selecting a product and creating new scope modeling record
     * @ Args       : Null
     * @ Return     : void
     */
    private static testMethod Void Mercer_New_Modelling_class_testMethod(){
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            String LOB = 'Retirement';
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;     
            Scope_Modeling__c testMod = mtscopedata.buildScopeModelProject(prodId);       
            ApexPages.StandardController con = new ApexPages.StandardController(testMod);
            Mercer_New_Modelling_class mod = new Mercer_New_Modelling_class(con);
            
            mod.getlstwrapper();
            mod.searchProducts();
            mod.searchValue = 'Asset';
            mod.getlstwrapper();
            mod.searchProducts();
            mod.SaveProduct();
            mod.Previous();
            mod.next();
            mod.beginning();
            mod.end();
            mod.getDisablePrevious();
            mod.getDisableNext();
            mod.getTotal_size();
            mod.getPageNumber();
            mod.getTotalPages();
        }

    }
        
}