/*Purpose:  This Apex class is created as a Part of ScopeIt Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Display warning message to user to inform that deleting opportunity product will also delete scope it tasks and employees
.Display errors (if any) while deleting opportunity product.


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Jagan   03/31/2014  This Apex class works as Controller for "Mercer_Product_Del_Override" VF page which is invoked by deleting a product.
============================================================================================================================================== 
*/
public class AP102_OppProd_Del_Override {
    public boolean showContinue{get;set;}
    OpportunityLineItem oli {get;set;}
    Id oppid;
    public AP102_OppProd_Del_Override(ApexPages.StandardController controller) {
        showContinue = false;
        oli = (opportunitylineitem)controller.getRecord();
        System.debug('oli :'+oli);
        if(oli<>null){
            System.debug('Hello :'); 
            oppid = [select id,opportunityid,project_Linked__C,Opportunity.stageName from opportunitylineitem where id=:oli.id].opportunityid;
        }
       // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Deleting the product will also delete any related scoping projects, tasks and employees. Click Cancel if you wish to save the project as a template first.  Do you want to continue?'));        
       //#Req-18760
       List<OpportunityLineItem> oliList =[select id,OpportunityId,Project_Linked__c ,Opportunity.StageName from OpportunityLineItem where OpportunityId=:oppid];
       boolean showDelIndentify = false ;
        if(!oliList.isEmpty()){
            for(OpportunityLineItem ol: oliList){
                if(ol.Opportunity.stageName == 'Above the Funnel' || ol.Opportunity.stageName =='Identify'){
                    showDelIndentify = true;
                    showContinue = true;
                    break;
                }
            } 
        }
        if(showDelIndentify){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Deleting the product will also delete any related scoping projects, tasks and employees. Click Cancel if you wish to save the project as a template first.  Do you want to continue?'));        
     	 OpportunityLineItem oli =[select id, Project_Linked__c from OpportunityLineItem where id=:oli.id];
      	if(oli.Project_Linked__c)
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,' SALE WILL BE UNLINKED FROM REVENUE. This product is related to a WebCAS project. Please update/remove the Opportunity Product ID linked to the project via your WebCAS support.'));
        
        }
        else if(oliList.size()==1 && oliList.get(0).id==oli.id && !oliList.get(0).Project_Linked__c){
           
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Opportunities past Identify Stage must have at least one associated product. If you need to delete this Product, please add the new one first, or if you need to delete this opportunity, please contact your Local CRM Team'));
       }
       else if(oliList.size()==1 && oliList.get(0).id==oli.id && oliList.get(0).Project_Linked__c){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Opportunities past Identify Stage must have at least one associated product. If you need to delete this Product, please add the new one first, or if you need to delete this opportunity, please contact your Local CRM Team - Note once deleted the SALE WILL BE UNLINKED FROM REVENUE. This product is related to a WebCAS project. Please update/remove the Opportunity Product ID linked to the project via your WebCAS support.'));
       }
       else if(oliList.size()>1){
       showContinue = true;
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Deleting the product will also delete any related scoping projects, tasks and employees. Click Cancel if you wish to save the project as a template first.  Do you want to continue?'));        
      OpportunityLineItem oli =[select id, Project_Linked__c from OpportunityLineItem where id=:oli.id];
      if(oli.Project_Linked__c)
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,' SALE WILL BE UNLINKED FROM REVENUE. This product is related to a WebCAS project. Please update/remove the Opportunity Product ID linked to the project via your WebCAS support.'));
      
       }
       
       
        
             
    }
    
    public pageReference deleteProject(){
        try{
            Scopeit_Project__c[] scp = [select id from scopeit_project__c where OpportunityProductLineItem_Id__c =:oli.id];
            if(scp.size()>0){
                database.delete(scp);database.delete(oli);
                
                return new PageReference('/'+oppid);
            }
            else{
                database.delete(oli);
                return new PageReference('/'+oppid);
            }
        }
        catch(Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,''+e.getMessage()));
            return null;
        }
    }
    public pageReference cancel(){
     return new PageReference('/'+oppid);

    }
}