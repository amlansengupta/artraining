public class MF2_SplitDelAPex {
    public  string opId{get;set;}
    public  Id recId{get;set;}
    public  boolean showError {get;set;}
    
    public MF2_SplitDelAPex(ApexPages.StandardController controller) {
        recId = controller.getId();
    }
    
    public  pagereference delSplit(){
        system.debug('*2');
        if(recId != null){
            OpportunityTeamMember otm=[select id, UserId, OpportunityId, Opportunity.OwnerId from OpportunityteamMember where Id=:recId];
            if(otm != null){
                opId = otm.OpportunityId;
                if(otm.UserId != otm.Opportunity.OwnerId){
                    List<OpportunitySplit> delSplit=[SELECT OpportunityId, SplitAmount, SplitPercentage, Opportunity.Name, Opportunity.Amount FROM OpportunitySplit WHERE SplitOwnerId =:otm.UserId and OpportunityId =:otm.OpportunityId];
                    if(!delSplit.isEmpty()){
                        try{
                            system.debug('*1');
                            delete delSplit;
                            system.debug('*11');
                            system.debug('*111');
                        }
                        Catch(Exception ex){  System.debug(ex.getMessage()); }
                    }
                    delete otm;
                    showError = false;
                } else {
                    showError =true;
                    system.debug('&*'+showError);
                    return null;
                }
            }
        }
        PageReference reference=new PageReference('/'+opId);
        reference.setRedirect(true);
        return reference;
    }
    
}