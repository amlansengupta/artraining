/*
Purpose: This apex class implements mass edit of Contact Distributions
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR     DATE        DETAIL    
 1.0        Sarbpreet   08/22/2013  Created Controller class for Mass Edit of Contact Distribution.
=============================================================================================================================================== 
*/



public with sharing class AP64_ContactDistributionListController {

    public Contact cont{get;set;}
    //List variable to store Contact Distribution
    public List<Contact_Distributions__c> CDList{get; set;}
    // List variable to store updated contact Distribution
    public List<Contact_Distributions__c> ContactDistributionList {
    get {
            if (ContactDistributionList == null) ContactDistributionList = new List<Contact_Distributions__c>();
            return ContactDistributionList;
        }
        set;} 
        
        
    public AP64_ContactDistributionListController(ApexPages.StandardController controller) {
        
        this.cont = (Contact)controller.getRecord();
        // Fetch Contact Distributions
        CDList = [select Id, Distribution__c,Distribution__r.Name,Distribution__r.Region__c, Distribution__r.LOB__c, Distribution_Preference__c from Contact_Distributions__c where Contact__c =:cont.Id];
        for(Contact_Distributions__c ContactDist : CDList)
        {
           // Add Contact Distributions to a new list
           ContactDistributionList.add(ContactDist);
        }  
    }
    
    // Method to save updated Contact Distribution records
    public PageReference save(){
        PageReference pageRef;
        try{
             update ContactDistributionList;
             pageRef = new PageReference('/' + cont.Id);
        }catch(System.DmlException e){
          System.debug('update Contact Distibution error==  '+e.getDmlMessage(0));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
          pageRef=null;
        }
        
         return pageRef;
    }
   
    //Method to cancel the current page and return to Contact Page
    public pageReference cancel()
    {
        pageReference pageRef = new pageReference('/'+cont.Id);
        return pageRef;
    }

 }