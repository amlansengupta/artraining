/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class AP96_scopeit_Add_Task_Employees_Test {

    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static Mercer_ScopeItTestData mtscopedata = new Mercer_ScopeItTestData();
    
  
  
     /*
     * @Description : Test class for adding task and employees to scope modeling   
    */
    static testMethod void AP96_myUnitTest2() {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
           
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();

           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           
           Test.startTest();
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
          
          
            
            
     
                mtscopedata.insertCurrentExchangeRate();
                Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
                Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
                Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
             
                String objname = testScopeModelProj.name;
                PageReference pageRef = Page.Mercer_Add_Task_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('projid', testScopeModelProj.id);
                system.currentPageReference().getParameters().put('object', objname);
                

                ApexPages.StandardController stdController2 = new ApexPages.StandardController(testScopeModelProj); 
                AP96_scopeit_Add_Task_Employees controller2 = new AP96_scopeit_Add_Task_Employees();
                
                            
                 String rid=string.valueof(testScopeModelEmployee.id);  
                 String n='Name';      
                 String level ='A';     
                 String busi='Benefits Admin';   
                 String mar='Australia'; 
                 String sub ='BPO Services' ;      
                 Decimal rt=10.00;        
                 AP96_scopeit_Add_Task_Employees.Selempwrapper semw = new AP96_scopeit_Add_Task_Employees.Selempwrapper(rid,n,level,busi,mar,sub,rt);
              
                 semw.isSelected = true;  
                 controller2.SelempWrpList.add(semw);
              
                PageReference pageReference3 = controller2.saveEmployees();          
                PageReference pageReference4 =  controller2.cancel(); 
                controller2.updateselectedemployees(); 
                controller2.clearFields();                     
                controller2.RemoveEmployees(); 
                  
                controller2.namesearch = 'Gen';
                controller2.levelsearch = 'A';
                controller2.emp.type__c = 'Generic Employee';
                controller2.emp.Market_Country__c = 'Chile';
                controller2.emp.Business__c = 'Property Administration';
                controller2.emp.Sub_Business__c ='BPO Services';
                controller2.getEmployees();
                  

                 controller2.getEmployees();
                controller2.getSelectedEmployees();
                controller2.RemoveEmployees(); 
                controller2.RemoveEmployees();
                  
                PageReference pageReference5 = controller2.Beginning();
                PageReference pageReference6 =  controller2.Previous();
                PageReference pageReference7 =  controller2.End();
                PageReference pageReference8 =  controller2.Next(); 
                controller2.getDisablePrevious();
                controller2.getDisableNext();
                controller2.getTotal_size();
                controller2.getPageNumber();
                controller2.getTotalPages();
                  
            Test.stopTest();
        }
    }
    
    /*
     * @Description : Test class for negative testing if scopeit task is left blank 
    */
    static testMethod void AP96_myUnitTestNegative() {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
            
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
         
           
           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           
           Test.startTest();
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
    
             
             mtscopedata.insertCurrentExchangeRate();
                ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
                ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
                ScopeIt_Employee__c testScopeitEmployee = mtscopedata.buildScopeitEmployee(employeeBillrate);
                String objname = testScopeitProj.name;
            
                PageReference pageRef = Page.Mercer_Add_Task_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('projid', testScopeitProj.id);
                
               
                ApexPages.StandardController stdController2 = new ApexPages.StandardController(testScopeitProj); 
                AP96_scopeit_Add_Task_Employees controller2 = new AP96_scopeit_Add_Task_Employees();
                controller2.objScpTsk.Task_name__c = null;
                PageReference pageReference3 = controller2.saveEmployees();
                
            Test.stopTest(); 
        }
    }
    
    
    
     /*
     * @Description : Test class for negative testing if scopeit Model task is left blank 
    */
    static testMethod void AP96_myUnitTestNegative1() {
            User user1 = new User();
            mtscopedata.insertCurrentExchangeRate();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            String LOB = 'Retirement';
           
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate();
         

           Opportunity testOppty = mtdata.buildOpportunity();
           ID  opptyId  = testOppty.id;
           Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
           PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
           ID  pbEntryId = pbEntry.Id;
           
           Test.startTest();
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
          
          
           
            
     
                
                Scope_Modeling__c testScopeModelProj = mtscopedata.buildScopeModelProject(prodId);
                Scope_Modeling_Task__c  testScopeModelTask =  mtscopedata.buildScopeModelTask();               
                Scope_Modeling_Employee__c testScopeModelEmployee =  mtscopedata.buildScopeModelEmployee(employeeBillrate);
             
                String objname = testScopeModelProj.name;
                PageReference pageRef = Page.Mercer_Add_Task_employees;  
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('projid', testScopeModelProj.id);
                system.currentPageReference().getParameters().put('object', objname);
                
            
    
                ApexPages.StandardController stdController2 = new ApexPages.StandardController(testScopeModelProj); 
                AP96_scopeit_Add_Task_Employees controller2 = new AP96_scopeit_Add_Task_Employees();
                  controller2.objModTsk.Task_name__c = null;
                PageReference pageReference3 = controller2.saveEmployees(); 
                
            Test.stopTest(); 
        }
    }

}