@isTest(seeAllData = true)

public class Opportunity_Summary_Schedulable_Test{

    public static testMethod void testExecute()
    {
        Test.StartTest();
    
        Opportunity_Summary_Insert_Schedulable schedulable = new Opportunity_Summary_Insert_Schedulable();     
        
        String sch = '0 0 23 * * ?'; 
        
        system.schedule('Test Opportunity Summary Insert Batch', sch, schedulable ); 
        
        Test.stopTest();
    
    }
}