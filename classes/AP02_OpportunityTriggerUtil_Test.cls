@isTest(seeAllData = false)
private class AP02_OpportunityTriggerUtil_Test {
 
 static testMethod void testvalidateSalesCreditAllocation() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
     opprec.Opportunity_Region__c ='International';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;         
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
        Map<id,Opportunity> newOppMap = new Map<id,Opportunity>();
        for(Opportunity opp:lstOpp){
            newOppMap.put(opp.Id,opp);
        }
        
        Mercer_Office_Geo_D__c objMOGD = new Mercer_Office_Geo_D__c();
        objMOGD.Name = '1011';
        objMOGD.Country__c = 'United States';
        insert objMOGD;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague1';
        testColleague1.Last_Name__c = 'TestLastName1';
        testColleague1.EMPLID__c = '122255890';
        //testColleague1.LOB__c = 'Health & Benefits';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.Email_Address__c = 'abc@abc.com';
        testColleague1.LOB__c = 'Health';
        testColleague1.Location__c = objMOGD.Id;
        testColleague1.SP_Flag__c ='Y';
        insert testColleague1;
     
      ApexConstants__c deactivationSetting = new ApexConstants__c();
     deactivationSetting.name='LiteProfiles';
     deactivationSetting.StrValue__c='00eE0000000iZ5b;00eE0000000j0Au;00eE0000000j5wx';
     insert deactivationSetting; 
     User  u =[select id,EmployeeNumber from User where id=:UserInfo.getUserId()];
     system.runAs(u){
     u.EmployeeNumber ='122255890';
     update u;
     }
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = opprec.Id;
        testSales.EmpName__c = testColleague1.Id;
        testSales.Percent_Allocation__c = 10;
        insert testSales;
   
        
        Test.startTest();
        AP02_OpportunityTriggerUtil.validateSalesCreditAllocation(newOppMap);
     	AP02_OpportunityTriggerUtil.OwnerSalesCreditAllocation(newOppMap,newOppMap);
        //OppHandlerClass objOpp = new OppHandlerClass();
        //objOpp.BeforeInsert(lstOpp);
        //string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
     User testUser1=new user();
     system.runAs(u){
         Id profId=[select id from Profile where name='System Administrator'].id;
        testUser1.alias ='alias' ;
        testUser1.email = 'test@xyz.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'lastName';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId =profId ;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'test' + '.' + 'lastName' +'@xyz.com';
        testUser1.EmployeeNumber = '1234567890';
        Database.insert(testUser1);
     }
     opprec.OwnerId = testUser1.Id;
     update opprec;
     AP02_OpportunityTriggerUtil.OwnerSalesCreditAllocation(newOppMap,newOppMap);
     opprec.Opportunity_region__c = 'North America';
     update opprec;
     AP02_OpportunityTriggerUtil.OwnerSalesCreditAllocation(newOppMap,newOppMap);
 }
 static testMethod void test1() {
      Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
       Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        
        testOppty.Buyer__c=testContact.Id;
        testOppty.AccountId=testAccount.Id;
     
        insert testOppty;
        
        ID  opptyId  = testOppty.id;
        
         test.startTest();
       
        //Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
          Id pricebookId = Test.getStandardPricebookId();   
                
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        insert pro;
        
      
       /* PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice; */
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = pro.Id,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice; 
        
       // PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :customPB.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1]; 
        
        
        OpportunityLineItem testOli= new OpportunityLineItem();
        testOli.OpportunityId=testOppty.id;
        testOli.PricebookEntryId = customPrice.Id;
        testOli.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+1);
        testOli.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
        testOli.UnitPrice = 1.00;
        testOli.Sales_Price_USD_calc__c = 1.00;
        testOli.CurrentYearRevenue_edit__c = 0.00;
        testOli.Year2Revenue_edit__c = 0.00;
        testOli.Year3Revenue_edit__c = 1.00;
        testOli.Is_Project_Required__c=true;
        testOli.Project_Linked__c=false;
        insert testOli;
        
        system.debug(testOppty.Step__c+'zyx');
        testOppty.Close_Stage_Reason__c='Price';
        testOppty.StageName= 'Pending Project';
     	testOppty.Project_Exception__c='Expansion of prior win';
        testOppty.Total_Scopable_Products_Revenue_USD__c=28000;
     	update testOppty;
     Id rt = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Product Expansion').getRecordTypeId();
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'test oppty - Product Expansion';
        testOppty1.Type = 'Rebid';
        testOppty1.StageName = 'Identify';
        testOppty1.IsRevenueDateAdjusted__c=true;
        testOppty1.CloseDate = date.newInstance(2019, 3, 1);
        testOppty1.CurrencyIsoCode = 'ALL';        
        testOppty1.Product_LOBs__c = 'Career';
        testOppty1.Opportunity_Country__c = 'INDIA';
        testOppty1.Opportunity_Office__c='Urbandale - Meredith';
        testOppty1.Opportunity_Region__c='International';
        testOppty1.RecordTypeId=rt;
        testOppty1.Buyer__c=testContact.Id;
        testOppty1.AccountId=testAccount.Id;
     
        insert testOppty1;
     
     
      OpportunityLineItem testOli1= new OpportunityLineItem();
        testOli1.OpportunityId=testOppty1.id;
        testOli1.PricebookEntryId = customPrice.Id;
        testOli1.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+3);
        testOli1.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()-3);
        testOli1.UnitPrice = 1.00;
        testOli1.Sales_Price_USD_calc__c = 1.00;
        testOli1.CurrentYearRevenue_edit__c = 0.00;
        testOli1.Year2Revenue_edit__c = 0.00;
        testOli1.Year3Revenue_edit__c = 1.00;
        testOli1.Is_Project_Required__c=true;
        testOli1.Project_Linked__c=false;
        insert testOli1;     
     update testOppty1;
     List<Opportunity> lOP = new List<Opportunity>();
     lOP.add(testOppty1);
     AP02_OpportunityTriggerUtil.processOpportunityAfterInsert(lop);
     
       testOppty1.StageName='Pending project';
     testOppty1.Project_Exception__c='Expansion of prior win';
     testOppty1.Parent_Opportunity_Name__c=testOppty.Id;
     testOppty1.Amount = 1;
     testOppty1.Total_Opportunity_Revenue_USD__c = 60;
     testOppty1.CloseDate =Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
     update testOppty1;
     Map<id,Opportunity> mOPP1 = new Map<id,Opportunity>();
     
     mOPP1.put(testOppty1.id,testOppty1);
     AP02_OpportunityTriggerUtil.totalRevenueChildOpp(mOPP1,null);
      AP02_OpportunityTriggerUtil.totalRevenueChildOpp(mOPP1,mOPP1);
      AP02_OpportunityTriggerUtil.totalRevenueChildOpp(null,mOPP1);
     AP02_OpportunityTriggerUtil.processOpportunityAfterUpdate(mOPP1,mOPP1);
        
    
 }
    static testMethod void test2() {
      Account testAccount = new Account();        
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='123456';
        testAccount.Type=' Marketing';
        insert testAccount;
        
       Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'test oppty';
        testOppty.Type = 'Rebid';
        testOppty.StageName = 'Identify';
        testOppty.IsRevenueDateAdjusted__c=true;
        testOppty.CloseDate = date.newInstance(2019, 3, 1);
        testOppty.CurrencyIsoCode = 'ALL';        
        testOppty.Product_LOBs__c = 'Career';
        testOppty.Opportunity_Country__c = 'INDIA';
        testOppty.Opportunity_Office__c='Urbandale - Meredith';
        testOppty.Opportunity_Region__c='International';
        
        testOppty.Buyer__c=testContact.Id;
        testOppty.AccountId=testAccount.Id;
     
        insert testOppty;
        
        ID  opptyId  = testOppty.id;
        
         test.startTest();
       
        //Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
          Id pricebookId = Test.getStandardPricebookId();   
                
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        Product2 pro = new Product2();
        pro.Name = 'TestProd';
        pro.Family = 'RRRF';
        pro.IsActive = True;
        insert pro;
        
      
       /* PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pro.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice; */
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = pro.Id,
            UnitPrice = 123000, IsActive = true , CurrencyIsoCode = 'ALL');
        insert customPrice; 
        
       // PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :customPB.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1]; 
        
        
        OpportunityLineItem testOli= new OpportunityLineItem();
        testOli.OpportunityId=testOppty.id;
        testOli.PricebookEntryId = customPrice.Id;
        testOli.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+1);
        testOli.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day());
        testOli.UnitPrice = 1.00;
        testOli.Sales_Price_USD_calc__c = 1.00;
        testOli.CurrentYearRevenue_edit__c = 0.00;
        testOli.Year2Revenue_edit__c = 0.00;
        testOli.Year3Revenue_edit__c = 1.00;
        testOli.Is_Project_Required__c=true;
        testOli.Project_Linked__c=false;
        insert testOli;
        
        system.debug(testOppty.Step__c+'zyx');
        testOppty.Close_Stage_Reason__c='Price';
        testOppty.StageName= 'Pending Project';
     	testOppty.Project_Exception__c='Expansion of prior win';
        testOppty.Total_Scopable_Products_Revenue_USD__c=28000;
     	update testOppty;
     Id rt = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Product Expansion').getRecordTypeId();
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'test oppty - Product Expansion';
        testOppty1.Type = 'Rebid';
        testOppty1.StageName = 'Above the Funnel';
        testOppty1.IsRevenueDateAdjusted__c=true;
        testOppty1.CloseDate = date.newInstance(2019, 3, 1);
        testOppty1.CurrencyIsoCode = 'ALL';        
        testOppty1.Product_LOBs__c = 'Career';
        testOppty1.Opportunity_Country__c = 'INDIA';
        testOppty1.Opportunity_Office__c='Urbandale - Meredith';
        testOppty1.Opportunity_Region__c='International';
        testOppty1.RecordTypeId=rt;
        testOppty1.Buyer__c=testContact.Id;
        testOppty1.AccountId=testAccount.Id;
        testOppty1.Above_Funnel_Stage_Begin_Date__c =null;
     testOppty1.CreatedDate=system.now();
        insert testOppty1;
     
     
      OpportunityLineItem testOli1= new OpportunityLineItem();
        testOli1.OpportunityId=testOppty1.id;
        testOli1.PricebookEntryId = customPrice.Id;
        testOli1.Revenue_End_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()+3);
        testOli1.Revenue_Start_Date__c = Date.newInstance(Date.today().year(), Date.today().month(), Date.today().Day()-3);
        testOli1.UnitPrice = 1.00;
        testOli1.Sales_Price_USD_calc__c = 1.00;
        testOli1.CurrentYearRevenue_edit__c = 0.00;
        testOli1.Year2Revenue_edit__c = 0.00;
        testOli1.Year3Revenue_edit__c = 1.00;
        testOli1.Is_Project_Required__c=true;
        testOli1.Project_Linked__c=false;
        insert testOli1;     
    List<Opportunity> lop = new List<Opportunity>();
        lop.add(testOppty1);
        map<Id,Opportunity> map1 = new Map<Id,Opportunity>();
        map1.put(testOppty1.id, testOppty1);
    AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
        testOppty1.StageName = 'Identify';
        update testOppty1;
        AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
        testOppty1.StageName = 'Active Pursuit';
        update testOppty1;
        AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
         testOppty1.StageName = 'Selected';
        update testOppty1;
        AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
        
         testOppty1.StageName = 'Pending Project';
        update testOppty1;
        AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
        
        
        testOppty1.StageName='Above the Funnel';
        update testOppty1;
        AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(lop,map1);
 }
 /*static testMethod void myUnitTestBeforeUpdate() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
        
        List<ApexConstants__c> listAPC = new List<ApexConstants__c>();
        ApexConstants__c obj01 = new ApexConstants__c(Name='Above the funnel',StrValue__c='Marketing/Sales Lead;Executing Discovery;');listAPC.add(obj01);
        ApexConstants__c obj02 = new ApexConstants__c(Name='Selected',StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;');listAPC.add(obj02);
        ApexConstants__c obj03 = new ApexConstants__c(Name='Identify',StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;');listAPC.add(obj03);
        ApexConstants__c obj04 = new ApexConstants__c(Name='Active Pursuit',StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;');listAPC.add(obj04);
        ApexConstants__c obj05 = new ApexConstants__c(Name='AP02_OpportunityTriggerUtil_1',StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;IPIB01;MDMK02;MLTI07;MMMF01;TPEO50;');listAPC.add(obj05);
        ApexConstants__c obj06 = new ApexConstants__c(Name='AP02_OpportunityTriggerUtil_2',StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;');listAPC.add(obj06);
        ApexConstants__c obj08 = new ApexConstants__c(Name='ScopeITThresholdsList',StrValue__c='International:1000000000;North America:1000000000');listAPC.add(obj08);
        ApexConstants__c obj09 = new ApexConstants__c(Name='Opportunity_Country',StrValue__c='Korea;');listAPC.add(obj09);
        insert listAPC;
        
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.BeforeUpdate(mapOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestBeforeDelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.BeforeDelete(mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterInsert() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterInsert(lstOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterUpdate() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterUpdate(mapOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterDelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterDelete(mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterUndelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterUndelete(mapOpp);
     
 }*/
}