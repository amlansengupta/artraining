/*Purpose:   This test class provides daat coverage to manually allocate Revenue to Opportunity Product
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   11/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                                               Date                    Modified By
12638:Removing Step                                                      17-Jan-2019             Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_AP75_OppProductEditController {
    /*
     * @Description : Test method to provide data coverage to AP75_OppProductEditController class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
       /* Mercer_TestData testData = new Mercer_TestData();
        Account testAccount = testData.buildAccount();   
        testAccount.one_code__c = '013331' ;        
         update testAccount;*/  
            User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
         
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        testOppty.Opportunity_Office__c = ' Aarhus - Axel';
        //Request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
      
       test.starttest();

        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        insert opptylineItem;
        
        opptylineItem.UnitPrice = 120.00;
        opptylineItem.CurrentYearRevenue_edit__c = 40;
        opptylineItem.Year2Revenue_edit__c = 40;
        opptylineItem.Year3Revenue_edit__c = 40;
        update opptylineItem;
        
         
                        
         ApexPages.StandardController stdController = new ApexPages.StandardController(opptylineItem);
         
         AP75_OppProductEditController controller = new AP75_OppProductEditController(stdController);
         controller.ReallocateRevenue();
         controller.saveRecord();
         controller.cancel();
        opptyLineItem.Revenue_End_Date__c = date.Today()+5;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 120.00;
        opptylineItem.CurrentYearRevenue_edit__c = 120;
        opptylineItem.Year2Revenue_edit__c = null;
        opptylineItem.Year3Revenue_edit__c = null;
        try{
        update opptylineItem;
        }catch(Exception ex){}
        
         AP75_OppProductEditController controller1 = new AP75_OppProductEditController(stdController);
         controller1.ReallocateRevenue();
         controller1.saveRecord();
         controller1.cancel();
         
         Test.stoptest();
        }


}
    /*
     * @Description : Test method to provide data coverage to AP75_OppProductEditController class
     * @ Args       : Null
     * @ Return     : void
     */
        static testMethod void myUnitTest1() {
        /* Mercer_TestData testData = new Mercer_TestData();
        Account testAccount = testData.buildAccount();   
         testAccount.one_code__c = '013331' ;    
         update testAccount;           
            User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        Mercer_TestData mdata = new Mercer_TestData();
        Account acc = mdata.buildAccount();
        acc.one_code__c ='123';
        acc.One_Code_Status__c ='Active';
        Update acc;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = acc.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
      
       
       
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        insert opptylineItem;

         Test.startTest();
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+365; 
        opptylineItem.UnitPrice = 120.00;
        opptylineItem.CurrentYearRevenue_edit__c = 0;
        opptylineItem.Year2Revenue_edit__c = 60;
        opptylineItem.Year3Revenue_edit__c = 60;
        update opptylineItem;
         
        
         ApexPages.StandardController stdController = new ApexPages.StandardController(opptylineItem);
         
         AP75_OppProductEditController controller2 = new AP75_OppProductEditController(stdController);
         controller2.ReallocateRevenue();
         controller2.saveRecord();
         controller2.cancel();
          
         Test.stoptest();
        }
*/

}
}