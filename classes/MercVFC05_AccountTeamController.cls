/*Purpose: This Controller is an extension controller for the MercerAddAccountTeam Page. 
          
==============================================================================================================================================
History ----------------------- 

                                                                         VERSION     AUTHOR  DATE        DETAIL 
                                                                               1.0 -            Suchi    2/20/2013   Created Controller Class
============================================================================================================================================== 
*/
public class MercVFC05_AccountTeamController {
    public Opportunity opprec{get; set;}
    public Map<Id,UserAccountTeamMember> UserIdUserTeamMap = new Map<Id,UserAccountTeamMember>();
    public List<OpportunityTeamMember> OpportunityTeamMemberList = new List<OpportunityTeamMember>();
    public MercVFC05_AccountTeamController(ApexPages.StandardController controller) {
    
        this.opprec = (Opportunity)controller.getRecord();
        System.debug('\n Opportunity record from the controller :'+opprec.Id );
        
        Id OppOwnerID = [Select OwnerId from Opportunity where Id = :opprec.Id].OwnerId;
        
        for(UserAccountTeamMember utm : [Select UserId,TeamMemberRole,OpportunityAccessLevel,OwnerId from UserAccountTeamMember where OwnerID = :OppOwnerID ])
        {
            system.debug('User Team Member'+utm );
            UserIdUserTeamMap.put(utm.UserId,utm);
        }    
        
        for(Id userId : UserIdUserTeamMap.keyset())
        {
                        //create a new instance of opportunity team member
                        OpportunityTeamMember teamMember = new OpportunityTeamMember();
                        //assign new record's owner to the team member user ID    
                        teamMember.userId = UserIdUserTeamMap.get(userId).userId;
                        //assign the new records ID to the opportunity team's opportunity reference    
                        teamMember.OpportunityId = opprec.Id;
                         //assign the new records Role to the opportunity team's opportunity reference 
                        teamMember.TeamMemberRole = UserIdUserTeamMap.get(userId).TeamMemberRole;
                        //add the instance to the list
                        
                        OpportunityTeamMemberList.add(teamMember);  
        }
        
         
    }
    
   public PageReference methodToInsertTeam(){
            
            if(!OpportunityTeamMemberList.isEmpty()) Database.insert(OpportunityTeamMemberList, false);
            
            PageReference pageRef = new PageReference('/'+opprec.Id);
            return pageRef;
            
            
    }
    
    public void postChatter()    
    {
        String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        String url = salesforceHost + '/services/data/v26.0/chatter/feeds/record/' + account.Id + '/feed-items';
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + Account.Owner_Id__c + '" }, { "type": "text", "text" : "' + ' ' + 'Just created an Opportunity' + '" } ] } }');
        Http http = new Http();
        if(!Test.isRunningTest())
        HTTPResponse res = http.send(req);          
        
    }
    

}