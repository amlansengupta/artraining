/* Purpose: This Batchable class updates respective Opportunity records and populates Currency Total Revenue USD field
===================================================================================================================================
The methods contains following functionality;
 • Execute() method takes a list of Oportunity as an input and updates Opp record to  populate Currency Total Revenue field.
 
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Reetika          10/25/2013  PMO#3122 : Created Batch Class to update Currency Revenue USD.
 2.0         Sarbpreet        5/06/2014   PMO#3566(June Release) : Updated the code to refer it to current currency rates rather than dated currency
 3.0         Sarbpreet        7/7/2014    Added Comments. 
 4.0         Jagan            1/14/2015   Updated batch class to include other USD fields
 ======================================================================================================================================*/
global class DC27_OpportunityUSDCurrencyUpdate implements Database.Batchable<sObject>, Database.Stateful{
    //String Variable to store query
    public String query ;
    
    // List variable to store error logs
    public List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    //Updated as part of PMO#3566(June Release)
    List<CurrencyType> exchangeRates = new List<CurrencyType>();
    
    
    /*
    *   Class Constructor for inistializing the value of 'query' variable 
    */
    public DC27_OpportunityUSDCurrencyUpdate(String qStr)
    {
         this.query= qStr;
    }

    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(query);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */ 
    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {  
    
         Integer errorCount = 0 ;
         //List valiable to store Opportunity Line Items
         List<Opportunity> oppLst = new List<Opportunity>();
         List<Opportunitylineitem> oppProdLst = new List<Opportunitylineitem>();
         system.debug('\n The size of scope is ' + scope.size());
         Set<String> IsoCodeSet = new Set<String>();
         Set<String> OppIdSet = new Set<String>();
         Double datedCurrency = null;    
         
        // AP67_userTriggerUtil.updateFromBatchJob = true ;
         
         System.debug('\n\n DC27_OpportunityUSDCurrencyUpdate '+scope.size() + ' '+scope);
        // Get all opp owner Ids            
        for (Opportunity opp : scope)
        {
            IsoCodeSet.add(Opp.CurrencyIsoCode);
            OppIdSet.add(Opp.id);
            
        }
        
        if(!IsoCodeSet.isEmpty()) {
            //Updated as part of PMO#3566(June Release)
            exchangeRates = [SELECT ISOCode, ConversionRate,IsActive FROM CurrencyType   WHERE ISOCode in :IsoCodeSet];
        }
        System.debug('\n\n DC27_OpportunityUSDCurrencyUpdate exchangeRates '+exchangeRates.size() + ' '+exchangeRates);    
            
        if(!exchangeRates.isEmpty())
        {               
             //Iterate through Opportunity records fetched through 'query'
             for(Opportunity opp : scope)
             {
                            double currentyearrevenue  = 0; 
                            double totalscopablerevenue = 0;
                            double totalopprevenue = 0;
                            System.debug('/n/n DC27_OpportunityUSDCurrencyUpdate opp.CurrencyIsoCode: '+opp.CurrencyIsoCode);
                            Date oppCloseDate = opp.CloseDate;
                            //Updated as part of PMO#3566(June Release)
                            for(CurrencyType currencyVal : exchangeRates)
                            {   
                                System.debug(opp.CurrencyIsoCode+'/n/n DC27_OpportunityUSDCurrencyUpdate currencyVal.IsoCode: '+currencyVal.IsoCode);
                                if(currencyVal.IsoCode == opp.CurrencyIsoCode)   
                                {
                                    System.debug('/n/n  DC27_OpportunityUSDCurrencyUpdate currencyVal.IsoCode : ');
                                    datedCurrency = currencyVal.ConversionRate;
                                    //To calculate USD value of Current year revenue as per PMO#3122
                                    currentyearrevenue = opp.Current_Year_Revenue__c;
                                    totalscopablerevenue = opp.Total_Scopable_Products_Revenue__c;
                                    totalopprevenue = opp.Total_Opportunity_Revenue__c;
                                    
                                    currentyearrevenue = currentyearrevenue / datedCurrency ;
                                    totalscopablerevenue = totalscopablerevenue / datedCurrency ;
                                    totalopprevenue = totalopprevenue / datedCurrency ;
                                    
                                    opp.Current_Year_Revenue_USD__c = currentyearrevenue;
                                    opp.Total_Scopable_Products_Revenue_USD__c = totalscopablerevenue;
                                    opp.Total_Opportunity_Revenue_USD__c = totalopprevenue;
                                    
                                    oppLst.add(opp) ;
                                    break;
                                    
                                }
                          }
                          
                                                    
                }
        } 
     
     oppProdLst=[select id,Expected_Product_Sales_Price_USD__c,OpportunityId from OpportunityLineItem where OpportunityId in:OppIdSet];
     if(oppProdLst.size()>0)
         database.update(oppProdLst,false);
     oppProdLst.clear();
         
     if(oppLst.size()>0){   
         // List variable to save the result of the Opportunity Line Items which are updated   
         List<Database.SaveResult> srList= Database.Update(oppLst, false);  
         integer i = 0;
         if(srList!=null && srList.size()>0) {
             // Iterate through the saved Sales Credit  records         
             for (Database.SaveResult sr : srList)
             {
                    // Check if  Sales Credit  records are not updated successfully.If not, then store those failure in error log. 
                    if (!sr.isSuccess()) 
                    {
                            errorCount++;
                            System.debug('Error DC27 '+sr.getErrors()[0].getMessage());
                    
                            errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('DC27:' +sr.getErrors()[0].getMessage(), errorLogs, oppLst[i].Id);  
        
                    }
                    i++;
             }
         }
    
         //Update the status of batch in BatchLogger object
         List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>(); 
         errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('DC27 Status :scope:' + scope.size() +
                         ' oppLst:' + oppLst.size() +     + ' Total Errorlog Size: '+errorLogs.size() +
                          ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);    
         insert     errorLogStatus ;
         
     }
  }
  
 /*
 *  Method Name: finish
 *  Description: Method is used perform any final operations on the records which are in scope.     
 */     
 global void finish(Database.BatchableContext BC){
         if(errorLogs.size()>0)
             database.insert(errorlogs, false);

  }    

}

/* Query for executing the class
    String qry = 'Select id, Name, CurrencyIsoCode,Current_Year_Revenue_USD__c,CloseDate,Current_Year_Revenue__c from Opportunity';           
    database.executeBatch(new DC27_OpportunityUSDCurrencyUpdate(qry), 2000);           
*/