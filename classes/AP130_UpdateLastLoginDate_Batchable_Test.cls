/*Purpose:  This test class is created as a Part of July 2015 Release for Req:6292
==============================================================================================================================================
The below code is the test class for UpdateLastBrowserLoginDate_Batchable batch class.

History 
----------------------------------------------------------------------------------------------------------------------
VERSION - 1.0
AUTHOR - C. Pon Venkatesh
DATE - 05/21/2015
============================================================================================================================================== 
*/

/*
 * Keep SeeAllData=true as the it is not possible to create entries in LoginHistory table and we need it to fetch existing records
 */
@isTest(seeAllData = true)
private class AP130_UpdateLastLoginDate_Batchable_Test {
  
   
  /*
    Test method for UpdateLastBrowserLoginDate_Batchable batch class
  */ 
   static testmethod void test() {
       
        Map<Id, Date> testedData = new Map<Id, Date>();
        List<Id> UserIdList = new List<Id>();
        String query;
        List<String> user_List ;
        List<String> profile_List;
        
        User user1 = new User();
        String mercerAdminUserProfile = Mercer_TestData.getsystemAdminUserProfile();
        user1 = Mercer_TestData.createUser1(mercerAdminUserProfile, 'usert1', 'usrLstName', 1);
        
       System.runAs(user1) {
            
            // Run the UpdateLastBrowserLoginDate_Batchable apex batch
             
            Test.startTest();
            AP130_UpdateLastLoginDate_Batchable updateLastLoginBatch = new AP130_UpdateLastLoginDate_Batchable();
            Database.executeBatch(updateLastLoginBatch);
            Test.stopTest();
            
            /*
             * Get the testDataPrep prepared for testing from the UpdateLastBrowserLoginDate_Batchable class
             */
            testedData = AP130_UpdateLastLoginDate_Batchable.testDataPrep;
           
            /*
             * Get the list of users for whom the Last_Browser_Login_Date__c has been updated
             */
            user_List = new List<String>();
            profile_List  = new List<String>();
            UsersAndProfilesList__c toBeExcluded = UsersAndProfilesList__c.getValues('UpdateLastBrowserLoginDate_Batchable');
            if(toBeExcluded.Profiles_List__c != null)
            profile_List = toBeExcluded.Profiles_List__c.split(';');
            if(toBeExcluded.Users_List__c != null)
            user_List = toBeExcluded.Users_List__c.split(';');
            Map<Id, User> excludedUsersMap = new Map<Id, User>([Select Id from User where Id IN :user_list OR ProfileId IN :profile_List]); 
            for(Id userId : testedData.keySet()){
                if(excludedUsersMap.get(userId)==null){
                    UserIdList.addAll(testedData.keySet());
                }
            }
            query = 'Select Id, Last_Browser_Login_Date__c from User where Id in : UserIdList';
            List<LoginHistory> verificationDataHistory = (List<LoginHistory>)Database.query(Label.UpdateLastBrowserLoginDate_Query);
            List<User> verificationData = (List<User>)Database.query(query);
            
            /*
             * For all the users, verify that the date has been updated as expected
             */
            /*for(User u : verificationData){
                if(u.Last_Browser_Login_Date__c != null)
                System.assertEquals(u.Last_Browser_Login_Date__c, testedData.get(u.Id)); 
            }*/
        }
   }
   }