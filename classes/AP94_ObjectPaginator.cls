/*Purpose:   This apex class is written for Pagination and used for Product Search Page Pagination
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 2940)Created Apex class
   2.0 -    Sarbpreet   7/6/2013    Updated Comments/documentation. 
============================================================================================================================================== 
*/
global class AP94_ObjectPaginator {

    //================================================
    // CONSTRUCTORS    
    //================================================
    
    /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(){
        this(DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS, DEFAULT_SKIP_SIZE, null);
    } 
    
    /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    AP92_ObjectPaginatorListener listener ){
        this(DEFAULT_PAGE_SIZE, DEFAULT_PAGE_SIZE_OPTIONS, DEFAULT_SKIP_SIZE, listener);
    }
     
    /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    List<Integer> pageSizeIntegerOptions ){
        this(DEFAULT_PAGE_SIZE, pageSizeIntegerOptions, DEFAULT_SKIP_SIZE, null);
    }
    
    /*
    *   Creation of Constructor
    */ 
    global AP94_ObjectPaginator(    List<Integer> pageSizeIntegerOptions,
                            AP92_ObjectPaginatorListener listener ){
        this(DEFAULT_PAGE_SIZE, pageSizeIntegerOptions, DEFAULT_SKIP_SIZE, listener);
    } 
    
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    List<Integer> pageSizeIntegerOptions,
                            Integer skipSize ){
        this(DEFAULT_PAGE_SIZE, pageSizeIntegerOptions, skipSize, null);
    } 
    
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator( List<Integer> pageSizeIntegerOptions,
                            Integer skipSize,
                             AP92_ObjectPaginatorListener listener ){
        this(DEFAULT_PAGE_SIZE, pageSizeIntegerOptions, skipSize, listener);
    } 
    
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize ){
        this(pageSize, DEFAULT_PAGE_SIZE_OPTIONS, DEFAULT_SKIP_SIZE, null);
    }
     
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize,
                            AP92_ObjectPaginatorListener listener ){
        this(pageSize, DEFAULT_PAGE_SIZE_OPTIONS, DEFAULT_SKIP_SIZE, listener);
    } 
    
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize,
                            Integer skipSize ){
        this(pageSize, DEFAULT_PAGE_SIZE_OPTIONS, skipSize, null);
    }
     
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize,
                            Integer skipSize,
                            AP92_ObjectPaginatorListener listener ){
        this(pageSize, DEFAULT_PAGE_SIZE_OPTIONS, skipSize, listener);
    } 
    
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize,
                            List<Integer> pageSizeIntegerOptions){
        this(pageSize, pageSizeIntegerOptions, DEFAULT_SKIP_SIZE, null);
    }
     
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize,
                            List<Integer> pageSizeIntegerOptions,
                            AP92_ObjectPaginatorListener listener){
        this(pageSize, pageSizeIntegerOptions, DEFAULT_SKIP_SIZE, listener);
    }
    
   /*
    *   Creation of Constructor
    */ 
    global AP94_ObjectPaginator(    Integer pageSize,
                            List<Integer> pageSizeIntegerOptions,
                            Integer skipSize){
        this(pageSize, pageSizeIntegerOptions, skipSize, null);
    }
     
   /*
    *   Creation of Constructor
    */
    global AP94_ObjectPaginator(    Integer pageSize, 
                            List<Integer> pageSizeIntegerOptions, 
                            Integer skipSize, 
                            AP92_ObjectPaginatorListener listener){
        this.listeners = new List<AP92_ObjectPaginatorListener>();                                
        setPageSize(pageSize);
        setPageSizeOptions(pageSizeIntegerOptions);
        setSkipSize(skipSize);
        addListener(listener);
    }

    //================================================
    // CONSTANTS    
    //================================================
    global static final     Integer         DEFAULT_PAGE_SIZE             = 20;
    global static final     List<Integer>   DEFAULT_PAGE_SIZE_OPTIONS     = new List<Integer>{10,20,50,100,200};
    global static final     Integer         DEFAULT_SKIP_SIZE             = 3;
    global static final     Integer         MAX_SKIP_SIZE                 = 20;

    //================================================
    // PROPERTIES    
    //================================================
    global List<Object>                     all                     {get;private set;}
    global List<Object>                     page                    {get;private set;}
    global Integer                          pageSize                {get;private set;} 
    global List<Integer>                    pageSizeIntegerOptions  {get;private set;} 
    global List<SelectOption>               pageSizeSelectOptions   {get;private set;} 
    global Integer                          skipSize                {get;private set;}
    global Integer                          pageNumber              {get;private set;}
    global List<AP92_ObjectPaginatorListener>    listeners               {get;private set;}    

    //================================================
    // DERIVED PROPERTIES    
    //================================================
    /*
    *  Getter Method for Page count
    */
    global Integer pageCount { 
        get{ 
            Double allSize = this.all == null ? 0 : this.all.size(); 
            Double pageSize = this.pageSize; 
            return this.all == null ? 0 : Math.ceil(allSize/pageSize).intValue(); 
        } 
    }
    
    /*
    *  Getter Method for record count
    */
    global Integer recordCount {
        get{ 
            return this.all == null ? 0 : this.all.size(); 
        } 
    }
    
   /*
    *  Getter Method for next page
    */
    global Boolean hasNext{
        get{ 
            return pageNumber >= 0 && pageNumber < this.pageCount-1;
        }
    }
    
    /*
    *  Getter Method for previous page
    */
    global Boolean hasPrevious{
        get{
            return pageNumber > 0 && pageNumber <= this.pageCount-1;
        }
    }
    /*
    *  Getter Method for start position of page
    */
    global Integer pageStartPosition {
        get{ 
            return this.pageNumber * this.pageSize; 
        } 
    }
    /*
    *  Getter Method for end position of page
    */
    global Integer pageEndPosition {
        get{ 
            Integer endPosition = (this.pageNumber + 1) * this.pageSize - 1;
            endPosition = endPosition < recordCount ? endPosition : recordCount-1;
            return endPosition; 
        } 
    }
   /*
    *  Getter Method for skip previous page number
    */
    global List<Integer> previousSkipPageNumbers {
        get{
            List<Integer> returnValues = new List<Integer>();
            for(Integer i = skipSize; i > 0; i--){
                if(pageNumber-i < 0){
                    continue;
                }
                returnValues.add(pageNumber-i);
            }
            return returnValues;
        }
    }
    /*
    *  Getter Method for skip next page number
    */
    global List<Integer> nextSkipPageNumbers {
        get{
            List<Integer> returnValues = new List<Integer>();
            for(Integer i = 1; i <= skipSize; i++){
                if(pageNumber+i >= pageCount){
                    break;
                }
                returnValues.add(pageNumber+i);
            }
            return returnValues;
        }
    }
   /*
    *  Getter Method for displaying page number
    */
    global Integer pageNumberDisplayFriendly {
        get{ 
            return this.pageNumber + 1; 
        } 
    }
    
    /*
    *  Getter Method to display start position of page 
    */
    global Integer pageStartPositionDisplayFriendly {
        get{ 
            return this.pageStartPosition + 1; 
        } 
    }
   /*
    *  Getter Method to display end position of page 
    */
    global Integer pageEndPositionDisplayFriendly {
        get{ 
            return this.pageEndPosition + 1; 
        } 
    }

    //================================================
    // METHODS    
    //================================================
  /*
   * @Description : Method to set the record
   * @ Args       : List<Object> all
   * @ Return     : void
   */  
    global void setRecords(List<Object> all){
        reset(all,this.pageSize);
    }
  /*
   * @Description : Method to set the page size
   * @ Args       : Integer pageSize
   * @ Return     : void
   */
    global void setPageSize(Integer pageSize){
        if(this.pageSize!=pageSize){
            reset(this.all,pageSize);
        }
    }
  /*
   * @Description : Method to get the page size
   * @ Args       : Integer pageSize
   * @ Return     : void
   */
    global Integer getPageSize(){
        return this.pageSize;
    }

  /*
   * @Description : Method to set page size options
   * @ Args       : Integer pageSize
   * @ Return     : void
   */
    global void setPageSizeOptions(List<Integer> pageSizeIntegerOptions){
        this.pageSizeIntegerOptions = pageSizeIntegerOptions;
        if(this.pageSizeSelectOptions == null){
            this.pageSizeSelectOptions = new List<SelectOption>();
        }
        this.pageSizeSelectOptions.clear();
        if(pageSizeIntegerOptions != null && pageSizeIntegerOptions.size() > 0){
            for(Integer pageSizeOption : pageSizeIntegerOptions){
                if(pageSizeOption < 1){
                    continue;
                }
                this.pageSizeSelectOptions.add(new SelectOption(''+pageSizeOption,''+pageSizeOption));
            }
        }
    }
  /*
   * @Description : Method to get page size options
   * @ Args       : null
   * @ Return     : List<SelectOption>
   */
    global List<SelectOption> getPageSizeOptions(){
        return this.pageSizeSelectOptions;
    }
  /*
   * @Description : Method to set skip size
   * @ Args       : Integer skipSize
   * @ Return     : void
   */
    global void setSkipSize(Integer skipSize){
        this.skipSize = skipSize < 0 || skipSize > MAX_SKIP_SIZE ? this.skipSize : skipSize;
    }
  /*
   * @Description : Method to skip the page
   * @ Args       : Integer pageNumber
   * @ Return     : PageReference
   */
    global PageReference skipToPage(Integer pageNumber){
        if(pageNumber < 0 || pageNumber > this.pageCount-1){
            throw new AP91_IllegalArgumentException();
        }
        this.pageNumber = pageNumber;
        updatePage();
        return null;
    }
  /*
   * @Description : Method to go to next page
   * @ Args       : null
   * @ Return     : PageReference
   */
    global PageReference next(){
        if(!this.hasNext){
            throw new AP90_IllegalStateException();
        }
        this.pageNumber++;
        updatePage();
        return null;
    }
  /*
   * @Description : Method to go to previous page
   * @ Args       : null
   * @ Return     : PageReference
   */
    global PageReference previous(){
        if(!this.hasPrevious){
            throw new AP90_IllegalStateException();
        }
        this.pageNumber--;
        updatePage();
        return null;
    }
  /*
   * @Description : Method to go to first page
   * @ Args       : null
   * @ Return     : PageReference
   */
    global PageReference first(){
        this.pageNumber = 0;
        updatePage();
        return null;
    }
  /*
   * @Description : Method to go to last page
   * @ Args       : null
   * @ Return     : PageReference
   */
    global PageReference last(){
        this.pageNumber = pageCount - 1;
        updatePage();
        return null;
    }
  /*
   * @Description : Method to reset page
   * @ Args       : List<Object> all, Integer pageSize
   * @ Return     : void
   */
    private void reset(List<Object> all, Integer pageSize){
        this.all = all;
        this.pageSize = pageSize < 1 ? DEFAULT_PAGE_SIZE : pageSize;
        this.pageNumber = 0;
        updatePage();
    }
  /*
   * @Description : Method to update the page
   * @ Args       : null
   * @ Return     : void
   */
    private void updatePage() {
        this.page = null;
        if(this.all != null && this.all.size() > 0){
            this.page = new List<Object>();
            for (Integer i = this.pageStartPosition; i <= this.pageEndPosition; i++) {
                this.page.add(this.all.get(i));
            }
        }
        firePageChangeEvent();
    }
  /*
   * @Description : Method to add listener
   * @ Args       : AP92_ObjectPaginatorListener listener
   * @ Return     : void
   */
    global void addListener(AP92_ObjectPaginatorListener listener){
        if(listener != null){
            listeners.add(listener);
        }
    }

  /*
   * @Description : Method to fire page change event
   * @ Args       : null
   * @ Return     : void
   */
    global void firePageChangeEvent(){
        for(AP92_ObjectPaginatorListener listener : listeners){
            listener.handlePageChange(this.page);
        }
    }
}