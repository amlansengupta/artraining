/* Purpose:This Batchable class mass updates Opportunities with a chatter post which closes in next week.
===================================================================================================================================
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          12/11/2013  As a part of Request# 3587(January Release) created Batch Class to post chatter on Opportunity object
 2.0 -       Sarbpreet          7/6/2014    Updated Comments/documentation.
 ======================================================================================================================================*/
global class AP76_OpportunityReminderBatchable implements Database.Batchable<sObject>, Database.Stateful{
    //String Variable to store query
    public String query ;
   
    //String qStr = 'Select id, CloseDate, Reminder__c, from Opportunity where CloseDate <> null and CloseDate > TODAY()';
    // List variable to store error logs
    List<BatchErrorLogger__c> errorLogs = new List<BatchErrorLogger__c>();
    
    /*
    *   Class Constructor for inistializing the value of 'query' variable 
    */
    public AP76_OpportunityReminderBatchable(String qStr)
    {
         this.query= qStr;
    }
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('quer ' +query);
         return Database.getQueryLocator(query);
    }
    /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext BC, List<Opportunity> scope)
    {  
         Integer errorCount = 0 ;
         List<Opportunity> OpptyListUpdate = new List<Opportunity>();
         system.debug('\n The size of scope is ' + scope.size() );
         Set<Id> AccidSet = new Set<Id>();

         //Iterate through Opportunity records fetched through 'query'
         for(Opportunity opp : scope)
         {
           AccidSet.add(opp.accountid);
                                     
         } 
          
        Map<Id,Account> AccNameMap = new Map<Id,Account>([select id,Name from Account where id in:AccidSet]); 
        List<FeedItem> feedList = new List<FeedItem>(); 
        
        for(Opportunity newOppty:scope){ 
            //String to hold the body of the Chatter Post//
            String postBody;
               
                postBody = 'The  '+ newOppty.name + ' '+ System.Label.CL95_Opportunity_Close_Date_Reminder;   
                String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + newOppty.Id;
                postBody = postBody.replace('[Account Name]',AccNameMap.get(newOppty.Accountid).Name);
                FeedItem f = new FeedItem();
                f.createdbyid = Label.MercerForceUserid;
                f.body = postBody + ' '+fullRecordURL;
                f.parentid = newOppty.id;
                feedList.add(f);
                //insert f;
                OpptyListUpdate.add(newOppty);
              

        }
          insert(feedList);                  

                 
     // List variable to save the result of the Opportunity Line Items which are updated   
     List<Database.SaveResult> srList= Database.Update(OpptyListUpdate, false);  
     system.debug('\nTotal Opportunity are ' + OpptyListUpdate.size());
     //MercerAccountStatusBatchHelper.createErrorLog();
  
     if(srList!=null && srList.size()>0) {
         // Iterate through the saved Opportunity Line Items  
         Integer i = 0;             
         for (Database.SaveResult sr : srList)
         {
                // Check if Oppportunity are not updated successfully.If not, then store those failure in error log. 
                if (!sr.isSuccess()) 
                {
                        errorCount++;
                        System.debug('Error AP76_OpportunityReminderBatchable '+sr.getErrors()[0].getMessage());
                        errorLogs = MercerAccountStatusBatchHelper.addToErrorLog('AP76:' +sr.getErrors()[0].getMessage(), errorLogs, OpptyListUpdate[i].Id);  
                }
                i++;
         }
     }
     
     //Update the status of batch in BatchLogger object
     List<BatchErrorLogger__c> errorLogStatus = new List<BatchErrorLogger__c>();

      errorLogStatus = MercerAccountStatusBatchHelper.addToErrorLog('AP76 Status :scope:' + scope.size() +' OpptyListUpdate:' + OpptyListUpdate.size() +
     + ' Total Errorlog Size: '+errorLogs.size() + ' Batch Error Count :'+errorCount  ,  errorLogStatus , scope[0].Id);
     
     insert errorLogStatus;    
    }
     /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext BC)
    {
                           
        if(errorLogs.size()>0) 
         {
             database.insert(errorlogs, false);
         }
    }    
}