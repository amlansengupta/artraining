/*Purpose: Test Class for providing code coverage to TRG09_Pricebookentry class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   02/04/2013  Created test class
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_TRG09_Pricebookentry  {

    private static Product2 testProduct = new Product2();
    /*
     * @Description : Test method to provide data coverage to TRG09_Pricebookentry class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
    {
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
            test.startTest();
            testProduct.Name = 'TestPro';
            testProduct.Family = 'RRF';
            testProduct.IsActive = True;
            insert testProduct;
            update testProduct;
            test.stoptest();
        }
        
    }
}