@isTest
Public Class MreportGeneratorTest
{
    static testmethod void myTest1()
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678902';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        Account a = new Account();
        a.Name = 'Test1';
        a.One_Code__c = 'ARFB12';
        a.GU_DUNS__c = '1256';   
        a.DUNS__c = '1256';
        a.Global_Ultimate_Name__c = 'Test GUNAME';
        a.Relationship_Manager__c = testColleague.Id;
        //a.Relationship_Manager_Mobile__c =  testColleague.Name;
        a.Account_Country__c = 'India';
        a.Account_Region__c = 'EuroPac';
        a.Account_Market__c = 'ASEAN';
        a.Market_Segment__c = '4 - Key';
        a.Talent_CY_Revenue__c = 45.35;
        a.Health_CY_Revenue__c = 89.52;
        a.Retirement_CY_Revenue__c = 75.96;
        a.Investments_CY_Revenue__c = 52.36;
        a.Outsourcing_CY_Revenue__c = 56.63;
        a.Other_CY_Revenue__c = 42.56;
        a.Talent_PY_Revenue__c = 145.35;
        a.Health_PY_Revenue__c = 189.52;
        a.Retirement_PY_Revenue__c = 175.96;
        a.Investments_PY_Revenue__c = 152.36;
        a.Outsourcing_PY_Revenue__c = 156.63;
        a.Other_PY_Revenue__c = 42.56;
        a.Industry = 'Agriculture';
        a.Primary_Industry_Category__c = 'test';
        a.Employees_Editable__c = 123;
        a.billingstreet = 'A-302';
        a.billingcountry = 'India';
        a.Total_CY_Revenue__c = 452.85;
        a.Total_PY_Revenue__c = 879.52;
        a.Total_PY_Revenue__c = 50.00;
        a.Outsourcing_PY_Revenue__c = 50.00;
        a.B_B_B_PY__c = 40.00;
        a.Health_PY_Revenue__c = 20.00;
        a.Investments_PY_Revenue__c = 20.00;
        a.Other_PY_Revenue__c = 30.00;
        a.Retirement_PY_Revenue__c = 20.00;
        a.Talent_PY_Revenue__c  = 10.00;
        insert a;
        
        
Account a1 = new Account();
        a1.Name = 'Test1';
        a1.One_Code__c = 'ARFB12';
        a1.GU_DUNS__c = '1256';   
        a1.DUNS__c = '1256';
        a1.Global_Ultimate_Name__c = 'Test GUNAME';
        a1.Relationship_Manager__c = testColleague.Id;
        //a1.Relationship_Manager_Mobile__c =  testColleague.Name;
        a1.Account_Country__c = 'India';
        a1.Account_Region__c = 'EuroPac';
        a1.Account_Market__c = 'ASEAN';
        a1.Market_Segment__c = '4 - Key';
        a1.Talent_CY_Revenue__c = 45.35;
        a1.Health_CY_Revenue__c = 89.52;
        a1.Retirement_CY_Revenue__c = 75.96;
        a1.Investments_CY_Revenue__c = 52.36;
        a1.Outsourcing_CY_Revenue__c = 56.63;
        a1.Other_CY_Revenue__c = 42.56;
        a1.Talent_PY_Revenue__c = 145.35;
        a1.Health_PY_Revenue__c = 189.52;
        a1.Retirement_PY_Revenue__c = 175.96;
        a1.Investments_PY_Revenue__c = 152.36;
        a1.Outsourcing_PY_Revenue__c = 156.63;
        a1.Other_PY_Revenue__c = 42.56;
        a1.Industry = 'Agriculture';
        a1.Primary_Industry_Category__c = 'test';
        a1.Employees_Editable__c = 123;
        a1.billingstreet = 'A-302';
        a1.billingcountry = 'India';
        a1.Total_CY_Revenue__c = 452.85;
        a1.Total_PY_Revenue__c = 879.52;
        a1.Total_PY_Revenue__c = 50.00;
        a1.Outsourcing_PY_Revenue__c = 50.00;
        a1.B_B_B_PY__c = 40.00;
        a1.Health_PY_Revenue__c = 20.00;
        a1.Investments_PY_Revenue__c = 20.00;
        a1.Other_PY_Revenue__c = 30.00;
        a1.Retirement_PY_Revenue__c = 20.00;
        a1.Talent_PY_Revenue__c  = 10.00;
        insert a1;

        
        list<Account> AccList = new List<Account>();
        AccList.add(a);
        
        List<ID> AccId = new List<ID>();
        AccId.add(a.id);
        //Test Class Fix
        FCST_Fiscal_Year_List__c fcs= new FCST_Fiscal_Year_List__c();
        fcs.name='Test';
        fcs.StartDate__c= System.today();
        fcs.EndDate__c= System.today();
        insert fcs;
        
        Growth_Plan__c cgp = new Growth_Plan__c();
        //cgp.name = 'TestCGP';
        cgp.Account__c = a.id;
        cgp.Planning_Year__c = '2016';
        cgp.TOT_Recurring_Carry_forward_Revenue__c = 10.00;
        
        cgp.TOT_Projectd_Current_Year_Revenue__c = 10.00;
        cgp.TOT_Projected_Sales_Revenue__c = 10.00;
        cgp.TOT_Projectd_Full_Year_Revenue__c = 20.00;
        cgp.TOT_of_Revenue_Growth_Anticipated01__c = 50;
        //cgp.Account__r.Account_Country__c = 'India';
        cgp.fcstPlanning_Year__c=fcs.id;
        insert cgp;
        
        Growth_Plan__c cgp1 = new Growth_Plan__c();
        //cgp.name = 'TestCGP';
        cgp1.Account__c = a1.id;
        cgp1.Planning_Year__c = '2016';
        cgp1.TOT_Recurring_Carry_forward_Revenue__c = 10.00;
        cgp1.TOT_Projectd_Current_Year_Revenue__c = 10.00;
        cgp1.TOT_Projected_Sales_Revenue__c = 10.00;
        cgp1.TOT_Projectd_Full_Year_Revenue__c = 20.00;
        cgp1.TOT_of_Revenue_Growth_Anticipated01__c = 50;
        //cgp.Account__r.Account_Country__c = 'India';
        cgp1.fcstPlanning_Year__c=fcs.id;
        insert cgp1;
        
        
        List<Growth_Plan__c> cgpList = new List<Growth_Plan__c>();
        
        cgpList.add(cgp);
        cgpList.add(cgp1);
        
          PageReference pg = Page.MReportGenerator1;
          Test.setCurrentPage(pg); 
          ApexPages.currentPage().getParameters().put('onecode','ARFB12');
          ApexPages.currentPage().getParameters().put('guduns','1256');
          ApexPages.currentPage().getParameters().put('guname','Test GUNAME');
          ApexPages.currentPage().getParameters().put('planningyear','2016');
          MReportGenerator1 controller = new MReportGenerator1();

          MReportGenerator1.allCgp allCgpController = new MReportGenerator1.allCgp();
          MReportGenerator1.oneCodeData oneCodeDataController = new MReportGenerator1.oneCodeData();
          MReportGenerator1.allProjRev allProjRevController = new MReportGenerator1.allProjRev();
         //controller.newplanningyearInt = 2016; 
         controller.Summary();
         controller.calculateReportDetails(AccList,Accid,cgpList);
         controller.oneCodeRMCountry(AccList,Accid,cgpList);
         //controller.projectedRevenueByLOB(AccList,Accid,cgpList);
        
    }
}