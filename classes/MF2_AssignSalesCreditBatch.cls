global class MF2_AssignSalesCreditBatch implements Database.Batchable<sObject>{
    //public static boolean OppUpdateFlag = true;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        string Opptm='select id, OpportunityId from OpportunityTeamMember where Opportunity.isPrivate=false and Opportunity.CreatedDate>=2015-01-01T00:00:00.000+0000';
        return Database.getQueryLocator(Opptm);
        
    }
    global void execute(Database.BatchableContext bc, List<OpportunityTeamMember> scope){
        //ConstantsUtility.oppTeamsAfterUpdatekip = true;
        set<Id> opp = new Set<Id>();
        Set<id> finalOpp =new set<Id>();
        Boolean hasSalesCredit = false;
        for(OpportunityTeamMember otm : scope){
            opp.add(otm.OpportunityId);
        }
        List<opportunity> oppList=[select id,(select id from Sales_Creditings__r) from Opportunity where id in:opp];
        if(!oppList.isEmpty()){
            for(Opportunity op: oppList){
                for(Sales_credit__c sc:op.Sales_Creditings__r){
                hasSalesCredit = true; break;
                }
       
            if(!hasSalesCredit){
                finalOpp.add(op.Id);
            }
        }
            List<OpportunityTeamMember> lTeam =[select id ,flag__c from Opportunityteammember where opportunityId in:finalOpp];
             //AP03_OpportunityTeamMemberTriggerUtil.OppUpdateFlag = false;
            for(OpportunityTeamMember om : lTeam){
                if(om.flag__c){
                    om.flag__C = false;
                }
            }
            update lTeam;
        }
        
    }
    
    global void finish(Database.BatchableContext bc){
    }
        
}