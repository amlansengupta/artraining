/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History ----------------------- 
                                                         VERSION     AUTHOR  DATE        DETAIL                  
                                                          1.0 -    Arijit Roy 03/29/2013  Created Batchable class
===============================================================================================================================================
 */
global class AP33_ResetOfficeBatchable implements Database.Batchable<sObject>{

	global final String officeQuery = 'Select Id, Isupdated__c From Mercer_Office_Geo_D__c Where Isupdated__c = true';
	
	 /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
     
    global Database.QueryLocator start(Database.BatchableContext BC)
	{

		//execute the query and fetch the records	
        return Database.getQueryLocator(officeQuery);
    }
    
     /*
     *  Method Name: execute
     *  Description: Add the records to a map and assign the values of 6 fields related to region to corresponding account and Opportunity fields                 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
     
    global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		List<Mercer_Office_Geo_D__c> officeForUpdate = new List<Mercer_Office_Geo_D__c>();
		for(Mercer_Office_Geo_D__c office : (List<Mercer_Office_Geo_D__c>) scope)
		{
			office.Isupdated__c = false;
			officeForUpdate.add(office);			
		}
		
		if(!officeForUpdate.isEmpty()) Database.update(officeForUpdate, false);
	}
	
	/*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
     
    global void finish(Database.BatchableContext BC)
    {
    	
    	 List<CronTrigger> listCronTrigger = [select Id from CronTrigger where State = 'DELETED' AND nextfiretime = null AND TimesTriggered = 1 AND PreviousFireTime = TODAY];
    	 
    	  If (listCronTrigger.size() > 0)
		  {
		  		for (Integer i = 0; i < listCronTrigger.size(); i++)
		   		{ System.abortJob(listCronTrigger[i].Id); }
		  }
    }
}