/*Purpose: This Apex class implements schedulable interface to schedule MS08_OpportunityRevenueBatchable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR       DATE                    DETAIL 
   1.0 -    Arijit     04/24/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/


global class MS07_OpportunityRevenueSchedulable implements Schedulable{
 global void execute(SchedulableContext sc)
 {
    Database.executeBatch(new MS08_OpportunityRevenueBatchable(), Integer.valueOf(System.Label.CL75_MS08BatchSize));
 }

}