@isTest(seeAllData = false)
private class OppHandlerClass_Test {

 static testMethod void myUnitTestisDisabled() {
     OppHandlerClass objOpp = new OppHandlerClass();
     objOpp.isDisabled();
     TriggerSettings__c TrgSetting = new TriggerSettings__c();
     TrgSetting.Name = 'Opportunity Trigger';
     TrgSetting.Trigger_Disabled__c = True;
     insert TrgSetting; 
     objOpp.isDisabled();
 }
 
 static testMethod void myUnitTestBeforeInsert() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        objOpp.BeforeInsert(lstOpp);
     
 }
 
 static testMethod void myUnitTestBeforeUpdate() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
        
        List<ApexConstants__c> listAPC = new List<ApexConstants__c>();
        ApexConstants__c obj01 = new ApexConstants__c(Name='Above the funnel',StrValue__c='Marketing/Sales Lead;Executing Discovery;');listAPC.add(obj01);
        ApexConstants__c obj02 = new ApexConstants__c(Name='Selected',StrValue__c='Selected & Finalizing EL &/or SOW;Pending Chargeable Code;');listAPC.add(obj02);
        ApexConstants__c obj03 = new ApexConstants__c(Name='Identify',StrValue__c='Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;');listAPC.add(obj03);
        ApexConstants__c obj04 = new ApexConstants__c(Name='Active Pursuit',StrValue__c='Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;');listAPC.add(obj04);
        ApexConstants__c obj05 = new ApexConstants__c(Name='AP02_OpportunityTriggerUtil_1',StrValue__c='MERIPS;MRCR12;MIBM01;HBIN04;HBSM01;IND210l;MSOL01;MAAU01;MWSS50;MERC33;MINL44;MINL45;MAR163;IPIB01;MERJ00;MIMB44;CPSG02;MCRCSR;IPIB01;MDMK02;MLTI07;MMMF01;TPEO50;');listAPC.add(obj05);
        ApexConstants__c obj06 = new ApexConstants__c(Name='AP02_OpportunityTriggerUtil_2',StrValue__c='Seoul - Gangnamdae-ro;Taipei - Minquan East;');listAPC.add(obj06);
        ApexConstants__c obj08 = new ApexConstants__c(Name='ScopeITThresholdsList',StrValue__c='International:1000000000;North America:1000000000');listAPC.add(obj08);
        ApexConstants__c obj09 = new ApexConstants__c(Name='Opportunity_Country',StrValue__c='Korea;');listAPC.add(obj09);
        insert listAPC;
        
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.BeforeUpdate(mapOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestBeforeDelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.BeforeDelete(mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterInsert() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterInsert(lstOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterUpdate() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterUpdate(mapOpp,mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterDelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterDelete(mapOpp);
     
 }
 
 static testMethod void myUnitTestAfterUndelete() {
     
        Mercer_TestData mdata = new Mercer_TestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        id uid = userinfo.getUserId();
        User user1 = [select id from user where id=:uid];
        String recipientid=user1.id;
        
        Account accrec =  mdata.buildAccount();
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc08766@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = accrec.Id;
        insert testContact;
        
        //create user record and insert
        
        //User usr = Mercer_TestData.createUser(mercerStandardUserProfile, 'testxyz', 'opptrgtest', new User());
        
        Opportunity opprec = new Opportunity();
        opprec.OwnerId = UserInfo.getUserId();
        opprec.Name = 'TestOppty';
        opprec.Type = 'New Client';
        opprec.AccountId = accrec.Id;
        opprec.Step__c = 'Pending Chargeable Code';
        opprec.StageName = 'Above the Funnel';
        opprec.CloseDate = date.Today();
        opprec.CurrencyIsoCode = 'ALL';
        opprec.Opportunity_Office__c = 'London - Queens';
        opprec.Amount = 100;
        opprec.Buyer__c=testContact.id;
        opprec.Close_Stage_Reason__c ='Other';
        opprec.LeadSource = 'Marketing Assigned';
        
        Opportunity opprec1 = new Opportunity();
        opprec1.OwnerId = UserInfo.getUserId();
        opprec1.Name = 'TestOppty';
        opprec1.Type = 'New Client';
        opprec1.AccountId = accrec.Id;
        opprec1.Step__c = 'Made Go Decision';
        opprec1.StageName = 'Pending Project';
        opprec1.CloseDate = date.Today();
        opprec1.CurrencyIsoCode = 'ALL';
        opprec1.Opportunity_Office__c = 'London - Queens';
        opprec1.Amount = 100;
        opprec1.Buyer__c=testContact.id;
        opprec1.Close_Stage_Reason__c ='Other';
        opprec1.LeadSource = 'Other';
        opprec1.isCloned__c=True;
        Test.startTest(); 
        List<Opportunity> lstOpp = new List<Opportunity>();
        lstOpp.add(opprec);lstOpp.add(opprec1);
        insert lstOpp;
     
        OppHandlerClass objOpp = new OppHandlerClass();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        for(Opportunity opp:lstOpp){
            mapOpp.put(opp.Id,opp);
        }
        objOpp.AfterUndelete(mapOpp);
     
 }
}