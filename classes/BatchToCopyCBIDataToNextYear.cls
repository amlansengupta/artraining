global class BatchToCopyCBIDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopyCBIDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
        String header = 'Account,Client Business Imperative,Growth Plan,Our Offering Area ,Status,To Be Proposed Solution,Record Status \n';
        string hearder2= 'Id,Account,Client Business Imperative,Growth Plan,Our Offering Area,Status ,To Be Proposed Solution,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear');
		}
		else{
			return null;
		}	
	}
	
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        
        Set<Id> accountIdSet = new Set<Id>(); 
        System.debug('oldGPList >>>>'+oldGPList.size());
        try {
			for(Growth_Plan__c gpOldobj :oldGPList){
					accountIdSet.add(gpOldobj.Account__c);
			}
			System.debug('BatchToCopyCBIDataToNextYear accountIdSet>>>>'+accountIdSet.size());	
			Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
			List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear ) from Account where ID IN:accountIdSet];
            for(Account a : accList){
				for(Growth_Plan__c gg : a.Growth_Plans__r){
					accIdVsNewGp.put(gg.Account__c,gg);
				}  
			}
			
			List<Client_Business_Imperatives__c> insertNewCBIObjList = new List<Client_Business_Imperatives__c>();
	    	List<Client_Business_Imperatives__c> cbiObjectList=[Select Account__c,Client_Business_Imperative__c,Growth_Plan__c,Our_Offering_Area__c,Status__c,To_Be_Proposed_Solution__c,LastModifiedBy.Name,lastModifiedDate from Client_Business_Imperatives__c where Growth_Plan__c IN:oldGPList ];
	    	
            System.debug('cbiObjectList >>>>'+cbiObjectList.size());
	    	if(cbiObjectList!=null && cbiObjectList.size()>0){
	    		for(Client_Business_Imperatives__c cbiOldObj: cbiObjectList){
	    			if(accIdVsNewGp.containsKey(cbiOldObj.Account__c)){
		    			Client_Business_Imperatives__c cbiNewObj = new Client_Business_Imperatives__c();
		    			if(cbiOldObj.Account__c!=null)
		    				cbiNewObj.Account__c=cbiOldObj.Account__c;
		    			if(cbiOldObj.Client_Business_Imperative__c!=null)	
		    				cbiNewObj.Client_Business_Imperative__c=cbiOldObj.Client_Business_Imperative__c;
		    			cbiNewObj.Growth_Plan__c=accIdVsNewGp.get(cbiOldObj.Account__c).Id;
		    			if(cbiOldObj.Our_Offering_Area__c!=null)	
		    				cbiNewObj.Our_Offering_Area__c=cbiOldObj.Our_Offering_Area__c;
		    			if(cbiOldObj.Status__c!=null)	
		    				cbiNewObj.Status__c=cbiOldObj.Status__c;
		    			if(cbiOldObj.To_Be_Proposed_Solution__c!=null)
		    				cbiNewObj.To_Be_Proposed_Solution__c=cbiOldObj.To_Be_Proposed_Solution__c;
                        
                        cbiNewObj.Imperatives_LastModified_By__c = cbiOldObj.LastModifiedBy.Name;
                        DateTime dt = cbiOldObj.LastModifiedDate;
                        cbiNewObj.Imperatives_LastModified_Date__c = Date.newInstance(dt.year(), dt.month(), dt.day());
		    			
		    			insertNewCBIObjList.add(cbiNewObj);
		    			
	    			}
	    		}
	    	}
	    	System.debug('BatchToCopyCBIDataToNextYear insertNewCBIObjList>>>>'+insertNewCBIObjList.size());
	    	if(insertNewCBIObjList!=null && insertNewCBIObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewCBIObjList,false);
	    		if(srList!=null && srList.size()>0){
    				for(Integer i=0;i<srList.size();i++){
    					String clientBI='',ourOfferingArea='',StatusStr='',ToBeProposed='';
    					
    					if(insertNewCBIObjList[i].Client_Business_Imperative__c!=null)
    						clientBI=insertNewCBIObjList[i].Client_Business_Imperative__c;
    					if(insertNewCBIObjList[i].Our_Offering_Area__c!=null)
    						ourOfferingArea=insertNewCBIObjList[i].Our_Offering_Area__c;
    					if(insertNewCBIObjList[i].Status__c!=null)
    						StatusStr=insertNewCBIObjList[i].Status__c;
    					if(insertNewCBIObjList[i].To_Be_Proposed_Solution__c!=null)
    						ToBeProposed=insertNewCBIObjList[i].To_Be_Proposed_Solution__c;
    						
    						
    						
    					if(srList.get(i).isSuccess()){
    						finalSuccessStr+=srList.get(i).getId()+','+insertNewCBIObjList[i].Account__c+','+clientBI.replace(',', '')+','+insertNewCBIObjList[i].Growth_Plan__c+','+ourOfferingArea.replace(',', '')+','+StatusStr.replace(',', '')+','+ToBeProposed.replace(',', '')+', Successfully created \n';
    					}
    					if(!srList.get(i).isSuccess()){
    						Database.Error error = srList.get(i).getErrors().get(0);
    						finalFailStr+=insertNewCBIObjList[i].Account__c+','+clientBI.replace(',', '')+','+insertNewCBIObjList[i].Growth_Plan__c+','+ourOfferingArea.replace(',', '')+','+StatusStr.replace(',', '')+','+ToBeProposed.replace(',', '')+','+error.getMessage()+' \n';
    					}
    				}
    			}
	    	}
	    	System.debug('BatchToCopyCBIDataToNextYear finalSuccessStr>>>>'+finalSuccessStr);
		    System.debug('BatchToCopyCBIDataToNextYear finalFailStr>>>> '+finalFailStr);		
		}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }   		
    }
    
    global void finish(Database.BatchableContext bc){
    	
    	System.debug('BatchToCopyCBIDataToNextYear finish finalSuccessStr>>>>'+finalSuccessStr);
		System.debug('BatchToCopyCBIDataToNextYear finish finalFailStr>>>> '+finalFailStr);
		    
		    
		 AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Client Business Imperatives Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Client Business Imperatives Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Client Business Imperatives Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Client Business Imperatives copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}