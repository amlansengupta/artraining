/*
==============================================================================================================================================
Request Id                                   Date                    Modified By
12638:Removing Step                          17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest
public class TaskEmailSendControllerTest{

    static testmethod void getDetails_Test(){
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        System.runas(user1){
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        //Test Class Fix
        testAccount.one_Code__c='Test';
        insert testAccount;
        
        test.startTest();
        contact c = new contact();
        c.accountid = testAccount.id;
        c.firstName = 'test first name';
        c.lastname = 'test last name';
        c.email = 'xyzq2@abc07.com';
        insert c;
        
        Task testtask = new Task();
        
        testtask.Subject = 'Marketing Qualified Lead - 001';
        testtask.Status = 'Assigned';     
        testtask.Priority = 'Normal';
        testtask.WhoId = c.Id;
        testtask.RecordTypeId = [Select Id From RecordType Where Name = 'RFC Task'][0].Id;
        insert testtask;    

        Opportunity oppty = new Opportunity();
        oppty.name = testtask.subject;
        oppty.accountid = c.accountid;
        //request id:12638 commenting step
        //oppty.Step__c = 'Marketing/Sales Lead';
        oppty.StageName = 'Above the Funnel';
        oppty.CloseDate = system.today() + 180;
        oppty.Type = 'Existing Client - New LOB';
        //oppty.LeadSource = tk.LeadSource__c;       
        oppty.Opportunity_Office__c = 'MQL - Temp Office';           
        oppty.RFC_Opportunity__c = TRUE;
        oppty.Ownerid = UserInfo.getUserId();
        oppty.CurrencyIsoCode= testtask.CurrencyIsoCode;
        oppty.Description = testtask.Description;
        oppty.Task_ID__c = testtask.Id;
            
         insert oppty;
            
         testtask.WhatId = oppty.Id;   
         update testtask;
         
         TaskEmailSendController controller = new TaskEmailSendController();
         controller.taskSfdcId = testtask.id;
         controller.getDetails();
         
        Lead lead = new Lead();
        lead.FirstName =  'Test FirstName';
        lead.LastName = 'Test LastName';
        lead.Company = 'Test Company';
        lead.Title = 'Test Title';
        lead.Status = 'Open';
        lead.Email = 'test3@email.com';
        //lead.LeanData__Reporting_Matched_Account__c = testAccount.Id;
        insert lead;
         
         testtask.WhoId = lead.Id;
         testtask.WhatId = null;
         update testtask;
         controller.getDetails();
         
         
         Test.stopTest();
         
        
        }
        
        }
        
        
}