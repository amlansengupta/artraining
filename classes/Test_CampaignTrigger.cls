/*Purpose:  This test class provides data coverage to Campaign trigger functionality
==============================================================================================================================================
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit    03/29/2013  Created test class .
                                
 
============================================================================================================================================== 
*/
@istest(seealldata=true)
private class Test_CampaignTrigger
{

   /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 1)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign1()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Arijit Test Campaign';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        insert campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        test.stoptest();
        }
    }
     /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 2)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign2()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){  
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Region';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        insert campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Market*';
        campaign.Market__c = 'Europe';
        update campaign;
        test.stoptest();
        }
         }
         
    /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 3)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign3() 
     {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){        
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Market';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        insert campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        test.stoptest();
        }
     }
     
      /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 4)
     * @ Args       : Null
     * @ Return     : void
     */
     private static testmethod void testCampaign4() 
     {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){    
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Sub-Market';
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        insert campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        update campaign;
        test.stoptest();
        }
     }
     
     
 /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 4)
     * @ Args       : Null
     * @ Return     : void
     */
private static testmethod void testCampaign6() 
     {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Global';
        campaign.scope__c = 'Global';
        insert campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        test.stoptest();
        }
}

 /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 5)
     * @ Args       : Null
     * @ Return     : void
     */
private static testmethod void testCampaign7() 
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){   
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.currencyISOcode = 'USD';
        campaign.Name = 'Test Country';
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        insert campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        update campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Global';
        update campaign;
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        update campaign;
        test.stoptest();
        }
    }

 /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 6)
     * @ Args       : Null
     * @ Return     : void
     */    
private static testmethod void testCampaign8() 
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Mercer_TestData.buildMercerOffice();
        test.starttest();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Sub-Region1';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Sub-Region';
        campaign.Sub_Region__c = 'Europe';
        insert campaign;
        campaign.scope__c = 'Global';
        update campaign;
        campaign.scope__c = 'Market';
        campaign.Market__c = 'Europe';
        update campaign;
        campaign.scope__c = 'Sub-Market';
        campaign.Sub_Market__c = 'India';
        update campaign;
        campaign.scope__c = 'Country';
        campaign.country__c = 'Japan';
        update campaign;
        campaign.scope__c = 'Region';
        campaign.Region__c ='International';
        update campaign;
        test.stoptest();
        }
        }

 /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 7)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign5() 
        {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
            try
            {
            test.starttest();
            Mercer_TestData.buildMercerOffice();
            Campaign campaign  = new Campaign();
            campaign.Name = 'Test Sub-Region1';
            campaign.currencyISOcode = 'USD';
            campaign.scope__c = 'Sub-Region';
            test.stoptest();
            insert campaign;
            
            }
            catch (DmlException e )
            {
                
                
                System.assert( e.getMessage().contains('Please enter sub-region'),e.getMessage() );

                    
                

            }
            }
            } 
             /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 8)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign9() 
          { 
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){
            try
            {
            test.starttest();
            Mercer_TestData.buildMercerOffice();
            Campaign campaign  = new Campaign();
            campaign.Name = 'Test Country1';
            campaign.currencyISOcode = 'USD';
            campaign.scope__c = 'Country';
            test.stoptest();
            insert campaign;
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter Country'),e.getMessage() );
                
                
            }
            }
            }
             /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 9)
     * @ Args       : Null
     * @ Return     : void
     */
    private static testmethod void testCampaign10()
        {   
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){       
            try
            {
            test.starttest();
            Mercer_TestData.buildMercerOffice();
            Campaign campaign  = new Campaign();
            campaign.Name = 'Test Region1';
            campaign.currencyISOcode = 'USD';
            campaign.scope__c ='Region';
            test.stoptest();
            insert campaign;
                
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter region'),e.getMessage() );
                
            }
            }
        } 
         /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 10)
     * @ Args       : Null
     * @ Return     : void
     */
        private static testmethod void testCampaign11()
        {   User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){
            try
            {
            test.starttest();
            Mercer_TestData.buildMercerOffice();
            Campaign campaign  = new Campaign();
            campaign.Name = 'Test Market1';
            campaign.currencyISOcode = 'USD';
            campaign.scope__c ='Market';
            test.stoptest();
            insert campaign;

                
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter market'),e.getMessage() );
                
            }
            }
        } 
        
         /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 11)
     * @ Args       : Null
     * @ Return     : void
     */
        private static testmethod void testCampaign12()
        {   User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){
            try
            {
            test.starttest();
            Mercer_TestData.buildMercerOffice();
            Campaign campaign  = new Campaign();
            campaign.Name = 'Test Office1';
            campaign.currencyISOcode = 'USD';
            campaign.scope__c ='Office';
            test.stoptest();
            insert campaign;

                
            }
            catch (DmlException e )
            {
            
             System.assert( e.getMessage().contains('Please enter an Office'),e.getMessage() );
                
            }
            }
        } 
        
         /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 12)
     * @ Args       : Null
     * @ Return     : void
     */
        private static testmethod void testCampaign13()
        {   
            User user1 = new User();
            String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
            user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
            system.runAs(User1){       
            List<Campaign> cList = new List<Campaign>();    
            
            Mercer_TestData.buildMercerOffice();
            Campaign campaign1  = new Campaign();
            campaign1.currencyISOcode = 'USD';
            campaign1.Name = 'Test Office2';
            campaign1.scope__c = 'Office';
            campaign1.Office__c = 'New Delhi';
            cList.add(campaign1);   
            
            Campaign campaign2  = new Campaign();
            campaign2.currencyISOcode = 'USD';
            campaign2.Name = 'Test Office4';
            campaign2.scope__c = 'Office';
            campaign2.Office__c = 'New Delhi';
            cList.add(campaign2);
            test.starttest();
            insert cList;
            test.stoptest();
            }
        }
        
         /*
     * @Description : Test method to provide data coverage to campaign trigger functionality(scenerio 13)
     * @ Args       : Null
     * @ Return     : void
     */
         private static testmethod void testCampaign14()
        {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        system.runAs(User1){
        Mercer_TestData.buildMercerOffice();
        Campaign campaign  = new Campaign();
        campaign.Name = 'Test Campaign 3457';
        campaign.currencyISOcode = 'USD';
        campaign.scope__c = 'Office';
        campaign.Office__c = 'New Delhi';
        test.starttest();
        insert campaign;
        test.stoptest();
        }
       
       
        }
        

        
        }