Public Class AdminContOwnerUpdateCont{

    Public Blob            csvFile          {get;set;}
    Public list<wrapper>   csvDetails       {get;set;}
    Public list<wrapper>   databaseDetails  {get;set;}
    Public Boolean         ShowResults      {get;set;}
    Public Boolean         ShowSuccess      {get;set;}
    Public Boolean         ShowDBdetails    {get;set;}
    Public String          EmailId          {get;set;}
    
    
    Map<String,String>  mapConId_SFDCId ;
    Map<String,String>  mapSFDCId_ConId ;
    Map<String,String>  mapPeopleSoftId_SFDCId ;
    Set<String>         ContactIds ;
    Set<String>         PeopleSoftIds ;
    Map<Id, Contact>    mapConsUpdate ;
    list<Contact>       updatecons ;
    
    SET<String> BackUpUserIDs = new SET<String> ();
    SET<String> BackUpConIDs = new SET<String> ();
    
    String Header = 'Contact ID, Comments \n';
    String SuccessHeader = 'ID, CONTACT ID, STATUS \n';
    String ErrorHeader = 'CONTACT ID, OWNER PEOPLESOFT ID , ERROR \n';
    String BackUpHeader = 'ID, CONTACT ID, OWNER PEOPLESOFT ID , OWNER ID \n';
    String SuccessFileContents = '';
    String ErrorFileContents  = '';
    String BackUPContents = '' ;
    
    
    Public AdminContOwnerUpdateCont() {
        ShowDBdetails      = False ;
        ShowResults        = False ;
        ShowSuccess        = False ;
    }
    
    public void updatecons () {
        
         
        
        Contact[] BackUpContacts = [ SELECT Id, Contact_ID2__c , OWNERID FROM CONTACT WHERE ID IN :BackUpConIDs] ;
        for( Contact c : BackUpContacts ){
            BackUpUserIDs.add(c.OwnerID); 
        }
        
        System.Debug(' BackUpUserIDs ==> ' + BackUpUserIDs );
        
        MAP<ID,User> MapBackupUserDetails = new MAP<ID,User> ([Select id, EmployeeNumber  from User where id IN: BackUpUserIDs ]) ;
        
        BackUPContents = BackUpHeader ;
        
        System.Debug(' MapBackupUserDetails ==> ' + MapBackupUserDetails );
        
        for( Contact c :BackUpContacts ){
            BackUPContents +=  c.ID +','+ c.Contact_ID2__c +','+ MapBackupUserDetails.get(c.OwnerId).EmployeeNumber +','+ c.OwnerId+ '\n';
        }

        
        Database.SaveResult[] srList = Database.update(updatecons,false);
        
        SuccessFileContents = SuccessHeader ;
         
        for(Integer idx = 0; idx < srList.size(); idx++) {
            
            System.Debug(' srList[idx] ==> ' + srList[idx] );
            
            if(srList[idx].isSuccess()) {
                SuccessFileContents += updatecons[idx].Id+','+mapSFDCId_ConId.get(updatecons[idx].Id)+', Item Updated\n';
            } else {
                 for(Database.Error err : srList[idx].getErrors()) {
                 
                     System.Debug(' err.getMessage() ==> ' + err.getMessage());
                     String errorMsg = err.getMessage() ;
                     errorMsg = errorMsg.substringbefore(','); 
                     ErrorFileContents  += mapSFDCId_ConId.get(updatecons[idx].Id)+','+errorMsg+ '\n';
                 }
            }
        }
         
        BackUPContents += ' ,  \n';
        SuccessFileContents += ' ,  \n';
        ErrorFileContents  += ' ,  \n';
        
        SendEmail( SuccessFileContents , ErrorFileContents , BackUPContents );
        
        ShowSuccess        = True ;
        ShowDBdetails      = False ;
    } 

    
    public void FetchContDetails(){
        
        ShowDBdetails      = True ;
        ShowResults        = True ;
        
        opencsvDetails();
        ErrorFileContents   += Header ;
       
        
        mapConid_SFDCId = new Map<String,String> () ;
        mapPeopleSoftId_SFDCId = new Map<String,String> () ;
        updatecons = new list<Contact> () ;
        mapSFDCId_ConId = new Map<String,String> () ;
        
        databaseDetails = new list<wrapper> (csvDetails) ; 
        mapConsUpdate = new Map<Id,Contact> () ;
        
        User[] users = [Select id, EmployeeNumber  from User where EmployeeNumber IN: PeopleSoftIds ];
        mapConsUpdate = new Map<Id,Contact> ([Select id, Contact_ID2__c  from Contact where Contact_ID2__c IN: ContactIds ]) ;
        
        
        for ( User u :users ) { 
            mapPeopleSoftId_SFDCId.put( u.EmployeeNumber, u.Id ); 
            
        }
        
        System.Debug(' mapPeopleSoftId_SFDCId ==> ' + mapPeopleSoftId_SFDCId );
        
        for ( contact a :mapConsUpdate.values() ) { 
            mapconid_SFDCId.put( a.Contact_ID2__c , a.Id ); 
            mapSFDCId_ConId.put( a.Id, a.Contact_ID2__c  ); 
        }
        
        System.Debug(' mapAccid_SFDCId ==> ' + mapconid_SFDCId );
        
        Map<Id,User> UserDetails = new Map<Id,User> ( [ SELECT  Name, Id, Lastname, firstname 
                                                            FROM User 
                                                        WHERE id in : mapPeopleSoftId_SFDCId.values() ]);
        
        BackUpUserIDs = new SET<String> ();
        
        for ( wrapper a :databaseDetails ) {
            
             
            a.isValid = True;
            
            if( mapconid_SFDCId.get(a.ContactId ) != null ) {
                a.SFDCContactId = mapconid_SFDCId.get(a.ContactId) ;
                a.IsValidContactId = true ;
            } else {
                a.SFDCContactId = 'Contact doesn`t Exists! ' ;
                a.IsValidContactId = false ;
                ErrorFileContents  += a.ContactId +','+a.SFDCContactId+ '\n';
                a.Comments = 'Contact doesn`t Exists! ' ;
                a.isValid = False ;
            }
            
            if (  a.IsValidContactId ) {
                if( mapPeopleSoftId_SFDCId.get(a.OwnerPeopleSoftId) <> null  ) {
                    a.SFDCOwnerId = mapPeopleSoftId_SFDCId.get(a.OwnerPeopleSoftId) ;
                    a.UserName = UserDetails.get(a.SFDCOwnerId).name; 
                    a.IsValidOwnerId = true ;
                } else {
                    a.SFDCOwnerId = 'User doesn`t Exists! ' ;
                    a.Comments = 'User doesn`t Exists! ' ;
                        ErrorFileContents  += a.ContactId +','+ a.SFDCOwnerId + '\n';
                    a.isValid = False ;
                }
            }
            
            if ( a.SFDCContactId.Startswith('003') && a.SFDCOwnerId.Startswith('005') ) { 
                     contact con = new Contact(Id = a.SFDCContactId );
                     BackUpConIDs.add(con.Id); 
                     con.OwnerId = a.SFDCOwnerId ;
                    updatecons.add(con); 
            }     
       } 
       
       
       
       System.Debug(' databaseDetails ==> ' + databaseDetails ); 
    }
     
   Public void opencsvDetails(){
        
        System.Debug(' opencsvDetails ==> '  ); 
        
        String strBody = csvFile.toString();
        list<String> filelines = strBody.split('\n');
        
        if ( ! filelines.Isempty() ) {
            
            ContactIds= new Set<String> () ;
            PeopleSoftIds = new Set<String> () ;
            
            filelines.remove(0);
            csvDetails = new list<wrapper> () ; 
                
                for ( String s :filelines ) {
                
                    String[] filecolumns = new String[]{};
                    filecolumns = s.split(',');
                    
                        wrapper tmp = new wrapper ();
                        tmp.ContactId = filecolumns[0].trim(); 
                        ContactIds.add(tmp.ContactId );
                        //IsValidContactIDTest = true ;
                        tmp.OwnerPeopleSoftId = filecolumns[1].trim();
                        PeopleSoftIds.add(tmp.OwnerPeopleSoftId);
                        //IsValidOwnerIDTest = true;
                         
                    csvDetails.add(tmp); 
                } 
        
        } // If Condition
    }
    
    
    
    
    Public Void SendEmail(String SuccessFile, String ErrorFile, String BackupFile){
        
        EmailId = [Select Id,email from user where id =: userinfo.getuserid() ].email  ;
        
        Messaging.EmailFileAttachment BackupcsvAttc = new Messaging.EmailFileAttachment();
        
        blob BackUpcsvBlob = Blob.valueOf(BackupFile);
        BackupcsvAttc.setFileName('BackUp.csv');
        BackupcsvAttc.setBody(BackUpcsvBlob);
        
        Messaging.EmailFileAttachment SuccesscsvAttc = new Messaging.EmailFileAttachment();
        
        blob SuccesscsvBlob = Blob.valueOf(SuccessFile);
        SuccesscsvAttc.setFileName('Success.csv');
        SuccesscsvAttc.setBody(SuccesscsvBlob);
        
        Messaging.EmailFileAttachment ErrorcsvAttc = new Messaging.EmailFileAttachment();
        
        blob ErrorcsvBlob = Blob.valueOf(ErrorFile);
        ErrorcsvAttc.setFileName('Error.csv');
        ErrorcsvAttc.setBody(ErrorcsvBlob);
         
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        list<string> toAddresses = new list<string> ();
        toAddresses.add(emailid);
        String subject ='Report CSV';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setPlainTextBody('Report CSV ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{SuccesscsvAttc , ErrorcsvAttc, BackupcsvAttc });
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    
    
    }
    
    public class wrapper {
        
        public String    ContactId            {get;set;}
        public String    OwnerPeopleSoftId    {get;set;}
        public Boolean   IsValidContactId     {get;set;}
        public String    SFDCContactId        {get;set;}
        public String    SFDCOwnerId          {get;set;}
        public Boolean   IsValidOwnerId       {get;set;}
        public String    Comments             {get;set;}
        public Boolean   isValid              {get;set;}
        public String    UserName             {get;set;}
        public String    FirstName            {get;set;}
        public String    LastName             {get;set;}
    
    }
    
}