/**
* @Description
* This class is a controller for Apex page which is used as a replacement for standard look up to perform custom search 
* operations
*/

public with sharing class CustomLookupController {
    
    //public Contact contact {get;set;} // new account to create
    public List<Contact> results {get;set;} // search results
    public List<Talent_TS_Modules__c> talentresults {get;set;}
    public string searchString{get;set;} // search keyword
    public string Acctid {get;set;}
    public string objectName {get;set;}
    public string recordId {get;set;}
    public boolean renderContactResults {get;set;}
    public boolean renderTalentResults {get;set;}
    public string selectedValue {get;set;}
    public boolean flag{get;set;}
    //Constructor  
    public CustomLookupController() {
        // get the current search string
        searchString = System.currentPageReference().getParameters().get('lksrch');
        Acctid = System.currentPageReference().getParameters().get('acctid');
        
        objectName = System.currentPageReference().getParameters().get('object');
        recordId = System.currentPageReference().getParameters().get('id');
        renderTalentResults =false;
        renderContactResults = false;
        selectedValue = 'SearchbyName';
         string urlvalue=Apexpages.currentPage().getUrl();
        if(urlvalue!=null){
        if(urlvalue.contains('CEMContacts'))
            flag = false;
        else
            flag= true;
        }
        runSearch();  
    }
    
    // performs the keyword search
    public PageReference search() {
        
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        system.debug('-->Malini'+searchString);
        if(searchString.contains('\'')){
            String toBeReplace = '\'';
            String replaceWith = '\\\'';
            searchString = searchString.replace(toBeReplace , replaceWith );
        }
        if(objectName == 'talenttsmodule'){
            renderTalentResults = true;
            talentresults = performtalenttsmoduleSearch(searchString); 
        }
        else if(objectName == 'clientproductcontact'){
            renderContactResults = true;
            results = performclientproductcontactSearch(searchString); 
        }
        else {
            renderContactResults = true;
            results = performSearch(searchString);                
        }
    } 
    
    //Method to perform search on Contacts: This is used in CEM
    private List<Contact> performSearch(string searchString) {
        // Prepare query string for complex serarches & prevent injections
        //String soql = 'select id, name,title,phone,email from contact where Accountid= \''+Acctid+'\'';
        String soql = 'select id, name,title,phone,email from contact where Contact_Status__c=\'Active\'';
        if(searchString != '' && searchString != null && selectedValue == 'SearchbyName')
        {
            soql = soql + ' AND name LIKE \'%' + searchString +'%\'';
        }
       /* else if(searchString != '' && searchString != null && selectedValue == 'SearchbyGlobalItemNumber')
        {
            soql = soql + ' AND Global_Item_Number__c LIKE \'%' + searchString +'%\'';  
        }*/
        else
        {
            soql = soql + ' limit 25';
        }
        System.debug('Query generated:' + soql);
        return database.query(soql); 
    }
    
    //Method to perform search on Talent TS Module: This is used in Talent TS  
    private List<Talent_TS_Modules__c> performtalenttsmoduleSearch(string searchString) {
        
        Talent_Contract__c objTc = [select id,Product__c from Talent_Contract__c where id=:recordId];
        String soql = 'select id, name,Global_Item_Number__c,Product_Year__c,Region__c from Talent_TS_Modules__c where Product_Name__c= \''+objTc.Product__c+'\'';
        
        if(searchString != '' && searchString != null && selectedValue == 'SearchbyName' ) 
        {
            soql = soql + ' and Name LIKE \'%' + searchString +'%\'';
        }
        else if(searchString != '' && searchString != null && selectedValue == 'SearchbyGlobalItemNumber')
        {
            soql = soql + ' and Global_Item_Number__c LIKE \'%' + searchString +'%\'';  
            
        }
        else
        {
            soql = soql + ' limit 25';  
        }
        System.debug(soql);
        
        List<Talent_TS_Modules__c> lstTalentRecords = database.query(soql);
        if(lstTalentRecords.size()>1000){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Too many results for given criteria. Please enter more parameters.'));
            return null;
        }
        
        return lstTalentRecords; 
        
    }
    
    //Method to perform search on Contacts: This is used in client product contact search in Talent TS
    private List<Contact> performclientproductcontactSearch(string searchString) {
        Talent_Contract__c objTc = [select id,Account__c from Talent_Contract__c where id=:recordId];
        String soql = 'select id, name,title,phone,email from Contact where Accountid= \''+objTc.Account__c+'\'';
        if(searchString != '' && searchString != null && selectedValue == 'SearchbyName') {
            soql = soql + ' and Name LIKE \'%' + searchString +'%\'';
            
        }
        /*else if(searchString != '' && searchString != null && selectedValue != 'SearchbyName') {
soql = soql + ' and Global_Item_Number__c LIKE \'%' + searchString +'%\'';

}*/
        else{
            soql = soql + ' limit 25';  
        }
        System.debug(soql);
        List<Contact> lstContactRecords = database.query(soql);
        if(lstContactRecords.size()>1000){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Too many results for given criteria. Please enter more parameters.'));
            return null;
        }
        return lstContactRecords; 
    }
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
}