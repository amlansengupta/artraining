/**************************************************************************************************************
User story : 
Purpose : This test class is created for the MF2_HomePageCustomLinksController class.
Created by : Soumil Dasgupta
Created Date : 17/12/2018
Project : MF2
****************************************************************************************************************/
@isTest(seeAllData = false)
public class MF2_HomePageCustomLinksControllerTest {

    public static testmethod void customLinksTestPositive(){
        HomePageQuickLinks__c customLinks = new HomePageQuickLinks__c();
        customLinks.Display_In_New_Window__c = true;
        customLinks.IsActive__c = true;
        customLinks.Link__c = 'abc';
        customLinks.Name = 'abc';
        customLinks.Place__c = 'Home Page';
        customLinks.UI_Label__c = 'abc';
        insert customLinks;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            MF2_HomePageComponentController.fetchCustomLinks('all');
        }
    }
    
    public static testmethod void customLinksTestNegative(){
        HomePageQuickLinks__c customLinks = new HomePageQuickLinks__c();
        customLinks.Display_In_New_Window__c = true;
        customLinks.IsActive__c = true;
        customLinks.Link__c = 'abc';
        customLinks.Name = 'abc';
        customLinks.Place__c = 'Home Page';
        customLinks.UI_Label__c = 'abc';
        insert customLinks;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            MF2_HomePageComponentController.fetchCustomLinks('abc');
        }
    }
    
    public static testmethod void alertsMessages(){
        List<String> customLinks = new List<String>();
        HomePageQuickLinks__c link=new HomePageQuickLinks__c();
        link.Type__c='ALERT';
        link.UI_Label__c='abc';
        link.Name = 'abc';
        link.Place__c = 'Home Page';
        insert link;
        
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
        System.runAs(user1) {
            MF2_HomePageComponentController.fetchHomePageAlertsMessages();
        }
    }
}