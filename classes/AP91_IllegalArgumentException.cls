/*Purpose:   This apex class is written for handling errors in  Product Search Pagination 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 2940)Created Apex class 
============================================================================================================================================== 
*/  
global class AP91_IllegalArgumentException extends Exception {}