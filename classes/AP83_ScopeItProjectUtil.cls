/*Purpose:   This apex class calculates the LOB margin for each LOB on Opportunity
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 3729)Created Apex class 
   2.0      Madhavi     10/22/2014  As per December 2014 release(Request #4881) ,updated class to add IC charges to opportunity.
============================================================================================================================================== 
*/
public class AP83_ScopeItProjectUtil {
    
    /*
     * @Description : This method calculates the LOB margin for each LOB on Opportunity 
     * @ Args       : scopeItProjectList   
     * @ Return     : void
     */
    public static void scopeItProject(List < ScopeIt_Project__c > scopeItProjectList) {
        try {
        Decimal profit_Loss_BA = 0;
        Decimal profit_Loss_CBO = 0;
        Decimal profit_Loss_EHB = 0;
        Decimal profit_Loss_INV = 0;
        Decimal profit_Loss_RET = 0;
        Decimal profit_Loss_TAL = 0;
        Decimal profit_Loss_GBS = 0;
        Decimal profit_Loss_OTH = 0;
                
        Decimal margin_BA = 0;
        Decimal margin_CBO = 0;
        Decimal margin_EHB = 0;
        Decimal margin_INV = 0;
        Decimal margin_RET = 0;
        Decimal margin_TAL = 0;
        Decimal margin_GBS = 0;
        Decimal margin_OTH = 0;
        

        Set < Id > oppIdSet = new set < ID > ();
        List < Opportunity > oppToUpdate = new List < Opportunity > ();
        Map < Opportunity, List < ScopeIt_Project__c >> opportunityMap = new Map < Opportunity, List < ScopeIt_Project__c >> ();
        for (ScopeIt_Project__c sp: scopeItProjectList) {
            oppIdSet.add(sp.Opportunity__c);
        }
        
        Map < Id, Opportunity > oppMap = new Map < Id, Opportunity > (
                [Select id, Name, Cost__c, Margin__c, Profit_Loss__c,Billable_Time_Charges__c,Non_Billable_Time_Charges__c,  (Select id, 
                s.LOB__c, s.Product__r.LOB__c, s.Opportunity__c, s.Sales_Price__c, s.cost__c, s.Profit_Loss__c,s.Billable_Time_Charges__c,
                s.Non_Billable_Time_Charges__c,s.Non_Billable_Expenses__c,IC_Charges__c,
                Margin__c From ScopeIt_Projects__r s) from Opportunity where Id IN: oppIdSet]);

        for (ID oppId: oppMap.keySet()) {
            Opportunity opp = oppMap.get(oppId);
            List < ScopeIt_Project__c > spList = new List < ScopeIt_Project__c > ();
            /*for (ScopeIt_Project__c sp: opp.ScopeIt_Projects__r) {
                spList.add(sp);
            }*/
            spList.addAll(opp.ScopeIt_Projects__r);
            opportunityMap.put(oppMap.get(oppId), spList);
        }

        for (Opportunity tempOpp: opportunityMap.keySet()) {
            Decimal netPrice_BA = 0;
            Decimal netPrice_CBO = 0;
            Decimal netPrice_EHB = 0;
            Decimal netPrice_INV = 0;
            Decimal netPrice_RET = 0;
            Decimal netPrice_TAL = 0;
            Decimal netPrice_GBS = 0;
            Decimal netPrice_OTH = 0;
            

            Decimal cost_BA = 0;
            Decimal cost_CBO = 0;
            Decimal cost_EHB = 0;
            Decimal cost_INV = 0;
            Decimal cost_RET = 0;
            Decimal cost_TAL = 0;
            Decimal cost_GBS = 0;
            Decimal cost_OTH = 0;
            
            Decimal non_Billable_Expense_BA=0;
            Decimal non_Billable_Expense_CBO=0;
            Decimal non_Billable_Expense_EHB=0;
            Decimal non_Billable_Expense_INV=0;
            Decimal non_Billable_Expense_RET=0;
            Decimal non_Billable_Expense_TAL=0;
            Decimal non_Billable_Expense_GBS=0;
            Decimal non_Billable_Expense_OTH=0;
            
            Decimal non_bill_charge_Sum = 0; 
            Decimal bill_charge_Sum = 0; 
            Decimal cost_Sum = 0;
            Decimal ICcharges_sum = 0;//Added as per December 2014 release(Request #4881)

            
            
            List < ScopeIt_Project__c > scopeItList = new List < ScopeIt_Project__c > ();
            scopeItList = opportunityMap.get(tempOpp);
            for (ScopeIt_Project__c scp: scopeItList) {
                 
                 


                //Sumation of Net Price and Cost
                if(scp.LOB__c!= null) {  
                    if (ConstantsUtility.STR_BenefitsAdmin.equals(scp.LOB__c)) {
                        netPrice_BA += (Decimal) scp.Sales_Price__c;
                        cost_BA += (Decimal) scp.cost__c;
                        non_Billable_Expense_BA += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_CrossBusinessOfferingsCBO.equals(scp.LOB__c)) {
                        netPrice_CBO += (Decimal) scp.Sales_Price__c;
                        cost_CBO += (Decimal) scp.cost__c;
                        non_Billable_Expense_CBO += scp.Non_Billable_Expenses__c;
                       
                    }
                    if (ConstantsUtility.STR_EmployeeHealthBenefits.equals(scp.LOB__c) ) {
                        netPrice_EHB += (Decimal) scp.Sales_Price__c;
                        cost_EHB += (Decimal) scp.cost__c;
                        non_Billable_Expense_EHB += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_Investments.equals(scp.LOB__c) ) {
                        netPrice_INV += (Decimal) scp.Sales_Price__c;
                        cost_INV += (Decimal) scp.cost__c;
                        non_Billable_Expense_INV += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_Retirement.equals(scp.LOB__c)) {
                        netPrice_RET += (Decimal) scp.Sales_Price__c;
                        cost_RET += (Decimal) scp.cost__c;
                        non_Billable_Expense_RET += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_Talent.equals(scp.LOB__c)) {
                        netPrice_TAL += (Decimal) scp.Sales_Price__c;
                        cost_TAL += (Decimal) scp.cost__c;  
                        non_Billable_Expense_TAL += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_GlobalBusinessSolutions.equals(scp.LOB__c)) {
                        netPrice_GBS += (Decimal) scp.Sales_Price__c;
                        cost_GBS += (Decimal) scp.cost__c;  
                        non_Billable_Expense_GBS += scp.Non_Billable_Expenses__c;
                    }
                    if (ConstantsUtility.STR_Other.equals(scp.LOB__c)) {
                        netPrice_OTH += (Decimal) scp.Sales_Price__c;
                        cost_OTH += (Decimal) scp.cost__c;  
                        non_Billable_Expense_OTH += scp.Non_Billable_Expenses__c;
                    }

                    
                    bill_charge_Sum += scp.Billable_Time_Charges__c;
                    non_bill_charge_Sum += scp.Non_Billable_Time_Charges__c;
                    

                    cost_Sum += scp.Cost__c + scp.Non_Billable_Expenses__c;
                    ICcharges_sum+=scp.IC_Charges__c;//Added as per December 2014 release(Request #4881)
                    
                }
            }
            
           
            
            tempOpp.Margin_BA__c = 0;
            tempOpp.Margin_RET__c = 0;
            tempOpp.Margin_INV__c = 0;
            tempOpp.Margin_TAL__c = 0;
            tempOpp.Margin_EHB__c = 0;
            tempOpp.Margin_CBO__c = 0;
            tempOpp.Margin_GBS__c = 0;
            tempOpp.Margin_OTH__c = 0;
            
            //if(non_bill_charge_Sum > 0)
            tempOpp.Non_Billable_Time_Charges__c = non_bill_charge_Sum;
            
            //if(bill_charge_Sum > 0)
            tempOpp.Billable_Time_Charges__c = bill_charge_Sum;
            
            tempOpp.Cost2__c=cost_Sum;
            tempOpp.IC_Charges__c =ICcharges_sum;//Added as per December 2014 release(Request #4881)
            //Calculating Margin for LOBs and checking its value 
            if (netPrice_BA != 0 && cost_BA != 0) {
                profit_Loss_BA = netPrice_BA - cost_BA - non_Billable_Expense_BA;
                margin_BA = (profit_Loss_BA / netPrice_BA) * 100;

                tempOpp.Margin_BA__c = Math.round(margin_BA);
            }
            if (netPrice_CBO != 0 && cost_CBO != 0) {
                profit_Loss_CBO = netPrice_CBO - cost_CBO - non_Billable_Expense_CBO;
                margin_CBO = (profit_Loss_CBO / netPrice_CBO) * 100;

                tempOpp.Margin_CBO__c = Math.round(margin_CBO);
            }
            if (netPrice_EHB != 0 && cost_EHB != 0) {
                profit_Loss_EHB = netPrice_EHB - cost_EHB - non_Billable_Expense_EHB;
                margin_EHB = (profit_Loss_EHB / netPrice_EHB) * 100;

                tempOpp.Margin_EHB__c = Math.round(margin_EHB);
            }
            if (netPrice_INV != 0 && cost_INV != 0) {
                profit_Loss_INV = netPrice_INV - cost_INV - non_Billable_Expense_INV;
                margin_INV = (profit_Loss_INV / netPrice_INV) * 100;

                tempOpp.Margin_INV__c = Math.round(margin_INV);
            }
            if (netPrice_RET != 0 && cost_RET != 0) {
                profit_Loss_RET = netPrice_RET - cost_RET - non_Billable_Expense_RET;
                margin_RET = (profit_Loss_RET / netPrice_RET) * 100;

                tempOpp.Margin_RET__c = Math.round(margin_RET);
            }
            if (netPrice_TAL != 0 && cost_TAL != 0) {
                profit_Loss_TAL = netPrice_TAL - cost_TAL - non_Billable_Expense_TAL;
                margin_TAL = (profit_Loss_TAL / netPrice_TAL) * 100;

                tempOpp.Margin_TAL__c = Math.round(margin_TAL);
            }
            if (netPrice_GBS != 0 && cost_GBS != 0) {
                profit_Loss_GBS = netPrice_GBS - cost_GBS - non_Billable_Expense_GBS;
                margin_GBS = (profit_Loss_GBS / netPrice_GBS) * 100;

                tempOpp.Margin_GBS__c = Math.round(margin_GBS);
            }
            if (netPrice_OTH != 0 && cost_OTH != 0) {
                profit_Loss_OTH = netPrice_OTH - cost_OTH - non_Billable_Expense_OTH;
                margin_OTH = (profit_Loss_OTH / netPrice_OTH ) * 100;

                tempOpp.Margin_OTH__c = Math.round(margin_OTH);
            }



            oppToUpdate.add(tempOpp);
        }
        if (!oppToUpdate.isEmpty()) {
            // If updating LOB in Opportunity, For product Expansion SOQL101 occurs.
            if(OppCloneWithProductLtngController.skipOppTrig){
            AP44_ChatterFeedReporting.FROMFEED = true;
            }
            Database.update(oppToUpdate);           
        }
        } catch(Exception ex){
            for(ScopeIt_Project__c sp : scopeItProjectList)
            {
                sp.adderror('Operation failed due to :'+ex.getMessage()+'. Please Contact your Administrator.');
            }
         }
    }
}