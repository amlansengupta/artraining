/*Handler Class for ContactTeam Object*/
public class ContactTeamTriggerHandler implements ITriggerHandler
{
    // Allows unit tests (or other code) to disable this trigger for the transactionhttps://mercer--lightning.cs71.my.salesforce.com/_ui/common/apex/debug/ApexCSIPage#
    public static Boolean TriggerDisabled = false;
    
    /*
Checks to see if the trigger has been disabled either by custom setting or by running code
*/
    private static boolean beforeInsertFirstRun = true;
    private static boolean beforeUpdateFirstRun = true;
    private static boolean beforeDeleteFirstRun = true;
    private static boolean afterInsertFirstRun = true;
    private static boolean afterUpdateFirstRun = true;
    private static boolean afterDeleteFirstRun = true;
    private static boolean afterUndeleteFirstRun = true;
    
    public Boolean IsDisabled()
    {
        TriggerSettings__c TrgSetting= TriggerSettings__c.getValues('Contact Team Trigger');
        system.debug('hi there :'+TrgSetting);
        //if (TrgSetting.Trigger_Disabled__c== true) { //commented by AG - 06-Sep-2018
        if (TrgSetting !=null && TrgSetting.Trigger_Disabled__c== true) 
        {
            return true;
        }
        else
        {
            return TriggerDisabled;
        }
    }
    public void BeforeInsert(List<SObject> newItems)
    {
        if(beforeInsertFirstRun)
        {
                    AP56_ContactTeamTriggerUtil.processContactTeambeforeInsert(newItems);
            beforeInsertFirstRun=false;
        }
    }
    public void BeforeUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap)
    {
        System.debug('i m in ctBeforeUpdate ');
        if(beforeUpdateFirstRun)
        {
            System.debug('i m in ctBeforeUpdate if');
            beforeUpdateFirstRun=false;
        }
        
    }
    public void BeforeDelete(Map<Id, SObject> oldItemsMap)
    {
        if(beforeDeleteFirstRun)
        {
            if(!AP19_ContactTriggerUtil.isMergeProcess)
          {
            AP56_ContactTeamTriggerUtil.processContactTeambeforeDelete(oldItemsMap.values());   
          }
            beforeDeleteFirstRun=false;
        }
        
    }
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemsMap)
    {
        if(afterInsertFirstRun)
        {
            //AP56_ContactTeamTriggerUtil.processContactTeambeforeInsert(newItems);
            AP56_ContactTeamTriggerUtil.concatenatedContactTeam(newItems);  
            
            afterInsertFirstRun=false;
        }
        
    }
    public void AfterUpdate(Map<Id, SObject> newItemsMap, Map<Id, SObject> oldItemsMap)
    { 
        if(afterUpdateFirstRun)
        {
            AP56_ContactTeamTriggerUtil.concatenatedContactTeam(newItemsMap.values());
            
            afterUpdateFirstRun=false;
        }
        
    }
    public void AfterDelete(Map<Id, SObject> oldItemsMap)
    {
        if(afterDeleteFirstRun)
        {
            AP56_ContactTeamTriggerUtil.concatenatedContactTeam(oldItemsMap.values());
            
            afterDeleteFirstRun=false;
        }
        
        
    }
    public void AfterUndelete(Map<Id, SObject> oldItemsMap)
    {
        if(afterUndeleteFirstRun)
        {
            afterUndeleteFirstRun=false;
        }
    }
    
}