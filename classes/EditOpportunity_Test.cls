/*
==============================================================================================================================================
Request Id                                			 Date                    Modified By
12638:Removing Step 								 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest
private class EditOpportunity_Test {

    static testMethod void editTest(){
        
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        ApexConstants__c setting1 = new ApexConstants__c();
        setting1.Name = 'Selected';
        setting1.StrValue__c = '';
        insert setting1;
        
        ApexConstants__c setting2 = new ApexConstants__c();
        setting2.Name = 'Identify';
        setting2.StrValue__c = '';
        insert setting2;
        
        ApexConstants__c setting3 = new ApexConstants__c();
        setting3.Name = 'Active Pursuit';
        setting3.StrValue__c = '';
        insert setting3;
        
         /*Request No. 17953 : Removing Finalist START
        ApexConstants__c setting4 = new ApexConstants__c();
        setting4.Name = 'Finalist';
        setting4.StrValue__c = '';
        insert setting4;
        Request No. 17953 : Removing Finalist END */
        
        ApexConstants__c setting5 = new ApexConstants__c();
        setting5.Name = 'AP02_OpportunityTriggerUtil_1';
        setting5.StrValue__c = '';
        insert setting5;
        
        ApexConstants__c setting6 = new ApexConstants__c();
        setting6.Name = 'AP02_OpportunityTriggerUtil_2';
        setting6.StrValue__c = '';
        insert setting6;
        
        ApexConstants__c setting7 = new ApexConstants__c();
        setting7.Name = 'ScopeITThresholdsList';
        setting7.StrValue__c = '';
        insert setting7;
        
        Account testAcct = new Account(Name = 'My Test Account');
        insert testAcct;
                    
        Opportunity oppt = new Opportunity(); 
        oppt.Name='Testoppt';
        oppt.AccountId=testAcct.id;
        //request id:12638 commenting step
        //oppt.step__c='Identified Deal';
        oppt.StageName='Identify';
        oppt.CloseDate=date.Today();
        oppt.Opportunity_Office__c='Minneapolis - South Seventh';
        oppt.CurrencyIsoCode = 'ALL';
        oppt.Type='New Client';
        oppt.Account_Region__c = 'EuroPac';
        insert oppt;  
 
        
        ApexPages.StandardController cntrl=new ApexPages.StandardController(oppt);
        EditOpportunity edopp=new EditOpportunity(cntrl);
        edopp.edit();
        edopp.save();
        edopp.saveAndReturn();
        edopp.doCancel();
        edopp.Cancel();
        
    }

}