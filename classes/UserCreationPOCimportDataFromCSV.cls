public class UserCreationPOCimportDataFromCSV {
    
    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    public String[] csvFileLines{get;set;}
    public string[] csvRecordData{get;set;}
    public List<User> acclist{get;set;}
    public List<String> colleaguelist{get;set;}
    public List<User> Userlist{get;set;}
    public List<User> finalUserUpdatelist{get;set;}
    public List<User> finalUserInsertlist{get;set;}
    public List<String> UserInsertlist{get;set;}
    Public List<Colleague__c> results  = new List<Colleague__c>();
    public List<String> ErrorInsertlist{get;set;}
    public List<String> ErrorUpdatelist{get;set;}
    public List<String> SuccessInsertlist{get;set;}
    public List<String> SuccessUpdatelist{get;set;}
    public List<User> SuccessInsertDisp{get;set;}
    public List<User> SuccessUpdateDisp{get;set;}
    public String Fname {get;set;}
    public String Lname {get;set;}
    public String Enumber {get;set;}
    public String Wmail {get;set;}
    Public String Cname{get;set;}
    Set<String> setOfEmpNo{get;set;} 
    String idResult{get;set;}
    Map<String,Colleague__c> MapCollegueId_EmpId ;
    Map<String,String> MapUserIsActive ;
    Map<String,String> MapUserRequestNo ;
    Map<String,String> MapUserRole ;
    Map<String,String> MapUserProfile ;
    Map<String,String> MapUserLocale ;
    Map<String,String> MapUserlanguage ;
    Map<String,String> MapUserTimeZone ;
    Map<String,String> MapUserCurrency ;
    Map<String,String> MapUserRequestor ;
    Map<String,String> MapUserGoals ;
    Map<String,String> MapUserMarketingUser ;
    Map<String,Id> MapUserRoleInsert ;
    Map<String,Id> MapUserProfileInsert ;
    Map<String,Id> MapUserRequestorInsert ;
    Map<String,String> MapUserLocaleInsert ;
    Map<String,String> MapUserLanguageInsert ;
    Map<String,Id> MapUserRoleInsert1 ;
    Map<String,Id> MapUserProfileInsert1 ;
    Map<String,String> MapUserLocaleInsert1 ;
    Map<String,String> MapUserLanguageInsert1 ;
    Map<String,Id> MapUserRequestorInsert1 ;
    
    public UserCreationPOCimportDataFromCSV (){
        csvFileLines = new String[]{};
        acclist = New List<User>(); 
        colleaguelist = New List<String>();
        Userlist = New List<User>();
        UserInsertlist = New List<String>();
        SuccessUpdatelist = New List<String>();
       // SuccessUpdatelist = null;
        ErrorUpdatelist = New List<String>();
       // ErrorUpdatelist = null;
        SuccessInsertlist = New List<String>();
       // SuccessInsertlist = null;
        ErrorInsertlist = New List<String>();
       // ErrorInsertlist = null;
    }
    
    public void importCSVFile(){
        SuccessUpdatelist.clear();
        ErrorUpdatelist.clear();
        SuccessInsertlist.clear();
        ErrorInsertlist.clear();
        colleaguelist = New List<String>();
        Userlist = New List<User>();
        UserInsertlist = New List<String>();
        MapUserIsActive = New Map<String,String> ();
        MapUserRequestNo = New Map<String,String> ();
        MapUserRole  = New Map<String,String> ();
        MapUserProfile = New Map<String,String> ();
        MapUserLocale = New Map<String,String> ();
        MapUserlanguage = New Map<String,String> ();
        MapUserTimeZone = New Map<String,String> (); 
        MapUserCurrency = New Map<String,String> ();
        MapUserRequestor = New Map<String,String> ();
        MapUserGoals = New Map<String,String> ();
        MapUserMarketingUser = New Map<String,String> ();
        MapCollegueId_EmpId = New Map<String,Colleague__c> ();
        results = new List<Colleague__c>();
        try{
            setOfEmpNo = new Set<String>();
            csvAsString = csvFileBody.toString();
            csvFileLines = csvAsString.split('\n'); 
            
            for(Integer i=1;i<csvFileLines.size();i++){
                
                csvRecordData = csvFileLines[i].split(',');
                Fname= csvRecordData[0] ; 
                LName= csvRecordData[1] ;             
                Wmail= csvRecordData[2];
                Enumber = csvRecordData[3];
                
                FName    =    Fname.trim() ;
                LName    =    LName.trim() ;
                Wmail    =    Wmail.trim() ;
                Enumber  =    Enumber.trim() ;
                
                setOfEmpNo.add(Enumber);
                MapUserIsActive.put(Enumber,csvRecordData[4]);
                MapUserRequestNo.put(Enumber,csvRecordData[5]);
                MapUserRole.put(Enumber,csvRecordData[6]);
                MapUserProfile.put(Enumber,csvRecordData[7]);
                MapUserLocale.put(Enumber,csvRecordData[8]);
                MapUserlanguage.put(Enumber,csvRecordData[9]);
                MapUserTimeZone.put(Enumber,csvRecordData[10]);
                MapUserCurrency.put(Enumber,csvRecordData[11]);
                MapUserRequestor.put(Enumber,csvRecordData[12]);
                MapUserGoals.put(Enumber,csvRecordData[13]);
                //MapUserMarketingUser.put(Enumber,csvRecordData[14]);
                //system.debug('%%%'+MapUserMarketingUser.get(Enumber));
            } //for loop ends here
            
            
            
            //results = new List<Colleague__c>();
            
            //MapCollegueId_EmpId = New Map<String,Colleague__c> ();
            
            results = [SELECT   Name, Email_Address__c, Empl_Status__c, EMPLID__c  
                       FROM Colleague__c 
                       WHERE  ( EMPLID__c = :setOfEmpNo AND EMPLID__c != Null )  
                       ORDER BY First_Name__c , Last_Name__c ];
                       
            //AND Empl_Status__c ='Active'
            
        } //try part ends here
        
        catch (Exception e)
        {
            ApexPages.addMessages(e);
        }
        
        if ( results.isEmpty() ) { 
            
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'No colleague record present for the input or the loadfile is Empty');
            ApexPages.addMessage(errorMessage);
            
        } else {                             
            for ( Colleague__c c :results  ) {                   
                if ( c.EMPLID__c <> Null )                      
                    MapCollegueId_EmpId.Put(c.EMPLID__c,c);
            }  
        }   
        
        for(String col:setOfEmpNo){
            
            if(!MapCollegueId_EmpId.containsKey(col)){
                colleaguelist.add(col); 
            }
        }      
        
        List<User> User = New List<User> ();
        User = [ Select Id, Name, EmployeeNumber  FROM User WHERE EmployeeNumber = : MapCollegueId_EmpId.keyset() ]  ;
        
        Map<string,user> UserMap = new Map<string,user>();
        system.debug('Total users fetched '+User.size());
        if(User.size()>0){
            for(Integer i=0;i<User.size();i++){
                UserMap.Put(User[i].EmployeeNumber,user[i]);
            }
        }
        for(String colemp:MapCollegueId_EmpId.keyset()){
            
            if(UserMap.containsKey(colemp)){
                Userlist.add(UserMap.get(colemp));             
            }
            else{
                UserInsertlist.add(colemp);                
            }
        }
        userUpdate(Userlist) ;  
        MapCollegueDetailstoUser( QueryColleagueAllDetails ( UserInsertlist ) ) ;   
    }
    
    Public void userUpdate( List<User> userRecord ){
        finalUserUpdatelist = New List<User>();
        List<UserRole> usrRoleId = new List<UserRole>();
        List<String> usrRoleName = new List<String>();
        List<Profile> profileId = new List<Profile>();
        List<String> profileName = new List<String>();
        List<User> usrRequestorId = new List<User>();
        List<String> usrRequestorEmplNo = new List<String>();
        List<String> usrLocaleName = new List<String>();
        List<String> usrLanguageName = new List<String>();
        List<UserCreationKey__c> usrLocale = new List<UserCreationKey__c>();
        List<UserCreationKey__c> usrLanguage = new List<UserCreationKey__c>();
        MapUserRoleInsert = new Map<String,Id>(); 
        MapUserProfileInsert = new Map<String,Id>();
        MapUserrequestorInsert = new Map<String,Id>();
        MapUserLocaleInsert = new Map<String,String>();
        MapUserLanguageInsert = new Map<String,String>();
        
        for(User usr:userRecord ){
            usrRoleName.add(MapUserRole.get(usr.EmployeeNumber));
            profileName.add(MapUserProfile.get(usr.EmployeeNumber));
            usrRequestorEmplNo.add(MapUserRequestor.get(usr.EmployeeNumber));
            usrLocaleName.add(MapUserLocale.get(usr.EmployeeNumber));
            usrLanguageName.add(MapUserlanguage.get(usr.EmployeeNumber));
            }
        
        usrRoleId =  [Select Id,Name from UserRole where Name =:usrRoleName];
        profileId =  [Select Id,Name from Profile where Name =:profileName];
        usrRequestorId = [Select Id,EmployeeNumber from User where EmployeeNumber =:usrRequestorEmplNo];
        usrLocale = [Select Id, LOCALESIDKEY__c,Locale_values__c from UserCreationKey__c where Locale_values__c = :usrLocaleName];
        usrLanguage = [Select Id, LANGUAGELOCALEKEY__c,Language__c from UserCreationKey__c where Language__c = :usrLanguageName];
        
        for(UserRole usrrole:usrRoleId ){
            MapUserRoleInsert.put(usrrole.Name,usrrole.Id); 
        }
        for(Profile prfl:profileId ){
            MapUserProfileInsert.put(prfl.Name,prfl.Id); 
        }
        for(User rqstr:usrRequestorId ){
            MapUserrequestorInsert.put(rqstr.EmployeeNumber,rqstr.Id); 
        }
        for(UserCreationKey__c uck: usrLocale ){
            MapUserLocaleInsert.put(uck.Locale_values__c,uck.LOCALESIDKEY__c); 
        }
        for(UserCreationKey__c uck: usrLanguage ){
            MapUserLanguageInsert.put(uck.Language__c,uck.LANGUAGELOCALEKEY__c); 
        }
        
        for(User usr:userRecord ){
            User userObj = new User() ;
            if(MapUserIsActive.containsKey(usr.EmployeeNumber)){
                userObj.id = usr.Id; 
                //userObj.UserPermissionsMarketingUser = False;
                userObj.mh_Blue_Sheet_Read_Write_License__c = True;
                userObj.Green_Sheet_Read_Write_License__c = True;
                userObj.Gold_Sheet_Read_Write_License__c = True;
                userObj.Welcome_Email_Sent__c = True;
                userObj.IsActive = Boolean.ValueOf(MapUserIsActive.get(usr.EmployeeNumber));  
                userObj.Request__c = MapUserRequestNo.get(usr.EmployeeNumber);
                userObj.EmailEncodingKey = 'ISO-8859-1';
                if(MapUserRole.get(usr.EmployeeNumber) != ''){
                    userObj.UserRoleId = MapUserRoleInsert.get(MapUserRole.get(usr.EmployeeNumber));
                }
                if(MapUserProfile.get(usr.EmployeeNumber) != ''){    
                    userObj.ProfileId = MapUserProfileInsert.get(MapUserProfile.get(usr.EmployeeNumber));
                }
                if(MapUserLocale.get(usr.EmployeeNumber) != ''){ 
                    userObj.LocaleSidKey = MapUserLocaleInsert.get(MapUserLocale.get(usr.EmployeeNumber));
                }
                //system.debug('&&& '+MapUserlanguage.get(usr.EmployeeNumber));
                //system.debug('&&& '+MapUserLanguageInsert.get(MapUserLanguage.get(usr.EmployeeNumber)));
                if(MapUserlanguage.get(usr.EmployeeNumber) != ''){ 
                    userObj.LanguageLocaleKey = MapUserLanguageInsert.get(MapUserLanguage.get(usr.EmployeeNumber));
                    system.debug('&&& '+userObj.LanguageLocaleKey);
                }
                if(MapUserTimeZone.get(usr.EmployeeNumber) != ''){ 
                    userObj.TimeZoneSidKey = MapUserTimeZone.get(usr.EmployeeNumber);
                }
                if(MapUserCurrency.get(usr.EmployeeNumber) != ''){     
                    userObj.DefaultCurrencyIsoCode = MapUserCurrency.get(usr.EmployeeNumber);
                }
                if(MapUserRequestor.get(usr.EmployeeNumber) != ''){    
                    userObj.Collegue__c = MapUserrequestorInsert.get(MapUserRequestor.get(usr.EmployeeNumber));         
                }
                if(MapUserGoals.get(usr.EmployeeNumber) == 'Yes'){ 
                    userObj.Goals__c = 'Sales';
                }
            }
            finalUserUpdatelist.add(userObj);       
        }
    }
    
    Public void MapCollegueDetailstoUser( List<Colleague__c> colResult  ){
        finalUserInsertlist = New List<User>();
        List<UserRole> usrRoleId1 = new List<UserRole>();
        List<String> usrRoleName1 = new List<String>();
        List<Profile> profileId1 = new List<Profile>();
        List<String> profileName1 = new List<String>();
        List<User> usrRequestorId1 = new List<User>();
        List<String> usrRequestorEmplNo1 = new List<String>();
        MapUserRoleInsert1 = new Map<String,Id>(); 
        MapUserProfileInsert1 = new Map<String,Id>();
        MapUserrequestorInsert1 = new Map<String,Id>();
        List<String> usrLocaleName1 = new List<String>();
        List<String> usrLanguageName1 = new List<String>();
        List<UserCreationKey__c> usrLocale1 = new List<UserCreationKey__c>();
        List<UserCreationKey__c> usrLanguage1 = new List<UserCreationKey__c>();
        MapUserLocaleInsert1 = new Map<String,String>();
        MapUserLanguageInsert1 = new Map<String,String>();
        
        for(Colleague__c col:colResult ){
            usrRoleName1.add(MapUserRole.get(col.EMPLID__c));
            profileName1.add(MapUserProfile.get(col.EMPLID__c));
            usrRequestorEmplNo1.add(MapUserRequestor.get(col.EMPLID__c));
            usrLocaleName1.add(MapUserLocale.get(col.EMPLID__c));
            usrLanguageName1.add(MapUserlanguage.get(col.EMPLID__c));
            }
            
        usrRoleId1 =  [Select Id,Name from UserRole where Name =:usrRoleName1];
        profileId1 =  [Select Id,Name from Profile where Name =:profileName1];
        usrRequestorId1 = [Select Id,EmployeeNumber from User where EmployeeNumber =:usrRequestorEmplNo1];
        usrLocale1 = [Select Id, LOCALESIDKEY__c,Locale_values__c from UserCreationKey__c where Locale_values__c = :usrLocaleName1];
        usrLanguage1 = [Select Id, LANGUAGELOCALEKEY__c,Language__c from UserCreationKey__c where Language__c = :usrLanguageName1];
        
        
        for(UserRole usrrole:usrRoleId1 ){
            MapUserRoleInsert1.put(usrrole.Name,usrrole.Id); 
        }
        for(Profile prfl:profileId1 ){
            MapUserProfileInsert1.put(prfl.Name,prfl.Id); 
        }
        for(User rqstr:usrRequestorId1 ){
            MapUserrequestorInsert1.put(rqstr.EmployeeNumber,rqstr.Id); 
        }
        for(UserCreationKey__c uck: usrLocale1 ){
            MapUserLocaleInsert1.put(uck.Locale_values__c,uck.LOCALESIDKEY__c); 
        }
        for(UserCreationKey__c uck: usrLanguage1 ){
            MapUserLanguageInsert1.put(uck.Language__c,uck.LANGUAGELOCALEKEY__c); 
        }
        
        for(Colleague__c result :colResult){
            User userObj1 = new User();
            if ( result  <> null ) {
                
                String SubRegion = result.Sub_Region__c;
                String SubMarket = result.Sub_Market__c;
                String Region = result.Region__c; 
                String CusFName;
                String CusLName;
                
                if (result.Prf_Last_Name__c == null)
                {
                    LName = result.Last_Name__c;
                    CusLName = result.Last_Name__c;
                }
                else{
                    LName = result.Prf_Last_Name__c;
                    CusLName = result.Prf_Last_Name__c;
                }
                if (result.Prf_First_Name__c == null)
                {
                    FName = result.First_Name__c;
                    CusFName = result.First_Name__c;
                } 
                else{
                    FName= result.Prf_First_Name__c;
                    CusFName = result.Prf_First_Name__c;
                }
                
                String EmpID = result.EMPLID__c;
                String Emailadd = result.Email_Address__c;
                String Dept = result.Dept_Descr__c;
                String Address1 = result.Address1__c;
                String Address2= result.Address2__c;
                String Address3 = result.Address3__c;
                String City = result.City__c;
                String country = result.Country__c;
                String currencycd = result.currency_CD__c;
                String Department = result.Dept_Descr__c;
                String emplstatus = result.Empl_Status__c;
                String GLDID = result.GLID__c;
                String grade = result.Grade__c;
                String Level10cd = result.Level_10_CD__c;
                String level10descr = result.Level_10_Descr__c;
                String level1cd = result.Level_1_CD__c;
                String level1descr = result.Level_1_Descr__c;
                String level2cd = result.Level_2_CD__c;
                String level2descr = result.Level_2_Descr__c;
                String level3cd = result.Level_3_CD__c;
                String level3descr = result.Level_3_Descr__c;
                String level4cd = result.Level_4_CD__c;
                String level4descr  = result.Level_4_Descr__c;
                String level5cd = result.Level_5_CD__c;
                String level5descr = result.Level_5_Descr__c;
                String level6cd = result.Level_6_CD__c;
                String lob = result.LOB__c;
                String level7cd = result.Level_7_CD__c;
                String level7descr = result.Level_7_Descr__c;
                String level8cd = result.Level_8_CD__c;
                String level8descr = result.Level_8_Descr__c;
                String level9cd = result.Level_9_CD__c;
                String level9descr = result.Level_9_Descr__c;
                String location = result.Location__r.Name;
                String locdesc = result.Location_Descr__c;
                String phone = result.Phone__c;
                String countrycode = result.Phone_Country_Code__c;
                String postal = result.Postal__c;
                String plname = result.Prf_Last_Name__c;
                String pfname = result.Prf_First_Name__c;
                String state = result.State__c;
                String supID = result.Supervisor_ID__c;
                String WWID = result.WWID__c;
                String Market = result.Market__c;
                String Office = result.Office__c;
                String alias = LName.substring(0,3) + 1;
                String cnname;
                if(CusFName == null)
                    CusFName = '';
                if(CusLName == null)
                    CusLName = '';
                
                if(LName  == null){
                    LName  = '';
                }if(FName == null){
                    FName = '';
                }
                if(EmpID == null||EmpID == 'null'){
                    EmpID = '';
                }
                
                if(Dept == null){
                    Dept = '';
                }if(Address1 == null){
                    Address1 = '';
                }if(Address2 == null){
                    Address2 = '';
                }if(Address3 == null){
                    Address3 = '';
                }if(City == null){
                    City = '';
                }if(Emailadd== null){
                    Emailadd = '';
                }
                if(country == null){
                    country = '';
                }if(currencycd == null){
                    currencycd = '';
                }if(Department == null){
                    Department = '';
                }if(emplstatus == null){
                    emplstatus = '';
                }if(GLDID == null){
                    GLDID = '';
                }if(grade == null){
                    grade = '';
                }if(Level10cd == null){
                    Level10cd = '';
                }if(level10descr == null){
                    level10descr = '';
                }if(level1cd == null){
                    level1cd = '';
                }if(level2cd == null){
                    level2cd = '';
                }if(level2descr == null){
                    level2descr = '';
                }if(level3cd == null){
                    level3cd  = '';
                }if(level3descr == null){
                    level3descr = '';
                }if(level4cd== null){
                    level4cd = '';
                }if(level4descr == null){
                    level4descr = '';
                }if(level5cd  == null){
                    level5cd   = '';
                }if(level5descr == null){
                    level5descr = '';
                }if(level6cd == null){
                    level6cd = '';
                }if(lob == null){
                    lob = '';
                }if(level7cd  == null){
                    level7cd  = '';
                }if(level7descr == null){
                    level7descr = '';
                }if(level8cd  == null){
                    level8cd = '';
                }if(level8descr == null){
                    level8descr = '';
                }if(level9cd == null){
                    level9cd  = '';
                }if(level9descr == null){
                    level9descr = '';
                }if( location  == null){
                    location = '';
                }if(phone == null){
                    phone = '';
                }if(countrycode  == null){
                    countrycode = '';
                }if(postal  == null){
                    postal = '';
                }if(plname == null){
                    plname = '';
                }if( pfname  == null){
                    pfname = '';
                }if(supID  == null){
                    supID = '';
                }if(state == null){
                    state  = '';
                }if( WWID  == null){
                    WWID = '';
                }
                if( Market  == null){
                    Market = '';
                }
                
                if((FName.length() > 0) && (LName.length() > 3))
                {
                    alias = FName.substring(0,1) + LName.substring(0,3);
                    alias = alias.toLowerCase();
                }
                if(Emailadd.length()>0 && Emailadd.indexOf('@',0)>=0)
                {
                    cnname = Emailadd.substring(0,Emailadd.indexOf('@',0));
                }
                else
                {
                    cnname = '';
                }
                
                String Street = Address1 + ' ' + Address2 +  '' + Address3;
                String FedID = Emailadd.toUpperCase(); // Req:3458 - December Release
                
                //  userPg.getparameters().put('Addressstreet',Street);
                
                if(MapUserIsActive.containsKey(result.EMPLID__c)){

                    userObj1.IsActive = Boolean.ValueOf(MapUserIsActive.get(result.EMPLID__c));  
                    userObj1.Request__c = MapUserRequestNo.get(result.EMPLID__c);
                   // system.debug('$$$ '+MapUserMarketingUser.get(result.EMPLID__c));
                    
                    system.debug('$$$ '+userObj1.UserPermissionsMarketingUser);
                    
                if(MapUserRole.get(result.EMPLID__c) != ''){
                    userObj1.UserRoleId = MapUserRoleInsert1.get(MapUserRole.get(result.EMPLID__c));
                }
                if(MapUserProfile.get(result.EMPLID__c) != ''){    
                    userObj1.ProfileId = MapUserProfileInsert1.get(MapUserProfile.get(result.EMPLID__c));
                }
                if(MapUserLocale.get(result.EMPLID__c) != ''){ 
                    userObj1.LocaleSidKey = MapUserLocaleInsert1.get(MapUserLocale.get(result.EMPLID__c));
                }
                if(MapUserlanguage.get(result.EMPLID__c) != ''){ 
                    userObj1.LanguageLocaleKey = MapUserLanguageInsert1.get(MapUserLanguage.get(result.EMPLID__c));
                }
                if(MapUserTimeZone.get(result.EMPLID__c) != ''){ 
                    userObj1.TimeZoneSidKey = MapUserTimeZone.get(result.EMPLID__c);
                }
                if(MapUserCurrency.get(result.EMPLID__c) != ''){     
                    userObj1.DefaultCurrencyIsoCode = MapUserCurrency.get(result.EMPLID__c);
                }
                if(MapUserRequestor.get(result.EMPLID__c) != ''){    
                    userObj1.Collegue__c = MapUserrequestorInsert1.get(MapUserRequestor.get(result.EMPLID__c));         
                }
                if(MapUserGoals.get(result.EMPLID__c) == 'Yes'){ 
                    userObj1.Goals__c = 'Sales';
                }
                 
                
                userObj1.Email = Emailadd;
                userObj1.Email_Address__c  = Emailadd;
                userObj1.Alias = alias;
                userObj1.Username = Emailadd;
                userObj1.FirstName = pfname;
                userObj1.LastName = plname;
                userObj1.CommunityNickname = cnname; //cnname
                userObj1.EmployeeNumber = EmpID;
                userObj1.Phone = phone; 
                userObj1.FederationIdentifier = FedID;
                userObj1.Welcome_Email_Sent__c = True;
                userObj1.City__c = City;
                userObj1.State__c = state;
                userObj1.mh_Blue_Sheet_Read_Write_License__c = True;
                userObj1.Gold_Sheet_Read_Write_License__c = True;
                userObj1.Green_Sheet_Read_Write_License__c = True;
                userObj1.EmailEncodingKey = 'ISO-8859-1';
                userObj1.street = Street;
                userObj1.Country__c = country;
                userObj1.Postal__c = postal;
                userObj1.Market__c = Market; 
                userObj1.Employee_Office__c = Office;
                userObj1.GLID__c = GLDID;
                userObj1.Location__c = location;
                userObj1.Prf_First_Name__c = pfname;
                userObj1.Prf_Last_Name__c = plname;
                userObj1.Last_Name__c = plname;
                userObj1.Employee_Office__c = locdesc;
                userObj1.Currency_CD__c = currencycd;
                userObj1.Supervisor_ID__c = SupID;
                userObj1.Grade__c = grade;
                userObj1.WWID__c = WWID;
                userObj1.LOB__c = lob;
                userObj1.Level_1_Descr__c = level1descr;
                userObj1.Level_2_Descr__c = level2descr;
                userObj1.Level_3_Descr__c = level3descr;
                userObj1.Level_4_Descr__c = level4descr;
                userObj1.Level_5_Descr__c = level5descr;
                userObj1.Level_7_Descr__c = level7descr;
                userObj1.Level_8_Descr__c = level8descr;
                userObj1.Level_9_Descr__c = level9descr;
                userObj1.Level_10_Descr__c = level10descr;
                userObj1.Level_1_CD__c = level1cd;
                userObj1.Level_2_CD__c = level2cd;
                userObj1.Level_3_CD__c = level3cd;
                userObj1.Level_4_CD__c = level4cd;
                userObj1.Level_5_CD__c = level5cd;
                userObj1.Level_6_CD__c = level6cd;
                userObj1.Level_7_CD__c = level7cd;
                userObj1.Level_8_CD__c = level8cd;
                userObj1.Level_9_CD__c = level9cd;
                userObj1.Level_10_CD__c = level10cd;
                userObj1.Sub_Region__c = SubRegion;
                userObj1.Sub_Market__c = SubMarket;
                userObj1.Region__c = Region;
                userObj1.Country__c = country;
                userObj1.First_Name__c = CusFName; 
                userObj1.Last_Name__c = CusLName;
              }
            }
            finalUserInsertlist.add(userObj1); 
        } 
    }
    
    /*
* @Description : This method fetches all the details of Colleague record
* @ Args       : String Id
* @ Return     : Colleague__c
*/ 
    
    Public List<Colleague__c> QueryColleagueAllDetails ( List<String> empId){
        List<Colleague__c> Colleague = null ;
        Colleague = [SELECT  id, Name, Address1__c, Address2__c, Address3__c, City__c, Country__c, currency_CD__c, Dept_Descr__c,
                     Email_Address__c, Empl_Status__c, EMPLID__c, First_Name__c, GLID__c, Grade__c, Last_Name__c, 
                     Level_10_CD__c, Level_1_CD__c, Level_1_Descr__c, Level_2_CD__c, Level_2_Descr__c, Level_10_Descr__c, 
                     Level_3_CD__c, Level_3_Descr__c, Level_4_CD__c, Level_4_Descr__c, Level_5_CD__c, Level_5_Descr__c,
                     Level_6_CD__c, LOB__c,Level_7_CD__c, Office__c, Sub_Market__c, Sub_Region__c, Region__c, Market__c, 
                     Level_7_Descr__c, Level_8_CD__c, Level_8_Descr__c, Level_9_CD__c, Level_9_Descr__c, Location_Descr__c, 
                     Phone__c, Location__r.Name, Phone_Country_Code__c, Postal__c, Prf_Last_Name__c, Prf_First_Name__c, 
                     State__c, Supervisor_ID__c, WWID__c 
                     FROM Colleague__c where EMPLID__c = : empId] ;
        return Colleague ;    
    }
    
    Public void permissionSetAssigning(){
        List<permissionSetAssignment> assigmentList = new List<permissionSetAssignment>();
         List<permissionSetAssignment> assigmentList1 = new List<permissionSetAssignment>();
        List<permissionset> permissionSetId = new List<permissionset>();
        List<permissionset> SBCpermissionSetId = new List<permissionset>();
        List<User> userRecord = New List<User> ();
        userRecord = [ Select Id, Name, EmployeeNumber, region__c  FROM User WHERE EmployeeNumber = : setOfEmpNo ] ;
        permissionSetId = [Select id,name from permissionset where Name in ( 'Salesforce_1_IOS_User','Tips_User')];
        SBCpermissionSetId = [Select id,name from permissionset where Name = 'SBC_Access'];
        for(User usr:userRecord ){
          for(permissionset psId:permissionSetId ){
            permissionSetAssignment assignment = new permissionSetAssignment ();            
            assignment.assigneeId = usr.Id;
            assignment.permissionSetId = psId.Id ;
            assigmentList.add(assignment);
            //system.debug('permission set values '+usr.Id);
          }
        }  
        //database.insert (assigmentList,false);
        Database.SaveResult[] srList2 = Database.insert(assigmentList,false);
        
        for(User usr:userRecord ){
        if (usr.region__c == 'North America'){
               //system.debug('permission set values -- inside region part'+usr.region__c);
               permissionSetAssignment assignment1 = new permissionSetAssignment ();
               assignment1.assigneeId = usr.Id;
               assignment1.permissionSetId = SBCpermissionSetId[0].Id ;
               assigmentList1.add(assignment1);
               //system.debug('permission set values -- inside region part'+assigmentList1);
             }
        }
        Database.SaveResult[] srList3 = Database.insert(assigmentList1,false);
        
        
        for (Database.SaveResult sr : srList2) {
        if (sr.isSuccess()) {
            //SuccessInsertlist.add(sr.getId());
            System.debug('Successfully updated Mix ' + sr.getId());
        }
        else {               
            for(Database.Error err : sr.getErrors())
            {   
               // ErrorInsertlist.add(err.getMessage());               
                System.debug(err.getStatusCode() + ': ##' + err.getMessage());
            }
                
          }
      }
    }
    
    
    Public void updateRecords(){   
        ErrorInsertlist = New List<String>();
        ErrorUpdatelist = New List<String>();
        SuccessInsertlist = New List<String>();
        SuccessUpdatelist = New List<String>();
        
               Database.SaveResult[] srList = Database.insert(finalUserInsertlist, false);
               Database.SaveResult[] srList1 = Database.update(finalUserUpdatelist, false);
      
      for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
            SuccessInsertlist.add(sr.getId());
            System.debug('Successfully updated Mix ' + sr.getId());
        }
        else {               
            for(Database.Error err : sr.getErrors())
            {   
                ErrorInsertlist.add(err.getMessage());               
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
            }
                
          }
      }
      for (Database.SaveResult sr1 : srList1) {
        if (sr1.isSuccess()) {
            SuccessUpdatelist.add(sr1.getId());
            System.debug('Successfully updated Mix ' + sr1.getId());
        }
        else {               
            for(Database.Error err1 : sr1.getErrors())
            {   
                ErrorUpdatelist.add(err1.getMessage());               
                System.debug(err1.getStatusCode() + ': ' + err1.getMessage());
            }
                
          }
      }        
        permissionSetAssigning();
        successErrorDisplay();
    }
    Public void successErrorDisplay(){   
        SuccessInsertDisp = New List<User>();
        SuccessUpdateDisp = New List<User>();
        SuccessInsertDisp = [Select Id,Username,EmployeeNumber,Email from User where id = :SuccessInsertlist];
        SuccessUpdateDisp = [Select Id,Username,EmployeeNumber,Email from User where id = :SuccessUpdatelist];
    }
}