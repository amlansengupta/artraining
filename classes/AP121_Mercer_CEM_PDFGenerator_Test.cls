/*Purpose: Test class to provide test coverage for CEM print functionality.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   11/13/2014    Created test class to provide test coverage for  AP121_Mercer_CEM_PDFGenerator  class
 
============================================================================================================================================== 
*/
@isTest (SeeAllData=true)
private class AP121_Mercer_CEM_PDFGenerator_Test{
/*
    * @Description : Testmethod to test CEM print.
 */ 
static testmethod void testCEMcreation(){

    Mercer_TestData mtdata = new Mercer_TestData();
    User user1 = new User();
    String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
    user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        
    System.runAs(user1) {    
    Colleague__c collg = mtdata.buildColleague();
    Account acc = mtdata.buildAccount();
    Contact con = mtdata.buildcontact();
    Client_Engagement_Measurement_Interviews__c  cem = mtdata.buildCEM();
    cem.Account__c = acc.id;
    cem.Status__c = 'Completed';
    cem.Overall_rating__c = '5';
    cem.Reviewer__c =collg.id;
    cem.CEM_Interviewer_2__c = collg.id;
    cem.Client_Contact_Reviewer_1__c = con.id;
    insert(cem);
    Test.startTest();
    system.currentPageReference().getParameters().put('id', cem.id);
    ApexPages.StandardController stdController = new ApexPages.StandardController(cem); 
    AP121_Mercer_CEM_PDFGenerator controller3 = new  AP121_Mercer_CEM_PDFGenerator(stdController);
    Test.stopTest();
    }
}

}