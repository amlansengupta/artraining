/*Purpose:  This Apex class contains static methods to process Product records on their various stages of insertion/updation 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Srishty   05/16/2013  Created Apex class to support trigger TRG09, 
   2.0 -    Jagan     04/24/2014  Updated code to update product sub business field
                                  This will add the price book entry(Price - 1)and marks it active for any product that is inserted 
                                  Will update the respective price book as active/inactive whenever a product is updated to active/inactive
   3.0 -    Sarbpreet 01/05/2016  As part of request# 8245(March Release 2016), updated logic to update Scope Required field and create scopeit project.
============================================================================================================================================== 
*/
public class AP59_PricebookentryTriggerUtil
 {
 
    private Static Set<String> prodIds = new Set<String>();
    //ID to store Standard Price Book ID
    private Static  Id pb;
    
    //List to store all PriceBookEntries for a given product
    private Static List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>();
    
    //List to store CurrencyISOCode from PriceBookEntry
    private Static List<String> pbeCurrencyList = new List<String>();

    private Static List<PriceBookEntry> Pbelist = new List<PriceBookEntry>();

     static{
         //pb = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
         if(Test.isRunningTest()){
             pb = Test.getStandardPricebookId();
         }else{
             pb = [select Id from PriceBook2 where IsStandard = True LIMIT 1].Id;
         }
     }
     
    /*
     * @Description : This method is called on before insertion of Pricebook entry records
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
    public static void processPbAfterInsert(List<Product2> triggernew)
    { 
     PricebookEntry pBookEntry;
     List<Schema.PicklistEntry> pleList = PricebookEntry.CurrencyIsoCode.getDescribe().getPicklistValues();
        try
        {
            for(Product2 pro : triggernew)
            {
               for(Schema.PicklistEntry pickVal : pleList)
                {
                    PricebookEntry pbEntry = new PricebookEntry();
                    pbEntry.Pricebook2Id = pb;
                    pbEntry.Product2Id = pro.Id;
                    pbEntry.UnitPrice = 1.00;
                    pbEntry.IsActive = (pro.IsActive == true)?true:false;
                    pbEntry.CurrencyIsoCode = pickVal.getValue();
        
                    priceBookEntryList.add(pbEntry);    
                }         
            }
        if (priceBookEntryList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
        //insert PriceBookEntries for a given Product
        if(!priceBookEntryList.isEmpty()) insert priceBookEntryList;    
        }catch(TriggerException tEx)
        {
            System.debug('Exception occured at TRG09_Pricebookentry Trigger After Insert with reason :'+tEx.getMessage());
            throw tEx;  
        }
        catch(Exception ex)
        {
            System.debug('Exception occured at TRG09_Pricebookentry Trigger After Insert with reason :'+ex.getMessage());
            throw ex;
        }   
    
    }
     /*
     * @Description : This method is called on after Updation of PricebookEntry records
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */
     public static void processPbAfterUpdate(Map<Id,Product2> triggernewMap, Map<Id, Product2>triggeroldMap)
    {
        Map<id, Product2> prodIdMap = new Map<id, Product2>();
        // As part of request# 8245(March Release 2016), updated logic to update Scope Required field and create scopeit project.
        Map<id, opportunitylineitem> opptyLineItemMap = new Map<id, opportunitylineitem>();
        List<opportunitylineitem> updateopptyLineItemList = new List<opportunitylineitem>();
        Set<id> priceBookEntryIds =  new Set<id>();
        Set<id> opptyIds =  new Set<id>();        
        Map<id, list<opportunitylineitem>> entryLineItemsMap = new Map<id, list<opportunitylineitem>>();
        
        
        try{
              for(Product2 prod : triggernewMap.values())
              {
                  if(prod.isActive<>triggeroldMap.get(prod.Id).isActive)
                  {
                      prodIds.add(prod.Id);
                  }
                  //As part of request# 8245(March Release 2016), updated logic to update Scope Required field and create scopeit project.
                  if(prod.Classification__c == 'Consulting Solution Area' && prod.Classification__c <> triggeroldMap.get(prod.Id).Classification__c)
                  {
                    prodIdMap.put(prod.id, prod);             
                  } 
                  
                   if(prod.Classification__c == 'Non Consulting Solution Area' && prod.Classification__c <> triggeroldMap.get(prod.Id).Classification__c)
                  {
                    prodIdMap.put(prod.id, prod);             
                  }           
                  
              }

            if(!prodIds.isEmpty())
              {
                  for (Product2 prod : [select Id, isActive, (select Id,isActive, Product2Id from PricebookEntries) from Product2 where Id IN : prodIds])
                  {         
                    for(PriceBookEntry pbe1:prod.PriceBookEntries)
                    {
                        pbe1.isActive = prod.isActive == true ? true : false; 
                        pbeList.add(pbe1);
                    }  
                  }
              }
                           
            Map<id, PriceBookEntry> pBookEntry = new Map<id, PriceBookEntry>([select Product2.Name,Product2id, CurrencyIsoCode from pricebookentry where Product2id=:prodIdMap.keyset()]);                                         
            for(opportunitylineitem oli : [Select id, PriceBookEntryID, opportunityID, unitprice, CurrencyIsoCode,PricebookEntry.Product2.Classification__c, opportunity.isclosed,Skip_Validations__c from opportunitylineitem where PriceBookEntryID  =:pBookEntry.keyset() and opportunity.isclosed = false ])
            {
                if(oli.PricebookEntry.Product2.Classification__c =='Consulting Solution Area')
                {
                system.debug('oli.PricebookEntry.Product2.Classification__c is '+oli.PricebookEntry.Product2.Classification__c );
                oli.Scope_Reqd01__c = true;
                }
                else
                oli.Scope_Reqd01__c = false;
                opptyLineItemMap.put(oli.id, oli);
                priceBookEntryIds.add(oli.PriceBookEntryID);
                opptyIds.add(oli.opportunityID);
                oli.Skip_Validations__c = true;
            }
            
            for(opportunitylineitem opptyli : opptyLineItemMap.values() )
            {
                if(opptyli.Skip_Validations__c == true)
                {
                updateopptyLineItemList.add(opptyli);
                }
            }
           
             if(!updateopptyLineItemList.isEmpty())
                {
                    
                    update updateopptyLineItemList;
                }                                
              
               Map<Id,Opportunity> opptyMap =new Map<ID,Opportunity>([select  ID,Opportunity_ID__c, isclosed from Opportunity where Id IN :opptyIds and  isclosed = false]);
               Map<id, PriceBookEntry> PriceBookEntryMap = new Map<id, PriceBookEntry>([select Product2.Name,Product2id from pricebookentry where id=:priceBookEntryIds]);  
               List<ScopeIt_Project__c> scopeitProjectList = [Select id, name, OpportunityProductLineItem_Id__c from ScopeIt_Project__c where OpportunityProductLineItem_Id__c =:opptyLineItemMap.keyset()];
               Map<id, ScopeIt_Project__c>  scopeProjectMap = new Map<id, ScopeIt_Project__c>();
               for(ScopeIt_Project__c sp : scopeitProjectList) 
               {
               		scopeProjectMap.put(sp.OpportunityProductLineItem_Id__c, sp);
               } 
               List<ScopeIt_Project__c> insScopProjLst = new list<ScopeIt_Project__c>();
               if(opptyLineItemMap.size()>0){
                   for(OpportunityLineItem oli : opptyLineItemMap.values()){
                        if(oli.PricebookEntry.Product2.Classification__c =='Consulting Solution Area'){
                        	if(!scopeProjectMap.containsKey(oli.id)) {
	                          ScopeIt_Project__c objProject = new ScopeIt_Project__c();
	                          String prodName = PriceBookEntryMap.get(oli.PricebookEntryid).Product2.Name+': '+opptyMap.get(oli.opportunityid).opportunity_id__c+' - Project';
	                          if(prodName.length()<68)
	                              objProject.Name = PriceBookEntryMap.get(oli.PricebookEntryid).Product2.Name;
	                          else
	                              objProject.Name = PriceBookEntryMap.get(oli.PricebookEntryid).Product2.Name.subString(0,45)+': '+opptyMap.get(oli.opportunityid).opportunity_id__c+' - Project';
	                              objProject.Sales_Price__c=oli.unitprice;
	                              objProject.OpportunityProductLineItem_Id__c = oli.id;
	                              objProject.CurrencyIsoCode = oli.CurrencyIsoCode;
	                              objProject.Product__c = PriceBookEntryMap.get(oli.PricebookEntryid).Product2Id;
	                              objProject.Opportunity__c = oli.opportunityid;
	                              insScopProjLst.add(objProject);
                        	}
                   }
                   }
              }
                   if(insScopProjLst.size()>0)
                       Database.insert(insScopProjLst);
                   // End of request# 8245(March Release 2016).
              
              
              
            
             
          if(!pbeList.isEmpty())
          {
            if (pbeList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) throw new TriggerException(TriggerException.TOO_MANY_DML_ROWS);
            update pbeList;  
          }
          
        }catch(TriggerException tEx)
        {
          System.debug('Exception occured at Opportunity Trigger processOpportunityBeforeUpdate with reason :'+tEx.getMessage()); 
          throw tEx;  
        } 

    }   
    
    public static void processProductBeforeInsert(List<Product2> triggernew){
    
        set<String> lobset = new Set<String>();
        
        for(Product2 objProd: triggernew)
            lobset.add(objProd.LOB__c);
        
        list<Product_Sub_Bus_Classification__c> lstProdClass = [select id,Business__c,Classification__c,SHORT_Sub_Business__c,Solution_Segment__c,
                                                                    sub_Busines__c from Product_Sub_Bus_Classification__c where
                                                                    Business__c in:lobset];            
        for(Product2 objProd:triggernew){
        
            for(Product_Sub_Bus_Classification__c prod: lstProdClass){
                
                if(objProd.lOB__c == prod.Business__c  && objProd.Segment__c == prod.Solution_Segment__c){
                    objProd.Classification__c =  prod.Classification__c;
                    objProd.SHORT_Sub_Business__c = prod.SHORT_Sub_Business__c;
                    objProd.Sub_Business__c = prod.Sub_Busines__c;
                }
            }
        }
            
        
    }
}