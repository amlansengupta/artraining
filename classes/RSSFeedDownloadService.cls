global class RSSFeedDownloadService {
    
    public class RSSFeed {
        
        public List<RSSFeedItem> items { get; set; }
        
        public RSSFeed() {
            System.debug('%%%%% Entering RSSFeed');
            items = new List<RSSFeedItem>();
            System.debug('%%%%% Exiting RSSFeed');
        }
        
    } 
    
    public class RSSFeedItem {
        
        public String id { get; set; }
        public String name { get; set; }
        public String link { get; set; }
        public String details { get; set; }
        public String publishDateTime { get; set; }
        public String summary { get; set; }
        
    }
    
    public class RSSOptions {
        
        public String feedType { get; set; } 

        public String feedChannelKey { get; set; }
        public String feedItemKey { get; set; }
        public String feedTitleKey { get; set; }
        public String feedLinkKey { get; set; }
        public String feedDescriptionKey { get; set; }
        
        public String feedItemInvalidNamespaceKey { get; set; }
        public String feedItemIdKey { get; set; }
        public String feedItemTitleKey { get; set; }
        public String feedItemLinkKey { get; set; }
        public String feedItemDetailsKey { get; set; }
        public String feedItemPublishDateTimeKey { get; set; }
        public String feedItemSummaryKey { get; set; }
        public String feedSummaryKey { get; set; }
        
        public RSSOptions() {
            System.debug('%%%%% Entering RSSOptions');
            
            // set default values
            
            feedType = RSSFeedDownloadService.FEED_TYPE_MSI;
            
            feedChannelKey = 'channel';
            feedItemKey = 'item'; 
            feedTitleKey = 'title';
            feedLinkKey = 'link';
            feedDescriptionKey = 'description';
            
            feedItemInvalidNamespaceKey = 'wrg.mercerhr.com';
            feedItemIdKey = 'BlurbId';
            feedItemTitleKey = 'title';
            feedItemLinkKey = 'link';
            feedItemDetailsKey = 'description'; // concatenate rss:link
            feedItemPublishDateTimeKey = 'PubDate'; // default time to midnight
            feedItemSummaryKey = 'ShortBlurbText';         
            
            System.debug('%%%%% Exiting RSSOptions');
        }
        
    }
    
    public static string FEED_TYPE_MSI = 'MSI';
    public static string FEED_TYPE_BRINK = 'BRINK';
    
    public static RSSFeed downloadRSSFeed( String url, RSSOptions options ) {
        System.debug('%%%%% Entering downloadRSSFeed with options');
        Http http = new Http();
        
        HttpRequest request = new HttpRequest();
        
        request.setMethod( 'GET' );
        
        // set endpoint for RSS feed
        request.setEndpoint( url );
        
        // send requset
        HttpResponse response = http.send( request );
            
        System.debug('%%%%% options: ' + options);  
        if( options.feedType == RSSFeedDownloadService.FEED_TYPE_BRINK ) {
            System.debug('%%%%% Setting BRINK options');
            options.feedChannelKey = 'channel';
            options.feedItemKey = 'item';
            options.feedTitleKey = 'title';
            options.feedLinkKey = 'atom:link';
            options.feedDescriptionKey = 'description';
            options.feedItemIdKey = 'guid';
            options.feedItemTitleKey = 'title';
            options.feedItemLinkKey = 'link';
            options.feedItemDetailsKey = 'description';
            options.feedItemSummaryKey = 'description';
        }

        System.debug('%%%%% options: ' + options);

        System.debug('%%%%% response.body: ' + response.getBody());

        return parseRssFeed( response.getXmlStreamReader(), options );
        
    }
    
    /**
    * Method to parse RSS feed. Since certain RSS feeds use CDATA tags we need
    * to use the XMLStreamReader instead of Dom.Document
    */
    private static RSSFeed parseRssFeed( XmlStreamReader reader, RSSOptions options ) {
        System.debug('%%%%% Entering parseRssFeed');
        System.debug('%%%%% reader: ' + reader);
        System.debug('%%%%% reader.getEventType: ' + reader.getEventType());
        System.debug('%%%%% xmlTag.Start_Element: ' + XmlTag.START_ELEMENT);
        System.debug('%%%%% reader.getLocalName: ' + reader.getLocalName());
        System.debug('%%%%% options.feedItemKey: ' + options.feedItemKey);

        boolean isSafeToGetNextXmlElement = true;
        RSSFeed feed = new RSSFeed();
        
        while( isSafeToGetNextXmlElement ) {
            if( reader.getEventType() == XmlTag.START_ELEMENT ) {

                if( reader.getLocalName() == options.feedItemKey ) {    
                    feed.items.add(parseRssItem(reader, options));  
                }       
            }
            
            if( reader.hasNext() ) {
                reader.next();
            }
            else {
                isSafeToGetNextXmlElement = false;              
                break;
            }
                        
        }
        
        System.debug('%%%%% Returnning feed:' + feed);
        return feed;
    }
    
    private static RSSFeedItem parseRssItem( XmlStreamReader reader, RSSOptions options ) {
        System.debug('%%%%% Entering parseRssItem');

        RSSFeedItem item = new RSSFeedItem();
        
        boolean isSafeToGetNextXmlElement = true;
        
        while( isSafeToGetNextXmlElement ) {
            
            if( reader.getEventType() == XmlTag.END_ELEMENT && reader.getLocalName() == 'item' ) {
                isSafeToGetNextXmlElement = false;
                break;
            }             
            else if( reader.getEventType() == XmlTag.START_ELEMENT ) {
                String localName = reader.getLocalName();
                String namespace = reader.getNamespace();
                System.debug('%%%%% localName: ' + localName);
                System.debug('%%%%% namespace: ' + namespace);
                System.debug('%%%%% options.feedItemSummaryKey: ' + options.feedItemSummaryKey);
                System.debug('%%%%% options.feedItemPublishDateTimeKey: ' + options.feedItemPublishDateTimeKey);

                // Parse the reader before doing the checks, since multiple fields can use the same key
                // and it keeps from repeating the same calls.
                String parsedString = parseString(reader);
                System.debug('%%%%% parsedString: ' + parsedString);
                System.debug('%%%%% options.feedType: ' + options.feedType);
                System.debug('%%%%% options.feedItemInvalidNamespaceKey: ' + options.feedItemInvalidNamespaceKey);
                
                // The localname for the Brink feed is giving 'item' for the title field, so adding in a check for that, but leaving the 
                // title check below in case it changes back, it will overwrite the title to what is stored by this check
                if (localName == options.feedItemKey && options.feedType.equalsIgnoreCase(RSSFeedDownloadService.FEED_TYPE_BRINK)) {
                    item.name = parsedString;
                }

                if ( localName == options.feedItemTitleKey ) {
                    System.debug('%%%%% localName == options.feedItemTitleKey');
                    // Make sure for MSI feeds that the title namespace is rss and not one of the other titles
                    if (!(options.feedType.equalsIgnoreCase(RSSFeedDownloadService.FEED_TYPE_MSI) && namespace.containsIgnoreCase(options.feedItemInvalidNamespaceKey))) {
                        if (!parsedString.equalsIgnoreCase('Mercer WDA')) {
                            item.name = parsedString;
                        }
                    } 

                    if (options.feedType.equalsIgnoreCase(RSSFeedDownloadService.FEED_TYPE_BRINK)) {
                        item.name = parsedString;
                    }
                }
                if ( localName == options.feedItemLinkKey ) {
                    item.link = parsedString;                                                           
                }
                if ( localName == options.feedItemIdKey ) {
                    item.id = parsedString;
                }
                if ( localName == options.feedItemDetailsKey ) {
                    item.details = parsedString;
                }
                if ( localName == options.feedItemPublishDateTimeKey ) {
                    item.publishDateTime = parsedString;
                }
                if ( localName == options.feedItemSummaryKey ) {
                    System.debug('%%%%% setting summary: ' + parsedString);
                    item.summary = parsedString;
                }
            }
            
            if( reader.hasNext() ) {
                reader.next();
            }
            else {
                isSafeToGetNextXmlElement = false;              
                break;
            }
        }
        
        System.debug('%%%%% Returning item: ' + item);
        return item;
    }
    
    private static String parseString( XmlStreamReader reader ) {
        System.debug('%%%%% Entering parseString');

        String returnVal = '';
        
        boolean isSafeToGetNextXmlElement = true;
        
        while( isSafeToGetNextXmlElement ) {
            
            if( reader.getEventType() == XmlTag.END_ELEMENT ) {
                break;
            } 
            else if( reader.getEventType() == XmlTag.CHARACTERS ) {
                returnVal = returnVal + reader.getText();
            } 
            else if( reader.getEventType() == XmlTag.CDATA ) {
                returnVal = reader.getText();
            }
            
            if ( reader.hasNext() ) {
                reader.next();
            } 
            else {
                isSafeToGetNextXmlElement = false;
                break;
            }
        }
        
        System.debug('%%%%% Returning returnVal: ' + returnVal);
        return returnVal;
    }   
}