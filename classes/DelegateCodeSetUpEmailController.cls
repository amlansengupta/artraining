public class DelegateCodeSetUpEmailController{
    ApexPages.standardController m_sc = null; 
    public string templateBody{get; set;}
    public string templateSubject {get; set;}
    public string emailTo{get;set;}
    public string emailCc{get;set;}
    public string emailBcc{get;set;}
    public String pageMessage{get;set;}
    Public String OppId{get;set;}
    Public Opportunity Opp{get;set;}
    Public Transient blob AttachmentBody {get;set;} // For binding to page
    Public Transient String AttachmentName {get;set;} 
    public Attachment attachment {get;set;}
    public List<id> attachIdList{get;set;}  
    private ApexPages.StandardController controller;
    
    
    public list<String> fileList{set;get;}
    public list<attachmentDetail> attachmentInfo{set;get;}
    
    public Boolean selectedattach{get;set;}    
    
    public DelegateCOdeSetUpEmailController(){}
    
    public DelegateCOdeSetUpEmailController(ApexPages.StandardController controller) {
        
        m_sc=controller;
        attachment = new Attachment();
        attachIdList= new List<id>();
        this.controller = controller;
        fileList=new List<String>();
        attachmentInfo=new List<attachmentDetail>();
        
        OppId=ApexPages.currentPage().getParameters().get('id');
        
        Opp=[select id,Name,StageName,CloseDate,Type,AccountID,Account.Name,Account.One_Code__c,Total_Opportunity_Revenue_USD__c,Concatenated_Products__c,Opportunity_ID__c   from Opportunity where Id=:OppId];
        templateBody=generateHtmlBody();
        templateSubject ='Project Setup Notification';
        
        system.debug('attachList******'+OppId);
    }
    
    public void doAttach() {
        
        System.Debug('##################################');
        //get uploaded document
        
        //create attachment from document
        
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = OppId; // the record the file is attached to
        attachment.IsPrivate = false;
        try {
            if( AttachmentBody !=null){
                attachment.body =AttachmentBody ; 
                attachment.name=AttachmentName;    
                
            }
            insert attachment;
            
            attachment.body = null;
            
            attachmentInfo.add(new attachmentDetail(attachment));
            //fileList.add(attachment.name);
            attachIdList.add(attachment.id);
            //attachList.add(attachment); 
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            //return null;
        } finally {
            attachment = new Attachment(); 
            attachment.body = null;
        }
        AttachmentBody =null;
        
        
        //System.Debug('Attachment SaveResult: '+sr);
        
    }
    
    public void deleteAttachment(){
        list<Attachment> todeleteAttach=new list<Attachment>();
        List<attachmentDetail> listTempWrapper = new List<attachmentDetail>();
        system.debug(attachmentInfo);
        for(attachmentDetail attach:attachmentInfo){
            if(attach.selected==True){
                todeleteAttach.add(attach.att);
            }else{
                listTempWrapper.add(attach);
            }
        }
        
        if(todeleteAttach.size() > 0) {
            delete todeleteAttach;
            attachmentInfo= listTempWrapper;
        } 
        
        
    }   
    public PageReference SendEmail()
    {       emailTo =Apexpages.currentPage().getParameters().get('emailToAdd');
            //Modified for Request #19079 - Start
            templateSubject =Apexpages.currentPage().getParameters().get('subjectToAdd');
            //Modified for Request #19079 - End
     system.debug('emailTo***'+emailTo);
     system.debug('templateSubject***'+templateSubject);
     system.debug('templateBody***'+templateBody);
     try{     
         attachment=new Attachment();
         if( AttachmentBody !=null){
             attachment.body =AttachmentBody ; 
             attachment.name=AttachmentName;    
             
         }
         
         if ((emailTo!= null && emailTo!='') && (templateSubject!= null && templateSubject!='') && (templateBody!= null && templateBody!='')){
             
             User usr=new User();
             String emailBody = templateBody;
             usr=[Select id,name,email,Sales_Person__c from user where id =:UserInfo.getUserId()];
             List<string> toMail=new List<String>();
             List<string> ccMail=new List<String>();
             if(emailTo.indexOf(';') != -1){
                 String[] toAdd=emailTo.split(';');
                 toMail.addAll(toAdd);
             }else{
                 toMail.add(emailTo.trim());
             }
             if(emailCc!=null && emailCc!='' && emailCc.indexOf(';') != -1){
                 String[] toAdd=emailCc.split(';');
                 ccMail.addAll(toAdd);
             }else if(emailCc!=null && emailCc!=''){
                 ccMail.add(emailCc.trim());
             }
             system.debug('toMail***'+toMail);
             
             
             
             List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
             List<attachment> delAtt=[Select id,Name,body from attachment where Id In:attachIdList];  
             if(delAtt.size()>0){
                 //addAttachment();
                 for(attachment att:delAtt){
                     Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                     efa.setFileName(att.name);
                     efa.setBody(att.body);
                     efaList.add(efa);
                 }
                 
                 mail.setFileAttachments(efaList );
                 
             }
             
             mail.setSubject(templateSubject);
             //.setTargetObjectId(usr.Id);
             mail.setSaveAsActivity(false);
             mail.setHtmlBody(templateBody); 
             mail.setToAddresses(toMail);
             
             if(ccMail!=null){
                 mail.setCcAddresses(ccMail);
             }
             String currentUserEmail = UserInfo.getUserEmail();
             String[] bccAddresses = new String[] {currentUserEmail};
                 mail.setBccAddresses(bccAddresses);
             mails.add(mail);
             System.debug('3***************'+mail);
             //Messaging.sendEmail(mails);
             Messaging.SendEmailResult [] r = Messaging.sendEmail(mails);
             for(Messaging.SendEmailResult rr:r){
                 System.debug('Email result ' + rr.IsSuccess());
                 if(!rr.IsSuccess()){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Email Not Sent. Please Retry.'));
                     return null;
                     
                 }
                 
                 Delete delAtt;
                 delAtt.clear();            
                 PageReference pageRef=new PageReference('/'+Opp.id);
                 return pageRef ;
             }
         }else if(emailTo == ''){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter Email Address'));
             
             return null;
         }else if(templateSubject == ''){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter Email Subject'));
             return null;
         }
         
     }Catch(Exception e){  
         //     System.debug('Exception***************'+e);
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Email not sent successfully'));
         return null;
     }
     
     
     return null;
    }
    
    
    public String generateHtmlBody()
    {
        List<OpportunityLineItem> oliList=new List<OpportunityLineItem>();
        if(Opp.id!=null){
            oliList= [Select Id, Name,currencyisocode , OpportunityId,Project_Manager__r.name, Product2Id,Project_Manager__c,Sales_Price_USD_calc__c,Product_Name_LDASH__c, UnitPrice,Is_Project_Required__c  FROM OpportunityLineItem WHERE OpportunityId =:Opp.id];
        }
        String htmlBody='';
        String htmlBody1='';
        String url=Label.WEbcas_URL;  
        htmlBody1 += '<div style="font-family: Calibri; font-size:14px !important;">';
        htmlBody1 += 'Opportunity Owners,'+'<br></br>';
        htmlBody1 += 'Before sending this email to your delegate:'+'</br>';
        htmlBody1 += '1. Complete <span style="color:darkred; "><b>REQUIRED</b></span> missing details below.'+'</br>';
        htmlBody1 += '2. <span style="color:darkred; "><b>Attach the relevant client agreement and/or note any existing client documents </b></span>that should be linked to the project(s).'+'</br>';
        htmlBody1 += '_____________________'+'<br>';
        htmlBody +=htmlBody1;     
        htmlbody += 'Click the link(s) below to set up chargeable project code(s) in WebCAS for the following MercerForce opportunity:'+'<br></br>';
        htmlbody +='<b>Opportunity Name: </b>' + Opp.Name +'</br>';
        htmlbody +='<b>Account: </b>'+ Opp.Account.Name + ' (' + Opp.Account.One_Code__c + ')'+'</br>';
        htmlbody +='<b>Total Opportunity Revenue: </b>'+ (Opp.Total_Opportunity_Revenue_USD__c).Format()+ ' ' + 'USD' +'<br></br>';
        // htmlbody +='<span style="color:darkred;"><b>Opportunity Owners - Please complete any missing details in fields below and attach relevant client agreements prior to delegating code set up.</b></span> <br></br>';
        
        
        for(OpportunityLineItem ol:oliList)
        {
            String req ='N';
            String manager='';
            if(ol.Is_Project_Required__c == true){
                req = 'Y';
            }else if(ol.Is_Project_Required__c == false){
                req = 'N';
            }
            
            if(ol.Project_Manager__c == null){
                manager = ' ';
            }
            else if(ol.Project_Manager__c != null){
                manager = ol.Project_Manager__r.name;
            }
            //#Req:18466- font style in loop added -- Start
            htmlbody +='</div><div style="font-family: Calibri; font-size:14px !important;">';
            htmlbody +=  '<b>Product: </b>' + ol.Product_Name_LDASH__c + '</br>';
            htmlbody += '<b>Project Required: </b>' + req + '</br>';
            htmlbody += '<a href='+url+'end_form=gocprojsf~'+ Opp.Account.One_Code__c+ '~' + Opp.Opportunity_ID__c + '~' + ol.Id +'>'+url+'end_form=gocprojsf~'+ Opp.Account.One_Code__c+ '~' + Opp.Opportunity_ID__c + '~' + ol.Id + '</a></br>';
            htmlbody += '<b>Charge Basis:</b> <span style="color:darkred; "><b>REQUIRED</b></span></br>';
            htmlbody += '<b>Payment Terms:</b> <span style="color:darkred; "><b>REQUIRED in US</b></span></br>';
            htmlbody += '<b>Engagement: </b><span style="color:darkred; "><b>REQUIRED - enter existing engagement details or request new</b></span></br>';
            htmlbody += '<b>Project Manager: </b>' + manager + '</br>';
            //htmlbody += '<b>Project Fee: </b>' + (ol.Sales_Price_USD_calc__c).Format() + ' USD' + '</br>';
            htmlbody += '<b>Project Fee: </b>' + ol.UnitPrice.format()+' '+ol.currencyisocode + '</br>';
            htmlbody += '<b>Program / Program Manager: </b>Enter a value if required</br>'; 
            htmlbody += '<b>Opp Prod Id: </b>' + ol.Id + '<br></br>';
            htmlbody +='</div>';
            //#Req-18466 End
        } 
        return htmlbody;
    }
    
    public class attachmentDetail{
        
        public attachment att {get;set;}
        public boolean selected {get;set;}
        
        public attachmentDetail(attachment a){
            att=a;
            selected=false;
        }
    }
}