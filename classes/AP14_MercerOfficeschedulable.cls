/*Purpose: This Apex class implements Schedulable interface to schedule AP14_MercerOfficeBatchable 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   12/28/2012  Created Apex Schedulable class
============================================================================================================================================== 
*/
global class AP14_MercerOfficeschedulable implements Schedulable{
    
    global Set<String> mogdSet= new Set<String>();
    
    global String accQuery; 
    // schedulable execute method
    global void execute(SchedulableContext SC) 
    {
        
        for(Mercer_Office_Geo_D__c officeGeo : [select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True])
        {
            mogdSet.add(officeGeo.Name);
        }
        
        if(!mogdSet.isEmpty())
        { 
            this.accQuery = 'Select Account_Country__c,Account_Office__c,ByPassVR__c,Account_Sub_Region__c,Account_Sub_Market__c,Office_Code__c,Account_Market__c, Account_Region__c FROM Account WHERE Office_Code__c IN ' + quoteKeySet(mogdSet) + ' AND One_Code_Status__c IN (\'Active\', \'Pending\', \'Pending - Workflow Wizard\') AND Duplicate_Flag__c = False ';
            Database.executeBatch(new AP13_MercerOfficeBatchable(accQuery), Integer.valueOf(system.label.CL51_OfficeBatchSize));    
        }
         
     }
     
     
    //convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
    global String quoteKeySet(Set<String> mapKeySet)
    {
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;

    }
}