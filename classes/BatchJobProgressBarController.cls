public class BatchJobProgressBarController {
	
	private Id batch_GP_ClassId;
    public Id batch_GP_Id {get;set;}
    final public String GP_NOT_START = 'not_started';
    final public String GP_PROCESSING = 'processing';
    final public String GP_FINISHED = 'finished';
    public String GP_batchStatus {get;set;}
    public String GP_message {get;set;}
	
    private Id batch_CBI_ClassId;
    public Id batch_CBI_Id {get;set;}
    final public String CBI_NOT_START = 'not_started';
    final public String CBI_PROCESSING = 'processing';
    final public String CBI_FINISHED = 'finished';
    public String CBI_batchStatus {get;set;}
    public String CBI_message {get;set;}
    
    private Id batch_CA_ClassId;
    public Id batch_CA_Id {get;set;}
    final public String CA_NOT_START = 'not_started';
    final public String CA_PROCESSING = 'processing';
    final public String CA_FINISHED = 'finished';
    public String CA_batchStatus {get;set;}
    public String CA_message {get;set;}
    
    private Id batch_ACBS_ClassId;
    public Id batch_ACBS_Id {get;set;}
    final public String ACBS_NOT_START = 'not_started';
    final public String ACBS_PROCESSING = 'processing';
    final public String ACBS_FINISHED = 'finished';
    public String ACBS_batchStatus {get;set;}
    public String ACBS_message {get;set;}
    
  
    
    private Id batch_SO_ClassId;
    public Id batch_SO_Id {get;set;}
    final public String SO_NOT_START = 'not_started';
    final public String SO_PROCESSING = 'processing';
    final public String SO_FINISHED = 'finished';
    public String SO_batchStatus {get;set;}
    public String SO_message {get;set;}
    
    private Id batch_SS_ClassId;
	public Id batch_SS_Id {get;set;}
	final public String SS_NOT_START = 'not_started';
	final public String SS_PROCESSING = 'processing';
	final public String SS_FINISHED = 'finished';
	public String SS_batchStatus {get;set;}
	public String SS_message {get;set;}
	
	private Id batch_ST_ClassId;
	public Id batch_ST_Id {get;set;}
	final public String ST_NOT_START = 'not_started';
	final public String ST_PROCESSING = 'processing';
	final public String ST_FINISHED = 'finished';
	public String ST_batchStatus {get;set;}
	public String ST_message {get;set;}

    private Id batch_SW_ClassId;
	public Id batch_SW_Id {get;set;}
	final public String SW_NOT_START = 'not_started';
	final public String SW_PROCESSING = 'processing';
	final public String SW_FINISHED = 'finished';
	public String SW_batchStatus {get;set;}
	public String SW_message {get;set;}
	
	private Id batch_GandA_ClassId;
    public Id batch_GandA_Id {get;set;}
    final public String GandA_NOT_START = 'not_started';
    final public String GandA_PROCESSING = 'processing';
    final public String GandA_FINISHED = 'finished';
    public String GandA_batchStatus {get;set;}
    public String GandA_message {get;set;}
    
    public Integer errornum {get;set;}
    public string fromSelectedYear{get;set;}
    public string toSelectedYear{get;set;}
    
	
    
    public void planningyearchange(){
    	if(fromSelectedYear!='None'){
        	Integer d = Integer.ValueOf(fromSelectedYear);
    		d=d+1;
        	toSelectedYear=String.ValueOf(d);
    	}
    }
    
    public BatchJobProgressBarController (){
        batch_CBI_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopyCBIDataToNextYear' Limit 1][0].id;
        batch_CBI_Id = null;
        
        batch_CA_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopyCADataToNextYear' Limit 1][0].id;
        batch_CA_Id = null;
        
        batch_ACBS_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopyACBSDataToNextYear' Limit 1][0].id;
        batch_ACBS_Id = null;
        
      
        
        batch_SO_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopySODataToNextYear' Limit 1][0].id;
        batch_SO_Id = null;
        
        batch_SS_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopySSDataToNextYear' Limit 1][0].id;
        batch_SS_Id = null;
        
        batch_ST_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopySTDataToNextYear' Limit 1][0].id;
        batch_ST_Id = null;
        
        batch_SW_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopySWDataToNextYear' Limit 1][0].id;
        batch_SW_Id = null;
        
        batch_GP_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopyGPDataToNextYear' Limit 1][0].id;
        batch_GP_Id = null;
        
        batch_GandA_ClassId = [Select Name, Id From ApexClass Where Name = 'BatchToCopyGandADataToNextYear' Limit 1][0].id;
        batch_GandA_Id = null;
        
        
        CBI_batchStatus = CBI_NOT_START;
        CBI_message = '';
        
        CA_batchStatus = CA_NOT_START;
        CA_message = ''; 
        
        ACBS_batchStatus = ACBS_NOT_START;
        ACBS_message = ''; 
        
    
        
        SO_batchStatus = SO_NOT_START;
        SO_message = '';
        
        SS_batchStatus = SS_NOT_START;
        SS_message = '';
        
        ST_batchStatus = ST_NOT_START;
        ST_message = '';
        
        SW_batchStatus = SW_NOT_START;
        SW_message = ''; 
        
        GP_batchStatus = GP_NOT_START;
        GP_message = ''; 
        
        GandA_batchStatus = GandA_NOT_START;
        GandA_message = ''; 
        
        
        errornum =0; 
       
    }
    public boolean getShowProgressBar() {
        if(CBI_batchStatus == CBI_PROCESSING )
            return true;
        if(CA_batchStatus == CA_PROCESSING )
            return true;
        if(ACBS_batchStatus == ACBS_PROCESSING )
            return true;
        
        if(SO_batchStatus == SO_PROCESSING )
            return true;
        if(SS_batchStatus == SS_PROCESSING )
            return true;
        if(ST_batchStatus == ST_PROCESSING )
            return true;
        if(SW_batchStatus ==SW_PROCESSING )
            return true;
            
        if(GP_batchStatus ==GP_PROCESSING )
            return true;
            
        if(GandA_batchStatus ==GandA_PROCESSING )
            return true;
       
            
        return false;
    }
    
    public List<SelectOption> getFromPlanningYears(){
        List<SelectOption> options = new List<SelectOption>();
        List<FCST_Fiscal_Year_List__c> fcstYearList=[Select Name from FCST_Fiscal_Year_List__c order by Name LIMIT 100];
        options.add(new SelectOption('None','--None--'));
        for(FCST_Fiscal_Year_List__c fcstYrObj:fcstYearList){
        	options.add(new SelectOption(fcstYrObj.Name,fcstYrObj.Name));	
        }
           
        return options;
        
    }
    public List<SelectOption> getToPlanningYears(){
        List<SelectOption> options = new List<SelectOption>();
        List<FCST_Fiscal_Year_List__c> fcstYearList=[Select Name from FCST_Fiscal_Year_List__c order by Name LIMIT 100];
        options.add(new SelectOption('None','--None--'));
        for(FCST_Fiscal_Year_List__c fcstYrObj:fcstYearList){
        	options.add(new SelectOption(fcstYrObj.Name,fcstYrObj.Name));	
        }
           
        return options;
        
    }
    
    //this method will fetch apex jobs and will update status,JobItemsProcessed,NumberOfErrors,TotalJobItems
   
    
    public BatchJob[] getCBIJobs() {
        
        if(batch_CBI_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_CBI_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_CBI_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_CBI_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_CBI_ClassId) {
	                CBI_batchStatus = CBI_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
     public BatchJob[] getCAJobs() {
        
        if(batch_CA_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_CA_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_CA_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_CA_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_CA_ClassId) {
	                CA_batchStatus = CA_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
	
	public BatchJob[] getACBSJobs() {
        
        if(batch_CA_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_ACBS_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_ACBS_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_ACBS_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_ACBS_ClassId) {
	                ACBS_batchStatus = ACBS_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
    
    
    public BatchJob[] getSOJobs() {
        
        if(batch_SO_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_SO_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_SO_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_SO_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_SO_ClassId) {
	                SO_batchStatus =SO_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }

    public BatchJob[] getSSJobs() {
        
        if(batch_SS_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_SS_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_SS_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_SS_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_SS_ClassId) {
	                SS_batchStatus =SS_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
    
    public BatchJob[] getSTJobs() {
        
        if(batch_ST_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_ST_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_ST_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_ST_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_ST_ClassId) {
	                ST_batchStatus =ST_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
    public BatchJob[] getSWJobs() {
        
        if(batch_SW_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_SW_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_SW_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_SW_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_SW_ClassId) {
	                SW_batchStatus =SW_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
     public BatchJob[] getGPJobs() {
        
        if(batch_GP_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_GP_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_GP_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_GP_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_GP_ClassId) {
	                GP_batchStatus =GP_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
     public BatchJob[] getGAndAJobs() {
        
        if(batch_GandA_ClassId!=null){
	        List<AsyncApexJob> apexJobs = 
	            [Select TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobItemsProcessed, Id, JobType, ApexClassId, CreatedDate From AsyncApexJob Where ApexClassId =: batch_GandA_ClassId Order by CreatedDate DESC];
	        
	        if(apexJobs.size() == 0) {
	            return new List<BatchJob>();
	        }
	
	        List<BatchJob> jobs = new List<BatchJob>();
	        for(AsyncApexJob job : apexJobs) {
	            if(job.id != batch_GandA_Id)
	                continue;
	                
	            BatchJob bj = new BatchJob();
	            bj.isCompleted = false;
	            
	            if(job.ApexClassId == batch_GandA_ClassId) {
	                bj.Job_Type = 'Process 1';
	            }            
	            bj.aj = job;
	
	            // NOT START YET
	            if(job.jobItemsProcessed == 0) {
	                bj.Percent= 0;
	                jobs.add(bj);
	                continue;
	            }
	
	            Decimal d = job.jobItemsProcessed;
	            d = d.divide(job.TotalJobItems, 2)*100;
	            bj.Percent= d.intValue();
	
	            // PROCESSING
	            if(bj.Percent != 100){
	                jobs.add(bj);
	                continue;
	            }
	
	            // FINISED
	            if(job.ApexClassId == batch_GandA_ClassId) {
	                GandA_message =GandA_FINISHED;
	                                
	            }
	            errornum += job.NumberOfErrors;
	            bj.isCompleted = true;
	            jobs.add(bj);           
	        }
	        
	   		return jobs;
        }
        else{
        	return null;
        }
		
        
    }
    
    
    public PageReference Start_CBI_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopyCBIDataToNextYear acc=new BatchToCopyCBIDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_CBI_Id=database.executebatch(acc,100);
                system.debug('************batch_CBI_Id:'+batch_CBI_Id);
  
        }
        CBI_batchStatus = CBI_PROCESSING;
        return null;
    }
    
    public PageReference Start_CA_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopyCADataToNextYear acc=new BatchToCopyCADataToNextYear(fromSelectedYear,toSelectedYear);
                batch_CA_Id=database.executebatch(acc,100);
                system.debug('************batch_CA_Id:'+batch_CA_Id);
  
        }
        CA_batchStatus = CA_PROCESSING;
        return null;
    }
    
    public PageReference Start_ACBS_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopyACBSDataToNextYear acc=new BatchToCopyACBSDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_ACBS_Id=database.executebatch(acc,100);
                system.debug('************batch_CA_Id:'+batch_ACBS_Id);
  
        }
        ACBS_batchStatus = ACBS_PROCESSING;
        return null;
    }
    
     
    
    public PageReference Start_SO_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopySODataToNextYear acc=new BatchToCopySODataToNextYear(fromSelectedYear,toSelectedYear);
                batch_SO_Id=database.executebatch(acc,100);
                system.debug('************batch_SO_Id:'+batch_SO_Id);
  
        }
        SO_batchStatus = SO_PROCESSING;
        return null;
    }
    
    public PageReference Start_SS_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopySSDataToNextYear acc=new BatchToCopySSDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_SS_Id=database.executebatch(acc,100);
                system.debug('************batch_SS_Id:'+batch_SS_Id);
  
        }
        SS_batchStatus = SS_PROCESSING;
        return null;
    }  
    
    public PageReference Start_ST_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopySTDataToNextYear acc=new BatchToCopySTDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_ST_Id=database.executebatch(acc,100);
                system.debug('************batch_SS_Id:'+batch_ST_Id);
  
        }
        ST_batchStatus = ST_PROCESSING;
        return null;
    }
    
    public PageReference Start_SW_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopySWDataToNextYear acc=new BatchToCopySWDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_SW_Id=database.executebatch(acc,100);
                system.debug('************batch_SW_Id:'+batch_SW_Id);
  
        }
        SW_batchStatus = SW_PROCESSING;
        return null;
    }
    
    public PageReference Start_GP_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopyGPDataToNextYear acc=new BatchToCopyGPDataToNextYear(fromSelectedYear,toSelectedYear);
                batch_GP_Id=database.executebatch(acc,100);
                system.debug('************batch_GP_Id:'+batch_GP_Id);
  
        }
        GP_batchStatus = GP_PROCESSING;
        return null;
    }
    
    public PageReference Start_GAndA_BatchJob() {
        //execute RecordTypeAccessFinder batch 
        if(!test.isRunningTest()){
        	
                BatchToCopyGandADataToNextYear acc=new BatchToCopyGandADataToNextYear(fromSelectedYear,toSelectedYear);
                batch_GandA_Id=database.executebatch(acc,100);
                system.debug('************batch_GandA_Id:'+batch_GandA_Id);
  
        }
        GandA_batchStatus = GandA_PROCESSING;
        return null;
    }   
    
    public PageReference update_CBI_Progress() {
        if(CBI_batchStatus == CBI_FINISHED) {
            CBI_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_CA_Progress() {
        if(CA_batchStatus == CA_FINISHED) {
            CA_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_ACBS_Progress() {
        if(ACBS_batchStatus ==ACBS_FINISHED) {
            ACBS_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_SO_Progress() {
        if(SO_batchStatus ==SO_FINISHED) {
            SO_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_SS_Progress() {
        if(SS_batchStatus ==SS_FINISHED) {
            SS_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_ST_Progress() {
        if(ST_batchStatus ==ST_FINISHED) {
            ST_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_SW_Progress() {
        if(SW_batchStatus ==SW_FINISHED) {
            SW_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_GP_Progress() {
        if(GP_batchStatus ==GP_FINISHED) {
            GP_message = 'COMPLETED';
        }
        return null;
    }
    
    public PageReference update_GAndA_Progress() {
        if(GandA_batchStatus ==GandA_FINISHED) {
            GandA_message = 'COMPLETED';
        }
        return null;
    }
       
    
    public class BatchJob{
        public AsyncApexJob aj {get;set;}
        public Integer Percent {get;set;}
        public String Job_Type {get;set;}
        public Boolean isCompleted {get;set;}
        public BatchJob(){}     
    }

}