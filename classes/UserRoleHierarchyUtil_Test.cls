/* =============================================================================================
Request Id : 17100 
This class is used as the test class for UserRoleHierarchyUtil
Date : 06-MAR-2019 
Created By : Keerthi Ramalingam
===============================================================================================*/

@isTest(seeAllData=true)
public class UserRoleHierarchyUtil_Test {
    static testMethod void UserRoleHierarchyUtil_Test_TestMethod()
    {	
        Set<ID> RoleIds = new Set<ID>();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        User testUser = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        RoleIds.add(testUser.UserRoleId);
        
        PermissionSet inPermissionSet = [Select Id, Name FROM PermissionSet where Name = 'Know_Your_Client' LIMIT 1];
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.PermissionSetId = inPermissionSet.Id;
        psa.AssigneeId = testUser.id;
        insert psa;
        
        Test.startTest();
        system.runAs(testUser){
        MFException__c Exceptionrec = new MFException__c();
        Exceptionrec.Error_Message__c = 'Could not fetch role hierarchy!';
        Exceptionrec.Name= 'RoleHierarchyException';
        Exceptionrec.RunningUser__c = testUser.Id;
        insert Exceptionrec;
        
        Account testAccount = new Account();  
        testAccount.OwnerId =testUser.Id;
        testAccount.Name = 'TestAccount';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Account_Region__c='EuroPac';
        testAccount.One_Code__c='17656';
        testAccount.Type=' Marketing';
        testAccount.Account_Sub_Market__c='Canada';
        testAccount.GU_Country__c='Canada';
        testAccount.Outsourcing_Status__c='In Conversion';
        testAccount.Clients_in_Common__c='Single';
        testAccount.National_ID__c='TestNation';
        insert testAccount;
        
        Set<ID> RoleIds2 = new Set<ID>();
        Account testAccount2 = new Account();  
        //testAccount2.OwnerId =testUser.Id;
        testAccount2.Name = 'TestAccount2';
        testAccount2.BillingCity = 'TestCity2';
        testAccount2.BillingCountry = 'TestCountry2';
        testAccount2.BillingStreet = 'Test Street2';
        testAccount2.Account_Region__c='EuroPac';
        testAccount2.One_Code__c='17656';
        testAccount2.Type=' Marketing';
        testAccount2.Account_Sub_Market__c='Canada';
        testAccount2.GU_Country__c='Canada';
        testAccount2.Outsourcing_Status__c='In Conversion';
        testAccount2.Clients_in_Common__c='Single';
        testAccount2.National_ID__c='TestNation';
        insert testAccount2;
        
        Account testAccount3 = new Account(); 
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abcdefghi@xyz.com';
        testContact.MobilePhone = '9999987699';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        
        Contact testContact2 = new Contact();
        
        
        UserRoleHierarchyUtil.getAllSubRoleIds(RoleIds2);  
        UserRoleHierarchyUtil.getAllSubRoleIds(RoleIds);
        UserRoleHierarchyUtil.isRunningUserInParentHierarchy(testAccount.Id);
        UserRoleHierarchyUtil.isRunningUserInParentHierarchy(testAccount2.Id);
            try{
                UserRoleHierarchyUtil.isRunningUserInParentHierarchy(testAccount3.Id);
            }catch(Exception ex){
                System.debug(ex.getMessage());
            }
        
        UserRoleHierarchyUtil.isRunningUserInParentHierarchyCon(testContact.Id);
        UserRoleHierarchyUtil.isRunningUserInParentHierarchyCon(testContact2.Id);
        UserRoleHierarchyUtil.recordTypeCheckAcc(testAccount.Id);
        UserRoleHierarchyUtil.isRenderSections();
        
         
        }
        Test.stopTest();
    }
}