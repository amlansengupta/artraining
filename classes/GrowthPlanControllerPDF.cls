public with sharing class GrowthPlanControllerPDF {
    
 public List<Account> lstAcc{get;set;}
 public List<Growth_plan__c> lstGp{get;set;}
 public List<Client_Business_Imperatives__c> lstClnt_BusImper{get;set;}
 public integer curPlanYear_PrevYear {get;set;}
 public integer curPlanYear_Pre_PrevYear {get;set;}
 public List<Goal_Relationship__c> lstGoals_Actions{get;set;}
 public Growth_plan__c growthPlan {get;set;}
 public List<Client_Business_Imperatives__c> lstClnt_Business_Imprtvs{get;set;}
 public List<Competitive_Assessment__c> lst_cmpt_assmt{get;set;} 
 public List<Forecast_Model__c> lst_frecst_mdl{get;set;}
 public string gpId {get;set;}
 public List<AccountAlignmentClient__c> lstAcct_Algnmnt{get;set;}
 public List<Client_Relationship__c> lstSensAtRisk{get;set;}
 public string lstCEMRating{get;set;}
 public date dateOfReview{get;set;}
 public String Reviewer{get;set;}
 public List<OneCodeData> onecode_info{get;set;}
 public string checkImage1{get;set;}
 public string checkImage2{get;set;}
 public string checkImage3{get;set;}
 public String checkImage4{get;set;}
 public Client_Engagement_Measurement_Interviews__c CEMObj{get;set;}
 public decimal currentAmount {get;set;}
 public decimal lastAmount {get;set;}
 public decimal wonAmount {get;set;}
 
 public integer noOf_OneCodes_Covered {get;set;}
 
 public decimal currentTotal {get;set;}
 public decimal lastTotal {get;set;}
 public decimal wonTotal {get;set;}
 
 public decimal sumpriorFY{get;set;} 
 public decimal sumCurCFR{get;set;} 
 public decimal sum_PriorActual{get;set;} 
 
 public Integer SelCYear{get;set;}
 public Integer SelPYear{get;set;}
 
 public decimal sumRevInYearSales{get;set;}  
 public decimal sumCurFYR {get;set;}  
 public decimal sumCurYOYGP {get;set;}
 public decimal sum_USD_Pre_Prior {get;set;}
 public decimal sum_RM_Pre_Prior {get;set;}
 
 public string clnt_challenges{get;set;}
 public List<Swot_Opportunity__c> lst_swot_opp{get;set;}
 public List<Swot_Strength__c> lst_swot_strength{get;set;}
 public List<Swot_Weak__c> lst_swot_weakness{get;set;}
 public List<Swot_Threat__c> lst_swot_threat{get;set;}
     
 public GrowthPlanControllerPDF(ApexPages.StandardController controller) {
  try{
      
     gpId=ApexPages.currentPage().getParameters().get('Id');
     //gpId='a1Y44000002Q6hD'; //Pfizer Id
    
     sumpriorFY  =0;
     sumCurCFR =0;
     sum_PriorActual=0; 
     sumRevInYearSales =0;
     sumCurFYR = 0;
     sumCurYOYGP = 0;
     sum_USD_Pre_Prior=0; 
     sum_RM_Pre_Prior=0;
     
     lst_swot_opp = New List<Swot_Opportunity__c>();
     lst_swot_strength = New List<Swot_Strength__c>();
     lst_swot_weakness = New List<Swot_Weak__c>();
     lst_swot_threat = New List<Swot_Threat__c>();
     onecode_info = new List<OneCodeData>();
     
     if(gpId!=null){
       growthPlan =[SELECT ID, Name,Are_Organization_Changes__c,Select_if_ready_for_review__c,Select_If_Reviewed__c,Client_Approved__c,CGP_Prospect__c, Status__c, Parent_Growth_Plan_ID__r.Name, Is_this_Client_a_Key_Account__c,Client_Challenges__c,
                    Share_of_Wallet__c, Economy_Factors_Status__c, Political_Factors_Status__c, Regulatory_Factors_Status__c,
                    Parent_Growth_Plan_ID__c, Growth_Indicator__c, Rebid_in_Planning_Year__c, Parent_Growth_Plan_On_CGP__c,
                    fcstPlanning_Year__r.Name, Actions__c, Account__r.One_code__c, Account__r.Account_Sub_Market__c,
                    Account__r.Account_Tier__c, Account__r.Account_office__c, Account__r.RM_LOB__c, Account__r.Name, 
                    Account__r.Account_Forecast_Type__c, Account__r.Account_Type__c, account__r.Average_NPS_Score__c, 
                    Account__r.NPS_Survey_Sent_Date__c, account__r.Relationship_Manager__r.Office__c, 
                    Account__r.Relationship_Manager__r.Name, Account__r.Id, Account__r.Date_of_Last_CEM_Interview__c,
                    Account__r.Account_Country__c from Growth_Plan__c where Id=: gpId];
      }
   
     IF(growthplan.Economy_Factors_Status__c=='<img src=" " alt=" " style="height:25px; width:25px;" border="0"/>'){
          checkImage1='none';
      }
      else{
          checkImage1='block';
      }
      IF(growthplan.Regulatory_Factors_Status__c=='<img src=" " alt=" " style="height:25px; width:25px;" border="0"/>'){
          checkImage2='none';
      }
      else{
          checkImage2='block';
      }
      IF(growthplan.Political_Factors_Status__c=='<img src=" " alt=" " style="height:25px; width:25px;" border="0"/>'){
          checkImage3='none';
      }
      else{
          checkImage3='block';
      }
      
      SelCYear = (Integer.valueOf(growthPlan.fcstPlanning_Year__r.Name));
      SelPYear = (Integer.valueOf(growthPlan.fcstPlanning_Year__r.Name))-1;
      
      System.debug('checkImage4'+checkImage4);
      curPlanYear_PrevYear = (Integer.valueOf(growthPlan.fcstPlanning_Year__r.Name))-1;
      curPlanYear_Pre_PrevYear  = (Integer.valueOf(growthPlan.fcstPlanning_Year__r.Name))-2;

      if(growthPlan!=null){
         // Plan year previous year is = current year -1;
         clnt_challenges = growthPlan.Client_Challenges__c;
         List<Client_Engagement_Measurement_Interviews__c> lstCEM = [select id,Overall_rating__c,Date_of_Review__c,Reviewer__c from Client_Engagement_Measurement_Interviews__c where  Account__r.Id =: growthPlan.Account__r.Id AND Status__c ='Completed' order by Date_of_Review__c Desc limit 1];
         if(lstCEM.size()>0){
             CEMObj = lstCEM[0];
            /* lstCEMRating = lstCEM[0].overall_rating__c;
             dateOfReview = lstCEM[0].Date_of_Review__c;
                 Reviewer = lstCEM[0].Reviewer__c; */
         }
         getOpportunities(growthPlan.Account__r.Id);
         List<Growth_Plan__c> lstGP = [select id, Name, Account__r.Name, Account__r.one_code__c, Parent_Growth_Plan_ID__r.Name, Account__r.Relationship_Manager__r.Name  
              from growth_plan__c where Parent_Growth_Plan_ID__r.Name =: growthPlan.Name  OR Name =: growthPlan.Name];
              
          if(lstGP!=null && lstGP.size()>0){
             noOf_OneCodes_Covered = lstGP.size();
             for(Growth_Plan__c gp:lstGP){
                  OneCodeData ocw = new OneCodeData();
                  ocw.AccName = gp.Account__r.Name;
                  ocw.OneCode = gp.Account__r.one_code__c;
                  if(gp.Parent_Growth_Plan_ID__r.Name!=null)
                     ocw.RMName = gp.Account__r.Relationship_Manager__r.Name;
                  onecode_info.add(ocw);
              }
          }
       }
     }
     catch(Exception e){}
  }

  public class OneCodeData{
      public string AccName {get;set;}
      public string OneCode {get;set;}
      public string RMName {get;set;}
  }
 public List<Account> getAccountDetails(){
     if(growthPlan!=null && growthPlan!=null){ 
        lstAcc=[Select Name,Account_Tier__c,NPS_Survey_Sent_Date__c,Average_NPS_Score__c from Account where Id =:growthPlan.Account__r.Id];
        if(lstAcc.size()>0)
          return lstAcc;
    }
   return lstAcc;
 } 

 public List<AccountAlignmentClient__c> getAcct_alignmnt(){  
        if(growthPlan!=null && growthPlan!=null){
           lstAcct_Algnmnt=[Select Buying_Center_Decision_Maker_Customer__c,Our_Alignment__c , Decision_Maker_Customer__c ,
                            Status_Icon__c,Contact__r.Name,Colleague__r.Name ,Relationship_Status__c, 
                            Decision_Criteria_Preferences__c, Actions_for_Improving_relationships__c, 
                            BuyingStructure_LastModified_By__c, BuyingStructure_LastModified_Date__c, BuyingStructure_Serial_Number__c 
                            from AccountAlignmentClient__c where Growth_Plan__r.Id =: growthPlan.Id order by BuyingStructure_Serial_Number__c ASC];
           if(lstAcct_Algnmnt.size()>0)
               return lstAcct_Algnmnt;
           system.debug('@@@ Buying Center:' +lstAcct_Algnmnt);
        }
        return lstAcct_Algnmnt;
}  

public List<Client_Relationship__c> getSensitiveAtRisk(){
    System.debug('test sensitive>>>>>growthPlan.Account__r.Id'+growthPlan.Account__r.Id+'>>>lstSensAtRisk....'+lstSensAtRisk);
    if(lstSensAtRisk==null)
    lstSensAtRisk = new List<Client_Relationship__c>();
    
    if(lstSensAtRisk!=null && growthPlan!=null){
         lstSensAtRisk = [select LOB_Status_Icon__c,LOB_Status__c,LOB__c,Why_at_Risk__c,At_Risk_Type__c,Date_Opened__c,
                          CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, ServiceIssue_LastModified_By__c, 
                          ServiceIssue_LastModified_Date__c, ServiceIssue_Serial_Number__c 
                          from Client_Relationship__c where Account__r.Id =: growthPlan.Account__r.Id order by ServiceIssue_Serial_Number__c ASC];
         System.debug('lstSensAtRisk>>>>>>'+lstSensAtRisk.size());
         if(lstSensAtRisk.size()>0)
           return lstSensAtRisk;
        }
        return lstSensAtRisk;
}
    
  
public List<Swot_Opportunity__c > getSwotOpportunity(){
    if(lst_swot_opp!=null && growthPlan!=null){
      lst_swot_opp=[Select Opportunity__c, Opportunity_LastModified_By__c, Opportunity_LastModified_Date__c, Opportunity_Serial_Number__c
                    from Swot_Opportunity__c where Growth_Plan__r.Id =: growthPlan.Id order by Opportunity_Serial_Number__c ASC];
      if(lst_swot_opp.size()>0)
         return lst_swot_opp;
    }
      return lst_swot_opp;
 }
 
 public List<Swot_Weak__c > getSwotWeakNess(){
     if(lst_swot_weakness!=null && growthPlan!=null){
       lst_swot_weakness=[Select Weakness__c, Weak_LastModified_By__c, Weak_LastModified_Date__c, Weak_Serial_Number__c
                          from Swot_Weak__c where Growth_Plan__r.Id =: growthPlan.Id order by Weak_Serial_Number__c ASC];
       system.debug('$$$ lst_swot_weakness' + lst_swot_weakness);
      if(lst_swot_weakness.size()>0)
         return lst_swot_weakness;
     }
      return lst_swot_weakness;
 }
 
 public List<Swot_Strength__c> getSwotStrength(){
     if(lst_swot_strength!=null && growthPlan!=null){
      lst_swot_strength=[Select Strength__c, Strength_LastModified_By__c, Strength_LastModified_Date__c, Strength_Serial_Number__c
                         from Swot_Strength__c where Growth_Plan__r.Id =: growthPlan.Id order by Strength_Serial_Number__c ASC];
      system.debug('$$$ lst_swot_weakness' + lst_swot_strength);
      if(lst_swot_strength.size()>0)
         return lst_swot_strength;
      }
      return lst_swot_strength;
 }
 
 public List<Swot_Threat__c > getSwotThreat(){
     if(lst_swot_threat!=null && growthPlan!=null){
      lst_swot_threat=[Select Threat__c, Threat_LastModified_By__c, Threat_LastModified_Date__c, Threat_Serial_Number__c
                       from Swot_Threat__c where Growth_Plan__r.Id =: growthPlan.Id order by Threat_Serial_Number__c ASC];
      if(lst_swot_threat.size()>0)
         return lst_swot_threat;
     }
      return lst_swot_threat;
 }

public List<Client_Business_Imperatives__c> getClient_Busns_Imprtvs(){
        
        if(growthPlan!=null && growthPlan!=null){ //Our_Alignment_Plan__c  
            lstClnt_Business_Imprtvs=[Select Client_Business_Imperative__c, Status_Icon__c, Our_Offering_Area__c,Status__c,
                                      To_Be_Proposed_Solution__c, Imperatives_LastModified_By__c, Imperatives_LastModified_Date__c,
                                      LastModifiedBy.Name, lastModifiedDate, Imperatives_Serial_Number__c from Client_Business_Imperatives__c where Growth_Plan__r.Id =: growthPlan.Id order by Imperatives_Serial_Number__c ASC];
            
            if(lstClnt_Business_Imprtvs.size()>0){
                return lstClnt_Business_Imprtvs;
            }
                
        }
        return lstClnt_Business_Imprtvs;
}

public List<Competitive_Assessment__c> getCmpt_Assesmnt(){
        if(growthPlan!=null && growthPlan!=null){
            lst_cmpt_assmt=[Select Competitor__c, Area__c, Account_Footprint_And_Strengths__c, Account_s_Perception_of_Competitor__c, 
                            Their_Key_Executive_Sponsor_s__c, Estimated_Wallet_Share__c, Assessment_LastModified_By__c, 
                            Assessment_LastModified_Date__c, Assessment_Serial_Number__c from Competitive_Assessment__c where Growth_Plan__r.Id =: growthPlan.Id order by Assessment_Serial_Number__c ASC];
            if(lst_cmpt_assmt.size()>0)
                return lst_cmpt_assmt;
        }
        return lst_cmpt_assmt;
}
    
 public List<Forecast_Model__c> getForecast_model(){
    system.debug('$$$ Decimal:' + sumpriorFY );
    try{
      if(growthPlan!=null && growthPlan!=null){
            lst_frecst_mdl=[Select Account__r.Name,USD_Prior_Revenue_Actual__c,Prior_Revenue_Actual__c,USD_Prior_FY_Projected__c, Growth_Plan__r.Name, Planning_Year__r.Name, LOB__c,Sub_Business__c,USD_Pre_Prior_FY_Actual__c, USD_R12_Revenue__c, 
                                    Prior_FY_Projected__c,Current_Carry_Forward_Revenue__c,R12_Current_YoY_Growth_Percent__c,Revenue_from_In_Year_Sales__c,Current_FY_Revenue__c, Current_YoY_Growth_Percent__c from Forecast_Model__c  where Growth_Plan__r.Id =: growthPlan.Id and Sub_Business__c = null];
            if(lst_frecst_mdl.size()>0){
                for( Forecast_Model__c fc:lst_frecst_mdl){
                    sum_USD_Pre_Prior += fc.USD_Pre_Prior_FY_Actual__c;
                    sum_PriorActual+= fc.USD_Prior_Revenue_Actual__c;
                    sum_RM_Pre_Prior += fc.USD_R12_Revenue__c;
                    sumpriorFY += fc.Prior_FY_Projected__c;
                    sumCurCFR += fc.Current_Carry_Forward_Revenue__c;
                    sumRevInYearSales += fc.Revenue_from_In_Year_Sales__c; 
                    sumCurFYR += fc.Current_FY_Revenue__c;
                }
                //sumCurYOYGP = ((((sumCurFYR/sumpriorFY)-1)*100)).setscale(1); 
                if(lst_frecst_mdl[0].Planning_Year__r.Name == '2020')
                    sumCurYOYGP=(((sumCurFYR-sum_PriorActual)/math.abs(sum_PriorActual))*100).setScale(1);
                else
                    sumCurYOYGP=(((sumCurFYR-sum_PriorActual)/math.abs(sum_PriorActual))*100).setScale(1);
               return lst_frecst_mdl;
            }
        }
       }catch(Exception e){}
      return lst_frecst_mdl;
 }
 
 public List<Goal_Relationship__c> getGoals_Actions(){
        if(growthPlan!=null && growthPlan!=null){
             lstGoals_Actions=[select Goal_Relationship__c.Name,Goal_Relationship__c.Area__c,Goal_Relationship__c.goal_Description__c,Goal_Relationship__c.Relationship_Status__c ,Goal_Relationship__c.Risks_Barriers__c,Goal_Relationship__c.Executive_Status__c,Goal_Relationship__c.Goal_LastModified_By__c, Goal_Relationship__c.Goal_LastModified_Date__c,Goal_Relationship__c.Goal_Serial_Number__c,Goal_Relationship__c.Executive_Name_And_Role__c,
             (select Name,Action_Name__c,Due_Date__c,Goal_Relationship__c,Owner__r.Name,Priority__c,Status__c,Action_LastModified_By__c,Action_LastModified_Date__c, Action_Serial_Number__c from Goal_Actions__r order by Action_Serial_Number__c ASC)
              from Goal_Relationship__c where Growth_Plan__r.Id =: growthPlan.Id order by Goal_Serial_Number__c ASC];
              system.debug('!!! Count: ' + lstGoals_Actions);
        }
        return lstGoals_Actions;
 }
 
 public static map<string,decimal> mapConversionTable;
 
 public static void CurrencyInitialization(){
        mapConversionTable = new map<string,decimal>();
        try{
            if (Opportunity.sObjectType.getDescribe().isAccessible()){
                for(sobject c : database.query('select id, isocode, conversionrate from currencytype where isactive = true')){ 
                    mapConversionTable.put(string.valueOf(c.get('isocode')), decimal.valueOf(string.valueOf(c.get('conversionrate'))));
                }
            }    
        }
        catch(Exception e){system.debug('MultiCurrency Not Enable');}
    }
    
 
public static map<Id,string> opportunityCurrencyCode(set<Id> opportunityIds){
        map<Id,string> mapOppCode = new map<Id,string>();
        try{
            if (Opportunity.sObjectType.getDescribe().isAccessible()){
                for(sobject c : database.query('select id,CurrencyIsoCode from opportunity where CurrencyIsoCode <> null and Id IN: opportunityIds')){ 
                    mapOppCode.put(Id.valueOf(string.valueOf(c.get('id'))), string.valueOf(c.get('CurrencyIsoCode')));
                }
            }    
        }
        catch(Exception e){system.debug('MultiCurrency Not Enable');}
        return mapOppCode;
}

public Static decimal Convert(string fromCurrency, string toCurrency, decimal Amount){        
        decimal ConvertedAmount = 0;
        try{
            if(Amount <> null && mapConversionTable <> null && mapConversionTable.containsKey(fromCurrency) && mapConversionTable.containsKey(toCurrency)){ConvertedAmount  = Amount * (mapConversionTable.get(toCurrency) / mapConversionTable.get(fromCurrency)); }
        }
        catch(Exception e){system.debug('MultiCurrency Not Enable');}
        return ConvertedAmount.setScale(2);
}
 
public void getOpportunities(string AccountId){
 
        map<string,List<Opportunity>> mapOpportunityCategory = new map<string,List<Opportunity>>();
        string DetailOppHeader = 'Waiting.....';
        decimal potentialTotal = 0;
        decimal potentialAmount = 0;
        decimal currentTotal = 0;
        decimal lastTotal = 0;
        decimal wonTotal = 0;
        currentAmount = 0;
        lastAmount = 0;
        wonAmount = 0;

        if(Growthplan.fcstPlanning_Year__c <> null){
            
            set<Id> setOppIds = new set<Id>();          
            Integer cYear = Integer.valueOf(growthPlan.fcstPlanning_Year__r.Name);
            //SelCYear = cyear;
            Integer pYear = cYear-1 ;
            //SelPYear = pYear;
            
            List<Opportunity> lstMOpp = [select Id,Name,StageName,CloseDate,iswon,isclosed,Amount,ForecastCategory from Opportunity where AccountId =: AccountId and (Calendar_Year(CloseDate) =: cYear or Calendar_Year(CloseDate) =: pYear)];
            for(Opportunity opp : lstMOpp){
                setOppIds.add(opp.id);
            }
            
            boolean isMultiCurrencyOrganization = userinfo.isMultiCurrencyOrganization();
            string DefaultCurrency = 'USD';   //userinfo.getDefaultCurrency();
            map<Id,string> mapcurrency = new map<Id,string>();
            if(isMultiCurrencyOrganization){
                CurrencyInitialization();
                mapcurrency = opportunityCurrencyCode(setOppIds);   
            }
            
            for(Opportunity opp : lstMOpp){
                Integer tyear = (opp.CloseDate).year();
                decimal OAccount = opp.Amount;
                if(OAccount == null)OAccount = 0;
                if(isMultiCurrencyOrganization) OAccount = Convert(mapcurrency.get(opp.Id), DefaultCurrency, OAccount);
                
                if(tyear == cYear ){
                   system.debug('@@@ CurYear:' + opp);
                    if(!opp.isclosed){ //open opportunity : current
                        currentTotal++;
                        currentAmount += OAccount;
                        system.debug('@@@ CurrentAmount:' + currentAmount );
                        List<Opportunity> lstOpp = new list<Opportunity>();
                        lstOpp.add(opp);
                        if(mapOpportunityCategory.containsKey('COpen')){
                            lstOpp.addAll(mapOpportunityCategory.get('COpen'));
                        }
                        mapOpportunityCategory.put('COpen',lstOpp);
                    }
                }else if(tyear == pYear ){
                   system.debug('@@@ PrevYear:' + opp);
                    if(!opp.isclosed){//open opp : previous
                        lastTotal++;
                        lastAmount += OAccount;
                        system.debug('@@@ lastAmount:' + lastAmount);
                    }
                    if(opp.iswon && opp.isclosed){ //closed Won
                        WonTotal++;
                        wonAmount += OAccount;
                        system.debug('@@@ wonAmount:' + wonAmount);
                        
                        List<Opportunity> lstOpp = new list<Opportunity>();
                        lstOpp.add(opp);
                        if(mapOpportunityCategory.containsKey('PWon')){
                            lstOpp.addAll(mapOpportunityCategory.get('PWon'));
                        }
                        mapOpportunityCategory.put('PWon',lstOpp);
                    }
                    List<Opportunity> lstOppl = new list<Opportunity>();
                    lstOppl.add(opp);
                    if(mapOpportunityCategory.containsKey('POpen')){
                        lstOppl.addAll(mapOpportunityCategory.get('POpen'));
                    }
                    mapOpportunityCategory.put('POpen',lstOppl);
                }
                
                if(opp.ForecastCategory <> null && string.valueOf(opp.ForecastCategory).equalsignorecase('open')){
                    potentialTotal++;
                    potentialAmount += OAccount;
                }

            }
        }
    }

}