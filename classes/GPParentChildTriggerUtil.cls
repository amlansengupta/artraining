public with sharing class GPParentChildTriggerUtil {
    
    public static void parentChildafterUpdate(List<Growth_Plan__c> growthPlanList){         
        
        Set<ID> gpIds = new Set<ID>();
        Set<Id> parid = new Set<Id>();
        Map<Id, Id> mapChildToParent = new Map<Id, Id>();
    
        
        for(Growth_Plan__c gr : growthPlanList)
        {
            mapChildToParent.put(gr.Id, gr.Parent_Growth_Plan_ID__c);
            gpIds.add(gr.Id);            
            parid.add(gr.Parent_Growth_Plan_ID__c);
        }
     
        List<Forecast_Model__c> updateForecastList = new List<Forecast_Model__c>();       
        
        Map<String, Forecast_Model__c> mapLobNameVsForecastListChild = new Map<String, Forecast_Model__c>();
        Map<String, Forecast_Model__c> mapLobNameVsForecastListParent = new Map<String, Forecast_Model__c>();
       
        
        List<Forecast_Model__c> forecastModelListChild = [Select Id, Account__c, Growth_Plan__c, LOB__c, Planning_Year__c, Sub_Business__c, Prior_Revenue_Actual__c, Pre_Prior_FY_Actual__c, Prior_LTM_Revenue__c from Forecast_Model__c where Growth_Plan__r.id IN: gpIds];
    
        if(forecastModelListChild != null && forecastModelListChild.size() > 0){
            for(Forecast_Model__c FCSTChild : forecastModelListChild){
                mapLobNameVsForecastListChild.put(FCSTChild.Growth_Plan__c + '' + FCSTChild.LOB__c + '' + FCSTChild.Sub_Business__c, FCSTChild);
            }
        }
     
        List<Forecast_Model__c> forecastModelListtoParent = [Select Id, Account__c, Growth_Plan__c, LOB__c, Planning_Year__c, Sub_Business__c, Prior_Revenue_Actual__c, Pre_Prior_FY_Actual__c, Prior_LTM_Revenue__c from Forecast_Model__c where Growth_Plan__c IN: parid]; 
       
        if(forecastModelListtoParent != null && forecastModelListtoParent.size() > 0) {
            for(Forecast_Model__c FCSTParent : forecastModelListtoParent){
                mapLobNameVsForecastListParent.put(FCSTParent.Growth_Plan__c + '' + FCSTParent.LOB__c + '' + FCSTParent.Sub_Business__c, FCSTParent);
            }
        }
        
        if(forecastModelListChild != null && forecastModelListChild.size() > 0) {
        
        for(Forecast_Model__c fcstObj : forecastModelListChild){
            
           Forecast_Model__c childObj = mapLobNameVsForecastListChild.get(fcstObj.Growth_Plan__c + '' + fcstObj.LOB__c + '' + fcstObj.Sub_Business__c);
           
           String keyValue = mapChildToParent.get(childObj.Growth_Plan__c);
           
           Forecast_Model__c parentObj = mapLobNameVsForecastListParent.get(keyValue + '' + fcstObj.LOB__c + '' + fcstObj.Sub_Business__c);
           
          
           
           if(parentObj != null){
               
                if(parentObj.Prior_LTM_Revenue__c == null)
                    parentObj.Prior_LTM_Revenue__c = 0;
             
                if(parentObj.Prior_Revenue_Actual__c == null)
                    parentObj.Prior_Revenue_Actual__c = 0;
             
                if(parentObj.Pre_Prior_FY_Actual__c == null)
                    parentObj.Pre_Prior_FY_Actual__c = 0;
               
                if(childObj.Prior_LTM_Revenue__c != null)
                    parentObj.Prior_LTM_Revenue__c += childObj.Prior_LTM_Revenue__c;
                if(childObj.Prior_Revenue_Actual__c != null)
                    parentObj.Prior_Revenue_Actual__c += childObj.Prior_Revenue_Actual__c;
                if(childObj.Pre_Prior_FY_Actual__c != null)
                    parentObj.Pre_Prior_FY_Actual__c += childObj.Pre_Prior_FY_Actual__c;
           
           
                childObj.Prior_LTM_Revenue__c=0;
                childObj.Prior_Revenue_Actual__c=0;
                childObj.Pre_Prior_FY_Actual__c=0;
           
                if(!updateForecastList.contains(parentObj)){
                    updateForecastList.add(parentObj);
                }
                if(!updateForecastList.contains(childObj)){
                    updateForecastList.add(childObj);
                }
           
           }
        } 
        
        }   
           if(updateForecastList != null && updateForecastList.size() > 0) {               
                update updateForecastList;  
           }            
    }
}