/*
Purpose: This Apex class implements batchable interface
==============================================================================================================================================
History
 ------------------------------------
 VERSION  AUTHOR       DATE        DETAIL    
 1.0 -    Shashank     03/29/2013  Created Controller class for Mass Edit of Sales Credit.
 2.0 -    Sarbpreet    7/6/2014    Updated Comments/documentation.
=============================================================================================================================================== 
*/
public with sharing class AP39_SalesCreditListController {

    public List<Sales_credit__c> salesCreditList{get; set;}
    public double spAllocation{get; set;}
    //public double teamAllocation{get; set;}
    public Opportunity opportunity{get;set;}
    //public double newAllocation{get; set;}
    public string salesId{get; set;}
    public double sum = 0;
    //public double sum1 = 0;
    //public double oldAllocation = 0;
    /*
     * @Description : Method to reload the page
     * @ Args       : null
     * @ Return     : PageReference
     */
    public PageReference reload(){
        return null;
    }
   /*
    *   Creation of constructor
    */
    public AP39_SalesCreditListController(ApexPages.StandardController controller) {
        
        this.opportunity = (Opportunity)controller.getRecord();
        
        salesCreditList = [select Id, EmpName__c, Employee_Office__c, Employee_Status__c, Name, Role__c,LOB__c, EmpName__r.LOB__c, Percent_Allocation__c, EmpName__r.Country__c from Sales_Credit__c where Opportunity__c =:opportunity.Id];
        for(Sales_Credit__c salesCredit : salesCreditList)
        {
            
            /*if(salesCredit.Role__c <> null && salesCredit.EmpName__r.LOB__c <> null && salesCredit.EmpName__r.Country__c <> null)
            {
                if(salesCredit.Role__c.equalsIgnoreCase(system.Label.CL41_salesCreditRole) && (salesCredit.EmpName__r.LOB__c.equalsIgnoreCase(system.Label.CL42_salesCreditLOB) || (salesCredit.EmpName__r.LOB__c.equalsIgnoreCase(system.Label.CL43_salesCreditLOB) && salesCredit.EmpName__r.Country__c == 'USA')))
                {
                   sum = sum + salesCredit.Percent_Allocation__c;
                   
                } 
                else
                {
                    sum1 = sum1 + salesCredit.Percent_Allocation__c;
                }  
            }
            else
            {
                sum1 = sum1 + salesCredit.Percent_Allocation__c;
            }*/
            
            sum = sum + salesCredit.Percent_Allocation__c;
            
        }
        spAllocation = sum;
        //teamAllocation = 200 - sum1;
        if(spAllocation < 0 )
        {
            spAllocation = 0;
        }
        /*if(teamAllocation < 0)
        {
            teamAllocation = 0;
        }*/
    }
    /*
     * @Description : This method saves the sales credit record
     * @ Args       : null
     * @ Return     : void
     */
    public pageReference save()
    {
        pageReference pageRef = null;
        //update salesCreditList;
        try
        {
                update salesCreditList;
        }catch(DMLexception e)
        {
            //if(e.getDmlStatusCode(0).equalsIgnoreCase('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0))); 
             return pageRef;  
        }catch(Exception ex)
        {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage())); 
            return pageRef;
        }
        pageRef=new pageReference('/'+opportunity.Id);
        return pageRef;
    }
    
     /*
     * @Description : This method cancels the current page and redirects to previous page
     * @ Args       : null
     * @ Return     : void
     */ 
    public pageReference cancel()
    {
        pageReference pageRef = new pageReference('/'+opportunity.Id);
        return pageRef;
    }
    /*
     * @Description : This method calculates the total sales credit
     * @ Args       : null
     * @ Return     : void
     */ 
    public void calculate()
    {
        spAllocation = sum;
        //teamAllocation = 200 - sum1;
        double sum2=0;
        //double sum3=0;
        for(Sales_Credit__c salesCredit : salesCreditList)
        {
            
            /*if(salesCredit.Role__c <> null && salesCredit.EmpName__r.LOB__c <> null && salesCredit.EmpName__r.Country__c <> null)
            {
                if(salesCredit.Role__c.equalsIgnoreCase(system.Label.CL41_salesCreditRole) && (salesCredit.EmpName__r.LOB__c.equalsIgnoreCase(system.Label.CL42_salesCreditLOB) || (salesCredit.EmpName__r.LOB__c.equalsIgnoreCase(system.Label.CL43_salesCreditLOB) && salesCredit.EmpName__r.Country__c == 'USA')))
                {
                   sum2 = sum2 + salesCredit.Percent_Allocation__c;
                   
                } 
                else
                {
                    sum3 = sum3 + salesCredit.Percent_Allocation__c;
                }  
            }
            else
            {
                sum3 = sum3 + salesCredit.Percent_Allocation__c;
            }*/
            
            sum2 = sum2 + salesCredit.Percent_Allocation__c;
            
        }
        spAllocation = sum2;
        //teamAllocation = 200 - sum3;
        if(spAllocation < 0 )
        {
            spAllocation = 0;
        }
        /*if(teamAllocation < 0)
        {
            teamAllocation = 0;
        }*/
    }
    
    
}