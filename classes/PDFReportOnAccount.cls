/*Purpose: This apex class is for generating GU profile Reports
           These reports can be generated based on the GU DUNS # or One Code
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE            DETAIL 
   1.0 -    Anand   20/May/2013     Created and finalized class 
============================================================================================================================================== 
*/

public Class PDFReportOnAccount{

    // Variables
    public String newOneCode {get; set;}
    public String newGUDUNS {get; set;}
    public String closedDate {get;set;}

    // Constructor Method for Class
    public PDFReportOnAccount(){
        newOneCode = ApexPages.currentPage().getParameters().get('onecode');
        newGUDUNS = ApexPages.currentPage().getParameters().get('guduns');
    }
    
    //Method added to Redirect to Report page
    public PageReference redirect()
    {
        if ((newOneCode==null||newOneCode=='')&& (newGUDUNS==null||newGUDUNS=='')){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please provide atleast GU DUNS# or onecode to generate report.');
            ApexPages.addMessage(myMsg);
        }
        else{
            // Redirect to Report page with one code and duns no to generate report
            PageReference page = new PageReference('/apex/GUAccountProfile');
            page.getParameters().put('onecode',newOneCode); 
            page.getParameters().put('guduns',newGUDUNS); 
            page.getParameters().put('closeddate',closedDate); 
            page.setRedirect(true);
            return page;
        }
        return null;
    }
}