@isTest
public class StageHistoryDurationUpdateBatchTest {
@isTest
    public static void test1(){
         Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        //testAccount.Relationship_Manager__c = testColleague1.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
        Opportunity testOppty1 = new Opportunity();
        testOppty1.Name = 'TestOppty41';
        testOppty1.Type = 'Rebid';
        testOppty1.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)   
        //Request Id:12638 commenting step     
        //testOppty1.Step__c = 'Identified Deal';
        //testOppty1.OwnerId = u.Id;
        testOppty1.StageName = 'Identify';
        testOppty1.CloseDate = date.Today();
        testOppty1.CurrencyIsoCode = 'ALL';
        testOppty1.Amount = 101;
        testOppty1.Opportunity_Office__c = 'London - Tower Place West';
        insert testOppty1;
        testOppty1.stageName = 'Above the Funnel';
        List<Opportunity> newlist =new List<Opportunity>();
        newlist.add(testOppty1);
        update testOppty1;
        
       // AP02_OpportunityTriggerUtil.opportunityStageAgeCalculationOnUpdate(newlist,oldmap);
        StageHistoryDurationUpdateBatch batch1 = new StageHistoryDurationUpdateBatch();
        Database.executeBatch(batch1);
        
    }

}