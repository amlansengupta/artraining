/*Purpose:   This apex class clones an opportunity
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   9/26/2013  Created Apex class controller
   2.0 -    Trisha		01/18/2019 12638-Replacing step with stage
============================================================================================================================================== 
*/



// [Oct Release Change (Request 747/2593)]:This class clones an opportunity 
public class AP69_OpportunityCloneController {
 
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    public Opportunity newOppty{get;set;}
    //public List<selectOption> opps;
    public boolean clonedBool{get;set;}
     public List<opportunity> opportunityList {
    get {
            if (opportunityList == null) opportunityList = new List<opportunity>();
            return opportunityList ;
        }
        set;} 
    public String stepValue{ get; set; }
    public String stageValue{get;set;}
    public String closeStageReasonValue{ get; set; }
    public String ChargeCodeExepValue{ get; set; }
    /********* request id:12638,adding stageValue to show in page on behalf of step start*********/
   /* public List<SelectOption> getstepVal() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption('None','--None--'));
        stOptions.add(new SelectOption('Marketing/Sales Lead','Marketing/Sales Lead'));
            stOptions.add(new SelectOption('Executing Discovery','Executing Discovery'));
            stOptions.add(new SelectOption('Identified Deal','Identified Deal'));
            stOptions.add(new SelectOption('Identified Single Sales Objective(s)','Identified Single Sales Objective(s)'));
            stOptions.add(new SelectOption('Assessed Potential Solutions & Strategy','Assessed Potential Solutions & Strategy'));
            stOptions.add(new SelectOption('Making Go/No Go Decision','Making Go/No Go Decision'));
        return stOptions;
        
    }*/
    
    public List<SelectOption> getStageVal() {
        List<SelectOption> stOptions = new List<SelectOption>();
        stOptions.add(new SelectOption(System.Label.CL57_OpportunityType,System.Label.None_With_Dash));
        stOptions.add(new SelectOption(System.Label.Stage_Above_the_Funnel,System.Label.Stage_Above_the_Funnel));            
            stOptions.add(new SelectOption(System.Label.Stage_Identify,System.Label.Stage_Identify));            
            //Request No. 17953 : Removing Qualify
          //stOptions.add(new SelectOption(System.Label.Stage_Qualify,System.Label.Stage_Qualify));                 
        return stOptions;
        
    }
    /********* request id:12638,adding stageValue to show in page end*********/
     public List<SelectOption> getcloseStageReasonVal() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--None--'));
        Schema.DescribeFieldResult fieldResult =Opportunity.Close_Stage_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
        
    }
    public List<SelectOption> getChargeCodeExepVal() {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('None','--None--'));
        
        Schema.DescribeFieldResult fieldResult =Opportunity.Project_Exception__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
        
    } 
    // initialize the controller
    public AP69_OpportunityCloneController(ApexPages.StandardController controller) { 
        //initialize the stanrdard controller
        this.controller = controller;
        stepValue='Identified Deal';
        // load the current record
        opp = (Opportunity)controller.getRecord();       
    
          //opp.Step__c = null;
         String fmtDate;
         DateTime dt;         

         Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Opportunity.fields.getMap();
         List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();

         String theQuery = 'SELECT ';
         List<Opportunity_Clone_Skip__c> ocs = Opportunity_Clone_Skip__c.getall().values();
         Set<String> fieldset = new Set<String>();
         for(Opportunity_Clone_Skip__c oc: ocs)
             fieldset.add(oc.Field_Name__c);
         
         for(Schema.SObjectField s : fldObjMapValues)
         {
              String theLabel = s.getDescribe().getLabel(); // Perhaps store this in another map
              String theName = s.getDescribe().getName();
                                     
              // Continue building your dynamic query string
              if(!fieldset.contains(theName))
                  theQuery += theName + ',';
         }
         
         
         
         // Trim last comma
         theQuery = theQuery.subString(0, theQuery.length() - 1);
                
         String oppid = opp.id;
         
         // Finalize query string
         theQuery += ' FROM opportunity WHERE id =: oppid';
                
         // Make your dynamic call
         opp = Database.query(theQuery);
         //opp.StageName = 'Identify';
         //opp.Step__c = 'Identified Deal';
         opp.recordtypeid ='012E0000000QxxE';
         
         opp.ownerid = userinfo.getuserid();
         //if(newOppty.Step__c == 'Identified Deal' || newOppty.Step__c == 'Identified Single Sales Objective(s)' || newOppty.Step__c == 'Assessed Potential Solutions & Strategy' || newOppty.Step__c == 'Making Go/No Go Decision')
         //opp.Probability = 10;
         //else
         //opp.Probability = 0;
         String CurrUserOffice = [select employee_office__c from user where id=:userinfo.getuserid()].employee_office__c;
        system.debug('Employee office....'+CurrUserOffice);
        
        List<String> officeList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Opportunity_Office__c.getDescribe();
        for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
           officeList.add(pickListVal.getLabel());
        }     
                    
        if(CurrUserOffice<>null && !CurrUserOffice.containsIgnoreCase('WFH') && officeList.contains(CurrUserOffice))
        {
            opp.opportunity_office__c = CurrUserOffice;
        }        
        else{
            opp.opportunity_office__c = null;
        }
        
        //opps.name='Clone - '+opps.name;
        opp.StageName = 'Identify';
        /****request id:12638 commenting step start****/
        //opp.Step__c = 'Identified Deal';
        /****request id:12638 commenting step end****/
        opp.probability=10;
        opp.accountId=null;
        opp.closeDate=null;
        opp.Buyer__c=null;
        opp.IsCloned__c=true;
        opp.Type=null;
        opp.ownerid = userinfo.getuserid(); 
        //opp.opportunity_office__c =CurrUserOffice;
        opp.Description=null;
        opp.Next_Action__c=null;
        opp.Close_Stage_Reason__c=null;
        opp.Closed_Stage_Reason_Detail__c =null;   
        opp.Project_Exception__c=null;
        opp.Date_as_Of__c=null;
        opp.Total_Project_Client_Rev_USD__c=null;
        opp.CY_Project_Client_Rev_USD__c=null;
        RecordType rt = [select Id,DeveloperName from RecordType where DeveloperName = 'Opportunity_Detail_Page_Assinment' and SobjectType = 'Opportunity' limit 1];
        opp.RecordTypeId=rt.id;
         
         newOppty = opp.clone(false); 
         
         
         //newOppty.Step__c = null;
         //insert newOpp;
         opportunityList .add(newOppty); 
          
                    
                          
        }
                 

         
       
      /*
      * @Description :This method  saves cloned Opportunity Page 
      * @ Args       : null   
      * @ Return     : void
      */
      
        public PageReference saveClone(){
            PageReference pageRef;
            try{
              /****request id:12638 commenting below blocks, as stage will not be dependent on step anymore start*****/
               /* if(newOppty.Step__c == 'Identified Deal' || newOppty.Step__c == 'Identified Single Sales Objective(s)' || newOppty.Step__c == 'Assessed Potential Solutions & Strategy' || newOppty.Step__c == 'Making Go/No Go Decision')
                    newOppty.StageName = 'Identify';
                else
                    newOppty.StageName = 'Above the Funnel';
                 */
                 
               /* if(newOppty.Probability  == null) {
                    if(newOppty.Step__c == 'Identified Deal' || newOppty.Step__c == 'Identified Single Sales Objective(s)' || newOppty.Step__c == 'Assessed Potential Solutions & Strategy' || newOppty.Step__c == 'Making Go/No Go Decision')
                        newOppty.Probability = 10;
                    else
                        newOppty.Probability = 0;
                 */ 
                 /****request id:12638 commenting below blocks, as stage will not be dependent on step anymore end*****/
                 if(newOppty.Probability  == null) {
                    if(newOppty.stageName == System.Label.Stage_Identify || newOppty.stageName == System.Label.Stage_Qualify)
                        newOppty.Probability = 10;
                    else
                        newOppty.Probability = 0;  
                 }
                 // newOppty.StageName = 'Test';
                 
                  
                 
                 /**** requestid:12638 replacing step in below block with stageName start ****/     
                /* if(newOppty.Step__c == '--None--' ){
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select a value for Step'));
                     return null;
                 }*/
                 /**** requestid:12638 replacing step in below block with stageName end ****/   
                 if(newOppty.stageName== System.Label.None_With_Dash ){
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.Stage_Value_Mandatory));
                     return null;
                 } 
                /****request id:12638, removing step and capturing stage start ****/
               // newOppty.Step__c=stepValue;
                newOppty.stageName=stageValue;
                /****request id:12638, removing step and capturing stage end ****/
                newOppty.Close_Stage_Reason__c=closeStageReasonValue;
                newOppty.Project_Exception__c=ChargeCodeExepValue;
                
                    if(clonedBool!=true){
                                         
                        insert newOppty ;
                        clonedBool=true;
                    }
                pageRef = new PageReference('/' + newOppty.Id);          
            }
            catch(System.DmlException e){
              System.debug('update Opp error==  '+e.getDmlMessage(0));
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
              pageRef=null;
            }
        
            return pageRef;
        }
   
     /*
     * @Description :This method  saves and direct to new Opportunity Page 
     * @ Args       : null   
     * @ Return     : void
     */
     public pageReference NewClone() {
        try{
            insert newOppty;            
        }catch(System.DmlException e){
          System.debug('update Opp error==  '+e.getDmlMessage(0));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
          return null;
        }
        

        PageReference pref = new pageReference('/setup/ui/recordtypeselect.jsp?ent=Opportunity&retURL=%2F'+newOppty.id+'&save_new_url=%2F006%2Fe%3FretURL%3D%252F'+newOppty.id);
        pref.setRedirect(true);      
        controller.save();       
        return pref;
        
   }
   
     /*
     * @Description :This method cancels the current page and return to Opportunity Page
     * @ Args       : null   
     * @ Return     : void
     */
     
     public pageReference cancelClone()
     {
        pageReference pageRef = new pageReference('/'+opp.Id);
        return pageRef;
     }


    
}