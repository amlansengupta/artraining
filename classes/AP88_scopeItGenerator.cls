/*Purpose:  This Apex class is created as a Part of March Release for Req:3729
==============================================================================================================================================
The methods called perform following functionality:
.Wrapper class to diplay with Radio buttons 
.Method to Clear Search and Perform search  
.Inserts data into TempScopeIt Projects with related Tasks and Employees.
.Methods to insert selected Template into Scope it Projects 


History 
----------------------------------------------------------------------------------------------------------------------
VERSION     AUTHOR     DATE          DETAIL 
   1.0 -    Ravi       02/10/2014    This Apex class works as Controller for "importfromtemplate" VF page whic is invoked from 
                                     "View/Import from Template button on Oppportunity".
   2.0 -    Jagan      05/14/2014    Updated code to fix the Too many SOQL queries issue.
   3.0 -    Sarbpreet  7/3/2014      Updated Comments/documentation.
   4.0 -    Madhavi    11/26/2014    Added IC_Charges__c,IC_Charge_Type__c fields to the query as part of #4881(dec 2014 release)                                    
============================================================================================================================================== 
*/
public  with sharing  class AP88_scopeItGenerator implements AP92_ObjectPaginatorListener {
 
   public Id oppId{get ;set;}
   Id Prodid {get;set;}
   public AP94_ObjectPaginator paginator {get;private set;}
   public id scpobj{get; set;}
   public list<tempWrapper> tw {get; set;}
   public String PR2 { get; set; }
   public String PR1 { get; set; }
   public String PR3 { get; set; }
   public String PR4 { get; set; }
   public String PR5 { get; set; }
   private string soql;
   public boolean renderForm {get;set;}
   public integer offsetCounter{get;set;}
   
 
   public Boolean isSearchHaveValue{get; set;} 
   
   public  Map<id,product2> aMap= new Map<id,product2>([select id,name from Product2]); 
   public List<tempWrapper> tempWrapList { get {
                if (tempWrapList == null) tempWrapList= new List<tempWrapper>();
                return tempWrapList;}
   set;}
        
   public List<tempWrapper> tempWrapListAll { get {
       if (tempWrapListAll == null) tempWrapListAll = new List<tempWrapper>();
       return tempWrapListAll;}
   set;}
    
    //ScopeIt_Project__c selectedProjects = new ScopeIt_Project__c();
    Id selectedProdId;
    ScopeIt_Project__c objSelProj;
   /*
    *   Constructor for AP88_scopeItGenerator class
    */    
    public AP88_scopeItGenerator (ApexPages.StandardSetController lstcont){
        //selectedProjects = (ScopeIt_Project__c)lstcont.getSelected();
        oppId=apexpages.currentpage().getparameters().get('oppid');
        Prodid = apexpages.currentpage().getparameters().get('id');
        if(String.isEmpty(oppId) || String.isEmpty(Prodid)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'This page should be launched from opportunity page by selecting scopeit project!!'));   
            return;
        }
        
        objSelProj = [select id,Product__c,OpportunityProductLineItem_Id__c from ScopeIt_Project__c where id=:Prodid];
        
        renderForm = true;
        offsetCounter=0;
        isSearchHaveValue=true;
        tw= new list<tempWrapper>(); 
        runSearch(); 
        
    }
    
    
    /*
    *   Wrapper class to diplay with Radio buttons 
    */  
    public class tempWrapper{
       public Temp_ScopeIt_Project__c tscp{get;set;}
       public Boolean tFlag{get;set;}
       /*
       *     Constructor for tempWrapper wrapper class
       */ 
       public tempWrapper(Temp_ScopeIt_Project__c t){
            tscp=t;
            tFlag=false;
       }
    }

  /*
   * @Description : This method is used for pagination.
   * @ Args       : List<Object> newPage
   * @ Return     : void
   */  
    public void handlePageChange(List<Object> newPage){
        tempWrapList.clear();
        if(newPage != null){
            for(Object tempWrap : newPage){
                tempWrapList.add((tempWrapper)tempWrap);
                tempWrapListAll.add((tempWrapper)tempWrap);
            }
            system.debug('paginate list is' + tempWrapList);
            system.debug('paginate list size is' + tempWrapList.size());
        }
    }
    
  /*
   * @Description : This method clears Search fields which are earlier selected
   * @ Args       : null
   * @ Return     : PageReference
   */
    public PageReference clearSearch() {
        if(PR1!=null)
             PR1 = '';
         if(PR2!=null)
             PR2 = '--None--';
         if(PR3!=null)
             PR3 ='--None--';
         if(PR4!=null)
             PR4 = '';
         if(PR5!=null)
             PR5 = '';
         
             runSearch();
            return null;
    }

  /*
   * @Description : This method Re-directs to opportunity page when cancel button is clicked.
   * @ Args       : null
   * @ Return     : PageReference
   */
    public PageReference cancel() {
        if(!string.isEmpty(oppId))
        return new PageReference('/'+oppId );
        else
        return new pageReference('/006/o');
        
    }
    
    set<id> NonScopedProds = new set<id>();
    set<id> AllProds = new set<id>();

  /*
   * @Description : This method is used to search Templates according  to the criteria selected in VF page.
   * @ Args       : null
   * @ Return     : PageReference
   */
    public PageReference runSearch() {
        
       set<id> opptyidset = new set<id>();
       NonScopedProds.clear();
       AllProds.clear();   
       String prodLob; 
        //Check if opportunity has any "Scoped Products"
        
        for(Opportunitylineitem oli:[select PricebookEntry.Product2Id,LOB__c from Opportunitylineitem where id=:objSelProj.OpportunityProductLineItem_Id__c])
        {
            prodLob = oli.LOB__c;
            AllProds.add(oli.PricebookEntry.Product2Id);
        }
                    
        soql='select CreatedDate,created_by__c,name,Product__c,Solution_Segment__c,LOB__c,Calc_Margin__c from Temp_ScopeIt_Project__c where Product_Id__c = :AllProds and LOB__c=: prodLob ' ;

             
        if(PR4!=null && String.isNotBlank(PR4)){
            if(soql.contains('where'))
                soql += ' and created_by__c LIKE \'%'+String.escapeSingleQuotes(PR4)+'%\' ';
            else
            
            soql += ' where  created_by__c LIKE \'%'+String.escapeSingleQuotes(PR4)+'%\' '; 
        }

        if(PR3!=null &&PR3!='--None--'){
            if(soql.contains('where')){
                soql += ' and  Solution_Segment__c = \''+String.escapeSingleQuotes(PR3)+'\' '; 
            }
            else
                soql += ' where Solution_Segment__c = \''+String.escapeSingleQuotes(PR3)+'\' '; 
        }

        if(PR2!=null && PR2!='--None--'){
            if(soql.contains('where')){
            soql += ' and  LOB__c = \''+String.escapeSingleQuotes(PR2)+'\' ';  
            }else
            soql += ' where  LOB__c = \''+String.escapeSingleQuotes(PR2)+'\' ';  
        }

        if(PR1!=null && String.isNotBlank(PR1)){
            if(soql.contains('where')){
            soql += ' and  Name LIKE \'%'+String.escapeSingleQuotes(PR1)+'%\' ';   

            }else 
            soql += ' where  Name LIKE \'%'+String.escapeSingleQuotes(PR1)+'%\' ';
        }

        soql+= ' ORDER BY CreatedDate desc';

        system.debug('Query' +soql );
        list<Temp_ScopeIt_Project__c> tspcList=new list<Temp_ScopeIt_Project__c> ();

        tempWrapList.clear();
        tspcList=Database.query(soql);
        system.debug('list' +tspcList);


        tw.clear();
        List<tempWrapper> paginatetempWrap=new List<tempWrapper>();
        
        if(!tspcList.isEmpty() && tspcList.size()>0 ){
        isSearchHaveValue = false;
        for(Temp_ScopeIt_Project__c t:tspcList){
            paginatetempWrap.add(new tempWrapper(t));
            //tw.add(new tempWrapper(t) );
            }
               //10 is pageSize, this refers to this class which acts as listener to paginator
            paginator = new AP94_ObjectPaginator(10,this);
            paginator.setRecords(paginatetempWrap);  
        }
        else{
            isSearchHaveValue=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Parameter entered for search did not return any Templates'));
        }
        return null;
    }
    

   /*
   * @Description : Method to insert selected Template 
   * @ Args       : null
   * @ Return     : PageReference
   */
    public PageReference insertTemplate(){
        
        ScopeIt_Project__c spc = new ScopeIt_Project__c();
        List<ScopeIt_Employee__c> scopeItEmployeeList = new List<ScopeIt_Employee__c>();
        List<ScopeIt_Task__c> lstScpTask = new List<ScopeIt_Task__c>();
        Set<Id> setTaskid = new set<Id>();
        Map<id,ScopeIt_Task__c> mapTask = new Map<id,ScopeIt_Task__c>();
       //Added IC_Charges__c,IC_Charge_Type__c fields to the query as part of #4881(dec 2014 release)
        for(Temp_ScopeIt_Project__c t:[select id,name,Product_Id__c,Product__c,Calc_Margin__c, LOB__c, (Select id,task_name__c,name,IC_Charges__c,IC_Charge_Type__c, Bill_Est_of_Increases__c,Billable_Expenses__c, Bill_Rate__c,Bill_Type__c,  Temp_ScopeIt_Project__c from Temp_ScopeIt_Tasks__r ) from Temp_ScopeIt_Project__c where id =:scpobj]){
     
            if(!AllProds.contains(t.Product_Id__c)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Selected template is not matched with any of assosiated products for the opportunity.'));   
                return null; 
            }
                
            try{
                for(Temp_ScopeIt_Task__c tst : t.Temp_ScopeIt_Tasks__r)
                {
                    scopeItEmployeeList.clear();
                    ScopeIt_Task__c st = new  ScopeIt_Task__c ();
                    st.task_name__c = tst.task_name__c;
                    st.Billable_Expenses__c=tst.Billable_Expenses__c;
                    st.Bill_Rate__c=tst.Bill_Rate__c;
                    st.Bill_Type01__c = tst.Bill_Type__c;
                    st.ScopeIt_Project__c =Prodid;
                    st.Bill_Est_of_Increases__c = tst.Bill_Est_of_Increases__c;
                    st.IC_Charge_Type__c = tst.IC_Charge_Type__c;//Added as part of #4881(dec 2014 release)
                    st.IC_Charges__c = tst.IC_Charges__c;//Added as part of #4881(dec 2014 release)
                    lstScpTask.add(st);
                    mapTask.put(tst.id, st);
                    setTaskid.add(tst.id);
                    
                }     
                database.insert(lstScpTask);
            
                for(Temp_ScopeIt_Employee__c tse:[select id,Temp_ScopeIt_Task__c,name,Billable_Time_Charges__c,Bill_Rate__c,Business__c,Market_Country__c,Employee__c,Cost__c,Hours__c from Temp_ScopeIt_Employee__c
                                            where  Temp_ScopeIt_Task__c in:setTaskid]){
                  
                     ScopeIt_Employee__c se= new ScopeIt_Employee__c();
                     system.debug('Employee_Bill_Rate__c is' + se.Employee_Bill_Rate__c); 
                     se.Employee_Bill_Rate__c =tse.Employee__c;
                     se.Hours__c=tse.Hours__c;
                     se.ScopeIt_Task__c=mapTask.get(tse.Temp_ScopeIt_Task__c).id;
                     se.Billable_Time_Charges__c =tse.Billable_Time_Charges__c;
                     se.Bill_Rate_Opp__c = tse.bill_rate__c;
                     se.ScopeIt_Project__c =Prodid;
                     se.cost__C = tse.cost__c;
                     scopeItEmployeeList.add(se);
                      
                }
                database.insert(scopeItEmployeeList);
            
                scopeItEmployeeList.clear();
                lstScpTask.clear();
                mapTask.clear();
                setTaskid.clear();
            
            }
            catch (exception ex){
                system.debug('Exception message..' +ex.getmessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Import failed with the error '+ex.getmessage()));   
                return null; 
            }
        }    
        return new PageReference('/'+oppId );
   }

   /*
   *    Getter method for select options of Solution Segments which are used in search 
   */
   public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        list<string> optionlist=new list<string>{'--None--','GPOS (Do not select)','Other (NonBillable/Functional)','OW','Mobility - Technology Solutions','Rewards - Consulting',
        'Rewards - Information Solutions',  'Rewards - Technology Solutions',   'Admin Services',   'DB Core',  'DB Risk',
        'DC & Savings', 'Legal',    'Absence Management',   'Defined Benefit',  'Defined Contribution', 'EH&B', 'Affinity Solutions',
        'DC Products/Insured Pensions', 'EH&B Administration (Flex, TPA, BA, IPA)', 'EH&B Specialty Services, Rx, Audit, Benchmarking',
        'GHSC Services',    'Health Mgmt. GHM (non-insured)',   'Individual/Executive Insurance Benefits',  
        'DB Inv Advice & Tools (inc. FSG)', 'DC Inv Solutions', 'Non DB/DC Inv Solutions',  'Financial Services',   
        'Client Manager / RMD Team Solutions',  'Retiree Medical Ins. Exchange/Valuations', 'DC Inv Advice & Tools',
        'Non DB/DC Inv Advice & Tools', 'DB Inv Solutions', 'Life, Accident & Disability',  'Medical Insurance',
        'Voluntary Solutions',  'Mercer Marketplace',   'Analytics - Consulting',   'Analytics - Information Solutions',
        'Analytics - Technology Solutions', 'Communication - Consulting',   'Communication - Technology Solutions', 'Diversity - Consulting',
        'LOP - Consulting', 'LOP - Technology Solutions',   'Mobility - Consulting',    'Mobility - Information Solutions', 
        'International Consulting Group','International - GBM', 'M & A Solutions'};
        for(String item : optionlist){
        options.add(new SelectOption(item,item));
        }
        return options;
    }
    
    /*
    *   Getter method for select options of LOB  which are used in search 
    */
    public List<SelectOption> getLob() {
        List<SelectOption> option = new List<SelectOption>();
        list<string> optionlist=new list<string>{'--None--','Benefits Admin','Cross Business Offerings (CBO)','Employee Health & Benefits','Investments','Retirement','Talent'};
        for(String item : optionlist){
        option.add(new SelectOption(item,item));
        }
        return option;
    }
    
 }