/*
Purpose: This class contains static methods to process Buying Influence records on its insertion or deletion.
This class contains methods to create ContactRole whenever a buying infleunce is created for an Opportunity and also deletes a contact role whenever buying
influence is deleted for an opportunity.

==============================================================================================================================================
History
 ----------------------- VERSION     AUTHOR  DATE        DETAIL    
               1.0 -    Shashank 03/29/2013  Created Utility Class.
               2.0-     Venkat   06/14/2015  As part of Req #6244, added logic to update the executeOppValidationsBasedOnStage to true whenever the class is called
=============================================================================================================================================== 
*/
public with sharing class AP40_BuyingInfluenceTriggerUtil 
{
    private static List<OpportunityContactRole> oppContactList = new List<OpportunityContactRole>();
    private static Map<string, Buying_Influence__c> oppIdBuyMap = new Map<string, Buying_Influence__c>();
    Public Static Boolean executeOppValidationsBasedOnStage;
    /*
     *  Method Name: insertContactRoleOnInsert
     *  Description: Insert Contact Role on insert of Buying Influence.
     *  Arguements: Map
     */
     
    Public AP40_BuyingInfluenceTriggerUtil(){
        //Req# 6244: As soon as the constructor is called, set the executeOppValidationsBasedOnStage variable to true. This variable is referenced in TRG02_OpportunityTrigger to check if any changes have happened on Buying Influence.
        executeOppValidationsBasedOnStage = true; 
    }
    
    public static void insertContactRoleOnInsert(Map<Id, Buying_Influence__c> triggernewMap)
    {
        try
        {
           AP131_insertContactRoleBuyingInfluence.insertContactRole(triggernewMap);
           
        }catch(TriggerException tEx){System.debug('Exception occured with reason :'+tEx.getMessage()); triggernewMap.values()[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
           
        }catch(Exception ex){ System.debug('Exception occured with reason :'+Ex.getMessage()); triggernewMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
          
        }
    }
     /*
     *  Method Name: deleteContactRoleOnDelete
     *  Description: Delete Contact Role on delete of Buying Influence.
     *  Arguements: Map
     */
    public static void deleteContactRoleOnDelete (Map<Id, Buying_Influence__c> triggeroldMap)
    {
        try
        {
            oppContactList.clear();
            for(Buying_Influence__c buy : triggeroldMap.values())
            {
                oppIdBuyMap.put(buy.Opportunity__c, buy);
            }
            
            for(Opportunity opp:[select Id, (select Id, ContactId, Role from OpportunityContactRoles) from Opportunity where Id IN :oppIdBuyMap.keyset()])
            {
                for(OpportunityContactRole oppContact : opp.OpportunityContactRoles)
                {
                    if(oppContact.ContactId == oppIdBuyMap.get(opp.Id).Contact__c && oppContact.Role == oppIdBuyMap.get(opp.Id).mh_Role__c )
                    {
                        oppContactList.add(oppContact);
                    }
                }
            }
            
            delete oppContactList;
        }catch(TriggerException tEx){ System.debug('Exception occured with reason :'+tEx.getMessage()); triggeroldMap.values()[0].adderror('Operation Failed due to: '+tEx.getMessage()+'. Please contact your Administrator.'); 
           
        }catch(Exception ex){System.debug('Exception occured with reason :'+Ex.getMessage());triggeroldMap.values()[0].adderror('Operation Failed due to: '+Ex.getMessage()+'. Please contact your Administrator.'); 
           
        }
    }
    
   /* public static void executeOppValidations(Map<Id, Buying_Influence__c> triggeroldMap)
    {
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Id> oppIdsList = new List<Id>();
        for(Buying_Influence__c BI : triggeroldMap.values()){
            oppIdsList.add(BI.Opportunity__c);
        }
        opportunities = [Select Id from Opportunity where Id IN: oppIdsList];
        update opportunities;
    }*/

    public static void updateContactfromOppBuyer(List<Buying_Influence__c> triggernew){
      
      
      System.debug('KCBI*************-3');
            system.debug('KCBI*****-2');

       Set<id> oppIds = new Set<id>();
        System.debug('test1');
       for(Buying_Influence__c BIid:triggernew)
       {
       
        oppIds.add(BIid.Opportunity__c);
        system.debug('KCBI*****-1' + BIid);
       }
   
       List<Opportunity> oppList = [Select id , Buyer__c from Opportunity where id IN:oppIds]; 
       system.debug('KCBI*****0' + oppList.size() + oppList);
        
        List<Opportunity> opptobeupdated=new List<Opportunity>();
        List<Buying_Influence__c> bitobeupdated= new List<Buying_Influence__c>();
        List<Buying_Influence__c> BIList = new List<Buying_Influence__c>();
        
        
        map<id,Opportunity> oppMap=new map<id,Opportunity>([select id,Buyer__c from Opportunity where id in:oppIds]);
        for(Buying_Influence__c BI : triggernew){
        system.debug('KCBI*****1' + BI);
            
            for(Opportunity opp:oppList){
              Buying_Influence__c BInew = new Buying_Influence__c();
                system.debug('KCBI*****2' + opp);
                if(BI.contact__c!=null && BI.opportunity__c==opp.id ){
                   system.debug('KCBI*****3' + BI.contact__c);
                   if(opp.Buyer__c !=null && opp.Buyer__c!=BI.contact__c){ 
                       /*BInew=BI;
                        BInew.contact__c=oppMap.get(BI.Opportunity__c).Buyer__c;
                        bitobeupdated.add(BInew);*/
                        opp.Buyer__c=BI.contact__c;
                        opptobeupdated.add(opp);
                      system.debug('KCBI*****4' + opp.Buyer__c + ':' + BI.contact__c);
                    }else{
                        system.debug('KCBI*****5' + opp.Buyer__c + ':' + BI.contact__c);
                        if(opp.Buyer__c!=BI.contact__c){
                        opp.Buyer__c=BI.contact__c;
                        opptobeupdated.add(opp);
                        }
                        
                    }
                }
            }
        }
        if(bitobeupdated.size()>0){
            System.debug('bitobeupdated*********'+bitobeupdated);
            update bitobeupdated;
            //checkTriggerRecursive.isStartKCBI= true;
        }
        if(opptobeupdated.size()>0){
            System.debug('KCBI*********6'+opptobeupdated);
            checkTriggerRecursive1.opportunityStartFlag = true;
            update opptobeupdated;
            //checkTriggerRecursive.isStartKCBI= true;
        }
        
     // }
    }
@future
public static void futureUpdate(Set<Id> ids)
{
    List<Buying_Influence__c> buyList = new List<Buying_Influence__c>([select id,contact__c,Opportunity__c from Buying_Influence__c where id in:ids]);
    List<ID> oppIDs = new List<ID>();
    for(Buying_Influence__c buy: buyList){
        oppIDs.add(buy.Opportunity__c);
    }
    map<id,Opportunity> oppMap=new map<id,Opportunity>([select id,Buyer__c from Opportunity where id in:oppIDs]);
    List<Opportunity> opptobeUpdated = new List<Opportunity>();
    for(Buying_Influence__c buy: buyList){
        Opportunity opp = oppMap.get(buy.Opportunity__c);
        opp.Buyer__c = buy.Contact__c;
        opptobeUpdated.add(opp);
    }
	update opptobeUpdated;    
}
}