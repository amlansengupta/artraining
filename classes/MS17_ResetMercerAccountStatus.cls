global class MS17_ResetMercerAccountStatus implements Database.Batchable<sObject>{
	 
	 
	
	/*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)  
    {
    	String query = 'Select MercerForce_Account_Status__c, Id From Account where MercerForce_Account_Status__c <> null';
    	return Database.getQueryLocator(query); 	
    }
    
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
    	List<Account> accountForUpdate = new List<Account>();
    	for(Account account : (List<Account>)scope)
    	{
    		account.MercerForce_Account_Status__c = null;
    		accountForUpdate.add(account);
    	}	
    	
    	if(!accountForUpdate.isEmpty()) database.update(accountForUpdate, false);
    }
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
    	MercerAccountStatusBatchHelper.callNextBatch('Opportunity Stage');	
    }
}