/*
Purpose:  This Apex class contains  the logic for creating Accounts ==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR                 DATE        DETAIL    
  1.0 -              Srishty Singh      4/11/2013   Controller Class for Mercer Account New page.  =========================================================================================================================================
*/

public with sharing class MercVFC09_AccountController {
    ApexPages.StandardController accController;
    public Account account{get; set;}
    
    // constructor for the class
    public MercVFC09_AccountController(ApexPages.StandardController controller) {
	   this.account = new Account();	
       this.accController = controller;
       this.account = (Account)controller.getRecord();
       if(accName <> null)
       {
       		account.Name = accName;	
       }
    }
    
    public pageReference saveAndnew()
    {
        PageReference pref = new pageReference('/apex/Mercer_Account_New?retURL=%2F001%2Fo&nooverride=1');
        pref.setRedirect(true);
        accController.save();
        // insert account;
        return pref;
    }
    
    public String accName{
    	get{
    		return ApexPages.currentPage().getParameters().get('acc2');
    	}
    	private set;
    }

}