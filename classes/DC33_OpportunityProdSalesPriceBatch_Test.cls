/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = true)
private class DC33_OpportunityProdSalesPriceBatch_Test {
  private static Mercer_TestData mtdata =new  Mercer_TestData();
  
   /*
     * @Description : Test method for DC33_OpportunityProdSalesPriceBatch batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
        
        String LOB = 'Retirement';
        //Colleague__c col = mtdata.buildColleague();
         
         User user1 = new User();
         String adminUserprofile = Mercer_TestData.getsystemAdminUserProfile();      
         user1 = Mercer_TestData.createUser1(adminUserprofile , 'usert2', 'usrLstName2', 2);
                 
         Account acc = mtdata.buildAccount();                  
         Test.startTest();
         Opportunity opp = mtdata.buildOpportunity();
     
         //opp.CurrencyIsoCode = 'EUR';
         //update opp;
         
         Product2 pro1 = mtdata.buildScopeProduct(LOB);
         
         Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];        
         PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro1.Id and CurrencyIsoCode = 'ALL' limit 1];
         Test.stopTest(); 
         OpportunityLineItem opptylinitem =  Mercer_TestData.createOpptylineItem(opp.Id, pbEntry.Id);
         
         system.debug('opptylinitem is '+ opptylinitem);
         
         system.runAs(User1){
                      
                 String  qry = 'Select id, OpportunityId, UnitPrice from Opportunitylineitem where id = \''+ opptylinitem.id+ '\'';
                
                 database.executeBatch(new DC33_OpportunityProdSalesPriceBatch(qry), 10); 
            
         }   
          
     
    }
}