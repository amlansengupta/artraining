/*Purpose: This Apex class implements Batchable to Update Relationship Manager if it is not a mercerforce User
==============================================================================================================================================
History 
----------------------- 
VERSION AUTHOR    DATE            DETAIL 
  1.0 -           Srishty    04/15/2012    Created Apex Batchable class
============================================================================================================================================== 
*/

global class AP42_RMOwnershipBatchable implements Database.Batchable<sObject>, Database.Stateful {
    global Map<String, List<Account>> User = new Map<String, List<Account>>();
    global Map<String, User> empIdUserMap = new Map<String, User>();
    global String query = 'Select Id, OwnerId, Relationship_Manager__r.EMPLID__c From Account where (OwnerId = \'005E00000028Q8P\' or OwnerId = \'005E0000002A6sM\') AND Relationship_Manager__r.EMPLID__c <> null AND One_Code_Status__c IN (\'Active\', \'Pending\', \'Pending - Workflow Wizard\') AND Duplicate_Flag__c = False';
    global AP42_RMOwnershipBatchable(Map<String, User> empIdUserMapFromSchedulable)
    
    {
        this.empIdUserMap = empIdUserMapFromSchedulable;
    
    }
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // this method will return 50 million rows
        return Database.getQueryLocator(query); 
        //return null;
    }   
    
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        // this will return 50000
        //for(Account : query)
        List<Account> accForUpdate = new List<Account>();
        List<Account> accList =(List<Account>)scope;
        for(Account account : accList)
        {
            if (account.Relationship_Manager__c <> null)
            {
                if(empIdUserMap.containskey(account.Relationship_Manager__r.EMPLID__c))
                {
                    if(account.OwnerId <> empIdUserMap.get(account.Relationship_Manager__r.EMPLID__c).Id)
                    {
                        account.OwnerId = empIdUserMap.get(account.Relationship_Manager__r.EMPLID__c).Id;
                        accForUpdate.add(account);  
                    }
                    
                }
            }
        }
        if(!accForUpdate.isEmpty()) Database.update(accForUpdate, false);
    } 
    
    
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
   

    }
   
}