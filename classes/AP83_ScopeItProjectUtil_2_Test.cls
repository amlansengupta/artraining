/*Purpose: Test class to provide test coverage for AP83_ScopeItProjectUtil_Test class.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   03/1/2014  Created test class to provide test coverage for AP83_ScopeItProjectUtil_Test class
 
============================================================================================================================================== 
*/
 @isTest(seeAllData = false)
private class AP83_ScopeItProjectUtil_2_Test {

        
    private static Mercer_TestData mtdata;
    private static Mercer_ScopeItTestData mtscopedata;
    private static Integer month = System.Today().month();
    private static Integer year = System.Today().year();   
    
      @testSetup static void setup(){
        mtdata = new Mercer_TestData();
         
        Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
       // setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        setting.StrValue__c = 'International:5000;North America:10000';         
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        
         /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
    Request No. 17953 : Removing Finalist END */ 
        
        Insert listofvalues;
        String LOB = 'Retirement';
        String billType = 'Billable';
        Product2 testProduct = new Product2();
        testProduct.Name = 'TestPro';
        testProduct.Family = 'RRF';
        testProduct.IsActive = True;
        testProduct.Classification__c = 'Consulting Solution Area';
        testProduct.LOB__c = 'Retirement';
        insert testProduct;
        ID prodId = testProduct.id;
       Colleague__c testColleague7 = mtdata.buildColleague1();
       Account acc = mtdata.buildAccount1();
       Opportunity testOppty = mtdata.buildOpportunity1();
       ID  opptyId  = testOppty.id;
       Pricebook2 prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
       PricebookEntry pbEntry = [select Id,product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.Id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
       ID  pbEntryId = pbEntry.Id;      
        
        test.starttest();       
           OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId); 
        test.stoptest();

    }
    
    /*
     * @Description : Test class for calculating LOB Margin for Product with 'Retirement' LOB   
    */
    static testMethod void myUnitTestRetirement() {  
        User user1 = new User();
        mtscopedata = new Mercer_ScopeItTestData();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        Opportunity opp = [Select Id from Opportunity where Name = 'test oppty'];
           ID  opptyId = opp.Id;
           OpportunityLineItem opptylineItem = [Select Id, CurrencyIsoCode, OpportunityId, PricebookEntryId, Revenue_Start_Date__c, Revenue_End_Date__c, UnitPrice, CurrentYearRevenue_edit__c From OpportunityLineItem Where OpportunityId =: opp.id ];
           ID oliId= opptylineItem.id;
           String oliCurr = opptylineItem.CurrencyIsoCode;
           Double oliUnitPrice = opptylineItem.unitprice;
           String employeeBillrate =  [Select e.Id, Bill_Cost_Ratio__c,Employee_Status__c,Sub_Business__c ,type__c,currenCyISOCode, Bill_Rate_Local_Currency__c, Business__c, Level__c, Market_Country__c From Employee_Bill_Rate__c e  limit 1].id;
           Product2 testProduct = [Select Id from Product2 where Name = 'TestPro'];
           ID prodId = testProduct.id;
           Test.startTest();                
           ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
           mtscopedata.buildScopeitTask();   
           mtscopedata.buildScopeitEmployee(employeeBillrate);
           delete testScopeitProj;
           Test.stopTest();
    }
    
    
}