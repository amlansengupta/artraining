/*Purpose: Test Class for providing code coverage to correct the contact share records for all contact team members
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   11/08/2013  Created test class
============================================================================================================================================== 
*/
@isTest
public class test_DC_ContactShareCorrectionBatchable{
    
    /*
     * @Description : Test method to correct the contact share records for all contact team members
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void test_contact_share(){
    
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Level_1_Descr__c = 'Merc';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        User tUser = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        Mercer_TestData mtest = new  Mercer_TestData ();
        tUser = Mercer_TestData.createUser(mercerStandardUserProfile, 'tUser', 'tLastName', tUser);  
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName1';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.DUNS__c = '444';
        insert testAccount;
        
        contact c = new contact();
        c.firstname ='test firstname';
        c.lastname = 'test lastname';
        c.accountid = testAccount.id;
        c.email = 'xyzq2@abc06.com';
        insert c;
        
        contact_team_member__c ctm = new contact_team_member__c();
        ctm.contact__c = c.id;
        ctm.ContactTeamMember__c = tuser.id;
        insert ctm;
        
        Contactshare[] cs = [select id from contactshare where contactid =:c.id and userorgroupid=:tUser.id];
        if(cs.size()>0) delete cs;
        
        system.runAs(tUser){
            ID Batchprocessid = database.executeBatch(new DC_ContactShareCorrectionBatchable(), Integer.valueOf(system.label.CL01_AutoMergeBatchSize));  
        }
        
    
    }

}