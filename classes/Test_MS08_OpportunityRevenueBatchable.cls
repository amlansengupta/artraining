/*Purpose: Test Class for providing code coverage to MS08_OpportunityRevenueBatchable class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   05/18/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                                               Date                    Modified By
12638:Removing Step                                      17-Jan-2019             Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_MS08_OpportunityRevenueBatchable  {
    /*
     * @Description : Test method to provide data coverage to MS08_OpportunityRevenueBatchable batchable class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest()
     {  
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getsystemAdminUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        System.runAs(user1){
        Colleague__c Coll = new Colleague__c();
        Coll.Name = 'Colleague';
        Coll.EMPLID__c = '98765421';
        Coll.LOB__c = '12345';
        Coll.Last_Name__c = 'TestLastName';
        Coll.Empl_Status__c = 'Active';
        Coll.Email_Address__c = 'abc@accenture.com';
        insert Coll;
        
        Account Acc = new Account();
        Acc.Name = 'TestAccountName';
        Acc.BillingCity = 'City';
        Acc.BillingCountry = 'Country';
        Acc.BillingStreet = 'Street';
        Acc.Relationship_Manager__c = Coll.Id;
        Acc.One_Code__c = '123';
        insert Acc;
        
        Contact objCon = new Contact();
        objCon.lastname = 'test Lastname';
        objCon.email = 'test@contact.com';
        objCon.AccountId = Acc.Id;
        insert objCon;
        
        Opportunity opp= new Opportunity();
        opp.Name = 'Test Opportunity3' + String.valueOf(Date.Today());
        opp.Type = 'New Client';
        //Request Id:12638 commenting step
        //opp.Step__c = 'Identified Deal';
        opp.CloseDate = Date.Today();
        opp.CurrencyIsoCode = 'USD';
        opp.AccountId = Acc.id;
        opp.StageName = 'Closed / Won';
        opp.Total_Opportunity_Revenue_USD__c = 90000.00;
        opp.Opportunity_Office__c= 'Montreal - McGill';
        opp.Buyer__c = objCon.Id;
        Test.startTest();
        insert opp;      
       
            
            database.executeBatch(new MS08_OpportunityRevenueBatchable(), 500);
            Test.StopTest();
       }  
    }
}