/*
Purpose: This test class provides data coverage to AP24_CampaignBatchable class
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    	 Arijit  03/29/2013  Created Test class
=============================================================================================================================================== 
*/
@isTest
private class Test_AP24_CampaignBatchable {
	/*
     * @Description : Test method to provide data coverage to AP24_CampaignBatchable class
     * @ Args       : null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Campaign camp = new Campaign();
        camp.Name = 'TestCampaign';
        camp.CurrencyIsoCode = 'AED';
        camp.Scope__c = 'Global';
        camp.EndDate = date.Today() - 1;
        camp.IsActive = true;
        insert Camp;
        
         User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
        string query = 'Select Id, EndDate, Status from Campaign where EndDate < Today AND IsActive = True';
        database.executeBatch(new AP24_CampaignBatchable(query), 500); 
        }
    }
}