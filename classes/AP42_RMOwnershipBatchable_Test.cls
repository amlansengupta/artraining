@isTest
public class AP42_RMOwnershipBatchable_Test {
	
    @isTest
    public static void testRMOwnershipBatch(){
        Integer month = System.Today().month();
        Integer year = System.Today().year();
       User user1 = new User();
       String adminprofile= Mercer_TestData.getsystemAdminUserProfile();      
       user1 = Mercer_TestData.createUser1(adminprofile,'usert7', 'usrLstName7', 7);
       system.runAs(User1){
          
         Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
         testMog.Name = 'ABC';
         testMog.Market__c = 'United States';
         testMog.Sub_Market__c = 'Canada – National';
         testMog.Region__c = 'Manhattan';
         testMog.Sub_Region__c = 'Canada';
         testMog.Country__c = 'Canada';
         testMog.Isupdated__c = true;
         testMog.Office_Code__c = '123';
         testMog.Office__c = 'US';
         insert testMog;
         
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '12345678901';
        testColleague.Location__c = testMog.Id;
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
        Account testAcc = new Account();
        testAcc.Name = 'TestAccountName';
           testAcc.OwnerId = user1.Id;
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingCountry = 'TestCountry';
        testAcc.BillingStreet = 'Test Street';
        testAcc.Relationship_Manager__c = testColleague.Id;
          testAcc.OwnerId= '005E00000028Q8P';  
        insert testAcc;
        
       
        Mercer_Office_Geo_D__c testMog1 = new Mercer_Office_Geo_D__c();
         testMog1.Name = 'ABC2';
         testMog1.Market__c = 'United States';
         insert testMog1; 
             
        testColleague.Location__c = testMog1.Id;
        update testColleague;
        
        
        List<Account> testAccount1 =[Select Id, OwnerId, Relationship_Manager__r.EMPLID__c, Name,BillingCountry From Account where Name =: 'TestAccountName' and (OwnerId = '005E00000028Q8P' or OwnerId = '005E0000002A6sM') AND Relationship_Manager__r.EMPLID__c <> null Limit 1];
        
        update testAccount1;
          String query = 'Select Id, OwnerId, Relationship_Manager__r.EMPLID__c Name From Account where (OwnerId = 005E00000028Q8P or OwnerId = 005E0000002A6sM) AND Relationship_Manager__r.EMPLID__c <> null';
       
    	
 		Map<String, User> rmOwnerMap = new Map<String, User>();
           rmOwnerMap.put(testColleague.EMPLID__c, user1);
       database.executeBatch(new AP42_RMOwnershipBatchable(rmOwnerMap));
       // Database.BatchableContext BC;
        //Batch_UpdateOpportunityFields.execute(BC,testOpportunity1);  
  
       }
        
    }
}