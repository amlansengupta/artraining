/*Purpose: Apex test class to to provide coverage for AP109_AccountStatusService
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi     8/9/2014   Provide coverage for AP109_AccountStatusService class
============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class AP109_AccountStatusService_Test{
    /*
    * @Description : Test method to provide  coverage to AP109_AccountStatusService 
    */
    static testmethod void OneCodestatusCheckTest(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
        //insert colleague
        Colleague__c colrec =mdata.buildColleague();
        //insert Account
        Account accrec =mdata.buildAccount();
        accrec.One_Code__c ='TEAM65';
        accrec.One_Code_Status__c='Inactive';
        accrec.MercerForce_Account_Status__c = 'No Activity - OBSOLETE';
        update accrec;
        Test.startTest();
        AP109_AccountStatusService.readAccountStatus(accrec.One_Code__c);
        Test.StopTest();
        }
    }
    
        static testmethod void OneCodestatusCheckTest1(){
    
        Mercer_TestData mdata = new Mercer_TestData();
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
        //insert colleague
        Colleague__c colrec =mdata.buildColleague();
        //insert Account
        Account accrec =mdata.buildAccount();
        accrec.One_Code__c ='TEAM65';
        accrec.One_Code_Status__c='Active';
        accrec.MercerForce_Account_Status__c = 'No Activity - OBSOLETE';
        update accrec;
        
        Contact conrec = mdata.buildContact();
        conrec.AccountId = accrec.Id;
        update conrec;
        
        conrec.Contact_Status__c = 'Inactive';
        update conrec;
        
        Test.startTest();
        AP109_AccountStatusService.readAccountStatus(accrec.One_Code__c);
        Test.StopTest();
        }
    }
}