/*************************************************************************************************************
User Story : US518
Purpose : This apex class is created to support the 'Save As Template' Custom link on ScopeIt Project.
Created by : Soumil Dasgupta
Created Date : 25/12/2018
Project : MF2
****************************************************************************************************************/

/***************************************************************************************************************
User Story : US518
Purpose : This apex class is created as the extension of MF2_ScopeItProjectSaveAsTemplate.
Created by : Soumil Dasgupta
Created Date : 25/12/2018
Project : MF2
****************************************************************************************************************/

Public class MF2_ScopeItProjectSaveAsTemplateApex {
    public String ScopeOpportunityId {get;set;}
    //public String ScopeItrecord {get;set;}
    
    public MF2_ScopeItProjectSaveAsTemplateApex(ApexPages.StandardSetController controller){
    //List<String> Scopeitrecords = ApexPages.currentPage().getParameters().get('ScopeitRecords');
    //ScopeItrecords = Apexpages.currentPage().getParameters().get('ScopeItrecords');
    //List<Scopeit_project__c> scopeItRecords = controller.getrecords();
    /*ScopeItrecords = new List<String>();
	System.debug('ScopeItRecords '+ ScopeItrecords);*/
    ScopeOpportunityId = ApexPages.currentPage().getParameters().get('OppId');
        System.debug('opp '+ScopeOpportunityId);
    
    }
    
    public pagereference redirectToOpp(){
        Pagereference pageRef = new Pagereference('/'+ScopeOpportunityId);
        return pageRef;
    }
    
    
    
}