/*Purpose: This Apex class implements schedulable interface to schedule MS19_NoActivityObsolteBatchable class.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR       DATE                    DETAIL 
   1.0 -              Srishty        05/17/2013       Created Apex Schedulable class
============================================================================================================================================== 
*/
global class MS20_NoActivityObsoleteSchedulable implements schedulable{
  
  global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new MS19_NoActivityObsoleteBatchable(), Integer.valueOf(System.Label.CL81_MS19BatchSize)); 
    }
}