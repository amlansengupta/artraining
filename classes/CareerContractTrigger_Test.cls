/*Request# 17079
This test class is responsible to provide test coverage for CareerContractTrigger.
*/   
@isTest(SeeAllData=False)
public class CareerContractTrigger_Test 
{
    private static Mercer_TestData mtdata = new Mercer_TestData();
    private static User testUser1 = new User();
    
    public static testmethod void testCareerContractTrigger()
    {
        User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1)
        {
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName1';
            testAccount1.BillingCity = 'TestCity1';
            testAccount1.BillingCountry = 'TestCountry1';
            testAccount1.BillingStreet = 'Test Street1';
            //testAccount1.Relationship_Manager__c = collId;
            testAccount1.One_Code__c='ABC123XYZ1';
            //testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
            //testAccount1.Integration_Status__c = 'Error';
            insert testAccount1;
            Id accId = testAccount1.Id;
            Talent_Contract__c careerContractObject = new Talent_Contract__c();
            careerContractObject.Account__c = accId;
            careerContractObject.Agreement_Type__c = 'PIF';
            careerContractObject.Billing_Currency__c = 'USD';
            careerContractObject.CurrencyIsoCode = 'USD';
            careerContractObject.License_Start_Date__c	= date.Today();
            careerContractObject.License_Expiration_Date__c = date.Today();
            careerContractObject.Product__c = 'Mobility';
            careerContractObject.Product_Status__c = 'Implementation';
            careerContractObject.Service_License_Fee_Freq__c = 'Annual';
            careerContractObject.Term_in_Months__c = 12;
            
            Test.startTest();
            insert careerContractObject;
            Test.stopTest();
            
        }
        
    }
    public static testmethod void testCareerContractTrigger1()
    {
        String mercerSystemAdminUserProfile = Mercer_TestData.getmercerSystemAdminUserProfile();
        //user2 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser1.alias = 'usert1';
        testUser1.email = 'test@xyz.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'usrLstName';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = mercerSystemAdminUserProfile;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = randomName + '.usrLstName@xyz.com';
        testUser1.EmployeeNumber = '1234567890'+1;
        Database.insert(testUser1) ;
        System.runAs(testUser1)
        {
            Account testAccount1 = new Account();
            testAccount1.Name = 'TestAccountName1';
            testAccount1.BillingCity = 'TestCity1';
            testAccount1.BillingCountry = 'TestCountry1';
            testAccount1.BillingStreet = 'Test Street1';
            //testAccount1.Relationship_Manager__c = collId;
            testAccount1.One_Code__c='ABC123XYZ1';
            //testAccount1.One_Code_Status__c = 'Pending - Workflow Wizard';
            //testAccount1.Integration_Status__c = 'Error';
            insert testAccount1;
            Id accId = testAccount1.Id;
            Talent_Contract__c careerContractObject1 = new Talent_Contract__c();
            careerContractObject1.Account__c = accId;
            careerContractObject1.Agreement_Type__c = 'PIF';
            careerContractObject1.Billing_Currency__c = 'USD';
            careerContractObject1.CurrencyIsoCode = 'USD';
            careerContractObject1.License_Start_Date__c	= date.Today();
            careerContractObject1.License_Expiration_Date__c = date.Today();
            careerContractObject1.Product__c = 'Mobility';
            careerContractObject1.Product_Status__c = 'Implementation';
            careerContractObject1.Service_License_Fee_Freq__c = 'Annual';
            careerContractObject1.Term_in_Months__c = 12;
            
            Test.startTest();
            insert careerContractObject1;
            Test.stopTest();
            
        }
    }
}