/*
@Class Name		  : SVNSUMMITS_RecordTypeUtility
@Created by		  :
@Description	  : Record type utilities
*/
global with sharing class SVNSUMMITS_RecordTypeUtility {

    // Record types cache
    private static Map<Schema.SObjectType,Map<String,Id>> recordTypesCache;
    private static List<sObject> recordTypesResults;
    static {
        recordTypesCache = new Map<Schema.SObjectType,Map<String,Id>>();
        recordTypesResults = new List<sObject>();
    }

    // Returns a map of active, user-available RecordType IDs for a given SObjectType,
    // keyed by each RecordType's unique, unchanging DeveloperName
    global static Map<String, Id> getRecordTypeIdsByDeveloperName(Schema.SObjectType token) {

        // try to get cached record
        Map<String, Id> mapRecordTypes = recordTypesCache.get(token);

        // if not found create new map
        if (mapRecordTypes == null) {
            mapRecordTypes = new Map<String, Id>();
            recordTypesCache.put(token, mapRecordTypes);
        } else {
            // If we do, return our cached result immediately!
            return mapRecordTypes;
        }

        // Get the Describe Result
        Schema.DescribeSObjectResult obj = token.getDescribe();

        //Check if we already queried all recordtypes.
        if (recordTypesResults == null || recordTypesResults.isEmpty()) {
            // Obtain ALL Active Record Types
            // (We will filter out the Record Types that are unavailable
            // to the Running User using Schema information)
            String soql = 'SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE IsActive = TRUE';
            try {
                recordTypesResults = Database.query(soql);
            } catch (Exception ex) {
                recordTypesResults = new List<SObject>();
            }
        }

        // Obtain the RecordTypeInfos for this SObjectType token
        Map<Id,Schema.RecordTypeInfo> recordTypeInfos = obj.getRecordTypeInfosByID();

        // Loop through all of the Record Types we found,
        // and weed out those that are unavailable to the Running User
        for (SObject rt : recordTypesResults) {
            if (recordTypeInfos.get(rt.Id) != null) {

                mapRecordTypes.put(String.valueOf(rt.get('DeveloperName')),rt.Id);

//                // We are not currently using this
//                //  Check to see if the record type is available to the current user
//                if (recordTypeInfos.get(rt.Id).isAvailable()) {
//                    // This RecordType IS available to the running user,
//                    // so add it to our map of RecordTypeIds by DeveloperName
//                    mapRecordTypes.put(String.valueOf(rt.get('DeveloperName')),rt.Id);
//                }
//                else {
//                    System.debug('The record type ' + rt.get('DeveloperName') + ' for object ' + rt.get('sObjectType') + ' is not availiable for the user.');
//                }
            }
        }

        return mapRecordTypes;
    }

}