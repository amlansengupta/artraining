/*
Purpose: This class is being used by the Ranking trigger, this is created to have Ranking functionality 
==============================================================================================================================================
History
VERSION  AUTHOR  DATE        DETAIL    
1.0 -    Gyan    03/17/2013  Created Utility Class for TRG23_RankingAfterTrigger

=============================================================================================================================================== 
*/
public class AP81_RankingTriggerUtil 
{
    /*
   * @Description : This method is called on after insertion, after updation and after deletion of an Ranking record
                    and concatenates all the Ranking associated with an Account and stores them in a field                 
   * @ Args       : List<Ranking__c>   
   * @ Return     : void
   */
   public static void concatenatedRanking(List<Ranking__c> rankList)
   {
           Set<Id> rankIDSet = new Set<Id>();
          // List variable of Account  
          List<Account> lstAccUpdate = new List<Account>() ;

          // Iterate through Ranking and fetch the Account IDs and add them in a set.  
          for(Ranking__c ctm : rankList)
          {
              rankIDSet.add(ctm.Account__c);
          }                 
          //Map vriable for storing ID and Name Account
          Map<Id,Account> AccMap =new Map<ID,Account>([Select Concatenated_Ranking__c, (Select Ranking_Description__c From Ranking__r) From Account where ID IN : rankIDSet]);
          
          // Iterate through Account map and fetch the Account IDs
          for(Id aid:AccMap.keyset())
          {
             Account acc =  AccMap.get(aid);
              if(acc.Ranking__r!=null && acc.Ranking__r.size()>0) 
              {
                    acc.Concatenated_Ranking__c = '';   
                     
                     // Iterate through Ranking associated with the Account 
                     for(Ranking__c ctm : acc.Ranking__r)
                     {    
                         // Check if newly added Ranking already exist in list or Not?
                         if(ctm.Ranking_Description__c <>null && !acc.Concatenated_Ranking__c.contains(ctm.Ranking_Description__c)) 
                          {
                            //Logic to Not add ; in last of String if it contains only one value.
                           if(acc.Concatenated_Ranking__c.length()!= 0)
                            acc.Concatenated_Ranking__c += ';'+ctm.Ranking_Description__c;
                           else
                              acc.Concatenated_Ranking__c += ctm.Ranking_Description__c;
                          }
                     }
                      lstAccUpdate.add(acc);  
              }
              else
              {
                     acc.Concatenated_Ranking__c = '' ;             
                     lstAccUpdate.add(acc);   
              }
                    
        }

        if(!lstAccUpdate.isEmpty())
        Database.update(lstAccUpdate) ;
       
       }

}