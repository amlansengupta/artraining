/*Purpose:  This Apex class contains static methods to convert curreny field on Cross Op Co Referral from the entered Currency to USD==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   01/21/2013  Created Apex to support TRG08
============================================================================================================================================== 
*/
public class AP16_CurrencyConversionUtil {


// Map to store ID and cross op co referral object
    public static Map<ID,Cross_OpCo_Referral__c> IdCrossOpMap = new Map<ID,Cross_OpCo_Referral__c>();
// List of Cross Op Co referral type
    public static List<Cross_OpCo_Referral__c> crossOpnewList = new List<Cross_OpCo_Referral__c>();
// Set of Isocodes    
    public static Set<String> IsoCodeSet = new Set<String>();
// Map of Isocode and Rate    
    public static Map<String,Decimal> IsoRateMap = new Map<String,Decimal>();

/*
     * @Description : This method is called on before insertion of Cross Op Co Referral
     * @ Args       : Trigger.newmap
     * @ Return     : void
     */

    public static void convertCurrencyBeforeInsert(List<Cross_OpCo_Referral__c> triggernew)
    {

// Iterate through the list of new records and create a map of  ID and crossOpCo record             
                for(Cross_OpCo_Referral__c crossOp : triggernew)
                {
                    IdCrossOpMap.put(crossOp.Id,crossOp);
                    IsoCodeSet.add(crossOp.CurrencyIsoCode);
                }
                                 
                // Iterate throught the Currency type object and create a map od Iso code and conversionrate
                
                for(currencytype ctype :[SELECT conversionrate,isocode FROM currencytype WHERE isocode IN :IsoCodeSet])
                {
                    IsoRateMap.put(ctype.isocode,ctype.conversionrate);
                }
                
                // Iterate through the saved values of the Record for all the Ids in the map ( records that are being inserted)             
                for(Cross_OpCo_Referral__c crossOp : triggernew)
                {
                
                        // Assign the converted amount value to the field which stored the USD value                    
                        Decimal ConRate = (Decimal)IsoRateMap.get(crossOp.CurrencyIsoCode);
                        crossOp.Amount_USD__c = crossOp.Amount__c / ConRate;
                        // Reassign the value of amount which was converted so that it stores the old value                     
                        crossOp.Amount__c = IdCrossOpMap.get(crossOp.Id).Amount__c;
                        // Add the record to the list for update                        
                        crossOpnewList.add(crossOp);
                    
                }
               
                crossOpnewList.clear();
                IsoRateMap.clear();
                IdCrossOpMap.clear();
                IsoCodeSet.clear();    
            }
            
/*
     * @Description : This method is called on after updation of Cross Op Co Referral
     * @ Args       : Trigger.newmap,Trigger.oldmap
     * @ Return     : void
     */   
            
     public static void convertCurrencyAfterUpdate(Map<Id, Cross_OpCo_Referral__c> triggernewmap, Map<Id, Cross_OpCo_Referral__c> triggeroldmap)
    {
            // Iterate through the list of new records and create a map of  ID and crossOpCo record         
            for(Cross_OpCo_Referral__c crossOp : triggernewmap.values())
            {
                 
                // Check if the value of the Amount field or the Currency is changed                
                if((crossOp.Amount__c <> triggeroldmap.get(crossOp.Id).Amount__c)||(crossOp.CurrencyIsoCode <> triggeroldmap.get(crossOp.Id).CurrencyIsoCode))
                {
                    
                    // Put the changed records in a map                 
                    IdCrossOpMap.put(crossOp.Id,crossOp);
                    IsoCodeSet.add(crossOp.CurrencyIsoCode);
                }
            }
            
            if(!IdCrossOpMap.isempty())
            {
                for(currencytype ctype :[SELECT conversionrate,isocode FROM currencytype WHERE isocode IN :IsoCodeSet])
                    {
                        IsoRateMap.put(ctype.isocode,ctype.conversionrate);
                    }

                // Check if any record has either of the 2 fields updated           
                if(!IsoRateMap.isempty())
                {
                    // Query the values and convert the Amount entered in any currency to USD               
                    for(Cross_OpCo_Referral__c crossOp : [Select Amount_USD__c,Amount__c,CurrencyIsoCode FROM Cross_OpCo_Referral__c where ID in :IdCrossOpMap.keyset()])
                        {
                                
                                Decimal ConRate = (Decimal)IsoRateMap.get(crossOp.CurrencyIsoCode);
                                // Assign the converted amount value to the field which stored the USD value                    
                                crossOp.Amount_USD__c = crossOp.Amount__c / ConRate;
                                // Reassign the value of amount which was converted so that it stores the old value                         
                                
                                // Add the record to the list for update                                                    
                                crossOpnewList.add(crossOp);
                            
                        }
                        // check if the list is empty and update it if not      
                        
                 if(!crossOpnewList.isEmpty()) Database.Update(crossOpnewList, false);                   
                    
                }
            }
            crossOpnewList.clear();
            IsoRateMap.clear();
            IdCrossOpMap.clear();
            IsoCodeSet.clear(); 
   }
    

}