/*Purpose: This class contains static methods which are used by batch class AP06
==============================================================================================================================================
The methods called perform following functionality:

•   Gets the one code from account workflow message field
•   Validates if scenario holds true for the account
•   Delete duplicate records

History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   12/24/2012  Created this util class
============================================================================================================================================== 
*/

public class AP07_AutoMergeUtil {
    
    
    /*
     *  @Method Name: getOneCode
     *  @Description: Method gets the one code from account workflow message field
     *  @Args : String
     *  @return: String  
     */
    public static String getOneCode(String workflowMessage)
    {
        String onecode;
        try
        {
            onecode = workflowMessage;
            onecode = workflowMessage.right(6).trim();
            
        }catch(Exception exe)
        {
            onecode = null;     
        }
        return onecode;
    }
    
    /*
     *  @Method Name: isScenarioOne
     *  @Description: This method validates if the scenario 1 hold true for the account 
     *  @Args : Account
     *  @return: boolean  
     */
    public static boolean isScenarioOne(Account account)
    {
        boolean isScenarioOne = false;
        if(account.Workflow_Submit_Status__c == System.label.CL02_AccountMergeWFSubmitStatus
                && account.One_Code_Status__c == System.label.CL03_AccountMergeOneCodeStatus1)
        {
            isScenarioOne = true;   
        }   
        return isScenarioOne;
    }
    
    /*
     *  @Method Name: isScenarioTwo
     *  @Description: This method validates if the scenario 2 hold true for the account 
     *  @Args : Account
     *  @return: boolean  
     */
    public static boolean isScenarioTwo(Account account)
    {
        boolean isScenarioTwo = false;
        if(account.Type == 'Marketing' 
            && account.One_Code_Status__c == System.label.CL04_AccountMergeOneCodeStatus2 
                && account.Inactivation_Code__c == System.label.CL05_InactivationCode)
        {
            isScenarioTwo = true;   
        }
        return isScenarioTwo;
    }


    /*
     *  @Method Name: isScenarioThree
     *  @Description: This method validates if the scenario 3 hold true for the account 
     *  @Args : Account
     *  @return: boolean  
     */
    public static boolean isScenarioThree(Account account)
    {
        boolean isScenarioThree = false;
        if(account.Workflow_Submit_Status__c == System.label.AccountMergeWFSubmitStatusForDuplicateOneCode 
            && account.One_Code__c == null && account.Duplicate_Client_Code_Rejected__c!=null)
        {
            isScenarioThree = true;   
        }
        return isScenarioThree ;
    }    
    

     /*
     *  @Method Name: deleteDuplicateRecords
     *  @Description: This method deletes duplicate records. 
     *  @Args : Map<String, List<Account>>
     *  @return: Map<String, List<Account>>  
     */
    public static Map<String, List<Account>> mergeAccounts(Map<String, List<Account>> oneCodeAccountListMap)
    {
        List<Account> accountForDelete = new List<Account>();
        List<AccountTeamMember> accTeamMemberForUpdate = new List<AccountTeamMember>();       
        List<Opportunity> opporutnityForUpdate = new List<Opportunity>();
        List<Contact> contactForUpdate = new List<Contact>();
        List<Task> taskForUpdate = new List<Task>();
        List<Revenue_Target__c> revenueForUpdate = new List<Revenue_Target__c>();
        List<Sensitve_At_Risk__c> sensitiveForUpdate = new List<Sensitve_At_Risk__c>();
        List<Contracts_Renewals__c> contractsForUpdate = new List<Contracts_Renewals__c>();
        List<Cross_OpCo_Referral__c> crossopcoForUpdate = new List<Cross_OpCo_Referral__c>();
        List<Incumbent__c> incumbentForUpdate = new List<Incumbent__c>();
        List<Account_Revenue__c> accRevenueForUpdate = new List<Account_Revenue__c>();
        List<Event> eventForUpdate = new List<Event>();
        List<Note> noteForUpdate = new List<Note>();
        List<Attachment> attachmentForUpdate = new List<Attachment>();
        List<Contact_Affiliations__c> contactAffliationsForUpdate = new List<Contact_Affiliations__c>();
        Map<string, string> parentIdAccountIdMap = new Map<string,string>();
        
        for(Account account : [SELECT Id,Name, Account_ID__c,Auto_Merge_Date__c,Auto_Merge_Flag__c,Auto_Merge_Status__c, 
                               Inactivation_Code__c, One_Code_Status__c,One_Code__c, Workflow_Message__c, Duplicated_Client_Code__c, 
                               Workflow_Submit_Status__c
                               FROM Account
                               WHERE One_Code__c IN : oneCodeAccountListMap.keyset() 
                               AND Auto_Merge_Flag__c = false])
        {
            if(oneCodeAccountListMap.containskey(account.One_Code__c))
            {   
                accountForDelete.addAll(oneCodeAccountListMap.get(account.One_Code__c));

                for(Account acc : oneCodeAccountListMap.get(account.One_Code__c))
                {
                    List<Opportunity> relatedOpps = acc.Opportunities;  
                    for(Opportunity opp : relatedOpps)
                    {
                        opp.AccountId = account.Id;
                        opporutnityForUpdate.add(opp);
                    }
                    
                    List<Contact> relatedCons = acc.Contacts;   
                    for(Contact con : relatedCons)
                    {
                        con.AccountId = account.Id;
                        contactForUpdate.add(con);
                    }
                    
                    List<Task> relatedTasks = acc.Tasks;    
                    for(Task tas : relatedTasks)
                    {
                        tas.whatId = account.Id;
                        taskForUpdate.add(tas);
                    }
                    
                    List<Note> relatedNotes = acc.Notes;
                    for(Note note : relatedNotes)
                    {
                        Note note1 = note.clone(false);
                        note1.ParentId = account.Id;
                        noteForUpdate.add(note1);
                    }
                    
                    List<Event> relatedEvents = acc.Events;    
                    for(Event eve : relatedEvents)
                    {
                        eve.whatId = account.Id;
                        eventForUpdate.add(eve);
                    }
                    
                    List<AccountTeamMember> relatedAccTeam = acc.AccountTeamMembers;  
                    for(AccountTeamMember aTMem : relatedAccTeam)
                    {
                        AccountTeamMember aTMem1 = aTMem.clone(false);
                        aTMem1.AccountId = account.Id;
                        accTeamMemberForUpdate.add(aTMem1);
                    }
                    
                    List<Revenue_Target__c> relatedRevenue = acc.Revenue_Target__r;  
                    for(Revenue_Target__c rev : relatedRevenue)
                    {
                        Revenue_Target__c rev1 = rev.clone(false);
                        rev1.Account__c = account.Id;
                        revenueForUpdate.add(rev1);
                    }
                   
                   List<Sensitve_At_Risk__c> relatedSensitive = acc.Sensitve_At_Risk__r;  
                    for(Sensitve_At_Risk__c sens : relatedSensitive)
                    {
                        Sensitve_At_Risk__c sens1 = sens.clone(false);
                        sens1.Account__c = account.Id;
                        sensitiveForUpdate.add(sens1);
                    } 
                    
                    List<Contracts_Renewals__c> relatedContracts = acc.Contracts_Renewals__r;   
                    for(Contracts_Renewals__c  conts : relatedContracts)
                    {
                        Contracts_Renewals__c conts1 = conts.clone(false);
                        conts1.Account__c = account.Id;
                        ContractsForUpdate.add(conts1);
                    }
                    
                    List<Cross_OpCo_Referral__c> relatedCrossOpCo = acc.Cross_OpCo_Referrals__r;    
                    for(Cross_OpCo_Referral__c cross : relatedCrossOpCo)
                    {
                        Cross_OpCo_Referral__c cross1 = cross.clone(false);
                        cross1.Account__c = account.Id;
                        crossopcoForUpdate.add(cross1);
                    }
                    
                    List<Account_Revenue__c> relatedAccRevenue = acc.Account_Revenues__r;
                    for(Account_Revenue__c accRev : relatedAccRevenue)
                    {
                        Account_Revenue__c accRev1 = accRev.clone(false);
                        accRev1.Account__c = account.Id;
                        accRevenueForUpdate.add(accRev1);
                    }
                    
                    List<Incumbent__c> relatedInc = acc.Incumbents1__r;
                    for(Incumbent__c inc : relatedInc)
                    {
                        Incumbent__c inc1 = inc.clone(false);
                        inc1.Account__c = account.Id;
                        inc1.Incumbent_Service_Provider__c  = account.Id;
                        incumbentForUpdate.add(inc1);
                    }
                    
                    List<Contact_Affiliations__c> relatedConAff = acc.Employee_History__r;
                    for(Contact_Affiliations__c conAff : relatedConAff)
                    {
                        conAff.Account__c = account.Id;
                        contactAffliationsForUpdate.add(conAff);
                    }
                    
                    parentIdAccountIdMap.put(acc.Id, account.Id);
                    
                }
                oneCodeAccountListMap.remove(account.One_Code__c);
            }
        }
        
        for(Attachment attach : [SELECT Body,BodyLength,ContentType,CreatedById,CreatedDate,Description,Id,IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,Name,OwnerId,ParentId,SystemModstamp FROM Attachment where ParentId IN :parentIdAccountIdMap.Keyset()])
        {
            Attachment attach1 = attach.clone(false);
            attach1.ParentId = parentIdAccountIdMap.get(attach.ParentId);
            attachmentForUpdate.add(attach1);
        }
        
        if(!opporutnityForUpdate.isEmpty()) Database.update(opporutnityForUpdate, false);
        if(!contactForUpdate.isEmpty()) Database.update(contactForUpdate, false);
        if(!taskForUpdate.isEmpty()) Database.update(taskForUpdate, false);
        if(!accTeamMemberForUpdate.isEmpty()) Database.insert(accTeamMemberForUpdate, false);
        if(!revenueForUpdate.isEmpty()) Database.insert(revenueForUpdate, false);
        if(!sensitiveForUpdate.isEmpty()) Database.insert(sensitiveForUpdate, false);
        if(!ContractsForUpdate.isEmpty()) Database.insert(ContractsForUpdate, false);
        if(!crossopcoForUpdate.isEmpty()) Database.insert(crossopcoForUpdate, false);
        if(!accRevenueForUpdate.isEmpty()) Database.insert(accRevenueForUpdate, false);
        if(!eventForUpdate.isEmpty()) Database.update(eventForUpdate, false);
        if(!incumbentForUpdate.isEmpty()) Database.insert(incumbentForUpdate, false);
        if(!noteForUpdate.isEmpty()) Database.insert(noteForUpdate, false);
        if(!attachmentForUpdate.isEmpty()) Database.insert(attachmentForUpdate, false);
        if(!contactAffliationsForUpdate.isEmpty()) Database.update(contactAffliationsForUpdate, false);
        if(!accountForDelete.isEmpty())Database.delete(accountForDelete, false);
        return oneCodeAccountListMap;
    }
    
    public static void mergeAccounts(Map<String, List<Account>> oneCodeAccountListMap, boolean check)
    {
        List<Account> accountForDelete = new List<Account>();   
        List<Account> accountForUpdate = new List<Account>();
        List<Opportunity> oppsForUpdate = new List<Opportunity>();
        List<AccountTeamMember> accTeamForUpdate = new List<AccountTeamMember>();
        List<Contact> consForUpdate = new List<Contact>();
        List<Task> tasForUpdate = new List<Task>();
        List<Revenue_Target__c> revForUpdate = new List<Revenue_Target__c>();
        List<Sensitve_At_Risk__c> sensForUpdate = new List<Sensitve_At_Risk__c>();
        List<Contracts_Renewals__c> contForUpdate = new List<Contracts_Renewals__c>();
        List<Cross_OpCo_Referral__c> crossForUpdate = new List<Cross_OpCo_Referral__c>();
        List<Incumbent__c> incForUpdate = new List<Incumbent__c>();
        List<Account_Revenue__c> accRevForUpdate = new List<Account_Revenue__c>();
        List<Event> eveForUpdate = new List<Event>();
        List<Note> notForUpdate = new List<Note>();
        List<Attachment> attachForUpdate = new List<Attachment>();
        List<Contact_Affiliations__c> conAffsForUpdate = new List<Contact_Affiliations__c>();
        Map<string, string> parentIdAccountIdMap = new Map<string,string>();
        if(check)
        {
            // iterate throug map key set
            for(String oneCode : oneCodeAccountListMap.keyset())
            {
                // get accounts for the one code
                List<Account> accounts = oneCodeAccountListMap.get(oneCode);
                // if account size greater then 1
                if(accounts <> null)
                {
                    if(accounts.size() > 1)
                    {
                        // get the first record from list and set the auto merge flag to false
                        Account account = accounts[0];
                        account.Auto_Merge_Flag__c = false;
                        accountForUpdate.add(account);
                        
                        Integer count = 1;
                        // delete rest of the accounts in list
                        while(count <= accounts.size() - 1)
                        {
                            
                            accountForDelete.add(accounts[count]);
                            Opportunity[] opps = accounts[count].Opportunities;
                            for(Opportunity opp : opps)
                            {
                                opp.AccountId = account.Id;
                                oppsForUpdate.add(opp);
                            }
                            Contact[] cons = accounts[count].Contacts;
                            for(Contact con : cons)
                            {
                                con.AccountId = account.Id;
                                consForUpdate.add(con);
                            }
                            Task[] tasks = accounts[count].Tasks;
                            for(Task tas : tasks)
                            {
                                tas.whatId = account.Id;
                                tasForUpdate.add(tas);
                            }
                            
                            List<Event> relatedEvents = accounts[count].Events;    
                            for(Event eve : relatedEvents)
                            {
                                eve.whatId = account.Id;
                                eveForUpdate.add(eve);
                            }
                            
                            AccountTeamMember[] AccTeams = accounts[count].AccountTeamMembers;  
                            for(AccountTeamMember aTeam : AccTeams)
                            {
                                AccountTeamMember aTeam1 = aTeam.clone(false);
                                aTeam1.AccountId = account.Id;
                                accTeamForUpdate.add(aTeam1);
                            }
                            
                            Revenue_Target__c[] Revenues = accounts[count].Revenue_Target__r;   
                            for(Revenue_Target__c rev : Revenues)
                            {
                                Revenue_Target__c rev1 = rev.clone(false);
                                rev1.Account__c = account.Id;
                                revForUpdate.add(rev1);
                            }
                    
                            Sensitve_At_Risk__c[] Sensitives = accounts[count].Sensitve_At_Risk__r;     
                            for(Sensitve_At_Risk__c sens : Sensitives)
                            {
                                Sensitve_At_Risk__c sens1 = sens.clone(false);
                                sens1.Account__c = account.Id;
                                sensForUpdate.add(sens1);
                            }
                    
                            Contracts_Renewals__c[] Contracts = accounts[count].Contracts_Renewals__r;  
                            for(Contracts_Renewals__c  conts : Contracts)
                            {
                                Contracts_Renewals__c conts1 = conts.clone(false);
                                conts1.Account__c = account.Id;
                                ContForUpdate.add(conts1);
                            }
                    
                            Cross_OpCo_Referral__c[] CrossOpCos = accounts[count].Cross_OpCo_Referrals__r;  
                            for(Cross_OpCo_Referral__c cross : CrossOpCos)
                            {
                                Cross_OpCo_Referral__c cross1 = cross.clone(false);
                                cross1.Account__c = account.Id;
                                crossForUpdate.add(cross1);
                            }
                            
                            List<Account_Revenue__c> relatedAccRevenue = accounts[count].Account_Revenues__r;
                            for(Account_Revenue__c accRev : relatedAccRevenue)
                            {
                                Account_Revenue__c accRev1 = accRev.clone(false);
                                accRev1.Account__c = account.Id;
                                accRevForUpdate.add(accRev1);
                            }
                            
                            List<Contact_Affiliations__c> relatedConAff = accounts[count].Employee_History__r;
                            for(Contact_Affiliations__c conAff : relatedConAff)
                            {
                                conAff.Account__c = account.Id;
                                conAffsForUpdate.add(conAff);
                            }
                            
                            List<Incumbent__c> relatedInc = accounts[count].Incumbents1__r;
                            for(Incumbent__c inc : relatedInc)
                            {
                                Incumbent__c inc1 = inc.clone(false);
                                inc1.Account__c = account.Id;
                                inc1.Incumbent_Service_Provider__c  = account.Id;
                                incForUpdate.add(inc1);
                            }
                            
                            List<Note> relatedNotes = accounts[count].Notes;
                            for(Note note : relatedNotes)
                            {
                                Note note1 = note.clone(false);
                                note1.ParentId = account.Id;
                                notForUpdate.add(note1);
                            }
                            
                            parentIdAccountIdMap.put(accounts[count].Id, account.Id);
        
                             count ++;
                        }   
                        
                        
                    }
                    else
                    {
                        Account account = accounts[0];
                        account.Auto_Merge_Flag__c = false;
                        accountForUpdate.add(account);
                    }   
                }
                
            }
            
            for(Attachment attach : [SELECT Body,BodyLength,ContentType,CreatedById,CreatedDate,Description,Id,IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,Name,OwnerId,ParentId,SystemModstamp FROM Attachment where ParentId IN :parentIdAccountIdMap.Keyset()])
            {
                Attachment attach1 = attach.clone(false);
                attach1.ParentId = parentIdAccountIdMap.get(attach.ParentId);
                attachForUpdate.add(attach1);
            }
            
            if(!oppsForUpdate.isEmpty()) Database.update(oppsForUpdate, false);
            if(!consForUpdate.isEmpty()) Database.update(consForUpdate, false);
            if(!tasForUpdate.isEmpty()) Database.update(tasForUpdate, false);
            if(!accTeamForUpdate.isEmpty()) Database.insert(accTeamForUpdate, false);
            if(!revForUpdate.isEmpty()) Database.insert(revForUpdate, false);
            if(!sensForUpdate.isEmpty()) Database.insert(sensForUpdate, false);
            if(!ContForUpdate.isEmpty()) Database.insert(ContForUpdate, false);
            if(!crossForUpdate.isEmpty()) Database.insert(crossForUpdate, false);
            if(!accountForUpdate.isEmpty()) Database.update(accountForUpdate, false);
            if(!accRevForUpdate.isEmpty()) Database.insert(accRevForUpdate, false);
            if(!eveForUpdate.isEmpty()) Database.update(eveForUpdate, false);
            if(!conAffsForUpdate.isEmpty()) Database.update(conAffsForUpdate, false);
            if(!incForUpdate.isEmpty()) Database.insert(incForUpdate, false);
            if(!notForUpdate.isEmpty()) Database.insert(notForUpdate, false);
            if(!attachForUpdate.isEmpty()) Database.insert(attachForUpdate, false);
            if(!accountForDelete.isEmpty()) Database.delete(accountForDelete, false);
        }
    }
    
}