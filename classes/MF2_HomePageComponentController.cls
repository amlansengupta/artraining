/**************************************************************************************************************
User story : 
Purpose : This apex class is created as the controller of MF2_HomePageCustomLinks.
Created by : Soumil Dasgupta
Created Date : 17/12/2018
Project : MF2
****************************************************************************************************************/

public class MF2_HomePageComponentController {
    @auraenabled
    public static List<HomePageQuickLinks__c> fetchCustomLinks(String linkLabel){
        List<HomePageQuickLinks__c> listOfQuickLinks = new List<HomePageQuickLinks__c>();
        List<User> userList = [Select EmployeeNumber from User where Id =:UserInfo.getUserId()];
        String employeeNumber;
        if(userList != null && userList.size() > 0){
            employeeNumber = userList.get(0).EmployeeNumber;
        }
        if(linkLabel == 'all'){
            Map<String,HomePageQuickLinks__c> quickLinkMap=new Map<String,HomePageQuickLinks__c>();
            
            for(HomePageQuickLinks__c link : HomePageQuickLinks__c.getAll().values()){
                if(link.Type__c == 'QUICK_LINK'){
                    if(link.UI_Label__c=='My Shared Contacts'){
                        link.Link__c = employeeNumber;
                    }
                    quickLinkMap.put(link.UI_Label__c.capitalize(),link);
                }
            }
            Set<String> quickLinkLabel = quickLinkMap.keySet();
            List<String> quickLinkLabelList=new List<String>();
            quickLinkLabelList.addAll(quickLinkLabel);
            quickLinkLabelList.sort();
            
            for(String label : quickLinkLabelList)
            {
                listOfQuickLinks.add(quickLinkMap.get(label));
            }
        }
        else if (linkLabel != 'all'){
            List<HomePageQuickLinks__c> listOfAllQuickLinks = HomePageQuickLinks__c.getAll().values();
            
            Map<String,HomePageQuickLinks__c> quickLinkMap=new Map<String,HomePageQuickLinks__c>();
            for(HomePageQuickLinks__c link : listOfAllQuickLinks){
                if(link.UI_Label__c == linkLabel && link.Type__c == 'QUICK_LINK'){
                    //  listOfQuickLinks.add(link);
                    quickLinkMap.put(link.UI_Label__c,link);
                }
            }
            Set<String> quickLinkLabel = quickLinkMap.keySet();
            List<String> quickLinkLabelList=new List<String>();
            quickLinkLabelList.addAll(quickLinkLabel);
            quickLinkLabelList.sort();
            
            for(String label : quickLinkLabelList)
            {
                listOfQuickLinks.add(quickLinkMap.get(label));
            }
        }
        return listOfQuickLinks;
    }
    
    @auraenabled
    public static List<String> fetchHomePageAlertsMessages(){
        List<String> listOfQuickLinks = new List<String>();
        for(HomePageQuickLinks__c link : HomePageQuickLinks__c.getAll().values()){
            if(link.Type__c == 'ALERT'){
                listOfQuickLinks.add(link.UI_Label__c);
            }
        }
        return listOfQuickLinks;
    }
    
}