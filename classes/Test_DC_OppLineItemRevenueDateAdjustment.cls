/* Purpose: This test class provides data coverage for updating Revenue satrt date and enddate on opty product for all open opportunities.
==================================================================================================================           
 
 History
 -------------------------
 VERSION     AUTHOR          DATE        DETAIL 
 1.0         Jagan         6/12/2013   Created test method for DC_OppLineItemRevenueDateAdjustment  batch class
 ==================================================================================================================*/ 
/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest(seeAllData = true)
private class Test_DC_OppLineItemRevenueDateAdjustment {
/*
 *  Method Name: myUnitTest
 *  Description: Method is used check for field update on opportunity product. 
 */
    static testMethod void myUnitTest() {
         //insert colleague
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '123456';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
         User user1 = new User();
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
               
        //insert Account
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount; 
        
        Test.startTest();    
        //insert opportunity
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;
        //Request Id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Opportunity_Office__c ='Hoboken - River';
        insert testOppty;
        Test.stopTest();
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        //insert opportunity product
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+750;
        opptyLineItem.Revenue_Start_Date__c = date.Today(); 
        opptylineItem.UnitPrice = 100.00;
        insert opptylineItem;
        
       
          //Test.startTest();    
        system.runAs(User1){ 
       
        database.executeBatch(new DC_OppLineItemRevenueDateAdjustment(), 100); 
        }
        //Test.stopTest();
       } 
      }