global class BatchToCopySTDataToNextYear implements Database.Batchable<sObject>,Database.Stateful{
	private String fromSelectedYear;
	private String toSelectedYear;
	private  string finalSuccessStr;
	private string finalFailStr;

    global BatchToCopySTDataToNextYear(String fromYear,String toYear) {
        fromSelectedYear=fromYear;
        toSelectedYear=toYear;
         String header = 'Account,Growth Plan,Threat,Record Status \n';
        string hearder2= 'Id,Account,Growth Plan,Threat,Record Status \n';
        finalSuccessStr=hearder2;
        finalFailStr=header;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
		if(fromSelectedYear!=null && fromSelectedYear!=''){
			return Database.getQueryLocator('select id,Name,Account__c,fcstPlanning_Year__r.name from Growth_Plan__c where fcstPlanning_Year__r.name =:fromSelectedYear  ');
		}
		else{
			return null;
		}	
	}
    global void execute(Database.BatchableContext bc, List<Growth_Plan__c> oldGPList){
        Set<Id> accountIdSet = new Set<Id>(); 
         System.debug('swot thread>>oldGPList>> '+oldGPList.size());
        try {
		for(Growth_Plan__c gpOldobj :oldGPList){
				accountIdSet.add(gpOldobj.Account__c);
				
		}
		
		Map<Id,Growth_Plan__c> accIdVsNewGp = new Map<Id,Growth_Plan__c>();
		List<Account> accList = [select id,name,(select id,name,Account__c from Growth_Plans__r where fcstPlanning_Year__r.name =:toSelectedYear) from Account where ID IN:accountIdSet];
		for(Account a : accList){
			for(Growth_Plan__c gg : a.Growth_Plans__r){
				accIdVsNewGp.put(gg.Account__c,gg);
			}  
		}
        System.debug('swot thread>>accList>> '+accList.size());
        
        List<Swot_Threat__c	> insertNewSTObjList = new List<Swot_Threat__c	>();	 
        List<Swot_Threat__c	> stObjectList = [select id,name,Account__c,Growth_Plan__c,Threat__c,LastModifiedBy.Name,lastModifiedDate from Swot_Threat__c where Growth_Plan__c IN :OldGPList	];
        System.debug('swot thread>>stObjectList>> '+stObjectList.size());
        if(stObjectList != null && stObjectList.size()> 0){
            for(Swot_Threat__c	 stOldObj : stObjectList){
                if(accIdVsNewGp.containsKey(stOldObj.Account__c)){
                    Swot_Threat__c	 stNewObj = new Swot_Threat__c	();
                    
                    if(stOldObj.Account__c != null)
                        stNewObj.Account__c = stOldObj.Account__c;
                    stNewObj.Growth_Plan__c = accIdVsNewGp.get(stOldObj.Account__c).Id;
                    
                    if(stOldObj.Threat__c != null)
                        stNewObj.Threat__c = stOldObj.Threat__c;
                    
                    stNewObj.Threat_LastModified_By__c = stOldObj.LastModifiedBy.Name;
                    DateTime dt = stOldObj.LastModifiedDate;
                    stNewObj.Threat_LastModified_Date__c = date.newInstance(dt.year(), dt.month(), dt.day());
                   
                   insertNewSTObjList.add(stNewObj);
                }
            }
        }
        System.debug('swot thread>>insertNewSTObjList>> '+insertNewSTObjList.size());
	      if(insertNewSTObjList!=null && insertNewSTObjList.size()>0){
	    		Database.SaveResult[] srList = Database.insert(insertNewSTObjList,false);
	    		if(srList!=null && srList.size()>0){
					for(Integer i=0;i<srList.size();i++){
						
						String threat='';
					if(insertNewSTObjList[i].Threat__c!=null)
						threat=insertNewSTObjList[i].Threat__c;
						
						if(srList.get(i).isSuccess()){
							finalSuccessStr+=srList.get(i).getId()+','+insertNewSTObjList[i].Account__c+','+insertNewSTObjList[i].Growth_Plan__c+','+threat.replace(',', '')+', Successfully created \n';
						}
						if(!srList.get(i).isSuccess()){
							Database.Error error = srList.get(i).getErrors().get(0);
							finalFailStr+=insertNewSTObjList[i].Account__c+','+insertNewSTObjList[i].Growth_Plan__c+','+threat.replace(',', '')+','+error.getMessage()+' \n';
						}
					}
				}
	    	}
	    	}
    	 catch(Exception e) {
            System.debug('Exception Message '+e);
            System.debug('Exception Line Number: '+e.getLineNumber());
        }
    }
    
    global void finish(Database.BatchableContext bc){
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
		  TotalJobItems, CreatedBy.Email, ExtendedStatus
		  from AsyncApexJob where Id = :BC.getJobId()];
		  
		  String loginUserEmail=UserInfo.getUserEmail();
			
    	Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
    	Messaging.EmailFileAttachment csvAttachment1 = new Messaging.EmailFileAttachment();
    	
		Blob csvBlob = blob.valueOf(finalSuccessStr);
		String csvSuccessName = 'Swot Threat Success File.csv';
		csvAttachment.setFileName(csvSuccessName);
		csvAttachment.setBody(csvBlob);
		
		Blob csvBlob1 = blob.valueOf(finalFailStr);
		String csvFailName = 'Swot Threat Fail File.csv';
		csvAttachment1.setFileName(csvFailName);
		csvAttachment1.setBody(csvBlob1);
		
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{'sandeep.yadav@forecastera.com'};
		String subject = 'Swot Threat Copy '+fromSelectedYear+' to '+toSelectedYear+ 'Year' ;
		email.setSubject(subject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody('Swot Threat copy Details');
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment,csvAttachment1});
		Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
	}
}