@IsTest
public class AutoConvertLeads_Test{
    
    private static testMethod void runLeadConversionMethod(){

           Lead leadRecord = new Lead(Company = 'TestCompany', Country = 'United States', FirstName = 'TestFirst', Email = 'test@test.com', LastName = 'TestLast', Status = 'Open');
           insert leadRecord;
           
           Lead insertedRecord = [Select Id, Status, Rating from Lead where Id = :leadRecord.Id];
           insertedRecord.Status = 'Hot';
           List<Id> lstIds = new List<Id>();
           lstIds.add(insertedRecord.Id);          
           AutoConvertLeads.LeadAssign(lstIds);   
        }
        
}