global class Product2Update_Batch implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<String> setProductCodes = new Set<String>();
        for(Project_Exception__c objPE:[Select Product_Code__c From Project_Exception__c where Product_Code__c!=null LIMIT 50000]){
            setProductCodes.add(objPE.Product_Code__c);    
        }
        String query = 'Select Id, ProductCode, Product_Exception__c from Product2 where ProductCode NOT IN: setProductCodes';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Product2> scope){  
        List<Product2> lstProd = new List<Product2>();
        for(Product2 objProd : scope){
            objProd.Product_Exception__c=false;
            lstProd.add(objProd);
        }
        Database.update(lstProd,false);
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
}