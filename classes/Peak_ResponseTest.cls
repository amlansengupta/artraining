@isTest
public with sharing class Peak_ResponseTest {

    @isTest
    public static void testPeakReponse(){
        Peak_Response response = new Peak_Response();
        Peak_TestUtils utils = new Peak_TestUtils();

        // Test success flag
        System.assertEquals(response.success,true);

        // Test messages
        response.messages.add(Peak_TestConstants.TEST_MESSAGE);
        System.assertEquals(response.messages[0],Peak_TestConstants.TEST_MESSAGE);

        // Test object list
        Account account = utils.createTestAccount();
        response.results.add(account);
        System.assertEquals(account,response.results[0]);
    }
}