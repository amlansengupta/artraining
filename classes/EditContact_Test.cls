@isTest
private class EditContact_Test {
    
    static testMethod void TestMethod1(){
        
        Account testAcct = new Account(Name = 'My Test Account',One_Code__c = 'ABCDEF');
        insert testAcct; 
        
        Contact testContact = new Contact();
        testContact.Salutation = 'Fr.';
        testContact.FirstName = 'TestFirstName';
        testContact.LastName = 'TestLastName';
        testContact.Job_Function__c = 'TestJob';
        testContact.Title = 'TestTitle';
        testContact.MailingCity  = 'TestCity';
        testContact.MailingCountry = 'TestCountry';
        testContact.MailingState = 'TestState'; 
        testContact.MailingStreet = 'TestStreet'; 
        testContact.MailingPostalCode = 'TestPostalCode'; 
        testContact.Phone = '9999999999';
        testContact.Email = 'abc@xyz.com';
        testContact.MobilePhone = '9999999999';
        testContact.AccountId = testAcct.Id;
        insert testContact;
        
        ApexPages.currentPage().getHeaders().put('Host','--c');
        ApexPages.StandardController cntrl=new ApexPages.StandardController(testContact);
        EditContact edCon=new EditContact(cntrl);
        edCon.edit();
        edCon.save();
        edCon.saveAndReturn();
        edCon.cancel();
        edCon.doCancel();
    
    }
    
    
}