/*Purpose: Test class to provide test coverage for AP110_ScopeItTemplate_Edit.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Madhavi   08/28/2014    Created test class to provide test coverage for AP110_ScopeItTemplate_Edit class
 
============================================================================================================================================== 
*/
@isTest(seeAllData=true)
private class AP110_ScopeItTemplate_Edit_Test {
 /*
    * @Description : Testmethod to test edit functionality on  scopemodelling
 */
static testmethod void Editscopemodelling(){
    User user1 = new User();
    String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
    user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);        
    System.runAs(user1){
        Mercer_ScopeItTestData  mtscopedata = new Mercer_ScopeItTestData();
        //Insert Product
        String LOB = 'Retirement';
        List<Pricebook2> prcbk = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        Product2 testProduct = mtscopedata.buildProduct(LOB);
        //insert Modelling Record
        Temp_ScopeIt_Project__c tempdata =Mercer_ScopeItTestData.createScopeitTemplate(LOB,testProduct.id,testProduct.name);
        insert tempdata ;
        Test.startTest();
        ApexPages.StandardController con = new ApexPages.StandardController(tempdata);        
        PageReference vfpage = Page.Mercer_ScopeITModeling_Edit_Override;
        vfpage.getParameters().put('id',tempdata.id);
        test.setcurrentPage(vfpage);
        AP110_ScopeItTemplate_Edit mod = new AP110_ScopeItTemplate_Edit(con);
        mod.getlstwrapper();
        mod.searchProducts();
        mod.searchValue = 'Asset';
        mod.getlstwrapper();
        mod.searchProducts();
        mod.SaveProduct();
        mod.Previous();
        mod.next();
        mod.beginning();
        mod.end();
        mod.getDisablePrevious();
        mod.getDisableNext();
        Test.stopTest();
    }
  }
}