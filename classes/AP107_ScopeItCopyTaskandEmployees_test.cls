@isTest(seeAllData=false)
public class AP107_ScopeItCopyTaskandEmployees_test{
    private static Mercer_TestData mtdata;
    private static Mercer_ScopeItTestData mtscopedata ;

    
    @testSetup static void setup(){
        mtdata = new Mercer_TestData();
        mtscopedata = new Mercer_ScopeItTestData(); 
        Pricebook2 pb = New Pricebook2();
        pb.id=Test.getStandardPricebookId();
        update pb;
        
        List<ApexConstants__c> listofvalues = new List<ApexConstants__c>();
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        listofvalues.add(setting);
        setting = new ApexConstants__c();
        setting.Name = 'ScopeITThresholdsList';
    //    setting.StrValue__c = 'EuroPac:5000;Growth Markets:2500;North America:10000';
        setting.StrValue__c = 'International:5000;North America:10000';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        listofvalues.add(setting);
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        listofvalues.add(setting);
        /*Request No. 17953 : Removing Finalist START
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        listofvalues.add(setting);
        Request No. 17953 : Removing Finalist END */

        
        Insert listofvalues;
        test.starttest();
            String LOB = 'Retirement';
            Account acc = mtdata.buildAccount();
            Opportunity testOppty = mtdata.buildOpportunity();
            ID opptyId = testOppty.id;
            Pricebook2 prcbk = new Pricebook2();
            prcbk.Id = Test.getStandardPricebookId(); 
            Product2 testProduct = mtscopedata.buildProduct(LOB);
            ID prodId = testProduct.id;
            PricebookEntry pbEntry = [select Id, product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :prcbk.id and Product2Id = : prodId and CurrencyIsoCode = 'ALL' limit 1];
            ID pbEntryId = pbEntry.Id;  
            OpportunityLineItem opptylineItem = Mercer_TestData.createOpptylineItem(opptyId,pbEntryId);
            ID oliId= opptylineItem.id;
            String oliCurr = opptylineItem.CurrencyIsoCode;
            Double oliUnitPrice = opptylineItem.unitprice;
        test.stoptest();
        String employeeBillrate = Mercer_ScopeItTestData.getEmployeeBillRate(); 
        mtscopedata.insertCurrentExchangeRate();   
        ScopeIt_Project__c testScopeitProj = mtscopedata.buildScopeitProject(prodId, opptyId,oliId,oliCurr,oliUnitPrice );
        ScopeIt_Task__c  testScopeitTask =  mtscopedata.buildScopeitTask();               
        ScopeIt_Employee__c testScopeitEmployee =  mtscopedata.buildScopeitEmployee(employeeBillrate);
    }

    
    
    static testMethod void testCopyTask(){
        User user1 = new User();
        String mercerStandardUserProfile = Label.Mercer_Standard_ID;
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert1', 'usrLstName', 1);
        System.runAs(user1) {
            
            Test.startTest();        
                ScopeIt_Task__c  testScopeitTask =  [Select Id from ScopeIt_Task__c where task_name__c = 'testscopetask'];                       
                AP107_ScopeItCopyTaskandEmployees.copyTask(testScopeitTask.id);             
            Test.stopTest();
        }
    
    }
   
  

}