global class SchedulableChatterPostInOpportunity implements Schedulable {

    
    // schedulable execute method
    global void execute(SchedulableContext SC) 
    {       
        // call database execute method to run  batch class batchableChatterPostForOpportunity
        database.executeBatch(new batchableChatterPostForOpportunity()); 
        database.executeBatch(new batchableChatterPostForOpportunity9Days());
    }
}