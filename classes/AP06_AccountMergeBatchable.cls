/*Purpose: This Apex class implements Batchable to merge accounts based on the integration feed from webcas, this batch class will
           look for two different scenarios everytime an account is updated
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   12/24/2012  Created Apex Batchable class
============================================================================================================================================== 
*/

global class AP06_AccountMergeBatchable implements Database.Batchable<sObject>, Database.Stateful {
    
    global String query;
    
    global List<Account> failedAccountForMerge = new List<Account>();
    
    global Map<String, List<Account>> oneCodeAccountMap = new Map<String, List<Account>>();
    
    // constructor for the class
    global AP06_AccountMergeBatchable(String query)
    {
        this.query = query;
    }
    
    
    /*
     *  Method Name: start
     *  Description: Method returns Database.querylocator object i.e, return all the queried records.
     *  Note: start method is a mandatory method to be extended in this batch class.     
     */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query); 
    }
    
    /***************************************************************************************************************
     *  *Scenario 1: if Workflow Submit Status = "CANCELLED" AND One Code Status = "Pending - Workflow Wizard"     *                                                                                            
     *  *Scenario 2: if Inactivation Code = "CD9" AND One Code Status = "Inactive" AND Client Type = "Marketing"   *
     *                                                                                                             *
     ***************************************************************************************************************/
    
    /*
     *  Method Name: execute
     *  Description: Method is used to find the duplicate account based on One Code and merge the duplicates 
     *  Note: execute method is a mandatory method to be extended in this batch class.     
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    {
        // transient list of account records
        transient List<Account> accountList = new List<Account>();      
        
        // get account records in scope
        accountList = (List<Account>)scope;
        
        // iterate through the records
        for(Account account : accountList)
        {
            
            // if scenario 1 condition is satisfied
            if(AP07_AutoMergeUtil.isScenarioOne(account))
            {
                // get one code from workflow message
                String oneCode = AP07_AutoMergeUtil.getOneCode(account.Workflow_Message__c);
                if(oneCode <> null)
                {
                    // if map contains one code as key add it to list
                    if(oneCodeAccountMap.containskey(oneCode))
                    {
                        oneCodeAccountMap.get(oneCode).add(account);
                    }else
                    {
                        // new list
                        List<Account> accList = new List<Account>();
                        accList.add(account);
                        // add to map
                        oneCodeAccountMap.put(oneCode, accList);
                            
                    }       
                }
                            
            }
            // if scenario 2 condition is satisfied
            else if(AP07_AutoMergeUtil.isScenarioTwo(account))
            {
                // get one from Duplicate client code
                String oneCode = account.Duplicated_Client_Code__c;
                // if map contains one code as key add it to list
                if(oneCode <> null)
                {
                    if(oneCodeAccountMap.containskey(oneCode))
                    {
                        oneCodeAccountMap.get(oneCode).add(account);
                    }else
                    {
                        // new list
                        List<Account> accList = new List<Account>();    
                        accList.add(account);
                        // add to map
                        oneCodeAccountMap.put(oneCode, accList);
                    }       
                }
                
            }
            else if(AP07_AutoMergeUtil.isScenarioThree(account))
              {
                // get one from Duplicate client code
                String oneCode = account.Duplicate_Client_Code_Rejected__c;
                // if map contains one code as key add it to list
                if(oneCode <> null)
                {
                    if(oneCodeAccountMap.containskey(oneCode))
                    {
                        oneCodeAccountMap.get(oneCode).add(account);
                    }else
                    {
                        // new list
                        List<Account> accList = new List<Account>();    
                        accList.add(account);
                        // add to map
                        oneCodeAccountMap.put(oneCode, accList);
                    }       
                }
                System.debug('Senerio three');
            }
            else
            {               
                // neither of the scenarios are satisfied
                // set auto merge flag to false
                account.Auto_Merge_Flag__c = false;
                // write failed messge
                account.Auto_Merge_Status__c = System.label.CL06_FailedMergeMessage;
                // add to list for updation
                failedAccountForMerge.add(account);
            }   
        }
        
        // if map has records, delete duplicate records
        if(!oneCodeAccountMap.isEmpty())
        {
            oneCodeAccountMap = AP07_AutoMergeUtil.mergeAccounts(oneCodeAccountMap);    
        }
        
        // if map still has values process further
        if(!oneCodeAccountMap.isEmpty())
        {
            AP07_AutoMergeUtil.mergeAccounts(oneCodeAccountMap, true);
        }
        
    }
    
    
    /*
     *  Method Name: finish
     *  Description: Method is used perform any final operations on the records which are in scope. 
     *  Note: finish method is a mandatory method to be extended in this batch class.     
     */
    global void finish(Database.BatchableContext bc)
    {
        
        if(!failedAccountForMerge.isEmpty()) Database.update(failedAccountForMerge, false);
        failedAccountForMerge.clear();

    }
}