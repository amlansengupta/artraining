public class GTM_Controller{
/*
public String imgURL1{get; set;}
public String imgURL2{get; set;}
public String imgURL3{get; set;}
public String imgURL4{get; set;}
public String imgURL5{get; set;}
public String imgURL6{get; set;}
public String imgURL7{get; set;}
public String imgURL8{get; set;}
public String imgURL9{get; set;}
public String imgURL10{get; set;}
public String imgURL11{get; set;}
public String imgURL12{get; set;}
public String imgURL13{get; set;}
public String imgURL14{get; set;}
public String imgURL15{get; set;}
public String imgURL16{get; set;}
public String imgURL17{get; set;}
public String imgURL18{get; set;}
public String imgURL19{get; set;} */
    public List<GTM_Section__c> gtmSectionList{get;set;}
    public Map<String, List<GTM_Image__c>> gtmMap{get;set;}

public GTM_Controller()
{ /*
   imgURL1='/servlet/servlet.FileDownload?file=015c0000000umFz';
   imgURL2='/servlet/servlet.FileDownload?file=015c0000000umGE';
   imgURL3='/servlet/servlet.FileDownload?file=015c0000000f9y2';
   imgURL4='/servlet/servlet.FileDownload?file=015c0000000f9y3';
   imgURL5='/servlet/servlet.FileDownload?file=015c0000000f9y4';
   imgURL6='/servlet/servlet.FileDownload?file=015c0000000f9y5';
   imgURL7='/servlet/servlet.FileDownload?file=015c0000000f9y6';
   imgURL8='/servlet/servlet.FileDownload?file=015c0000000f9y7';
   imgURL9='/servlet/servlet.FileDownload?file=015c0000000f9y8';
   imgURL10='/servlet/servlet.FileDownload?file=015c0000000f9xt';
   imgURL11='/servlet/servlet.FileDownload?file=015c0000000f9xu';
   imgURL12='/servlet/servlet.FileDownload?file=015c0000000f9xv';
   imgURL13='/servlet/servlet.FileDownload?file=015c0000000f9xw';
   imgURL14='/servlet/servlet.FileDownload?file=015c0000000f9xx';
   imgURL15='/servlet/servlet.FileDownload?file=015c0000000f9xy';
   imgURL16='/servlet/servlet.FileDownload?file=015c0000000f9xz';
   imgURL17='/servlet/servlet.FileDownload?file=015c0000000f9y0';
   imgURL18='/servlet/servlet.FileDownload?file=015c0000000fA7J';
   imgURL19='/servlet/servlet.FileDownload?file=015c0000000fA7T';
  */
        gtmMap = new Map<String, List<GTM_Image__c>>();
        
        gtmSectionList = new List<GTM_Section__c>();
        
        gtmSectionList = [SELECT Name, Section_Order__c, Items_per_Row__c, 
                         (SELECT Name, Alt__c, url__c,title__c, imageLocation__c, Order__c
                          FROM GTM_Images__r ORDER BY Order__c)
                          FROM GTM_Section__c ORDER BY Section_Order__c];  
  }       

    
    public PageReference goToSettings(){
    
        PageReference pRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm() + GTM_Image__c.sObjectType.getDescribe().getKeyPrefix());
        return pRef;
    }  
}