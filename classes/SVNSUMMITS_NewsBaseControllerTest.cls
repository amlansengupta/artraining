/**
 * Created by 7Summits on 5/12/17.
 */

@isTest
public with sharing class SVNSUMMITS_NewsBaseControllerTest {

    @isTest
    public static void initClassTest(){
        // We are installing this package as an unmanaged package so there
        // isn't a namespace which is basically what this class does. It
        // sets a namespace. Test is just for coverage since it needs 75%.
        Object baseModel = new SVNSUMMITS_NewsBaseController();
    }

    @isTest
    public static void getModelTest(){
        // We are installing this package as an unmanaged package so there
        // isn't a namespace which is basically what this class does. It
        // sets a namespace. Test is just for coverage since it needs 75%.
        Object baseModel = SVNSUMMITS_NewsBaseController.getModel();
    }

}