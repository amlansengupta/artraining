/*Purpose: This Apex class is to update the opportunity is Revenue Udpated field
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   04/11/2014  Created class
============================================================================================================================================== 
*/
public class AP104_OpportunityFieldUpdate implements Schedulable {
    
    // schedulable execute method
    public void execute(SchedulableContext SC) 
    {       
        List<Opportunity> OppUpdList = new List<Opportunity>();
        
            for(Opportunity opp: [select id,IsRevenueDateAdjusted__c from Opportunity where IsRevenueDateAdjusted__c= true]){
            opp.IsRevenueDateAdjusted__c = false;
            OppUpdList.add(opp);
        }
        if(oppUpdList.size()>0)
            database.Update(oppUpdList);
    }
}