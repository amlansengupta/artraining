// ===================
// Test Peak Test Utils
// ===================
@isTest
public with sharing class Peak_TestUtilsTest {

	// Create a standard user
	@isTest
	public static void testCreateStandardUser(){
		Peak_TestUtils testUtils = new Peak_TestUtils();
		User testUser = testUtils.createStandardUser();
		System.assertEquals(Peak_TestConstants.LASTNAME,testUser.LastName);
	}

	// Create a guest user
	@isTest
	public static void testCreateGuestUser(){
		Peak_TestUtils testUtils = new Peak_TestUtils();
		User testUser = testUtils.createGuestUser();
		System.assertEquals(Peak_TestConstants.LASTNAME,testUser.LastName);
	}

	@isTest
	public static void testGetFakeId() {
		system.assert(Peak_TestUtils.getFakeId(User.SObjectType) != null);
	}

}