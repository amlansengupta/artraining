/* Purpose:This shedulable class implements DC11_OpportunityLineItemBatchable batch class to mass update the Opportunity records when an Opportunity Product associated with it is updated.
===================================================================================================================================
The methods contains following functionality;
 • Execute() method fires a query fetch all Account records and executes DC24_AccountBatchable batch class  
 History
 ---------------------
 VERSION     AUTHOR             DATE        DETAIL 
 1.0         Sarbpreet          07/23/2013  Created Shedulable Class to  implement DC11_OpportunityLineItemBatchable
 2.0         Sarbpreet          7/7/2014    Added Comments.
 ======================================================================================================================================*/
global class DC23_OpportunityLineItemSchedulable implements schedulable{
/*
*   Creation of constructor
*/
global DC23_OpportunityLineItemSchedulable(){}
/*
 * Schedulable execute method     
 */
global void execute(SchedulableContext SC)    
 {  
     //String variable to fetch the Opportunity id, their names and Opportunity Line Item ids associated with the Opportunity
     String  qry = 'Select id, Name,(Select id from OpportunityLineItems) from Opportunity where StageName NOT IN (\'Closed / Won\',\'Closed / Lost\',\'Closed / Declined to Bid\',\'Closed / Client Cancelled\') AND CALENDAR_YEAR(CloseDate) >= 2015' ;
     if(Test.isRunningTest()){ 
         qry = 'Select id, Name,(Select id from OpportunityLineItems) from Opportunity LIMIT 100';        
         database.executeBatch(new DC11_OpportunityLineItemBatchable(qry), 200);  
     } else {     
         database.executeBatch(new DC11_OpportunityLineItemBatchable(qry), 2000);  
     }
 }   
 }