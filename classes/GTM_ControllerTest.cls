@isTest
public class GTM_ControllerTest{
static testMethod void myUnitTest() 
{

    GTM_Section__c section = new GTM_Section__c();
    section.Items_per_Row__c = 4;
    section.Section_Order__c = 1;
    insert section;
    
    GTM_Image__c image = new GTM_Image__c();
    image.GTM_Section__c = section.Id;
    image.Alt__c= 'Alt Text';
    image.title__c = 'Description';
    image.imageLocation__c = '/loading.gif';
    image.Order__c = 1;
    image.url__c = 'https://mercer.my.salesforce.com';
    insert image;
    
    GTM_Controller controller = new GTM_Controller();
    controller.goToSettings();
}
}