/*Purpose: Test Class for providing code coverage to TRG07_SalesCreditTrigger trigger
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Arijit   02/04/2013  Created test class
============================================================================================================================================== 
*/
/*
==============================================================================================================================================
Request Id                 					 Date                    Modified By
12638:Removing Step 						 17-Jan-2019			 Trisha Banerjee
==============================================================================================================================================
*/
@isTest(seeAllData = True)
private class Test_TRG07_SalesCreditTrigger 
{
    
    /*
     * @Description : Test method to provide data coverage to SalesCredit Trigger (Insert Scenerio)
     * @ Args       : Null
     * @ Return     : void
     */
     
     
     static testMethod void insertSwitchSetting(){
         Switch_Settings__c ss = Switch_Settings__c.getOrgDefaults();
         ss.IsOpportunityProductTriggerActive__c = false;
         ss.IsOpportunityTriggerActive__c = false;
         ss.IsOpportunityProductValidationActive__c = false;
         ss.IsOpportunityValidationActive__c = false;
         upsert ss;
     }
     
    static testMethod void TestMethod1() 
    {   
       
        Test.Starttest();
        insertSwitchSetting();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        insert u;
        
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Health';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
               
        System.runAs(u) {
           Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague1.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)        
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.OwnerId = u.Id;
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Amount = 101;
        testOppty.Opportunity_Office__c = 'Norwalk - Merritt';
        insert testOppty;
         
                
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague2.Id;
        testSales.Percent_Allocation__c = 100;
        testSales.Role__c ='EH&B Specialist';
        insert testSales;
        }
      
        Test.StopTest();
    }
    static testMethod void TestMethod2() 
    {   
        
        Test.Starttest();
        insertSwitchSetting();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Health';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
        
        System.runAs(u) {
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague1.Id;
            testAccount.One_Code__c = '1234';
            insert testAccount;
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty4';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;      
            //request id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            testOppty.OwnerId = u.Id;
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Amount = 101;
            testOppty.Opportunity_Office__c = 'London - Tower Place West';
            insert testOppty; 
            
            
            
            Sales_Credit__c testSales = new Sales_Credit__c();
            testSales.Opportunity__c = testOppty.Id;
            testSales.EmpName__c = testColleague2.Id;
            testSales.Percent_Allocation__c = 102;
            testSales.Role__c ='EH&B Specialist';
            try{
                insert testSales;
            }catch(exception e){
                
            }
            
            
            
        }
        Test.StopTest();
    }
    static testMethod void TestMethod3() 
    {   
        
        Test.Starttest();
        insertSwitchSetting();
                
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Health';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
        
        System.runAs(u) {
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague1.Id;
            testAccount.One_Code__c = '1234';
            insert testAccount;
            
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty4';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;      
            //request id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            testOppty.OwnerId = u.Id;
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Amount = 101;
            testOppty.Opportunity_Office__c = 'London - Tower Place West';
            insert testOppty; 
            
            
            
            Sales_Credit__c testSales = new Sales_Credit__c();
            testSales.Opportunity__c = testOppty.Id;
            testSales.EmpName__c = testColleague2.Id;
            testSales.Percent_Allocation__c = 102;
            testSales.Role__c ='Sales Professional';
            try{
                insert testSales;
            }catch(exception e){
                
            }
            
        }
        Test.StopTest();
    }
        static testMethod void TestMethod4() {   
        insertSwitchSetting();
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        User u1 = new User(Alias = 'staansdt', Email='standsddusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234675534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standsddusertestss1@testorg.com');
        insert u;
        insert u1;
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Health';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
        
        Colleague__c testColleague3 = new Colleague__c();
        testColleague3.Name = 'TestColleague2';
        testColleague3.EMPLID__c = '1234675534';
        testColleague3.LOB__c = 'Health';
        testColleague3.Empl_Status__c = 'Active';
        testColleague3.country__c='CAN';
        testColleague3.Location__c = mogd1.Id;
        testColleague3.Last_Name__c = 'TestLastName';
        insert testColleague3;
        
        System.runAs(u) {
            Account testAccount = new Account();
            testAccount.Name = 'TestAccountName';
            testAccount.BillingCity = 'TestCity';
            testAccount.BillingCountry = 'TestCountry';
            testAccount.BillingStreet = 'Test Street';
            testAccount.Relationship_Manager__c = testColleague1.Id;
            testAccount.One_Code__c = '1234';
            insert testAccount;
 Test.Starttest();         
         
            Opportunity testOppty = new Opportunity();
            testOppty.Name = 'TestOppty4';
            testOppty.Type = 'New Client';
            testOppty.AccountId = testAccount.Id;      
            //request id:12638 commenting step
            //testOppty.Step__c = 'Identified Deal';
            testOppty.OwnerId = u.Id;
            testOppty.StageName = 'Identify';
            testOppty.CloseDate = date.Today();
            testOppty.CurrencyIsoCode = 'ALL';
            testOppty.Amount = 101;
            testOppty.Opportunity_Office__c = 'London - Tower Place West';
            insert testOppty; 
            
            OpportunityTeamMember testoppTeamMember = new OpportunityTeamMember();
        
        testoppTeamMember.OpportunityId = testOppty.id;
        testoppTeamMember.UserId = u1.id;
        testoppTeamMember.TeamMemberRole= 'Executive Sponsor';
        testoppTeamMember.OppTeamMemID__c = u1.id;
        
        insert testoppTeamMember;
            
            Sales_Credit__c testSales = new Sales_Credit__c();
            testSales.Opportunity__c = testOppty.Id;
            testSales.EmpName__c = testColleague3.Id;
            testSales.Percent_Allocation__c = 100;
            testSales.Role__c ='Sales Professional';
            //try{
                //insert testSales;
            //}catch(exception e){}
            
            
            
        }
        Test.StopTest();
    }
    static testMethod void TestMethod5() 
    {   
        insertSwitchSetting();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        insert u;
        
        User u1 = new User(Alias = 'staansdt', Email='standsddusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234675534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standsddusertestss1@testorg.com');
        insert u1;
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Career';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        testColleague1.SP_Flag__c='Y';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
        
        Colleague__c testColleague3 = new Colleague__c();
        testColleague3.Name = 'TestColleague2';
        testColleague3.EMPLID__c = '1234675534';
        testColleague3.LOB__c = 'Health';
        testColleague3.Empl_Status__c = 'Active';
        testColleague3.country__c='CAN';
        testColleague3.Location__c = mogd1.Id;
        testColleague3.Last_Name__c = 'TestLastName';
        insert testColleague3;
        
        System.runAs(u) {
           Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague1.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
        
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)        
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.OwnerId = u.Id;
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Amount = 101;
        testOppty.Opportunity_Office__c = 'Norwalk - Merritt';
        testOppty.Opportunity_Region__c = 'International';
        insert testOppty; 
        
         Test.Starttest();   
         
           /*OpportunityTeamMember testoppTeamMember = new OpportunityTeamMember();
        
        testoppTeamMember.OpportunityId = testOppty.id;
        testoppTeamMember.UserId = u.id;
        testoppTeamMember.TeamMemberRole= 'Executive Sponsor';
        testoppTeamMember.OppTeamMemID__c = u.id;
        
        insert testoppTeamMember;*/
        
        Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        //iSalesGoal.Employee_ID__c = '12345678902';
        iSalesGoal.Colleague__c = testColleague3.id;
        iSalesGoal.OwnerId = u1.Id;
        insert iSalesGoal;
        
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague2.Id;
        testSales.Percent_Allocation__c = 100;
        insert testSales;
        testSales.EmpName__c = testColleague3.Id;
        update testSales;
            
        Sales_Credit__c testSales2 = new Sales_Credit__c();
        testSales2.Opportunity__c = testOppty.Id;
        testSales2.EmpName__c = testColleague1.Id;
        testSales2.Percent_Allocation__c = 15;
        insert testSales2;
        
        Sales_Credit_Line_Item__c sclm=new Sales_Credit_Line_Item__c();
        sclm.name='test';
        sclm.Individual_Sales_Goal__c=iSalesGoal.id;
        sclm.Sales_Credit_lookup__c=testSales.id;
        sclm.CurrencyIsoCode='USD';
        insert sclm;
            
        
       }
        Test.StopTest();
    }
     static testMethod void TestMethod6() 
    {   
        insertSwitchSetting();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'staandt', Email='standardusertestss1@testorg.com', 
            EmailEncodingKey='UTF-8',employeenumber='1234534', LastName='Tessting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestss1@testorg.com');
        insert u;
        
        User u1 = new User(Alias = 'staansdt', Email='standsddusertestss1@testorg.com', 
                          EmailEncodingKey='UTF-8',employeenumber='1234675534', LastName='Tessting', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standsddusertestss1@testorg.com');
        insert u1;
        
        Mercer_Office_Geo_D__c mogd1 = new Mercer_Office_Geo_D__c();
        mogd1.Name = 'TestMogd1';
        mogd1.Office_Code__c = 'TestCode1';
        mogd1.Office__c = 'Aberdeen';
        mogd1.Region__c = 'Global';
        mogd1.Market__c = 'Benelux';
        mogd1.Sub_Market__c = 'US - Midwest';
        mogd1.Country__c = 'Canada';
        mogd1.Sub_Region__c = 'Canada';
        insert mogd1;
        
        Colleague__c testColleague1 = new Colleague__c();
        testColleague1.Name = 'TestColleague';
        testColleague1.EMPLID__c = '1234534';
        testColleague1.LOB__c = 'Health';
        testColleague1.Empl_Status__c = 'Active';
        testColleague1.country__c='USA';
        testColleague1.Location__c = mogd1.Id;
        testColleague1.Last_Name__c = 'TestLastName';
        insert testColleague1;
        
        Colleague__c testColleague2 = new Colleague__c();
        testColleague2.Name = 'TestColleague2';
        testColleague2.EMPLID__c = '123453445';
        testColleague2.LOB__c = 'Health';
        testColleague2.Empl_Status__c = 'Active';
        testColleague2.country__c='USA';
        testColleague2.Location__c = mogd1.Id;
        testColleague2.Last_Name__c = 'TestLastName';
        insert testColleague2;
        
        
        
        System.runAs(u) {
           Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague1.Id;
        testAccount.One_Code__c = '1234';
        insert testAccount;
        Test.Starttest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty4';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Updated as part of 5166(july 2015)        
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identified Deal';
        testOppty.OwnerId = u.Id;
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Amount = 101;
        testOppty.Opportunity_Office__c = 'Norwalk - Merritt';
        insert testOppty; 
        
            
         
          
        
        Individual_Sales_Goal__c iSalesGoal = new Individual_Sales_Goal__c();
        iSalesGoal.Sales_Goals__c = 500;
        //iSalesGoal.Employee_ID__c = '12345678902';
        iSalesGoal.Colleague__c = testColleague2.id;
        iSalesGoal.OwnerId = u1.Id;
        insert iSalesGoal;
        
        Sales_Credit__c testSales = new Sales_Credit__c();
        testSales.Opportunity__c = testOppty.Id;
        testSales.EmpName__c = testColleague2.Id;
        testSales.Percent_Allocation__c = 100;
        insert testSales;        
         Test.StopTest();
        Sales_Credit_Line_Item__c sclm=new Sales_Credit_Line_Item__c();
        sclm.name='test';
        sclm.Individual_Sales_Goal__c=iSalesGoal.id;
        sclm.Sales_Credit_lookup__c=testSales.id;
        sclm.CurrencyIsoCode='USD';
        insert sclm;
            
        delete testSales;
       }
       
    }
   
    
}