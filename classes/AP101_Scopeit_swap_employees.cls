/*Purpose:  This Apex class is used for swapping the employees associated with a task related to a particular Scopeit project.
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   02/10/2014  Created the controller class for swapping employees
 
============================================================================================================================================== 
*/
public class AP101_Scopeit_swap_employees {
   
    private id projId;
    private Decimal oppcurrcode;
    private List<ScopeIt_Employee__c> lstEmp = new List<ScopeIt_Employee__c>();
    private List<Scope_Modeling_Employee__c> lstModEmp = new List<Scope_Modeling_Employee__c>();   
    
    public String objectType {get;set;}
    public List<EmployeeWrapper> lstWrp {get;set;}
    public ScopeIt_Project__c objScopProj {get;set;}
    public Scope_Modeling__c objScopModProj {get;set;}
    
    /*
     *  Creation of Contructor
    */
    public AP101_Scopeit_swap_employees() {      
        projId = system.currentPageReference().getParameters().get(ConstantsUtility.STR_ObjID);
        objectType = system.currentPageReference().getParameters().get(ConstantsUtility.STR_Object);
        if(objectType == null || ConstantsUtility.STR_Blank.equals(objectType)){
            objScopProj = [select id,name,currencyisocode from ScopeIt_Project__c where id=:projId];
            lstWrp = new List<EmployeeWrapper>();
            getUniqueEmployees();
            oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :objScopProj.CurrencyIsoCode].Conversion_Rate__c;
        }
        else{
            objScopModProj = [select id,name,currencyisocode from Scope_Modeling__c  where id=:projId];
            lstWrp = new List<EmployeeWrapper>();
            getUniqueModEmployees();
            oppcurrCode = [select name,Conversion_Rate__c from Current_Exchange_Rate__c where name = :objScopModProj.CurrencyIsoCode].Conversion_Rate__c;
        }
    }
       
    
    /* 
     * @Description : Getter method for getting Unique Scoprit Employees.
     * @ Args       : null   
     * @ Return     :List<EmployeeWrapper>
     */ 
    public List<EmployeeWrapper> getUniqueEmployees(){
        lstEmp.clear();
        lstWrp.clear();
        set<id> empidset = new set<id>();
        try {
        for(ScopeIt_Employee__c sep:[select id,name,Employee_Bill_Rate__c, Employee_Bill_Rate__r.Level__c,  Bill_Rate_Opp__c,
                Employee_Bill_Rate__r.Market_Country__c,Employee_Bill_Rate__r.Business__c,Employee_Bill_Rate__r.name,currencyisocode,
                Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c,Hours__c,ScopeIt_Task__c from ScopeIt_Employee__c where ScopeIt_Task__r.ScopeIt_Project__c =:projid])
        {
            lstEmp.add(sep);
            
            if(!empidset.contains(sep.Employee_Bill_Rate__c)){
                empidset.add(sep.Employee_Bill_Rate__c);
                EmployeeWrapper empw = new EmployeeWrapper();
                empw.objEmp = sep;
                empw.toEmp = sep;
                empw.fromEmpName = sep.employee_bill_rate__r.name;
                empw.fromEmpBusiness = sep.Employee_Bill_Rate__r.Business__c;                
                empw.fromEmpLevel = sep.Employee_Bill_Rate__r.Level__c;                
                empw.fromEmpMarket = sep.Employee_Bill_Rate__r.Market_Country__c;                                                
                empw.fromEmpId = sep.employee_bill_rate__c;
                lstWrp.add(empw);               
            }
        }  
        }  catch (System.DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
        }
        return lstWrp;
    }
    
    /* 
     * @Description : Getter method for getting Unique Scope Modeling Employees.
     * @ Args       : null   
     * @ Return     :List<EmployeeWrapper>
     */ 
    public List<EmployeeWrapper> getUniqueModEmployees(){
        lstModEmp.clear();
        lstWrp.clear();
        set<id> empidset = new set<id>();
        try {
        for(Scope_Modeling_Employee__c sep:[select id,name,Employee_Bill_Rate__c, Employee_Bill_Rate__r.Level__c,  Bill_Rate_Opp__c,
                Employee_Bill_Rate__r.Market_Country__c,Employee_Bill_Rate__r.Business__c,Employee_Bill_Rate__r.name,currencyisocode,
                Employee_Bill_Rate__r.Bill_Rate_Local_Currency__c,Hours__c,Scope_Modeling_Task__c from Scope_Modeling_Employee__c where Scope_Modeling_Task__r.Scope_Modeling__c =:projid])
        {
            lstModEmp.add(sep);
            
            if(!empidset.contains(sep.Employee_Bill_Rate__c)){
                empidset.add(sep.Employee_Bill_Rate__c);
                EmployeeWrapper empw = new EmployeeWrapper();
                empw.objModEmp = sep;
                empw.toModEmp = sep;
                empw.fromEmpName = sep.employee_bill_rate__r.name;
                empw.fromEmpBusiness = sep.Employee_Bill_Rate__r.Business__c;                
                empw.fromEmpLevel = sep.Employee_Bill_Rate__r.Level__c;                
                empw.fromEmpMarket = sep.Employee_Bill_Rate__r.Market_Country__c;                 
                empw.fromEmpId = sep.employee_bill_rate__c;
                lstWrp.add(empw);
            }
        }   
        }  catch (System.DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
        }
        return lstWrp;
    }
    
    

   /*
    * @Description : This method saves the swapped employees
    * @ Args       : null   
    * @ Return     : void
    */
    public pageReference saveEmployees(){
        if(objectType == null){
            map<Id,Id> updatedEmpMap = new Map<Id,Id>();
            List<ScopeIt_Employee__c> updList = new List<ScopeIt_Employee__c>();
            for(EmployeeWrapper ew:lstWrp){
                 if(ew.fromEmpid <> ew.toEmp.Employee_Bill_Rate__c){
                    updatedEmpMap.put(ew.fromEmpId,ew.toEmp.Employee_Bill_Rate__c);
                    //newempidset.add(ew.toEmp.Employee_Bill_Rate__c);
                }
            }
            
            for(ScopeIt_Employee__c st:lstEmp){
                if(updatedEmpMap.containsKey(st.Employee_Bill_Rate__c)){
                   st.Employee_Bill_Rate__c =  updatedEmpMap.get(st.Employee_Bill_Rate__c);
                   updList.add(st);
                }
            }
            try {
            database.update(lstEmp);
            } catch (System.DmlException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));  
                return null;             
            }
        }
        else{
            map<Id,Id> updatedEmpMap = new Map<Id,Id>();
            List<Scope_Modeling_Employee__c> updList = new List<Scope_Modeling_Employee__c>();
            for(EmployeeWrapper ew:lstWrp){
                if(ew.fromEmpid <> ew.toModEmp.Employee_Bill_Rate__c){
                    updatedEmpMap.put(ew.fromEmpId,ew.toModEmp.Employee_Bill_Rate__c);
                }
            }
            
            for(Scope_Modeling_Employee__c st:lstModEmp){
                if(updatedEmpMap.containsKey(st.Employee_Bill_Rate__c)){
                   st.Employee_Bill_Rate__c =  updatedEmpMap.get(st.Employee_Bill_Rate__c);
                   updList.add(st);
                }
            }
            try {
            database.update(lstModEmp);
            } catch (System.DmlException e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));        
                return null;        
            }
        }
        return new PageReference(ConstantsUtility.STR_BackSlash+projid);
    }
    
    /*
     * @Description : This method cancels the event
     * @ Args       : null   
     * @ Return     : void
     */
    public pageReference cancel(){
        return new PageReference('/'+projId);
    }
    
    /*
     * Created wrapper class for Employees
     */
    public class EmployeeWrapper{
    
        public ScopeIt_Employee__c objEmp {get;set;}
        public Scope_Modeling_Employee__c  objModEmp {get;set;}
        public Scope_Modeling_Employee__c ToModEmp {get;set;}
        public String fromEmpName {get;set;}
        public String fromEmpBusiness {get;set;}
        public String fromEmpLevel {get;set;}
        public String fromEmpMarket {get;set;}                        
        public Id fromEmpId {get;set;}
        public ScopeIt_Employee__c toEmp {get;set;}
        public String currencyCode {get;set;}
        public Decimal toempbillrate {get;set;}
        
    }
}