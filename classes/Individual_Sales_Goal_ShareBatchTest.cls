@isTest
public class Individual_Sales_Goal_ShareBatchTest {
    @isTest
    public static void test1(){
         colleague__c testColleague = new colleague__c();
         testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        testColleague.Level_1_Descr__c = 'Mercer';
        
        insert testColleague;
        Id LiteId =[select id from Profile where Name ='MercerForce Lite'].id;
        ApexConstants__c deactivationSetting = new ApexConstants__c();
        deactivationSetting.Name='LiteProfiles';
        deactivationSetting.StrValue__c =LiteId;
        insert deactivationSetting;
        
        Id ProfId=[select id from Profile where Name ='System Administrator'].Id;
         User usr2 = new User();
        user Us1= new user();
        user Us2= new user();
        user Us3= new user();
        user Us4= new user();
        user Us5= new user();
        User u= [select id from User where id=:UserInfo.getUserId()];
        system.runAs(u){
         
         Us1 = Mercer_TestData.createUser(ProfId,'test','TestLastName1',us1);
             Us2 = Mercer_TestData.createUser(ProfId,'test','TestLastName2',us2);
             Us3 = Mercer_TestData.createUser(ProfId,'test','TestLastName3',us3);
             Us4 = Mercer_TestData.createUser(ProfId,'test','TestLastName4',us4);
             Us5 = Mercer_TestData.createUser(ProfId,'test','TestLastName5',us5);
             
        usr2.alias = 'user3k2';
        usr2.email = 'tek22@bbcm.com';
        usr2.emailencodingkey='UTF-8';
        usr2.lastName = 'TestLastName5';
        usr2.languagelocalekey='en_US';
        usr2.localesidkey='en_US';
        usr2.ProfileId = ProfId;
        usr2.timezonesidkey='Europe/London';
        usr2.UserName = 'bbb26@bbcm.com';
        usr2.EmployeeNumber = '1234567890';
        usr2.isActive = true;
        usr2.S_Dash_Supervisor__c = us1.id;
        usr2.S_Dash_Supervisor_2__c = us2.id;  
        usr2.S_Dash_Supervisor_3__c = us3.id;
        usr2.S_Dash_Supervisor_4__c = us4.id;  
        usr2.S_Dash_Supervisor_5__c = us5.id;    
        insert usr2;
        }
        system.runAs(usr2){
        Individual_Sales_Goal__c isg = new Individual_Sales_Goal__c();
        isg.colleague__c=testColleague.id;
        insert isg;
        }
        Individual_Sales_Goal_ShareBatch batch = new Individual_Sales_Goal_ShareBatch();
        Database.executeBatch(batch);
    }

}