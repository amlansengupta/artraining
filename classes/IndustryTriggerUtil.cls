public class IndustryTriggerUtil {

    public static void updateAccountSICInfo(Map<ID,Industry__c> newMap)
    {
        List<String> primary_SIC_CodeList = new List<String>();
        for(Industry__c ind : newMap.values())
        {
          primary_SIC_CodeList.add(ind.SIC_Code__c);  
        }
        
        List<Industry__c> indList = new List<Industry__c>([Select id, SIC_Code__c, Industry_Category__c, SIC_Description__c from Industry__c where SIC_Code__c = :primary_SIC_CodeList]);
        Map<String, Industry__c> indMap = new Map<String, Industry__c>();
        for(Industry__c ind: indList){
            indMap.put(ind.SIC_Code__c, ind);
        }
        
        List<Account> acList = [Select id, name,Primary_SIC_Code__c from Account where Primary_SIC_Code__c =: primary_SIC_CodeList];
        List<Account> acListUpd = new List<Account>();
        if(acList.size()>0){
            for(Account acc: acList){
                String sicode = acc.Primary_SIC_Code__c;
            	Industry__c ic = indMap.get(sicode);
                acc.Industry_Category__c = ic.Industry_Category__c;
                acc.Primary_SIC_Description__c = ic.SIC_Description__c;                
                acListUpd.add(acc);
            }
        }
        
        if(acListUpd.size()>0){
            update acListUpd;
        }
    }
    
    public static void deleteAccountSICInfo(Map<ID,Industry__c> newMap)
    {
        List<String> primary_SIC_CodeList = new List<String>();
        for(Industry__c ind : newMap.values())
        {
          primary_SIC_CodeList.add(ind.SIC_Code__c);  
        }
        
        List<Industry__c> indList = new List<Industry__c>([Select id, SIC_Code__c, Industry_Category__c, SIC_Description__c from Industry__c where SIC_Code__c = :primary_SIC_CodeList]);
        Map<String, Industry__c> indMap = new Map<String, Industry__c>();
        for(Industry__c ind: indList){
            indMap.put(ind.SIC_Code__c, ind);
        }
        
        List<Account> acList = [Select id, name,Primary_SIC_Code__c from Account where Primary_SIC_Code__c =: primary_SIC_CodeList];
        List<Account> acListUpd = new List<Account>();
        if(acList.size()>0){
            for(Account acc: acList){
                String sicode = acc.Primary_SIC_Code__c;
            	Industry__c ic = indMap.get(sicode);
                acc.Industry_Category__c = '';
                acc.Primary_SIC_Description__c = '';
                acc.Primary_SIC_Code__c = '';
                acListUpd.add(acc);
            }
        }
        
        if(acListUpd.size()>0){
            update acListUpd;
        }
    }    
}