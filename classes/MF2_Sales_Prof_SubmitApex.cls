global class MF2_Sales_Prof_SubmitApex{
    @Testvisible
    
    webservice static String submit(Id sId){
       
        String status = '';
        Id recTypeId = Schema.SObjectType.Sales_Professional__c.getRecordTypeInfosByName().get('Locked').getRecordTypeId();
        Sales_Professional__c sp =[select id,SOW_and_EL_Uploaded__c,EL_On_File__c from Sales_Professional__c where id=:sId];
        sp.status__c='Submitted for Approval';
        sp.RecordTypeId = recTypeId;
        List<Webcas_Details__C> listWbCs = [select Sales_Professional__c, Project_description__c,Webcas_Project_number__c from Webcas_details__c where Sales_Professional__c =:sId AND (Webcas_Project_number__c!='' AND Webcas_Project_number__c!=null)];
        system.debug('listWbCs::'+listWbCs);
         
        if((sp.SOW_and_EL_Uploaded__c ||sp.EL_On_File__c) && !listWbCs.isEmpty() ){
            status = 'success';
            Database.Update(sp);
            //PageReference nextPage = new PageReference('/lightning/r/Sales_Professional__c/' + sId +'/view');    
            sendEmailToApprover(sp); 
            
        }
        else if(listWbCs.isEmpty() && !sp.SOW_and_EL_Uploaded__c && !sp.EL_On_File__c){
            system.debug('error found');
            status = 'Error';
            
        }
        else if(!sp.SOW_and_EL_Uploaded__c && !sp.EL_On_File__c){
            status = 'NoFile';
            system.debug('NoFile found');
            //ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please upload required documents in files section for form submission.');
            //ApexPages.addMessage(myMsg); 
           
            
        }
        else if(listWbCs.isEmpty()){
           status = 'NoWb';
            system.debug('NoWb found');
            //ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,'Please upload required documents in files section for form submission.');
            //ApexPages.addMessage(myMsg); 
           
        }
         return status; 
        
    }
    public static void sendEmailToApprover(Sales_Professional__c sp){
        
        set<Id> approverId = new Set<Id>();
        Map<string,user> usrMap = new Map<string,user>();
        Map<Id,User> UserMap= new Map<Id, User>();
        Map<String,id> empnoMap = new Map<string,Id>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Sales_Professional__c spF=[select id,Name, Client_Name__c,Sales_Professional__c, Opportunity_Name__c,status__c,MSL_or_MSM_Name__c,Sales_Professional__r.Name from Sales_Professional__c where id=:sp.Id];
        if(spF.status__c =='Submitted for Approval' && spF.MSL_or_MSM_Name__c!=null ){
            approverId.add(spF.MSL_or_MSM_Name__c);
        }
        
        if(!approverId.isEmpty()){
            for(colleague__c col :[select id, EMPLID__c from Colleague__c where id in:approverId]){
                empnoMap.put(col.EMPLID__c,col.id);
            }
        }
        if(!empnoMap.isEmpty()){
            for(User u:[select id, Name,Email,EmployeeNumber from user where EmployeeNumber in:empnoMap.keyset()]){
                usrMap.put(u.EmployeeNumber,u);
            }
        }
        
        if(!usrMap.isEmpty()){
            for(string ut:usrMap.keyset()){
                if(empnoMap.containskey(ut)){
                    UserMap.put(empnoMap.get(ut),usrMap.get(ut));
                }
            }
        }
        if(!UserMap.isEmpty()){            
            OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress where displayName='US Growth Leadership' limit 1];
            EmailTemplate templateId = [Select id,body,HtmlValue,subject from EmailTemplate where name = 'MSL Approval Email'];
            String emailBody = templateId.HtmlValue;
            System.debug('***'+emailBody);
            if(UserMap.containskey(spF.MSL_or_MSM_Name__c))
                emailBody = emailBody.replace('<SP_MSL>', UserMap.get(spF.MSL_or_MSM_Name__c).Name);
            
            else
                emailBody = emailBody.replace('<SP_MSL>', '' );
            //system.debug('hjhhkhkh:'+spF.Sales_Professional__r.Name);
            if(spF.Sales_Professional__C!=null)
            emailBody = emailBody.replace('<SALES PROFESSIONAL NAME>', spF.Sales_Professional__r.Name);
            else
            emailBody = emailBody.replace('<SALES PROFESSIONAL NAME>', '');
            emailBody = emailBody.replace('<ACCOUNT NAME>', spF.Client_Name__c);
            emailBody = emailBody.replace('<OPPORTUNITY NAME>', spF.Opportunity_Name__c);
            
            emailBody = emailBody.replace('<INSER LINK TO THE APPLICATION FORM>','<a href="'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+spF.id+'">'+spF.name+'</a>');
            
            // emailBody = emailBody.replace('{!Opportunity.OwnerFullName}',opps.owner.name);
            System.debug('&&&'+emailBody);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setSubject(templateId.subject);
            if(UserMap.containskey(spF.MSL_or_MSM_Name__c))
                mail.setTargetObjectId(UserMap.get(spF.MSL_or_MSM_Name__c).Id);
            mail.setSaveAsActivity(false);
            mail.setHtmlBody(emailBody); 
            mail.setOrgWideEmailAddressId(owa.id);
            mails.add(mail);
            
            Messaging.sendEmail(mails);
        }
        
    }
}