public without sharing class IEPromptController {
    @AuraEnabled
    public static boolean isShown()
    {
        boolean flag=false;
        try{
            Date myDate = date.today();
            String format = myDate+'`True';
            //String wrongFormat = myDate+'`False';
            
            User usr=null;
            List<User> listToUpdate=new List<User>();
            List<User> lastLogin=[select Id,isShown__c,LastLoginDate from User where Id=:UserInfo.getUserID()];
            system.debug(lastLogin);
            if(lastLogin!=null && lastLogin.size()>0)
            {
                usr=lastLogin.get(0);
            }
            if(usr !=null)
            {
                for(User obj : lastLogin)
                {
                    system.debug('myDate:'+myDate+'Date.valueOf(obj.LastLoginDate):'+obj.LastLoginDate.date());
                    boolean isSame = myDate.isSameDay(obj.LastLoginDate.date());
                    system.debug('isSame:'+isSame);
                    if(isSame)
                    {
                        if(obj.isShown__c != format)
                        {
                            flag=true;obj.isShown__c = format; listToUpdate.add(obj); }
                        else{flag=false;}
                    }
                    //listToUpdate.add(obj);
                }
            }
            system.debug('flag:'+flag+' , listToUpdate:'+listToUpdate);
            update listToUpdate;
        }catch(Exception ex){system.debug(ex.getMessage());ExceptionLogger.logException(ex, 'IEPromptController', 'isShown');}
         return flag;     
    }
}