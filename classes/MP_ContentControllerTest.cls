/**
 * Created by emaleesoddy on 5/10/17.
 */
@IsTest(SeeAllData=true)
public with sharing class MP_ContentControllerTest {

    //"{"topicId":"0TO4C000000GsQ1WAK","featuredContent":[{"recordId":"","type":"news"},{"recordId":"","type":"event"},{"recordId":"","type":"blog"}]}"
    static testMethod void testController(){

        //Map of record type name to id
        Map<String, String> recordTypeMap = new Map<String, String>();
        for(RecordType r : [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :MP_ContentController.RECORD_TYPE_BLOG OR DeveloperName = :MP_ContentController.RECORD_TYPE_NEWS OR DeveloperName = :MP_ContentController.RECORD_TYPE_META]){
            recordTypeMap.put(r.DeveloperName, r.Id);
        }

        //Create news article
        News__c n = new News__c(
                Name = 'test',
                Summary__c = 'test',
                Author__c = UserInfo.getUserId(),
                Publish_DateTime__c = Date.today(),
                RecordTypeId = recordTypeMap.get(MP_ContentController.RECORD_TYPE_NEWS)
        );
        insert n;

        //Create blog article
        News__c b = new News__c(
                Name = 'test',
                Summary__c = 'test',
                Author__c = UserInfo.getUserId(),
                Publish_DateTime__c = Date.today(),
                RecordTypeId = recordTypeMap.get(MP_ContentController.RECORD_TYPE_BLOG)
        );
        insert b;
        
        
         //Create group article
        News__c b1 = new News__c(
                Name = '',
                Summary__c = 'Description',
                Author__c = userinfo.getUserId(),
                Publish_DateTime__c = Date.today(),
                RecordTypeId = recordTypeMap.get('Meta')
        );
        insert b1;

        //Create event
        Event__c e = new Event__c(
                Name = 'test',
                Start_DateTime__c = Date.today(),
                End_DateTime__c = Date.today(),
                Location_Name__c = 'test',
                Location_Address__c = '123 test st.',
                All_Day_Event__c = false
        );
        insert e;
        string networkId=[select id from Network limit 1].Id;
         list <CollaborationGroup> testGroups = new list <CollaborationGroup>();
            for (integer i=0; i<4; i++){
                CollaborationGroup colGroup = new CollaborationGroup(Name='Test Chatter Group' + i,CollaborationType = 'Public', CanHaveGuests = false,
                        isArchived = false, isAutoArchiveDisabled = false, networkId = networkId );
                testGroups.add(colGroup);
            }
            
            insert testGroups;
        Map<String, Object> mp = new Map<String, Object>{
                'topicId' => null,
                'featuredContent' => new List<Map<String, Object>>{
                        new Map<String, Object>{
                                'recordId' => n.Id,
                                'type' => 'news'
                        },
                        new Map<String, Object>{
                                'recordId' => b.Id,
                                'type' => 'blog'
                        },
                        new Map<String, Object>{
                                'recordId' => b1.Id,
                                'type' => 'meta'
                        },
                        new Map<String, Object>{
                                'recordId' => e.Id,
                                'type' => 'event'
                        },
                       new Map<String, Object>{
                                'recordId' => testGroups[0].Id,
                                'type' => 'group'
                        }
                            
                }
        };
        MP_ContentController.getContent(JSON.serialize(mp));
        
        Map<String, Object> mp2 = new Map<String, Object>{
                'topicId' => null,
                'featuredContent' => new List<Map<String, Object>>{
                       new Map<String, Object>{
                                'recordId' => testGroups[0].Id,
                                'type' => 'group'
                        }
                            
                }
        };
        MP_ContentController.getContent(JSON.serialize(mp2));

        ConnectApi.Topic tp = ConnectApi.Topics.createTopic( Network.getNetworkId(), 'tester', '' );
        ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), n.Id, 'tester');
        ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), b.Id, 'tester');
        ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), e.Id, 'tester');
         ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), b1.Id, 'tester');
          //ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), testGroups[0].Id, 'tester');
	    mp.put('topicId', [SELECT Id FROM Topic WHERE Name = 'tester'].Id);
        MP_ContentController.getContent(JSON.serialize(mp));
        
         mp2.put('topicId', [SELECT Id FROM Topic WHERE Name = 'tester'].Id);
        MP_ContentController.getContent(JSON.serialize(mp2));




    }
    
}