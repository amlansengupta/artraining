/*
 Purpose: This Class contains logic related to opportunity lite related list functionality          
 ==============================================================================================================================================
 History 
 ----------------------- 
 VERSION     AUTHOR                  DATE        DETAIL    
 1.0 -      Saravanan Chokkalingam   07/13/2013  Created Controller Class
 2.0 -      Sarbpreet                7/6/2014    Updated Comments/documentation.
 ============================================================================================================================================== 
 */
public class opportunity_lite_related_list{

    public Opportunity_Lite__c opplite {get;set;}
    /*
    * Creation of Constructor
    */
    public opportunity_lite_related_list(ApexPages.StandardController controller ){
        opplite = (Opportunity_Lite__c)controller.getRecord();
        
        opplite = [select id, opportunity__c from Opportunity_Lite__c where id=:opplite.id];
        
    }
    /*
     * @Description : Getter method for getting Opportunity Products
     * @ Args       : null
     * @ Return     : list<oppprods>
     */
    public list<oppprods> getOppProducts(){
        list<oppprods> prodlst = new list<oppprods>();
        list<opportunityLineItem> lstopptyitems = [select id,CurrencyISOCode, PricebookEntry.Product2.Name, Segment__c, Cluster__c, LOB__c, UnitPrice,Care_Package_Peer_Group__c,
                        CurrentYearRevenue_edit__c, AnnualizedRevenue__c, Revenue_Start_Date__c,Revenue_End_Date__c,Scope_Reqd__c,Assets_Under_Mgmt__c from OpportunityLineItem
                        where opportunityid =:opplite.opportunity__c];
                        
        for(OpportunityLineItem opp:lstopptyitems){
            oppProds pro = new oppProds();
            pro.proName = opp.PricebookEntry.Product2.Name;
            pro.segment = opp.Segment__c;
            pro.Cluster = opp.cluster__c;
            pro.lob = opp.lob__c;
            pro.CarePackage= opp.Care_Package_Peer_Group__c;
            pro.ScoSoln = opp.Scope_Reqd__c;
            pro.Assets = opp.Assets_Under_Mgmt__c;
            if(opp.unitprice<>null){
                
                pro.unitprice = opp.CurrencyISOCode +' '+opp.unitprice;
            }
            pro.cyr = opp.currentyearrevenue_edit__c;
            pro.ar = opp.annualizedrevenue__c;
            if(opp.revenue_start_date__c <>null){
                DateTime d = opp.revenue_start_date__c;
                String dateStr =  d.format('dd/MM/yyyy') ;
                pro.revstartdate = dateStr;
                
            }
            if(opp.revenue_end_date__c<>null){
                DateTime d = opp.revenue_end_date__c;
                String dateStr = d.format('dd/MM/yyyy');
                pro.revenddate = dateStr;
            }
            prodlst.add(pro);
        }
        
        system.debug(lstopptyitems);
        
        return prodLst;
    
    }
    /*
    *    Wrapper class for Opportunity Products
    */
    public class oppprods{
    
        public String proName {get;set;}
        public String Segment {get;set;}
        public String Cluster{get;set;}
        public String lob{get;set;}
        public String unitprice{get;set;}
        public Decimal cyr{get;set;}
        public Decimal ar{get;set;}
        public String revstartdate{get;set;}
        public String revenddate{get;set;}
        public String CarePackage{get;set;}
        public Boolean ScoSoln{get;set;}
        public Decimal Assets{get;set;}
        /*
        *    Method for Opportunity LOB
        */
        public void oppLob(){}
    
    }

}