/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*
Purpose: This test class provides data coverage to AP29_UserSyncBatchable class
==============================================================================================================================================History
 History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL                   
 1.0 -       Arijit  03/29/2013  Created Test class 

============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_AP29_UserSyncBatchable {
    /*Public static void createCS(){
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_1';
        setting.StrValue__c = 'MERIPS;MRCR12;MIBM01;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'AP02_OpportunityTriggerUtil_2';
        setting.StrValue__c = 'Seoul;Taipei;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Above the funnel';
        setting.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Identify';
        setting.StrValue__c = 'Identified Single Sales Objective(s);Making Go/No Go Decision;Identified Deal;Assessed Potential Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Selected';
        setting.StrValue__c = 'Selected & Finalizing EL &/or SOW;Pending WebCAS Project(s);';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Active Pursuit';
        setting.StrValue__c = 'Presented Proposal;Made Go Decision;Completed Deal Review;Constructed Proposal;Developed Solutions & Strategy;';
        insert setting;
        
        setting = new ApexConstants__c();
        setting.Name = 'Finalist';
        setting.StrValue__c = 'Presented Finalist Presentation;Selected as Finalist;Developed Finalist Strategy;Rehearsed Finalist Presentation;';
        insert setting;
    }*/

/*
*   Test Data for User Creation
*/
private static User createUser(String profileId, String alias, String lastName, User testUser, integer i, string Name)
    {
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        testUser = new User();
        testUser.alias = alias;
        testUser.email = 'test@xyz.com';
        testUser.emailencodingkey='UTF-8';
        testUser.lastName = lastName;
        testUser.languagelocalekey='en_US';
        testUser.localesidkey='en_US';
        testUser.ProfileId = profileId;
        testUser.timezonesidkey='Europe/London';
        testUser.UserName = randomName + '.' + lastName +'@xyz.com';
        testUser.EmployeeNumber = '1234567890'+i;
        testUser.Location__c = Name;
        insert testUser;
        system.runAs(testUser){
        }
        return testUser;
    }
    
    /*
    *   Test Data for quote Key set
    */
    private static String quoteKeySet(Set<String> mapKeySet)
    {
       User user1 = new User();
       String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
       user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
       system.runAs(User1){ 
       } 
        String newSetStr = '' ;
        for(String str : mapKeySet)
            newSetStr += '\'' + str + '\',';

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;        

        return newSetStr;
        

    }
    /*
     * @Description : Test method to provide data coverage to AP29_UserSyncBatchable 
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //createCS();
        Set<String> mogdSet= new Set<String>();
        
        
        ConstantsUtility.STR_BYPASS_BUYINGINFLUENCE_FORBATCH = 'true';
        Colleague__c testColleague = new Colleague__c();
        
        testColleague.Name = 'TestColleague';
        testColleague.EMPLID__c = '1234567890';
        testColleague.LOB__c = '11111';
        testColleague.Empl_Status__c = 'Active';
        insert testColleague;
        
             Mercer_Office_Geo_D__c testMog = new Mercer_Office_Geo_D__c();
             testMog.Name = 'ABC';
             testMog.Market__c = 'United States';
             testMog.Sub_Market__c = 'Canada – National';
             testMog.Region__c = 'Manhattan';
             testMog.Sub_Region__c = 'Canada';
             testMog.Country__c = 'Canada';
             testMog.Isupdated__c = true;
             testMog.Office_Code__c = '123';
             //Updated as part of 5166(july 2015)
            List<String> oppOffices_List = new List<String>();               
            ApexConstants__c officesToBeExcluded = ApexConstants__c.getValues('AP02_OpportunityTriggerUtil_2');        
            if(officesToBeExcluded.StrValue__c != null){        
            oppOffices_List = officesToBeExcluded.StrValue__c.split(';');
            testMog.Office__c = oppOffices_List[0];
            }
             //testMog.Office__c = 'US';
             insert testMog;
             
             User tUser1 = new User();
        
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        
        tUser1 = createUser(mercerStandardUserProfile, 'tUser1', 'tLastName1', tUser1, 1, testMog.Name);
             
             
             /*Account testAcc = new Account();
             testAcc.Name = 'TestAccountName';
             testAcc.BillingCity = 'TestCity';
             testAcc.BillingCountry = 'TestCountry';
             testAcc.BillingStreet = 'Test Street';
             testAcc.Relationship_Manager__c = testColleague.Id;
             insert testAcc;
             
        List<Account> aList = [select Account_Country__c,Account_Sub_Region__c,Account_Sub_Market__c,Office_Code__c,Account_Market__c, Account_Region__c  from Account LIMIT 100];
            
             for (Account account : aList)
          {
             account.Account_Country__c = testMog.Country__c;
             account.Market_Segment__c = testMog.Market__c;
             account.Account_Sub_Market__c = testMog.Sub_Market__c;
             account.Account_Region__c = testMog.Region__c;
             account.Account_Sub_Region__c = testMog.Sub_Region__c;
             account.Office_Code__c = testMog.Name;
          }
             update aList;
             
             Opportunity opptest = new Opportunity();
             opptest.Name = 'test oppty';
             opptest.Type = 'New Client';
             opptest.AccountId =  aList[0].id; 
             opptest.Step__c = 'Marketing/Sales Lead';
             opptest.StageName = 'Above the funnel';
             opptest.CloseDate = system.today();
             opptest.Opportunity_Office__c = 'Aarhus';
             insert opptest;
             
        List<Opportunity> oppList = [select Opportunity_Office__c,Opportunity_Country__c,Opportunity_Sub_Region__c,Opportunity_Sub_Market__c,Office_Code__c,Opportunity_Market__c, Opportunity_Region__c  from Opportunity LIMIT 100];
            
            for (Opportunity oppty : oppList)
           {
             oppty.Opportunity_Market__c = testMog.Market__c;
             oppty.Opportunity_Region__c = testMog.Region__c;
             oppty.Opportunity_Sub_Market__c = testMog.Sub_Market__c;
             oppty.Opportunity_Sub_Region__c = testMog.Sub_Region__c;
             oppty.Office_Code__c = testMog.Name;
             oppty.Opportunity_Country__c = testMog.Country__c;
             oppty.Opportunity_Office__c = testMog.Office__c;
           }        
             update oppList;*/
           
        
        
        for(Mercer_Office_Geo_D__c officeGeo : [select Id,Country__c,Market__c,Sub_Market__c,Sub_Region__c,Region__c,Name,Office__c from Mercer_Office_Geo_D__c where Isupdated__c = True])
        {
            mogdSet.add(officeGeo.Name);
        }
        
        
        system.runAs(tUser1 ){
        Test.startTest();
            String q = 'select Location__c, Market__c, Sub_Market__c, Sub_Region__c, Region__c, Employee_Office__c, Country__c  from User where Location__c IN' + quoteKeySet(mogdSet) + 'LIMIT 100';
            
            database.executeBatch(new AP29_UserSyncBatchable(q), 500);

        Test.StopTest();
        }
    }
}