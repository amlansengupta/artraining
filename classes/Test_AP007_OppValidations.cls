/*
==============================================================================================================================================
Request Id                                								 Date                    Modified By
12638:Removing Step 													 17-Jan-2019			 Trisha Banerjee
17601:Replacing Stage value 'Pending Step' with 'Identify'               21-Jan-2109             Archisman Ghosh
==============================================================================================================================================
*/
@isTest
public class Test_AP007_OppValidations{

    static testMethod void Test_AP007_OppValidations(){

        List<Opportunity> ListOpp=New List<Opportunity>();
        List<User> ListUsr =New List<User>(); 
        String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();
        User testUser1= new User(); 
        List<Account>  accList = new List<Account>();
        List<Colleague__c> clgList = new List<Colleague__c>();
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>();
        
        ApexConstants__c setting = new ApexConstants__c();
        setting.Name = 'Opportunity_Country';
        setting.StrValue__c = 'Korea;';
        insert setting;
        
        ApexConstants__c setting9 = new ApexConstants__c();
        setting9.Name = 'LiteProfiles';
        setting9.StrValue__c = '00eE0000000iZ5b;00eE0000000j0Au;00eE0000000j5wx';
        insert setting9;
        
        ApexConstants__c setting8 = new ApexConstants__c();
        setting8.Name = 'Above the funnel';
        setting8.StrValue__c = 'Marketing/Sales Lead;Executing Discovery;';
        insert setting8;
        
        ApexConstants__c setting1 = new ApexConstants__c();
        setting1.Name = 'Selected';
        setting1.StrValue__c = '';
        insert setting1;
        
        ApexConstants__c setting2 = new ApexConstants__c();
        setting2.Name = 'Identify';
        setting2.StrValue__c = '';
        insert setting2;
        
        ApexConstants__c setting3 = new ApexConstants__c();
        setting3.Name = 'Active Pursuit';
        setting3.StrValue__c = '';
        insert setting3;
        
         /*Request No. 17953 : Removing Finalist START
        ApexConstants__c setting4 = new ApexConstants__c();
        setting4.Name = 'Finalist';
        setting4.StrValue__c = '';
        insert setting4;
        Request No. 17953 : Removing Finalist END */
        
        ApexConstants__c setting5 = new ApexConstants__c();
        setting5.Name = 'AP02_OpportunityTriggerUtil_1';
        setting5.StrValue__c = '';
        insert setting5;
        
        ApexConstants__c setting6 = new ApexConstants__c();
        setting6.Name = 'AP02_OpportunityTriggerUtil_2';
        setting6.StrValue__c = '';
        insert setting6;
        
        ApexConstants__c setting7 = new ApexConstants__c();
        setting7.Name = 'ScopeITThresholdsList';
        setting7.StrValue__c = '';
        insert setting7;
      
       
        Mercer_Office_Geo_D__c mogd = new Mercer_Office_Geo_D__c();
        mogd.Name = 'TestMogd';
        mogd.Office_Code__c = 'TestCode';
        mogd.Office__c = 'Aarhus';
        mogd.Region__c = 'Asia/Pacific';
        mogd.Market__c = 'ASEAN';
        mogd.Sub_Market__c = 'US - Central';
        mogd.Country__c = 'United States';
        mogd.Sub_Region__c = 'United States';
        insert mogd;
            
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '12345678909';
        testColleague.Location__c = mogd.Id;
        testColleague.LOB__c = 'Employee Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc009@abc.com';
        testColleague.Country__c = 'USA';
        testColleague.Level_1_Descr__c = 'Marsh';
        testColleague.Location_Descr__c = 'Marsh';
        clgList.add(testColleague);
        insert clgList;
  
  
        testUser1.alias = 'abc';
        testUser1.email = 'abc009@abc.com';
        testUser1.emailencodingkey='UTF-8';
        testUser1.lastName = 'TestLastName2';
        testUser1.languagelocalekey='en_US';
        testUser1.localesidkey='en_US';
        testUser1.ProfileId = mercerStandardUserProfile;
        testUser1.timezonesidkey='Europe/London';
        testUser1.UserName = 'abc009@abc.com';
        testUser1.EmployeeNumber = '12345678909';
        testUser1.Level_1_Descr__c='Marsh';
        ListUsr.add(testUser1);
        insert ListUsr;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.One_Code_Status__c='Inactive';
        testAccount.One_Code__c=' ';
        testAccount.Account_Region__c = 'EuroPac';
        testAccount.Relationship_Manager__c = testColleague.Id;
        accList.add(testAccount);
        insert accList;

         List<User> testUsr1 = [Select ID,Profile.Name,Level_1_Descr__c,UserName  from User where UserName ='abc009@abc.com' Limit 1];
         List<Colleague__c> testClg1 = [Select ID, Name, EMPLID__c from Colleague__c where EMPLID__c ='12345678909' Limit 1];
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty1';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.id;        
        //request id:12638 commenting step
        //testOppty.Step__c = 'Identify Deal';
        //Request Id:17601;Replacing Stage value 'Pending Step' with 'Identify'
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        testOppty.Account_Region__c = 'EuroPac';
        testOppty.OwnerID = testUsr1[0].id; 
        testOppty.Opportunity_Office__c='Minneapolis - South Seventh';
       // testOppty.Sibling_Contact_Name__c = testClg1[0].id;
        ListOpp.add(testOppty);
        insert ListOpp;
        
        for(Opportunity opp:ListOpp){
        oppMap.put(opp.Id,opp);
        }
       
        Test.startTest();
        AP007_OppValidations.validationcheckBeforeInsert(ListOpp);
        AP007_OppValidations.validationcheckBeforeUpdate(ListOpp);
        AP007_OppValidations.updateFieldsAfterInsert(ListOpp);
        AP007_OppValidations.updateFieldsAfterUpdate(ListOpp);
        AP007_OppValidations.updateFieldsAfterUpdateOpp(ListUsr);
        AP007_OppValidations.updateFieldsAfterAccountUpdate(accList);
        AP007_OppValidations.updateFieldsAfterColleagueUpdate(clgList);
        AP007_OppValidations.validationcheckBeforeUpdateChanges(oppMap,oppMap);
        Test.stopTest();       
    }
}