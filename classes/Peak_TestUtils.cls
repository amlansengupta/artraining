// ===================
// Peak Test Utils
// ===================
public with sharing class Peak_TestUtils {
    public Peak_TestUtils() {

    }

    // Create a standard user
    public User createStandardUser(){
        Contact contact = createTestContact();
        User testUser;

        List<Profile> standardProfileList = [SELECT Id FROM Profile WHERE Name='Customer Community Plus User'];

        if (!Peak_Utils.isNullOrEmpty(standardProfileList)) {
            Profile standardProfile = standardProfileList[0];

            testUser = new User(Alias = Peak_TestConstants.STANDARD_ALIAS, Email=Peak_TestConstants.STANDARD_EMAIL, EmailEncodingKey=Peak_TestConstants.ENCODING, FirstName=Peak_TestConstants.FIRSTNAME, LastName=Peak_TestConstants.LASTNAME, LanguageLocaleKey=Peak_TestConstants.LOCALE,LocaleSidKey=Peak_TestConstants.LOCALE, ProfileId = standardProfile.Id, TimeZoneSidKey=Peak_TestConstants.TIMEZONE, UserName=Peak_TestConstants.STANDARD_USERNAME);
            testUser.ContactId = contact.id;
        }

        return testUser;
    }

    // Create a guest user
    public User createGuestUser(){
        List<Profile> standardProfileList;
        User guestUser;

        try {
            standardProfileList = [SELECT Id FROM Profile WHERE Name='Standard User'];
        } catch (DmlException e) {
            System.debug(e);
        }

        if (!Peak_Utils.isNullOrEmpty(standardProfileList)) {
            Profile standardProfile = standardProfileList[0];
            guestUser = new User(Alias = Peak_TestConstants.GUEST_ALIAS, Email=Peak_TestConstants.GUEST_EMAIL, EmailEncodingKey=Peak_TestConstants.ENCODING, FirstName=Peak_TestConstants.FIRSTNAME,LastName=Peak_TestConstants.LASTNAME, LanguageLocaleKey=Peak_TestConstants.LOCALE,LocaleSidKey=Peak_TestConstants.LOCALE, ProfileId = standardProfile.Id,TimeZoneSidKey=Peak_TestConstants.TIMEZONE, UserName=Peak_TestConstants.GUEST_USERNAME);
        }

        return guestUser;
    }

    // create collegue
    public Colleague__c createTestColleague(){
        Colleague__c colleague = new Colleague__c(Name=Peak_TestConstants.COLLEGUE_NAME);
        insert colleague;
        return colleague;
    }

    // Create an account so we can create a Contact
    public Account createTestAccount(){
        Colleague__c colleague = createTestColleague();
        CollaborationGroup testGroup = createGroup('Test Group','Unlisted');
        insert testGroup;
        Account account = new Account(name=Peak_TestConstants.ACCOUNT_NAME, one_code__c='Test', PrivateCollaborationGroupID__c=testGroup.Id, Relationship_Manager__c=colleague.Id);
        insert account;
        return account;
    }

    // Create a contact associated with an account
    public Contact createTestContact(){
        Account account = createTestAccount();
        Contact contact = new Contact(firstName=Peak_TestConstants.FIRSTNAME, lastName=Peak_TestConstants.LASTNAME, MailingCountry=Peak_TestConstants.MAILING_COUNTRY, email=Peak_TestConstants.STANDARD_EMAIL);
        contact.accountId = account.id;
        insert contact;

        return contact;
    }

    // Create an attachment
    public Attachment createAttachment(Id parentId){
        Attachment attachment = new Attachment();
        Blob bodyBlob=Blob.valueOf('');
        attachment.ParentId = parentId;
        attachment.body = bodyBlob;
        attachment.Name = Peak_TestConstants.FIRSTNAME;
        return attachment;
    }

    // Fake ID
    public static String getFakeId(Schema.SObjectType sot)
    {
        Integer s_num = 1;
        String result = String.valueOf(s_num++);
        return sot.getDescribe().getKeyPrefix() + '0'.repeat(12-result.length()) + result;
    }

    // Chatter Group
    public CollaborationGroup createGroup(String groupName,String collaborationType){
        CollaborationGroup testGroup = new CollaborationGroup(Name=groupName,CollaborationType=collaborationType);
        // testGroup.NetworkId = networkId; // 0DB46000000TN1PGAW
        return testGroup;
    }

    public Event__c generateEvent(Boolean allDayBoolean){ 
        Event__c event = new Event__c(Name = Peak_TestConstants.EVENT_TESTNAME,Start_DateTime__c = Peak_TestConstants.FROMDT,End_DateTime__c=Peak_TestConstants.TODATE,All_Day_Event__c=allDayBoolean);
        return event;
    }


    // Chatter Group Membership
    /*public CollaborationGroupMember createGroupMembership(String groupId, Id userId){
        CollaborationGroupMember groupMembership = new CollaborationGroupMember();
        groupMembership.CollaborationGroupId = groupId;
        groupMembership.MemberId = userId;
        return groupMembership;
    }*/
}