/*
Purpose: This test class provides data coverage to AP56_ProjectClientRevenueController class
==============================================================================================================================================
History
 ----------------------- 
 VERSION     AUTHOR  DATE        DETAIL    
 1.0 -    Shashank 03/29/2013  Created test class
=======================================================
Modified By     Date         Request
Trisha          18-Jan-2019  12638-Commenting Oppotunity Step
Trisha          21-Jan-2019  17601-Replacing stage value 'Pending Step' with 'Identify' 
=======================================================                                                     
=============================================================================================================================================== 
*/
@isTest(seeAllData = true)
private class Test_AP56_ProjectClientRevenueController 
{
    /*
     * @Description : Test method to provide data coverage to AP56_ProjectClientRevenueController class
     * @ Args       : Null
     * @ Return     : void
     */
    static testMethod void myUnitTest() 
    {
        Colleague__c testColleague = new Colleague__c();
        testColleague.Name = 'TestColleague';
        testColleague.Last_Name__c = 'TestLastName';
        testColleague.EMPLID__c = '00000000000';
        testColleague.LOB__c = 'Health & Benefits';
        testColleague.Empl_Status__c = 'Active';
        testColleague.Email_Address__c = 'abc@abc.com';
        insert testColleague;
        
        Account testAccount = new Account();
        testAccount.Name = 'TestAccountName';
        testAccount.BillingCity = 'TestCity';
        testAccount.BillingCountry = 'TestCountry';
        testAccount.BillingStreet = 'Test Street';
        testAccount.Relationship_Manager__c = testColleague.Id;
        insert testAccount;
        
        test.starttest();
        Opportunity testOppty = new Opportunity();
        testOppty.Name = 'TestOppty';
        testOppty.Type = 'New Client';
        testOppty.AccountId = testAccount.Id;
        //Request Id 12638-Commenting step__c
        //testOppty.Step__c = 'Identified Deal';
        //Request id:17601 ; replacing stage value 'Pending Step' with 'Identify' 
        testOppty.StageName = 'Identify';
        testOppty.CloseDate = date.Today();
        testOppty.CurrencyIsoCode = 'ALL';
        //Added by Soumil on 26/10/2018
        testOppty.Opportunity_Office__c = 'Aarhus - Axel';
        insert testOppty;
        
        Pricebook2 pb = [Select Id, Name from Pricebook2 where isActive = true and isStandard = true Limit 1];
        
        Product2 pro = new Product2();
        pro.Name = 'TestPro';
        pro.Family = 'RRF';
        pro.IsActive = True;
        insert pro;
        
        PricebookEntry pbEntry = [select Id, Product2Id, UnitPrice, IsActive, Pricebook2Id, CurrencyIsoCode from PricebookEntry where Pricebook2Id = :pb.Id and Product2Id = :pro.Id and CurrencyIsoCode = 'ALL' limit 1];
        test.stoptest();
        
        OpportunityLineItem opptylineItem = new OpportunityLineItem();
        opptylineItem.OpportunityId = testOppty.Id;
        opptylineItem.PricebookEntryId = pbEntry.Id;
        opptyLineItem.Revenue_End_Date__c = date.Today()+30;
        opptyLineItem.Revenue_Start_Date__c = date.Today()+20;
        insert opptylineItem;
        
        ApexPages.StandardController stdController = new ApexPages.Standardcontroller(testOppty);
        AP56_ProjectClientRevenueController controller = new AP56_ProjectClientRevenueController(stdController);
        
    }
}