public class ClientProductContactTriggerUtil {
    
    Public static Map<String,String> idMap = new Map<String,String> ();
    Public static List<Contact> conList = new List<Contact> ();
    Public static Map<String,String> cpcMap = new Map<String,String> ();
    Public static Map<Id,Id> cpcMap2 = new Map<Id,Id> ();
    Public static Map<Id,Id> loopMap = new Map<Id,Id> ();
    Public static Map<Id,Contact> conMap = new Map<Id,Contact> ();
    Public static List<Talent_Contract__c> tcList = new List<Talent_Contract__c> ();
    Public static List<Talent_Contract__c> tcList1 = new List<Talent_Contract__c> ();
    Public static List<Client_Product_Contact__c> cpcList2 = new List<Client_Product_Contact__c> ();
    
    public static void clientProductContact(List < Client_Product_Contact__c > triggernew) {
        System.debug('*** '+triggernew);
        
        for(Client_Product_Contact__c cpc:triggernew){
            cpcMap.put(cpc.Id,cpc.Talent_Contract__c);
        }
        
        cpcList2 = [Select Id,Contact__c,Talent_Contract__c from Client_Product_Contact__c where Role__c includes('Billing Contact') AND Talent_Contract__c in :cpcMap.values() ORDER BY CreatedDate];
        system.debug('!!'+cpcList2.size());
        system.debug('!!'+cpcList2);
        
        for(Client_Product_Contact__c cpc : triggernew){
            idMap.put(cpc.contact__c,cpc.Id);
            System.debug('*** '+idMap);
        }
        for(Client_Product_Contact__c cpc:cpcList2){
            if(!loopMap .containsKey(cpc.Talent_Contract__c)){
                loopMap.put(cpc.Talent_Contract__c,cpc.Contact__c);
                cpcMap2.put(cpc.Contact__c,cpc.Talent_Contract__c);
                System.debug('*** '+cpcMap2);
            }
        }
        
        conList = [Select Id, Name, Email, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry from contact where Id in :cpcMap2.keyset()];
        for(Client_Product_Contact__c cpc:triggernew){
            for(contact con:conList){
                conMap.put(idMap.get(cpc.Contact__c),con);
                System.debug('*** '+conMap);
            }
        }
        for(Client_Product_Contact__c cpc : triggernew){
            System.debug('*** '+cpc.Id);
            //System.debug('*** '+conMap.get(cpc.Id)); 
            if(conMap.get(cpc.Id) != null){ 
            String Street = String.valueOf(conMap.get(cpc.Id).Account.BillingStreet);
            if(Street == null){
                Street = '';
            }
            else{
                Street = Street +'\n';
            }
            String City = String.valueOf(conMap.get(cpc.Id).Account.BillingCity);
            if(City == null){
                City = '';
            }
            else{
                City = City +'\n';
            }
            String State = String.valueOf(conMap.get(cpc.Id).Account.BillingState);
            if(State == null){
                State = '';
            }
            else{
                State = State +'\n';
            }
            String PostalCode = String.valueOf(conMap.get(cpc.Id).Account.BillingPostalCode);
            if(PostalCode == null){
                PostalCode = '';
            }
            else{
                PostalCode = PostalCode +'\n';
            }
            String Country = String.valueOf(conMap.get(cpc.Id).Account.BillingCountry);
            if(Country == null){
                Country = '';
            }
            String Address;
            Address = Street+City+State+PostalCode+Country;
            
            Talent_Contract__c tcObj =  new Talent_Contract__c ();
            tcObj.Id = cpc.Talent_Contract__c;
            tcObj.Billing_Contact_Name__c = conMap.get(cpc.Id).Name;
            tcObj.Billing_Contact_Address__c = Address;
            tcObj.Billing_Contact_Email__c = conMap.get(cpc.Id).Email;
            tcList.add(tcObj);
            System.debug('*** '+tcList);
        }
         else{
            Talent_Contract__c tcObj =  new Talent_Contract__c ();
            tcObj.Id = cpc.Talent_Contract__c;
            tcObj.Billing_Contact_Name__c = '';
            tcObj.Billing_Contact_Address__c = '';
            tcObj.Billing_Contact_Email__c = '';
            tcList.add(tcObj);
        }
       }
            Update tcList;

    }
    public static void cpcAfterDelete(List < Client_Product_Contact__c > triggerold) {
        for(Client_Product_Contact__c cpc:triggerold){
            cpcMap.put(cpc.Id,cpc.Talent_Contract__c);
        }
        
        cpcList2 = [Select Id,Contact__c,Talent_Contract__c from Client_Product_Contact__c where Role__c includes('Billing Contact') AND Talent_Contract__c in :cpcMap.values() ORDER BY CreatedDate];
        system.debug('!!'+cpcList2.size());
        system.debug('!!'+cpcList2);
 
        for(Client_Product_Contact__c cpc : triggerold){
            idMap.put(cpc.contact__c,cpc.Id);
            System.debug('*** '+idMap);
        }
        for(Client_Product_Contact__c cpc:cpcList2){
            if(!loopMap.containsKey(cpc.Talent_Contract__c)){
                loopMap.put(cpc.Talent_Contract__c,cpc.Contact__c);
                cpcMap2.put(cpc.Contact__c,cpc.Talent_Contract__c);
                System.debug('*** '+cpcMap2);
            }
        }
        
        conList = [Select Id, Name, Email, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry from contact where Id in :cpcMap2.keyset()];
        for(Client_Product_Contact__c cpc:triggerold){
            for(contact con:conList){
                conMap.put(idMap.get(cpc.Contact__c),con);
                System.debug('*** '+conMap);
            }
        }
        for(Client_Product_Contact__c cpc : triggerold){
            System.debug('*** '+cpc.Id);
           // System.debug('*** '+conMap.get(cpc.Id)); 
            if(conMap.get(cpc.Id) != null){
            String Street = String.valueOf(conMap.get(cpc.Id).Account.BillingStreet);
            if(Street == null){
                Street = '';
            }
            else{
                Street = Street +'\n';
            }
            String City = String.valueOf(conMap.get(cpc.Id).Account.BillingCity);
            if(City == null){
                City = '';
            }
            else{
                City = City +'\n';
            }
            String State = String.valueOf(conMap.get(cpc.Id).Account.BillingState);
            if(State == null){
                State = '';
            }
            else{
                State = State +'\n';
            }
            String PostalCode = String.valueOf(conMap.get(cpc.Id).Account.BillingPostalCode);
            if(PostalCode == null){
                PostalCode = '';
            }
            else{
                PostalCode = PostalCode +'\n';
            }
            String Country = String.valueOf(conMap.get(cpc.Id).Account.BillingCountry);
            if(Country == null){
                Country = '';
            }
            String Address;
            Address = Street+City+State+PostalCode+Country;
            
            Talent_Contract__c tcObj =  new Talent_Contract__c ();
            tcObj.Id = cpc.Talent_Contract__c;
            tcObj.Billing_Contact_Name__c = conMap.get(cpc.Id).Name;
            tcObj.Billing_Contact_Address__c = Address;
            tcObj.Billing_Contact_Email__c = conMap.get(cpc.Id).Email;
            tcList1.add(tcObj);
            System.debug('*** '+tcList1);
        }
       else{
            Talent_Contract__c tcObj =  new Talent_Contract__c ();
            tcObj.Id = cpc.Talent_Contract__c;
            tcObj.Billing_Contact_Name__c = '';
            tcObj.Billing_Contact_Address__c = '';
            tcObj.Billing_Contact_Email__c = '';
            tcList1.add(tcObj);
         }
        }  
        Update tcList1; 
     } 

}