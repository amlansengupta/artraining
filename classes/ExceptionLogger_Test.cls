@isTest
public class ExceptionLogger_Test {
    static testMethod void testMyException() {
        String originatingClass= 'ExceptionLogger_Test';
        String originatingMethod='testMyException';
        Exception ex = new DmlException ('This is custom exception');
        ExceptionLogger.logException(ex, originatingClass, originatingMethod);      
    }
}