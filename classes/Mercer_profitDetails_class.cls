/*Purpose:  This Apex class is used for dispalying the Profit details on an Opportunity
==============================================================================================================================================
History 
---------------------------------------------------------------------------------------------------------
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Jagan   02/10/2014  Created the controller class for Profit details
   2.0 -    Malini  27/12/2016  Modified Class to add/modify LOB as part of Org Restructure
============================================================================================================================================== 
*/
public with sharing class Mercer_profitDetails_class {
    public Mercer_profitDetails_class(){}
    public boolean renderba {get;set;}
    public boolean rendercbo {get;set;}
    public boolean renderehb {get;set;}
    public boolean renderret {get;set;}
    public boolean renderinv {get;set;}
    public boolean renderTAL {get;set;}
    public boolean renderGBS {get;set;}
    public boolean renderOTH {get;set;}
    
    public opportunity opp {get;set;}
    public String INVColor {get;set;}
    public String BAColor {get;set;}
    public String EHBColor {get;set;}
    public String TALColor {get;set;}
    public String CBOColor {get;set;}
    public String RETColor {get;set;}
    public String GBSColor {get;set;}
    public String OTHColor {get;set;}
    
    /*
     * Creation of Constructor
     */
    public Mercer_profitDetails_class(ApexPages.StandardController controller) {
        
        opp = (Opportunity) controller.getRecord();
        renderBA = false;
        renderCBO = false;
        renderEHB = false;
        renderRET = false;
        renderINV = false;
        renderTAL = false;
        renderGBS = false;
        renderOTH = false;
        
        opp = [select Opportunity_Country__c,Margin_INV__c,Margin_CBO__c,Margin_GBS__c,Margin_OTH__c,Margin_RET__c,Margin_BA__c,Margin_EHB__c,Margin_TAL__c from opportunity where id=:opp.id];
        
        if(opp.Margin_INV__c <> null  && opp.Margin_INV__c<>0 ){ 
            renderINV = true;
            INVColor = returnColor(ConstantsUtility.STR_INV, opp.Margin_INV__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_CBO__c <> null && opp.Margin_CBO__c<>0 ){ 
            renderCBO = true;
            CBOColor = returnColor(ConstantsUtility.STR_CBO, opp.Margin_CBO__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_RET__c <> null && opp.Margin_ret__c<>0 ){ 
            renderRET = true;
            RETColor = returnColor(ConstantsUtility.STR_RET, opp.Margin_RET__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_TAL__c <> null && opp.Margin_TAL__c<>0){ 
            renderTAL = true;
            TALColor = returnColor(ConstantsUtility.STR_TAL, opp.Margin_TAL__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_EHB__c <> null  && opp.Margin_EHB__c<>0){ 
            renderEHB = true;
            EHBColor = returnColor(ConstantsUtility.STR_HB, opp.Margin_EHB__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_BA__c <> null && opp.Margin_BA__c<>0){ 
            renderBA = true;
            BAColor = returnColor(ConstantsUtility.STR_BA, opp.Margin_BA__c,opp.Opportunity_Country__c);   
        } 
        if(opp.Margin_GBS__c <> null  && opp.Margin_GBS__c<>0 ){ 
            renderGBS = true;
            GBSColor = returnColor(ConstantsUtility.STR_GBS, opp.Margin_GBS__c,opp.Opportunity_Country__c);   
        }
        if(opp.Margin_OTH__c <> null  && opp.Margin_OTH__c<>0 ){ 
            renderOTH = true;
            OTHColor = returnColor(ConstantsUtility.STR_OTH, opp.Margin_OTH__c,opp.Opportunity_Country__c);   
        }
        
    }
    
    /* 
     * @Description : Method for returning a color based on LOB and Margin
     * @ Args       : String lob,Decimal margin,String country    
     * @ Return     : String
     */ 
    public String returnColor(String lob, Decimal margin, String country){
        
        String color = null;
        Margin_Benchmark__c[] objMargin = [select LOB__c, Country__c, Amber__c, Green__c, Red__c from Margin_Benchmark__c where LOB__c=:lob 
                                             and country__c = :country];
        try {
        if(objMargin.size()>0){
            String greenVal = objMargin[0].Green__c;
            String amberVal = objMargin[0].Amber__c;
            String stringRedVal = objMargin[0].REd__c;
            
            Decimal grnVal = Integer.Valueof(greenVal.subString(1,greenVal.length()));
            Decimal redVal = Integer.Valueof(stringRedVal.subString(1,stringRedVal.length()));
           
            if(margin > grnVal) {
                color = ConstantsUtility.STR_GreenCode;
            }
            else if(margin  < redVal) {
                color = ConstantsUtility.STR_Red;  
            }
            else { 
                color = ConstantsUtility.STR_AmberCode; 
            }                         
        }
        else {          
            if(margin >= 25) {
                color = ConstantsUtility.STR_GreenCode;
            }
            else if(margin  < 10) {
                color = ConstantsUtility.STR_Red; 
            }
            else {
                color = ConstantsUtility.STR_AmberCode; 
            }        
        }
        } catch (System.DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
        }
        return color;
    }
    
    /* 
     * @Description : Method for setting a color based on LOB and Margin
     * @ Args       : List<ScopeIt_Project__c> triggernew    
     * @ Return     : Void
     */
    public void setColor(List<ScopeIt_Project__c> triggernew){
        
        String color = null;
        Map<id,Margin_Benchmark__c> mapMargin = new Map<id,Margin_Benchmark__c>([select id,LOB__c, Country__c, Amber__c, Green__c, Red__c from Margin_Benchmark__c]);
        Map<String,String> LOBMap = new Map<String,String>();
       /* LOBMap.put('Benefits Admin','BA');
        LOBMap.put('Cross Business Offerings (CBO)','CBO');
        LOBMap.put('Employee Health & Benefits','H&B');
        LOBMap.put('Investments','INV');
        LOBMap.put('Retirement','RET');
        LOBMap.put('Talent','TAL');*/
        
        try {
        for(ScopeIt_Project__c st:triggernew){
            Decimal margin = st.Margin__c;
            if(mapMargin.size()>0){
                for(Id mid:mapMargin.keyset()){    
                    if( mapMargin.get(mid).LOB__c == st.LOB__c && mapMargin.get(mid).Country__c.EqualsIgnoreCase(st.Opportunity_Country__c)){
                        
                        String greenVal = mapMargin.get(mid).Green__c;
                        String amberVal = mapMargin.get(mid).Amber__c;
                        String stringRedVal = mapMargin.get(mid).REd__c;
                        
                        Decimal grnVal = Integer.Valueof(greenVal.subString(1,greenVal.length()));
                        Decimal redVal = Integer.Valueof(stringRedVal.subString(1,stringRedVal.length()));
                        
                        system.debug('Green Value...'+grnVal+'...Margin value..'+margin);
                        system.debug('Red Value...'+redVal);
                        
                        
                        if(margin > grnVal) {
                            color = ConstantsUtility.STR_Green;
                        }
                        else if(margin  < redVal) {
                            color = ConstantsUtility.STR_Red; 
                        }
                        else {
                            color = ConstantsUtility.STR_Amber; 
                        }
                        
                        st.Margin_Color__c = color;  
                        return;
                    }
                    else {
                        
                        if(margin >= 25) {
                            color = ConstantsUtility.STR_Green;
                        }
                        else if(margin  < 10) {
                            color = ConstantsUtility.STR_Red; 
                        }
                        else {
                            color = ConstantsUtility.STR_Amber; 
                        }
                        st.Margin_Color__c = color;    
                        
                    }
                     
                }
                system.debug('margin Color...'+st.Margin_Color__c);
            }
        }
        } catch (System.DmlException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, e.getDmlMessage(0)));
        }
    }
}