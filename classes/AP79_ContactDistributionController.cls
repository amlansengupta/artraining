/**
* Purpose: This Controller is an  controller for the Multiple Distributions Page with search. 
           It adds multiple rows of Contact Distributions for the user to enter.
*
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR         DATE             DETAIL 
   1.0 -    Sarbpreet      18/12/2013       Req#2314 : Adding multiple rows at one time along with Distribution Search
   2.0 -    Sarbpreet      5/2/2014         As part of June Release 2014(PMO# 4402) updated code to query some more field to display on layout
   3.0 -    Sarbpreet      7/3/2014         Updated Comments/documentation.
   4.0 -    Sarbpreet      07/28/2014       As part of PMO Request 4654(September Release) added distribution owner field
   5.0 -    Sarbpreet      01/12/2015       As part of PMO Request # 4613(February 2015 Release) enchanced search functionality
============================================================================================================================================== 

*/
public with sharing class AP79_ContactDistributionController {

    public Contact theCon {get;set;}
    public List<Contact_Distributions__c> contDistList {get;set;}
    public Map<id, Contact_Distributions__c>  editDistMap ;
    public Distribution__c[] avlDistributions {get;set;}
    public String searchParam {get;set;}
    Set<ID> contDistId = new Set<ID>();
    public Boolean overLimit {get;set;}
    
    public String PR1 {get;set;}
    public String PR2 {get;set;}
    public String PR3 {get;set;}
    public String PR4 {get;set;}
    
    private static final string STR_Distribution_Name = 'Distribution Name';
    private static final string STR_Distribution_LOB = 'Distribution LOB';
    private static final string STR_Distribution_Region = 'Distribution Region';
    private static final string STR_Distribution_Market = 'Distribution Market';
    private static final string STR_Equals = 'Equals';
    private static final string STR_Contains = 'Contains';
    private static final string STR_Starts_With = 'Starts With';
    
    
    private String qString;
    
    public List<Contact_Distributions__c> lstContDistDel = new List<Contact_Distributions__c>();
  
    public List<DistributionWrapper> distributionAll { get {
            if (distributionAll == null) distributionAll = new List<DistributionWrapper>();
            return distributionAll;
        }
        set;
    } 
      
    public List<DistributionWrapper> selecteddistribution {
        get {
            if (selecteddistribution == null){ 
                 selecteddistribution = new List<DistributionWrapper>();
               }
            return selecteddistribution;
        }
        set;
    }
    
    public List<ContactDistributionWrapper> conDtListwrap {
        get {
            if (conDtListwrap  == null){ 
                 conDtListwrap  = new List<ContactDistributionWrapper>();
               }
            return conDtListwrap ;
        }
        set;
    }
    
    
    
    private Contact_Distributions__c[] forDeletion = new Contact_Distributions__c[]{};
    
      /*
     * @Description : This method clears the search filters(As part of PMO Request # 4613(February 2015 Release) enchanced search functionality).   
     * @ Args       : null     
     * @ Return     : void
     */
    public void clearSearch() {
        try {
            if (searchParam != null) {
                searchParam = ConstantsUtility.STR_Blank;
            }
            if (PR3 != null) {
                PR3 = ConstantsUtility.STR_Blank;
            }
            if (PR2 != null) {
                PR2 = ConstantsUtility.STR_Blank;
            }
            if (PR1 != null) {
                PR1 = ConstantsUtility.STR_Blank;
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        runSearch();
    }
    
    
    
     /*
     * @Description : This method runs the actual query (As part of PMO Request # 4613(February 2015 Release) enchanced search functionality).
     * @ Args       : null
     * @ Return     : void
     */
    public void runQuery() {

        try {
            distributionAll.clear();
            
      
        qString+= ' order by d.Name';
        qString+= ' limit 101';
        
        avlDistributions = database.query(qString);
        
       
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(avlDistributions.size()==101){
            avlDistributions.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
        
        for(Distribution__c dist : avlDistributions )
        {            
            distributionAll.add(new distributionWrapper(dist));
        }
        
       
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }

    }
    
    
       /*
     * @Description : This method removes the distributions from the Distibution's list which are already added to the Contact
     * @ Args       : null
     * @ Return     : void 
    */
     public void unselectFields() {
        ContDistId.clear();
         if(editDistMap.size()>0){
             for(Contact_Distributions__c cd:editDistMap.values())
             {  
                ContDistId.add(cd.Distribution__c);
             }
             String tempDist = ' and Id not in (';
             for(Id i : ContDistId){
                tempDist+= '\'' + (String)i + '\',';
             }
             String distremove = tempDist.substring(0,tempDist.length()-1);
             distremove+= ')';
    
            qString+= distremove;     
        }

     }
    
    
    

   /*
    *  Creation of constructor for AP79_ContactDistributionController class
    */
    public AP79_ContactDistributionController(ApexPages.StandardController controller) {

        theCon = database.query('select Id,Name, currencyISOCode from Contact where Id = \'' + controller.getRecord().Id + '\' limit 1');
        editDistMap = new Map<id,Contact_Distributions__c>([Select id,name,Distribution__c,Distribution_Preference__c, contact__c,Designation_Type__c  from Contact_Distributions__c where Contact__c =:theCon.Id]);
       
        contDistList = new List<Contact_Distributions__c>() ;
        
        qString = 'Select id, d.name, d.LOB__C,d.Scope__c, d.Region__c, d.Market__c, d.Frequency__c, d.Target_Audience__c, d.Description__c, d.Owner.name From Distribution__c d where d.Active__c = true ';
        unselectFields();
        runQuery();
        
    }
    
   /*
    * @Description : Method for searching the distribution
    * @ Args       : null
    * @ Return     : String
    */     
    public PageReference runSearch() {
         try {
           distributionAll.clear();
           //As part of June Release 2014(PMO# 4402) updated code to query some more field to display on layout
           //As part of PMO Request 4654(September Release) added distribution owner field
           qString = 'Select id, d.name, d.LOB__C,d.Scope__c, d.Region__c, d.Market__c, d.Frequency__c, d.Target_Audience__c, d.Description__c, d.Owner.name From Distribution__c d where d.Active__c = true ';

                
        //As part of PMO Request # 4613(February 2015 Release) enchanced search functionality
        if (searchParam != null) {
                    qString += ' and  (d.Name LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\' or d.LOB__c LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\'or d.Region__c LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\' or d.Market__c LIKE \'%' + String.escapeSingleQuotes(searchParam) + '%\')';
                }
            if (STR_Distribution_Name.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (d.Name = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_Distribution_LOB.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (d.LOB__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_Distribution_Region.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (d.Region__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_Distribution_Market.equals(PR3) && STR_Equals.equals(PR1) && PR2 != null) {
                    qString += ' and  (d.Market__c = \'' + String.escapeSingleQuotes(PR2) + '\')';
                }

                if (STR_Distribution_Name.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Name LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_LOB.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (d.LOB__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_Region.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Region__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_Market.equals(PR3) && STR_Contains.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Market__c LIKE \'%' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_Name.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Name LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_LOB.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (d.LOB__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_Region.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Region__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }

                if (STR_Distribution_Market.equals(PR3) && STR_Starts_With.equals(PR1) & PR2 != null) {
                    qString += 'and (d.Market__c LIKE \'' + String.escapeSingleQuotes(PR2) + '%\')';
                }
        
        
        
        unselectFields();
        runQuery();
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));

        }
        return null;
    }
    
   /*
    * @Description : Method for adding selected distribution
    * @ Args       : null
    * @ Return     : String
    */ 
    public PageReference selectedDB(){
            avlDistributions.clear();
            selecteddistribution.clear();
            contDistList.clear();
            for(distributionWrapper dbChecked : distributionAll){
                    if(dbChecked.checked) { 
                        selecteddistribution.add(new distributionWrapper(dbchecked.dist));
                        Contact_Distributions__c contactdistObj= new Contact_Distributions__c(Contact__c=theCon.Id,Distribution__c=dbChecked.dist.id, Distribution_Preference__c='Yes');
                        contDistList.add(contactdistObj);                        
                        editDistMap.put(contactdistObj.Distribution__c, contactdistObj);
                   }
            }
            
          for(Contact_Distributions__c cdd : contDistList)
          {      
             boolean distExists = false;
             for(ContactDistributionWrapper cd1 :conDtListwrap) {
                if(cd1.ConDt.Distribution__c== cdd.Distribution__c){
                    distExists =true;
                }
             }
          
             if(!distExists ) {
                          conDtListwrap.add(new ContactDistributionWrapper(cdd));
             }
         }
         PageReference pageRef = ApexPages.currentPage();          
         runSearch(); 
         return null;           
    }
   
   
    
   
   
   /*
    * @Description : Method for removing selected contact distribution from list
    * @ Args       : null
    * @ Return     : String
    */  
    public PageReference deletebutton() {
            PageReference pageRef;         
            try{
                    integer Count = 0;
                    List<ContactDistributionWrapper> removeconDtListwrap = new List<ContactDistributionWrapper> ();
                    
                    for(ContactDistributionWrapper ContDistdel :  conDtListwrap)
                    {
                         if(ContDistdel.checked == true)
                         {
                           lstContDistDel.clear();      
                           lstContDistDel.add(ContDistdel.ConDt); 
                           
                           if(editDistMap.containsKey(ContDistdel.ConDt.distribution__c)){
                                editDistMap.remove(ContDistdel.ConDt.distribution__c);
                           }
                                   
                         } else {
                            removeconDtListwrap.add(ContDistdel );
                         }
                         count++;
                   }
                   conDtListwrap = removeConDtListwrap;   
            }catch(System.DmlException e){
                  System.debug('insert contact Distribution error==  '+e.getDmlMessage(0));
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
                  pageRef=null;
            }
            runSearch();
            return null; 
    }  

   
   /*
    * @Description : Method for saving saving contact distribution
    * @ Args       : null
    * @ Return     : String
    */ 
    public PageReference savebutton(){
        PageReference pageReference;
              
        try{
             contDistList.clear();
             for(ContactDistributionWrapper cw :conDtListwrap ) {
                contDistList.add(cw.ConDt);
             }    
             insert contDistList;
             system.debug('>>>'+contDistList);
             pageReference = new PageReference(ConstantsUtility.STR_BackSlash + ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ObjID));
        }catch(System.DmlException e){
          System.debug('insert contact Distribution error==  '+e.getDmlMessage(0));
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(0)));
          pageReference=null;
        }
        
         return pageReference;
   }
   
     
   /*
    * @Description : If user hits cancel we commit no changes and return them to the Contact
    * @ Args       : null
    * @ Return     : String
    */  
    public PageReference cancelbutton(){
        return new PageReference(ConstantsUtility.STR_BackSlash + ApexPages.currentPage().getParameters().get(ConstantsUtility.STR_ObjID));
    }
    
    /*
    *   Distribution Wrapper class
    */
    public class distributionWrapper { 
        public Boolean checked{ get; set; }
        public Distribution__c dist {get; set;} 
        
        /*
        *   Constructor for distributionWrapper class
        */
        public distributionWrapper(Distribution__c db){        
            dist = db;
            checked = false;
        } 
    }

   /*
    *   Contact Distribution Wrapper class
    */
    public class ContactDistributionWrapper {
        public Contact_Distributions__c ConDt { get; set;}   
        public Boolean checked {get; set;}
        /*
        *   Constructor for ContactDistributionWrapper class
        */
        public ContactDistributionWrapper(Contact_Distributions__c cd){
            ConDt = cd;
            checked = false;
         }
    }
  
}