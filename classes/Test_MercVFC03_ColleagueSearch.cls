/*Purpose: Test Class for providing code coverage to MercVFC03_ColleagueSearch class
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Suchi   19/19/2012  Created test class
============================================================================================================================================== 
*/
@isTest
private class Test_MercVFC03_ColleagueSearch {
    /* * @Description : This test method provides data coverage for searching a colleague based on First Name and Last Name        
       * @ Args       : no arguments        
       * @ Return     : void       
    */
    static testMethod void Test_MercVFC03_ColleagueSearch()
    {
         
         
         List<Colleague__c> collList= new List<Colleague__c>();        
        //TestData class instantiatedselectVar
           //Mercer_TestData mtdata = new Mercer_TestData();
           //selectVar = mtdata.buildColleaguesearch();
           //collList = mtdata.buildColleagueinstance();
          // for(integer i = 0; i<=5; i++)
           //{     
                Colleague__c testColleague = new Colleague__c();
                testColleague.Name = 'TestColleague';
                testColleague.Last_Name__c = 'testLastName';
                testColleague.EMPLID__c = '1234567890';
                testColleague.LOB__c = '11111';
                testColleague.Email_Address__c = 'abc@gmail.com';
                collList.add(testColleague);
          // }
           insert collList;
              User user1 = new User();
    	String mercerStandardUserProfile = Mercer_TestData.getmercerStandardUserProfile();      
        user1 = Mercer_TestData.createUser1(mercerStandardUserProfile, 'usert2', 'usrLstName2', 2);
        
        system.runAs(User1){
           
           PageReference pageRef = Page.Mercer_searchcolleague;
           Test.setCurrentPage(pageRef);
           ApexPages.currentPage().getParameters().put('Select', collList[0].Id);
           
           
           
       ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(collList[0]);          
       //controller instantiated 
        MercVFC03_ColleagueSearch_Controller controller = new MercVFC03_ColleagueSearch_Controller(stdController);
       collList = controller.getResults();
       controller.setResults(collList);
       controller.Fname = 'TestColleague1';
       controller.Lname = 'testLastName1';
       controller.Wmail = 'abc@gmail.com';
       controller.Enumber = '13242';
       //executeSearch method called   
           controller.fireQuery();
       //createnew method called 
           controller.getResultCount();
           controller.setResultCount(1);
           controller.getResults();
        //   controller.setResults();
           controller.getRenderCount();
           controller.createSelUser();
           //controller.MapCollegueDetailstoUser();
           controller.MapCollegueDetailstoUser(testColleague);
           controller.QueryColleagueAllDetails(testColleague.id);
       //instatiate parameters on page             
           controller.createNewUser();
        //controller.size();
        }
     }

    

}