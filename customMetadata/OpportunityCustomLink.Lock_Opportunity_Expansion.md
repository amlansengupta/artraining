<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lock Opportunity Expansion</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">/apex/MF2_LockOpportunityExpansionCustomLink?oppid=</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Lock Opportunity Expansion (Admin Only)</value>
    </values>
</CustomMetadata>
