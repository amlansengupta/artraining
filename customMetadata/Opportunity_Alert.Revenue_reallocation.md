<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Revenue reallocation</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Check your Rev Start and End Dates and Revenues. They have been auto updated.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Revenue reallocation</value>
    </values>
</CustomMetadata>
