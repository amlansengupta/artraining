<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Opportunity(s) At Pending Project</label>
    <protected>false</protected>
    <values>
        <field>Display_In_New_Window__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">/lightning/o/Opportunity/list?filterName=00B2S000005aKmkUAE</value>
    </values>
    <values>
        <field>Query__c</field>
        <value xsi:type="xsd:string">Select Count() from Opportunity where StageName=&apos;Pending Project&apos; AND RecordType.Name=&apos;Opportunity Detail Page Assignment&apos; AND Age_of_Pending_Project_Stage__c &gt; {1}</value>
    </values>
    <values>
        <field>Threshold__c</field>
        <value xsi:type="xsd:string">14</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Opportunities at Pending Project for 2+ weeks</value>
    </values>
</CustomMetadata>
