<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ScopeIt</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Use ScopeIt Project to scope your opportunity.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">ScopeIt</value>
    </values>
</CustomMetadata>
