<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Marketing Code Message</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">This Account is a Marketing code. The One Code needs to pass CDT audit. See the One Code Audit section under the Projects tab for more info.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Marketing Code Message</value>
    </values>
</CustomMetadata>
