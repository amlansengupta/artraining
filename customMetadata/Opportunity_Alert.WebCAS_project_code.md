<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>WebCAS project code</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">#</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Link WebCAS project code(s) to close this as a win.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">WebCAS project code</value>
    </values>
</CustomMetadata>
