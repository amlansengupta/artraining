<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Past Due Opportunity</label>
    <protected>false</protected>
    <values>
        <field>Display_In_New_Window__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">/lightning/o/Opportunity/list?filterName=00B2S000005aKmjUAE</value>
    </values>
    <values>
        <field>Query__c</field>
        <value xsi:type="xsd:string">Select Count() from Opportunity where StageName IN (&apos;Above the Funnel&apos;,&apos;Identify&apos;,&apos;Active Pursuit&apos;,&apos;Selected&apos;) AND RecordType.Name=&apos;Opportunity Detail Page Assignment&apos; AND CloseDate &lt; TODAY</value>
    </values>
    <values>
        <field>Threshold__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Opportunities that are past due</value>
    </values>
</CustomMetadata>
