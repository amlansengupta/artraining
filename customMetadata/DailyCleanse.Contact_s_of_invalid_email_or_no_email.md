<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact(s) of invalid email or no email</label>
    <protected>false</protected>
    <values>
        <field>Display_In_New_Window__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">/lightning/o/Contact/list?filterName=00B2S000005aKmhUAE</value>
    </values>
    <values>
        <field>Query__c</field>
        <value xsi:type="xsd:string">Select Count() from Contact where Contact_Status__c =&apos;Active&apos;  AND RecordType.Name=&apos;Contact Detail&apos; AND (Invalid_Work_Email__c = true OR Email = &apos;&apos;)</value>
    </values>
    <values>
        <field>Threshold__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Contacts with invalid or no email address</value>
    </values>
</CustomMetadata>
