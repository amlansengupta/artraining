<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Sales Credit</label>
    <protected>false</protected>
    <values>
        <field>Link__c</field>
        <value xsi:type="xsd:string">#</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Allocate sales credit to your team.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Sales Credit</value>
    </values>
</CustomMetadata>
