<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Above the Funnel</label>
    <protected>false</protected>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>STAGENAME__c</field>
        <value xsi:type="xsd:string">Above the Funnel</value>
    </values>
    <values>
        <field>end_date__c</field>
        <value xsi:type="xsd:string">Above_Funnel_Stage_End_Date__c</value>
    </values>
    <values>
        <field>start_date__c</field>
        <value xsi:type="xsd:string">Above_Funnel_Stage_Begin_Date__c</value>
    </values>
</CustomMetadata>
