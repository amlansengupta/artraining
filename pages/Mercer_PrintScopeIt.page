<apex:page standardcontroller="Opportunity" extensions="AP86_pdfGenerator" renderAs="PDF" applyHtmlTag="false" applyBodyTag="false" showHeader="false">
    <head>
        <style type="text/css">
                
                @page 
                {
                size:landscape;
                margin: 2% 2% 2% 2%; 
                @bottom-right {
                content: "Page " counter(page) " of " counter(pages);
                }
               @bottom-left {
                    content: "Date: {!DAY(TODAY())}-{!monthShort}-{!YEAR(TODAY())}";
                    font-family: sans-serif;
                    font-size: 80%;
                            }
                }
            
                *{
                font-family: calibri,sans-serif;
                 font-size:13px;
                }
                
                .break { 
                    page-break-before: always; 
                }
                
                .break1 { 
                    page-break-after: avoid; 
                }
                
                .classA{
                text-align:left;
                }
                
            
            }
        </style>
    </head>
    <body>
        <right>
            <apex:image url="{!$Resource.MercerForceIconPDF}" width="200px" height="35px" />
        </right>
        <center>
             <h1 style="font-size:200%">ScopeIt Opportunity Detail</h1>

        </center>
        <hr style="height:3px;border:block;color:#333;background-color:#333;" />
        <div class="break1">
             <h3 style="font-size:125%"> Opportunity Information </h3>

            <hr/>
            <apex:panelGrid id="thePanel" border="0" columns="2" columnClasses="classA" width="100%">
                <apex:panelGroup >
                    <apex:outputLabel value="Opportunity Name:  " for="theName" />
                    <apex:outputText value="{!opportunity.name}" id="theName" /></apex:panelGroup>
                <apex:panelGroup >
                    <apex:outputLabel value="Account Name:  " for="theAccount" />
                    <apex:outputText value="{!opportunity.Account.name}" id="theAccount" styleClass="Name" /></apex:panelGroup>
                <apex:panelGroup >
                    <apex:outputLabel value="Owner:  " for="theOwner" />
                    <apex:outputText value="{!opportunity.owner.name}" id="theOwner" /></apex:panelGroup>
                <apex:panelGroup >
                    <apex:outputLabel value="OneCode:  " for="theCode" />
                    <apex:outputText value="{!opportunity.OneCode__c}" id="theCode" /></apex:panelGroup>
                <apex:panelGroup >
                    <apex:outputLabel value="Stage:  " for="theStage" />
                    <apex:outputText value="{!opportunity.StageName}" id="theStage" /></apex:panelGroup>
                <apex:panelGroup >
                    <apex:outputLabel value="Region:  " for="theMarket" />
                    <apex:outputText value="{!opportunity.Opportunity_Region__c}" id="theMarket" /></apex:panelGroup>
                <!--<apex:panelGroup>
                <apex:outputLabel value="Product LOBs:" for="theLOB"/>
                <apex:outputText value="{!opportunity.Product_LOBs__c}" id="theLOB"/>
                </apex:panelGroup> 
                                            
                <apex:panelGroup >
                    <Apex:outputLabel value="Total Opportunity Revenue:  " for="TOR" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number, ###,###,##0.00}" id="Amount">
                        <apex:param value="{!opportunity.Amount}" /></apex:outputText>
                </apex:panelGroup>
                -->

                <apex:panelGroup >
                  <apex:outputLabel value="Currency:  " for="theCurrency" />
                  <apex:outputText value="{!opportunity.CurrencyIsoCode}" id="theCurrency" /></apex:panelGroup>                    
                    
                <apex:panelGroup >
                    <apex:outputLabel value="Country:  " for="theCountry" />
                    <apex:outputText value="{!opportunity.Opportunity_Country__c}" id="theCountry" /></apex:panelGroup>

                <apex:outputText />                        
                <apex:outputText />

                 <h3 style="text-decoration:underline;">Price and Profitablity Details</h3>

                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="Total Opportunity Revenue:  " for="TOR1" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number, ###,###,##0.00}" id="Amount1" rendered="{!opportunity.Amount!=null}">
                        <apex:param value="{!opportunity.Amount}" /></apex:outputText>
                </apex:panelGroup>    
                <apex:outputText />            
                <apex:panelGroup >
                    <Apex:outputLabel value="Sales Price (ScopeIt Not Required):  " for="theScopeItPriceNotRequired" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theScopeItPriceNotRequired" rendered="{!opportunity.Sales_Price_ScopeIt_Not_Required__c!=null}">
                        <apex:param value="{!opportunity.Sales_Price_ScopeIt_Not_Required__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="Sales Price (ScopeIt Products):  " for="theScopeItProductsSalesprice" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theScopeItProductsSalesprice" rendered="{!opportunity.Total_Scopable_Products_Revenue__c!=null}">
                        <apex:param value="{!opportunity.Total_Scopable_Products_Revenue__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:panelGroup >
                    <Apex:outputLabel value="Cost (Billable and Non-Billable):  " for="theCost" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theCost" rendered="{!opportunity.Cost2__c!=null}">
                        <apex:param value="{!opportunity.Cost2__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="Profit/Loss:  " for="theProfit" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theProfit" rendered="{!opportunity.Profit_Loss__c!=null}">
                        <apex:param value="{!opportunity.Profit_Loss__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:panelGroup >
                    <Apex:outputLabel value="Billable Time Charges:  " for="theCharges" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theCharges" rendered="{!opportunity.Billable_Time_Charges__c!=null}">
                        <apex:param value="{!opportunity.Billable_Time_Charges__c}" /></apex:outputText>
                </apex:panelGroup>
               
                
          
                <apex:panelGroup >
                    <Apex:outputLabel value="Margin:  " for="theMargin" />
                    <apex:outputText value="{0, number, 00}%" id="theMarginOpp">
                        <apex:param value="{!opportunity.Margin__c }" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="IC Charges:  " for="theICcharge" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theICcharge" rendered="{!opportunity.IC_Charges__c!=null}">
                        <apex:param value="{!opportunity.IC_Charges__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="Planned Fee Adj:  " for="thePlanned" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="thePlanned" rendered="{!opportunity.Planned_Fee_Adj__c!=null}">
                        <apex:param value="{!opportunity.Planned_Fee_Adj__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <!--<apex:panelGroup >
                  <apex:outputLabel value="Planned Fee Adj Desc:  " for="thePlannedDesc" />
                  <apex:outputText value="{!opportunity.Planned_Fee_Adj_Desc__c}" id="thePlannedDesc" /></apex:panelGroup>                   
                <apex:outputText />
                -->
                <apex:panelGroup >
                    <Apex:outputLabel value="Non-Billable Expenses:  " for="theNonBillExp" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theNonBillExp" rendered="{!opportunity.Non_Billable_Expenses__c!=null}">
                        <apex:param value="{!opportunity.Non_Billable_Expenses__c}" /></apex:outputText>
                </apex:panelGroup>
                <apex:outputText />
                <apex:panelGroup >
                    <Apex:outputLabel value="Non-Billable Time Charges:  " for="theNonBillTimeCharges" />
                    <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" id="theNonBillTimeCharges" rendered="{!opportunity.Non_Billable_Time_Charges__c!=null}">
                        <apex:param value="{!opportunity.Non_Billable_Time_Charges__c}" />
                    </apex:outputText>
                </apex:panelGroup>
               
                <apex:panelGroup >
                    <Apex:outputLabel value=">BA:  " for="theBA" rendered="{!opportunity.Margin_BA__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theBA" rendered="{!opportunity.Margin_BA__c!=0}">
                        <apex:param value="{!opportunity.Margin_BA__c }" /></apex:outputText>
              
              <Apex:outputLabel value=">CBO:  " for="theCBO" rendered="{!opportunity.Margin_CBO__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theCBO" rendered="{!opportunity.Margin_CBO__c!=0}">
                        <apex:param value="{!opportunity.Margin_CBO__c }" /></apex:outputText>
                              
               <Apex:outputLabel value=">EH&B:  " for="theEHB" rendered="{!opportunity.Margin_EHB__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theEHB" rendered="{!opportunity.Margin_EHB__c!=0}">
                        <apex:param value="{!opportunity.Margin_EHB__c}" /></apex:outputText>
             
               <Apex:outputLabel value=">INV:  " for="theINV" rendered="{!opportunity.Margin_INV__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theINV" rendered="{!opportunity.Margin_INV__c!=0}">
                        <apex:param value="{!opportunity.Margin_INV__c }" /></apex:outputText>
                
                <Apex:outputLabel value=">RET:  " for="theRET" rendered="{!opportunity.Margin_RET__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theRET" rendered="{!opportunity.Margin_RET__c!=0}">
                        <apex:param value="{!opportunity.Margin_RET__c}" /></apex:outputText>
                <Apex:outputLabel value=">TAL:  " for="theTAL" rendered="{!opportunity.Margin_TAL__c!=0}"/>
                    <apex:outputText value="{0, number, 00}%" id="theTAL" rendered="{!opportunity.Margin_TAL__c!=0}">
                     <apex:param value="{!opportunity.Margin_TAL__c}" /></apex:outputText>
                        
                </apex:panelGroup>
               
                
            </apex:panelGrid>
            <br/>
            <br/>
            <table style="table-layout:auto; width:100%; " border="0" cellspacing="4" cellpadding="0">
                <tr>
                    <th>Project Name</th>
                    <th>LOB</th>
                    <th>Solution Segment</th>
                    <th>Sales Price</th>
                    <th>Planned Fee Adj</th>
                    <th>Profit (Loss)</th>
                    <th>Margin</th>
                </tr>
                <apex:repeat value="{!projectList}" var="s">
                    <tr>
                        <td >
                            <apex:outputField value="{!s.name}" />
                        </td>
                        <td>
                            <apex:outputText value="{!s.LOB__c}" />
                        </td>
                        <td>
                            <apex:outputText value="{!s.Solution_Segment__c}" />
                        </td>
                       <td>
                            <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" style="text-align:right" rendered="{!s.Sales_Price__c!=null}">
                                <apex:param value="{!s.Sales_Price__c}" /></apex:outputText>
                        </td>                        
                        <td>
                            <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" style="text-align:right" rendered="{!s.Planned_Fee_Adj__c!=null}">
                                <apex:param value="{!s.Planned_Fee_Adj__c}" /></apex:outputText>
                        </td>
                        <td>
                            <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" style="text-align:right" rendered="{!s.Profit_Loss__c!=null}">
                                <apex:param value="{!s.Profit_Loss__c}" /></apex:outputText>
                        </td>
                        <td>
                            <apex:outputText value="{!s.Calc_Margin__c}" />
                        </td>
                    </tr>
                </apex:repeat>
            </table>
        </div>
        
        <div >
      
            <apex:repeat value="{!projectWrapperList}" var="a">
              <div class="break"></div>
                <br/>
                <br/>
                <hr/>
                 <h4 style="font-size:125%">Project Summary</h4>

                <hr/>
                <apex:panelGrid columns="2" width="100%">
                    <apex:panelGroup >
                        <Apex:outputLabel value="Project Name:  " for="theProjectName2" />
                        <apex:outputText value="{!a.Project.name}" style="font-style:Arial" id="theProjectName2" /></apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Project LOB:  " for="theProjectLOB" />
                        <apex:outputText value="{!a.Project.LOB__c}" id="theProjectLOB" /></apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Solution Segment:  " for="theSolutionSegment" />
                        <apex:outputText value="{!a.Project.Solution_Segment__c}" id="theProjectName" /></apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Product:  " for="theProduct" />
                        <apex:outputText value="{!a.Project.Product__r.name}" id="theProduct" /></apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Cluster:  " for="theCluster" />
                        <apex:outputText value="{!a.Project.Cluster__c}" id="theCluster" /></apex:panelGroup>
                    <br/>
                    <apex:outputText />
                    <apex:outputText />
                     <h4 style="text-decoration:underline;">Price and Profitablity Details</h4>

                    <apex:outputText />
                    <apex:panelGroup >
                        <Apex:outputLabel value="Sales Price:  " for="theSalesPrice1" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Sales_Price__c!=null}">
                            <apex:param value="{!a.Project.Sales_Price__c}" /></apex:outputText>
                    </apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Cost (Billable and Non-Billable):  " for="theCost" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Cost_Billable_and_Non_Billable__c!=null}">
                            <apex:param value="{!a.Project.Cost_Billable_and_Non_Billable__c}" /></apex:outputText>
                    </apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Billable Expenses:  " for="theBillableExpenses" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Billable_Expenses__c!=null}">
                            <apex:param value="{!a.Project.Billable_Expenses__c}" /></apex:outputText>
                    </apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Profit (Loss):  " for="theProfit" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Profit_Loss__c!=null}">
                            <apex:param value="{!a.Project.Profit_Loss__c}" /></apex:outputText>
                    </apex:panelGroup>
                    <apex:panelGroup >
                        <Apex:outputLabel value="Non-Billable Expenses:  " for="thePNonBillExp" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Non_Billable_Expenses__c!=null}">
                            <apex:param value="{!a.Project.Non_Billable_Expenses__c}" /></apex:outputText>
                    </apex:panelGroup>

                    <apex:panelGroup >
                        <Apex:outputLabel value="Margin:  " for="theMargin" />
                        <apex:outputText value="{0, number, 00}%" id="theMargin" >
                            <apex:param value="{!a.Project.Margin__c}" /></apex:outputText>
                    </apex:panelGroup>
                    
                    <apex:outputText />
                                    
                    <apex:panelGroup >
                        <Apex:outputLabel value="Margin Benchmark:  " for="theBenchmark" />
                        <apex:outputText value="{!a.Project.Margin_Benchmark__c}" escape="false    "/>
                    </apex:panelGroup>
                    
                    
                    <apex:panelGroup >
                        <Apex:outputLabel value="Billable Time Charges:  " for="theCharges" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Billable_Time_Charges__c!=null}">
                            <apex:param value="{!a.Project.Billable_Time_Charges__c}" /></apex:outputText>
                    </apex:panelGroup>
                    
                    <apex:outputText />  
                      <apex:panelGroup >
                        <Apex:outputLabel value="IC Charges:  " for="theIcCharges" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.IC_Charges__c!=null}">
                            <apex:param value="{!a.Project.IC_Charges__c}" /></apex:outputText>
                    </apex:panelGroup> 
                     <apex:outputText />                   
                    <apex:panelGroup >
                        <Apex:outputLabel value="Planned Fee Adj:  " for="thePlanned" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Planned_Fee_Adj__c!=null}">
                            <apex:param value="{!a.Project.Planned_Fee_Adj__c}" /></apex:outputText>
                    </apex:panelGroup>
                    
                    <apex:outputText />  
                                    
                    <apex:panelGroup >
                        <Apex:outputLabel value="Non-Billable Time Charges:  " for="theNCharges" />
                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!a.project.Non_Billable_Time_Charges__c!=null}">
                            <apex:param value="{!a.Project.Non_Billable_Time_Charges__c}" /></apex:outputText>
                    </apex:panelGroup>
                </apex:panelGrid>
                <br/>
                <br/>
                <table style="table-layout:auto; width:100% " cellspacing="4" cellpadding="0">
                    <tr>
                        <th>Task Name</th>
                        <th>Bill Type</th>
                        <th>Expenses</th>
                        <th>Billable Time Charges</th>
                        <th>Non-Billable Time Charges</th> 
                        <th>IC Charges</th> 
                        <th>Bill Rate% Increase</th>
                        <th>Bill Est. # of Increases</th>                                                
                    </tr>
                    <apex:repeat value="{!a.taskList}" var="b">
                        <tr>
                            <td>
                                <apex:outputText value="{!b.Task_Name__c}" />
                            </td>
                            <td>
                                <apex:outputText value="{!b.Bill_Type01__c}" />
                            </td>

                            <td>
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!b.Billable_Expenses__c!=null}">
                                    <apex:param value="{!b.Billable_Expenses__c}" /></apex:outputText>
                            </td>
                            <td>
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!b.Billable_Time_Charges__c!=null}">
                                    <apex:param value="{!b.Billable_Time_Charges__c}" /></apex:outputText>
                            </td>     
                            <td>
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!b.Calc_Non_Billable_Time_Charges__c!=null}">
                                    <apex:param value="{!b.Calc_Non_Billable_Time_Charges__c}" /></apex:outputText>
                            </td>
                            <td>
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!b.IC_Charges__c!=null}">
                                    <apex:param value="{!b.IC_Charges__c}" /></apex:outputText>
                            </td> 
                            <td>
                                <apex:outputText value="{!b.Bill_Rate__c}">
                                </apex:outputText>
                            </td>      
                            <td>
                                <apex:outputText value="{!b.Bill_Est_of_Increases__c}"></apex:outputText>
                            </td>                     
                                                                                       

                        </tr>
                    </apex:repeat>
                </table>
                <br/>
                <br/>
               
                <div>
                    <apex:repeat value="{!a.taskList}" var="c">
                     <div class="break"></div>
                        <hr></hr>
                        
<h4 style="font-size:125%">Task Summary</h4>

                        <hr></hr>
                        <apex:panelGrid columns="2" width="100%">
                            <apex:panelGroup >
                                <Apex:outputLabel value="Task Name:  " for="theTask" />
                                <apex:outputText value="{!c.Task_Name__c}" id="theTask" /></apex:panelGroup>
                            <apex:panelGroup >
                                <Apex:outputLabel value="Bill Type:  " for="theBillType" />
                                <apex:outputText value="{!c.Bill_Type01__c}" id="theBillType" /></apex:panelGroup> 
                            <apex:panelGroup >
                                <Apex:outputLabel value="Expenses:  " for="theBExpense" />
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!c.Billable_Expenses__c!=null}">
                                    <apex:param value="{!c.Billable_Expenses__c}" /></apex:outputText>
                            </apex:panelGroup>
                            <apex:outputText />
                            <apex:panelGroup >
                                <Apex:outputLabel value="Bill Rate% Increase:  " for="theBRate" />
                                <apex:outputText value="{!c.Bill_Rate__c}" id="theBRate" /></apex:panelGroup>
                            <apex:panelGroup >
                                <Apex:outputLabel value="Bill Est. # of Increases:  " for="theIncrease" />
                                <apex:outputText value="{!c.Bill_Est_of_Increases__c}" id="theIncrease" /></apex:panelGroup>

                             <h4 style="text-decoration:underline;">Price and Profitability Details</h4>

                            <apex:outputText />
                            <apex:panelGroup >
                                <Apex:outputLabel value="Billable Time Charges:  " for="theBCharges" />
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!c.Calc_Billable_Time_Charges__c!=null}">
                                    <apex:param value="{!c.Calc_Billable_Time_Charges__c}" /></apex:outputText>
                            </apex:panelGroup>
                            <apex:panelGroup >
                                <Apex:outputLabel value="Cost (Billable and Non-Billable):  " for="theBCharges" />
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!c.Cost_Billable_and_Non_Billable__c!=null}">
                                    <apex:param value="{!c.Cost_Billable_and_Non_Billable__c}" /></apex:outputText>
                            </apex:panelGroup>
                             <apex:panelGroup >
                                <Apex:outputLabel value="IC Charges:  " for="theCharges" />
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!c.IC_Charges__c!=null}">
                                    <apex:param value="{!c.IC_Charges__c}" /></apex:outputText>
                            </apex:panelGroup>
                            <apex:outputText />
                            <apex:panelGroup >
                                <Apex:outputLabel value="Non-Billable Time Charges:  " for="theNBCharges" />
                                <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!c.Calc_Non_Billable_Time_Charges__c!=null}">
                                    <apex:param value="{!c.Calc_Non_Billable_Time_Charges__c}" /></apex:outputText>
                            </apex:panelGroup>
                        </apex:panelGrid>
                        <br/>
                        <br/>
                        <table style="table-layout: auto; width:100%" cellspacing="2" cellpadding="0">
                            <tr>
                                <th>Employee</th>
                                <th>Level</th>
                                <th>Business</th>
                                <th>Market/Country</th>
                                <th>Bill Rate</th>
                                <th>Billable Time Charges</th>
                                <th>Hours</th>
                            </tr>
                            <apex:repeat value="{!c.Employees__r}" var="e">
                                <tr>
                                    <td>
                                        <apex:outputText value="{!e.Name__c}" />
                                    </td>
                                    <td>
                                        <apex:outputText value="{!e.Level__c}" />
                                    </td>                                    
                                    <td>
                                        <apex:outputText value="{!e.Business__c}" />
                                    </td>
                                    <td>
                                        <apex:outputText value="{!e.Market_Country__c}" />
                                    </td>
        
                                    <td>
                                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!e.Bill_Rate_Opp__c!=null}">
                                            <apex:param value="{!e.Bill_Rate_Opp__c}" /></apex:outputText>
                                    </td>  
                                    
                                    <td>
                                        <apex:outputText value="{!opportunity.CurrencyIsoCode} {0, number,###,###,##0.00}" rendered="{!e.Billable_Time_Charges__c!=null}">
                                            <apex:param value="{!e.Billable_Time_Charges__c}" /></apex:outputText>
                                    </td>  
                                    <td>
                                        <apex:outputText value="{!e.Hours__c}" />
                                    </td>
                                </tr>
                            </apex:repeat>
                        </table>
                    </apex:repeat>
                </div>
            </apex:repeat>
        </div>
        
    </body>
</apex:page>