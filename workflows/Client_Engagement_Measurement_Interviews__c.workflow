<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MF2_UpdateCompletionDate</fullName>
        <description>update the completion date to today.</description>
        <field>CEM_Completion_Date_New__c</field>
        <formula>Today()</formula>
        <name>MF2_UpdateCompletionDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MF2_UpdateCompletionDate</fullName>
        <actions>
            <name>MF2_UpdateCompletionDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Client_Engagement_Measurement_Interviews__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
