<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Action_Item_Assigned</fullName>
        <description>Action Item Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>AI_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Action_Item_Assigned</template>
    </alerts>
    <rules>
        <fullName>Action Item Assigned</fullName>
        <actions>
            <name>Action_Item_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow will send an email to action item owners when they are assigned an action item.</description>
        <formula>OR(ISNEW(), ISCHANGED( AI_Owner__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Action Item Owner</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Action_Item__c.AI_Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow will send an email to the owner of an action item.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
