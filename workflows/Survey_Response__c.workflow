<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NPS_Detractor_Email_Alert</fullName>
        <description>NPS Detractor Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Account_RM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NPS_Detractor_Alert2</template>
    </alerts>
    <alerts>
        <fullName>NPS_Passive_Email_Alert</fullName>
        <description>NPS Passive Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Account_RM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NPS_Passive_Alert_2</template>
    </alerts>
    <alerts>
        <fullName>NPS_Promoter_Email_Alert</fullName>
        <description>NPS Promoter Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Account_RM_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NPS_Promoter_Alert_2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Survey_Response_RM_Email_Update</fullName>
        <description>Update Account RM&apos;s email on the Survey response object.</description>
        <field>Account_RM_Email__c</field>
        <formula>Account__r.Relationship_Manager__r.Email_Address__c</formula>
        <name>Survey Response RM Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>NPS Detractor  Rule</fullName>
        <actions>
            <name>NPS_Detractor_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Survey_Response_RM_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 ) AND 8</booleanFilter>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country__c</field>
            <operation>notEqual</operation>
            <value>Australia,New Zealand</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NPS Detractor  Rule - To Fix</fullName>
        <actions>
            <name>NPS_Detractor_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Survey_Response_RM_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Survey_Response__c.Name</field>
            <operation>equals</operation>
            <value>R-001091</value>
        </criteriaItems>
        <description>This rule was create to fix data for a Qualtrics issue . This is no longer needed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NPS Passive Rule</fullName>
        <actions>
            <name>NPS_Passive_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Survey_Response_RM_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>8</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>7</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country__c</field>
            <operation>notEqual</operation>
            <value>Australia,New Zealand</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NPS Promoter Rule</fullName>
        <actions>
            <name>NPS_Promoter_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Survey_Response_RM_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>10</value>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Response__c.Question_2__c</field>
            <operation>equals</operation>
            <value>9</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Country__c</field>
            <operation>notEqual</operation>
            <value>Australia,New Zealand</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
