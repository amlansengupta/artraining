<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CheckIsupdate</fullName>
        <field>Isupdated__c</field>
        <literalValue>1</literalValue>
        <name>CheckIsupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MOGD_check isupdated</fullName>
        <actions>
            <name>CheckIsupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If any of the 6 fields on the Mercer office table is updated then the Mercer Office checkbox is checked.</description>
        <formula>OR(ISNEW(), ISCHANGED( Market__c ) , ISCHANGED( Region__c ), ISCHANGED(Country__c), ISCHANGED( Name ), ISCHANGED( Sub_Market__c ), ISCHANGED( Sub_Region__c ), ISCHANGED( Office__c )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
