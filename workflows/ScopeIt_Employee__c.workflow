<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Billable_TimeCharge</fullName>
        <field>Billable_Time_Charges__c</field>
        <formula>Bill_Rate_Opp__c * Hours__c</formula>
        <name>Update Billable TimeCharge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cost</fullName>
        <field>Cost__c</field>
        <formula>( Bill_Rate_Opp__c / Bill_Cost_Ratio__c ) * Hours__c</formula>
        <name>Update Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ScopeItEmployee_Update_BillTimeCost</fullName>
        <actions>
            <name>Update_Billable_TimeCharge</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ScopeIt_Employee__c.Hours__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
