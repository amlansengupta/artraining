<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Opp_Team_Role_Blank</fullName>
        <field>TeamMemberRole</field>
        <name>Opp Team Role Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opp Owner Role Blank</fullName>
        <actions>
            <name>Opp_Team_Role_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityTeamMember.TeamMemberRole</field>
            <operation>equals</operation>
            <value>Opportunity Owner</value>
        </criteriaItems>
        <description>Opp Owner Role Blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
