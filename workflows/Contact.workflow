<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Blank_out_Deactivation_Date</fullName>
        <description>Created for Request #18724</description>
        <field>Last_Deactivated_Date__c</field>
        <name>Blank out Deactivation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CON01_Status_Pending_Consent</fullName>
        <field>Contact_Status__c</field>
        <literalValue>Pending Consent</literalValue>
        <name>CON01 Status Pending Consent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type</fullName>
        <description>This will change contact record type after contact is created</description>
        <field>RecordTypeId</field>
        <lookupValue>Contact_Detail</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_RingLead_DMS_Status_Pending</fullName>
        <description>Contact - Set RingLead DMS Status to &apos;pending&apos;</description>
        <field>RingLead_DMS_Status__c</field>
        <formula>&quot;pending&quot;</formula>
        <name>Contact - RingLead DMS Status-&gt;Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Deactivated_Date</fullName>
        <description>Crated for Request #18724</description>
        <field>Last_Deactivated_Date__c</field>
        <formula>TODAY()</formula>
        <name>Last Deactivated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Blank out deactivation date for Active Contact</fullName>
        <actions>
            <name>Blank_out_Deactivation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for Request #18724</description>
        <formula>AND(ISCHANGED( Contact_Status__c),  NOT(ISPICKVAL(Contact_Status__c, &apos;Inactive&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CON01 Update Status Pending Consent</fullName>
        <actions>
            <name>CON01_Status_Pending_Consent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Country of the contact is South Korea, the Contact status will be updated to Pending Consent.</description>
        <formula>((  ( ISCHANGED(MailingCountry) ||  ISNEW()) &amp;&amp;  MailingCountry &lt;&gt; null  &amp;&amp;  CONTAINS(UPPER(MailingCountry) ,  &apos;KOREA&apos; ))  || ( (ISCHANGED(OtherCountry ) ||  ISNEW()) &amp;&amp;  OtherCountry &lt;&gt; null  &amp;&amp;  CONTAINS(UPPER(OtherCountry) ,  &apos;KOREA&apos;   ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Deactivation Date</fullName>
        <actions>
            <name>Last_Deactivated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Created for Request #18724</description>
        <formula>AND(ISCHANGED(Contact_Status__c),  ISPICKVAL(Contact_Status__c, &apos;Inactive&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Contact Record Type on Detail</fullName>
        <actions>
            <name>Change_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_ID2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Change the contact record type after the Contact is Created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Set RingLead DMS Status to %27pending%27</fullName>
        <actions>
            <name>Contact_RingLead_DMS_Status_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.RingLead_DMS_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set RingLead DMS Status to &apos;pending&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
