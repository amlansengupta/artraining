<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Global_Contract_On_Account</fullName>
        <field>My_Global_Contract__c</field>
        <formula>TEXT( Global_Contract__c)</formula>
        <name>Update Global Contract On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Rate_Card_on_Account</fullName>
        <field>My_Global_Rate_Card__c</field>
        <formula>TEXT( Global_Rate_Card__c )</formula>
        <name>Update Global Rate Card on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Special_Terms_On_Account</fullName>
        <field>My_Global_Special_Terms__c</field>
        <formula>TEXT( Global_Special_Terms__c )</formula>
        <name>Update Global Special Terms On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_My_Global_Client_Governance</fullName>
        <field>My_Global_Client_Governance__c</field>
        <formula>TEXT(Global_Client_Governance__c)</formula>
        <name>Update My Global Client Governance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_My_Global_Client_Strategy_c</fullName>
        <field>My_Global_Client_Strategy__c</field>
        <formula>TEXT(Global_Client_Strategy__c)</formula>
        <name>Update My Global Client Strategy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_My_Other_Global_Agreement</fullName>
        <field>My_Other_Global_Agreement__c</field>
        <formula>TEXT(Other_Global_Agreement__c)</formula>
        <name>Update My Other Global Agreement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Global_Account_Name__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Global Contract on Account</fullName>
        <actions>
            <name>Update_Global_Contract_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Global_Contract__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Global Rate Card on Account</fullName>
        <actions>
            <name>Update_Global_Rate_Card_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Global_Rate_Card__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Global Special Terms On Account</fullName>
        <actions>
            <name>Update_Global_Special_Terms_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Global_Special_Terms__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update My Global Client Governance</fullName>
        <actions>
            <name>Update_My_Global_Client_Governance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Global_Client_Governance__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update My Global Client Strategy</fullName>
        <actions>
            <name>Update_My_Global_Client_Strategy_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Global_Client_Strategy__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update My Other Global Agreement</fullName>
        <actions>
            <name>Update_My_Other_Global_Agreement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(Other_Global_Agreement__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
