<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendEmailToPMForClosedWon</fullName>
        <description>Send an Email to Project Manager For Closed Won</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/VFT01_Email_AlertClosedWonOpportunities</template>
    </alerts>
    <fieldUpdates>
        <fullName>Blue_Sheet_Attachment_Last_Updated</fullName>
        <description>Blue Sheet Attachment Last Updated field will be updated with Today&apos;s date</description>
        <field>Blue_Sheet_Attachment_Last_Updated__c</field>
        <formula>NOW()</formula>
        <name>Blue Sheet Attachment Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_Update</fullName>
        <description>Close date is updated as today</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Country</fullName>
        <field>Opportunity_Office_for_Final_Country__c</field>
        <formula>TEXT( Opportunity_Country__c )</formula>
        <name>Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ExpectedCloseDatePopulate</fullName>
        <field>Expected_Close_Date__c</field>
        <formula>CloseDate</formula>
        <name>ExpectedCloseDatePopulate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Step_Modified_Date_Update</fullName>
        <field>Last_Step_Modified_Date__c</field>
        <formula>TODAY()</formula>
        <name>Last Step Modified Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Market</fullName>
        <field>Opportunity_Office_for_Final_Market__c</field>
        <formula>TEXT(Opportunity_Market__c)</formula>
        <name>Market</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mercer_Opportunity_CloseDate_Update</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Mercer_Opportunity_CloseDate_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Office</fullName>
        <field>Opportunity_Office_for_Final_Office__c</field>
        <formula>TEXT( Opportunity_Office__c )</formula>
        <name>Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Clear_M_drive_stage_change_date</fullName>
        <field>M_Drive_Stage_Change_Date__c</field>
        <name>Opp Clear M drive stage change date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Stage_Closed_Won_FU</fullName>
        <field>StageName</field>
        <literalValue>Closed / Won</literalValue>
        <name>Opp_Stage_Closed_Won_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_update_stage_change_date</fullName>
        <field>Stage_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Opp update stage change date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Opportunity_Detail_Page_Assinment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity Record Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Record_Type_Change_1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Opportunity_Detail_Page_Assinment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity_Record_Type_Change_1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opty_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Opportunity_Detail_Page_Assinment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opty_Record_Type_Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Project_Stage_Change</fullName>
        <description>Change the stage to Pending Project when step is changed to Pending Project</description>
        <field>StageName</field>
        <literalValue>Pending Project</literalValue>
        <name>Pending Project Stage Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Push_Counter</fullName>
        <field>Push_Count__c</field>
        <formula>IF( !ISNULL(Push_Count__c) , Push_Count__c +1, 1)</formula>
        <name>Push Counter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Region</fullName>
        <field>Opportunity_Office_for_Final_Region__c</field>
        <formula>TEXT(Opportunity_Region__c)</formula>
        <name>Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reminder_checked</fullName>
        <field>Reminder__c</field>
        <literalValue>1</literalValue>
        <name>Reminder checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Above_the_Funnel</fullName>
        <field>StageName</field>
        <literalValue>Above the Funnel</literalValue>
        <name>Stage Update Above the Funnel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Active_Pursuit</fullName>
        <field>StageName</field>
        <literalValue>Active Pursuit</literalValue>
        <name>Stage Update Active Pursuit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Closed</fullName>
        <field>StageName</field>
        <literalValue>Closed / Declined to Bid</literalValue>
        <name>Stage Update Closed/Declined to Bid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Closed_Client_Cancelled</fullName>
        <field>StageName</field>
        <literalValue>Closed / Client Cancelled</literalValue>
        <name>Stage Update Closed/Client Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed / Lost</literalValue>
        <name>Stage Update Closed/Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed / Won</literalValue>
        <name>Stage Update Closed/Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Finalist</fullName>
        <field>StageName</field>
        <literalValue>Finalist</literalValue>
        <name>Stage Update Finalist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Identify</fullName>
        <field>StageName</field>
        <literalValue>Identify</literalValue>
        <name>Stage Update Identify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Selected</fullName>
        <field>StageName</field>
        <literalValue>Selected</literalValue>
        <name>Stage Update Selected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Update_Won_Loss_Debrief</fullName>
        <field>StageName</field>
        <literalValue>Won / Loss Debrief</literalValue>
        <name>Stage Update Won/Loss Debrief</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubMarket</fullName>
        <field>Opportunity_Office_for_Final_Market_Star__c</field>
        <formula>TEXT(Opportunity_Sub_Market__c)</formula>
        <name>SubMarket</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubRegion</fullName>
        <field>Opportunity_Office_for_Final_Sub_Region__c</field>
        <formula>TEXT( Opportunity_Sub_Region__c )</formula>
        <name>SubRegion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePastDueEmailSent</fullName>
        <field>of_Past_Due_Emails_Sent__c</field>
        <formula>IF( OR( NOT( ISNULL( of_Past_Due_Emails_Sent__c ) ), of_Past_Due_Emails_Sent__c = 0 )  , of_Past_Due_Emails_Sent__c + 1, 1)</formula>
        <name>UpdatePastDueEmailSent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ATF_Begin_Date</fullName>
        <field>Above_Funnel_Stage_Begin_Date__c</field>
        <formula>CreatedDate</formula>
        <name>Update ATF Begin Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BlueSheetAttachmentCheck</fullName>
        <field>Blue_Sheet_Attachment__c</field>
        <literalValue>1</literalValue>
        <name>Update_BlueSheetAttachmentCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BlueSheetAttachmentUncheck</fullName>
        <field>Blue_Sheet_Attachment__c</field>
        <literalValue>0</literalValue>
        <name>Update_BlueSheetAttachmentUncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Last_Modified_by_User</fullName>
        <field>Date_Last_Modified_by_User__c</field>
        <formula>today()</formula>
        <name>Update Date Last Modified by User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EmailSent_checkbox</fullName>
        <field>Email_Sent__c</field>
        <literalValue>0</literalValue>
        <name>Update EmailSent checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Identify_Begin_Date</fullName>
        <field>Identify_Stage_Begin_Date__c</field>
        <formula>if(Text(Identify_Stage_Begin_Date__c) =&apos;&apos;, 
IF(TEXT(Above_Funnel_Stage_End_Date__c) &lt;&gt;&apos;&apos;,Above_Funnel_Stage_End_Date__c,TODAY()),Identify_Stage_Begin_Date__c)</formula>
        <name>Update Identify Begin Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PM_Notificaton</fullName>
        <field>NotifyPM__c</field>
        <literalValue>1</literalValue>
        <name>Update PM Notificaton</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Stage change date</fullName>
        <actions>
            <name>Opp_Clear_M_drive_stage_change_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_update_stage_change_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>July Release - Incorrect stage duration - Capture Stage change date when ever stage is updated</description>
        <formula>ISNEW() || ISCHANGED( StageName )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP01_Mercer_Close _Date</fullName>
        <actions>
            <name>Mercer_Opportunity_CloseDate_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Deactivated as part of september release because it is contradicting the validation rule VR13_Closed_Date_Validation_Rule
When the step is changed to any of the &quot;Closed&quot; values the close date is populated to todays date -</description>
        <formula>AND(ISChanged(StageName), OR(ISPICKVAL(StageName,&apos;Closed / Won&apos;),ISPICKVAL(StageName,&apos;Closed / Lost&apos;),ISPICKVAL(StageName,&apos;Closed / Declined to Bid&apos;),ISPICKVAL(StageName, &apos;Closed / Client Cancelled&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP04_Detail_Page_Assignment</fullName>
        <actions>
            <name>Opportunity_Record_Type_Change_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Opportunity New Page Assignment</value>
        </criteriaItems>
        <description>Assign a smaller layout when the new button is clicked.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP05_Stage - Active Pursuit WF</fullName>
        <actions>
            <name>Stage_Update_Active_Pursuit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Presented Proposal,Made Go Decision,Completed Deal Review,Constructed Proposal,Developed Solutions &amp; Strategy</value>
        </criteriaItems>
        <description>Update Stage if step equals either Presented Proposal,Constructed Proposal,Completed Deal Review,Made Go Decision,Developed Solutions &amp; Strategy</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP10_Stage - Finalist WF</fullName>
        <actions>
            <name>Stage_Update_Finalist</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Presented Finalist Presentation,Selected as Finalist,Developed Finalist Strategy,Rehearsed Finalist Presentation</value>
        </criteriaItems>
        <description>Update the Stage field to Finalist when Step is selected to Presented Finalist Presentation,Selected as Finalist,Developed Finalist Strategy,Rehearsed Finalist Presentation</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP11_Stage - Identify WF</fullName>
        <actions>
            <name>Update_Identify_Begin_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identify</value>
        </criteriaItems>
        <description>Update the Stage field to Identify when Step is selected to Identified Single Sales Objective(s),Making Go/No Go Decision,Identified Deal,Assessed Potential Solutions &amp; Strategy</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP12_Stage - Selected WF</fullName>
        <actions>
            <name>Stage_Update_Selected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>contains</operation>
            <value>Selected &amp; Finalizing EL &amp;/or SOW,SOW Signed - Pending WebCAS Projects</value>
        </criteriaItems>
        <description>Update Stage to Selected when Step is Selected &amp; Finalizing EL &amp;/or SOW .</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP13_Stage - Won%2FLoss Debrief WF</fullName>
        <actions>
            <name>Stage_Update_Won_Loss_Debrief</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Won / Loss Debrief</value>
        </criteriaItems>
        <description>Update stage to Won / Loss Debrief when step is Won / Loss Debrief</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP14_Update_LOB_With_ProductFamily</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>This workflow will update LOB field with the value from Product Family field from Product object.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP17_ExpectedCloseDate</fullName>
        <actions>
            <name>ExpectedCloseDatePopulate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP18_Notify Project Manager for Closed%2FWon</fullName>
        <actions>
            <name>SendEmailToPMForClosedWon</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_PM_Notificaton</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Notify Project Manager to create a Project Plan when the Opportunity reaches &quot;Closed/Won&quot; Stage for the first time.
[27/6] - Added fix to not send mail when oppty is created</description>
        <formula>NOT(ISNEW()) &amp;&amp;
ISPICKVAL( StageName , &quot;Closed / Won&quot;)&amp;&amp;
ISCHANGED(StageName) &amp;&amp;
NotifyPM__c = False &amp;&amp; No_Email_Alert__c =FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP20_Blue_Sheet_Attachment_Checked</fullName>
        <actions>
            <name>Blue_Sheet_Attachment_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Blue_Sheet_Attachment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.mh_Last_Updated_Blue_Sheet__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When Blue Sheet Attachment is checked Then Blue Sheet Attachment Last Updated should auto populate with todays date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OPP21_Opportunity Close date reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Reminder__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Send chatter notification to opportunity owner one week before Closing date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_checked</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OPP22_Stage - Above the funnel WF</fullName>
        <actions>
            <name>Update_ATF_Begin_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Above the Funnel</value>
        </criteriaItems>
        <description>Update the Stage field to Above the Funnel when Step is selected to Identified Marketing/Sales Lead, Nurturing Relationship, Executed Discovery, Qualified Lead</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP23_Update_LastModifiedByUserfield</fullName>
        <actions>
            <name>Update_Date_Last_Modified_by_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When ‘real’ user other than &apos;mercerforce &apos;creates or saves the opportunity with any changes, enter today’s date.</description>
        <formula>$User.LastName!=&apos;MercerForce&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP24_Push Counter</fullName>
        <actions>
            <name>Push_Counter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Feb Release. Workflow to update Close Date counter whenever close date is changed to a later date.</description>
        <formula>ISCHANGED ( CloseDate )  &amp;&amp; PRIORVALUE(CloseDate ) &lt; CloseDate &amp;&amp; $User.Id !=$Label.MercerForceUserid</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP25_Update_Past_Due_Emails_Sent</fullName>
        <actions>
            <name>UpdatePastDueEmailSent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED( Email_Sent__c ), Email_Sent__c = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP26_Update_Email_Sent</fullName>
        <actions>
            <name>Update_EmailSent_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( CloseDate )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP_Stage - Pending Project WF</fullName>
        <actions>
            <name>Pending_Project_Stage_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Pending Chargeable Code</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp_ClosedWon_WF</fullName>
        <actions>
            <name>Opp_Stage_Closed_Won_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Closed / Won (SOW Signed)</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Close Date Update for Sales to Revenue products</fullName>
        <actions>
            <name>Close_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Step__c</field>
            <operation>equals</operation>
            <value>Pending Chargeable Code</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Final field Updates</fullName>
        <actions>
            <name>Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Market</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Office</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SubMarket</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SubRegion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISPICKVAL(Opportunity_Office__c,&apos; &apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Step Modified Date</fullName>
        <actions>
            <name>Last_Step_Modified_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(  Step__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
