<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Lead_Ringlead_DMS_Status_Pending</fullName>
        <description>Lead Ringlead DMS Status uodate to &apos;Pending&apos;</description>
        <field>RingLead_DMS_Status__c</field>
        <formula>&quot;pending&quot;</formula>
        <name>Lead - Ringlead DMS Status -&gt; Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lead - Set RingLead DMS Status to %27pending%27</fullName>
        <actions>
            <name>Lead_Ringlead_DMS_Status_Pending</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.mkto71_Original_Source_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set RingLead DMS Status to &apos;pending&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
