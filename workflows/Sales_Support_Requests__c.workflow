<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Assigned_Processor_Change_Notification_for_CBF</fullName>
        <description>Assigned Processor Change Notification for CBF</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_CBP_Group</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>archisman.ghosh@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dinesh.sharma@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Assigned_Processor_Notification</template>
    </alerts>
    <alerts>
        <fullName>Delivery_Date_Change_Notification_for_CBF</fullName>
        <description>Delivery Date Change Notification for CBF</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_CBP_Group</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>archisman.ghosh@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dinesh.sharma@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Delivery_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Due_Date_Notification</fullName>
        <description>Due Date Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>archisman.ghosh@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christine.diaz@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>grace.celentano@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.kavoulakis-mayes@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Due_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Peer_Reviewer_Change_Notification_for_CBF</fullName>
        <description>Peer Reviewer Change Notification for CBF</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_CBP_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Peer_Reviewer_Notification</template>
    </alerts>
    <alerts>
        <fullName>Projected_Meeting_Date_Notification</fullName>
        <description>Projected Meeting Date Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>archisman.ghosh@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>christine.diaz@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>grace.celentano@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jay.steinmetz@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.kavoulakis-mayes@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Projected_Meeting_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Requestor_Acknowledgement_For_CBP</fullName>
        <description>Requestor Acknowledgement For SBC</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Request_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Request_CBP_Form_Update_Alert_Mail</fullName>
        <description>Sales Support Request CBP Form Update Alert Mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_CBP_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SBC_Notification_CBP</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Request_Presentation_Update_Alert_Mail</fullName>
        <description>Sales Support Request Presentation Update Alert Mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_Prstn_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SBC_Notification_Presentation</template>
    </alerts>
    <alerts>
        <fullName>Sales_Support_Request_Proposal_Update_Alert_Mail</fullName>
        <description>Sales Support Request Proposal Update Alert Mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_Proposal_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SBC_Notification_Proposal</template>
    </alerts>
    <alerts>
        <fullName>Status_Change_Notification_for_CBF</fullName>
        <description>Status Change Notification for CBF</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Support_Requests_CBP_Group</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>archisman.ghosh@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dinesh.sharma@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mpowerhelp@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Status_Notification</template>
    </alerts>
    <rules>
        <fullName>Assigned Processor Change Notification CBF</fullName>
        <actions>
            <name>Assigned_Processor_Change_Notification_for_CBF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;, ISCHANGED(  Assigned_To__c ),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Delivery Date Change Notification CBF</fullName>
        <actions>
            <name>Delivery_Date_Change_Notification_for_CBF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;, ISCHANGED(  GOSS_Delivery_Date__c  ),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Due Date Notification</fullName>
        <actions>
            <name>Due_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Client_Due_Date__c),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Peer Reviewer Change Notification CBF</fullName>
        <actions>
            <name>Peer_Reviewer_Change_Notification_for_CBF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;, ISCHANGED(  Peer_Reviewer__c  ),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Projected Meeting Date Notification</fullName>
        <actions>
            <name>Projected_Meeting_Date_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Projected_Meeting_Date__c),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SBC Notification for CBP</fullName>
        <actions>
            <name>Requestor_Acknowledgement_For_CBP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SBC Notification for Presentation</fullName>
        <actions>
            <name>Requestor_Acknowledgement_For_CBP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( RecordType.DeveloperName = &apos;Presentation_request_form&apos;,LEN( Account__c )&gt;0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SBC Notification for Proposal Form</fullName>
        <actions>
            <name>Requestor_Acknowledgement_For_CBP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( RecordType.DeveloperName = &apos;Proposal_request_form&apos;,LEN( Account__c )&gt;0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SBC Update Notification for CBP</fullName>
        <actions>
            <name>Sales_Support_Request_CBP_Form_Update_Alert_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SBC Update Notification for Presentation</fullName>
        <actions>
            <name>Sales_Support_Request_Presentation_Update_Alert_Mail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( RecordType.DeveloperName = &apos;Presentation_request_form&apos;,LEN( Account__c )&gt;0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SBC Update Notification for Proposal Form</fullName>
        <actions>
            <name>Sales_Support_Request_Proposal_Update_Alert_Mail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( RecordType.DeveloperName = &apos;Proposal_request_form&apos;,LEN( Account__c )&gt;0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Status Change Notification CBF</fullName>
        <actions>
            <name>Status_Change_Notification_for_CBF</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Client Briefing Pack (US &amp; Canada)&apos;, ISCHANGED( Status_Administrative_use_only__c ),NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
