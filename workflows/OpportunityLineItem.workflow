<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Edit_field_Update</fullName>
        <field>CurrentYearRevenue_edit__c</field>
        <formula>CurrentYearRevenue__c</formula>
        <name>Edit field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NumberOfLives</fullName>
        <field>Number_Of_Lives__c</field>
        <formula>NumberOfLivesFormula__c</formula>
        <name>NumberOfLives</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPPR_skipvalidateupdate</fullName>
        <field>Skip_Validations__c</field>
        <literalValue>0</literalValue>
        <name>OPPR_skipvalidateupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Oppty_LOB_update</fullName>
        <field>LOB_Old__c</field>
        <formula>Product2.LOB__c</formula>
        <name>Oppty LOB update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rev_End_Date_FU</fullName>
        <field>Revenue_End_Date__c</field>
        <formula>Revenue_Start_Date__c + (Revenue_End_Date__c -  PRIORVALUE( Revenue_Start_Date__c ) )</formula>
        <name>Rev End Date FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rev_Start_Date_FU</fullName>
        <field>Revenue_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Rev Start Date FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CurrentYearRevenue_edit</fullName>
        <field>CurrentYearRevenue_edit__c</field>
        <formula>CurrentYearRevenue__c</formula>
        <name>Update CurrentYearRevenue_edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Scope_Reqd</fullName>
        <field>Scope_Reqd01__c</field>
        <literalValue>1</literalValue>
        <name>Update Scope Reqd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Year2Revenue_edit</fullName>
        <field>Year2Revenue_edit__c</field>
        <formula>Year2Revenue__c</formula>
        <name>Update Year2Revenue_edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Year3Revenue_edit</fullName>
        <field>Year3Revenue_edit__c</field>
        <formula>Year3Revenue__c</formula>
        <name>Update Year3Revenue_edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validate_Opp_Product_Flag_False</fullName>
        <description>Validate Opp Product Flag False</description>
        <field>validateOppProductFlag__c</field>
        <literalValue>0</literalValue>
        <name>Validate Opp Product Flag False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OPPR01_Mercer_OptyProd_Segment_Cluster</fullName>
        <actions>
            <name>Oppty_LOB_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the cluster/segment/LOB on opportunity product.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPR02_Update edit fields with formula fields</fullName>
        <actions>
            <name>Update_CurrentYearRevenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year2Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year3Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates value of CurrentYearRevenue_edit, Year2Revenue_edit, Year3Revenue_edit same as the value of CurrentYearRevenue, Year2Revenue, Year3Revenue</description>
        <formula>(text(Revenue_Start_Date__c)&lt;&gt; &apos;&apos; ||  text(Revenue_End_Date__c)&lt;&gt;&apos;&apos; || text(UnitPrice)&lt;&gt;&apos;&apos;) &amp;&amp;
(text( CurrentYearRevenue_edit__c )=&apos;&apos; || CurrentYearRevenue_edit__c =0) &amp;&amp;
(text(  Year2Revenue_edit__c  )=&apos;&apos; || Year2Revenue_edit__c=0 )&amp;&amp;
(text( Year3Revenue_edit__c) =&apos;&apos; || Year3Revenue_edit__c=0)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPPR03_Update edit fields with formula fields</fullName>
        <actions>
            <name>Update_CurrentYearRevenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year2Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year3Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>!ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPR04_Update edit fields when opp close date is changed</fullName>
        <actions>
            <name>Update_CurrentYearRevenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year2Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Year3Revenue_edit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This will fire only when revenue dates are changed. this will not fire when cyr or yr2 or yr3 values changed.</description>
        <formula>!ISNEW() &amp;&amp; OR(ISCHANGED( Revenue_Start_Date__c ), ISCHANGED(Revenue_End_Date__c))&amp;&amp; Opportunity.IsRevenueDateAdjusted__c =TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPR05_Mercer_OptyProd_ScopeReqd</fullName>
        <actions>
            <name>Update_Scope_Reqd</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Classification__c</field>
            <operation>equals</operation>
            <value>Consulting Solution Area</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPR06_skipvalidations</fullName>
        <actions>
            <name>OPPR_skipvalidateupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Skip_Validations__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity_Closed_Won_WF</fullName>
        <actions>
            <name>Rev_End_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Rev_Start_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Opportunity.IsClosed &amp;&amp; Opportunity.IsWon &amp;&amp;  (Revenue_Start_Date__c &lt; Opportunity.CloseDate)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Validate Opp Product Validate Flag False</fullName>
        <actions>
            <name>Validate_Opp_Product_Flag_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.validateOppProductFlag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set Validate Opp Product Validate Flag False</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
