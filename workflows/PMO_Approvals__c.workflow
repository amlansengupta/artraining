<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PMO_Approval_Notification</fullName>
        <description>PMO Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PMO_Approval_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_ApprovedRejected_Date</fullName>
        <field>Approved_Rejected_Date__c</field>
        <formula>Today()</formula>
        <name>Update ApprovedRejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CommentsHistory</fullName>
        <field>Comments_History__c</field>
        <formula>PRIORVALUE( Comments__c ) + BR() +
LastModifiedBy.Username + BR() +
TEXT(DATEVALUE(LastModifiedDate)) + BR() +
Comments_History__c</formula>
        <name>Update CommentsHistory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RequestStage</fullName>
        <field>Request_Stage__c</field>
        <formula>TEXT(Request__r.Status__c)</formula>
        <name>Update RequestStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PMO01_Update RequestStage</fullName>
        <actions>
            <name>Update_RequestStage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PMO01_Update RequestStage</description>
        <formula>NOT(ISNULL(Request__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PMO02_Update ApprovedRejected Date</fullName>
        <actions>
            <name>Update_ApprovedRejected_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PMO_Approvals__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>PMO02_Update ApprovedRejected Date</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PMO03_Update Approval CommentsHistory</fullName>
        <actions>
            <name>Update_CommentsHistory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PMO03_Update Approval CommentsHistory</description>
        <formula>ISCHANGED(Comments__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PMO04_Assign Approval</fullName>
        <actions>
            <name>PMO_Approval_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>PMO04_Assign Approval</description>
        <formula>OR(ISNEW(), ISCHANGED( Approver__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
