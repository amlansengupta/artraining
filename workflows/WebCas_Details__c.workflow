<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_If_Project_Added</fullName>
        <field>Is_Project_Added__c</field>
        <literalValue>1</literalValue>
        <name>Check If Project Added</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Sales_Professional__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>ProjectCode_Check Is Project Added</fullName>
        <actions>
            <name>Check_If_Project_Added</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(ISCHANGED(WebCas_Project_Number__c),ISNEW()),NOT(ISBLANK(WebCas_Project_Number__c)),  Sales_Professional__r.Is_Project_Added__c = false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
