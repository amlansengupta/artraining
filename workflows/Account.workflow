<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <fieldUpdates>
        <fullName>ACCT_ID_Update</fullName>
        <field>ACCT_ID__c</field>
        <formula>ACCT_IDf__c</formula>
        <name>ACCT ID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_Update_DateLastModified</fullName>
        <field>User_Last_Modified_Date__c</field>
        <formula>NOW()</formula>
        <name>Acc_Update_DateLastModified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Address_Update</fullName>
        <field>Account_Address_Updated_date__c</field>
        <formula>TODAY()</formula>
        <name>Account Address Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Class_default</fullName>
        <field>Account_Segmentation__c</field>
        <literalValue>Not Yet Assigned</literalValue>
        <name>Account Class default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Detail_Page_Assignment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account_Record_Type_Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Tier_Default</fullName>
        <field>Account_Tier__c</field>
        <literalValue>Tier 3</literalValue>
        <name>Account Tier Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Blank_Layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Global_Intermediate_View</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Blank Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Auto_Merge_Flag</fullName>
        <field>Auto_Merge_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Check Auto Merge Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Country_Code</fullName>
        <field>Country_Code__c</field>
        <formula>RIGHT(TEXT(Country__c),2)</formula>
        <name>Country Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_GOC_ID</fullName>
        <description>Delete GOC_ID when INT_ID is equal to GOC_ID</description>
        <field>GOC_ID__c</field>
        <formula>IF(GOC_ID__c = INT_ID__c, &quot;&quot;, GOC_ID__c)</formula>
        <name>Delete GOC_ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Erase_Auto_Merge_Date</fullName>
        <field>Auto_Merge_Date__c</field>
        <name>Erase Auto Merge Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GU_Flag_Check</fullName>
        <field>GU_Flag__c</field>
        <literalValue>1</literalValue>
        <name>GU Flag Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GU_Flag_Uncheck</fullName>
        <field>GU_Flag__c</field>
        <literalValue>0</literalValue>
        <name>GU Flag Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Growth_Team_FU</fullName>
        <field>Growth_Team_New__c</field>
        <formula>Geo_D_Link__r.Growth_Team__c</formula>
        <name>Geo D Link Not Null Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Country_FU</fullName>
        <field>Managed_Country_New__c</field>
        <formula>Geo_D_Link__r.Country__c</formula>
        <name>Geo D Link Not Null Managed Country FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Office_FU</fullName>
        <field>Managed_Office_Details__c</field>
        <formula>Geo_D_Link__r.Office__c</formula>
        <name>Geo D Link Not Null Managed Office FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Region_FU</fullName>
        <field>Managed_Region_New__c</field>
        <formula>Geo_D_Link__r.Region__c</formula>
        <name>Geo D Link Not Null Managed Region FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Sub_RegionFU</fullName>
        <field>Managed_Sub_Region_New__c</field>
        <formula>Geo_D_Link__r.Sub_Region__c</formula>
        <name>Geo D Link Not Null Managed Sub-RegionFU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Zone_FU</fullName>
        <field>Managed_Zone_New__c</field>
        <formula>Geo_D_Link__r.Market__c</formula>
        <name>Geo D Link Not Null Managed Zone FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Geo_D_Link_Not_Null_Managed_Zone_Star_FU</fullName>
        <field>Managed_Zone_Star_New__c</field>
        <formula>Geo_D_Link__r.Sub_Market__c</formula>
        <name>Geo D Link Not Null Managed Zone* FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Gold_Sheet_Attachment_Last_Updated</fullName>
        <description>Gold Sheet Attachment Last Updated field updated with today&apos;s date</description>
        <field>Gold_Sheet_Attachment_Last_Updated__c</field>
        <formula>Now()</formula>
        <name>Gold Sheet Attachment Last Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Growth_Team_From_US_Growth_Team_FU</fullName>
        <field>Growth_Team_New__c</field>
        <formula>US_Growth_Team__r.Growth_Team__c</formula>
        <name>Growth Team From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Country_From_US_Growth_Team_FU</fullName>
        <field>Managed_Country_New__c</field>
        <formula>US_Growth_Team__r.Country__c</formula>
        <name>Managed Country From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Office</fullName>
        <field>Managed_Office_Details__c</field>
        <formula>Relationship_Manager__r.Office__c</formula>
        <name>Managed Office</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Office_From_US_Growth_Team_FU</fullName>
        <field>Managed_Office_Details__c</field>
        <formula>US_Growth_Team__r.Office__c</formula>
        <name>Managed Office From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Office_Update</fullName>
        <description>Update Managed Office*</description>
        <field>Managed_Office_Details__c</field>
        <formula>Managed_Office__r.Office__c</formula>
        <name>Managed Office Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Office_Update_is_Null</fullName>
        <field>Managed_Office_Details__c</field>
        <formula>Relationship_Manager__r.Location__r.Office__c</formula>
        <name>Managed Office Update is Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Region_From_US_Growth_Team_FU</fullName>
        <field>Managed_Region_New__c</field>
        <formula>US_Growth_Team__r.Region__c</formula>
        <name>Managed Region From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_SubRegion_From_US_Growth_Team_FU</fullName>
        <field>Managed_Sub_Region_New__c</field>
        <formula>US_Growth_Team__r.Sub_Region__c</formula>
        <name>Managed SubRegion From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Zone_From_US_Growth_Team_FU</fullName>
        <field>Managed_Zone_New__c</field>
        <formula>US_Growth_Team__r.Market__c</formula>
        <name>Managed Zone From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managed_Zone_Star_From_US_Growth_Team_FU</fullName>
        <field>Managed_Zone_Star_New__c</field>
        <formula>US_Growth_Team__r.Sub_Market__c</formula>
        <name>Managed Zone* From US Growth Team FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_GOC_ID_to_INT_ID</fullName>
        <field>INT_ID__c</field>
        <formula>GOC_ID__c</formula>
        <name>Move GOC ID to INT ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Auto_Merge_Date</fullName>
        <field>Auto_Merge_Date__c</field>
        <formula>Today()+7</formula>
        <name>Populate Auto Merge Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_Country_FU</fullName>
        <field>Managed_Country_New__c</field>
        <formula>Managed_Office__r.Country__c</formula>
        <name>RM Office (E) Not Null US&amp;C Country FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_GrowthTeamFU</fullName>
        <field>Growth_Team_New__c</field>
        <formula>Managed_Office__r.Growth_Team__c</formula>
        <name>RM Office (E) Not Null US&amp;C GrowthTeamFU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_Office_FU</fullName>
        <field>Managed_Office_Details__c</field>
        <formula>Managed_Office__r.Office__c</formula>
        <name>RM Office (E) Not Null US&amp;C Office FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_Region_FU</fullName>
        <field>Managed_Region_New__c</field>
        <formula>Managed_Office__r.Region__c</formula>
        <name>RM Office (E) Not Null US &amp;C Region FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_S_Region_FU</fullName>
        <field>Managed_Sub_Region_New__c</field>
        <formula>Managed_Office__r.Sub_Region__c</formula>
        <name>RM Office (E) Not Null US &amp;C S-Region FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_Zone_FU</fullName>
        <field>Managed_Zone_New__c</field>
        <formula>Managed_Office__r.Market__c</formula>
        <name>RM Office (E) Not Null US&amp;C Zone FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Office_E_Not_Null_US_C_Zone_Star_FU</fullName>
        <field>Managed_Zone_Star_New__c</field>
        <formula>Managed_Office__r.Sub_Market__c</formula>
        <name>RM Office (E) Not Null US&amp;C Zone* FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remuneration_Review_Date_Edited_FU</fullName>
        <description>To capture the date everytime the field ‘Remuneration Review Date (Remuneration_Review_Date__c)’ is changed</description>
        <field>Remuneration_Review_Date_Edited__c</field>
        <formula>TODAY()</formula>
        <name>Remuneration Review Date Edited FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCountryField</fullName>
        <field>BillingCountry</field>
        <formula>(TRIM(LEFT(TEXT(Country__c) ,LEN(TEXT(Country__c))-4)))</formula>
        <name>UpdateCountryField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Auto_Merge_Date</fullName>
        <field>Auto_Merge_Date__c</field>
        <formula>Today()+7</formula>
        <name>Update Auto Merge Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BypassVR</fullName>
        <field>ByPassVR__c</field>
        <literalValue>0</literalValue>
        <name>Update BypassVR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_D_U_N_S_number</fullName>
        <field xsi:nil="true"/>
        <formula>DUNS__c</formula>
        <name>Update D-U-N-S number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_One_Code_Status</fullName>
        <description>One Code Status will be updated to &apos;Pending&apos; when an Account record gets created.</description>
        <field>One_Code_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update_One_Code_Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Normal</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Detail_Page_Assignment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Normal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Read_Only_View</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_The_Account_Client_Type</fullName>
        <field>Type</field>
        <literalValue>Marketing</literalValue>
        <name>Update Account Client Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACC01 _Default Marketing</fullName>
        <actions>
            <name>Update_The_Account_Client_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notEqual</operation>
            <value>informaticaadmin@mercer.com</value>
        </criteriaItems>
        <description>Default the Type to marketing if the username creating is not equal to informaticaadmin@mercer.com</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACC02 _Detail_Page_Assignment</fullName>
        <actions>
            <name>Account_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Assign the New page layout on record creation</description>
        <formula>AND($RecordType.Name  = &apos;Account_New_Page_Assignment&apos;,  NOT(ISPICKVAL( One_Code_Status__c , &apos;Inactive&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACC03_Erase Auto Merge Date Based On Auto Merge Flag</fullName>
        <actions>
            <name>Erase_Auto_Merge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Auto_Merge_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If  the Auto-Merge flag is unchecked (manually) the Auto-Merge date  should be empty  if there was a value, it should be erased</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC04_Populate Auto Merge Date based on Auto Merge Flag</fullName>
        <actions>
            <name>Populate_Auto_Merge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Auto_Merge_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the Auto-Merge flag is re-checked (manually) the Auto-Merge date should populate with the current system sate + 7 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC05_Update Auto Merge and Auto Merge Date</fullName>
        <actions>
            <name>Check_Auto_Merge_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Auto_Merge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Account.Workflow_Submit_Status__c</field>
            <operation>equals</operation>
            <value>CANCELLED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.One_Code_Status__c</field>
            <operation>equals</operation>
            <value>Pending - Workflow Wizard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Inactivation_Code__c</field>
            <operation>equals</operation>
            <value>CD9</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.One_Code_Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Marketing</value>
        </criteriaItems>
        <description>Scenario 1:
if workflow status equals &quot;CANCELLED&quot; AND One Code Status equals &quot;Pending - Workflow Wizard&quot;

Scenario 2:
If Inactivation code equals &quot;CD9&quot; and One code status equals &quot;Inactive&quot; and Client Type equals &quot;Marketing&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC06_UpdateOneCodeStatusOnAccCreation</fullName>
        <actions>
            <name>Update_One_Code_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_ID__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notEqual</operation>
            <value>informaticaadmin@mercer.com</value>
        </criteriaItems>
        <description>Value of One_Code_Status__c field should be set to &apos;Pending&apos; when new accounts are created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACC07 Update D-U-N-S number</fullName>
        <actions>
            <name>Update_D_U_N_S_number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>DUNS__c &lt;&gt; &apos;&apos; &amp;&amp; ISCHANGED(DUNS__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC10_Country</fullName>
        <actions>
            <name>UpdateCountryField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate the OOB country from the country picklist field.</description>
        <formula>AND(NOT(ISPICKVAL(Country__c , &quot;&quot;)),OR(ISCHANGED(Country__c), ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC11 Country Code</fullName>
        <actions>
            <name>Country_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate the country code from the Country Picklist</description>
        <formula>AND(NOT(ISPICKVAL(Country__c , &quot;&quot;)),OR(ISCHANGED(Country__c), ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC12 GU Flag Check</fullName>
        <actions>
            <name>GU_Flag_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the GU DUNS # and DUNS# are the same then GU flag checkbox should be unchecked.</description>
        <formula>AND(GU_Flag__c = FALSE,  !ISBLANK(DUNS__c), DUNS__c =  GU_DUNS__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC13 GU Flag Uncheck</fullName>
        <actions>
            <name>GU_Flag_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If DUN# not equal to GU DUNS# then the GU flag gets unchecked.</description>
        <formula>AND(GU_Flag__c = TRUE,  DUNS__c &lt;&gt;  GU_DUNS__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC14_AssignBlankLayout</fullName>
        <actions>
            <name>Assign_Blank_Layout</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Intermediate Level</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Global Level</value>
        </criteriaItems>
        <description>This is used to assign the Global layout to those accounts with Client type global or intermediate.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC15_AssignReadOnlyLayout</fullName>
        <actions>
            <name>Update_Record_Type_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.One_Code_Status__c</field>
            <operation>contains</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Duplicate_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Assign a read only layout for those accounts which have one code status inactice and duplicate flag true.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC16_AssignNormalLayout</fullName>
        <actions>
            <name>Update_Record_Type_Normal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.One_Code_Status__c</field>
            <operation>notEqual</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Duplicate_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>contains</operation>
            <value>Client,Marketing,Multi-Client</value>
        </criteriaItems>
        <description>Assign a normal layout for those accounts which have one code status is acitve</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC17 Populate ACCT ID</fullName>
        <actions>
            <name>ACCT_ID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC18 Deleted GOC ID</fullName>
        <actions>
            <name>Move_GOC_ID_to_INT_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.GOC_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.INT_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC19 Delete GOC ID</fullName>
        <actions>
            <name>Delete_GOC_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.GOC_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.INT_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Move GOC_ID to INT_ID</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC20 Update Auto Merge Flag</fullName>
        <actions>
            <name>Check_Auto_Merge_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Auto_Merge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Inactivation_Code__c</field>
            <operation>equals</operation>
            <value>CD9</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Inactivation_Reason__c</field>
            <operation>equals</operation>
            <value>CRM Merge</value>
        </criteriaItems>
        <description>When the following conditions are met:

Inactivation Code = &quot;CD9&quot;
Inactivation Description = &quot;CRM Merge&quot;

The System shall set the &quot;Auto Merge Flag&quot; checkbox = TRUE.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC21_Gold_Sheet_Attachment_Checked</fullName>
        <actions>
            <name>Gold_Sheet_Attachment_Last_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Gold_Sheet_Attachment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Gold_Sheet_Attachment_Last_Updated__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When Gold Sheet Attachment is checked then Gold Sheet Attachment Last Updated field should auto populate with todays date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC22 Update Auto Merge Flag- Completed</fullName>
        <actions>
            <name>Check_Auto_Merge_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Auto_Merge_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Workflow_Submit_Status__c</field>
            <operation>equals</operation>
            <value>COMPLETE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Duplicate_Client_Code_Rejected__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.One_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When the following conditions are met: 

Workflow Submit Status = COMPLETE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ACC23_Account Updation Date Update</fullName>
        <actions>
            <name>Account_Address_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED( BillingCity ),ISCHANGED( BillingState ),ISCHANGED( BillingStreet ),ISCHANGED( BillingPostalCode ), ISCHANGED( BillingCountry ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC24_Update_LastModifiedByUserfield</fullName>
        <actions>
            <name>Acc_Update_DateLastModified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When ‘real’ user other than &apos;mercerforce &apos;creates or saves the Account with any changes, enter today’s date. (Implemented for Request #18848)</description>
        <formula>$User.LastName!=&apos;MercerForce&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>AccOwnerAddedToAccTeamOnAccCreation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>Account Owner gets added to Account Team whenever an Acount Record gets created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Managed Office Update</fullName>
        <actions>
            <name>Managed_Office_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Managed_Office__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Managed Office Update is Null</fullName>
        <actions>
            <name>Managed_Office_Update_is_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISBLANK( Managed_Office__c ), OR( NOT( ISPICKVAL(Account_Country__c, &apos;United States&apos; )),  BillingCountry &lt;&gt; &apos;United States&apos;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM Changes Managed Office Update</fullName>
        <actions>
            <name>Managed_Office</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Relationship_Manager__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM Office %28E%29 %26 US Growth Team Null WF</fullName>
        <actions>
            <name>Geo_D_Link_Not_Null_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Country_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Office_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Region_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Sub_RegionFU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Zone_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Geo_D_Link_Not_Null_Managed_Zone_Star_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT( AND(NOT(ISBLANK(Managed_Office__c)), OR(ISPICKVAL(Account_Country__c, &apos;United States&apos;),ISPICKVAL(Account_Country__c, &apos;Canada&apos;))   ) ), NOT( AND(ISBLANK(Managed_Office__c),ISPICKVAL(Account_Country__c, &apos;United States&apos;), BillingCountry = &apos;United States&apos;,  NOT(ISBLANK(US_Growth_Team__c))) ),  NOT(ISBLANK(Geo_D_Link__c))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RM Office %28E%29 Not Null For US %26 Canada WF</fullName>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_Country_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_GrowthTeamFU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_Office_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_Region_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_S_Region_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_Zone_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RM_Office_E_Not_Null_US_C_Zone_Star_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(Managed_Office__c)), OR(ISPICKVAL(Account_Country__c, &apos;United States&apos;),ISPICKVAL(Account_Country__c, &apos;Canada&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remuneration Review Date Last Updated WF</fullName>
        <actions>
            <name>Remuneration_Review_Date_Edited_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>WF to capture the date everytime the field ‘Remuneration Review Date (Remuneration_Review_Date__c)’ is changed.</description>
        <formula>ISNEW() ||  ISCHANGED(Remuneration_Review_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Class and Account Tier when created</fullName>
        <actions>
            <name>Account_Class_default</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Tier_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>1. To default the values for Account Class and Account Tier 

PMO 3263 - October Release</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>US Growth Team Not Null WF</fullName>
        <actions>
            <name>Growth_Team_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_Country_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_Office_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_Region_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_SubRegion_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_Zone_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managed_Zone_Star_From_US_Growth_Team_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(Managed_Office__c),ISPICKVAL(Account_Country__c, &apos;United States&apos;), BillingCountry = &apos;United States&apos;, NOT(ISBLANK(US_Growth_Team__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
