<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Record_Count_Batch_Email_Alert</fullName>
        <description>Record Count Batch Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>amit.neogi@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kaushik.bose@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rajeswar.das@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>soham.dan@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sulbha.joshi@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Record_Count_Batch_Template</template>
    </alerts>
</Workflow>
