<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Remove_Unit_Test_Completed_On</fullName>
        <field>Unit_Test_Completed_On__c</field>
        <name>Remove Unit Test Completed On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unit_Test_Completed_On</fullName>
        <field>Unit_Test_Completed_On__c</field>
        <formula>TODAY()</formula>
        <name>Update Unit Test Completed On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unit_Test_Result_to_0</fullName>
        <field>Unit_Test_Result__c</field>
        <formula>0</formula>
        <name>Update Unit Test Result to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unit_Test_Result_to_0_5</fullName>
        <field>Unit_Test_Result__c</field>
        <formula>0.5</formula>
        <name>Update Unit Test Result to 0.5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unit_Test_Result_to_1</fullName>
        <field>Unit_Test_Result__c</field>
        <formula>1</formula>
        <name>Update Unit Test Result to 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Unit Test Completed On when Failed</fullName>
        <actions>
            <name>Remove_Unit_Test_Completed_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Script__c.Unit_Test_Status__c</field>
            <operation>equals</operation>
            <value>Failed,Not Started</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unit Test Completed On when Passed</fullName>
        <actions>
            <name>Update_Unit_Test_Completed_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Script__c.Unit_Test_Status__c</field>
            <operation>equals</operation>
            <value>Passed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unit Test Result when Failed</fullName>
        <actions>
            <name>Update_Unit_Test_Result_to_0_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Script__c.Unit_Test_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Unit Test Result when None</fullName>
        <actions>
            <name>Update_Unit_Test_Result_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Script__c.Unit_Test_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Unit Test Result when Passed</fullName>
        <actions>
            <name>Update_Unit_Test_Result_to_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Test_Script__c.Unit_Test_Status__c</field>
            <operation>equals</operation>
            <value>Passed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
