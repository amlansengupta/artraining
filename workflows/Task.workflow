<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RFC_Task_Assigned_Email</fullName>
        <description>RFC Task Assigned Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TaskEmailSendVFEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>RFC_Task_MQL_Opp_Notification</fullName>
        <description>RFC Task MQL Opp Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/MQLOpportunityEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>RFC_Task_RFC_Opp_Notification</fullName>
        <description>RFC Task RFC Opp Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/RFCOpportunityEmailTemplate</template>
    </alerts>
    <fieldUpdates>
        <fullName>Task_Assigned_Staus_Change_Date_FU</fullName>
        <field>Assigned_Status_Change_Date__c</field>
        <formula>IF( ((ISCHANGED(Status) ||  ISNEW()) &amp;&amp;  ISPICKVAL(Status, &apos;Assigned&apos;)), TODAY(),Assigned_Status_Change_Date__c)</formula>
        <name>Task_Assigned_Staus_Change_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Assigned_Staus_End_Date_FU</fullName>
        <field>Assigned_Status_End_Date__c</field>
        <formula>IF( ISPICKVAL(Status, &apos;Assigned&apos;),Null,IF( (ISCHANGED(Status) &amp;&amp;  TEXT(PRIORVALUE(Status)) = &apos;Assigned&apos; ), TODAY(),Assigned_Status_End_Date__c))</formula>
        <name>Task_Assigned_Staus_End_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_In_Progress_Staus_Change_Date_FU</fullName>
        <field>In_Progress_Status_Change_Date__c</field>
        <formula>IF( ((ISCHANGED(Status) ||  ISNEW()) &amp;&amp; ISPICKVAL(Status, &apos;In Progress&apos;)), TODAY(), In_Progress_Status_Change_Date__c )</formula>
        <name>Task_In_Progress_Staus_Change_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_In_Progress_Staus_End_Date_FU</fullName>
        <field>In_Progress_Status_End_Date__c</field>
        <formula>IF( ISPICKVAL(Status, &apos;In Progress&apos;),Null, IF(    (ISCHANGED(Status) &amp;&amp; 
       TEXT(PRIORVALUE(Status)) = &apos;In Progress&apos; ),       TODAY(),In_Progress_Status_End_Date__c))</formula>
        <name>Task_In_Progress_Staus_End_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Not_Started_Staus_Change_Date_FU</fullName>
        <field>Not_Started_Status_Change_Date__c</field>
        <formula>IF( ((ISCHANGED(Status) ||  ISNEW()) &amp;&amp; ISPICKVAL(Status, &apos;Not Started&apos;)), TODAY(),Not_Started_Status_Change_Date__c)</formula>
        <name>Task_Not_Started_Staus_Change_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Not_Started_Staus_End_Date_FU</fullName>
        <field>Not_Started_Status_End_Date__c</field>
        <formula>IF( ISPICKVAL(Status, &apos;Not Started&apos;),Null,IF( (ISCHANGED(Status) &amp;&amp; TEXT(PRIORVALUE(Status)) = &apos;Not Started&apos; ), TODAY(),Not_Started_Status_End_Date__c))</formula>
        <name>Task_Not_Started_Staus_End_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Reassigned_Staus_Change_Date_FU</fullName>
        <field>Reassigned_Status_Change_Date__c</field>
        <formula>IF( ((ISCHANGED(Status) ||  ISNEW()) &amp;&amp; ISPICKVAL(Status, &apos;Reassigned&apos;)), TODAY(),Reassigned_Status_Change_Date__c)</formula>
        <name>Task_Reassigned_Staus_Change_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Reassigned_Staus_End_Date_FU</fullName>
        <field>Reassigned_Status_End_Date__c</field>
        <formula>IF( ISPICKVAL(Status, &apos;Reassigned&apos;),Null,IF( (ISCHANGED(Status) &amp;&amp; TEXT(PRIORVALUE(Status)) = &apos;Reassigned&apos; ), TODAY(),Reassigned_Status_End_Date__c))</formula>
        <name>Task_Reassigned_Staus_End_Date_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completion_Date</fullName>
        <field>Completion_Date__c</field>
        <formula>Today()</formula>
        <name>Update Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_to_Marketo</fullName>
        <description>Type Picklist is set to Marketo by default.</description>
        <field>Type</field>
        <literalValue>Marketo</literalValue>
        <name>Update Type to Marketo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>RFC Task Assigned Notification</fullName>
        <actions>
            <name>RFC_Task_Assigned_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>RFC Task Assigned Notification</description>
        <formula>RecordType.Name == &apos;RFC Task&apos; &amp;&amp; ( ISPICKVAL(Status,&apos;Assigned&apos;) || ISPICKVAL(Status,&apos;Reassigned&apos;)) &amp;&amp; ISCHANGED(OwnerId) &amp;&amp;  ISPICKVAL(LeadSource__c, &apos;Request Consultation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RFC Task MQL Opp Notification</fullName>
        <actions>
            <name>RFC_Task_MQL_Opp_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>RFC Task MQL Opportunity Notification</description>
        <formula>RecordType.Name == &apos;RFC Task&apos; &amp;&amp; ISPICKVAL(Status, &apos;Closed - Create Opportunity&apos;) &amp;&amp;  Opportunity_Country__c = &apos;United States&apos; &amp;&amp;    !ISPICKVAL(LeadSource__c, &apos;Request Consultation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RFC Task MQL Opp Notification1</fullName>
        <actions>
            <name>RFC_Task_MQL_Opp_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>RFC Task MQL Opportunity Notification1</description>
        <formula>RecordType.Name == &apos;RFC Task&apos; &amp;&amp; ISPICKVAL(Status, &apos;Closed - Create Opportunity&apos;) &amp;&amp; (Opportunity_Country__c != &apos;United States&apos; &amp;&amp; Opportunity_Country__c != &apos;US&apos; &amp;&amp; Opportunity_Country__c != &apos;USA&apos;) &amp;&amp; WhatId != null &amp;&amp;    Owner:User.FirstName !=  &apos;CEC&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RFC Task RFC Opp Notification</fullName>
        <actions>
            <name>RFC_Task_RFC_Opp_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>RFC Task RFC Opportunity Notification</description>
        <formula>RecordType.Name == &apos;RFC Task&apos;&amp;&amp; ISPICKVAL(Status, &apos;Closed - Create Opportunity&apos;) &amp;&amp;  Opportunity_Country__c == &apos;United States&apos; &amp;&amp;    ISPICKVAL(LeadSource__c, &apos;Request Consultation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TASK01_Update_Completion_Date</fullName>
        <actions>
            <name>Update_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed,Completed - Opportunity Created,Closed - Create Opportunity,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Completion_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TASK02_Default Type to Marketo</fullName>
        <actions>
            <name>Update_Type_to_Marketo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Marketo Sync</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>notEqual</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Whenever type is selected as &quot;Marketo&quot; Task  is auto-populated with &quot;Marketo&quot; whenever a Task is created by user with profile = Marketo Sync.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task_Capture_Status_Update_Date_WF</fullName>
        <actions>
            <name>Task_Assigned_Staus_Change_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Assigned_Staus_End_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_In_Progress_Staus_Change_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_In_Progress_Staus_End_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Not_Started_Staus_Change_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Not_Started_Staus_End_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Reassigned_Staus_Change_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Reassigned_Staus_End_Date_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
