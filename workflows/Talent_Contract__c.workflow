<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opportunity_Closed_Won_Non_renewal</fullName>
        <ccEmails>nats.support@mercer.com</ccEmails>
        <description>Opportunity Closed/Won (Non-renewal)</description>
        <protected>false</protected>
        <recipients>
            <recipient>al.dambrosio@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phil.edgecombe@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TalentTS_Opportunity_Closed_Won_Non_renewal</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Won_Non_renewal_GlobalMembership</fullName>
        <ccEmails>Support.NA.Membership@mercer.com</ccEmails>
        <description>Opportunity Closed/Won (Non-renewal) - GlobalMembership</description>
        <protected>false</protected>
        <recipients>
            <recipient>al.dambrosio@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phil.edgecombe@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TalentTS_Opportunity_Closed_Won_Non_renewal</template>
    </alerts>
    <alerts>
        <fullName>TC_Cancellation_Email_Alert</fullName>
        <ccEmails>software@mercer.com</ccEmails>
        <description>TC - Cancellation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Talent Solutions Account Coordinator</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Talent Solutions Strategic Account Mgr</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TalentTS_Contract_Cancellation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Product_Status_Date_Update</fullName>
        <field>Product_Status_Date__c</field>
        <formula>Today()</formula>
        <name>Product Status Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TC01 - Contract Expiration</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Talent_Contract__c.Product_Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Talent_Contract__c.Product__c</field>
            <operation>notEqual</operation>
            <value>Rewards Membership</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TC_Cancellation_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Talent_Contract__c.Product_Expiration_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TC02 - Opportunity Closed%2FWon %28Non-renewal%29</fullName>
        <actions>
            <name>Opportunity_Closed_Won_Non_renewal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Talent_Contract__c.Auto_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Talent_Contract__c.Product__c</field>
            <operation>notEqual</operation>
            <value>Rewards Membership</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TC04 - Opportunity Closed%2FWon %28Non-renewal%29 - GM</fullName>
        <actions>
            <name>Opportunity_Closed_Won_Non_renewal_GlobalMembership</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Talent_Contract__c.Auto_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Talent_Contract__c.Product__c</field>
            <operation>equals</operation>
            <value>Rewards Membership</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TC06 - Product Status Date Update</fullName>
        <actions>
            <name>Product_Status_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Product_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
