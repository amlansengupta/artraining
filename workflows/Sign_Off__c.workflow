<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approval_Date</fullName>
        <field>Sign_Off_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approval Date</fullName>
        <actions>
            <name>Approval_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sign_Off__c.Sign_Off_Status__c</field>
            <operation>equals</operation>
            <value>Conditional Approval,Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
