<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Defect_Assigned</fullName>
        <description>Defect Assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Developer_Assigned__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Defect_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Defect_Ready_for_Validation</fullName>
        <description>Defect Ready for Validation</description>
        <protected>false</protected>
        <recipients>
            <field>Reported_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Defect_Assigned_for_Validation</template>
    </alerts>
    <rules>
        <fullName>Defect Assigned</fullName>
        <actions>
            <name>Defect_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>This workflow will send an email to Defect Assigned when defects are assigned to them.</description>
        <formula>OR(ISNEW(), ISCHANGED(  Developer_Assigned__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Defect Assigned for Validation</fullName>
        <actions>
            <name>Defect_Ready_for_Validation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Defect__c.Status__c</field>
            <operation>equals</operation>
            <value>4. Ready to Validate</value>
        </criteriaItems>
        <description>This workflow will send an email a user who reported a defect as a reminder for validation.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
