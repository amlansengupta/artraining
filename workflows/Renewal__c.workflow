<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Opportunity_Closed_Won_Renewal</fullName>
        <ccEmails>nats.support@mercer.com</ccEmails>
        <description>Opportunity Closed/Won (Renewal)</description>
        <protected>false</protected>
        <recipients>
            <recipient>al.dambrosio@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phil.edgecombe@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TalentTS_Opportunity_Closed_Won_Renewal</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Won_Renewal_GM</fullName>
        <ccEmails>Support.NA.Membership@mercer.com</ccEmails>
        <description>Opportunity Closed/Won (Renewal) - GM</description>
        <protected>false</protected>
        <recipients>
            <recipient>al.dambrosio@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phil.edgecombe@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TalentTS_Opportunity_Closed_Won_Renewal</template>
    </alerts>
    <rules>
        <fullName>TC03 - Opportunity Closed%2FWon %28Renewal%29</fullName>
        <actions>
            <name>Opportunity_Closed_Won_Renewal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Renewal__c.Auto_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Talent_Contract__c.Product__c</field>
            <operation>notEqual</operation>
            <value>Rewards Membership</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TC05 - Opportunity Closed%2FWon %28Renewal%29 - GM</fullName>
        <actions>
            <name>Opportunity_Closed_Won_Renewal_GM</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Renewal__c.Auto_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Talent_Contract__c.Product__c</field>
            <operation>equals</operation>
            <value>Rewards Membership</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
