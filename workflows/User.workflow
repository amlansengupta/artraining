<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_Alert_for_any_admin_user_create_edit</fullName>
        <description>Send Email Alert for any admin user create/edit</description>
        <protected>false</protected>
        <recipients>
            <recipient>rajeswar.das@mercer.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TimeOffManager/Admin_User_Touched</template>
    </alerts>
    <fieldUpdates>
        <fullName>Profile_Update_Date</fullName>
        <field>Last_Activation_Date__c</field>
        <formula>IF(!CONTAINS(Profile.Name,&apos;Lite&apos;) || Profile.Name=&apos;Mercer System Admin Lite&apos;, TODAY(),null)</formula>
        <name>Profile Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Title</fullName>
        <field>Title</field>
        <formula>UserRole.Name</formula>
        <name>Set Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Amin User Created or Edited</fullName>
        <actions>
            <name>Send_Email_Alert_for_any_admin_user_create_edit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>MercerForce</value>
        </criteriaItems>
        <description>Send alert for Amin User Created or Edited</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Title</fullName>
        <actions>
            <name>Set_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Title</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Mercer Premier Community</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>USER_01_Profile Update Date</fullName>
        <actions>
            <name>Profile_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Feb Release. Workflow to capture the date when User License was changed, activated.</description>
        <formula>IsActive = True &amp;&amp; ( ISCHANGED(IsActive) ||  ISCHANGED( ProfileId ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
