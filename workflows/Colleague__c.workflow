<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CheckIsupdate</fullName>
        <field>Isupdated__c</field>
        <literalValue>1</literalValue>
        <name>CheckIsupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Deactivated_On_Field</fullName>
        <field>Deactivated_On__c</field>
        <formula>TODAY()</formula>
        <name>Update Deactivated On Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Coll01_check isupdated</fullName>
        <actions>
            <name>CheckIsupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the location field is updated it checks the isupdated flag.</description>
        <formula>ISCHANGED( Location__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Coll02_deactivated_on</fullName>
        <actions>
            <name>Update_Deactivated_On_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Colleague__c.Empl_Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
