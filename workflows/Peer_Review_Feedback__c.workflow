<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Peer_Review_Feedback_asignee_email_alert</fullName>
        <description>Peer Review Feedback asignee email alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Peer_Review_Feedback_email_notification</template>
    </alerts>
    <alerts>
        <fullName>Peer_Review_Feedback_validator_email_alert</fullName>
        <description>Peer Review Feedback validator email alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>mercerforcesupport@mercer.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Peer_Review_Feedback_email_notification</template>
    </alerts>
</Workflow>
