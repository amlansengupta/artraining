<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateCloseDate</fullName>
        <field>Date_Closed__c</field>
        <formula>TODAY()</formula>
        <name>UpdateCloseDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Date_Opened_Field</fullName>
        <description>When the Sensitive/At Risk record is created, and the status is Open,the Date Opened will default to today.</description>
        <field>Date_Opened__c</field>
        <formula>IF( ISBLANK( Date_Opened__c ) , Today(),Date_Opened__c  )</formula>
        <name>Update the Date Opened Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Date Opened on Sensitive%2FAt Risk Record</fullName>
        <actions>
            <name>Update_the_Date_Opened_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Sensitve_At_Risk__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UpdateCloseDateFieldOnSensitive%2FAtRisk</fullName>
        <actions>
            <name>UpdateCloseDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Sensitve_At_Risk__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Lost Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sensitve_At_Risk__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Partially Retained</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sensitve_At_Risk__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Retained</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
