<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Date_Scoped_field</fullName>
        <field>Date_Scope_Initiated__c</field>
        <formula>IF(AND(PRIORVALUE(Cost_Billable_and_Non_Billable__c)=0, Cost_Billable_and_Non_Billable__c&gt;0), Today(), null )</formula>
        <name>Update Date Scoped field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ScopeITProject Update Date Scoped Field</fullName>
        <actions>
            <name>Update_Date_Scoped_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( OR( AND(  PRIORVALUE(Cost_Billable_and_Non_Billable__c)=0,Cost_Billable_and_Non_Billable__c&gt;0 ), AND( PRIORVALUE(Cost_Billable_and_Non_Billable__c)&gt;0,Cost_Billable_and_Non_Billable__c=0 ) ), true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
