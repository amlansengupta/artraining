/*
Purpose:  This trigger adds Opportunity Owner in Opportunity Team when Opty record is created or Opty Owner is updated.     
==============================================================================================================================================
History 
----------------------- VERSION     AUTHOR  DATE        DETAIL    1.0 -    Arijit 12/19/2012  Created Apex Trigger for Opty(before insert, after insert, before delete)
============================================================================================================================================== 
*/

trigger TRG03_OppTeamMemberTrigger on OpportunityTeamMember (before insert,after insert,After delete,after update,before delete) {
    
    private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember(); 
    if(Trigger.isInsert)
    {  
        System.debug('In opp team member trigger');
        if(Trigger.isBefore)
        {
            AP03_OpportunityTeamMemberTriggerUtil.processOpportunityTeamMemberBeforeInsert(trigger.new);
        }
                   
        if(trigger.isAfter)
        {
            AP03_OpportunityTeamMemberTriggerUtil.processOpportunityTeamMemberAfterInsert(trigger.new);
            if(!isDMUser)
               
                AP15_SalesCreditTeamTriggerUtil.validateSalesCreditAfterInsert2(trigger.new);
                
            AP03_OpportunityTeamMemberTriggerUtil.UpdateOpportunityAfterInsert (trigger.newmap);
             
        }   
    }
    
    
    
    if(Trigger.isDelete)
    {
       /* if(trigger.isBefore)
        {
            System.debug('HI 38');
            Set<id> ownerSet = new Set<Id>();
            set<id> oppSet = new Set<Id>();
            for(OpportunityTeamMember otm:trigger.old){
                  ownerSet.add(otm.userId);
                oppSet.add(otm.OpportunityId);
            }
            map<Id,Id> mapOppOwner = new Map<Id,Id>();
            if(!oppSet.isEmpty()){
                for(opportunity opp:[select id from Opportunity where ownerId in:ownerSet and Id in:oppSet]){
                  mapOppOwner.put(opp.Id,opp.OwnerId);  
                }
                if(!mapOppOwner.isEmpty()){
                    for(OpportunityTeamMember otm:trigger.old){
                        if(mapOppOwner.containskey(otm.OpportunityId)&& mapOppOwner.get(otm.OpportunityId)==otm.userid){
                            otm.addError('Current Opportunity Owner Can not be deleted from Team');
                        }
                    }
                }
                
            }
            //AP03_OpportunityTeamMemberTriggerUtil.processSalesCredteAfterDelete(trigger.old);           
        }*/
        if(trigger.isAfter)
        {
            AP03_OpportunityTeamMemberTriggerUtil.processOpportunityTeamMemberBeforedelete(trigger.oldmap);
            //AP03_OpportunityTeamMemberTriggerUtil.processSalesCredteAfterDelete(trigger.old);           
        }  
        if(trigger.isAfter)
        {
       // if(!ConstantsUtility.oppTeamsAfterUpdatekip)
            AP03_OpportunityTeamMemberTriggerUtil.processOpportunityAfterUpdate(trigger.oldmap);
        }
    }
    if(trigger.isupdate)
    {
        /* if(trigger.isbefore)
        {
            set<Id> OppId= new set<Id>();
            set<Id> teamMemId = new Set<Id>();
            for(OpportunityTeamMember otm: trigger.new){
            OppId.add(otm.OpportunityId);
                teamMemId.add(otm.Id);
            }
            List<OpportunityTeamMember> lstOppMember =[select id,Role__c from OpportunityTeamMember where OpportunityId in:OppId and id not in: teamMemId and role__c='Opportunity Owner' ];
            if(!lstOppMember.isEmpty()){
                for(OpportunityTeamMember ot:lstOppMember){
                    ot.Role__c='';
                }
            }
        }*/
        System.debug('In oppteammember update');
        if(trigger.isafter)
        {
            //18970-Fix2
            if(!AP03_OpportunityTeamMemberTriggerUtil.skipFlag){
            AP03_OpportunityTeamMemberTriggerUtil.processOpportunityAfterUpdate(trigger.newmap);
            }
            //AP15_SalesCreditTeamTriggerUtil.validateSalesCreditAfterInsert2(trigger.new);
                set<Id> OppId= new set<Id>();
            List<OpportunityTeamMember> listOppTm = new List<OpportunityTeamMember>();
            List<OpportunityTeamMember> lOppTmUpdate = new List<OpportunityTeamMember>();
            for(OpportunityTeamMember otm: trigger.new){
            OppId.add(otm.OpportunityId);
            }
            Map<Id,Opportunity> mOpp = new Map<Id,Opportunity>();
            for(Opportunity opp :[select id,ownerId From Opportunity where id in:OppId]){
            mOpp.put(opp.id,opp);
            }
            for(OpportunityTeamMember tm: trigger.new){
            if(!tm.flag__c && mOpp.containskey(tm.OpportunityId) &&(mOpp.get(tm.OpportunityId).OwnerId==tm.userId))
            listOppTm.add(tm);
            }
            if(!listOppTm.isEmpty()){
             System.debug('OppTeam'+listOppTm);
             AP15_SalesCreditTeamTriggerUtil.validateSalesCreditAfterInsert2(listOppTm);
             for(OpportunityTeamMember om:listOppTm){
             OpportunityTeamMember otm = new OpportunityTeamMember(id = om.id);
             otm.flag__c= true;
             lOppTmUpdate.add(otm);
             }
             if(!lOppTmUpdate.isEmpty()){
             update lOppTmUpdate;
             }
        }
        }
    }
}