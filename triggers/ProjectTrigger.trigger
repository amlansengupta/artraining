/*Made Inactive as per the trigger refactoring*/
trigger ProjectTrigger on Revenue_Connectivity__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {
    
    TriggerDispatcher.Run(new ProjectTriggerHandler());
}