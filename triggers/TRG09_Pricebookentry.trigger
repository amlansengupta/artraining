trigger TRG09_Pricebookentry on Product2 (after insert, after update, before insert) {

    boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember();
    if(trigger.isAfter && Trigger.isInsert && System.Label.CL53_ByPassTrigger=='false')
    { 
        AP59_PricebookentryTriggerUtil.processPbAfterInsert(trigger.new);
        
    }
   
    if(Trigger.isUpdate && !isDMUser)
    { 
        AP59_PricebookentryTriggerUtil.processPbAfterUpdate(trigger.newMap, trigger.oldMap);
                 
    } 
    
    if(trigger.isBefore && trigger.isInsert){
        AP59_PricebookentryTriggerUtil.processProductBeforeInsert(trigger.new);
    }
    
}