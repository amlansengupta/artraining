/*Purpose:  This trigger checks if only one record for each Planned Year is created per Account
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   30/10/2014  (As part of Request # 5367)Created Apex Trigger
   2.0 -    Apoorva     05/08/2016   (As part of Request #9790 added after Update trigger.)
 
============================================================================================================================================== 
*/
trigger TRG27_GrowthPlanTrigger on Growth_Plan__c (before insert, before update,after update,after insert) {
    if(Trigger.IsBefore)
    {
    if(Trigger.isInsert )
    {
         AP115_GrowthPlanTriggerUtil.processGrowthPlanBeforeInsert(Trigger.new) ;
         
         
         Set<Id> parentAccountIdsF = new Set<Id>();
         Map<id, Account> relatedAccountMap = new Map<Id, Account>();
         
         for(Growth_Plan__c gpFObj : trigger.new){
            if(gpFObj.Account__c != null){
                parentAccountIdsF.add(gpFObj.Account__c);
            }
        }
    
        if(parentAccountIdsF != null && parentAccountIdsF.size() > 0) {
            relatedAccountMap  = new Map<id, Account>([select id, Name, One_Code__c from Account where id in :parentAccountIdsF]);
        }
         
        
        for(Growth_Plan__c gcPlanF : Trigger.new){
          
            if(relatedAccountMap != null && relatedAccountMap.size() > 0 && relatedAccountMap.containsKey(gcPlanF.Account__c)) {
                
                
                gcPlanF.GP_One_Code__c = relatedAccountMap.get(gcPlanF.Account__c).One_Code__c;
                gcPlanF.GP_Account_Name__c = relatedAccountMap.get(gcPlanF.Account__c).Name;
            }
        }
    }
    
    
   
    if(Trigger.isUpdate) 
    {
         AP115_GrowthPlanTriggerUtil.processGrowthPlanBeforeUpdate(trigger.newmap, trigger.oldmap);
         
    }
    }
    if(Trigger.IsAfter)
    {
        if(Trigger.IsUpdate)
        {
            Boolean flag = false;
            for(Growth_Plan__c gp : trigger.new)
            {
                
               
                    if(gp.Parent_Growth_Plan_ID__c != trigger.oldMap.get(gp.Id).Parent_Growth_Plan_ID__c ) {
                      
                        flag = true;
                    
                    }
                 if(checkRecursion.runOnce()){
                if(gp.Parent_Growth_Plan_ID__c != null && gp.Parent_Growth_Plan_ID__c != trigger.oldMap.get(gp.Id).Parent_Growth_Plan_ID__c){
                  GPParentChildTriggerUtil.parentChildafterUpdate(trigger.new);
                }
                 } 
                
            }
            
            if(flag == true)
            {
                AP115_GrowthPlanTriggerUtil.processGrowthPlanAfterUpdate(trigger.newmap,trigger.oldmap);
            }
        }
        
       if(Trigger.IsInsert)
        {
            AP115_GrowthPlanTriggerUtil.processGrowthPlanAfterInsert(trigger.newmap);
            CreatingFModelForGP.creatingForcasTModel(Trigger.newMap);
        }
        
       
    }
    
    
}