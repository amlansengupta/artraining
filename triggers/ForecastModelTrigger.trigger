trigger ForecastModelTrigger on Forecast_Model__c (after update,before insert) { 
    if(trigger.isAfter && Trigger.isUpdate){
        List<Forecast_Model__c> forecastModelList = new List<Forecast_Model__c>();
        for(Forecast_Model__c fMObj : trigger.new){
            if(fMObj.Sub_Business__c ==null ){
                forecastModelList.add(fMObj);
                //System.debug('fMObj.Current_FY_Revenue__c='+fMObj.Current_FY_Revenue__c);
            } 
        }
        if(forecastModelList.size() > 0){
            UpdateGPFYPlanRevenue.updateGPValue(forecastModelList);
        }
    }
   
}