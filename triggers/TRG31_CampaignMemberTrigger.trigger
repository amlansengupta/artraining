/*Purpose:  Trigger to query camapign member records to restrict users from deleting campaign members
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE            DETAIL 
   1.0 -    Sarbpreet  2/3/2016         Created Apex Trigger
  
============================================================================================================================================== 
*/
trigger TRG31_CampaignMemberTrigger on CampaignMember (after delete) {
    
    if(trigger.isdelete)
    AP133_CampaignMemberTriggerUtil.campaignMemberAfterDelete(Trigger.old, true); 


}