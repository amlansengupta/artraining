/*
Trigger to update the Currency ISO Code
*/
trigger ScopeIt_Mod_Task_before_Trigger on Scope_Modeling_Task__c (before insert, before update) {
    
    for(Scope_Modeling_Task__c objScop:trigger.new)
        objScop.CurrencyIsoCode = objScop.Opportunity_currency_code__c;
  
}