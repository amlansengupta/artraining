/*Purpose:  This trigger checks if User is being made active and was not active previously ,ten set u.Newly_Licenensed__c = true , so that batch can process Accounts for the colleage related to user (if there is any)
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Reetika   07/19/20132  if User is being made active and was not active previously ,ten set u.Newly_Licenensed__c = true , so that batch can process Accounts for the colleage related to user (if there is any)
2.0      Sarbpreet 28/09/203    Added  functionality to automatically remove Supervisor when user is deactivated
3.0      Gyan       25/10/2013    Added functionality to add User into AllMercerForce User Group
============================================================================================================================================== 
*/
trigger TRG20_UserTrigger on User (before insert,before update,after insert,after update) {
    
    if(ConstantsUtility.STR_ACTIVE.equals(Label.Trigger_User_Switch)){
        if(Trigger.isInsert)
        {            
            if(Trigger.isBefore)
            {
                AP67_userTriggerUtil.processUserBeforeInsert(Trigger.new) ;
                
            }
            if(Trigger.isAfter)
            {    
                Set<Id> UserId=new Set<id>();
                for(User u:Trigger.new){
                    UserId.add(u.Id);
                }
                
                //Mix DML Fix Start
                if(!AP04_ColleagueTriggerUtil.updatedFromColleague ){    
                    AP67_userTriggerUtil.updatecolleagueuser(UserId);
                }
                //Mix DML Fix End
                AP67_userTriggerUtil.processUserAfterInsert(Trigger.new) ;
                //AP67_userTriggerUtil.test142(Trigger.new) ;
            }
        }
        
        if(Trigger.isUpdate) 
        {
            if(Trigger.isBefore)
            {
                AP67_userTriggerUtil.processUserBeforeUpdate(Trigger.new,Trigger.old) ;
                AP67_userTriggerUtil.usertracker(Trigger.newMap,Trigger.oldMap);
                AP67_userTriggerUtil.processUserFedIDBeforeUpdate(Trigger.newMap,Trigger.oldMap);
                //Added for #18495
                AP67_userTriggerUtil.trackProfileUpdate(Trigger.new,Trigger.oldMap);
            }
            if(Trigger.isAfter)
            {
                
                List<User> OppUserLst1 = new List<User>();
                AP67_userTriggerUtil.processUserAfterUpdate(Trigger.newMap,Trigger.oldMap) ;
                for (User opptyUser : trigger.new)
                {
                    if((opptyUser.ProfileId!= trigger.oldMap.get(opptyUser.Id).ProfileId) ||(opptyUser.Level_1_Descr__c != trigger.oldMap.get(opptyUser.Id).Level_1_Descr__c)){
                        OppUserLst1.add(opptyUser);
                    }
                } 
                if(!OppUserLst1.isEmpty()){
                    AP007_OppValidations.updateFieldsAfterUpdateOpp(OppUserLst1);
                }
                // AP67_userTriggerUtil.processUserFedIDAfterUpdate(Trigger.newMap,Trigger.oldMap);
            }
        }  
    }
}