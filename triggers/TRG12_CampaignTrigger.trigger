/*
Purpose:  
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL    
1.0 -       Arijit    03/29/2013  Created Apex Trigger for Campaign (before insert,before update)
2.0			Reeika	  11/18/2013  Req#3457 ,Added event 'After Insert' for this trigger and Added Marketo statuses to campaigns at After Insert
============================================================================================================================================== 
*/

trigger TRG12_CampaignTrigger on Campaign (before insert, before update,after insert) 
    {
       if(Trigger.isInsert && Trigger.new.size() == 1 && Trigger.isBefore){
            AP22_CampaignTriggerUtil.CampaignBeforeInsert(Trigger.new);
       } 
       
       if(Trigger.isUpdate && Trigger.new.size() == 1 )
            AP22_CampaignTriggerUtil.CampaignAfterUpdate(Trigger.newMap,trigger.oldMap); 
            
       if(Trigger.isInsert && Trigger.isAfter)
       		AP22_CampaignTriggerUtil.campaignAfterInsert(Trigger.new);
    
    }