/*Purpose:  This trigger calculates the LOB margin for each LOB on Opportunity and highlights them with specific colors.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Sarbpreet   2/07/2014   As per March Release 2014(Request # 3729) Created Apex Trigger for Scope It Project (before insert, after insert, before update, after update, after delete)
   2.0 -    Jagan       2/25/2014   Removed all the individual try catch statements and kept only one
============================================================================================================================================== 
*/
trigger TRG24_ScopeItProjectTrigger on ScopeIt_Project__c(after delete, after insert, after update, before insert, before update) {
    try {
        if (trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
            AP83_ScopeItProjectUtil.scopeItProject(trigger.new);
        
        if (trigger.isBefore) {
                        
            Mercer_profitDetails_class mpc = new Mercer_profitDetails_class();
            mpc.setColor(trigger.new);
            for (ScopeIt_Project__c scp: trigger.new) 
                scp.currencyisocode = scp.opportunity_currency_code__c;
        }
           
        if (Trigger.isDelete) {
            AP83_ScopeItProjectUtil.scopeItProject(trigger.old);
        }
    } 
     catch (Exception ex) {
        System.debug('Exception occured at ScopeItProject Trigger with reason :' + ex.getMessage());
    }

}