/*
Purpose:  
This trigger adds/deletes Feed Comment in custom Chatter Feed Reporting Object whenever a post is created/deleted on Acct, Contact,Opty,User,Group.      
==============================================================================================================================================
History ----------------------- 
VERSION     AUTHOR  DATE        DETAIL    1.0 -    
Shashank    04/18/2013         Created Apex Trigger on FeedComment(after insert, before delete)
============================================================================================================================================== 
*/

trigger TRG17_FeedCommentTrigger on FeedComment (after insert, before delete) 
{
    if(Trigger.isInsert)
    {
        if(Trigger.isAfter)
        {
            AP51_FeedCommentReporting.insertFeedComment(trigger.new);
        }
    }
    
    if(Trigger.isDelete)
    {
        if(Trigger.isBefore)
        {
            AP51_FeedCommentReporting.deleteFeedComment(trigger.old);
        }
    }
}