/* Purpose: This trigger written to populate Opportunity Product LOB & Count of LOBs from all of the associated products on the opportunity.
============================================================================================================
History
-------------
VERSION     AUTHOR               DATE        DETAIL 
   1.0 -    Reetika, Sarbpreet   07/18/2012  Created Apex Trigger for OpportunityLineItem (After Insert,After Update, After Delete)
                                             to populate Opportunity Product LOB & Count of LOBs
   2.0      Jagan                03/14/2014  Scope it project related updates                                                
============================================================================================================================================== 
*/


trigger TRG19_OpportunityLineItemAfterTrigger on OpportunityLineItem (After Insert,After Update, After Delete) {
   // if(Boolean.valueOf(Label.TRG19_OpportunityLineItemAfterTriggerSwitch) || Test.isRunningTest()){
        Switch_Settings__c switchSetting = Switch_Settings__c.getInstance(UserInfo.getUserId());
        if(switchSetting.IsOpportunityProductTriggerActive__c  || Test.isRunningTest()){
            if(Trigger.isAfter && AP02_OpportunityTriggerUtil.preventFirstCloneCall!='STOP Cloning')
                {  
                     
                   system.debug('***@OLI outside if' +checkTriggerRecursive.lineItemRun );
                   if((Trigger.isInsert || Trigger.isUpdate) && checkTriggerRecursive.lineItemRun == false) 
                   {
                       AP60_OpportunityLineItemTriggerUtil.processOliAfterTrigger(Trigger.new, false);
                       AP60_OpportunityLineItemTriggerUtil.ScopeitProjectUpdates(trigger.new,trigger.oldmap);
                       checkTriggerRecursive.lineItemRun = true;
                       system.debug('***@OLI inside if' +checkTriggerRecursive.lineItemRun );
                       //For Chatter Post
                       //AP60_OpportunityLineItemTriggerUtil.chatterpost(trigger.New);
                   }
                   
                   if(Trigger.isDelete)
                   {
                       AP60_OpportunityLineItemTriggerUtil.processOliAfterTrigger(Trigger.old, true);
                       AP60_OpportunityLineItemTriggerUtil.ScopeitProjectUpdates(trigger.new,trigger.oldmap);
                   }
               
                 }
     }
 }