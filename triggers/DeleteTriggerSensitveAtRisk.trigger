/*Made Inactive as per the trigger refactoring*/
trigger DeleteTriggerSensitveAtRisk  on Sensitve_At_Risk__c (before delete) {
    delete [select Id,At_Risk_Type__c ,Why_at_Risk__c,LOB_Status__c,LOB__c,Sensitive_At_Risk__c from Client_Relationship__c where Sensitive_At_Risk__c IN: trigger.oldmap.keyset()];
}