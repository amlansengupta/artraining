/*This is the only trigger from which events and methods get fired on ContactTeam Object*/

trigger ContactTeamTrigger on Contact_Team_Member__c (before insert,after insert,before update,after update,before delete,after delete)
{
             TriggerDispatcher.run(new ContactTeamTriggerHandler());
}