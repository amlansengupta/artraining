trigger IndustryTrigger on Industry__c (after  insert, after update, after  delete) {
if(ConstantsUtility.STR_ACTIVE.equals(Label.Industry_Trigger_Switch)){ 
     if((Trigger.isInsert) && Trigger.isAfter)
     {
         IndustryTriggerUtil.updateAccountSICInfo(Trigger.newmap);
     }
    
     if(Trigger.isUpdate && Trigger.isAfter)
     {
         IndustryTriggerUtil.updateAccountSICInfo(Trigger.oldmap);
     }
         
     if((Trigger.isDelete) && Trigger.isAfter)
     {
         IndustryTriggerUtil.deleteAccountSICInfo(Trigger.oldmap);
     }    
}
}