/*
Purpose:  To Restrict User to Un-Join from Chatter Group - All MercerForce Users.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL   
1.0 -       Gyan      10/24/2013  Created Apex Trigger
============================================================================================================================================== 
*/

trigger TRG21_RestrictUsersUnjoiningfromGroups on CollaborationGroupMember (after delete){
    collaborationgroup cg = [select id from collaborationgroup where name = 'All MercerForce Users' limit 1];
    for(CollaborationGroupMember cgm: trigger.old){
         if(cgm.CollaborationRole == 'Standard' && cgm.memberid==userinfo.getuserid() && cgm.CollaborationGroupId == cg.id){
             cgm.addError('You cannot unjoin from this group.');
         }    
    }
}