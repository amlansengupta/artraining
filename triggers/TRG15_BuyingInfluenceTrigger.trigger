/*
Purpose:  
This trigger adds Contact to Contact Role related list when a buying Influence is added/removed from an opportunity.   
==============================================================================================================================================
History 
----------------------- VERSION     AUTHOR  DATE        DETAIL    1.0 -    Shashank 04/12/2013  Created Apex Trigger for Buying Influence(before delete, after insert)
============================================================================================================================================== 
*/

trigger TRG15_BuyingInfluenceTrigger on Buying_Influence__c (before delete, after insert, before insert,before update) 
{
    checkTriggerRecursive1.isitFromBITrigger = true;
    System.debug('KCBI******Inside BI Trigger');
    if(Trigger.isAfter)
    {   
        System.debug('KCBI******Inside BI Trigger isAfter');
        if(Trigger.isInsert && checkTriggerRecursive1.isStartKCBI == false)
        {
            checkTriggerRecursive1.isStartKCBI = true;
            System.debug('KCBI******Inside BI Trigger isAfter isInsert');
            AP40_BuyingInfluenceTriggerUtil.insertContactRoleOnInsert(trigger.newMap);
            AP40_BuyingInfluenceTriggerUtil.updateContactfromOppBuyer(trigger.new);
         System.debug('KCBI*************-33'+trigger.new);
           
        }
    }
    
    if(Trigger.isBefore && checkTriggerRecursive1.isStartKCBI1 == false)
    {
        checkTriggerRecursive1.isStartKCBI1 = true;
       // checkTriggerRecursive1.isStartKCBI = true;
        System.debug('KCBI******Inside BI Trigger isBefore');
        if(Trigger.isDelete)
        {
            System.debug('KCBI******Inside BI Trigger isBefore isDelete');
            AP40_BuyingInfluenceTriggerUtil.deleteContactRoleOnDelete(trigger.oldMap);
        }
        if(Trigger.isUpdate)
        {
            Set<Id> toUpdate = new Set<Id>();
            for(Buying_Influence__c buy : trigger.oldMap.values())
            {
                toUpdate.add(buy.id);
            }
            if(checkTriggerRecursive1.isStartKCBI2==false || Test.isRunningTest()){
                checkTriggerRecursive1.isStartKCBI2= true;
            AP40_BuyingInfluenceTriggerUtil.futureUpdate(toUpdate);
            }
            //AP40_BuyingInfluenceTriggerUtil.updateContactfromOppBuyer(trigger.new,trigger.oldMap);
        }
        //checkTriggerRecursive1.isStartKCBI1 = true;
    }
}