trigger TRG11_CompetitorTrigger_deprecated on Competitor__c (before delete) {

    if(Trigger.isDelete)
    {
        if(Trigger.isBefore)
        {
            AP20_CompetitorTriggerUtil.processCompetitorBeforeDelete(Trigger.old);
        }
    }    
    
}