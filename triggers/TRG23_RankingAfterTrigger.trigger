/* Purpose: This trigger is written to populate concatenatedRanking Field On Account Which contains all Ranking Description value seperated by ;
============================================================================================================
History
-------------
VERSION     AUTHOR               DATE        DETAIL 
   1.0 -   Gyan                  12/16/2013  Created Apex Trigger Under Req-3401 for Ranking__c (After Insert,After Update, After Delete)
                                             to populate concatenatedRanking Field On Account 
============================================================================================================================================== 
*/
trigger TRG23_RankingAfterTrigger on Ranking__c (After Insert,After Update, After Delete) 
{
       if(Trigger.isInsert || Trigger.isUpdate) 
       {
          AP81_RankingTriggerUtil.concatenatedRanking(trigger.new);
        
       }
       
       if(Trigger.isDelete)
       {
          AP81_RankingTriggerUtil.concatenatedRanking(Trigger.old);
       }
 }