/*Purpose:  TO-DO
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
1.0 -    Joban   12/14/2012  Created Apex Trigger for Opportunity (before insert, after insert, before update, after update)
2.0 -    Sujata  12/18/2012  Called methods from AP02_OpportunityTriggerUtil class for various events. 
3.0 -    Sarbpreet 3/10/2014 Added Opportunity Trigger switch
4.0 -    Jyotsna  04/08/2015  PMO#5568(June release) : Create a new method processClosedWonOpportunityBeforeUpdate method to put validations when Opportunity is closed Won.
5.0 -    Venkat   05/27/2015  PMO#6244(July 2015 Release) 
6.0 -    Bala     08/20/2015  PMO#5568(September 2015)
============================================================================================================================================== 
This is the only trigger from which events and methods get fired on Opportunity Object
*/

trigger TRG02_OpportunityTrigger on Opportunity (before insert, after insert, before update,after update,after Delete)
{   
    //if(ConstantsUtility.STR_ACTIVE.equals(Label.OpportunityTriggerSwitch)){
     Switch_Settings__c switchSetting = Switch_Settings__c.getInstance(UserInfo.getUserId());
    if(switchSetting.IsOpportunityTriggerActive__c && !AP44_ChatterFeedReporting.FROMFEED){
    TriggerDispatcher.Run(new OppHandlerClass());             
//}
    }
}