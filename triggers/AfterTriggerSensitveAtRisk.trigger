/*Made Inactive as per the trigger refactoring*/
trigger AfterTriggerSensitveAtRisk on Sensitve_At_Risk__c (after insert, after update) {
    database.executeBatch(new BATCH_OneDirectionalSensitiveDataSync(trigger.newmap.keyset()),2000);
}