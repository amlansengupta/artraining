trigger TRG18_OpportunityLineItemTrigger on OpportunityLineItem (before insert, before update) 
{
    
    // if(Label.TRG18_OpportunityLineItemTriggerStatus == 'Active'){
    Switch_Settings__c switchSetting = Switch_Settings__c.getInstance(UserInfo.getUserId());
    if(switchSetting.IsOpportunityProductTriggerActive__c  || Test.isRunningTest()){
        
        /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
        if(Trigger.isBefore)
        {
            
            system.debug('@@@@@ after executing updateProjectRequired');
            if(Trigger.isInsert){
                AP60_OpportunityLineItemTriggerUtil.processOpportunityProductonInsert(trigger.new);
                OppLineItemTriggerUtil_ProjectEx.updateProjectRequired(trigger.new);
            }
            if(Trigger.isUpdate){            
                AP60_OpportunityLineItemTriggerUtil.processOpportunityProductonUpdate(trigger.newMap, trigger.oldMap);
                OppLineItemTriggerUtil_ProjectEx.updateProjectRequired(trigger.new);
            }
            boolean isExcludedProfile = ExcludeUserUtility.isExcludedUser(UserInfo.getUserName());
            if(!isExcludedProfile){
                for(OpportunityLineItem oppLineItem:trigger.new)
                {
                    oppLineItem.User_Last_Modified_Date__c = System.now();
                    oppLineItem.User_Last_Modified_By__c = UserInfo.getUserId(); 
                }
                
            }
        }
    }
}