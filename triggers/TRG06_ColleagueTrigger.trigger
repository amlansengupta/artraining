/*
Purpose: 
This trigger is used for for sync Colleague and User
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL    
1.0 -       Arijit   12/19/2012  Created Apex Trigger for Colleague(before insert, after insert, before update, after update)
============================================================================================================================================== 
*/

trigger TRG06_ColleagueTrigger on Colleague__c (before insert, after insert, before update, after update) 
{
   if(ConstantsUtility.STR_ACTIVE.equals(Label.Colleague_Trigger_Switch)){ 
    if(Trigger.isBefore)
    {
        for(Colleague__c colleague : Trigger.new)
        {
            if(colleague.Prf_First_Name__c <> null && colleague.Prf_Last_Name__c == null && colleague.Last_Name__c <> null)
            {
                colleague.Name = colleague.Prf_First_Name__c + ' ' + colleague.Last_Name__c;
            }
            else if(colleague.Prf_First_Name__c == null && colleague.Prf_Last_Name__c <> null && colleague.First_Name__c <> null)
            {
                colleague.Name = colleague.First_Name__c + ' ' + colleague.Prf_Last_Name__c;
            }
            else if(colleague.Prf_First_Name__c <> null && colleague.Prf_Last_Name__c <> null)
            {
                colleague.Name = colleague.Prf_First_Name__c + ' ' + colleague.Prf_Last_Name__c;
            }
            else if(colleague.Prf_First_Name__c == null && colleague.Prf_Last_Name__c == null && colleague.First_Name__c <> null && colleague.Last_Name__c <> null)
            {
                colleague.Name = colleague.First_Name__c + ' ' + colleague.Last_Name__c;
            }
        }
    }
    
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
           
                 AP04_ColleagueTriggerUtil.processColleagueAfterInsert(trigger.new);
        }
        
        
        if(Trigger.isUpdate)
        {
           
            //Mix DML Fix Start
            if(!AP67_userTriggerUtil.updatedFromUser){
                 AP04_ColleagueTriggerUtil.processColleagueAfterUpdate(trigger.new,trigger.oldmap);
            }
            //Mix DML Fix Start
                 
                 List<Colleague__c> OppCollList = new List<Colleague__c>();
                 
                 for(Colleague__c col : trigger.new)
                 {
                     if(col.Location_Descr__c != trigger.oldmap.get(col.id).Location_Descr__c)
                     {
                         OppCollList.add(col);
                     }
                 }
                 
                 if(!OppCollList.IsEmpty())
                 {
                     AP007_OppValidations.updateFieldsAfterColleagueUpdate(OppCollList);
                 }
            
        }
    }
    
   }
}