/*
Purpose: 
This trigger provides Read/Write privilidges to Contact Team Members. 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL    1.0 -    
Sujata      12/14/2012          Created Apex Trigger for Contact Team Member (before insert,before delete)
==============================================================================================================================================
Made Inactive
*/

trigger TRG05_ContactTeamTrigger on Contact_Team_Member__c (before insert,before delete) 
{        
    try
    {
        if(Trigger.isInsert)
        {   
            AP56_ContactTeamTriggerUtil.processContactTeambeforeInsert(trigger.new);
        }
    }
    catch(TriggerException tEx)
    {
        System.debug('Exception occured at ContactTeamMember Trigger BeforeInsert with reason :'+tEx.getMessage());
    }
    catch(Exception ex)
    {
        System.debug('Exception occured at ContactTeamMember Trigger BeforeInsert with reason :'+ex.getMessage());
    }
    

    try
    {
        if(Trigger.isDelete)
        {
            if(!AP19_ContactTriggerUtil.isMergeProcess)
            {
                AP56_ContactTeamTriggerUtil.processContactTeambeforeDelete(trigger.old);   
            }
            
        
        }
    }
    catch(TriggerException tEx)
    {
        System.debug('Exception occured at ContactTeamMember Trigger BeforeDelete with reason :'+tEx.getMessage());
    }
    catch(Exception ex)
    {
        System.debug('Exception occured at ContactTeamMember Trigger BeforeDelete with reason :'+ex.getMessage());
    }
}