/*Purpose:  
This trigger adds Contact Owner in ContactTeamMember when Contact record is created or Contact Owner is updated.
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL    
1.0 -       Sujata 12/14/2012  Created Apex Trigger for Contact (before insert, after insert,before update,after update,after delete)
2.0 -       Sarbpreet 3/10/2014 Added Contact Trigger Switch
============================================================================================================================================== 
*/
trigger TRG04_ContactTrigger on Contact (before insert, after insert,before update,after update,after delete) {
    
    //private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember();
    if(ConstantsUtility.STR_ACTIVE.equals(Label.ContactTriggerSwitch)){
        if(Trigger.isInsert)
        { 
            /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
            if(Trigger.isBefore){
                AP19_ContactTriggerUtil.processContactBeforeInsert(Trigger.new);
                boolean isExcludedProfile = ExcludeUserUtility.isExcludedUser(UserInfo.getUserName());
                if(!isExcludedProfile){
                    for(Contact cont:Trigger.new)
                    {
                        cont.User_Last_Modified_Date__c = System.now();
                        cont.User_Last_Modified_By__c = UserInfo.getUserId(); 
                    }
                }
            }
            
            if(Trigger.isAfter) {
                
                AP19_ContactTriggerUtil.processContactAfterInsert(Trigger.newMap);              
            }
        }
        /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
        if(Trigger.isUpdate)
        { 
            if(Trigger.isBefore) {
                AP19_ContactTriggerUtil.processContactBeforeUpdate(Trigger.newMap, Trigger.OldMap);
                AP19_ContactTriggerUtil.processContactBeforeInsert(Trigger.new);
                boolean isExcludedProfile = ExcludeUserUtility.isExcludedUser(UserInfo.getUserName());
                if(!isExcludedProfile){
                    for(Contact cont:Trigger.new)
                    {
                        cont.User_Last_Modified_Date__c = System.now();
                        cont.User_Last_Modified_By__c = UserInfo.getUserId(); 
                    }
                }
            }
            
            if(Trigger.isAfter) {
                
                AP19_ContactTriggerUtil.processContactAfterUpdate(Trigger.new, Trigger.newMap , Trigger.oldMap);            
            }
        }      
        
        if(Trigger.isDelete)
        {
            if(Trigger.isAfter)  {
                AP19_ContactTriggerUtil.processContactAfterDelete(Trigger.old);  
            }
        }
        
    }
}