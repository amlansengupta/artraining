/*Purpose:  To notify Users on Create or update of Task 
    Related Apex Class(es) : Task_Send_Notification_email
    Part of Release: November 2013   
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR      DATE        DETAIL 
   1.0 -    Jagan                   Part of Release: November 2013 
   2.0      Sarbpreet   18-Dec-2013 Req#3472/Jan Release : Auto-assignment of Task when Lead score meets the threshold.Added call to method 'task_assign'
   3.0 -    Jagan       11 Jan 2014 Added custom label to inactivate anytime
   4.0      Madhavi     11/11/2014  As per Request #5079(December Release),added call to method 'relatedActivityLastModifiedbyUser'
   5.0      Jagan       12/2/2014   Added code for Req:5590 to check if the assigned owner is inactive.
============================================================================================================================================== 
*/


trigger Task_NotifyUser on Task (before insert, after insert, before update, after update,before delete) {
    If(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
    {
        //Task_Send_Notification_email.send_email(trigger.new,trigger.oldmap);
            
        Task_Send_Notification_email.relatedActivityLastModifiedbyUser(trigger.new);//Added as part #5079(Dec 2014 release)
        if(Label.TaskTriggerSwitch =='True' && Trigger.isInsert)
        {
            // Req#3472/Jan Release
            Task_Send_Notification_email.task_assign(trigger.newmap);
        }
 

                // MQL Changes May 2018
    if(Trigger.isUpdate && Task_Send_Notification_email.FLAG_CHECK_TASK == false){
        Task_Send_Notification_email.task_validateLead(trigger.new);        
        Task_Send_Notification_email.create_Opportunity(trigger.newmap, trigger.oldmap);
    }

    }
    //Added for 5590
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
    Task_Send_Notification_email.task_validateLead(trigger.new);        
        if(Trigger.isInsert || Trigger.isUpdate){
            Task_Send_Notification_email.populateOpportunityCountry(trigger.new);
        }    
    if(Trigger.isInsert)
    {
        if(trigger.isBefore){
            Task_Send_Notification_email.task_validateLead(trigger.new);
        }
        // MQL Changes May 2018
        for(Task t:trigger.new){
        if(t.Subject !=null && t.Subject.contains('Request Consultation') ){
            t.Temp_Lead_Source__c = 'Request Consultation';
            t.LeadSource__c = 'Request Consultation';
        }
        if(t.Subject !=null && t.Subject.contains('Organic') ){
            t.Temp_Lead_Source__c = 'Organic';
            t.LeadSource__c = 'Organic';            
        }
        if(t.Subject !=null && t.Subject.contains('Telequalified') ){           
            //Changes for Defect #19183 
            //t.Temp_Lead_Source__c = 'Telequalified';            
            //t.LeadSource__c = 'Telequalified';  
            t.Temp_Lead_Source__c = 'Telequal';          
            t.LeadSource__c = 'Telequal';            
        }
        if(t.Subject !=null && t.Subject.contains('Third Party') ){
            t.Temp_Lead_Source__c = 'Third Party';
            t.LeadSource__c = 'Third Party';            
        }  
        if(t.Subject !=null && t.Subject.contains('Event') ){
            t.Temp_Lead_Source__c = 'Event';
            t.LeadSource__c = 'Event';            
        }            
        }
        ///
        Task_Send_Notification_email.checkOwnerBeforeInsert(trigger.new);
       }                                          
    }
      if(Trigger.isBefore && Trigger.isDelete)
      {
         Task_Send_Notification_email.checkOwnerProfileBeforeDelete(trigger.old);
      }   
    
}