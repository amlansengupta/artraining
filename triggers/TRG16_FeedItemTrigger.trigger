/*
Purpose:  
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL   
1.0 -       Shashank  12/14/2012  Created Apex Trigger for FeedItem(after insert, before delete)
1.1 -        Gyan     10/25/2013    Modified Apex Trigger As per req-3407
1.2         Madhavi   11/11/2014   As per Request#5079(Dec 2014 release)-added a call to method checkChatterPostsOnOpportunity.
============================================================================================================================================== 
*/

trigger TRG16_FeedItemTrigger on FeedItem (after insert, before delete) 
{
    if(Trigger.isInsert)
    {
        AP44_ChatterFeedReporting.FROMFEED = true;
        if(Trigger.isAfter)
        {
            AP44_ChatterFeedReporting.insertChatterFeed(trigger.new);
            AP44_ChatterFeedReporting.checkChatterPostsOnOpportunity(trigger.new);//Added as part #5079(Dec 2014 release)
            String grpName = Label.All_MercerForce_Users;
            //Fetch the ID of "All MercerForce Users" Group.
            List<CollaborationGroup> cg = new List<CollaborationGroup>();
            cg =  [Select Name,Id From CollaborationGroup where Name = :grpName limit 1];
            //Below code will be executed only For All MercerForceUsers Group 
            if(!cg.isEmpty() && cg.size()>0) {
                for(FeedItem feed:trigger.new)
                {
                    if(feed.ParentId==cg[0].Id || (feed.body <> null && feed.body.contains('@All MercerForce Users')) )
                    {
                        AP44_ChatterFeedReporting.restrictChatterFeedForAllMercerGroup(trigger.new);
                    }                
                }
            }
        }
    }
    
    if(Trigger.isDelete)
    {
        AP44_ChatterFeedReporting.FROMFEED = true;
        if(Trigger.isBefore)
        {
            AP44_ChatterFeedReporting.deleteChatterFeed(trigger.old);
        }
    }
}