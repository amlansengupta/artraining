/* This is a trigger for the Global Contract Account object
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR     DATE        DETAIL 
   1.0 -    Venkat   05/27/2015  Req #4944(July 2015 Release) 
============================================================================================================================================== 
*/

trigger GlobalContractAfter on Global_Contract_Account__c (After Delete, After Insert, After Update) {
    if(Trigger.isAfter && Trigger.isDelete){
       Set<ID> acidset = new Set<ID>();
       for(Global_Contract_Account__c gca:trigger.Old){
               acidset.add(gca.Global_Account_Name__c);
        }
        List<Account> acList = [Select Id,My_Global_Contract__c,My_Global_Rate_Card__c,My_Global_Special_Terms__c from Account where ID IN :acidset ];
        List<Account> inAccLst = new List<Account>() ;
        for(Account ac:acList) {
            ac.My_Global_Contract__c = null ; 
            ac.My_Global_Rate_Card__c = null ;
            ac.My_Global_Special_Terms__c = null ;
            inAccLst.add(ac);
        }
        if(inAccLst.size()>0) {
            Database.update(inAccLst ) ; 
        }
    } 
    

}