/*Purpose:  This trigger adds Account Owner in AccountTeamMember when Account record is created or Account Owner is updated.
            The Relationship Manager is assigned as the Account Owner if the Relationship Manager exists in User table and is an Active User. 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   12/14/2012  Created Apex Trigger for Account (before insert, after insert, before update, after update)
============================================================================================================================================== 
This is the only trigger from which events and methods get fired on Account Object
*/


trigger TRG01_AccountTrigger on Account (before insert, after insert,before update,after update)
{
   // if(ConstantsUtility.STR_ACTIVE.equals(Label.AccountTriggerSwitch)){
   Switch_Settings__c switchSetting = Switch_Settings__c.getInstance();
     /*ConstantsUtility.STR_ACTIVE.equals(System.Label.AccountTriggerSwitch) part is to skip trigger and remove 101 SOQL ERROR*/
    system.debug('Trigger Active'+switchSetting.IsAccountTriggerActive__c);
    if(ConstantsUtility.STR_ACTIVE.equals(System.Label.AccountTriggerSwitch) && switchSetting.IsAccountTriggerActive__c){
    //if(ConstantsUtility.STR_ACTIVE.equals(System.Label.AccountTriggerSwitch)){
    TriggerDispatcher.run(new AccountTriggerHandler());  
}
}