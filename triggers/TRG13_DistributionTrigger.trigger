/*
Purpose: 

==============================================================================================================================================
History
----------------------- 
VERSION     AUTHOR    DATE        DETAIL    
1.0 -       Shashank  03/30/2013  Created Apex Trigger for Distribution(before insert, before update)
============================================================================================================================================== 
*/

trigger TRG13_DistributionTrigger on Distribution__c (before insert,before update) 
{
  if(Trigger.isInsert && Trigger.new.size() == 1)
  {
        AP27_DistributionClassUtil.distributionBeforeInsert(Trigger.new);
  }
  
   if(Trigger.isUpdate && Trigger.new.size() == 1)
   {
        AP27_DistributionClassUtil.distributionBeforeUpdate(Trigger.newMap,trigger.oldMap); 
   }
}