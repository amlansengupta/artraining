/*This is the only trigger from which events and methods get fired on SensitveAtRisk Object*/
trigger TriggerSensitveAtRisk on Sensitve_At_Risk__c (before insert,after insert,before update,after update,before delete,after delete)
{    
       TriggerDispatcher.Run(new SensitveAtRiskTriggerhandler()); 
}