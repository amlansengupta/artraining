/*Purpose:  Trigger to query Colleague id and Proudct id based on information from WebCAS and associate them to look up fields in Project object
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE            DETAIL 
   1.0 -    Jagan   1st April 2015  Created Apex Trigger
   2.0 -    Bala    20th August 2015 Modified function name
============================================================================================================================================== 
Made Inactive as per the trigger refactoring
*/

trigger updateColleagueandProduct on Revenue_Connectivity__c (Before insert, Before update, After insert, After Update) {
    if(Boolean.valueOf(Label.Project_Trigger_Switch) || Test.isRunningTest()){
        if(trigger.IsBefore){
            //AP125_ProjectTriggerClass.updateColleagueandProudct(trigger.new, trigger.newMap);
        }
        //if(trigger.IsAfter){}
           // AP125_ProjectTriggerClass.closeCheckonProductProject(trigger.new, trigger.newMap);
    }
}