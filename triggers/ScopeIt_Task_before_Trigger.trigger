/*
Trigger to update Currency ISO Code
*/
trigger ScopeIt_Task_before_Trigger on ScopeIt_Task__c (before insert, before update) {
    
    for(ScopeIt_Task__c objScop:trigger.new){
        objScop.CurrencyIsoCode = objScop.Opportunity_currency_code__c;
    }
    
  
    
}