/*
Purpose:  This trigger checks Validation for Indv Sales Goal
and Creates a Sales Credit Line Item corresponding to Individual Sales Goal whenever an Indv Sales Goal record is created/updated
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL   
 1.0 -      Arijit 2/05/2013  Created Apex Trigger for Individual Sales Goal(before insert, after insert, before update, after update)
 2.0-       Apoorva/Jigyasu 7/04/2016 Created Apex Trigger for Individual Sales Goal(after delete)//for request 10298
 3.0 -      Jigyasu 7/04/2016 Modified Apex Trigger for Individual Sales Goal(after delete)//**req 10298**
============================================================================================================================================== 
*/

trigger TRG10_IndividualSalesGoalTrigger on Individual_Sales_Goal__c (before insert, after insert, before update, after update,after delete) 
{
   if(ConstantsUtility.STR_ACTIVE.equals(Label.SalesCreditTriggerSwitch)){
    if(Trigger.IsBefore)
    {
        if(Trigger.IsInsert)
        {
            AP18_IndividualSalesGoalTriggerUtil.salesGoalValidationInsert(trigger.New);
        }
    
        if(Trigger.IsUpdate)
        {
            AP18_IndividualSalesGoalTriggerUtil.salesGoalValidationUpdate(trigger.NewMap, trigger.OldMap);
        }
    }
    
    if(Trigger.IsAfter)
    {
        if(Trigger.IsInsert)
        {
            AP18_IndividualSalesGoalTriggerUtil.salesGoalAterInsert(trigger.New);
        }
    
        if(Trigger.IsUpdate)
        {
            AP18_IndividualSalesGoalTriggerUtil.salesGoalAterUpdate(trigger.NewMap, trigger.OldMap);
        }
        //Code written to update Goals field in User as per request 10298
        if(Trigger.IsDelete)
        {
          AP18_IndividualSalesGoalTriggerUtil.deleteIndividualGoal(trigger.Old); 
            
        }
    }
  }
}