trigger ClientProductContactTrigger on Client_Product_Contact__c (after insert,after update,after delete) {
 if(ConstantsUtility.STR_ACTIVE.equals(Label.Client_Product_Contact_Trigger_Switch)){ 
  if( Trigger.isInsert || Trigger.isUpdate ){
  ClientProductContactTriggerUtil.clientProductContact(trigger.new);
  }  
  if(Trigger.isDelete){
  ClientProductContactTriggerUtil.cpcAfterDelete(trigger.old);
  }
 }
}