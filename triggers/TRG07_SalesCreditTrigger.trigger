/*Purpose:  This Trigger applies a validation on the Percent of Allocation of Sales Credit each time when a Sales Credit record is Created or Updated.
            The percent Allocation cannot exceed 100% if the given criteria is met. 
             
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Shashank   12/28/2012  Created Apex Trigger for Sales_Credit__c (after insert, after update)
   2.0      Sujata     02/11/2013  Added code logic to check for duplicacy in Sales Credit team for a particular Opportunity on before insert event
============================================================================================================================================== 
*/

trigger TRG07_SalesCreditTrigger on Sales_Credit__c (before insert,after insert, before update, after update, before delete,after delete) 
{
     private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember();
    if(ConstantsUtility.STR_ACTIVE.equals(Label.SalesCreditTriggerSwitch)){
    
     if(Trigger.isInsert)  
     {
         if(Trigger.isBefore)
         {
             
            
            //Set to contain ID of Opportunity corresponding to Sales Credit record.
            Set<ID> OppIDList = new Set<ID>();
            
            //Map to contain Opportunity ID and its corresponding Sales Credit Team Members List as key,value.
            Map<ID,List<Sales_Credit__c>> SCreditTeamMemberMap = new Map<ID,List<Sales_Credit__c>>();

            for(Sales_Credit__c sc : Trigger.new)
            {
                OppIDList.add(sc.Opportunity__c);
            }
          
            if(!OppIDList.isEmpty())
            {
                for(Opportunity opp : [Select Id, Name, (Select Id, Opportunity__c,EmpName__c FROM Sales_Creditings__r ) FROM Opportunity WHERE Id IN : OppIDList])
                {
                    SCreditTeamMemberMap.put(opp.Id,opp.Sales_Creditings__r);  
                }
            }
           
            for(Sales_Credit__c salesCredit : Trigger.new)
            {
                if(SCreditTeamMemberMap.containskey(salesCredit.Opportunity__c)){
                for(Sales_Credit__c teamMember : SCreditTeamMemberMap.get(salesCredit.Opportunity__c))
                {  
                    if(salesCredit.EmpName__c == teamMember.EmpName__c )
                    {      
                        salesCredit.addError('Sales Credit team members can not appear more than once. Please remove duplicate user(s).');
                    }
                } 
                }
            }
                
         }
         
         
         if(Trigger.isAfter)
         {    
             AP10_SalesCreditTriggerUtil.salesCreditAllocaton(trigger.new); 
               
            if(!isDMUser)AP10_SalesCreditTriggerUtil.validateSalesCreditAfterInsert(Trigger.newMap);
            AP10_SalesCreditTriggerUtil.insertSalesCreditLineItem(trigger.newMap); 
                        
         }
     }
        
     
     if(Trigger.isUpdate)
     {
            if(Trigger.isBefore)
            {    
                //Set to contain ID of Opportunity corresponding to Sales Credit record.
                Set<ID> OppIDList = new Set<ID>();
                
                //Map to contain Opportunity ID and its corresponding Sales Credit Team Members List as key,value.
                Map<ID,List<Sales_Credit__c>> SCreditTeamMemberMap = new Map<ID,List<Sales_Credit__c>>();
            
                for(Sales_Credit__c sc : Trigger.newMap.values())
                {
                    if(sc.EmpName__c <> trigger.oldMap.get(sc.Id).EmpName__c)
                    OppIDList.add(sc.Opportunity__c);
                }
              
                if(!OppIDList.isEmpty())
                {
                    for(Opportunity opp : [Select Id, Name, (Select Id, Opportunity__c,EmpName__c FROM Sales_Creditings__r ) FROM Opportunity WHERE Id IN : OppIDList])
                    {
                        SCreditTeamMemberMap.put(opp.Id,opp.Sales_Creditings__r);  
                    }
                }
               
                for(Sales_Credit__c salesCredit : Trigger.newMap.values())
                {
                    if(salesCredit.EmpName__c <> trigger.oldMap.get(salesCredit.Id).EmpName__c)
                    {
                        for(Sales_Credit__c teamMember : SCreditTeamMemberMap.get(salesCredit.Opportunity__c))
                        {  
                            if(salesCredit.EmpName__c == teamMember.EmpName__c )
                            {      
                                salesCredit.addError('Sales Credit team members can not appear more than once. Please remove duplicate user(s).');
                            }
                        }
                    }               
                }
                   
            }
           
           if(Trigger.isAfter)
           {
               System.debug('In Update');
                AP10_SalesCreditTriggerUtil.validateSalesCreditAfterUpdate(Trigger.newMap, Trigger.oldMap);
                AP10_SalesCreditTriggerUtil.updateSalesCreditLineItem(trigger.newmap, trigger.oldmap);     
                List<Sales_Credit__c> scAllocList=new List<Sales_Credit__c>();
                for(Sales_Credit__c sc : Trigger.new){
                    if(sc.SP_CM_Allocation__c!=null){
                        if(Trigger.oldMap.get(sc.Id).SP_CM_Allocation__c!=sc.SP_CM_Allocation__c){
                            scAllocList.add(sc);
                        }
                    }                    
                }
                
                if(scAllocList.size()>0){
                    AP10_SalesCreditTriggerUtil.salesCreditAllocaton(scAllocList); 
                }          
           }
     }
     
     if(Trigger.isDelete)
     {
            if(Trigger.isBefore)
            {
                    //Code to allow only Opportunity Owner and System Administrator to delete members from Sales Credit Team on that Opportunity.
                    
                    //Map to contain Opportunity ID and Sales Credit ID as key-value.
                    Map<ID,ID> oppIDscIDMap = new Map<ID,ID>();
                    Map<ID,ID> oppMap = new Map<ID,ID>();
            
                    //Map to contain Sales Credit ID and Opportunity Owner ID as key-value.
                    Map<ID,ID> scIDOppOwnerIDMap = new  Map<ID,ID>();
                    
                    Profile prof = [select Name from Profile where Id = : UserInfo.getProfileId()];
                          
                    for(Sales_Credit__c sCredit : Trigger.old)
                    {
                        //oppIDscIDMap.put(sCredit.Opportunity__c,sCredit.Id);
                        oppIDscIDMap.put(sCredit.Id,sCredit.Opportunity__c);
                    }           
                    
                    for(Opportunity opp : [Select Id,Name,OwnerId FROM Opportunity WHERE Id IN : oppIDscIDMap.values()])
                    {
                        OppMap.put(opp.Id,OPP.ownerId);
                        //scIDOppOwnerIDMap.put(oppIDscIDMap.get(opp.Id),opp.OwnerId);
                    }
                    If(!oppMap.isEmpty() && !oppIdScIdMap.isEmpty()){
                     For(id SCId: oppidscidmap.keyset()){
                      If(oppmap.containskey(oppidscidmap.get(SCId))){
                      Scidoppowneridmap.put(SCId,oppmap.get(oppidscidmap.get(SCId)));
                      }
                      }
                    }
                    
                    for(Sales_Credit__c sCredit : trigger.old)
                    {
                        if(!'System Administrator'.equalsIgnoreCase(prof.Name))
                        {
                            if(scIDOppOwnerIDMap.get(sCredit.Id) <> Userinfo.getuserID())
                            {
                                sCredit.addError('Only Opportunity Owner can delete this member');
                            }
                        }
                    }    
                   
                   
                   
                   // AP10_SalesCreditTriggerUtil.validateSalesCreditOnDelete(trigger.OldMap);
                    AP10_SalesCreditTriggerUtil.deleteSalesCreditLineItem(trigger.old);
                     
           }      
     }
     // Requirement#2590 : Call method to update Concatenated Opportunity Team ,data pulled from Sales Credit - "Employee name <space> Percentage allocation
     if(Trigger.isAfter) {
        if(Trigger.isDelete)
            AP15_SalesCreditTeamTriggerUtil.processSalesCredit(trigger.old) ;
        else AP15_SalesCreditTeamTriggerUtil.processSalesCredit(trigger.new) ;
     }
    }
}