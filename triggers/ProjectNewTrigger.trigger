/*This is the only trigger from which events and methods get fired on Project Object*/
trigger ProjectNewTrigger on Revenue_Connectivity__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)  {  
    TriggerDispatcher.Run(new ProjectTriggerHandler());
}