/* Purpose: This trigger written to populate ConcatenatedContactTeam Field On Contact Which contains all contact Team Memer value seperated by ;
============================================================================================================
History
-------------
VERSION     AUTHOR               DATE        DETAIL 
   1.0 -   Gyan                  12/16/2013  Created Apex Trigger Under Req-3584 for Contact_Team_Member__c (After Insert,After Update, After Delete)
                                             to populate ConcatenatedContactTeam Field On Contact .
============================================================================================================================================== 
Made Inactive as per the trigger refactoring
*/
trigger TRG22_ContactTeamAfterTrigger on Contact_Team_Member__c (After Insert,After Update, After Delete) 
{
       if(Trigger.isInsert || Trigger.isUpdate) 
       {
          AP56_ContactTeamTriggerUtil.concatenatedContactTeam(trigger.new);
        
       }
       
       if(Trigger.isDelete)
       {
          AP56_ContactTeamTriggerUtil.concatenatedContactTeam(Trigger.old);
       }
 }