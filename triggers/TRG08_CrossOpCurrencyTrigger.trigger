/*Purpose:  This trigger contains the logic to populate the Amount USD field converted to USD on CrossOpCo Referral Object 
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR  DATE        DETAIL 
   1.0 -    Joban   1/7/2012    Created the Trigger
============================================================================================================================================== 
*/

trigger TRG08_CrossOpCurrencyTrigger on Cross_OpCo_Referral__c (before insert,after update) {
   


    if(trigger.isInsert)
    {
      if(trigger.isbefore)
      System.debug(' Before insert called--------------------------------------');
        	AP16_CurrencyConversionUtil.convertCurrencyBeforeInsert(trigger.new);
    }
          
   
    if(trigger.isupdate)
    {
    	if(trigger.isafter)
		{
			if(AP17_ExecutionControl.firstRun)
			{
					AP17_ExecutionControl.firstRun = false;
	    	    	System.debug(' After Update called--------------------------------------');
	    	    	System.debug(' AP17_ExecutionControl.firstRun--------------------------------------'+AP17_ExecutionControl.firstRun);
			   		AP16_CurrencyConversionUtil.convertCurrencyAfterUpdate(trigger.newmap, trigger.oldmap);
			   		
			}
			
		}
    		
    }



}