/*Created By: Harsh Vats, Date: 12/14/2018 
Trigger on Career Contract object to facilitate the working of Request# 17079
*/

trigger CareerContractTrigger on Talent_Contract__c (before insert,before delete,before update) {
    
    /*Request# 17079: 12/12/2018: Modified the existing method by fetching data from custom metadata type and ensure the working of two new fields UserLastModifiedBy and UserLastModifiedDate. */
    try
    {
        if(Trigger.isBefore)
        {
            boolean isExcludedProfile = ExcludeUserUtility.isExcludedUser(UserInfo.getUserName());
            if(!isExcludedProfile){
                for(Talent_Contract__c careerContract:trigger.new)
                {
                    careerContract.User_Last_Modified_Date__c = System.now();
                    careerContract.User_Last_Modified_By__c = UserInfo.getUserId(); 
                }
                
            }
            
        }
    }
    catch(Exception ex)
    {
        System.debug('Exception: '+ex.getStackTraceString());
    }
    
}