/*
To update bill rate with opportunity currency
*/
trigger Scopit_Emp_Before_Trigger on ScopeIt_Employee__c (before insert, before update) {

    //if(trigger.isInsert){
        for(ScopeIt_Employee__c scpEmp: trigger.new)
            scpEmp.currencyIsoCode = scpEmp.Opportunity_currency_code__c;
    //}
    //else{ 
    
        Map<String,Decimal> exchMap = new Map <String,Decimal>();
        List<String> ratelist = new List<String>();
        set<id> empidset = new set<Id>();
        Map<String,Decimal> BillRateMap = new Map <String,Decimal>();
        
        for(ScopeIt_Employee__c scpEmp: trigger.new)
            empidset.add(scpEmp.Employee_Bill_Rate__c);
            
        for(Employee_Bill_Rate__c objBillRate:[select id,Bill_Rate_Local_Currency__c from Employee_Bill_Rate__c where id=:empidset])
            BillRateMap.put(objBillRate.id,objBillRate.Bill_Rate_Local_Currency__c);
           
        for(Current_Exchange_Rate__c er:[select name,Conversion_Rate__c from Current_Exchange_Rate__c])
            exchMap.put(er.name,er.Conversion_Rate__c);  
        
                
        for(ScopeIt_Employee__c scpEmp: trigger.new){
            //system.debug(trigger.oldmap.get(scpemp.id).Employee_Bill_Rate__c +'......'+ scpEmp.Employee_Bill_Rate__c);
            if(trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(scpemp.id).Employee_Bill_Rate__c <> scpEmp.Employee_Bill_Rate__c)){
        
                system.debug(BillRateMap.get(scpEmp.Employee_Bill_Rate__c)+'...'+exchMap.get(scpEmp.currenCyISOCode)+'...'+exchMap.get(scpEmp.Employee_Currency_Code__c));
        
                if(BillRateMap.containsKey(scpEmp.Employee_Bill_Rate__c) && exchMap.containsKey(scpEmp.Employee_Currency_Code__c) && scpEmp.currenCyISOCode<>null)
                    scpEmp.Bill_Rate_Opp__c = ((BillRateMap.get(scpEmp.Employee_Bill_Rate__c) * exchMap.get(scpEmp.currenCyISOCode)) / exchMap.get(scpEmp.Employee_Currency_Code__c)).round();
                else
                    scpEmp.Bill_Rate_Opp__c = 0;
            }
        }
    //}
}