/*
Purpose:  This trigger adds Contact Distribution History whenever a Distribution is added/deleted from Contact  
==============================================================================================================================================
History ----------------------- VERSION     AUTHOR  DATE        DETAIL   
1.0 -    Shashank 04/8/2013  Created Apex Trigger for Contact Distribution(after insert, before delete)
2.0 -    Savina   12/12/2013 'Jan Relese#3633 ,added beforeUpdate Trigger
============================================================================================================================================== 
*/

trigger TRG14_ContactDistributionHistoryTrigger on Contact_Distributions__c (after insert, before delete, before update, before insert) 
{
    private static boolean isDMUser = AP17_ExecutionControl.isDataMigrationGroupMember(); 
    if(Trigger.isBefore && !isDMUser)
    {
        if(Trigger.isDelete)
        {
            AP35_ContactDistributionHistoryUtil.processContactDistributionBeforeDelete(trigger.oldMap);
        }
        // Added before update and and before insert trigger as a part of Request# 3633(January Release)
        if(Trigger.isUpdate)
        { 
               AP35_ContactDistributionHistoryUtil.processContactDistributionBeforeUpdate(Trigger.new, trigger.oldMap);
        }
        if(Trigger.isInsert)
        {
              AP35_ContactDistributionHistoryUtil.processContactDistributionBeforeInsert(Trigger.new);
        }

    }
    
    if(Trigger.isAfter && !isDMUser)
    
    {
        if(Trigger.isInsert)
        {
            AP35_ContactDistributionHistoryUtil.processContactDistributionAfterInsert(trigger.newMap);
        }
    }
         
}