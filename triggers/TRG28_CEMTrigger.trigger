/*Purpose:  This trigger calls create task method  when CEM record is inserted or updated
==============================================================================================================================================
History 
----------------------- 
VERSION     AUTHOR    DATE        DETAIL 
   1.0 -    Madhavi 11/7/2014  (As part of Dec 2014 release Request # 2264)Created Apex Trigger
 
============================================================================================================================================== 
*/
trigger TRG28_CEMTrigger on Client_Engagement_Measurement_Interviews__c (after insert,after update) {

If(Trigger.isAfter){

AP122_CEMTriggerUtil.createTask(trigger.new,trigger.oldmap);
}
}