trigger TRG11_CompetitorTrigger on Competitor__c (before delete) {

    if(Trigger.isDelete)
    {
        if(Trigger.isBefore)
        {
            AP20_CompetitorTriggerUtil.processCompetitorBeforeDelete(Trigger.old);
        }
    }    
    
}