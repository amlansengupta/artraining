({
		doInit : function(component, event, helper) {
				helper.setIsFollowing(component);
		},

		toggleFollow: function(component, event, helper) {
				if (component.get("v.isFollowing")) {
						helper.unfollow(component);
				} else {
						helper.follow(component);
				}
		}
})