// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	doInit : function(component, event, helper) {
       var totalResults = event.getParam("totalResults");
       component.set("v.numberOfResults", totalResults);
	},

    //Changed due to resolve the java script Error of Renderer/afterRender
    afterScriptsLoaded: function(component, event, helper){
        //svg4everybody();
    },
    setSitePrefix: function(component, event, helper) {
        helper.get_SitePrefix(component);
    }
})