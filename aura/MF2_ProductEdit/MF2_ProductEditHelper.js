({
	allocateRevHelper : function(component, event) {
		var unitprice = component.find("unitprice").get("v.value");
        var revstart = component.find("revstart").get("v.value");
        var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
            var action = component.get("c.reallocateRevenue");
            action.setParams({
                'unitPrice' : unitprice,
                'RevStart' : revstart,
                'RevEnd' : revend,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var allrev = response.getReturnValue(); 
                    var cYRev = allrev.cYRev;
                    component.set("v.cyRev",cYRev);
                    //alert(cYRev);
                    var secYRev = allrev.secYRev;
                    component.set("v.scndRev",secYRev);
                    var thrdYRev = allrev.thrdYRev;
                    component.set("v.thrdRev",thrdYRev);
                    var calcRev = allrev.calcRev;
                    component.set("v.annualized",calcRev);
                  	component.set("v.holdSubmit",false);
                }                
            });
            
            $A.enqueueAction(action);
            
        }
    }
})