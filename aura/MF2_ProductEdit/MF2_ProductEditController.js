({
    doInit : function(component, event, helper) {
        //alert('doinit load first');
        //$A.get('e.force:refreshView').fire();
        component.set("v.Spinner", true);
        component.set("v.showError", false);
        component.set("v.Error",'');
        var act = component.get("c.fetchOLI");
        act.setParams({
            'oliId' : component.get("v.oliId"),
        });
        act.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              component.set("v.flagCol",true);
                console.log(JSON.stringify(response.getReturnValue()));
                //alert('&&'+response.getReturnValue().Sales_Professional__c);
                component.set("v.oli", response.getReturnValue());
                var passColltoLookupEvent = $A.get("e.c:MF2_PassColleaguetoLookup");
                var passColltoLookupEvent1 = $A.get("e.c:MF2_PassColleaguetoLookup");
               
                passColltoLookupEvent.setParams({
                    "SalesProfId" : component.get("v.oli").Sales_Professional__c,                    
                    "ProjManId" : component.get("v.oli").Project_Manager__c,
                    "DSSalesLeadId" : component.get("v.oli").DS_Sales_Leader__c,
                })
                 passColltoLookupEvent.fire();
                
               /* var col1 = component.get("v.colLookup");
                 passColltoLookupEvent1.setParams({
                    "oliId" : component.get("v.oli").Sales_Professional__c
                })
                 passColltoLookupEvent1.fire();
                var col2 = component.get("v.colLookup1");
                component.set("v.colLookup",col1);
                component.set("v.colLookup1",col2);*/
                component.set("v.oppId",response.getReturnValue().OpportunityId);
                component.set("v.unitPrice",response.getReturnValue().UnitPrice);
                component.set("v.cyRev",response.getReturnValue().CurrentYearRevenue_edit__c);
                component.set("v.scndRev",response.getReturnValue().Year2Revenue_edit__c);
                component.set("v.thrdRev",response.getReturnValue().Year3Revenue_edit__c);
                component.set("v.annualizedRevEdit",response.getReturnValue().Annualized_Revenue_editable__c);
                component.set("v.annualized",response.getReturnValue().Annualized_Revenue_editable__c);
                component.set("v.assetsUnderMgm",response.getReturnValue().Assets_Under_Mgmt__c);
                component.set("v.selectedWDMarket",response.getReturnValue().WD_Market_Segment__c);
                component.set("v.selectedSNMarket",response.getReturnValue().ServiceNow_Market_Segment__c);
                component.set("v.selectedWDReg",response.getReturnValue().Workday_Region__c);
                component.set("v.selectedMerInf",response.getReturnValue().Mercer_Influence__c);
                component.set("v.oli.Project_Manager__c",response.getReturnValue().Project_Manager__c);
               
                component.set("v.oli.Sales_Professional__c",response.getReturnValue().Sales_Professional__c);
                //component.set("v.selectedMerInf",response.getReturnValue().Mercer_Influence__c);
                component.set("v.selectedSNR",response.getReturnValue().ServiceNow_Region__c);
                component.set("v.CurrencyIsoCode",response.getReturnValue().Opportunity.CurrencyIsoCode);
                //component.find("unitprice").set("v.format",response.getReturnValue().Opportunity.CurrencyIsoCode+" #,###.00");
                var prodSegment = response.getReturnValue().PricebookEntry.Product2.Segment__c;
                //alert('Segment : '+prodSegment.toUpperCase());
                component.set("v.flagCol",true);
                if(prodSegment.toUpperCase() == "WORKDAY"){
                    component.set("v.isSegmentWD",true);
                }
                if(prodSegment.toUpperCase() == "SERVICENOW"){
                    component.set("v.isSegmentSN",true);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
            
             //component.get("v.flagCol",true);
        });
        
        var action = component.get("c.fetchDigitalPicklist");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var digitalPKList = response.getReturnValue();
                var wdMarketList = digitalPKList.wdMarketList;
                var snMarketList = digitalPKList.snMarketList;
                var wdRegList = digitalPKList.wdRegList;
                var merInfList = digitalPKList.merInfList;
                var ServiceNowList = digitalPKList.serviceNowRegionList;
                /*alert('wdmarket : '+wdMarketList);
                alert('snmarket : '+snMarketList);
                alert('wdregion : '+wdRegList);*/
                
                var wdMarkets = [];
                var snMarkets = [];
                var wdRegions = [];
                var merInfs = [];
                var SnrList= [];
                
                for(var i = 0; i<wdMarketList.length; i++ ){
                    var wdMarket = {
                        "label": wdMarketList[i],
                        "value": wdMarketList[i],
                    };
                    wdMarkets.push(wdMarket);
                }
                wdMarkets.unshift('');
                
                for(var i = 0; i<snMarketList.length; i++ ){
                    var snMarket = {
                        "label": snMarketList[i],
                        "value": snMarketList[i],
                    };
                    snMarkets.push(snMarket);
                }
                snMarkets.unshift('');
                
                for(var i = 0; i<wdRegList.length; i++ ){
                    var wdRegion = {
                        "label": wdRegList[i],
                        "value": wdRegList[i],
                    };
                    wdRegions.push(wdRegion);
                }
                wdRegions.unshift('');
                
                for(var i = 0; i<merInfList.length; i++ ){
                    var merInf = {
                        "label": merInfList[i],
                        "value": merInfList[i],
                    };
                    merInfs.push(merInf);
                }
                merInfs.unshift('');
                for(var i = 0; i<ServiceNowList.length; i++ ){
                    var Snr = {
                        "label": ServiceNowList[i],
                        "value": ServiceNowList[i],
                    };
                    SnrList.push(Snr);
                }
                SnrList.unshift('');
                component.set("v.wdMarketList", wdMarkets);
                component.set("v.snMarketList", snMarkets);
                component.set("v.wdRegList", wdRegions);
                component.set("v.merInfList", merInfs);
                component.set("v.SNRList", SnrList);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
        });
        
        
        
        $A.enqueueAction(act);
        $A.enqueueAction(action);
        
    },
    
    handleOnLoad : function (component, event, helper){
        //alert('form load first');
        //var eventFields = event.getParam("fields");
        //var oppId = eventFields["Opportunity"];
        /*console.log(JSON.stringify(component.get("v.oli")));
        var prodSegment = component.get("v.oli").PricebookEntry.Product2.Segment__c;
        //alert('Segment : '+prodSegment);
        if(prodSegment == "Workday"){
            component.set("v.isSegmentWD",true);
        }
        if(prodSegment == "Servicenow"){
            component.set("v.isSegmentSN",true);
        }*/
        //alert('segment : '+ component.get('v.isSegmentWD'))
        component.set("v.showError", false);
        component.set("v.Error",'');
        var oppId = component.get("v.oppId");
        var actionopp = component.get("c.fetchOpportunity");
        actionopp.setParams({
            'oppId' : oppId,
        });
        actionopp.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opp", response.getReturnValue());
                //component.find("cyRev").set("v.format",response.getReturnValue().CurrencyIsoCode+"#,###.00");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(JSON.stringify(errors));
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(actionopp);
        
    },
    
    handleWDM : function(component, event, helper){
        //alert('selected : '+ event.getParam("value"));
        component.set("v.selectedWDMarket", event.getParam("value"));  
    },
    
    handleSNM : function(component, event, helper){
        component.set("v.selectedSNMarket", event.getParam("value"));   
    },
    
    handleWDR : function(component, event, helper){
        component.set("v.selectedWDReg", event.getParam("value"));  
    },
    
    handleMI : function(component, event, helper){
        component.set("v.selectedMerInf",event.getParam("value"));  
    },
    handleSNR : function(component, event, helper){
        component.set("v.selectedSNR",event.getParam("value"));  
    },
    
    allocateRev : function(component, event, helper){
        event.preventDefault();
        var unitprice = component.find("unitprice").get("v.value");
        var revstart = component.find("revstart").get("v.value");
        var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
            var action = component.get("c.reallocateRevenue");
            action.setParams({
                'unitPrice' : unitprice,
                'RevStart' : revstart,
                'RevEnd' : revend,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var allrev = response.getReturnValue(); 
                    var cYRev = allrev.cYRev;
                    component.set("v.cyRev",cYRev);
                    var secYRev = allrev.secYRev;
                    component.set("v.scndRev",secYRev);
                    var thrdYRev = allrev.thrdYRev;
                    component.set("v.thrdRev",thrdYRev);
                    var calcRev = allrev.calcRev;
                   // component.set("v.annualized1",calcRev);
                    console.log("annualized:"+calcRev);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    component.set("v.showError", true);
                    component.set("v.Error",errors);
                }
            });
            
            $A.enqueueAction(action);
            
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": "Please put Sales Price, Revenue Start Date and Revenue End Date before clicking on Allocate Revenue button.",
            });
            toastEvent.fire();
        }
    },
    
    handleOnSubmit : function(component, event, helper) {
        // alert(component.get("v.dslookup").Id);
        event.preventDefault();
        component.set("v.submitted", false);
        component.set("v.callOppAction", true);
        component.set("v.showError", false);
        component.set("v.Error", '');
        
        var thisRecordCode = component.get("v.opp").CurrencyIsoCode;
        var eventFields = event.getParam("fields");
        eventFields["UnitPrice"] = component.get("v.unitPrice");
        var resultstring = eventFields["UnitPrice"];
        // alert('Hi');
        var act = component.get("c.fetchCurrEx");
        act.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // alert('Hi success');
                console.log("First success");
                var exchangeRates = response.getReturnValue();
                if(exchangeRates !== null || !exchangeRates.isEmpty()){
                    var map = {};
                    var h = new Object(); 
                    if(exchangeRates){
                        for (var i = 0; i < exchangeRates.length; i++) {
                            map[exchangeRates[i].Name] = exchangeRates[i].Conversion_Rate__c;
                        }
                    }
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error in fetching Currency Exchange message: " + errors[0].message);
                        alert("Error in fetching Currency Exchange message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert("Unknown error");
                } 
            }
            // alert('Hi');
            
            var unitpriceusd =  resultstring / map[thisRecordCode];
             
            if(unitpriceusd >= "{!$Label.c.Revenue_Amount}"){
                component.set("v.showError", true);
                component.set("v.Error", "{!$Label.c.Revenue_Amount_Error_Message}");
            }
            else{
                
                 var cyRev = component.get("v.cyRev");
                var scndRev = component.get("v.scndRev");
                var thrdRev = component.get("v.thrdRev");
                //alert('hi'+cyRev);
                console.log('1'+cyRev);
                console.log('2'+scndRev);
                console.log('3'+thrdRev);
                if((cyRev === null || cyRev === '' || cyRev === 0) && 
                   (scndRev === null || scndRev === '' || scndRev === 0 ) &&
                   (thrdRev === null || thrdRev === '' || thrdRev === 0)){
                 var unitprice = component.find("unitprice").get("v.value");
        		var revstart = component.find("revstart").get("v.value");
       	 			var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
                var action = component.get("c.reallocateRevenue");
                action.setParams({
                    'unitPrice' : unitprice,
                    'RevStart' : revstart,
                    'RevEnd' : revend,
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                   // alert(state);
                    if (state === "SUCCESS") {
                        // alert(component.get("v.callOppAction") );
                        console.log("Second success");
                        var allrev = response.getReturnValue(); 
                        var cYRev = allrev.cYRev;
                        component.set("v.cyRev",cYRev);
                        // alert(allrev);
                        var secYRev = allrev.secYRev;
                        
                        component.set("v.scndRev",secYRev);
                        var thrdYRev = allrev.thrdYRev;
                        component.set("v.thrdRev",thrdYRev);
                        
                        var calcRev = allrev.calcRev;
                        //console.log("1st check"+calcRev);
                        //component.set("v.annualized1",calcRev);
                        //component.set("v.holdSubmit",false);
                       // console.log("1st check"+component.get("v.annualized1"));
                    
                    var oppId = component.get("v.oppId");
                    
                    if(component.get("v.cyRev") === null || component.get("v.cyRev") == undefined){
                        component.set("v.cyRev",0); 
                    }
                    if(component.get("v.scndRev") === null || component.get("v.scndRev") == undefined){
                        component.set("v.scndRev",0); 
                    }
                    if(component.get("v.thrdRev") === null || component.get("v.thrdRev") == undefined){
                        component.set("v.thrdRev",0); 
                    }
                    console.log("2nd check");
                    eventFields["CurrentYearRevenue_edit__c"] = component.get("v.cyRev");
                    eventFields["Year2Revenue_edit__c"] = component.get("v.scndRev");
                    eventFields["Year3Revenue_edit__c"] = component.get("v.thrdRev");
                    eventFields["WD_Market_Segment__c"] = component.get("v.selectedWDMarket");
                    eventFields["Workday_Region__c"] = component.get("v.selectedWDReg");
                    eventFields["ServiceNow_Market_Segment__c"] = component.get("v.selectedSNMarket");
                    eventFields["Mercer_Influence__c"] = component.get("v.selectedMerInf");
                    eventFields["ServiceNow_Region__c"] = component.get("v.selectedSNR");
                    eventFields["Annualized_Revenue_editable__c"] = component.get("v.annualized");
                    eventFields["Assets_Under_Mgmt__c"] = component.get("v.myCurrency");
                    //event.setParam("fields",fields);
                    // alert(component.get("v.cyRev"));
                    //alert(component.get("v.selectedSNMarket"));
                    
                    if(component.get("v.unitPrice") === 0 || component.get("v.unitPrice") === null ||component.get("v.unitPrice") === ''
                       || eventFields["Revenue_Start_Date__c"] === null || eventFields["Revenue_Start_Date__c"] === ''
                       || eventFields["Revenue_End_Date__c"] === null || eventFields["Revenue_End_Date__c"] === ''
                       || eventFields["Duration__c"] === null || eventFields["Duration__c"] === ""){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please fill up all the required fields. (Sales Price, Revenue Start Date, Revenue End Date, Duration)");
                    } 
                    
                    if(eventFields["DS_Net_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Net BPs Fee cannot be negative");
                    }
                    
                    if(eventFields["DS_Sub_Manager_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Sub-Manager BPs Fee cannot be negative");
                    }
                    
                    if((eventFields["CurrentYearRevenue_edit__c"] === null || eventFields["CurrentYearRevenue_edit__c"] === '' || eventFields

["CurrentYearRevenue_edit__c"] === 0) && 
                       (eventFields["Year2Revenue_edit__c"] === null || eventFields["Year2Revenue_edit__c"] === '' || eventFields["Year2Revenue_edit__c"] === 0 ) &&
                       (eventFields["Year3Revenue_edit__c"] === null || eventFields["Year3Revenue_edit__c"] === '' || eventFields["Year3Revenue_edit__c"] === 0)){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please allocate the revenue for the Product by clicking on Allocate Revenue button."); 
                    }
                    
                    if((component.get("v.isSegmentWD") === true) && 
                       (component.get("v.selectedWDMarket") === undefined || component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") == null || component.get("v.selectedMerInf") === "" 
                        || component.get("v.selectedWDReg") === undefined || component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "" )){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for Workday Market Segment, Workday Region and Mercer Influence");        
                    }
                    console.log("3rd check");
                    //alert(component.get("v.isSegmentSN"));
                    if((component.get("v.isSegmentSN") === true) && 
                       ( component.get("v.selectedSNMarket") === undefined || component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === 

"" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === "" 
                        ||  component.get("v.selectedSNR") === undefined || component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "")){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for ServiceNow Market Segment, ServiceNow Region and Mercer Influence.");
                    }
                    
                   // if($A.get("$Browser.isPhone")){
                         if(component.get("v.colLookup")!=null && component.get("v.colLookup").Id != undefined ){
                            eventFields["Project_Manager__c"] = component.get("v.colLookup").Id; 
                        }
                    else{
                        eventFields["Project_Manager__c"] = null; 
                    }
                         if(component.get("v.colLookup1")!=null && component.get("v.colLookup1").Id != undefined  ){
                            eventFields["Sales_Professional__c"] = component.get("v.colLookup1").Id; 
                        }
                    else{
                        eventFields["Sales_Professional__c"] = NULL; 
                    }
                        if(component.get("v.dslookup")!=null && component.get("v.dslookup").Id != undefined  ){
                            eventFields["DS_Sales_Leader__c"] = component.get("v.dslookup").Id; 
                        }
                    else{
                        eventFields["DS_Sales_Leader__c"] = null; 
                    }
                    //}
                    
                    //alert(component.get("v.callOppAction") );
                    if(component.get("v.callOppAction") === true){
                        component.find("editForm").submit(eventFields);
                        component.set("v.submitted", true);
                        
                    }  
                    console.log("Hi there");
                    }                
                    
                    
                    
                });
                $A.enqueueAction(action);
        }
                }
                else{
                   /* if(component.get("v.cyRev") === null || component.get("v.cyRev") == undefined){
                        component.set("v.cyRev",0); 
                    }
                    if(component.get("v.scndRev") === null || component.get("v.scndRev") == undefined){
                        component.set("v.scndRev",0); 
                    }
                    if(component.get("v.thrdRev") === null || component.get("v.thrdRev") == undefined){
                        component.set("v.thrdRev",0); 
                    }*/
                    console.log("2nd check");
                    eventFields["CurrentYearRevenue_edit__c"] = component.get("v.cyRev");
                    eventFields["Year2Revenue_edit__c"] = component.get("v.scndRev");
                    eventFields["Year3Revenue_edit__c"] = component.get("v.thrdRev");
                    eventFields["WD_Market_Segment__c"] = component.get("v.selectedWDMarket");
                    eventFields["Workday_Region__c"] = component.get("v.selectedWDReg");
                    eventFields["ServiceNow_Market_Segment__c"] = component.get("v.selectedSNMarket");
                    eventFields["Mercer_Influence__c"] = component.get("v.selectedMerInf");
                    eventFields["ServiceNow_Region__c"] = component.get("v.selectedSNR");
                    eventFields["Annualized_Revenue_editable__c"] = component.get("v.annualized");
                    eventFields["Assets_Under_Mgmt__c"] = component.get("v.myCurrency");
                    //event.setParam("fields",fields);
                    // alert(component.get("v.cyRev"));
                    //alert(component.get("v.selectedSNMarket"));
                    
                    if(component.get("v.unitPrice") === 0 || component.get("v.unitPrice") === null ||component.get("v.unitPrice") === ''
                       || eventFields["Revenue_Start_Date__c"] === null || eventFields["Revenue_Start_Date__c"] === ''
                       || eventFields["Revenue_End_Date__c"] === null || eventFields["Revenue_End_Date__c"] === ''
                       || eventFields["Duration__c"] === null || eventFields["Duration__c"] === ""){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please fill up all the required fields. (Sales Price, Revenue Start Date, Revenue End Date, Duration)");
                    } 
                    
                    if(eventFields["DS_Net_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Net BPs Fee cannot be negative");
                    }
                    
                    if(eventFields["DS_Sub_Manager_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Sub-Manager BPs Fee cannot be negative");
                    }
                    
                    if((eventFields["CurrentYearRevenue_edit__c"] === null || eventFields["CurrentYearRevenue_edit__c"] === '' || eventFields

["CurrentYearRevenue_edit__c"] === 0) && 
                       (eventFields["Year2Revenue_edit__c"] === null || eventFields["Year2Revenue_edit__c"] === '' || eventFields["Year2Revenue_edit__c"] === 0 ) &&
                       (eventFields["Year3Revenue_edit__c"] === null || eventFields["Year3Revenue_edit__c"] === '' || eventFields["Year3Revenue_edit__c"] === 0)){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please allocate the revenue for the Product by clicking on Allocate Revenue button."); 
                    }
                    
                    if((component.get("v.isSegmentWD") === true) && 
                       (component.get("v.selectedWDMarket") === undefined || component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") == null || component.get("v.selectedMerInf") === "" 
                        || component.get("v.selectedWDReg") === undefined || component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "" )){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for Workday Market Segment, Workday Region and Mercer Influence");        
                    }
                    console.log("3rd check");
                    //alert(component.get("v.isSegmentSN"));
                    if((component.get("v.isSegmentSN") === true) && 
                       ( component.get("v.selectedSNMarket") === undefined || component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === 

"" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === "" 
                        ||  component.get("v.selectedSNR") === undefined || component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "")){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for ServiceNow Market Segment, ServiceNow Region and Mercer Influence.");
                    }
                    
                   //alert('Hi'+component.get("v.callOppAction")+component.get("v.submitted"));
                    if(component.get("v.colLookup")!=null && component.get("v.colLookup").Id != undefined ){
                            eventFields["Project_Manager__c"] = component.get("v.colLookup").Id; 
                        }
                    else{
                        eventFields["Project_Manager__c"] = null; 
                    }
                         if(component.get("v.colLookup1")!=null && component.get("v.colLookup1").Id != undefined  ){
                            eventFields["Sales_Professional__c"] = component.get("v.colLookup1").Id; 
                        }
                    else{
                        eventFields["Sales_Professional__c"] = null; 
                    }
                        if(component.get("v.dslookup")!=null && component.get("v.dslookup").Id != undefined  ){
                            eventFields["DS_Sales_Leader__c"] = component.get("v.dslookup").Id; 
                        }
                    else{
                        eventFields["DS_Sales_Leader__c"] = null; 
                    }
                    
                    console.log('&&'+component.get("v.callOppAction")+component.get("v.submitted"));
                 if(component.get("v.callOppAction") === true && component.get("v.submitted") == false){
                        component.find("editForm").submit(eventFields);
                        component.set("v.submitted", true);
                        
                    } 
                }
            }
            
        }); 
        $A.enqueueAction(act);
        
        
    },
    
    handleCancel : function(component, event, helper){
        event.preventDefault();
        if(component.get("v.openedfromVF")){
            if($A.get("$Browser.isPhone")){
                sforce.one.back(true);  
            }
            else{
                location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view');
            }
        } 
        else{
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.oppId"),
                "slideDevName": "detail"
            });
            navEvt.fire();  
        }  
    },
    
    handleOnSuccess : function(component, event, helper) {
        //alert('OLI created');
        component.set("v.submitted", true);
        var record = event.getParam("response");
        
        if(component.get("v.openedfromVF")){
            if($A.get("$Browser.isPhone")){
                alert('Product: '+record.fields.Product2.displayValue + ' saved successfully for Opportunity:' + record.fields.Opportunity.displayValue+'!');
                sforce.one.back(true);  
            }
            else{
                component.set("v.isSuccess",true);
            }
        }
        else{
            var record = event.getParam("response");
            component.find("notificationsLibrary").showToast({
                "title": "Saved",
                "message": "{0} saved for {1}",
                "messageData": [
                    {
                        url: '/' + record.Id,
                        label: record.fields.Product2.displayValue
                    },
                    {
                        url: '/' + record.fields.OpportunityId.value, 
                        label: record.fields.Opportunity.displayValue
                    }
                ]
            });   
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.oppId"),
                "slideDevName": "detail"
            });
            navEvt.fire();  
        } 
        
    },
    
    handleOnError : function(component, event, helper) {
        //component.set("v.submitted",false);
        component.set("v.submitted", true);
        var message = '';
        var errors = event.getParams();
        //console.log(JSON.stringify(errors));
        //alert(JSON.stringify(errors));
        var errormessages = errors.output;
        
        if ($A.util.isEmpty(errormessages.errors) === false) {
            if (errormessages.errors.length > 0) {
                for (var j = 0; errormessages.errors.length > j; j++) {
                    var fielderror = errormessages.errors[j];
                    
                    // message += fielderror.errorCode + ' (' + fielderror.field + ') : ' + fielderror.message;
                    message += fielderror.message;
                }
            }
        }
        if ($A.util.isEmpty(errormessages.fieldErrors) === false) {
            
            var fieldErrorObj = Object.values(errormessages.fieldErrors)[0];
            console.log(JSON.stringify(fieldErrorObj[0]));
            message += fieldErrorObj[0].errorCode + ' (' + fieldErrorObj[0].field + ') : ' + fieldErrorObj[0].message;
            
        }
        component.set("v.showError", true);
        component.set("v.Error",message);
        
    },
    
    showSpinner: function(component, event, helper) {
        if(component.get("v.submitted")){
            component.set("v.Spinner", true); 
        }
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },
    
    netKeyPress : function(component,event,helper){
        //  alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.netBpsErrorText","");
        if(val<0){
            component.set("v.netBpsErrorText","Net BPs Fee cannot be negative");
            $A.util.addClass(component.find("netBpsError"),"show"); 
        }
    },
    
    keyPress : function(component,event,helper){
        // alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.bpsErrorText","");
        if(val<0){
            component.set("v.bpsErrorText","Sub-Manager BPs Fee cannot be negative");
            $A.util.addClass(component.find("bpsError"),"show"); 
        }
    },
    
    closeModal: function(component, event, helper) {
        component.set("v.isSuccess", false);
        if(component.get("v.openedfromVF")){
            location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view'); 
        }
    },
})({
    doInit : function(component, event, helper) {
        //alert('doinit load first');
        //$A.get('e.force:refreshView').fire();
        component.set("v.Spinner", true);
        component.set("v.showError", false);
        component.set("v.Error",'');
        var act = component.get("c.fetchOLI");
        act.setParams({
            'oliId' : component.get("v.oliId"),
        });
        act.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.oli", response.getReturnValue());
                component.set("v.oppId",response.getReturnValue().OpportunityId);
                component.set("v.unitPrice",response.getReturnValue().UnitPrice);
                component.set("v.cyRev",response.getReturnValue().CurrentYearRevenue_edit__c);
                component.set("v.scndRev",response.getReturnValue().Year2Revenue_edit__c);
                component.set("v.thrdRev",response.getReturnValue().Year3Revenue_edit__c);
                component.set("v.annualizedRevEdit",response.getReturnValue().Annualized_Revenue_editable__c);
                component.set("v.annualized",response.getReturnValue().Annualized_Revenue_editable__c);
                component.set("v.assetsUnderMgm",response.getReturnValue().Assets_Under_Mgmt__c);
                component.set("v.selectedWDMarket",response.getReturnValue().WD_Market_Segment__c);
                component.set("v.selectedSNMarket",response.getReturnValue().ServiceNow_Market_Segment__c);
                component.set("v.selectedWDReg",response.getReturnValue().Workday_Region__c);
                component.set("v.selectedMerInf",response.getReturnValue().Mercer_Influence__c);
                component.set("v.selectedSNR",response.getReturnValue().ServiceNow_Region__c);
                component.set("v.CurrencyIsoCode",response.getReturnValue().Opportunity.CurrencyIsoCode);
                //component.find("unitprice").set("v.format",response.getReturnValue().Opportunity.CurrencyIsoCode+" #,###.00");
                var prodSegment = response.getReturnValue().PricebookEntry.Product2.Segment__c;
                //alert('Segment : '+prodSegment.toUpperCase());
                if(prodSegment.toUpperCase() == "WORKDAY"){
                    component.set("v.isSegmentWD",true);
                }
                if(prodSegment.toUpperCase() == "SERVICENOW"){
                    component.set("v.isSegmentSN",true);
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
        });
        
        var action = component.get("c.fetchDigitalPicklist");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var digitalPKList = response.getReturnValue();
                var wdMarketList = digitalPKList.wdMarketList;
                var snMarketList = digitalPKList.snMarketList;
                var wdRegList = digitalPKList.wdRegList;
                var merInfList = digitalPKList.merInfList;
                var ServiceNowList = digitalPKList.serviceNowRegionList;
                /*alert('wdmarket : '+wdMarketList);
                alert('snmarket : '+snMarketList);
                alert('wdregion : '+wdRegList);*/
                
                var wdMarkets = [];
                var snMarkets = [];
                var wdRegions = [];
                var merInfs = [];
                var SnrList= [];
                
                for(var i = 0; i<wdMarketList.length; i++ ){
                    var wdMarket = {
                        "label": wdMarketList[i],
                        "value": wdMarketList[i],
                    };
                    wdMarkets.push(wdMarket);
                }
                wdMarkets.unshift('');
                
                for(var i = 0; i<snMarketList.length; i++ ){
                    var snMarket = {
                        "label": snMarketList[i],
                        "value": snMarketList[i],
                    };
                    snMarkets.push(snMarket);
                }
                snMarkets.unshift('');
                
                for(var i = 0; i<wdRegList.length; i++ ){
                    var wdRegion = {
                        "label": wdRegList[i],
                        "value": wdRegList[i],
                    };
                    wdRegions.push(wdRegion);
                }
                wdRegions.unshift('');
                
                for(var i = 0; i<merInfList.length; i++ ){
                    var merInf = {
                        "label": merInfList[i],
                        "value": merInfList[i],
                    };
                    merInfs.push(merInf);
                }
                merInfs.unshift('');
                for(var i = 0; i<ServiceNowList.length; i++ ){
                    var Snr = {
                        "label": ServiceNowList[i],
                        "value": ServiceNowList[i],
                    };
                    SnrList.push(Snr);
                }
                SnrList.unshift('');
                component.set("v.wdMarketList", wdMarkets);
                component.set("v.snMarketList", snMarkets);
                component.set("v.wdRegList", wdRegions);
                component.set("v.merInfList", merInfs);
                component.set("v.SNRList", SnrList);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
        });
        
        $A.enqueueAction(act);
        $A.enqueueAction(action);
        
    },
    
    handleOnLoad : function (component, event, helper){
        //alert('form load first');
        //var eventFields = event.getParam("fields");
        //var oppId = eventFields["Opportunity"];
        /*console.log(JSON.stringify(component.get("v.oli")));
        var prodSegment = component.get("v.oli").PricebookEntry.Product2.Segment__c;
        //alert('Segment : '+prodSegment);
        if(prodSegment == "Workday"){
            component.set("v.isSegmentWD",true);
        }
        if(prodSegment == "Servicenow"){
            component.set("v.isSegmentSN",true);
        }*/
        //alert('segment : '+ component.get('v.isSegmentWD'))
        component.set("v.showError", false);
        component.set("v.Error",'');
        var oppId = component.get("v.oppId");
        var actionopp = component.get("c.fetchOpportunity");
        actionopp.setParams({
            'oppId' : oppId,
        });
        actionopp.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opp", response.getReturnValue());
                //component.find("cyRev").set("v.format",response.getReturnValue().CurrencyIsoCode+"#,###.00");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(JSON.stringify(errors));
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(actionopp);
        
    },
    
    handleWDM : function(component, event, helper){
        //alert('selected : '+ event.getParam("value"));
        component.set("v.selectedWDMarket", event.getParam("value"));  
    },
    
    handleSNM : function(component, event, helper){
        component.set("v.selectedSNMarket", event.getParam("value"));   
    },
    
    handleWDR : function(component, event, helper){
        component.set("v.selectedWDReg", event.getParam("value"));  
    },
    
    handleMI : function(component, event, helper){
        component.set("v.selectedMerInf",event.getParam("value"));  
    },
    handleSNR : function(component, event, helper){
        component.set("v.selectedSNR",event.getParam("value"));  
    },
    
    allocateRev : function(component, event, helper){
        event.preventDefault();
        var unitprice = component.find("unitprice").get("v.value");
        var revstart = component.find("revstart").get("v.value");
        var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
            var action = component.get("c.reallocateRevenue");
            action.setParams({
                'unitPrice' : unitprice,
                'RevStart' : revstart,
                'RevEnd' : revend,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var allrev = response.getReturnValue(); 
                    var cYRev = allrev.cYRev;
                    component.set("v.cyRev",cYRev);
                    var secYRev = allrev.secYRev;
                    component.set("v.scndRev",secYRev);
                    var thrdYRev = allrev.thrdYRev;
                    component.set("v.thrdRev",thrdYRev);
                    var calcRev = allrev.calcRev;
                   // component.set("v.annualized1",calcRev);
                    console.log("annualized:"+calcRev);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    component.set("v.showError", true);
                    component.set("v.Error",errors);
                }
            });
            
            $A.enqueueAction(action);
            
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": "Please put Sales Price, Revenue Start Date and Revenue End Date before clicking on Allocate Revenue button.",
            });
            toastEvent.fire();
        }
    },
    
    handleOnSubmit : function(component, event, helper) {
        // alert('Hi');
        event.preventDefault();
        
        component.set("v.callOppAction", true);
        component.set("v.showError", false);
        component.set("v.Error", '');
        
        var thisRecordCode = component.get("v.opp").CurrencyIsoCode;
        var eventFields = event.getParam("fields");
        eventFields["UnitPrice"] = component.get("v.unitPrice");
        var resultstring = eventFields["UnitPrice"];
        // alert('Hi');
        var act = component.get("c.fetchCurrEx");
        act.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // alert('Hi success');
                console.log("First success");
                var exchangeRates = response.getReturnValue();
                if(exchangeRates !== null || !exchangeRates.isEmpty()){
                    var map = {};
                    var h = new Object(); 
                    if(exchangeRates){
                        for (var i = 0; i < exchangeRates.length; i++) {
                            map[exchangeRates[i].Name] = exchangeRates[i].Conversion_Rate__c;
                        }
                    }
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error in fetching Currency Exchange message: " + errors[0].message);
                        alert("Error in fetching Currency Exchange message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert("Unknown error");
                } 
            }
            // alert('Hi');
            
            var unitpriceusd =  resultstring / map[thisRecordCode];
             
            if(unitpriceusd >= "{!$Label.c.Revenue_Amount}"){
                component.set("v.showError", true);
                component.set("v.Error", "{!$Label.c.Revenue_Amount_Error_Message}");
            }
            else{
                
                 var cyRev = component.get("v.cyRev");
                var scndRev = component.get("v.scndRev");
                var thrdRev = component.get("v.thrdRev");
                //alert('hi'+cyRev);
                console.log('1'+cyRev);
                console.log('2'+scndRev);
                console.log('3'+thrdRev);
                if((cyRev === null || cyRev === '' || cyRev === 0) && 
                   (scndRev === null || scndRev === '' || scndRev === 0 ) &&
                   (thrdRev === null || thrdRev === '' || thrdRev === 0)){
                 var unitprice = component.find("unitprice").get("v.value");
        		var revstart = component.find("revstart").get("v.value");
       	 			var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
                var action = component.get("c.reallocateRevenue");
                action.setParams({
                    'unitPrice' : unitprice,
                    'RevStart' : revstart,
                    'RevEnd' : revend,
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                   // alert(state);
                    if (state === "SUCCESS") {
                        // alert(component.get("v.callOppAction") );
                        console.log("Second success");
                        var allrev = response.getReturnValue(); 
                        var cYRev = allrev.cYRev;
                        component.set("v.cyRev",cYRev);
                        // alert(allrev);
                        var secYRev = allrev.secYRev;
                        
                        component.set("v.scndRev",secYRev);
                        var thrdYRev = allrev.thrdYRev;
                        component.set("v.thrdRev",thrdYRev);
                        
                        var calcRev = allrev.calcRev;
                        //console.log("1st check"+calcRev);
                        //component.set("v.annualized1",calcRev);
                        //component.set("v.holdSubmit",false);
                       // console.log("1st check"+component.get("v.annualized1"));
                    
                    var oppId = component.get("v.oppId");
                    
                    if(component.get("v.cyRev") === null || component.get("v.cyRev") == undefined){
                        component.set("v.cyRev",0); 
                    }
                    if(component.get("v.scndRev") === null || component.get("v.scndRev") == undefined){
                        component.set("v.scndRev",0); 
                    }
                    if(component.get("v.thrdRev") === null || component.get("v.thrdRev") == undefined){
                        component.set("v.thrdRev",0); 
                    }
                    console.log("2nd check");
                    eventFields["CurrentYearRevenue_edit__c"] = component.get("v.cyRev");
                    eventFields["Year2Revenue_edit__c"] = component.get("v.scndRev");
                    eventFields["Year3Revenue_edit__c"] = component.get("v.thrdRev");
                    eventFields["WD_Market_Segment__c"] = component.get("v.selectedWDMarket");
                    eventFields["Workday_Region__c"] = component.get("v.selectedWDReg");
                    eventFields["ServiceNow_Market_Segment__c"] = component.get("v.selectedSNMarket");
                    eventFields["Mercer_Influence__c"] = component.get("v.selectedMerInf");
                    eventFields["ServiceNow_Region__c"] = component.get("v.selectedSNR");
                    eventFields["Annualized_Revenue_editable__c"] = component.get("v.annualized");
                    eventFields["Assets_Under_Mgmt__c"] = component.get("v.myCurrency");
                    //event.setParam("fields",fields);
                    // alert(component.get("v.cyRev"));
                    //alert(component.get("v.selectedSNMarket"));
                    
                    if(component.get("v.unitPrice") === 0 || component.get("v.unitPrice") === null ||component.get("v.unitPrice") === ''
                       || eventFields["Revenue_Start_Date__c"] === null || eventFields["Revenue_Start_Date__c"] === ''
                       || eventFields["Revenue_End_Date__c"] === null || eventFields["Revenue_End_Date__c"] === ''
                       || eventFields["Duration__c"] === null || eventFields["Duration__c"] === ""){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please fill up all the required fields. (Sales Price, Revenue Start Date, Revenue End Date, Duration)");
                    } 
                    
                    if(eventFields["DS_Net_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Net BPs Fee cannot be negative");
                    }
                    
                    if(eventFields["DS_Sub_Manager_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Sub-Manager BPs Fee cannot be negative");
                    }
                    
                    if((eventFields["CurrentYearRevenue_edit__c"] === null || eventFields["CurrentYearRevenue_edit__c"] === '' || eventFields

["CurrentYearRevenue_edit__c"] === 0) && 
                       (eventFields["Year2Revenue_edit__c"] === null || eventFields["Year2Revenue_edit__c"] === '' || eventFields["Year2Revenue_edit__c"] === 0 ) &&
                       (eventFields["Year3Revenue_edit__c"] === null || eventFields["Year3Revenue_edit__c"] === '' || eventFields["Year3Revenue_edit__c"] === 0)){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please allocate the revenue for the Product by clicking on Allocate Revenue button."); 
                    }
                    
                    if((component.get("v.isSegmentWD") === true) && 
                       (component.get("v.selectedWDMarket") === undefined || component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") == null || component.get("v.selectedMerInf") === "" 
                        || component.get("v.selectedWDReg") === undefined || component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "" )){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for Workday Market Segment, Workday Region and Mercer Influence");        
                    }
                    console.log("3rd check");
                    //alert(component.get("v.isSegmentSN"));
                    if((component.get("v.isSegmentSN") === true) && 
                       ( component.get("v.selectedSNMarket") === undefined || component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === 

"" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === "" 
                        ||  component.get("v.selectedSNR") === undefined || component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "")){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for ServiceNow Market Segment, ServiceNow Region and Mercer Influence.");
                    }
                    
                    if($A.get("$Browser.isPhone")){
                        if(component.get("v.colLookups")!=null && component.get("v.colLookup").Id != undefined){
                            eventFields["Project_Manager__c"] = component.get("v.colLookup").Id; 
                        }
                        else{
                        eventFields["Project_Manager__c"] =  null;
                        }
                        if(component.get("v.colLookup1")!=null && component.get("v.colLookup1").Id != undefined){
                           eventFields["Sales_Professional__c"] = component.get("v.colLookup1").Id; 
                        }
                        else{
                        eventFields["Sales_Professional__c"] =  null;
                        }
                        if(component.get("v.dslookup")!=null && component.get("v.dslookup").Id != undefined){
                            eventFields["DS_Sales_Leader__c"] = component.get("v.dslookup").Id; 
                        }
                        else{
                            eventFields["DS_Sales_Leader__cs"] =  null;
                        }
                       
                    }
                    
                    //alert(component.get("v.callOppAction") );
                    if(component.get("v.callOppAction") === true){
                        component.find("editForm").submit(eventFields);
                        component.set("v.submitted", true);
                        
                    }  
                    console.log("Hi there");
                    }                
                    
                    
                    
                });
                $A.enqueueAction(action);
        }
                }
                else{
                   /* if(component.get("v.cyRev") === null || component.get("v.cyRev") == undefined){
                        component.set("v.cyRev",0); 
                    }
                    if(component.get("v.scndRev") === null || component.get("v.scndRev") == undefined){
                        component.set("v.scndRev",0); 
                    }
                    if(component.get("v.thrdRev") === null || component.get("v.thrdRev") == undefined){
                        component.set("v.thrdRev",0); 
                    }*/
                    console.log("2nd check");
                    eventFields["CurrentYearRevenue_edit__c"] = component.get("v.cyRev");
                    eventFields["Year2Revenue_edit__c"] = component.get("v.scndRev");
                    eventFields["Year3Revenue_edit__c"] = component.get("v.thrdRev");
                    eventFields["WD_Market_Segment__c"] = component.get("v.selectedWDMarket");
                    eventFields["Workday_Region__c"] = component.get("v.selectedWDReg");
                    eventFields["ServiceNow_Market_Segment__c"] = component.get("v.selectedSNMarket");
                    eventFields["Mercer_Influence__c"] = component.get("v.selectedMerInf");
                    eventFields["ServiceNow_Region__c"] = component.get("v.selectedSNR");
                    eventFields["Annualized_Revenue_editable__c"] = component.get("v.annualized");
                    eventFields["Assets_Under_Mgmt__c"] = component.get("v.myCurrency");
                    //event.setParam("fields",fields);
                    // alert(component.get("v.cyRev"));
                    //alert(component.get("v.selectedSNMarket"));
                    
                    if(component.get("v.unitPrice") === 0 || component.get("v.unitPrice") === null ||component.get("v.unitPrice") === ''
                       || eventFields["Revenue_Start_Date__c"] === null || eventFields["Revenue_Start_Date__c"] === ''
                       || eventFields["Revenue_End_Date__c"] === null || eventFields["Revenue_End_Date__c"] === ''
                       || eventFields["Duration__c"] === null || eventFields["Duration__c"] === ""){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please fill up all the required fields. (Sales Price, Revenue Start Date, Revenue End Date, Duration)");
                    } 
                    
                    if(eventFields["DS_Net_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Net BPs Fee cannot be negative");
                    }
                    
                    if(eventFields["DS_Sub_Manager_BPs_Fee__c"] < 0){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Sub-Manager BPs Fee cannot be negative");
                    }
                    
                    if((eventFields["CurrentYearRevenue_edit__c"] === null || eventFields["CurrentYearRevenue_edit__c"] === '' || eventFields

["CurrentYearRevenue_edit__c"] === 0) && 
                       (eventFields["Year2Revenue_edit__c"] === null || eventFields["Year2Revenue_edit__c"] === '' || eventFields["Year2Revenue_edit__c"] === 0 ) &&
                       (eventFields["Year3Revenue_edit__c"] === null || eventFields["Year3Revenue_edit__c"] === '' || eventFields["Year3Revenue_edit__c"] === 0)){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please allocate the revenue for the Product by clicking on Allocate Revenue button."); 
                    }
                    
                    if((component.get("v.isSegmentWD") === true) && 
                       (component.get("v.selectedWDMarket") === undefined || component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") == null || component.get("v.selectedMerInf") === "" 
                        || component.get("v.selectedWDReg") === undefined || component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "" )){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for Workday Market Segment, Workday Region and Mercer Influence");        
                    }
                    console.log("3rd check");
                    //alert(component.get("v.isSegmentSN"));
                    if((component.get("v.isSegmentSN") === true) && 
                       ( component.get("v.selectedSNMarket") === undefined || component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === 

"" 
                        || component.get("v.selectedMerInf") === undefined || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === "" 
                        ||  component.get("v.selectedSNR") === undefined || component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "")){
                        component.set("v.callOppAction", false);
                        component.set("v.showError", true);
                        component.set("v.Error", "Please select a value for ServiceNow Market Segment, ServiceNow Region and Mercer Influence.");
                    }
                    
                    if($A.get("$Browser.isPhone")){
                        if(component.get("v.colLookup")!=null && component.get("v.colLookup").Id != undefined){
                            eventFields["Project_Manager__c"] = component.get("v.colLookup").Id; 
                        }else{
                            eventFields["Project_Manager__c"]= null;
                        }
                        if(component.get("v.colLookup1")!=null && component.get("v.colLookup1").Id != undefined){
                            eventFields["Sales_Professional__c"] = component.get("v.colLookup1").Id; 
                        }
                        else{
                        eventFields["Sales_Professional__c"] = null;
                    }
                        if(component.get("v.dslookup")!=null && component.get("v.dslookup").Id != undefined){
                            eventFields["DS_Sales_Leader__c"] = component.get("v.dslookup").Id; 
                        }
                        else{
                            eventFields["DS_Sales_Leader__c"]= null;
                        }
                    }
                 if(component.get("v.callOppAction") === true && component.get("v.submitted")===false){
                        component.find("editForm").submit(eventFields);
                        component.set("v.submitted", true);
                        
                    } 
                }
            }
            
        }); 
        $A.enqueueAction(act);
        
        
    },
    
    handleCancel : function(component, event, helper){
        event.preventDefault();
        if(component.get("v.openedfromVF")){
            if($A.get("$Browser.isPhone")){
                sforce.one.back(true);  
            }
            else{
                location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view');
            }
        } 
        else{
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.oppId"),
                "slideDevName": "detail"
            });
            navEvt.fire();  
        }  
    },
    
    handleOnSuccess : function(component, event, helper) {
        //alert('OLI created');
        component.set("v.submitted", true);
        var record = event.getParam("response");
        
        if(component.get("v.openedfromVF")){
            if($A.get("$Browser.isPhone")){
                alert('Product: '+record.fields.Product2.displayValue + ' saved successfully for Opportunity:' + record.fields.Opportunity.displayValue+'!');
                sforce.one.back(true);  
            }
            else{
                component.set("v.isSuccess",true);
            }
        }
        else{
            var record = event.getParam("response");
            component.find("notificationsLibrary").showToast({
                "title": "Saved",
                "message": "{0} saved for {1}",
                "messageData": [
                    {
                        url: '/' + record.Id,
                        label: record.fields.Product2.displayValue
                    },
                    {
                        url: '/' + record.fields.OpportunityId.value, 
                        label: record.fields.Opportunity.displayValue
                    }
                ]
            });   
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.oppId"),
                "slideDevName": "detail"
            });
            navEvt.fire();  
        } 
        
    },
    
    handleOnError : function(component, event, helper) {
        //component.set("v.submitted",false);
        component.set("v.submitted", true);
        var message = '';
        var errors = event.getParams();
        //console.log(JSON.stringify(errors));
        //alert(JSON.stringify(errors));
        var errormessages = errors.output;
        
        if ($A.util.isEmpty(errormessages.errors) === false) {
            if (errormessages.errors.length > 0) {
                for (var j = 0; errormessages.errors.length > j; j++) {
                    var fielderror = errormessages.errors[j];
                    
                    // message += fielderror.errorCode + ' (' + fielderror.field + ') : ' + fielderror.message;
                    message += fielderror.message;
                }
            }
        }
        if ($A.util.isEmpty(errormessages.fieldErrors) === false) {
            
            var fieldErrorObj = Object.values(errormessages.fieldErrors)[0];
            console.log(JSON.stringify(fieldErrorObj[0]));
            message += fieldErrorObj[0].errorCode + ' (' + fieldErrorObj[0].field + ') : ' + fieldErrorObj[0].message;
            
        }
        component.set("v.showError", true);
        component.set("v.Error",message);
        
    },
    
    showSpinner: function(component, event, helper) {
        if(component.get("v.submitted")){
            component.set("v.Spinner", true); 
        }
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },
    
    netKeyPress : function(component,event,helper){
        //  alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.netBpsErrorText","");
        if(val<0){
            component.set("v.netBpsErrorText","Net BPs Fee cannot be negative");
            $A.util.addClass(component.find("netBpsError"),"show"); 
        }
    },
    
    keyPress : function(component,event,helper){
        // alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.bpsErrorText","");
        if(val<0){
            component.set("v.bpsErrorText","Sub-Manager BPs Fee cannot be negative");
            $A.util.addClass(component.find("bpsError"),"show"); 
        }
    },
    
    closeModal: function(component, event, helper) {
        component.set("v.isSuccess", false);
        if(component.get("v.openedfromVF")){
            location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view'); 
        }
    },
})