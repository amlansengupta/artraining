({
    //Request Id#17448:This method will get invoked once the related component gets loaded and requests server side controller class
    //to refresh the bill rates
	doInit : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.refreshBillRates");
        action.setParams({
            'recordId' : recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.loaded",true);
            if(state == "SUCCESS"){
                component.set("v.pageMessage","Bill Rates refreshed successfully.");
            }else if (state === "ERROR") {
                component.set("v.pageMessage","Could not refresh Bill Rates! Please try after sometime.");
            }
        });
        $A.enqueueAction(action);
	}
})