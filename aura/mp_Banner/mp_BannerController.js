/**
 * Created by abdoundure on 8/14/18.
 */
({
    doInit: function (component, event, helper) {
        helper.getSitePath(component);
        helper.getFeaturedNewsById(component);
    }
})