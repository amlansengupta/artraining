({
    doInit : function(component, event, helper) {
        if(parent.location.href.indexOf("inContextOfRef") > -1){
            var urlStr = parent.location.href;
            var selector = "inContextOfRef";
            var base64Context = urlStr.substring(urlStr.indexOf(selector)+selector.length+1);
            
            if (base64Context.startsWith("1\.")) {
                base64Context = base64Context.substring(2);
            }
            if(base64Context.indexOf("%") > -1){
                base64Context = base64Context.substring(0, base64Context.indexOf("%"));
            }
            if(base64Context.indexOf("&") > -1){
                base64Context = base64Context.substring(0, base64Context.indexOf("&"));
            }
            //alert('Hi'+base64Context);
            console.log("content in URL::"+window.atob(base64Context));
            var addressableContext = JSON.parse(window.atob(base64Context));
            if((addressableContext.attributes.relationshipApiName =="Sales_Professional__r") || (addressableContext.attributes.objectApiName =="Opportunity")){
                var parentOpptyId = addressableContext.attributes.recordId;
                var action = component.get("c.getOpportunityStatus");
                action.setParams({
                    "opportunityId":parentOpptyId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if(state == "SUCCESS"){
                        var isClosedOwn = response.getReturnValue();
                        console.log(isClosedOwn == true);
                        if(isClosedOwn == true){
                            component.set("v.isNotClosedOwnOppty", false);
                            var createRecordEvent = $A.get("e.force:createRecord");
                            createRecordEvent.setParams({
                                "entityApiName": "Sales_Professional__c",
                                "defaultFieldValues": {
                                    'Opportunity__c' : parentOpptyId
                                }
                            });
                            createRecordEvent.fire();
                        }else{
                            component.set("v.isNotClosedOwnOppty", true);
                        }
                    }else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                        
                    }
                });
                
                $A.enqueueAction(action);
            }
        }
        
    },
    navigateToParent: function(component, recId) {
        window.history.back();
    },
})