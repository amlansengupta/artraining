({
    doInit : function(component, event, helper) {
		var oppId = component.get("v.recordId");
        var action = component.get("c.recordTypeCheck");
        action.setParams({
            'oppId' : oppId
    	});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //opp = response.getReturnValue();
                //alert("opp "+ opp);
                component.set("v.flag", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
	}
})