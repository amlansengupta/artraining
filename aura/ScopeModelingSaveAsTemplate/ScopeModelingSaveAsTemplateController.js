({
    //Request Id#17447 : This function gets called when the component gets loaded to create the related scope objects
	doInit : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.createTscope");
        //alert(recordId);
        //var scopeIdList = [];
        //scopeIdList.push(recordId);
        action.setParams({
            'scpId' : recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.loaded",true);
            if(state == "SUCCESS"){
                var message = response.getReturnValue();
                component.set("v.pageMessage",response.getReturnValue());
                //location.replace(window.location.href);
            }else if(state == "ERROR"){
                component.set("v.pageMessage","Could not create scope modeling.Try after sometime.");
                //location.replace(window.location.href);
            }
        });
        $A.enqueueAction(action);
	}
})