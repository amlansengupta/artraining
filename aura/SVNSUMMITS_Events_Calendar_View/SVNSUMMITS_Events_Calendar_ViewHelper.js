// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    // Initialize the calendar events
    initializeCalendar: function(cmp) {
        var sitePath = cmp.get("v.sitePath");
        var action;
        var filterByFollowingTopic = cmp.get("v.filterByFollowingTopic");

        // check if we are filtering by topics I'm following. This comes
        // from the news filter component when the user checks the select
        // box in the UI.
        if(filterByFollowingTopic === true) {
            action = cmp.get("c.getEventsFilteredByFollowing");
        } else {
            action = cmp.get("c.getEvents");
        }

        action.setParams({
            eventListFlag: cmp.get("v.titletext") !== 'Recommended For You',
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            strfilterType: cmp.get("v.filterType"),
            strRecordId: cmp.get("v.topicValue"),
            networkId: '',
            sortBy: cmp.get("v.sortBy"),
            filterByTopic: cmp.get("v.filterByTopic"),
            topicName: cmp.get("v.filterByTopic"),
            filterBySearchTerm: '',
            searchTerm: cmp.get("v.searchstr"),
            filterOn: cmp.get("v.filterOn"),
            fromDate: '',
            toDate: '',
            listViewMode: cmp.get("v.listViewMode")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (cmp.isValid() && state === "SUCCESS") {
                var eventsListWrapper = response.getReturnValue();
                var events=[];
                var appEvent = $A.get("e.c:SVNSUMMITS_Events_Header_Event");
                appEvent.setParams({
                    "totalResults" : eventsListWrapper.totalResults,
                });
                appEvent.fire();

                for (var i=0; i < eventsListWrapper.objEventList.length; i++) {
                    var startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).tz(eventsListWrapper.strTimeZone).format();
                    var endDate = moment(eventsListWrapper.objEventList[i].End_DateTime__c).tz(eventsListWrapper.strTimeZone).format();

                    events.push({
                        'title':eventsListWrapper.objEventList[i].Name,
                    	'allDay':eventsListWrapper.objEventList[i].All_Day_Event__c,
                        'start':startDate,
                        'end':endDate,
                        'Id':eventsListWrapper.objEventList[i].Id,
                        'url':'/event/'+eventsListWrapper.objEventList[i].Id});
                }

                cmp.set('v.eventCalendarValues', events);
                this.initializeCalendarJQ(cmp);
            }

        });
        $A.enqueueAction(action);
    },

    // Initialize the calendar
    initializeCalendarJQ: function(cmp) {
        $('#calendar').fullCalendar( 'destroy' );
        $('#calendar').fullCalendar({
            header: {
                left:'month,agendaWeek,agendaDay',
                center: 'title',
                right:'prev,next'
            },
            events: cmp.get('v.eventCalendarValues'),
            eventClick: function(event) {
                if (event.url) {
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": event.url
                    });
                    urlEvent.fire();
                    return false;
                }
            },
            eventLimit: true,
            views: {
                agenda: {
                    eventLimit: 5
                }
            }
        });
    },

    // Re-Initialize the calendar and refresh on basis of topics selected
    CalendarViewTopicFilter:function(cmp,event) {
        var filterByTopic = event.getParam("filterByTopic");

        var action = cmp.get("c.getEvents");
        action.setParams({
            eventListFlag: cmp.get("v.titletext") !== 'Recommended For You',
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            strfilterType: cmp.get("v.filterType"),
            strRecordId: cmp.get("v.topicValue"),
            networkId: '',
            sortBy: cmp.get("v.sortBy"),
            filterByTopic: filterByTopic,
            topicName: filterByTopic,
            filterBySearchTerm: '',
            searchTerm: cmp.get("v.searchstr"),
            filterOn: cmp.get("v.filterOn"),
            fromDate: null,
            toDate: null,
            listViewMode: cmp.get("v.listViewMode")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (cmp.isValid() && state === "SUCCESS") {
                var eventsListWrapper  = response.getReturnValue();
                var events=[];

                for (var i=0; i < eventsListWrapper.objEventList.length; i++) {
                    var startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format('YYYY-MM-DD');
                    var endDate   = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format('YYYY-MM-DD');

                    events.push({
                        'title':eventsListWrapper.objEventList[i].Name,
                        'start':startDate,
                        'end':endDate,
                        'Id':eventsListWrapper.objEventList[i].Id,
                        'url':'/event/' + eventsListWrapper.objEventList[i].Id
                    });
                }

                cmp.set('v.eventCalendarValues', events);
                this.initializeCalendarJQ(cmp);
            }
        });

        $A.enqueueAction(action);
    },

    debug: function(cmp, msg, variable) {
        var debugMode = cmp.get("v.debugMode");
        if(debugMode) {
            if(msg) {
                console.log(msg);
            }
            if(variable) {
                console.log(variable);
            }
        }
    }
})