({
    onSuccess : function(component, event, helper){
        event.preventDefault();
        var oppId = component.get("v.recordId");
        location.replace("/lightning/r/Opportunity/"+oppId+"/view");
    },
    
    doInit : function(component, event, helper) {
		var oppId = component.get("v.recordId");
        var action = component.get("c.recordTypeCheck");
        action.setParams({
            'oppId' : oppId
    	});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //opp = response.getReturnValue();
                //alert("opp "+ opp);
               // alert(JSON.stringify(response.getReturnValue()));
                component.set("v.flag", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
	}
})