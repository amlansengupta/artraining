/**
 * Created by emaleesoddy on 3/22/17.
 */
({
    getCurrentYear: function(component) {
        var currentYear = new Date().getFullYear();
        component.set("v.currentYear", currentYear);
    }
})