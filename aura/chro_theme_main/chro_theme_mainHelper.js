/**
 * Created by 7Summits on 11/30/17.
 */
({
    
    getUserInfo: function(component) {
        var action = component.get('c.getCurrentUser');
        action.setCallback(this, function(a) {
            component.set('v.currentUser', a.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    getAccountInfo: function(component) {
        var action = component.get('c.getAccountInfo');
        action.setCallback(this, function(a) {
            component.set('v.accountInfo', a.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    toggleSearch: function(component) {
        var search = component.find('navSearch');
        $A.util.toggleClass(search, 'open');
    },

    hideSearch: function(component) {
        var search = component.find('navSearch');
        $A.util.removeClass(search, 'open');
    }

})