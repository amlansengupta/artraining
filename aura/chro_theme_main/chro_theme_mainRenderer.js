/**
 * Created by 7Summits on 11/30/17.
 */
({
    render : function(cmp, helper){
        var ret = this.superRender();
        return ret;
    },

    rerender : function(cmp, helper){
        this.superRerender();
    },

    afterRender: function (component, helper) {
        this.superAfterRender();

        var initSearch = function initSearch(component, helper) {
            // now that we have the parent search component we can reach down to the subcomponent
            // and access the needed elements.
            var resultContainer = searchContainer.getElementsByClassName('result-container')[0];
            var searchButton = searchContainer.getElementsByClassName('search-button')[0];
            var searchField = searchContainer.getElementsByClassName('search-field')[0];

            // user clicks result in search results, close search box
            if (resultContainer !== undefined) {
                resultContainer.addEventListener('click', function (e) {
                    if (e.target) {
                        helper.getParentAnchor(component, e.target);
                    }
                });
            }

            // user clicks the search button, close search box
            if (searchButton !== undefined) {
                searchButton.onclick = function () {
                    helper.hideSearch(component);
                };
            }

            // user presses enter key in search box, close search box
            if (searchField !== undefined) {

                // remove search input autocomplete
                searchField.setAttribute('autocomplete', 'off');

                searchField.onkeypress = function (event) {
                    if (event.which === 13 || event.keyCode === 13) {
                        helper.hideSearch(component);
                    }
                };
            }
        };

        // get search container that is in this component
        var searchContainer = document.getElementById('navSearch');

        // if the nav search container isn't null attach events
        if (searchContainer !== null && searchContainer !== undefined) {
            initSearch(component, helper);
        }


    }

})