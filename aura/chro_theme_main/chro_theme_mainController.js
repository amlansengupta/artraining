/**
 * Created by 7Summits on 11/30/17.
 */
({
    doInit: function(component, event, helper){
        helper.getUserInfo(component);
        helper.getAccountInfo(component);
    },

    toggleSearch: function(component, event, helper) {
        helper.toggleSearch(component);
    },

    hideSearch: function(component, event, helper) {
        helper.hideSearch(component);
    },

    gotoURL: function(cmp, evt) {
        const urlEvent = $A.get('e.force:navigateToURL');
        const url = evt.currentTarget.dataset.url;

        urlEvent.setParams({
            'url': url
        });

        urlEvent.fire();
    }


})