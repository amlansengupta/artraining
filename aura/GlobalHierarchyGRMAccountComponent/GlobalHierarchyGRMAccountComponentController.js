({
    //Request Id#17418:This method gets invoked when the component gets loaded and requests the server to retrieve the running user's
    //profile name for front-end validation
    onload: function(component, event, helper){
        var accID = component.get("v.recordId");
        var action = component.get("c.getProfileName");
        action.setCallback(this, function(response){
            component.set("v.loaded",true);
            if(response.getState() == "SUCCESS"){
                var profileName = response.getReturnValue();
                
                if(profileName == 'MercerForce Lite'){ 
                    component.set("v.pageMessage","The Global Account View is not available to MercerForce Lite users. However, the Global Ultimate Profile (available in the Custom Links section of this page) might provide the information you seek.");
                } 
                else{ 
                    window.open('/apex/Mercer_AccountHierarchy?id='+accID);
                    location.replace("/"+accID);
                }
            }else if(response.getState() == "ERROR"){
                component.set("v.pagaeMessage","Could not find your profile. Please try after sometime.");
            }
        });
        $A.enqueueAction(action);
        
                
                
    }
})