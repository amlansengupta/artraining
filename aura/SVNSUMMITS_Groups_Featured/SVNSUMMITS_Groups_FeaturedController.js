// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    //Method called after scripts loaded
	doInit : function(component, event, helper) {
        
        helper.getFeaturedGroups(component);
        helper.getSitePrefix(component);
        //helper.initializeSlider(component);
        helper.isNicknameDisplayEnabled(component);
        
	},
    
})