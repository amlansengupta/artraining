({
    init : function(component, event, helper) {
        var pageUrl = window.location.href;
        helper.init(component, event, helper);
        helper.getUser(component, event, helper);
    },

    handlePageChange : function(component, event, helper) {
        helper.handlePageChange(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        component.set("v.displayOnboarding", false);
    },
    // if user clicks 'X' on final screen, we also want to check the Onboarding Complete field on their User record
    closeModalFinal: function(component, event, helper) {
        console.log('close modal first');
        component.set("v.displayOnboarding", false);
        helper.closeModalFinal(component, event, helper)
    },
    handleWelcomeClick: function (component, event, heler) {
        component.set("v.page","1");
    },
    handleProfileClick: function (component, event, heler) {
        component.set("v.page","2");
    },
    handleTopicsClick: function (component, event, heler) {
        component.set("v.page","3");
    },
    handleGroupsClick: function (component, event, heler) {
        component.set("v.page","4");
    },
    openWelcome: function (component, event, helper) {
        component.set('v.displayOnboarding', true);
        component.set("v.page","1");
    },
    openProfile: function (component, event, helper) {
        component.set('v.displayOnboarding', true);
        component.set("v.page","2");
    },
    openTopic: function (component, event, helper) {
        component.set('v.displayOnboarding', true);
        component.set("v.page","3");
    },
    openGroup: function (component, event, helper) {
        component.set('v.displayOnboarding', true);
        component.set("v.page","4");
    }

})