({
doInit: function(component, event, helper) {
       // alert('hi');
          component.set("v.flag",true);
        var rId = component.get("v.recordId");
        //alert(rId);
          var action = component.get("c.getRecord");
        action.setParams({
            'recId' : rId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
                var obj = response.getReturnValue();
            	//alert(obj.RecordType.Name);
            if(obj.RecordType.Name == 'Locked'){
                //component.set("v.flag",false);
                component.set("v.flag",true);
            }
        });
        
        $A.enqueueAction(action);
		
	},
    editRecord : function(component, event, helper){
        event.preventDefault();
        helper.showEdit(component);
    },
    
    handleSuccess : function(component, event, helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({"title" : "Success!","message":"The Sales Professional's information is updated","type":"success"});
        toastEvent.fire();
        //helper.showView(component);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "detail"
        });
        navEvt.fire();
        
    },
    
    handleCancel : function(component, event, helper){
        helper.showView(component);
        event.preventDefault();
    }, 
    
    handleOnSubmit: function(component, event, helper){
         event.preventDefault();
         var eventFields = event.getParam("fields");
        component.find('form').submit(eventFields);
       
    }
   
})