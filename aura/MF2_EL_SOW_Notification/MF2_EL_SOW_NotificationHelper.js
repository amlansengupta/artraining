({
	showView : function(component) {
		var editForm = document.getElementById("editForm");
        $A.util.removeClass(editForm, "slds-show");
        $A.util.addClass(editForm, "slds-hide");
        var viewForm = document.getElementById("viewForm");
        $A.util.removeClass(viewForm, "slds-hide");
        $A.util.addClass(viewForm, "slds-show");
        
    },
    
    showEdit : function(component) {
		var editForm = document.getElementById("editForm");
        $A.util.removeClass(editForm, "slds-hide");
        $A.util.addClass(editForm, "slds-show");
        var viewForm = document.getElementById("viewForm");
        $A.util.removeClass(viewForm, "slds-show");
        $A.util.addClass(viewForm, "slds-hide");
          
    }
})