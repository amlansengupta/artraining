({
        helperFun : function(component,event,secId) 
    {
            var cont = component.find(secId);
            for(var cmp in cont) 
            {
                $A.util.toggleClass(cont[cmp], 'slds-show');  
                $A.util.toggleClass(cont[cmp], 'slds-hide');  
            }
    }
})