({
     doInit: function(component, event, helper) {
        
        var recId =component.get("v.recordId");         
        var action = component.get("c.getContact"); 
        action.setParams({
            
            "conid" : recId                
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.contact", response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
         
         var action = component.get("c.isRunningUserInParentHierarchyCon");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnedVal = response.getReturnValue();
                
                component.set("v.isParent",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
     sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');
    }, 
      
    sectionTwo : function(component, event, helper) {
       helper.helperFun(component,event,'articleTwo');
    },
    
     sectionThree : function(component, event, helper) {
       helper.helperFun(component,event,'articleThree');
    },
    
     sectionFour : function(component, event, helper) {
       helper.helperFun(component,event,'articleFour');
    },
    
     sectionFive : function(component, event, helper) {
       helper.helperFun(component,event,'articleFive');
    },
    
     sectionSix : function(component, event, helper) {
       helper.helperFun(component,event,'articleSix');
    },
    
     navigateTo: function(component, recId) {
        var recId  = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
    },
    
    closeModal:function(component,event,helper){    
        var contEdit = component.find('contactEdit');
        var contDisp = component.find('contactDisplay');
        $A.util.removeClass(contDisp,"slds-hide");
        $A.util.addClass(contDisp,"slds-show");
        $A.util.removeClass(contEdit,"slds-show"); 
        $A.util.addClass(contEdit,"slds-hide");
    },
    
     handleSaveContact: function(component, event, helper) {
        var eventFields = event.getParam("fields");
        var fieldSet ={"Id":component.get("{!v.contact.Id}"),
                       "Mac":eventFields["M_A__c"],"Bac":eventFields["Benefits_Advisory__c"],
                       "Hr":eventFields["HR_Transformation__c"],"Cb":eventFields["Core_Brokerage__c"],"DefinedBenefit":eventFields["Defined_Benefit_Pension_Risk__c"],
                       "MobilityTalent":eventFields["Mobility_Talent_IS__c"],"DefinedContribution":eventFields["Defined_Contribution__c"],"NonMedical":eventFields["Non_Medical_Voluntary_Benefits__c"],
                       "EndowmentFoundations":eventFields["Endowment_Foundations_Mgmt__c"],"Private_Exchange__c":eventFields["Private_Exchange__c"],
                       "Executive_Rewards__c":eventFields["Executive_Rewards__c"],"Talent_Strategy__c":eventFields["Talent_Strategy__c"],"Financial_Wellness__c":eventFields["Financial_Wellness__c"],
                        "Wealth_Management__c":eventFields["Wealth_Management__c"], "Global_Benefits__c":eventFields["Global_Benefits__c"], "Workforce_Rewards__c":eventFields["Workforce_Rewards__c"],
                      };
        
        console.log(JSON.stringify(eventFields));
        console.log(fieldSet);
        event.preventDefault();
        var action = component.get("c.saveContact");
        action.setParams({ 
            "cnct": JSON.stringify(fieldSet)
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state == "SUCCESS" || state == "DRAFT") {
                var clonedCnctId = response.getReturnValue();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": clonedCnctId,
                    "slideDevName": "detail"
                });
                navEvt.fire();
            } else if (state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (state === "ERROR") {
                console.log('Problem saving record, error: ' + state);
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    
     EditCompOpen:function(component,event,helper){
        var contEdit = component.find('contactEdit');
        var contDisp = component.find('contactDisplay');
        $A.util.removeClass(contDisp,"slds-show");
        $A.util.addClass(contDisp,"slds-hide");
        $A.util.removeClass(contEdit,"slds-hide"); 
        $A.util.addClass(contEdit,"slds-show");
    }
})