({
	doInit : function(component, event, helper) {
		var action = component.get("c.fetchHomePageAlertsMessages");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.listOfLinks", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
	}
})