/**
 * Created by abdoundure on 10/7/18.
 */
({
    gotoUrl: function(component, event, helper) {
        var url = event.getSource().get("v.value");
        helper.gotoURL(component, url)
    }
})