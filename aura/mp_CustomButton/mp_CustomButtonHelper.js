/**
 * Created by abdoundure on 10/7/18.
 */
({
    gotoURL: function (component, url) {
        // var url = event.currentTarget.dataset.url;
        var action = $A.get('e.force:navigateToURL');
        action.setParams({
            'url': url
        });
        action.fire();
    }
})