({
	doInit : function(component, event, helper) {
       var openedfromVF = component.get("v.openedfromVF");
        //alert('openedfromVF : '+openedfromVF);
        if(!openedfromVF){
        	component.set("v.oppId",component.get("v.recordId"))
        }
        var oppId = component.get("v.oppId");
        //alert('oppId : '+oppId);
        /*var productSearchInitialEVT = $A.get("e.c:MF2_ProductSearchInitialEvent");
            productSearchInitialEVT.setParams({
                "oppId" : oppId
            })
        productSearchInitialEVT.fire();*/
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:MF2_ProductSearch",
            componentAttributes: {
                oppId : oppId,
                openedfromVF : openedfromVF,
            }
        });
        evt.fire()
        
        /*var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/lightning/n/Add_Product?oppId="+oppId,
                
            });
        
        urlEvent.fire();*/
        
    }
})