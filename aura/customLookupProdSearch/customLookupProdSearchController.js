({
    doInit : function(component,event,helper){
       /* var objectapiname = component.get("v.objectAPIName").toLowerCase();
        var val=component.get("v.val");
        alert('**'+val);
        if(val !=null && val != '' && val != undefined){
            var action = component.get("c.fetchDefaultLookUp");
            action.setParams({
                'recId': val,
                'ObjectName' : component.get("v.objectAPIName")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var storeResponse = response.getReturnValue();
                    
                    component.set("v.selectedRecord", storeResponse);   
                    var lookuppill = component.find("lookup-pill");
                    $A.util.addClass(lookuppill, 'slds-show');
                    $A.util.removeClass(lookuppill, 'slds-hide');
                    
                    var searchRes = component.find("searchRes");
                    $A.util.addClass(searchRes, 'slds-is-close');
                    $A.util.removeClass(searchRes, 'slds-is-open');
                    
                    var lookupField = component.find("lookupField");
                    $A.util.addClass(lookupField, 'slds-hide');
                    $A.util.removeClass(lookupField, 'slds-show'); 
                }
                
            });
            $A.enqueueAction(action);
        }*/
    },
    
    LookupDefaultValue : function(component,event,helper){
        var objectapiname = component.get("v.objectAPIName").toLowerCase();  
        var val;
        //alert(event.getParam("SalesProfId"));
        // alert(event.getParam("ProjManId"));
        // alert(event.getParam("DSSalesLeadId"));
        
        
        if(component.get("v.hiddenlabel") == 'Sales Professional'){
          val =  event.getParam("SalesProfId"); 
        }
        if(component.get("v.hiddenlabel") == 'Project Manager'){
          val =  event.getParam("ProjManId"); 
        }
        if(component.get("v.hiddenlabel") == 'DS'){
          val =  event.getParam("DSSalesLeadId"); 
        }
        
        //alert('New'+val);
        if(val !=null && val != '' && val != undefined){
            var action = component.get("c.fetchDefaultLookUp");
            action.setParams({
                'recId': val,
                'ObjectName' : component.get("v.objectAPIName")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var storeResponse = response.getReturnValue();
                    
                    component.set("v.selectedRecord", storeResponse);   
                    var lookuppill = component.find("lookup-pill");
                    $A.util.addClass(lookuppill, 'slds-show');
                    $A.util.removeClass(lookuppill, 'slds-hide');
                    
                    var searchRes = component.find("searchRes");
                    $A.util.addClass(searchRes, 'slds-is-close');
                    $A.util.removeClass(searchRes, 'slds-is-open');
                    
                    var lookupField = component.find("lookupField");
                    $A.util.addClass(lookupField, 'slds-hide');
                    $A.util.removeClass(lookupField, 'slds-show'); 
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    
    onfocus : function(component,event,helper){
        var objc=component.get("v.objectAPIName").toUpperCase();
        component.set("v.obj",objc);
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        var searchRes = component.find("searchRes");
        $A.util.addClass(searchRes, 'slds-is-open');
        $A.util.removeClass(searchRes, 'slds-is-close');
        
        if(component.get("v.SearchKeyWord") == null || component.get("v.SearchKeyWord") == undefined || component.get("v.SearchKeyWord") == ''){
            var getInputkeyWord = '';
        }
        else{
           var getInputkeyWord = component.get("v.SearchKeyWord");
        }
        helper.searchHelper(component,event,getInputkeyWord,component.get("v.filter"));
    },
    
    onblur : function(component,event,helper){       
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    keyPressController : function(component, event, helper) {
        var objc=component.get("v.objectAPIName").toUpperCase();
        component.set("v.obj",objc);
        var source = event.getSource().getLocalId();
        var filter = component.get("v.filter");
        //alert(source);
        var getInputkeyWord;
        var getModalInputkeyWord;
        if(source === 'input'){
            getInputkeyWord = component.get("v.SearchKeyWord");
            if(getInputkeyWord.length > 0){
                $A.util.addClass(component.find("mySpinner"), "slds-show");
                var searchRes = component.find("searchRes");
                $A.util.addClass(searchRes, 'slds-is-open');
                $A.util.removeClass(searchRes, 'slds-is-close');
                helper.searchHelper(component,event,getInputkeyWord, getModalInputkeyWord,source,filter);
            }
            else{  
                component.set("v.listOfSearchRecords", null ); 
                var searchRes = component.find("searchRes");
                $A.util.addClass(searchRes, 'slds-is-close');
                $A.util.removeClass(searchRes, 'slds-is-open');
            }
        }
        else{
            getModalInputkeyWord = component.get("v.ModalSearchKeyWord");
            if(getModalInputkeyWord.length > 0){
              $A.util.addClass(component.find("mySpinner"), "slds-show"); 
              helper.searchHelper(component,event,getInputkeyWord, getModalInputkeyWord,source,filter);
            }
            else{
              component.set("v.modalSearchRecords", null );
              component.set('v.modalSearchRecordNum', 0);
            }
        }
        
        
        //alert(source);
       
    },
    
    
    clear :function(component,event,helper){
        var lookuppill = component.find("lookup-pill");
        var lookupField = component.find("lookupField"); 
        
        $A.util.addClass(lookuppill, 'slds-hide');
        $A.util.removeClass(lookuppill, 'slds-show');
        
        $A.util.addClass(lookupField, 'slds-show');
        $A.util.removeClass(lookupField, 'slds-hide');
        
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", null); 
    },
    
    
    handleComponentEvent : function(component, event, helper) {
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        component.set("v.selectedRecord" , selectedAccountGetFromEvent);
        
        var pill = component.find("lookup-pill");
        $A.util.addClass(pill, 'slds-show');
        $A.util.removeClass(pill, 'slds-hide');
        
        var res = component.find("searchRes");
        $A.util.addClass(res, 'slds-is-close');
        $A.util.removeClass(res, 'slds-is-open');
        
        var lookupField = component.find("lookupField");
        $A.util.addClass(lookupField, 'slds-hide');
        $A.util.removeClass(lookupField, 'slds-show');  
    },
    
    openSearch : function(component, event, helper){
        /*var modalBody;
        $A.createComponent("c:customLookupModal", 
                           { 
                            listOfSearchRecords : component.get('v.listOfSearchRecords'), 
                            objectApiName : component.get('v.objectAPIName'),
                            selectedRecord : component.getReference('v.selectedRecord'),
                           },
           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   component.find('overlayLib').showCustomModal({
                       header: "Lookup Data",
                       body: modalBody,
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function(){
                           alert('selectedrecords : '+ JSON.stringify(component.get('v.selectedRecord')));
                           
                           if(component.get('v.selectedRecord') != null && component.get('v.selectedRecord') != '' && component.get('v.selectedRecord') != undefined){
                               var pill = component.find("lookup-pill");
                               $A.util.addClass(pill, 'slds-show');
                               $A.util.removeClass(pill, 'slds-hide');
                               
                               var res = component.find("searchRes");
                               $A.util.addClass(res, 'slds-is-close');
                               $A.util.removeClass(res, 'slds-is-open');
                               
                               var lookupField = component.find("lookupField");
                               $A.util.addClass(lookupField, 'slds-hide');
                               $A.util.removeClass(lookupField, 'slds-show'); 
                           }
                       }
                   })
               }
           });*/
        //alert(JSON.stringify(component.get('v.listOfSearchRecords')));
        if(component.get('v.listOfSearchRecords') != null && component.get('v.listOfSearchRecords') != undefined && component.get('v.listOfSearchRecords') != '' ){
            component.set('v.ModalSearchKeyWord', component.get('v.SearchKeyWord'));
            component.set('v.modalSearchRecords', component.get('v.listOfSearchRecords'));
            component.set('v.modalSearchRecordNum', component.get('v.listOfSearchRecords').length);
            component.set('v.SearchKeyWord','');
            component.set("v.listOfSearchRecords", null );
        }
        //alert(component.get('v.modalSearchRecords')[0].Name);
   		component.set("v.modalOpen" , true);
    },
    
    closeModal : function (component, event, helper){
        helper.closeHelper(component, event);
    },
    
    setlookup : function(component, event, helper){
        var objid = event.target.id;
        //alert('objid '+ objid);
        var modalSearchRecords = component.get('v.modalSearchRecords');
        modalSearchRecords.forEach(function myfunc(item){
            if(item.Id === objid){
                component.set('v.selectedRecord',item);
            }
        });
        
        
        var pill = component.find("lookup-pill");
        $A.util.addClass(pill, 'slds-show');
        $A.util.removeClass(pill, 'slds-hide');
        
        var res = component.find("searchRes");
        $A.util.addClass(res, 'slds-is-close');
        $A.util.removeClass(res, 'slds-is-open');
        
        var lookupField = component.find("lookupField");
        $A.util.addClass(lookupField, 'slds-hide');
        $A.util.removeClass(lookupField, 'slds-show');
        
        helper.closeHelper(component,event);
        
    },
    
   
    
})