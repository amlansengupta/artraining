({
	searchHelper : function(component,event,getInputkeyWord,getModalInputkeyWord,source,filter) {
	  
        var action = component.get("c.fetchLookUpValues");
        var keyword;
        if(source === 'input'){
           keyword = getInputkeyWord;
        }
        else{
           keyword = getModalInputkeyWord;
        }  
        
        action.setParams({
            'searchKeyWord': keyword,
            'filter' : filter,
            'ObjectName' : component.get("v.objectAPIName")
          });
       
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(source === 'input'){
                    if (storeResponse.length == 0) {
                        component.set("v.Message", 'No Result Found...');
                    } 
                    else {
                        component.set("v.Message", '');
                    }
                }
                if(source === 'input'){
                    component.set("v.listOfSearchRecords", storeResponse);
                }
                else{
                    component.set("v.modalSearchRecords", storeResponse);
                    component.set("v.modalSearchRecordNum", storeResponse.length);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    closeHelper : function (component, event){
        component.set("v.modalOpen" , false);
        component.set('v.SearchKeyWord','');
        component.set("v.listOfSearchRecords", null );
        component.set('v.ModalSearchKeyWord','');
        component.set("v.modalSearchRecords", null );
        component.set('v.modalSearchRecordNum', 0);
    },
})