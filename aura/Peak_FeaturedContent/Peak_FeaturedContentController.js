/**
 * @File Name     :
 * @Created by    : 7Summits
 * @Description   :
 **/
({

    doInit: function(component, event, helper){
        var isTopicPage = component.get('v.isTopicPage');
        var topicId = component.get('v.topicId');

        // if we are on a topic page need to make sure there is
        // a topic ID set before we do anything. This is just
        // for the page builder page which doesn't pass the topicId.
        if(isTopicPage === false || (isTopicPage === true && topicId)){
            helper.setInitialFeaturedContentConfig(component);
            helper.getFeaturedContent(component);
        }

        // THIS CAN BE REMOVE ONCE IE11 SUPPORT ENDS
        // This was the easier way to account for poor IE11
        // flexbox support. We will add a class to the component
        // and then update the CSS accordingly. When we are able
        // to stop supporting IE11 we can just pull out this code
        // and the supporting CSS in the component.
        var ieVersion = helper.helpers.checkForIE();
        if(ieVersion === 11) {
            component.set('v.isIE', true);
        } else {
            component.set('v.isIE', false);
        }
        // end IE 11 Support

    }

})