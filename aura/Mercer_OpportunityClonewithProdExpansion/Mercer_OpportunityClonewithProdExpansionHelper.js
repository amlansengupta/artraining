({
	fetchOpportunityRecord : function(component, recordId, isCloned, isExpansion){
        var action = component.get("c.getOpportunityRecordDetails"); 
        action.setParams({
            "oppid" : recordId,
            "isCloned" : isCloned,
            "isexpansion" : isExpansion
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert(JSON.stringify(oppDetail));
                var oppDetail = response.getReturnValue();
                component.set("v.opportunity", oppDetail);
                 
                //alert(oppDetail.Name + " - Product Expansion");
                //alert(oppDetail.Buyer__c);
                component.set("v.expOppName", oppDetail.Name );
                component.set("v.ownerFlag",true);
                component.set("v.buyerFlag",true);
                //var type = component.get("v.opportunity");
                //alert(type[0].Type);
                //component.set(type.Type, "Product Expansion");
                window.setTimeout(
                $A.getCallback( function() {
                    component.find("office").set("v.value", oppDetail.Opportunity_Office__c);
                    //alert(oppDetail.Step__c);
                    //component.find("step_default").set("v.value", oppDetail.Step__c);
                    //component.find("stage_default").set("v.value", oppDetail.StageName);
                }));
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchStepOptions : function(component, recordId, isExpansion){
        var action = component.get("c.getstepVal");
        action.setParams({
            "isExpansion" : isExpansion
        });
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var stepValues = response.getReturnValue();
                console.log(JSON.stringify(stepValues));
                for (var loopCounter = 0; loopCounter < stepValues.length; loopCounter++) {
                    opts.push(stepValues[loopCounter]);
                }
                component.set("v.stepOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchCloseStageReasons : function(component, recordId){
        var action = component.get("c.getcloseStageReasonVal");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var reasonValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < reasonValues.length; loopCounter++) {
                    opts.push(reasonValues[loopCounter]);
                }
                component.set("v.closeStageReasons", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchChargeCodeExceptions : function(component, recordId){
        var action = component.get("c.getChargeCodeExepVal");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    /*opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });*/
                    opts.push(allValues[i]);
                }
                component.set("v.chargeCodeExceptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchOfficeOptions : function(component,recordId){
      var action = component.get("c.getOffice");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var reasonValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < reasonValues.length; loopCounter++) {
                    opts.push(reasonValues[loopCounter]);
                }
                component.set("v.officeOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);   
    }
   
})