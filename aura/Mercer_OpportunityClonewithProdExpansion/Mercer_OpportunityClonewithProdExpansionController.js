({
    doInit : function(component, event, helper) {
        if($A.get("$Browser.isPhone")){
            alert('This feature is not available for mobile experience');
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.recordId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else{
            component.set("v.isProductFetched",false);
            var recId =component.get("v.recordId"); 
            var action = component.get("c.getProdStatusCount"); 
            action.setParams({
                "opp" : recId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    var prodDetail = response.getReturnValue();
                    var ProdCount = JSON.parse(prodDetail)[0];
                    if((ProdCount.inactivecount==0||ProdCount.inactivecount>0) && ProdCount.activecount==0){  
                        component.set("v.isProductFetched",false);
                        component.set("v.isNoActive",true);
                }else if(ProdCount.inactivecount>0 && ProdCount.activecount>0 ){  
                    component.set("v.isSomeActive",true);
                }else if(ProdCount.inactivecount==0 && ProdCount.activecount>0){ 
                    if(component.get("v.stgName") =='Closed / Won')
                    {
                        if (ProdCount.projectNotLinkedCount==0){
                            helper.fetchCloseStageReasons(component,recId);
                            helper.fetchOfficeOptions(component,recId);
                            helper.fetchOpportunityRecord(component,recId,false,true);
                            component.set("v.isProductFetched",true);                
                        }else if(ProdCount.projectNotLinkedCount>0){
                            component.set("v.projectNotLinkedCount",true);
                        }
                    }
                    else{
                        component.set("v.NotClosedWon",true);
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            }
        });
            
            
            var Oppstageval;
            var action1 = component.get("c.getOptyStage");
            action1.setParams({
                "oppIdVal" : recId
            });
            action1.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    
                    Oppstageval = response.getReturnValue(); 
                    component.set("v.actualOppName",Oppstageval.Name); 
                    component.set("v.stgName",Oppstageval.StageName); 
                    component.set("v.AccountName",Oppstageval.Account.Name);
                    $A.enqueueAction(action);
                }  else if (state === "ERROR") {
                    //alert("ErrorStage");
                }
            });
            $A.enqueueAction(action1);
            
        }   
    },
    
    makeisProductFetchedtrue : function(component, event, helper){
        component.set("v.isProductFetched",true);
        component.set("v.projectNotLinkedCount",false);
        var recId =component.get("v.recordId"); 
        helper.fetchCloseStageReasons(component,recId);
        helper.fetchOfficeOptions(component,recId);
        helper.fetchOpportunityRecord(component,recId,false,true);
    },
    
    onCloneSave : function(component, event, helper){
        event.preventDefault();
         component.set("v.isError",false);
        var flagSave = true;
        var eventFields = event.getParam("fields");
        component.set("v.errMsg",'');
        var byrValue = component.get("v.opportunity").Buyer__c;
        
        if(byrValue==''||byrValue=='undefined'||byrValue==null||(JSON.stringify(byrValue) === JSON.stringify({}))){
            $A.util.removeClass(component.find("Buyr2"), "none");
            flagSave = false;
        } 
        
        var office = component.get("v.selectedOffice");
        if(office=='' || office == 'None' || office =='undefined'){
            $A.util.removeClass(component.find("office2"), "none");  
            flagSave = false;
        }
        
        var owner = component.get("v.opportunity").OwnerId;
        if(owner==''||owner=='undefined'||owner==null||(JSON.stringify(owner) === JSON.stringify({}))){
            $A.util.removeClass(component.find("ownerid"), "none");
            flagSave = false;
        }
         var OppName = component.get("v.expOppName");
        //alert(OppName);
        //alert(OppName.length());
        if(OppName.length>120){
           flagSave = false;
             component.set("v.isError",true);
           component.set("v.errMsg","Expanding this opportunity will exceed the character limit of the opportunity name, please reduce the opportunity name of the parent in order to proceed.");
        }
        
        
        
        var eventFields = event.getParam("fields");
        if(flagSave == true){
            var OwnerId;
            var BuyerId;
            if($A.get("$Browser.isPhone")){
                OwnerId =component.get("v.opportunity").OwnerId.Id;
                BuyerId = component.get("v.opportunity").Buyer__c.Id;
            }
            else{
                OwnerId =component.get("v.opportunity").OwnerId;
                BuyerId = component.get("v.opportunity").Buyer__c;
            }
            $A.util.removeClass(component.find("office2"), "customRequiredOffice"); 
            $A.util.removeClass(component.find("Buyr2"), "customRequired");
            $A.util.removeClass(component.find("ownerid"), "customRequiredOwner");
            var fieldSet = {"Name":component.get("v.expOppName"),
                            "AccountId":component.get("v.opportunity").AccountId,
                            "StageName":component.get("v.stgName"),
                            "Description":eventFields["Description"],
                            "Next_Action__c":eventFields["Next_Action__c"],
                            "Buyer__c":BuyerId,
                            "Opportunity_Office__c":component.get("v.selectedOffice"),
                            "CloseDate":component.get("v.opportunity").CloseDate,
                            "Probability":component.get("v.opportunity").Probability,
                            "OwnerId":OwnerId,
                            "Closed_Stage_Reason_Detail__c":component.get("v.opportunity").Closed_Stage_Reason_Detail__c,
                            "Opportunity_Country__c":component.get("v.opportunity").Opportunity_Country__c,
                            "Type":component.get("v.opportunity").Type,
                            "RecordTypeId":component.get("v.opportunity").RecordTypeId};
            
            console.log('Test'+ JSON.stringify(eventFields));
            console.log(fieldSet);
            
            var action = component.get("c.saveClonedOpportunity");
            var opts = [];
            action.setParams({
                "clonedCopy" : JSON.stringify(fieldSet),
                "origOpportunity" : component.get("v.opportunity"),
                "isExpansion":true
                
            });
             component.set("v.Spinner",true);
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){  
                    component.set("v.Spinner",false);
                    var result = response.getReturnValue();
                    if(result.clonedId!=null){
                        document.location.replace('/'+result.clonedId);
                    }
                    else{
                        component.set("v.isError",true);
                        component.set("v.errMsg",result.errorToShow);
                    }
                    
                }else if (state === "ERROR") {
                    component.set("v.Spinner",false);
                    var errors = response.getError();
                    if (errors) {
                        //alert("record failed successfully");
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            //alert("record failed successfully "+ errors[0].message ); 
                            component.set("v.isError",true);
                            component.set("v.errMsg","Product Expansion failed! Cause: "+errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
            
            
            
        }
    },
    closeWindow : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var recId =component.get("v.recordId"); 
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    close : function(component,event,helper){
        component.set("v.isSomeActive",false);
        component.set("v.isProductFetched",false);
        var recId =component.get("v.recordId"); 
        var action = component.get("c.getProdStatusCount"); 
        action.setParams({
            "opp" : recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {        
                var prodDetail = response.getReturnValue();
                var ProdCount = JSON.parse(prodDetail)[0];
                if(component.get("v.stgName")=='Closed / Won')
                {
                    if(ProdCount.projectNotLinkedCount==0){
                        component.set("v.isProductFetched",true);
                        helper.fetchCloseStageReasons(component,recId);
                        helper.fetchOfficeOptions(component,recId);
                        helper.fetchOpportunityRecord(component,recId,false,true);
                        
                    }else if(ProdCount.projectNotLinkedCount>0){
                        component.set("v.projectNotLinkedCount",true);
                    }
                }
                else{
                    component.set("v.NotClosedWon",true); 
                }
            }
            
        })
        $A.enqueueAction(action);
    }
    
})({
    doInit : function(component, event, helper) {
        if($A.get("$Browser.isPhone")){
            alert('This feature is not available for mobile experience');
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.recordId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else{
            component.set("v.isProductFetched",false);
            var recId =component.get("v.recordId"); 
            var action = component.get("c.getProdStatusCount"); 
            action.setParams({
                "opp" : recId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    var prodDetail = response.getReturnValue();
                    var ProdCount = JSON.parse(prodDetail)[0];
                    if((ProdCount.inactivecount==0||ProdCount.inactivecount>0) && ProdCount.activecount==0){  
                        component.set("v.isProductFetched",false);
                        component.set("v.isNoActive",true);
                }else if(ProdCount.inactivecount>0 && ProdCount.activecount>0 ){  
                    component.set("v.isSomeActive",true);
                }else if(ProdCount.inactivecount==0 && ProdCount.activecount>0){ 
                    if(component.get("v.stgName") =='Closed / Won')
                    {
                        if (ProdCount.projectNotLinkedCount==0){
                            helper.fetchCloseStageReasons(component,recId);
                            helper.fetchOfficeOptions(component,recId);
                            helper.fetchOpportunityRecord(component,recId,false,true);
                            component.set("v.isProductFetched",true);                
                        }else if(ProdCount.projectNotLinkedCount>0){
                            component.set("v.projectNotLinkedCount",true);
                        }
                    }
                    else{
                        component.set("v.NotClosedWon",true);
                    }
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            }
        });
            
            
            var Oppstageval;
            var action1 = component.get("c.getOptyStage");
            action1.setParams({
                "oppIdVal" : recId
            });
            action1.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                
                    
                    Oppstageval = response.getReturnValue(); 
                    component.set("v.actualOppName",Oppstageval.Name); 
                    component.set("v.stgName",Oppstageval.StageName); 
                    component.set("v.AccountName",Oppstageval.Account.Name);
                    $A.enqueueAction(action);
                }  else if (state === "ERROR") {
                    //alert("ErrorStage");
                }
            });
            $A.enqueueAction(action1);
            
        }   
    },
    
    makeisProductFetchedtrue : function(component, event, helper){
        component.set("v.isProductFetched",true);
        component.set("v.projectNotLinkedCount",false);
        var recId =component.get("v.recordId"); 
        helper.fetchCloseStageReasons(component,recId);
        helper.fetchOfficeOptions(component,recId);
        helper.fetchOpportunityRecord(component,recId,false,true);
    },
    
    onCloneSave : function(component, event, helper){
        event.preventDefault();
         component.set("v.isError",false);
        var flagSave = true;
        var eventFields = event.getParam("fields");
        component.set("v.errMsg",'');
        var byrValue = component.get("v.opportunity").Buyer__c;
        
        if(byrValue==''||byrValue=='undefined'||byrValue==null||(JSON.stringify(byrValue) === JSON.stringify({}))){
            $A.util.removeClass(component.find("Buyr2"), "none");
            flagSave = false;
        } 
        
        var office = component.get("v.selectedOffice");
        if(office=='' || office == 'None' || office =='undefined'){
            $A.util.removeClass(component.find("office2"), "none");  
            flagSave = false;
        }
        
        var owner = component.get("v.opportunity").OwnerId;
        if(owner==''||owner=='undefined'||owner==null||(JSON.stringify(owner) === JSON.stringify({}))){
            $A.util.removeClass(component.find("ownerid"), "none");
            flagSave = false;
        }
         var OppName = component.get("v.expOppName");
        //alert(OppName);
        //alert(OppName.length());
        if(OppName.length>120){
           flagSave = false;
             component.set("v.isError",true);
           component.set("v.errMsg","Expanding this opportunity will exceed the character limit of the opportunity name, please reduce the opportunity name of the parent in order to proceeds");
        }
        
        
        
        var eventFields = event.getParam("fields");
        if(flagSave == true){
            var OwnerId;
            var BuyerId;
            if($A.get("$Browser.isPhone")){
                OwnerId =component.get("v.opportunity").OwnerId.Id;
                BuyerId = component.get("v.opportunity").Buyer__c.Id;
            }
            else{
                OwnerId =component.get("v.opportunity").OwnerId;
                BuyerId = component.get("v.opportunity").Buyer__c;
            }
            $A.util.removeClass(component.find("office2"), "customRequiredOffice"); 
            $A.util.removeClass(component.find("Buyr2"), "customRequired");
            $A.util.removeClass(component.find("ownerid"), "customRequiredOwner");
            var fieldSet = {"Name":component.get("v.expOppName"),
                            "AccountId":component.get("v.opportunity").AccountId,
                            "StageName":component.get("v.stgName"),
                            "Description":eventFields["Description"],
                            "Next_Action__c":eventFields["Next_Action__c"],
                            "Buyer__c":BuyerId,
                            "Opportunity_Office__c":component.get("v.selectedOffice"),
                            "CloseDate":component.get("v.opportunity").CloseDate,
                            "Probability":component.get("v.opportunity").Probability,
                            "OwnerId":OwnerId,
                            "Closed_Stage_Reason_Detail__c":component.get("v.opportunity").Closed_Stage_Reason_Detail__c,
                            "Opportunity_Country__c":component.get("v.opportunity").Opportunity_Country__c,
                            "Type":component.get("v.opportunity").Type,
                            "RecordTypeId":component.get("v.opportunity").RecordTypeId};
            
            console.log('Test'+ JSON.stringify(eventFields));
            console.log(fieldSet);
            
            var action = component.get("c.saveClonedOpportunity");
            var opts = [];
            action.setParams({
                "clonedCopy" : JSON.stringify(fieldSet),
                "origOpportunity" : component.get("v.opportunity"),
                "isExpansion":true
                
            });
             component.set("v.Spinner",true);
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){  
                    component.set("v.Spinner",false);
                    var result = response.getReturnValue();
                    if(result.clonedId!=null){
                        document.location.replace('/'+result.clonedId);
                    }
                    else{
                        component.set("v.isError",true);
                        component.set("v.errMsg",result.errorToShow);
                    }
                    
                }else if (state === "ERROR") {
                    component.set("v.Spinner",false);
                    var errors = response.getError();
                    if (errors) {
                        //alert("record failed successfully");
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            //alert("record failed successfully "+ errors[0].message ); 
                            component.set("v.isError",true);
                            component.set("v.errMsg","Product Expansion failed! Cause: "+errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
            
            
            
        }
    },
    closeWindow : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var recId =component.get("v.recordId"); 
        navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    close : function(component,event,helper){
        component.set("v.isSomeActive",false);
        component.set("v.isProductFetched",false);
        var recId =component.get("v.recordId"); 
        var action = component.get("c.getProdStatusCount"); 
        action.setParams({
            "opp" : recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {        
                var prodDetail = response.getReturnValue();
                var ProdCount = JSON.parse(prodDetail)[0];
                if(component.get("v.stgName")=='Closed / Won')
                {
                    if(ProdCount.projectNotLinkedCount==0){
                        component.set("v.isProductFetched",true);
                        helper.fetchCloseStageReasons(component,recId);
                        helper.fetchOfficeOptions(component,recId);
                        helper.fetchOpportunityRecord(component,recId,false,true);
                        
                    }else if(ProdCount.projectNotLinkedCount>0){
                        component.set("v.projectNotLinkedCount",true);
                    }
                }
                else{
                    component.set("v.NotClosedWon",true); 
                }
            }
            
        })
        $A.enqueueAction(action);
    }
    
})