// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    // Method called on scripts load
	doInit : function(component, event, helper) {
        console.log("doing init for groups_Filters");
        helper.getSitePrefix(component);
	},

    // Method to sort groups
    sortByMyGroups : function(component, event, helper) {

        var cmpTarget = component.find('checkImg');

        var appEvent = $A.get("e.c:SVNSUMMITS_Groups_Filters_Event");

        if(cmpTarget.elements[0].className == 'showImg tickSymbol' || cmpTarget.elements[0].className == 'tickSymbol showImg'){
            $A.util.removeClass(cmpTarget, 'showImg');
        	$A.util.addClass(cmpTarget, 'hideImg');

            component.set("v.isSortBySelected",false);
            appEvent.setParams({
                "searchMyGroups" : ''
            });

            if(component.get("v.isSearchText") == true){
                appEvent.setParams({
                    "searchString" : component.get("v.searchString"),
                });
            }

        }else{

            $A.util.removeClass(cmpTarget, 'hideImg');
        	$A.util.addClass(cmpTarget, 'showImg');

            component.set("v.isSortBySelected",true);

            appEvent.setParams({
                "searchMyGroups" : component.find("myGrps").get("v.value"),
            });

            if(component.get("v.isSearchText") == true){
                appEvent.setParams({
                    "searchString" : component.get("v.searchString"),
                });
            }
        }
        appEvent.fire();

    },

	// Method to select sort by method
	selectSortBy : function(component, event, helper) {
	   var appEvent = $A.get("e.c:SVNSUMMITS_Groups_SortBy_Event");
		appEvent.setParams({
			"sortBy" : component.find("headerSort").get("v.value"),
		});
		appEvent.fire();
	},

})