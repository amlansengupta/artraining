// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit: function (component, event, helper) {
        helper.getAuthors(component);
    },

    setAuthor: function (component, event, helper) {
        if (component.find("filterByAuthor").get("v.value") === '') {
            var cmpTarget = component.find('clearButton');
            $A.util.removeClass(cmpTarget, 'toggleClass1');
            $A.util.addClass(cmpTarget, 'toggleClass');
        }
        helper.sendFilterEvent(component);
    },

    clearAuthor: function (component, event, helper) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Author_Filter_Event");
        appEvent.setParams({
            "filterByAuthor": null,
        });
        appEvent.fire();

        $(".author").each(function () {
            $(this).find('a').remove();
        });

        helper.getAuthors(component);

        var cmpTarget = component.find('clearButton');
        $A.util.removeClass(cmpTarget, 'toggleClass1');
        $A.util.addClass(cmpTarget, 'toggleClass');
    },

    selectAuthor: function (component, event, helper) {
        var cmpTarget = component.find('clearButton');
        $A.util.removeClass(cmpTarget, 'toggleClass');
        $A.util.addClass(cmpTarget, 'toggleClass1');

        if (!component.get('v.showButton')) {
            helper.sendFilterEvent(component);
        }
    }

})