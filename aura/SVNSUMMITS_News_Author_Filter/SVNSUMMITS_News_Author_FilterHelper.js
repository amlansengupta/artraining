// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	sendFilterEvent : function(component) {
		var appEvent = $A.get("e.c:SVNSUMMITS_News_Author_Filter_Event");
		appEvent.setParams({
				"filterByAuthor" : component.find("filterByAuthor").get("v.value"),
		});
		appEvent.fire();
	},

	getAuthors : function(component) {
		var action = component.get("c.getAuthors");

        action.setParams({
            recordType : component.get("v.recordType")
        });

        action.setCallback(this, function(response) {

            var values = response.getReturnValue();
            var valuesTemp = [];
            for (var value in values){
                if(values.hasOwnProperty(value)){
                    valuesTemp.push({
                        key: value,
                        value: values[value]
                    });
                }
            }
            component.set("v.values", valuesTemp);
        });
        $A.enqueueAction(action);
	},

    initializeAuthorDropdown: function(component) {
        try{

            $(".author")
            .addClass("ui fluid search")
            .dropdown({
                placeholder: "Filter by Author"
            });

        }catch(e){this.debug(component,null,e);}
    },
    debug: function(component, msg, variable) {

        var debugMode = component.get("v.debugMode");
        if(debugMode){
            if(msg){
            	console.log(msg);
            }
            if(variable){
            	console.log(variable);
            }
        }
    }
})