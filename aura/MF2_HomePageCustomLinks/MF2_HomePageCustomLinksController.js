({
    doInit : function(component, event, helper) {
        var action = component.get("c.fetchCustomLinks");
        action.setParams({
            'linkLabel' : 'all'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.listOfLinks", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
    },
    
    /*handleClick : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var clickedLink= selectedItem.dataset.variablename;
        var allLink = component.get("v.listOfLinks");
        alert('onclick value '+ clickedLink);
        
        for(var i=0; i=parseInt(allLink.length)-1; i++){
            if(allLink[i].Link__c == clickedLink){
                if(allLink[i].IsActive__c == true){
                    if(allLink[i].Display_In_New_Window__c == true){
                        window.open(clickedLink);
                    }
                    else{
                        window.parent.location = clickedLink;
                    }
                }
                else{
                    alert('The Link is not active');
                }
            }
        }
    }, */
      
    //Function for opening the link in new window.
    openInNewWindow : function(component, event, helper){
        var selectedItem = event.currentTarget;
        var clickedLink= selectedItem.dataset.variablename;
         window.open(clickedLink);
    },
     openInNewWindowReportURL : function(component, event, helper){
        var selectedItem = event.srcElement.href;
        event.preventDefault();
         window.open(selectedItem);
    },
    //Function for opening the link in same window.
    openInSamewindow : function(component, event, helper){
    var selectedItem = event.currentTarget;
        var clickedLink= selectedItem.dataset.variablename;
         location.replace(clickedLink);

    
    },
    })