/**
 * Created by emaleesoddy on 3/22/17.
 */
({
    doInit: function (component, event, helper) {
        helper.getAccountInfo(component);
        helper.getUserInfo(component);
    }
})