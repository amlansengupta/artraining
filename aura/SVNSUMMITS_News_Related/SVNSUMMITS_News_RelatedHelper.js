/**
 * Created by sachinkadian on 10/5/18.
 */
({
    getRelatedNews : function(component){
        var action = component.get("c.getRelatedNewsData");
        action.setParams({ newsId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            let result = response.getReturnValue();
            component.set("v.listOfRelatedNews",result);

        });
        $A.enqueueAction(action);
    }
})