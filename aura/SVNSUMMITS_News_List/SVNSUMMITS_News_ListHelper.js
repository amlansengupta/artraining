// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	getNewsList: function (component) {
		this.debug(component, "Fetch News Called..", null);
		component.set("v.strError", null);
		var self = this;
        var action;
		var filterByFollowingTopic = component.get("v.filterByFollowingTopic");
		var filterByUserFollow = component.get("v.filterByUserFollow");

        // check if we are filtering by topics the user if following.
		// This attribute in configured in the component design.
		if(filterByUserFollow === true){
            action = component.get("c.getNewsByUserFollowedTopics");
            action.setParams({
                numberOfNewsPerPage: component.get("v.numberOfNewsPerPage"),
                networkId: '',
                sortBy: component.get("v.sortBy"),
                recordType: component.get("v.recordType"),
                numDaysToLookBackForTopics: '7'
            });
		} else {

			// check if we are filtering by topics I'm following. This comes
			// from the news filter component when the user checks the select
			// box in the UI.
            if(filterByFollowingTopic === true) {
                action = component.get("c.getNewsFilteredByFollowing");
            }
            // if the user is not filtering by topic call the default APEX function
            else {
                action = component.get("c.getNews");
			}
			
			action.setParams({
                numberOfNews: 0,
                numberOfNewsPerPage: component.get("v.numberOfNewsPerPage"),
                strfilterType: '',
                strRecordId: component.get("v.filterId"),
                networkId: '',
                sortBy: component.get("v.sortBy"),
                filterByTopic: component.get("v.filterByTopic"),
                filterByAuthor: component.get("v.filterByAuthor"),
                topicName: component.get("v.topicName"),
                filterOn: component.get("v.filterOn"),
                searchTerm: component.get("v.searchstr"),
                fromDate: component.get("v.fromDate"),
                toDate: component.get("v.toDate"),
                recordType: component.get("v.recordType")
            });
		}

		// handle the action response
		action.setCallback(this, function (response) {
			var state = response.getState();

			if (component.isValid() && state === "SUCCESS") {
				var newsListWrapper = response.getReturnValue();

				// if we are filtering by topics the user is following
				// we get a list of news wrappers back instead of a single one
				// so we need additional logic in place
				if(filterByUserFollow === true){
					if(Array.isArray(newsListWrapper) && newsListWrapper.length === 2){
						// NOTE: this will return a merged newsListWrapper but some of existing attributes will be removed
						// because they no longer make sense when the objects are merged. For example since we are merging
						// two objects we now lose the ability to paginate so those attributes that control that are removed.
						var mergedNewsWrapper = self.mergeNewsListWrapperFilterByUserTopic(newsListWrapper[0], newsListWrapper[1]);
                        mergedNewsWrapper = self.formatNewsListWrapper(mergedNewsWrapper);
                        self.handleGetNewsSuccess(mergedNewsWrapper, component, self);
					}
				} else {
                    newsListWrapper = self.formatNewsListWrapper(newsListWrapper);
                    self.handleGetNewsSuccess(newsListWrapper, component, self);
				}
			}
		});

		$A.enqueueAction(action);
	},

    /**
	 * Set component attributes based on the newsListWrapper, fire
	 * an needed events and check for internal object error messages.
	 *
     * @param newsListWrapper - object containing news data
     * @param component
     */
	handleGetNewsSuccess: function(newsListWrapper, component){

        //updated to catch exception if any occurred in apex
        if (newsListWrapper.field !== '' && newsListWrapper.field !== null) {
            if (newsListWrapper.field === 'Date') {
                if (newsListWrapper.errorMsg !== '' && newsListWrapper.errorMsg !== null) {
                    if (newsListWrapper.errorMsg === 'List index out of bounds: 1') {
                        component.set("v.strError", 'Invalid Date.');
                    } else {
                        component.set("v.strError", 'Unexpected Error Occured, Please contact your Administrator.');
                    }
                }
            } else {
                if (newsListWrapper.errorMsg !== '' && newsListWrapper.errorMsg !== null)
                    component.set("v.strError", 'Unexpected Error Occured, Please contact your Administrator.');
            }
        }

        //updated code to hide recommended component if no records found
        if (component.get("v.displayMode") === 'Compact' || component.get("v.displayMode") === 'Title Only') {
            if (newsListWrapper.newsList.length === 0) {
                $('.SVNSUMMITS_News_List').hide();
            }
        }

        // fire event to let all other components know the total results have changed
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Header_Event");
        appEvent.setParams({
            "totalResults": newsListWrapper.totalResults
        });
        appEvent.fire();

        // set this components attributes
        component.set("v.totalNews", newsListWrapper.totalResults);
        component.set("v.newsListWrapper", newsListWrapper);
	},

    /**
	 * Takes two newsListWrapper objects and merges their date into
	 * one object. But NOTE some of existing attributes will be removed
	 * because they no longer make sense when the objects are merged.
	 * For example since we are merging two objects we now lose the ability
	 * to paginate so those attributes that control that are removed.
	 *
     * @param newsListWrapperOne - newsListWrapper object
     * @param newsListWrapperTwo - newsListWrapper object
     * @returns {object}
     */
	mergeNewsListWrapperFilterByUserTopic: function(newsListWrapperOne, newsListWrapperTwo){
		var newsListWrapper = {};

		// set the error fields if needed
		newsListWrapper.errorMsg = (newsListWrapperOne.errorMsg !== undefined &&  newsListWrapperOne.errorMsg !== '') ? newsListWrapperOne.errorMsg : newsListWrapperTwo.errorMsg;
        newsListWrapper.field = (newsListWrapperOne.field !== undefined &&  newsListWrapperOne.field !== '') ? newsListWrapperOne.field : newsListWrapperTwo.field;
        newsListWrapper.strTimeZone = newsListWrapperOne.strTimeZone;

		// merge the news lists into one
        newsListWrapper.newsList = newsListWrapperOne.newsList.concat(newsListWrapperTwo.newsList);

        // if the newList is large than the number of items we want to display slice it
        if(newsListWrapper.newsList.length > newsListWrapperOne.listSizeValue) {
            newsListWrapper.newsList = newsListWrapper.newsList.slice(0, newsListWrapperOne.listSizeValue);
		}

		// merge the each wrappers mapping to comments
        newsListWrapper.newsToCommentCountMap = $.extend({}, newsListWrapperOne.newsToCommentCountMap, newsListWrapperTwo.newsToCommentCountMap);

        // merge the each wrappers mapping to topics
        newsListWrapper.newsToTopicsMap = $.extend({}, newsListWrapperOne.newsToTopicsMap, newsListWrapperTwo.newsToTopicsMap);

        // merge the each wrappers mapping to topic names
        newsListWrapper.topicNameToId = $.extend({}, newsListWrapperOne.topicNameToId, newsListWrapperTwo.topicNameToId);

        // set the total results size to the length of the newslist
        newsListWrapper.totalResults = newsListWrapper.newsList;

        return newsListWrapper;
	},

    /**
	 * Given a newsListWrapper object we will update the newsList inside it to contain topic(s)
	 * that are associated with that record.
     * @param newsListWrapper
     * @returns {*}
     */
    formatNewsListWrapper: function(newsListWrapper) {
        for (var i = 0; i < newsListWrapper.newsList.length; i++) {
            newsListWrapper.newsList[i].commentCount = newsListWrapper.newsToCommentCountMap[newsListWrapper.newsList[i].Id];
            newsListWrapper.newsList[i].strTime = moment.utc(newsListWrapper.newsList[i].Publish_DateTime__c).fromNow();
            if (newsListWrapper.newsList[i].Name.length > 70) {
                newsListWrapper.newsList[i].Name = newsListWrapper.newsList[i].Name.substring(0, 70);
            }
            newsListWrapper.newsList[i].topics1 = [];
            newsListWrapper.newsList[i].topics1.push(newsListWrapper.newsToTopicsMap[newsListWrapper.newsList[i].Id]);
            newsListWrapper.newsList[i].topics = [];

			/* Logic for topics will be displayed till 27 characters only */
            if (newsListWrapper.newsList[i].topics1 !== undefined) {
                for (var j = 0; j < newsListWrapper.newsList[i].topics1.length; j++) {
                    if (newsListWrapper.newsList[i].topics1[j] !== undefined) {
                        for (var jj = 0; jj < newsListWrapper.newsList[i].topics1[j].length; jj++) {
                            if (newsListWrapper.newsList[i].topics !== undefined) {
                                newsListWrapper.newsList[i].topics.push(newsListWrapper.newsList[i].topics1[j][jj]);
                            }
                        }
                    }
                }
            }
        }

		return newsListWrapper;
	},

	getNextPage: function (component) {

		this.debug(component, "Next Clicked...", null);
		var action = component.get("c.nextPage");
		action.setParams({
			numberOfNews: 0,
			numberOfNewsPerPage: component.get("v.numberOfNewsPerPage"),
			pageNumber: component.get("v.newsListWrapper").pageNumber,
			strfilterType: '',
			strRecordId: component.get("v.filterId"),
			networkId: '',
			sortBy: component.get("v.sortBy"),
			filterByTopic: component.get("v.filterByTopic"),
			filterByAuthor: component.get("v.filterByAuthor"),
			topicName: component.get("v.topicName"),
			filterOn: component.get("v.filterOn"),
			searchTerm: component.get("v.searchstr"),
			fromDate: component.get("v.fromDate"),
			toDate: component.get("v.toDate"),
            recordType: component.get("v.recordType")
		});

		action.setCallback(this, function (actionResult) {
			var newsListWrapper = actionResult.getReturnValue();
			for (var i = 0; i < newsListWrapper.newsList.length; i++) {
				newsListWrapper.newsList[i].strTime = moment.utc(newsListWrapper.newsList[i].Publish_DateTime__c).fromNow();
				if (newsListWrapper.newsList[i].Name.length > 70) {
					newsListWrapper.newsList[i].Name = newsListWrapper.newsList[i].Name.substring(0, 70);
				}
				newsListWrapper.newsList[i].topics1 = [];
				newsListWrapper.newsList[i].topics1.push(newsListWrapper.newsToTopicsMap[newsListWrapper.newsList[i].Id]);
				newsListWrapper.newsList[i].topics = [];

				/* Logic for topics will be displayed till 27 characters only */
				if (newsListWrapper.newsList[i].topics1 !== undefined) {
					for (var j = 0; j < newsListWrapper.newsList[i].topics1.length; j++) {
						var newsTopicname = '';
						if (newsListWrapper.newsList[i].topics1[j] !== undefined) {
							for (var jj = 0; jj < newsListWrapper.newsList[i].topics1[j].length; jj++) {
								newsTopicname += newsListWrapper.newsList[i].topics1[j][jj].Topic.Name;
								if (newsTopicname.length <= 27 && newsListWrapper.newsList[i].topics !== undefined) {
									newsListWrapper.newsList[i].topics.push(newsListWrapper.newsList[i].topics1[j][jj]);
								}
							}
						}
					}
				}
			}
			component.set("v.newsListWrapper", newsListWrapper);
			var pageNumberComp = this.component.find("pageNumber");
			pageNumberComp.set("v.value", newsListWrapper.pageNumber);

		});

		$A.enqueueAction(action);
	},

	getPreviousPage: function (component) {

		this.debug(component, "Previous Clicked...", null);
		var action = component.get("c.previousPage");
		action.setParams({
			numberOfNews: 0,
			numberOfNewsPerPage: component.get("v.numberOfNewsPerPage"),
			pageNumber: component.get("v.newsListWrapper").pageNumber,
			strfilterType: '',
			strRecordId: component.get("v.filterId"),
			networkId: '',
			sortBy: component.get("v.sortBy"),
			filterByTopic: component.get("v.filterByTopic"),
			filterByAuthor: component.get("v.filterByAuthor"),
			topicName: component.get("v.topicName"),
			filterOn: component.get("v.filterOn"),
			searchTerm: component.get("v.searchstr"),
			fromDate: component.get("v.fromDate"),
			toDate: component.get("v.toDate"),
            recordType: component.get("v.recordType")
		});

		action.setCallback(this, function (actionResult) {
			var newsListWrapper = actionResult.getReturnValue();
			for (var i = 0; i < newsListWrapper.newsList.length; i++) {
				newsListWrapper.newsList[i].strTime = moment.utc(newsListWrapper.newsList[i].Publish_DateTime__c).fromNow();
				if (newsListWrapper.newsList[i].Name.length > 70) {
					newsListWrapper.newsList[i].Name = newsListWrapper.newsList[i].Name.substring(0, 70);
				}
				newsListWrapper.newsList[i].topics1 = [];
				newsListWrapper.newsList[i].topics1.push(newsListWrapper.newsToTopicsMap[newsListWrapper.newsList[i].Id]);
				newsListWrapper.newsList[i].topics = [];

				/* Logic for topics will be displayed till 27 characters only */
				if (newsListWrapper.newsList[i].topics1 !== undefined) {
					for (var j = 0; j < newsListWrapper.newsList[i].topics1.length; j++) {
						var newsTopicname = '';
						if (newsListWrapper.newsList[i].topics1[j] !== undefined) {
							for (var jj = 0; jj < newsListWrapper.newsList[i].topics1[j].length; jj++) {
								newsTopicname += newsListWrapper.newsList[i].topics1[j][jj].Topic.Name;
								if (newsTopicname.length <= 27 && newsListWrapper.newsList[i].topics !== undefined) {
									newsListWrapper.newsList[i].topics.push(newsListWrapper.newsList[i].topics1[j][jj]);
								}
							}
						}
					}
				}
			}
			component.set("v.newsListWrapper", newsListWrapper);
			var pageNumberComp = this.component.find("pageNumber");
			pageNumberComp.set("v.value", newsListWrapper.pageNumber);

		});

		$A.enqueueAction(action);
	},

	get_SitePrefix: function (component) {
		var action = component.get("c.getSitePrefix");
		action.setCallback(this, function (actionResult) {
			var sitePath = actionResult.getReturnValue();
			component.set("v.sitePath", sitePath);
			component.set("v.sitePrefix", sitePath.replace("/s", ""));
		});
		$A.enqueueAction(action);
	},

	isNicknameDisplayEnabled: function (component) {
		var action = component.get("c.isNicknameDisplayEnabled");
		action.setCallback(this, function (actionResult) {
			component.set("v.isNicknameDisplayEnabled", actionResult.getReturnValue());
			this.debug(component, "Nick Name for Community Boolean : ", component.get("v.isNicknameDisplayEnabled"));
		});
		$A.enqueueAction(action);
	},

	sortByShowHide: function () {

		$(window).click(function () {
			$('#dropDwnBtn_menu').hide();
		})

		$('#dropDwnBtn').click(function (even) {
			even.stopPropagation();
		})

		$('#dropDwnBtn').click(function () {
			$('#dropDwnBtn_menu').toggle();
		});

		$('#dropDwnBtn_menu').click(function () {
			if ($('#dropDwnBtn_menu').show()) {
				$('#dropDwnBtn_menu').hide();
			} else {
				$('#dropDwnBtn_menu').show();
			}
		});
	},

    debug: function (component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    }

})