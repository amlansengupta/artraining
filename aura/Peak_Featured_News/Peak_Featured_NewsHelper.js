/**
 * @File Name     :
 * @Created by    : 7Summits
 * @Description   :
 **/
({

    /**
     * Get the featured news content using the news IDs
     * that are configured in the widget design.
     * @param component
     */
    getFeaturedNewsById: function(component) {
        var action = component.get("c.getFeaturedNews");

        action.setParams({
            recordId1: component.get("v.featured1"),
            recordId2: component.get("v.featured2")
        });

        action.setCallback(this, function(a) {
            var results = a.getReturnValue();

            if(results.newsList !== undefined && Array.isArray(results.newsList) && results.newsToTopicsMap !== undefined){
                var sitePrefix = component.get('v.sitePrefix');
                var featuredNewsUpdate = [];

                for(var i=0; i < results.newsList.length; i++){
                    var newsObj = this.helpers.formatNewsObject(results.newsList[i], results.newsToTopicsMap, sitePrefix);
                    featuredNewsUpdate.push(newsObj);
                }

                component.set("v.featuredNews", featuredNewsUpdate);
            }

        });

        $A.enqueueAction(action);
    },

    /**
     * Get the url of the community
     * @param component
     */
    getSitePath: function(component) {
        var action = component.get('c.getSitePrefix');

        action.setCallback(this, function (actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set('v.sitePath', sitePath);
            component.set('v.sitePrefix', sitePath.replace('/s',''));
        });

        $A.enqueueAction(action);
    },

    /**
     * Internal helpers
     */
    helpers: {

        /**
         * Takes a news object and formatted it for display in the component view.
         * @param newsObj - news object
         * @param newsToTopicsMap - object where key is new objects ID, used to
         * map topics to news object
         * @returns {object} - formatted news object
         */
        formatNewsObject: function(newsObj, newsToTopicsMap, sitePrefix){
            var result = {};

            if(newsObj !== undefined){

                // get first topic used
                result.topic = (
                    newsToTopicsMap[newsObj.Id] !== undefined &&
                    Array.isArray(newsToTopicsMap[newsObj.Id]) &&
                    newsToTopicsMap[newsObj.Id][0] !== undefined &&
                    newsToTopicsMap[newsObj.Id][0].Topic !== undefined &&
                    newsToTopicsMap[newsObj.Id][0].Topic.Name !== undefined
                ) ? newsToTopicsMap[newsObj.Id][0].Topic.Name  : '';

                // get title
                result.title = newsObj.Name || '';

                // get summary
                result.summary = newsObj.Summary__c || '';

                // get the featured image
                result.img = (
                    newsObj.Attachments !== undefined &&
                    Array.isArray(newsObj.Attachments) &&
                    newsObj.Attachments[0] !== undefined &&
                    newsObj.Attachments[0].Id !== undefined
                ) ? (sitePrefix + '/servlet/servlet.FileDownload?file=' + newsObj.Attachments[0].Id) : '';

                // get url to news
                result.id = (newsObj.Id || '');
            }

            return result;
        }

    }

});