/**
 * @File Name     :
 * @Created by    : 7Summits
 * @Description   :
 **/
({
    
    doInit: function (component, event, helper) {
        helper.getSitePath(component);
        helper.getFeaturedNewsById(component);
    }

});