// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
		doInit : function(component, event, helper) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_DisplayMode_Event");
        appEvent.setParams({
            "listViewMode" : component.get("v.listViewMode")
        });
        appEvent.fire();
		},

		setListView : function(component, event, helper) {
				component.set("v.listViewMode", "List");

				helper.setListButtonActive(component);
				var appEvent = $A.get("e.c:SVNSUMMITS_News_DisplayMode_Event");
				appEvent.setParams({
						"listViewMode" : "List"
				});
				appEvent.fire();
		},

		setTileView : function(component, event, helper) {
				component.set("v.listViewMode", "Tile");

				helper.setTileButtonActive(component);
				var appEvent = $A.get("e.c:SVNSUMMITS_News_DisplayMode_Event");
				appEvent.setParams({
						"listViewMode" : "Tile"
				});
				appEvent.fire();
		},

		setDisplayMode : function(component, event, helper) {
				var listViewMode = event.getParam("listViewMode");
				component.set("v.listViewMode", listViewMode);
// TODO - THIS NEEDS TO INITIALIZE THE STYLES ON THE BUTTONS
		}
})