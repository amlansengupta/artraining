({
  init : function(component, event, helper) {
        var groupList = component.get('v.groupIds').replace(/\s/g,'').split(',');
        var action = component.get("c.getGroups");
        action.setParams({
            "groupIds": groupList
        });
        console.log("Here", action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnedGroups = response.getReturnValue();
              //  console.log('My group Values', returnedGroups);
                component.set("v.groups", returnedGroups);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getExpert1: function (component, event, helper) {
        var expertOne = component.get('v.expert1Id');
        var action = component.get("c.getMercerExperts");
        action.setParams({
            "UserId": expertOne
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var expertList = response.getReturnValue();
                console.log('test expert one: ', expertList);
                component.set("v.expert1", expertList);
                component.set('v.expert1Joined', expertList.Follow__c);
               // console.log('set values: ', component.get("v.expert1"));
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getExpert2: function (component, event, helper) {
        var expertTwo = component.get('v.expert2Id');
        var action = component.get("c.getMercerExperts");
        action.setParams({
            "UserId": expertTwo
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var expertList = response.getReturnValue();
             //   console.log('test expert two: ', expertList);
                component.set("v.expert2", expertList);
                component.set('v.expert2Joined', expertList.Follow__c);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getExpert3: function (component, event, helper) {
        var expertThree= component.get('v.expert3Id');
        var action = component.get("c.getMercerExperts");
        action.setParams({
            "UserId": expertThree
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var expertList = response.getReturnValue();
             //   console.log('test expert three: ', expertList);
                component.set("v.expert3", expertList);
                component.set('v.expert3Joined', expertList.Follow__c);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getExpert4: function (component, event, helper) {
        var expertFour = component.get('v.expert4Id');
        var action = component.get("c.getMercerExperts");
        action.setParams({
            "UserId": expertFour
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {   
                var expertList = response.getReturnValue();
             //   console.log('test expert four: ', expertList);
                component.set("v.expert4", expertList);
                component.set('v.expert4Joined', expertList.Follow__c);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    followExperts: function (component, event, helper) {
        var action = component.get("c.followMercerExperts");
        action.setParams({
            "expertId1": component.get("v.expert1Id"),
            "expertFollow1": component.get("v.expert1Joined"),
            "expertId2": component.get("v.expert2Id"),
            "expertFollow2": component.get("v.expert2Joined"),
            "expertId3": component.get("v.expert3Id"),
            "expertFollow3": component.get("v.expert3Joined"),
            "expertId4": component.get("v.expert4Id"),
            "expertFollow4": component.get("v.expert4Joined")
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },

    completeOnboarding : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "Close", "slide" : "Group"});
        compEvent.fire();
    },
    goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "3", "slide" : ""});
        compEvent.fire();
    },
    userAddGroup : function(component, event, helper) {
        var buttonName = event.getSource().get("v.name");
        var action = component.get("c.insertGroupMember");
        action.setParams({
            "groupId": buttonName
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('success');
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

        //after the group follow is committed, re initialize the list in the component so the state updates to 'Join' or 'Joined'
        this.init(component, event, helper);

    },

    userRemoveGroup : function(component, event, helper) {
        var buttonName = event.getSource().get("v.name");
        console.log('button label: ' + buttonName);
        var action = component.get("c.insertGroupMember");
        action.setParams({
            "groupId": buttonName,
            "following": false
        });
        console.log(action);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('success');
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        this.init(component, event, helper);
    }
})