({
    init : function(component, event, helper) {
        helper.init(component, event, helper);
        helper.getExpert1(component, event, helper);
        helper.getExpert2(component, event, helper);
        helper.getExpert3(component, event, helper);
        helper.getExpert4(component, event, helper);
    },
    completeOnboarding : function(component, event, helper) {
        helper.completeOnboarding(component, event, helper);
        helper.followExperts(component, event, helper);
    },
    userFollowExpert1 : function (cmp, event, helper) {
        var following = cmp.get("v.expert1Joined");
        if (following){
            cmp.set("v.expert1Joined", false);
        } else {
            cmp.set("v.expert1Joined", true);
        }
    },
    userFollowExpert2 : function (cmp, event, helper) {
        var following = cmp.get("v.expert2Joined");
        if (following){
            cmp.set("v.expert2Joined", false);
        } else {
            cmp.set("v.expert2Joined", true);
        }
    },
    userFollowExpert3 : function (cmp, event, helper) {
        var following = cmp.get("v.expert3Joined");
        if (following){
            cmp.set("v.expert3Joined", false);
        } else {
            cmp.set("v.expert3Joined", true);
        }
    },
    userFollowExpert4 : function (cmp, event, helper) {
        var following = cmp.get("v.expert4Joined");
        if (following){
            cmp.set("v.expert4Joined", false);
        } else {
            cmp.set("v.expert4Joined", true);
        }
    },
    userUnfollowExpert1 : function (component, event, helper) {
        component.set("v.expert1Joined", false);
    },
    userUnfollowExpert2 : function (component, event, helper) {
        component.set("v.expert2Joined", false);
    },
    userUnfollowExpert3 : function (component, event, helper) {
        component.set("v.expert3Joined", false);
    },
    userUnfollowExpert4 : function (component, event, helper) {
        component.set("v.expert4Joined", false);
    },

    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
    },
   userAddGroup : function (component, event, helper) {
        helper.userAddGroup(component, event, helper);
    },
    userRemoveGroup : function (component, event, helper) {
        helper.userRemoveGroup(component, event, helper);
    }
})