// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    // Method called to scroll the calendar scroll
    calenderScroll : function(component,event) {
        // vertical scroll position is the same as the number of pixels that are hidden from
        // view above the scrollable area. Setting the scrollTop positions the vertical scroll of
        // each matched element.
        //if(component.get("v.isRender")) {
        var scrollSize = 400;
        $('#stDate').click(function(e) {
            if($(window).scrollTop() > scrollSize)
            $(window).scrollTop(0);
        });
        $('#endDt').click(function(e){
            if($(window).scrollTop() > scrollSize) {
                $(window).scrollTop(0);
            }
        });
        //}
    },

    setPlaceholderText: function(component){

        // set placeholder text
        var frmLabel = component.get('v.fromDtLabel');
        var fromDate = document.querySelectorAll('.cSVNSUMMITS_Events_Date_Filter .dateInputStdate');
        if(fromDate[0] !== undefined && fromDate[0].placeholder !== undefined){
            fromDate[0].placeholder = frmLabel;
        }

        // set placeholder text
        var toLabel = component.get('v.toDtLabel');
        var toDate = document.querySelectorAll('.cSVNSUMMITS_Events_Date_Filter .dateInputEnddate');
        if(toDate[0] !== undefined && toDate[0].placeholder !== undefined){
            toDate[0].placeholder = toLabel;
        }
    },

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if(debugMode){
            if(msg){
                console.log(msg);
            }
            if(variable){
                console.log(variable);
            }
        }
    }

})