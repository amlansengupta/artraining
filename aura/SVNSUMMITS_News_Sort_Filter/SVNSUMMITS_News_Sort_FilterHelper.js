// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
		setSitePrefix : function(component) {
				var action = component.get("c.getSitePrefix");
				action.setCallback(this, function(actionResult) {
						var sitePath = actionResult.getReturnValue();
						component.set("v.sitePrefix", sitePath.replace("/s",""));
				});
				$A.enqueueAction(action);
		},

		sortByMostRecent : function(component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_SortBy_Filter_Event");
        appEvent.setParams({
            "sortBy" : "Most Recent"
        });
        appEvent.fire();
    },

    sortByOldestFirst : function(component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_SortBy_Filter_Event");
        appEvent.setParams({
            "sortBy" : "Oldest First"
        });
        appEvent.fire();
    }
})