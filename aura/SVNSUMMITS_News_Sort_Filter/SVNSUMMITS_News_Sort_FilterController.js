// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
		doInit : function(component, event, helper) {
				 helper.setSitePrefix(component);
		},

		setSortOption : function(component, event, helper) {
				if (component.get('v.sortBy') === 'Most Recent') {
					helper.sortByMostRecent(component);
				} else if (component.get('v.sortBy') === 'Oldest First') {
					helper.sortByOldestFirst(component);
				}
		}
})