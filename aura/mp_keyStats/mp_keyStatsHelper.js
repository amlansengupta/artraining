/**
 * Created by abdoundure on 9/24/18.
 */
({
    getStats: function (component, event, helper) {
        var topicId = component.get('v.topicId');
        var action = component.get("c.getFollowersAndPostsForTopic");
        action.setParams({
            "topicId": topicId
        });

        console.log('get stat action', action);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var stats = response.getReturnValue();
                console.log('test stats: ', stats);
                component.set("v.numberOfPost", stats);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})