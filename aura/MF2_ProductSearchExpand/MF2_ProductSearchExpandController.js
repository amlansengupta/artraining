({
    doInit: function(component, event, helper) {
        component.set("v.callOppAction", true);
        component.set("v.showError", false);
        component.set("v.Error", '');
        component.set("v.isSuccess",false);
        var action = component.get("c.fetchDigitalPicklist");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var digitalPKList = response.getReturnValue();
                var wdMarketList = digitalPKList.wdMarketList;
                var snMarketList = digitalPKList.snMarketList;
                var wdRegList = digitalPKList.wdRegList;
                var merInfList = digitalPKList.merInfList;
                var ServiceNowRegList = digitalPKList.serviceNowRegionList;
                /*alert('wdmarket : '+wdMarketList);
                alert('snmarket : '+snMarketList);
                alert('wdregion : '+wdRegList);*/
                
                var wdMarkets = [];
                var snMarkets = [];
                var wdRegions = [];
                var merInfs = [];
                var SnRegion =[];
                for(var i = 0; i<wdMarketList.length; i++ ){
                    var wdMarket = {
                        "label": wdMarketList[i],
                        "value": wdMarketList[i],
                    };
                    wdMarkets.push(wdMarket);
                }
                wdMarkets.unshift('');
                
                for(var i = 0; i<snMarketList.length; i++ ){
                    var snMarket = {
                        "label": snMarketList[i],
                        "value": snMarketList[i],
                    };
                    snMarkets.push(snMarket);
                }
                snMarkets.unshift('');
                
                for(var i = 0; i<wdRegList.length; i++ ){
                    var wdRegion = {
                        "label": wdRegList[i],
                        "value": wdRegList[i],
                    };
                    wdRegions.push(wdRegion);
                }
                wdRegions.unshift('');
                
                for(var i = 0; i<merInfList.length; i++ ){
                    var merInf = {
                        "label": merInfList[i],
                        "value": merInfList[i],
                    };
                    merInfs.push(merInf);
                }
                merInfs.unshift('');
                
                 for(var i = 0; i<ServiceNowRegList.length; i++ ){
                    var SnrL = {
                        "label": ServiceNowRegList[i],
                        "value": ServiceNowRegList[i],
                    };
                    SnRegion.push(SnrL);
                }
                SnRegion.unshift('');
                
                component.set("v.wdMarketList", wdMarkets);
                component.set("v.snMarketList", snMarkets);
                component.set("v.wdRegList", wdRegions);
                component.set("v.merInfList", merInfs);
                component.set("v.ServiceNowRegList", SnRegion);
            }
        	else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.showError", true);
                component.set("v.Error",errors);
            }
        });
        
        $A.enqueueAction(action);  
    },
    
    searchProductExpand : function(component, event, helper){
        var oppId = event.getParam("oppId");
        var pbeID = event.getParam("pbeID");
        var pbeList = event.getParam("searchResultPBE");
        var isExpanded = event.getParam("isExpanded");
        var start = event.getParam("v.start");
        var end = event.getParam("v.end");
        var openedfromVF = event.getParam("openedfromVF");
        
        component.set("v.isExpanded",isExpanded);
        component.set("v.oppId",oppId);
        component.set("v.selectedWDMarket","");
        component.set("v.selectedSNMarket","");
        component.set("v.selectedWDReg","");
        component.set("v.selectedMerInf","");
        component.set("v.cyRev",0);
        component.set("v.scndRev",0);
        component.set("v.thrdRev",0);
        component.set("v.callOppAction", true);
        component.set("v.showError", false);
        component.set("v.Error", "");
        component.set("v.submitSaveMore",false);
        component.set("v.netBpsErrorText", "");
        component.set("v.bpsErrorText","");
        component.set("v.unitPrice","");
        component.set("v.annualized",0);
        component.set("v.myCurrency","");
        component.set("v.openedfromVF",openedfromVF);
        component.set("v.isSuccess",false);
                
        if(isExpanded){
            var pbeExpand;
            var opp = [];
            var action = component.get("c.fetchOpportunity");
            action.setParams({
                'oppId' : oppId,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //opp = response.getReturnValue();
                    //alert("opp "+ opp);
                    component.set("v.opp", response.getReturnValue());
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    component.set("v.showError", true);
                    component.set("v.Error",errors);
                }
            });
            
            $A.enqueueAction(action);
            
            for (var i=0; i< pbeList.length; i++ ){
                if(pbeList[i].pbEntry.Id == pbeID){
                    pbeExpand = (pbeList[i]);
                    break;
                }
            }
            component.set("v.expandedPBERecord",pbeExpand);
            //alert('exrecord :'+pbeExpand.pbEntry.Product2.Name);
            console.log('pbeExpand : '+JSON.stringify(pbeExpand));
            if(component.get("v.expandedPBERecord").pbEntry.Product2.Segment__c == "Workday"){
                component.set("v.isSegmentWD", true);
            }
            else{
                component.set("v.isSegmentWD", false);
            }
            if(component.get("v.expandedPBERecord").pbEntry.Product2.Segment__c == "ServiceNow"){
                component.set("v.isSegmentSN", true);
            }
            else{
                component.set("v.isSegmentSN", false);
            }
            //window.scrollTo('bottom');
            //document.getElementById('addTable').focus();
            //window.scrollTo(0,document.body.scrollHeight);
            //window.scrollTo(0,document.documentElement.scrollHeight);
            //window.scrollBy(0,10000000);
        }
    },
    
    handleOnload : function(component, event, helper) {
        
        jQuery("document").ready(function(){
            $("slds-form_stacked").replaceAll("slds-form_horizontal");
            $("slds-form-element_stacked").replaceAll("slds-form-element_horizontal");
        })
        $A.util.removeClass("lightning-input-field","slds-form-element_horizontal");
        $A.util.removeClass("lightning-input","slds-form-element_horizontal");
        /*$A.util.removeClass(component.find("comission"),"slds-form-element_horizontal");
        $A.util.removeClass(component.find("revstart"),"slds-form-element_horizontal");
        $A.util.removeClass(component.find("revend"),"slds-form-element_horizontal");
        $A.util.removeClass(component.find("replacerev"),"slds-form-element_horizontal");
        $(".slds-form-element_stacked").replaceAll(".slds-form-element_horizontal");*/
    },
    
    handleCancel : function(component, event, helper){
        event.preventDefault();
        // Commented the below part as part of Feedback 2862.
        /*component.set("v.expandedPBERecord",null);
        component.set("v.isExpanded",false);
        component.set("v.submitSaveMore",false);
        var cancelProductExpand = $A.get("e.c:MF2_CancelProductExpandEvent");
        cancelProductExpand.setParams({
            "isExpanded" : component.get("v.isExpanded"),
            "eventSource" : 'cancel',
        })
        cancelProductExpand.fire();*/
        if(component.get("v.openedfromVF")){
            if($A.get("$Browser.isPhone")){
            	sforce.one.back(true);  
            }
            else{
            	location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view');
            }
        } 
        else{
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.oppId"),
                "slideDevName": "detail"
            });
            navEvt.fire();  
        }
    },
    
    saveMore : function(component, event, helper){
    	component.set("v.submitSaveMore",true);
    },
    
    saveOnly : function(component, event, helper){
    	component.set("v.submitSaveMore",false);
    },
    
    handleOnSubmit : function(component, event, helper) {
        event.preventDefault();
        //alert('onsubmit called');
        //component.set("v.submitSaveMore",false);
        component.set("v.callOppAction", true);
        component.set("v.showError", false);
        component.set("v.Error", '');
        
        var thisRecordCode = component.get("v.opp").CurrencyIsoCode;
        var eventFields = event.getParam("fields");
        var resultstring = component.get("v.unitPrice");
        var act = component.get("c.fetchCurrEx");
        act.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var exchangeRates = response.getReturnValue();
                if(exchangeRates !== null || !exchangeRates.isEmpty()){
                    var map = {};
                    var h = new Object(); 
                    if(exchangeRates){
                        for (var i = 0; i < exchangeRates.length; i++) {
                            map[exchangeRates[i].Name] = exchangeRates[i].Conversion_Rate__c;
                        }
                    }
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.showError", true);
                        component.set("v.Error", 'Error in fetching Currency Exchange message: " + errors[0].message');
                        console.log("Error in fetching Currency Exchange message: " + errors[0].message);
                        alert("Error in fetching Currency Exchange message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    alert("Unknown error");
                } 
            }
            
            var unitpriceusd =  resultstring / map[thisRecordCode];
            if(unitpriceusd >= "{!$Label.c.Revenue_Amount}"){
                component.set("v.showError", true);
                component.set("v.Error", "{!$Label.c.Revenue_Amount_Error_Message}");
            }
            else{
                var oppId = component.get("v.oppId");
                var pbEntryRecord = component.get("v.expandedPBERecord");
                var productId = pbEntryRecord.pbEntry.Product2.Id;
                var priceBookId = pbEntryRecord.pbEntry.Id;
                
                //var eventFields = event.getParam("fields");
                eventFields["OpportunityId"] = oppId;
                eventFields["Product2Id"] = productId;
                eventFields["PricebookEntryId"] = priceBookId;
                eventFields["UnitPrice"] = component.get("v.unitPrice");
                eventFields["Annualized_Revenue_editable__c"] = component.get("v.annualized");
                
                var cyRev = component.get("v.cyRev");
                var scndRev = component.get("v.scndRev");
                var thrdRev = component.get("v.thrdRev");
                if((cyRev === null || cyRev === '' || cyRev === 0) && 
                   (scndRev === null || scndRev === '' || scndRev === 0 ) &&
                   (thrdRev === null || thrdRev === '' || thrdRev === 0)){
                    helper.allocateRevHelper(component,event);
                }
                
                if(component.get("v.unitPrice") === 0 || component.get("v.unitPrice") === null ||component.get("v.unitPrice") === ''
                   || eventFields["Revenue_Start_Date__c"] === null || eventFields["Revenue_Start_Date__c"] === ''
                   || eventFields["Revenue_End_Date__c"] === null || eventFields["Revenue_End_Date__c"] === ''
                   || eventFields["Duration__c"] === null || eventFields["Duration__c"] === ""){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                  	component.set("v.Error", "Please fill up all the required fields. (Sales Price, Revenue Start Date, Revenue End Date, Duration)");
                }
                
                if(eventFields["DS_Net_BPs_Fee__c"] < 0){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                  	component.set("v.Error", "Net BPs Fee cannot be negative");
                }
                
                if(eventFields["DS_Sub_Manager_BPs_Fee__c"] < 0){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                  	component.set("v.Error", "Sub-Manager BPs Fee cannot be negative");
                }
                
                console.log(component.get("v.cyRev"));
                console.log(component.get("v.scndRev"));
                console.log(component.get("v.thrdRev"));
                
                
                if(component.get("v.cyRev") === null || component.get("v.cyRev") == undefined || component.get("v.cyRev") == ''){
                   component.set("v.cyRev",0); 
                }
                if(component.get("v.scndRev") === null || component.get("v.scndRev") == undefined || component.get("v.scndRev") == ''){
                   component.set("v.scndRev",0); 
                }
                if(component.get("v.thrdRev") === null || component.get("v.thrdRev") == undefined || component.get("v.thrdRev") == ''){
                   component.set("v.thrdRev",0); 
                }
                
                eventFields["CurrentYearRevenue_edit__c"] = component.get("v.cyRev");
                eventFields["Year2Revenue_edit__c"] = component.get("v.scndRev");
                eventFields["Year3Revenue_edit__c"] = component.get("v.thrdRev");
                eventFields["WD_Market_Segment__c"] = component.get("v.selectedWDMarket");
                eventFields["Workday_Region__c"] = component.get("v.selectedWDReg");
                eventFields["ServiceNow_Market_Segment__c"] = component.get("v.selectedSNMarket");
                eventFields["Mercer_Influence__c"] = component.get("v.selectedMerInf");
                eventFields["ServiceNow_Region__c"] = component.get("v.selectedSNR");
                eventFields["Assets_Under_Mgmt__c"] = component.get("v.myCurrency");
                
                 
               
                    if(component.get("v.colLookup").Id != undefined){
                        eventFields["Project_Manager__c"] = component.get("v.colLookup").Id; 
                    }
                 
                    if(component.get("v.colLookup1").Id != undefined){
                        eventFields["Sales_Professional__c"] = component.get("v.colLookup1").Id; 
                    }
                    if(component.get("v.dslookup").Id != undefined){
                        eventFields["DS_Sales_Leader__c"] = component.get("v.dslookup").Id; 
                    }
                
                
                //event.setParam("fields",fields);
                //alert(JSON.stringify(eventFields));
                
                /*if((eventFields["CurrentYearRevenue_edit__c"] === null || eventFields["CurrentYearRevenue_edit__c"] === '' || eventFields["CurrentYearRevenue_edit__c"] === 0) && 
                   (eventFields["Year2Revenue_edit__c"] === null || eventFields["Year2Revenue_edit__c"] === '' || eventFields["Year2Revenue_edit__c"] === 0 ) &&
                   (eventFields["Year3Revenue_edit__c"] === null || eventFields["Year3Revenue_edit__c"] === '' || eventFields["Year3Revenue_edit__c"] === 0)){
                   component.set("v.callOppAction", false);
                   component.set("v.showError", true);
                   component.set("v.Error", "Please allocate the revenue for the Product by clicking on Allocate Revenue button."); 
                }*/
                
                
                
                if((component.get("v.isSegmentWD") === true) && 
                   (component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "" 
                    || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === ""
                    || component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "") ){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Please select a value for Workday Market Segment, Workday Region and Mercer Influence on the Business Specific Details Tab");        
                }
                
                //alert('alert :'+component.get("v.selectedSNR"));
                if((component.get("v.isSegmentSN") === true) && 
                   ( component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === "" 
                    || component.get("v.selectedMerInf") === null || component.get("v.selectedMerInf") === ""
                    || component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "")){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Please select a value for ServiceNow Market Segment and Mercer Influence and ServiceNow Region on the Business Specific Details Tab");
                }
                
                if((component.get("v.isSegmentWD") === true) && 
                   (!(component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === "" )
                    || !(component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "") )){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Can not select values for ServiceNow fields for this Product.");        
                }
                console.log('isServiceNow Segment : '+component.get("v.isSegmentSN"));
                console.log('cond1:'+component.get("v.selectedWDMarket") === null);
               	console.log('cond2:'+component.get("v.selectedWDMarket") === "");
                console.log('cond3:'+component.get("v.selectedWDReg") === null);
                console.log('cond4:'+component.get("v.selectedWDReg") === "");
                if((component.get("v.isSegmentSN") === true) && 
                   (!(component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "") 
                    || !(component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "") )){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Can not select values for WorkDay fields for this Product.");        
                }
                //18340
                /*if((component.get("v.isSegmentWD") === false) && 
                   (!(component.get("v.selectedWDMarket") === null || component.get("v.selectedWDMarket") === "") 
                    || !(component.get("v.selectedWDReg") === null || component.get("v.selectedWDReg") === "") )){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Can not select values for WorkDay fields for this Product.");        
                }
                
                if((component.get("v.isSegmentSN") === false) && 
                   (!(component.get("v.selectedSNMarket") === null || component.get("v.selectedSNMarket") === "" )
                    || !(component.get("v.selectedSNR") === null || component.get("v.selectedSNR") === "") )){
                    component.set("v.callOppAction", false);
                    component.set("v.showError", true);
                    component.set("v.Error", "Can not select values for ServiceNow fields for this Product.");        
                }*/
                
                
                
                //alert(component.get("v.callOppAction") === true);
                if(component.get("v.callOppAction") === true){
                    component.find('form').submit(eventFields);
                    component.set("v.submitted",true);
                    component.set("v.Spinner", true);
                }  
            }
        });
        $A.enqueueAction(act);
        
    },
    
    handleOnSuccess : function(component, event, helper) {
        
        component.set("v.Spinner", false);
        var record = event.getParam("response");
        if(component.get("v.openedfromVF") && !$A.get("$Browser.isPhone")){
            component.set("v.isSuccess",true);  
        }
        else if(component.get("v.openedfromVF") && $A.get("$Browser.isPhone")){
            alert('Product: '+record.fields.Product2.displayValue + ' saved successfully for Opportunity:' + record.fields.Opportunity.displayValue+'!');
            if(component.get("v.submitSaveMore")){
                component.set("v.expandedPBERecord",null);
                component.set("v.isExpanded",false);
                var cancelProductExpand = $A.get("e.c:MF2_CancelProductExpandEvent");
                cancelProductExpand.setParams({
                    "isExpanded" : component.get("v.isExpanded"),
                    "eventSource" : 'savemore',
                })
                cancelProductExpand.fire();
                component.set("v.submitSaveMore",false);
                component.set("v.submitted",false);
                
            }
            else{
                component.set("v.submitted",false);
                sforce.one.back(true);
            }
        }
        else if(!component.get("v.openedfromVF")){
                component.find("notificationsLibrary").showToast({
                    "title": "Saved",
                    "message": "{0} saved for {1}",
                    "messageData": [
                        {
                            url: '/' + record.Id,
                            label: record.fields.Product2.displayValue
                        },
                        {
                            url: '/' + record.fields.OpportunityId.value, 
                            label: record.fields.Opportunity.displayValue
                        }
                    ],
                    "type": "success"
                });
                
                if(component.get("v.submitSaveMore")){
                    component.set("v.expandedPBERecord",null);
                    component.set("v.isExpanded",false);
                    var cancelProductExpand = $A.get("e.c:MF2_CancelProductExpandEvent");
                    cancelProductExpand.setParams({
                        "isExpanded" : component.get("v.isExpanded"),
                        "eventSource" : 'savemore',
                    })
                    cancelProductExpand.fire();
                    component.set("v.submitSaveMore",false);
                }
                else{
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get("v.oppId"),
                        "slideDevName": "detail"
                    });
                    navEvt.fire();  
                }
            }
    },
    
    handleOnError : function(component, event, helper) {
        component.set("v.submitted",false);
        component.set("v.Spinner", false);
        component.set("v.showSpinner", false);
        component.set("v.submitSaveMore",false);
        var message = '';
        var errors = event.getParams();
        console.log(errors);
        var errormessages = errors.output;
        
        if ($A.util.isEmpty(errormessages.errors) === false) {
            if (errormessages.errors.length > 0) {
                for (var j = 0; errormessages.errors.length > j; j++) {
                    var fielderror = errormessages.errors[j];
                    
                    //message += fielderror.errorCode + ' (' + fielderror.field + ') : ' + fielderror.message;
                    message +=fielderror.message;
                }
            }
        }
        
        if ($A.util.isEmpty(errormessages.fieldErrors) === false) {
            var fieldErrorObj = Object.values(errormessages.fieldErrors)[0];
            console.log(JSON.stringify(fieldErrorObj[0]));
            
            //message += fieldErrorObj[0].errorCode + ' (' + fieldErrorObj[0].field + ') : ' + fieldErrorObj[0].message;
            message += fieldErrorObj[0].message;
        }
        component.set("v.showError", true);
        component.set("v.Error",message);
        
    },
    
    handleWDM : function(component, event, helper){
        //alert('selected : '+ event.getParam("value"));
        component.set("v.selectedWDMarket", event.getParam("value"));  
    },
    
    handleSNM : function(component, event, helper){
        component.set("v.selectedSNMarket", event.getParam("value"));   
    },
    
    handleWDR : function(component, event, helper){
        //alert(event.getParam("value"));
        component.set("v.selectedWDReg", event.getParam("value"));  
    },
    
    handleMI : function(component, event, helper){
        component.set("v.selectedMerInf",event.getParam("value"));  
    },
    handleSnR : function(component, event, helper){
        component.set("v.selectedSNR",event.getParam("value"));  
    },
    
    allocateRev : function(component, event, helper){
        event.preventDefault();
        helper.allocateRevHelper(component,event);
    },
    
    /*showSpinner: function(component, event, helper) {
        if(component.get("v.submitted")){
            component.set("v.Spinner", true); 
        }
   },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },*/
    
    netKeyPress : function(component,event,helper){
      //  alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.netBpsErrorText","");
        if(val<0){
           component.set("v.netBpsErrorText","Net BPs Fee cannot be negative");
            $A.util.addClass(component.find("netBpsError"),"show"); 
        }
    },
    
    keyPress : function(component,event,helper){
       // alert(event.getSource().get("v.value"));
        var val = event.getSource().get("v.value");
        component.set("v.bpsErrorText","");
        if(val<0){
           component.set("v.bpsErrorText","Sub-Manager BPs Fee cannot be negative");
            $A.util.addClass(component.find("bpsError"),"show"); 
        }
    },
    
    closeModal: function(component, event, helper) {
        component.set("v.isSuccess", false);
        if(component.get("v.openedfromVF")){
            if(component.get("v.submitSaveMore")){
                component.set("v.expandedPBERecord",null);
                component.set("v.isExpanded",false);
                var cancelProductExpand = $A.get("e.c:MF2_CancelProductExpandEvent");
                cancelProductExpand.setParams({
                    "isExpanded" : component.get("v.isExpanded"),
                    "eventSource" : 'savemore',
                })
                cancelProductExpand.fire();
                component.set("v.submitSaveMore",false);
                component.set("v.submitted",false);
            }
            else{
                component.set("v.submitted",false);
                location.replace('/lightning/r/Opportunity/'+component.get("v.oppId")+'/view'); 
            }
        }
    },
    
    
})