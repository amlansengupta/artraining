({
	allocateRevHelper : function(component, event) {
		var unitprice = component.find("unitprice").get("v.value");
        var revstart = component.find("revstart").get("v.value");
        var revend = component.find("revend").get("v.value");
        
        if(unitprice != null && unitprice != '' &&
           revstart != null && revstart != '' &&
           revend != null && revend != ''){
            var action = component.get("c.reallocateRevenue");
            action.setParams({
                'unitPrice' : unitprice,
                'RevStart' : revstart,
                'RevEnd' : revend,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var allrev = response.getReturnValue(); 
                    var cYRev = allrev.cYRev;
                    component.set("v.cyRev",cYRev);
                    var secYRev = allrev.secYRev;
                    component.set("v.scndRev",secYRev);
                    var thrdYRev = allrev.thrdYRev;
                    component.set("v.thrdRev",thrdYRev);
                    var calcRev = allrev.calcRev;
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    component.set("v.showError", true);
                    component.set("v.Error",errors);
            	}
            });
            
            $A.enqueueAction(action);
            
        }
        else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": "Please put Sales Price, Revenue Start Date and Revenue End Date before clicking on Allocate Revenue button.",
            });
            toastEvent.fire();
        }
	}
})