// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getExcludedIDs : function(excludedIds) {
        var idList = [];
		var splitChar = ',';

		if (excludedIds.indexOf(splitChar) != -1) {
		   var ids = excludedIds.split(splitChar);
		   for (var pos = 0; pos < ids.length; ++pos) {
		       if (ids[pos].length > 0) {
		           idList.push(ids[pos]);
		       }
		   }
		}
		else {
		   idList.push(excludedIds);
		}

        return idList;
    },

    getMembers : function(component,event)   {
        var action = component.get("c.getMembers");
        var excludedIds = component.get("v.excludedMembers");

        if (excludedIds.length > 0) {
            action = component.get("c.getMembersEx");

	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	            excludeList : this.getExcludedIDs(excludedIds),
	        });
        }
        else {
	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	        });
        }

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {

                var membersListWrapper = response.getReturnValue();
                console.log(membersListWrapper);

                for(var i=0;i<membersListWrapper.membersList.length;i++){

                    // Store the number of followers to display on the component
                    membersListWrapper.membersList[i].intNumberOfFollowers = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intNumberOfFollowers;

                    // Store the number of like received to display on the component
                    membersListWrapper.membersList[i].intLikeReceived = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intLikeReceived;

                    // Store the topics name for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopics = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics;

                    // Store the topics name for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopics1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics1;

                    // Store the topics name for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopics2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics2;

                    // Store the topics Id for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopicId = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId;

                    // Store the topics Id for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopicId1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId1;

                    // Store the topics Id for displaying on component
                    membersListWrapper.membersList[i].strKnowledgeTopicId2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId2;
                }

                // Assign the values in wrapper class
                component.set("v.membersListWrapper", membersListWrapper);
            }
        });
		// get the true member count not limited by the StandardSetController
		var countAction = component.get("c.getMemberCount");


		if (excludedIds.length > 0) {
			countAction = component.get("c.getMemberCountEx");
            countAction.setParams({
	            excludeList : this.getExcludedIDs(excludedIds),
            });

		}
        countAction.setCallback(this, function (actionResult){
	        var appEvent = $A.get("e.c:SVNSUMMITS_Members_Header_Event");
	        appEvent.setParams({
	            "totalResults" : actionResult.getReturnValue()
	        });

            appEvent.fire();
        });

		$A.enqueueAction(countAction);

		$A.enqueueAction(action);
	},

    getNextPage: function(component, event) {
        var self = this;
        var action;
        var excludedIds = component.get("v.excludedMembers");

        if (excludedIds.length > 0) {
            action = component.get("c.nextPageEx");

	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            pageNumber : component.get("v.membersListWrapper").pageNumber,
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	            excludeList : this.getExcludedIDs(excludedIds),
	        });
        }
        else {
            action = component.get("c.nextPage");

	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            pageNumber : component.get("v.membersListWrapper").pageNumber,
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	        });
        }

        action.setCallback(component, function(actionResult) {
            var membersListWrapper = actionResult.getReturnValue();

            for(var i=0;i< membersListWrapper.membersList.length;i++){

                // Store the number of followers to display on the component
                membersListWrapper.membersList[i].intNumberOfFollowers = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intNumberOfFollowers;
                // Store the number of like received to display on the component
                membersListWrapper.membersList[i].intLikeReceived = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intLikeReceived;
                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics;
                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics1;

                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics2;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId1;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId2;
            }

            // Assign the values in wrapper class
            component.set("v.membersListWrapper", membersListWrapper);

            var pageNumberComp = component.find("pageNumber");
            pageNumberComp.set("v.value",membersListWrapper.pageNumber);

        });

        $A.enqueueAction(action);
    },

    getPreviousPage: function(component, event) {
        var self = this;
		var action;
        var excludedIds = component.get("v.excludedMembers");

        if (excludedIds.length > 0) {
            action = component.get("c.previousPageEx");

	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            pageNumber : component.get("v.membersListWrapper").pageNumber,
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	            excludeList : this.getExcludedIDs(excludedIds),
	        });
        }
        else {
            action = component.get("c.previousPage");

	        action.setParams({
	            numberOfMembers : component.get("v.numberOfMembers"),
	            pageNumber : component.get("v.membersListWrapper").pageNumber,
	            sortBy  : component.get("v.sortBy"),
	            searchMyMembers : component.get("v.searchMyMembers"),
	            searchString : component.get("v.searchString"),
	        });
		}

        action.setCallback(component, function(actionResult) {
            var membersListWrapper = actionResult.getReturnValue();

            for(var i=0;i< membersListWrapper.membersList.length;i++){

                // Store the number of followers to display on the component
                membersListWrapper.membersList[i].intNumberOfFollowers = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intNumberOfFollowers;

                // Store the number of like received to display on the component
                membersListWrapper.membersList[i].intLikeReceived = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).intLikeReceived;

                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics;

                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics1;

                // Store the topics name for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopics2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopics2;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId1 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId1;

                // Store the topics Id for displaying on component
                membersListWrapper.membersList[i].strKnowledgeTopicId2 = (membersListWrapper.mapUserId_Wrapper[membersListWrapper.membersList[i].Id]).strKnowledgeTopicId2;
            }

            // Assign the values in wrapper class
            component.set("v.membersListWrapper", membersListWrapper);

            var pageNumberComp = component.find("pageNumber");
            pageNumberComp.set("v.value",membersListWrapper.pageNumber);

        });

        $A.enqueueAction(action);
    },

    get_SitePrefix : function(component) {
        var action = component.get("c.getSitePrefix");

        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s",""));
        });

        $A.enqueueAction(action);
    },

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");

        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    },
})