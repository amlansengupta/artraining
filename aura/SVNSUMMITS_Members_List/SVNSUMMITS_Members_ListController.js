// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	doInit : function(component, event, helper) {
        helper.debug(component,"called doInit method after script load...",null);
        helper.get_SitePrefix(component);
		helper.getMembers(component, event);
	},

    getNextPage : function(component, event, helper) {
        helper.debug(component,"nextPage called", null);
        component.set("v.membersListWrapper.membersList",null);
        helper.getNextPage(component, event);
    },

    getPreviousPage : function(component, event, helper) {
        helper.debug(component,"previousPage called", null);
        component.set("v.membersListWrapper.membersList",null);
        helper.getPreviousPage(component, event);
    },

    setSortBy :  function(component, event, helper) {
        helper.debug(component,"sort by method called", null);
        var sortBy = event.getParam("sortBy");
		component.set("v.sortBy", sortBy);
        helper.getMembers(component, event);
    },

    setMembersFilters :  function(component, event, helper) {
        helper.debug(component,"members filter called", null);
        var searchString = event.getParam("searchString");
        var searchMyMembers = event.getParam("searchMyMembers");
        component.set("v.searchString", searchString);
        component.set("v.searchMyMembers", searchMyMembers);
        helper.getMembers(component, event);
    },
})