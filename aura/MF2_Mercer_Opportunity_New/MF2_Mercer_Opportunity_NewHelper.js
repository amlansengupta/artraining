({
    setDefaultValues : function(component, event, opptyWrapper) {
        var officeOptions = [];
        for(var loopCounter = 0; loopCounter < opptyWrapper.officeList.length; loopCounter++){
            var opt;
            if(opptyWrapper.officeList[loopCounter] === opptyWrapper.oppOffice){
                opt = {id:opptyWrapper.officeList[loopCounter],label:opptyWrapper.officeList[loopCounter],selected:true};
                component.set("v.prePopulatedOffice",opptyWrapper.oppOffice);
            }else{
                opt = {id:opptyWrapper.officeList[loopCounter],label:opptyWrapper.officeList[loopCounter],selected:false};
            }
            officeOptions.push(opt);
        }
        component.set("v.officeValues", officeOptions);
        
        var stageOptions = [];
        for(var loopCounter = 0; loopCounter < opptyWrapper.stages.length; loopCounter++){
            var opt;
            if(opptyWrapper.stages[loopCounter] === opptyWrapper.selectedStage){
                opt = {id:opptyWrapper.stages[loopCounter],label:opptyWrapper.stages[loopCounter],selected:true};
                component.set("v.selectedStage",opptyWrapper.selectedStage);
            }else{
                opt = {id:opptyWrapper.stages[loopCounter],label:opptyWrapper.stages[loopCounter],selected:false};
            }
            stageOptions.push(opt);
        }
        component.set("v.options", stageOptions);
    }, 
    onSaveHelper : function(component, event, redirectToProductSearch){
        
        event.preventDefault();
        component.set("v.errors",""); 
        component.set("v.flag",false) ;
        var eventFields = event.getParam("fields");
        var account =eventFields['AccountId'];
        //alert('account' +account);
        component.set("v.officeErrorText","");
        component.set("v.accountErrorText","");
        $A.util.removeClass(component.find("accountError"),"show");
        component.set("v.typeErrorText","");
        component.set("v.currencyErrorText","");
        var isValidationErrorFound = false;
        if(component.get("v.prePopulatedOffice")==='--None--'){
            
            component.set("v.officeErrorText","Please fill in Opportunity Office");
            $A.util.addClass(component.find("officeError"),"show");
            isValidationErrorFound = true;
        }
       /* if(event.getParam("fields")["AccountId"] ==null || event.getParam("fields")["AccountId"] == ""){
            
            component.set("v.accountErrorText","Please fill in Account");
            $A.util.addClass(component.find("accountError"),"show");
            isValidationErrorFound = true;
        }*/
        if(account===null || account===''|| (JSON.stringify(account) === JSON.stringify({})) || (JSON.stringify(account) === '')){
            
            component.set("v.accountErrorText","Please fill in Account");
            $A.util.addClass(component.find("accountError"),"show");
            isValidationErrorFound = true;
        }
        if(event.getParam("fields")["Type"] == ""){
            
            component.set("v.typeErrorText","Please fill in Type");
            $A.util.addClass(component.find("typeError"),"show");
            isValidationErrorFound = true;
        }
        if(event.getParam("fields")["CurrencyIsoCode"] === "--None--" || event.getParam("fields")["CurrencyIsoCode"] ==''){
            
            component.set("v.currencyErrorText","Please fill in Opportunity Currency");
            $A.util.addClass(component.find("currencyError"),"show");
            isValidationErrorFound = true;
        }
        if(!isValidationErrorFound){
            component.set("v.Spinner",true);
            var eventFields = event.getParam("fields");
            component.get("v.prePopulatedOffice");
            component.get("v.selectedStage");
            
            var accid;
            if($A.get("$Browser.isPhone")){
                 accid = component.get("v.accountLookup").Id;  
                }
            else{
                   //alert(component.find("PMMObile").get("v.value"));
                  accid = eventFields["AccountId"];  
                }
            //alert(component.get("v.campaignLookup"));
            var fieldSet = {"Name":eventFields["Name"],
                            //"AccountId":eventFields["AccountId"],
                            "AccountId":accid,
                            "CloseDate":eventFields["CloseDate"],
                            "Type":eventFields["Type"],
                            "Buyer__c":eventFields["Buyer__c"],
                            "CurrencyIsoCode":eventFields["CurrencyIsoCode"],
                            "Opportunity_Name_Local__c":eventFields["Opportunity_Name_Local__c"],
                            "StageName":component.get("v.selectedStage"),
                            "Opportunity_Office__c":component.get("v.prePopulatedOffice"),
                            "IsPrivate":eventFields["IsPrivate"],
                            "CampaignId":component.get("v.campaignLookup")
                           };
            console.log(JSON.stringify(fieldSet));
            
            var action = component.get("c.saveOpp");
            action.setParams({
                "fieldSet" : JSON.stringify(fieldSet),
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    component.set("v.Spinner",false);
                    var result = response.getReturnValue();
                    //component.set("v.opportunityId","")
                    //alert(result.clonedId);
                    if(result!=null){
                        if(!redirectToProductSearch)
                        {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": result,
                                "slideDevName": "detail"
                            });                
                            navEvt.fire();
                        }
                        else
                        {
                            //location.replace("/lightning/n/Add_Product?c__oppId="+result);
                            /*var evt = $A.get("e.force:navigateToComponent");
                            evt.setParams({
                                componentDef : "c:MF2_ProductSearch",
                                componentAttributes: {
                                    oppId : result,
                                    openedfromVF : false,
                                }
                            });
                            evt.fire()*/
                            var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({
                                "url": "/apex/MF2_ProductSearchContainerPage?id="+result
                            });
                            urlEvent.fire();
                        }
                    }
                }else if (state === "ERROR") {
                    component.set("v.Spinner",false);
                    var errors = response.getError();
                    console.log("response helper", JSON.stringify(errors));
                    //alert(JSON.stringify(errors));
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        
                        var message = errors[0].message;
                        if(message.includes("Insufficient access"))
                            message = 'You do not have access on the Opportunity';
                        component.set("v.errors",message); 
                        component.set("v.flag",true) ;
                        
                    }
                }
            });
            
            $A.enqueueAction(action);
        }
    }
})