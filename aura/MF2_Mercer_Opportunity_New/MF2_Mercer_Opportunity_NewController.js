({
    doInit : function(component, event, helper) {
        jQuery("document").ready(function(){
            var action = component.get("c.getAutoPopulateOpportunityFields");
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    var opptyWrapper = response.getReturnValue();
                    component.set("v.options", opptyWrapper.stages);
                    component.set("v.officeValues", opptyWrapper.officeList);
                    helper.setDefaultValues(component, event, opptyWrapper);
                    //$A.get("e.force:refreshView").fire();
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    
                }
            });
            
            $A.enqueueAction(action);
        });
    },
    
    onSave:function(component,event,helper){
        var saveButton = event.getSource();
    	saveButton.set('v.disabled',true);
        if(component.get("v.prodSearchFlag") == false){
            helper.onSaveHelper(component, event, false);
        }
        else
        {
            helper.onSaveHelper(component, event, true);
        }
        //component.set("v.prodSearchFlag",false);
    },
    setProdSearchFlag:function(component,event,helper){
        component.set("v.prodSearchFlag",true);
    },
    closeModal:function(component,event,helper){    
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-hide");
        $A.util.addClass(accDisp,"slds-show");
        $A.util.removeClass(accEdit,"slds-show"); 
        $A.util.addClass(accEdit,"slds-hide");
    },
    navigateTo: function(component, recId) {
        window.history.back();
    },
    handleOnError: function(component, event) {
        var errors = event.getParams();
        console.log("response", JSON.stringify(errors));
    },
    handleOnload:function(component, event, helper){
        if(window.location.search.indexOf("accid") > -1){
            var start = window.location.search.indexOf("accid")+8;
            var end = window.location.search.indexOf("inContextOfRef")-4;
            var accId = window.location.search.substring(start, end);
            component.find("accountLookup").set("v.value", accId);
            
        }else if(window.location.search.indexOf("contId") > -1){
            var start = window.location.search.indexOf("contId")+8;
            var end = window.location.search.indexOf("inContextOfRef")-4;
            var contId = window.location.search.substring(start, end);
            component.find("buyerLookup").set("v.value", contId);
            
        }else if(parent.location.href.indexOf("inContextOfRef") > -1){
            var urlStr = parent.location.href;
            var selector = "inContextOfRef";
            var base64Context = urlStr.substring(urlStr.indexOf(selector)+selector.length+1);
            
            if (base64Context.startsWith("1\.")) {
                base64Context = base64Context.substring(2);
            }
            if(base64Context.indexOf("%") > -1){
                base64Context = base64Context.substring(0, base64Context.indexOf("%"));
            }
            if(base64Context.indexOf("&") > -1){
                base64Context = base64Context.substring(0, base64Context.indexOf("&"));
            }
            //alert('Hi'+base64Context);
            console.log(window.atob(base64Context));
            var addressableContext = JSON.parse(window.atob(base64Context));
            //alert(addressableContext.attributes.objectApiName);
            if(addressableContext.attributes.objectApiName =="Account"){
            	component.find("accountLookup").set("v.value", addressableContext.attributes.recordId);
            }else if(addressableContext.attributes.objectApiName =="Contact"){
                component.find("buyerLookup").set("v.value", addressableContext.attributes.recordId);
            }
            
            else if(addressableContext.attributes.objectApiName =="Campaign"){                
               component.set("v.campaignLookup", addressableContext.attributes.recordId);
            }
        }
    }
    
    
})