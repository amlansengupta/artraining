({
    doInit: function(component, event, helper){
        //alert('fieldflag :'+component.get("v.FieldFlag"));
        if(component.get("v.FieldFlag").endsWith("__C")){
            if(component.get("v.FieldFlag") === 'COLLEAGUE__C'){
               var iconobj = ('standard:avatar').toLowerCase();
            }
            else{
                var iconobj = ('custom:'.concat(objectapiname)).toLowerCase();
            }
            component.set("v.IconName", iconobj); 
        }
        else{
          var iconobj = ('standard:'.concat(component.get("v.FieldFlag"))).toLowerCase();
          component.set("v.IconName", iconobj);  
        }
        
        
        /*if(component.get("v.oRecord").Id == '0010m00000Mn4qfAAB'){
            alert ('Icon name : '+ component.get("v.IconName")); 
        }*/
        
        if(component.get("v.FieldFlag") === 'ACCOUNT'){
            component.set("v.objectAcc", true);
        } 
        
        if(component.get("v.FieldFlag") === 'COLLEAGUE__C'){
            component.set("v.objectCol", true);
        } 
        
        if(component.get("v.oRecord").Email_Address__c != null && component.get("v.oRecord").Email_Address__c != ''){
           component.set("v.emailAddressPresent", true); 
            
        }
        //alert('text-overflow:ellipsis');
        if(component.get("v.oRecord").Account_Name_Local__c == '' 
           || component.get("v.oRecord").Account_Name_Local__c == null||component.get("v.oRecord").Account_Name_Local__c == 'undefined'){
            component.set("v.localNamePresent", false);
        }
        else{
            component.set("v.localNamePresent", true);
        }
    },
    
    selectRecord : function(component, event, helper){      
       
    //  get the selected record from list  
      var getSelectRecord = component.get("v.oRecord");
       //alert("US Record"+getSelectRecord.Name+" "+getSelectRecord.Id);
    // call the event   
      var compEvent = component.getEvent("oSelectedRecordEvent");
    // set the Selected sObject Record to the event attribute.  
         compEvent.setParams({"recordByEvent" : getSelectRecord });  
    // fire the event  
         compEvent.fire();
    },
    
    openPop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        
    },
})