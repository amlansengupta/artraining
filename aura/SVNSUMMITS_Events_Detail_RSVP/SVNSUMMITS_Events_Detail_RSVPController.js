// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    setSitePrefix : function(component, event, helper) {
        helper.get_SitePrefix(component);
        helper.getRSVPMember(component);
        helper.debug(component,"RSVP Called",null);
    },

    fetchEventRecord : function(component, event, helper) {
        var action = component.get("c.getEventRecord");

        action.setParams({
            eventRecordId :component.get("v.recordId"),
        });
        var eventsListWrapper;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                eventsListWrapper  = response.getReturnValue();

                for(var i=0;i< eventsListWrapper.objEventList.length;i++) {
                    var startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                    var startTime = moment(startDate).toDate();

                    var endDate = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                    var endTime = '' ;
                    if (endDate.trim() !== '' && endDate !== undefined) {
                        endTime = moment(endDate).toDate();
                    }

                    var diffDays = Math.round(Math.abs((endTime.getTime() - startTime.getTime())/(24*60*60*1000)));
                    eventsListWrapper.objEventList[i].daysOfMultiDaysEvent = diffDays;

                    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
                    eventsListWrapper.objEventList[i].strDay = moment(startTime).format('ddd');

                    eventsListWrapper.objEventList[i].strMonth = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("MMM");
                    eventsListWrapper.objEventList[i].intDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("DD");
                    eventsListWrapper.objEventList[i].strMinute = moment(startTime).format('HH:mm A');
                    eventsListWrapper.objEventList[i].strEndMinute = moment(endTime).format('HH:mm A');
                    eventsListWrapper.objEventList[i].topics = [];
                    eventsListWrapper.objEventList[i].topics.push(eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);
                    var today = new Date();
                    var td = moment(today).format('YYYY-MM-DD HH:mm:ss');

                    if (endDate.trim() !== '' && endDate !== undefined) {
                        if (endDate < td) {
                            component.set("v.passedEvent",false);
                        }
                    }
                }

                component.set("v.wrappedEventsObj", eventsListWrapper);
            }
        });
        $A.enqueueAction(action);
        var action2 = component.get("c.checkRSVPevents");
        action2.setParams({
            EventId :component.get('v.eventRecordId')
        });
        action2.setCallback(this,function(response){
            component.set('v.isPresentRSVP', response.getReturnValue());
        });
        $A.enqueueAction(action2);
    },
    yesClicked : function(component, event, helper) {

        var cmpTarget = component.find('yesCalled');
        $A.util.removeClass(cmpTarget, 'normalState');
        $A.util.addClass(cmpTarget, 'activeState');
        $A.util.addClass(cmpTarget, 'checked');

        var cmpTargetNoCalled = component.find('noCalled');
        $A.util.addClass(cmpTargetNoCalled, 'normalState');
        $A.util.removeClass(cmpTargetNoCalled, 'activeState');
        $A.util.removeClass(cmpTargetNoCalled, 'checked');
        var action= component.get("c.createRSVPevents");
        action.setParams({
            EventName :component.get('v.recordId'),
            response: 'Yes'
        });
        $A.enqueueAction(action);
        helper.getRSVPMember(component);

    },

    noClicked : function(component, event, helper) {
        component.set('v.isDisabled', true);
        var cmpTarget = component.find('yesCalled');
        $A.util.addClass(cmpTarget, 'normalState');
        $A.util.removeClass(cmpTarget, 'activeState');
        $A.util.removeClass(cmpTarget, 'checked');

        var cmpTarget1 = component.find('noCalled');
        $A.util.removeClass(cmpTarget1, 'normalState');
        $A.util.addClass(cmpTarget1, 'activeState');
        $A.util.addClass(cmpTarget1, 'checked');
        var action= component.get("c.deleteRSVPevents");
        action.setParams({
            EventId :component.get('v.recordId')
        });
        $A.enqueueAction(action);
        helper.getRSVPMember(component);
    },
})