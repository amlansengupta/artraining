// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    get_SitePrefix : function(component) {
    	var action = component.get("c.getSitePrefix");

        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePrefix", sitePath.replace("/s",""));

		});

        $A.enqueueAction(action);
    },

    getRSVPMember : function(component) {
    	var action = component.get("c.getRSVPMemberAttendes");

    	action.setParams({
            EventName: component.get("v.recordId")
        });

    	action.setCallback(this, function(actionResult) {
            var isYesEnable = actionResult.getReturnValue();
            component.set("v.isPresentRSVP", isYesEnable);
		});

        $A.enqueueAction(action);
	},

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");

        if(debugMode) {
            if (msg) {
            	console.log(msg);
            }
            if (variable) {
            	console.log(variable);
            }
        }
    }
})