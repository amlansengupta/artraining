({
    doinit: function(component,event, helper){  
        var ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
        //alert(is_ie);
        component.set("v.checkForInternetExplorer",is_ie);
        if(is_ie == true)
        {
        var action = component.get("c.isShown");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                //var alertMessagesList = response.getReturnValue();
                component.set("v.isShown",response.getReturnValue());
                
            } else if (response.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
       }
    },
    
    openModel: function(component, event, helper) {
        component.set("v.isModalOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    
    setUpChrome: function(component, event, helper) {
        window.open("http://communities.mercer.com/MercerForce/Documents/Set%20Up%20Chrome.pdf");
    }
    
    
})