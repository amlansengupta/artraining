// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    fetchEventRecord : function(component, event, helper) {
        helper.getEventRecord(component, event, helper);
        helper.get_SitePrefix(component);
        
        /*fixes for Add to Calendar*/
        $(function() {
              $(document).on("click", function(e) {
                  if ($(e.target).is(".atcb-link")) {
                      $('.atcb-list').addClass('showList');
                      $('.atcb-list').addClass('visibleList');
                      $('.atcb-list').removeClass('hiddenList');
                  } else {
                      $('.atcb-list').addClass('hiddenList');
                  }
              });              
          });
        /*fixes for Add to Calendar*/
        
    },
    
    afterScriptsLoaded: function(component, event, helper){
        svg4everybody();
    },
    
    callEditPage: function(component, event, helper){
        helper.initializeEventPageEdit(component, event);
    },
    
    closeEdit: function(component, event, helper) {
        component.set('v.isEdit', false);
    },
    
    gotoURL : function (component, event, helper)
    {
        var payment = component.find("payment").get("v.value");       
        var urlEvent = $A.get("e.force:navigateToURL");
        
        urlEvent.setParams({
            "url": payment
        });
        urlEvent.fire();
    },
    
    //calling method in helper for updating
    //add to calendar button name as per provided by user in properties
    updateATCbuttonName : function (component, event, helper) {
        addtocalendar.load();
        helper.updateATCbuttonName(component);
    }
})