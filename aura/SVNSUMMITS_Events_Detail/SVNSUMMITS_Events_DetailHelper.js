// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    //Applying site prefix
    get_SitePrefix: function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s", ""));
        });
        $A.enqueueAction(action);

        var eventRecordId = component.get("v.recordId");
        var action1 = component.get("c.isRecordEditable");
        action1.setParams({
            eventRecordId: eventRecordId,
        });

        action1.setCallback(this, function(actionResult1) {
            var isObjectEditable = actionResult1.getReturnValue();
            component.set("v.isObjectEditable", isObjectEditable);
        });
        $A.enqueueAction(action1);
    },

    getEventRecord: function(component, event, helper) {
        var self = this;
        var eventRecordId = component.get("v.recordId");
        var action = component.get("c.getEventRecord");

        action.setParams({
            eventRecordId: eventRecordId,
        });

        var eventsListWrapper;
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                eventsListWrapper = response.getReturnValue();

                var json = JSON.stringify(eventsListWrapper);

				// use browser info if guest user
				//self.debug(component, "Browser timezone = " +  Intl.DateTimeFormat().resolvedOptions().timeZone, Intl.DateTimeFormat().resolvedOptions());
				//self.debug(component, "Moment timezone guess = " + moment.tz.guess() );

				// adjust for guest user
                if (!eventsListWrapper.strTimeZone || eventsListWrapper.strTimeZone === 'GMT') {
                    eventsListWrapper.strTimeZone = moment.tz.guess();
                }

                for (var i = 0; i < eventsListWrapper.objEventList.length; i++) {

                    eventsListWrapper.objEventList[i].showTo = false;
                    eventsListWrapper.objEventList[i].showEndDate = false;

                    var startDate;
                    var startTime;
                    var endDate;
                    var endTime;
                    var localeStartDate;
                    var localeEndDate;
					var startDay, startMonth, startYear;
					var endDay, endMonth, endYear;

                    if (eventsListWrapper.objEventList[i].Start_DateTime__c !== null) {
                        localeStartDate = moment.tz(eventsListWrapper.objEventList[i].Start_DateTime__c,
                                                    eventsListWrapper.strTimeZone)
                                                .format('YYYY-MM-DD HH:mm:ss');

						self.debug(component, "Time zone      : " + eventsListWrapper.strTimeZone);
						self.debug(component, "Start Date     : " + eventsListWrapper.objEventList[i].Start_DateTime__c);
						self.debug(component, "localeStartDate: " + localeStartDate);

                        eventsListWrapper.objEventList[i].localeStartDate = localeStartDate;

                        startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                        startTime = moment(startDate).toDate();
                        eventsListWrapper.objEventList[i].strMinute = moment(startTime);
                        var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
                        eventsListWrapper.objEventList[i].strDay = days[startTime.getDay()];

                        //eventsListWrapper.objEventList[i].Start_DateTime__c = startDate;
                    }

                    if (eventsListWrapper.objEventList[i].End_DateTime__c !== null) {
                        localeEndDate = moment.tz(eventsListWrapper.objEventList[i].End_DateTime__c,
                                                  eventsListWrapper.strTimeZone)
                                              .format('YYYY-MM-DD HH:mm:ss');

						self.debug(component, "End Date       : " + eventsListWrapper.objEventList[i].End_DateTime__c);
						self.debug(component, "localeEndDate  : " + localeEndDate);

                        eventsListWrapper.objEventList[i].localeEndDate = localeEndDate;

                        endDate = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                        endTime = moment(endDate).toDate();
                        eventsListWrapper.objEventList[i].strEndMinute = moment(endTime);

                        //eventsListWrapper.objEventList[i].End_DateTime__c = endDate;
                    }

                    if (startTime !== null && endTime !== null) {
                        var diffDays = Math.round(Math.abs((endTime.getTime() - startTime.getTime()) / (24 * 60 * 60 * 1000)));
                        eventsListWrapper.objEventList[i].daysOfMultiDaysEvent = diffDays;
                        eventsListWrapper.objEventList[i].showTo = true;
                    }

			        startDay   = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("DD");
			        startMonth = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("MMM");
			        startYear  = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("YYYY");

			        endDay     = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("DD");
			        endMonth   = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("MMM");
			        endYear    = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("YYYY");

			        eventsListWrapper.objEventList[i].strMonth = startMonth;
			        eventsListWrapper.objEventList[i].intDate  = startDay;
			        eventsListWrapper.objEventList[i].strYear  = startYear;

					if (startDay !== endDay || startMonth !== endMonth || startYear !== endYear) {
						eventsListWrapper.objEventList[i].showEndDate = true;
					    eventsListWrapper.objEventList[i].endDay = endDay;
					    eventsListWrapper.objEventList[i].endMonth = endMonth;
					    eventsListWrapper.objEventList[i].endYear = endYear;
		            }

                    eventsListWrapper.objEventList[i].strMonth = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("MMM");
                    eventsListWrapper.objEventList[i].intDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("DD");

                    eventsListWrapper.objEventList[i].topics = [];
                    eventsListWrapper.objEventList[i].topics.push(eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);

                    component.set("v.events.Details__c", eventsListWrapper.objEventList[0].Details__c);

                    eventsListWrapper.objEventList[0].Details__Clean__c = eventsListWrapper.objEventList[0].Details__c.replace(/(<([^>]+)>)/ig,"");

                }

                component.set("v.wrappedEventsObj", eventsListWrapper);
            } else {
                console.log("problem with getEventRecord: state = ");
            }
        });
        $A.enqueueAction(action);
    },

    //updating add to calendar button name as per provided by user in properties
    updateATCbuttonName: function(component, event, helper) {
        this.debug(component, 'updateATCButton', component.get("v.wrappedEventsObj"));

        $('.atcb-link').each(function() {
            $(this).html(component.get("v.addToCalendarButtonText"));
        });
    },

    initializeEventPageEdit: function(component, event, helper) {
        var body = component.find('editView');
        body.set("v.body", []);

        try {
            var topics = '';
            if (component.get("v.recordId") && component.get("v.recordId") !== '') {
                topics = JSON.stringify(component.get('v.wrappedEventsObj.objEventList')[0].topics[0]);
            }
            $A.createComponent('c:SVNSUMMITS_Create_Event', {
                    'isEdit': true,
                    'sObjectId': component.get("v.recordId"),
                    'selectedTopics': topics,
                    'debugMode': component.get("v.debugMode")
                },
                function(editView) {
                    var op = component.find("editView");
                    var editViewbody = op.get('v.body');
                    editViewbody.push(editView);
                    op.set("v.body", editViewbody);
                    component.set("v.isEdit", true);
                });
        } catch (e) {
            //console.log('e----', e);
            this.debug(component, "initializeEventPageEdit", e);
        }
    },

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    }
})