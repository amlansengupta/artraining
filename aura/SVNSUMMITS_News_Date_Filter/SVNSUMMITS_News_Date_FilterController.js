// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit: function(component,event,helper) {
        component.set("v.isRender", true);
    },
	setDates : function(component,event,helper) {

        if(component.find("frmdate").get("v.value") === '' && component.find("todate").get("v.value") === ''){
            var cmpTarget = component.find('clearButton');
            $A.util.removeClass(cmpTarget, 'toggleClass1');
            $A.util.addClass(cmpTarget, 'toggleClass');
        }

        helper.sendFilterEvent(component);
	},
    clearDates : function(component,event,helper) {
        component.set("v.strError",null);
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Date_Filter_Event");
        appEvent.setParams({
            "fromDate" : null,
            "toDate" : null,
        });
        appEvent.fire();
        component.find("frmdate").set("v.value",'');
        component.find("todate").set("v.value",'');
        var cmpTarget = component.find('clearButton');
        $A.util.removeClass(cmpTarget, 'toggleClass1');
        $A.util.addClass(cmpTarget, 'toggleClass');
	},
    dateChange: function(component,event,helper) {
        var cmpTarget = component.find('clearButton');
        $A.util.removeClass(cmpTarget, 'toggleClass');
        $A.util.addClass(cmpTarget, 'toggleClass1');

        if (!component.get('v.showButton')) {
            helper.sendFilterEvent(component);
        }
    }
})