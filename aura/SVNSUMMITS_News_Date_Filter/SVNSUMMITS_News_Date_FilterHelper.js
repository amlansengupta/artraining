// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    sendFilterEvent: function (component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Date_Filter_Event");

        var currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);

        var frmDtStr;
        if (component.find("frmdate").get("v.value") !== null && component.find("frmdate").get("v.value") !== '') {
            this.debug(component, "date filter called", component.find("frmdate").get("v.value"));
            if (moment(component.find("frmdate").get("v.value")).isValid()) {
                if (component.find("frmdate").get("v.value").length > 0) {
                    var dtString = component.find("frmdate").get("v.value").split('-');
                    if (dtString[1] < 10) {
                        dtString[1] = '0' + dtString[1];
                    }

                    if (dtString[2] < 10) {
                        dtString[2] = '0' + dtString[2];
                    }
                    frmDtStr = new Date(dtString[0] + '-' + dtString[1] + '-' + dtString[2]);
                }
            } else {
                this.debug(component, "Date From Error", null);
                component.set("v.strError", 'Invalid Date');
            }
        }

        var toDtStr;
        if (component.find("todate").get("v.value") !== null && component.find("todate").get("v.value") !== '') {
            if (moment(component.find("todate").get("v.value")).isValid()) {
                if (component.find("todate").get("v.value").length > 0) {
                    var dtString1 = component.find("todate").get("v.value").split('-');
                    if (dtString1[1] < 10) {
                        dtString1[1] = '0' + dtString1[1];
                    }

                    if (dtString1[2] < 10) {
                        dtString1[2] = '0' + dtString1[2];
                    }
                    toDtStr = new Date(dtString1[0] + '-' + dtString1[1] + '-' + dtString1[2]);
                }
            } else {
                this.debug(component, "Date To Error", null);
                component.set("v.strError", 'Invalid Date');
            }
        }

        if (frmDtStr > currentDate) {
            component.set("v.strError", 'From Date Can not be greater than today.');
        } else if (toDtStr > currentDate) {
            component.set("v.strError", 'To Date Can not be greater than today.');
        } else if (toDtStr < frmDtStr) {
            component.set("v.strError", 'To Date Can not be less than From Date.');
        } else {
            if (component.find("frmdate").get("v.value") !== null && component.find("frmdate").get("v.value") !== '' && component.find("todate").get("v.value") !== null && component.find("todate").get("v.value") !== '') {
                if (moment(component.find("frmdate").get("v.value")).isValid() &&
                    moment(component.find("todate").get("v.value")).isValid()) {
                    component.set("v.strError", null);
                    appEvent.setParams({
                        "fromDate": component.find("frmdate").get("v.value"),
                        "toDate": component.find("todate").get("v.value"),
                    });
                    appEvent.fire();
                }
            } else if (component.find("frmdate").get("v.value") !== null && component.find("frmdate").get("v.value") !== '') {
                if (moment(component.find("frmdate").get("v.value")).isValid()) {
                    component.set("v.strError", null);
                    appEvent.setParams({
                        "fromDate": component.find("frmdate").get("v.value"),
                    });
                    appEvent.fire();
                }
            } else if (component.find("todate").get("v.value") !== null && component.find("todate").get("v.value") !== '') {
                if (moment(component.find("todate").get("v.value")).isValid()) {
                    component.set("v.strError", null);
                    appEvent.setParams({
                        "toDate": component.find("todate").get("v.value"),
                    });
                    appEvent.fire();
                }
            }
        }
    },

    calenderScroll: function (component) {
        if (component.get("v.isRender")) {
            $('#frmDtDiv').click(function () {
                if ($(window).scrollTop() > 400)
                    $(window).scrollTop(352);
            });

            $('#toDtDiv').click(function () {
                if ($(window).scrollTop() > 400)
                    $(window).scrollTop(352);
            });
        }
    },

    debug: function (component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    }
})