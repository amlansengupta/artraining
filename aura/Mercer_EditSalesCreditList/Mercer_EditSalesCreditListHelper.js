({
    scFetch : function(component,event) {
        
        var opId;
        if(component.get("v.recordId") != null && component.get("v.recordId") != ''){
            opId = component.get("v.recordId");
        }
        else{
            var indexOfEquals = window.location.search.indexOf('=');
            var opId = window.location.search.substr(indexOfEquals+1);
        }
        //alert(opId);
        var action = component.get("c.fetchSalesCredit");
        action.setParams({
            "recId" : opId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert("state "+ state); 
            //alert(response.getReturnValue());
            if(state == 'SUCCESS'){
                var ScList = response.getReturnValue();
                //alert(JSON.stringify(ScList));
                // alert("opp name "+ ScList[0].ename);
                
                if(ScList==null){
                    component.set("v.data",'No Records Exist for Edit');
                    //component.set("v.SCList",null);
                    component.set("v.total",0);
                } 
                else{
                    
                    component.set("v.SCList",ScList);
                    //alert('hi1');
                    //alert(JSON.stringify(ScList));
                    console.log('Fields shown'+JSON.stringify(component.get("v.SCList")));
                    
                    var sum =0;
                    
                    if(ScList!=null && ScList.length>0){
                        //alert('hi');
                        //alert(ScList[i].Percent_Allocation__c);
                        for(var i = 0; i < ScList.length; i++) {
                            sum=sum+ScList[i].scl.Percent_Allocation__c;
                        }
                        component.set("v.recordId", ScList[0].scl.Opportunity__c);
                    }
                    component.set("v.total",sum); 
                }  
            }});
        
        $A.enqueueAction(action);
        
    }	
    ,
    oppNameFetch :function(component){
        var opId;
        if(component.get("v.recordId") != null && component.get("v.recordId") != ''){
            opId = component.get("v.recordId");
        }
        else{
            var indexOfEquals = window.location.search.indexOf('=');
            var opId = window.location.search.substr(indexOfEquals+1);
        }
        
        var action = component.get("c.fetchOppName");
        action.setParams({
            "recId" : opId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert("state "+ state); 
            //alert(response.getReturnValue());
            if(state == 'SUCCESS'){
                var oppname = response.getReturnValue();
                //alert("opp name "+ oppname);
                component.set("v.oppname",oppname);
                
            }});
        
        $A.enqueueAction(action);
        
    },
    UpdateTotal : function(component){
        var sum =0;
        var ScmList=component.get("v.SCList");
        alert(ScmList.length);
        if(ScmList.length>0){
            //alert('hi');
            //alert(ScList[i].Percent_Allocation__c);
            for(var i = 0; i < ScmList.length; i++) {
                sum=sum+ScmList[i].Percent_Allocation__c;
            }
            
        }
        component.set("v.total",sum); 
    },
    fetchRole : function(component,event){
        
        var action = component.get("c.fetchRole");
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert('hi');
            //alert("state "+ state); 
            //alert(response.getReturnValue());
            if(state == 'SUCCESS'){
                var roles = response.getReturnValue();
                //alert("opp name "+ oppname);
                component.set("v.RoleList",roles);
                this.scFetch(component,event);
                
            }});
        
        $A.enqueueAction(action);
    },
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        // var RowItemList = component.get("v.salesList");
        component.set("v.data",'');
        /* RowItemList.push({
            'sobjectType': 'Sales_Credit__c',
            'EmpName__c': '',
            'Role__c': '',
            'Percent_Allocation__c': '',
            'Opportunity__c':component.get("v.recordId"),
            'EmpName__r':'',
            'SP_CM_Allocation__c':''
        });*/
        var opId;
        if(component.get("v.recordId") != null && component.get("v.recordId") != ''){
            opId = component.get("v.recordId");
        }
        else{
            var indexOfEquals = window.location.search.indexOf('=');
            var opId = window.location.search.substr(indexOfEquals+1);
        }
        var EmpNameComposite ={
            'Id':'',
            'Name':''
        }
        var salesCredObj = {
            'sobjectType': 'Sales_Credit__c',
            'EmpName__c': '',
            'Role__c': '--None--',
            'Percent_Allocation__c': '',
            'Opportunity__c':opId,
            'EmpName__r':EmpNameComposite,
            'SP_CM_Allocation__c':''
        };
        var scWrapperObj = {scl:salesCredObj, isChecked:false};
        var displayedList = component.get("v.SCList");
        if(!displayedList || displayedList == null){
            displayedList = [];
        }
        displayedList.push(scWrapperObj);
        
        // set the updated list to attribute (contactList) again    
        //component.set("v.salesList", RowItemList);
        component.set("v.SCList", displayedList);
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allSCRows = component.get("v.salesList");
        for (var indexVar = 0; indexVar < allSCRows.length; indexVar++) {
            if (allSCRows[indexVar].EmpName__c == '' || allSCRows[indexVar].Percent_Allocation__c == null ) {
                isValid = false;
                alert('Emp Name or Percent Allocation Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    }
    /* deleteSelected : function(component,event,selctedRec){
        var action = component.get("c.delSlctRec");
        action.setParams({
            "slctRec": selctedRec
        });
        action.setCallback(this, function(response){
            var state =  response.getState();
            if(state == "SUCCESS")
            {
                component.set("v.account",response.getReturnValue());
                console.log("Successfully Deleted..");
            }else if (state=="ERROR") {
                console.log(action.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }*/
    
    
    
})