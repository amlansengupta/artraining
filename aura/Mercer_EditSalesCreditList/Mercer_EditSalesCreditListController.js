({
    doInit : function(component, event, helper) {
        component.find("cbox").set("v.value", false);
        component.set("v.salesList",[]);
        component.set("v.SCList",[]);
        component.set("v.isOpen",false);
        component.set("v.lob",'');       
        component.set("v.flag",false);
        component.set("v.lobEmp",null);
        //helper.scFetch(component);
        //if(component.get("v.SCList")!=null){
        helper.fetchRole(component,event);
        //}
        helper.oppNameFetch(component);
        //helper.createObjectData(component,event);
        //helper.UpdateTotal(component);
    },
    saveRecord : function(component,event,helper){
        component.set("v.Spinner",true);
        var flag=true;
        var scNew=[];
        scNew =component.get("v.salesList");        
        var sclist=[];
        sclist = component.get("v.SCList");
        //alert(JSON.stringify(sclist));
        //var EditSalesCredits=[];
        var SalesCredits =[];
        if(sclist!=null && sclist.length>0){
            for(var i=0;i<sclist.length;i++){
                SalesCredits[i] = sclist[i].scl;
            }
            //alert(JSON.stringify(SalesCredits));
            for(var i=0;i<SalesCredits.length;i++){
                //alert($A.get("$Browser.isPhone"));
                if($A.get("$Browser.isPhone")){
                    // alert('hi');
                    //alert(SalesCredits[i].EmpName__r);
                    if(SalesCredits[i].EmpName__r !='undefined' && SalesCredits[i].EmpName__r !='' && JSON.stringify(SalesCredits[i].EmpName__r)!=JSON.stringify({}) ){
                        SalesCredits[i].EmpName__c = SalesCredits[i].EmpName__r.Id;
                    }
                    else{
                        flag=false;
                    }
                }
                else{
                    //alert(SalesCredits[i].EmpName__c);
                    if(SalesCredits[i].EmpName__c !='undefined' && SalesCredits[i].EmpName__c !=''){
                        SalesCredits[i].EmpName__c = SalesCredits[i].EmpName__c;
                    }
                    else{
                        flag=false;
                    }
                }
                if(SalesCredits[i].Role__C=='--None--'){
                    SalesCredits[i].Role__C = null; 
                }
            }  
            if(!flag){
                component.set("v.Spinner",false);
                component.set("v.errors","EmployeeName: You must enter a value"); 
                component.set("v.flag",true);
            }else{
                component.set("v.errors",""); 
                component.set("v.flag",false);
            }
        }
        // alert(JSON.stringify(SalesCredits));
        //alert(scNew.length);
        /*if(scNew.length>0){
            for(var i=0;i<scNew.length;i++){
                alert(JSON.stringify(scNew[i].EmpName__r));
                if(scNew[i].EmpName__r !='undefined' && scNew[i].EmpName__r !='' && JSON.stringify(scNew[i].EmpName__r)!=JSON.stringify({}) ){
				   
                    scNew[i].EmpName__c = scNew[i].EmpName__r.Id;
                    if(scNew[i].Role__c!=''){
                        SalesCredits[i].Role__c = scNew[i].Role__c;
                     }
                     if(scNew[i].Percent_Allocation__c!=''){
                        SalesCredits[i].Percent_Allocation__c=scNew[i].Percent_Allocation__c;
                     }
                    if(scNew[i].SP_CM_Allocation__c!=''){
                        SalesCredits[i].SP_CM_Allocation__c=scNew[i].SP_CM_Allocation__c;
                     }
                     SalesCredits.push(scNew[i]);
                    
                 }
                 
             }
            
        }*/            
        //alert(JSON.stringify(SalesCredits));
        var action = component.get("c.save");
        action.setParams({
            "scl" : JSON.stringify(SalesCredits)
        });
        if(flag){
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log("state "+ state); 
                console.log("***"+JSON.stringify(response.getReturnValue()));
                //alert(response.getReturnValue());
                //alert(state);
                if(state == 'SUCCESS'){
                    component.set("v.Spinner",false);
                    var oppId = response.getReturnValue();
                    // alert(oppId);
                    if(oppId !=null ) {
                        var sObjectEvent = $A.get("e.force:navigateToSObject");
                        sObjectEvent.setParams({
                            "recordId":oppId ,
                            "slideDevName": "detail"
                            
                        });
                        sObjectEvent.fire();
                    }
                    else if(component.get("v.recordId") !=null){
                        
                        var sObjectEvent1 = $A.get("e.force:navigateToSObject");
                        sObjectEvent1.setParams({
                            "recordId":component.get("v.recordId") ,
                            "slideDevName": "detail"
                            
                        });
                        sObjectEvent1.fire();
                    }
                        else{
                            component.set("v.errors",'No Sales Credit Record to Save and Edit') ; 
                            component.set("v.flag",true) ;
                        }
                    
                }
                else{
                    component.set("v.Spinner",false);
                    var errors = response.getError();
                    //alert(JSON.stringify(errors));
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        if(errors[0].pageErrors.length>0){
                            var message = errors[0].pageErrors[0].message;
                            if(message.includes("Insufficient access"))
                                message = 'You do not have access on the Opportunity';
                            //alert(message);
                            component.set("v.errors",message); 
                            component.set("v.flag",true) ;
                        }
                        if(errors[0].fieldErrors!=null){
                            //alert('hi');
                            if(errors[0].fieldErrors.Percent_Allocation__c!=null && errors[0].fieldErrors.Percent_Allocation__c.length>0)
                                //message += fieldErrorObj[0].errorCode + ' (' + fieldErrorObj[0].field + ') : ' + fieldErrorObj[0].message;
                                var message = errors[0].fieldErrors.Percent_Allocation__c[0].message;
                            // alert(message);
                            component.set("v.errors",message); 
                            component.set("v.flag",true) ;
                        }
                    }
                    
                }
            });
            
            
            $A.enqueueAction(action);
        }
        
        
    },
    calculate : function(component,event,helper){
        var sclist = component.get("v.SCList");
        var salesCredits =[];
        var sum=0
        // var inputAllocation = event.getSource().get("v.value");
        /*if(inputAllocation > 100){
            component.set("v.errors",'Sales Credit must not be more than 100%. Please amend the values.'); 
            component.set("v.flag",true) ;
            component.find("saveButton").set("v.disabled",true);
        }*/
        for(var i=0;i<sclist.length;i++){
            sum=sum+sclist[i].scl.Percent_Allocation__c;
            
            //alert(sum);
            /*if(sum>200){
                component.set("v.errors",'Sales Credit must not be more than 200%. Please amend the values.'); 
                component.set("v.flag",true) ; 
                component.find("saveButton").set("v.disabled",true);
            }
            else{
                component.set("v.errors",''); 
                component.set("v.flag",false) ;  
                component.set("v.total",sum);
                component.find("saveButton").set("v.disabled",false);
            }*/
        }
        component.set("v.total",sum);
        //component.set("v.flag",true) ;
    },
    Cancel:function(component,event,helper){
        //UAT feedback #2920
        var opId;
        console.log("record id:"+component.get("v.recordId"));
        if(component.get("v.recordId") != null && component.get("v.recordId") != ''){
            opId = component.get("v.recordId");
        }
        else{
            var indexOfEquals = window.location.search.indexOf('=');
            var opId = window.location.search.substr(indexOfEquals+1);
        }
        window.location="/lightning/r/Opportunity/"+opId+"/view";
    },
    updateLOBPhone:function(component,event,helper){
        var lobcomp = component.find("LOB");
        //alert(lobcomp);      
        var col =event.getParam("colId");
        var rowIndex=event.getParam("lobId");
        //alert(JSON.stringify(col));
        //alert("Row No : " + rowIndex);
        component.set("v.lobEmp",col);
        var action = component.get("c.fetchLOB");
        action.setParams({
            "colId" : col.Id
        }); 
        
        action.setCallback(this, function(response) {
            var sclist = component.get("v.SCList");
            var state = response.getState();
            //console.log("state "+ state); 
            // console.log("***"+JSON.stringify(response.getReturnValue()));
            //alert(response.getReturnValue());
            if(state == 'SUCCESS'){
                var lob = response.getReturnValue();
                //(lob);
                if(lob!=null && lob!=''){
                    
                    //var rowIndex = event.currentTarget.parentElement.parentElement.id ;
                    if(sclist.length>1){
                        
                        //alert(rowIndex);
                        //component.set("v.lob",lob);
                        lobcomp[rowIndex].set("v.value",lob);
                    }
                    else{
                        lobcomp.set("v.value",lob);
                    }
                }
                else{
                    if(sclist.length>1){
                        lobcomp.set("v.value",'');
                    }
                    else{
                        lobcomp[rowIndex].set("v.value",'');
                    }
                }
            }
        });
        
        $A.enqueueAction(action); 
        
        
        
    },
    updateLOB:function(component,event,helper){
        var lobcomp = component.find("LOB");
        var rowIndex= event.getSource().get("v.class");
        var col= event.getSource().get("v.value")[0];
        //alert(rowIndex);
        //alert(col);
        var action = component.get("c.fetchLOB");
        action.setParams({
            "colId" : col
        }); 
        
        action.setCallback(this, function(response) {
            var sclist = component.get("v.SCList");
            var state = response.getState();
            //console.log("state "+ state); 
            // console.log("***"+JSON.stringify(response.getReturnValue()));
            //alert(response.getReturnValue());
            //alert(sclist.length);
            //alert(JSON.stringify(response.getError()));
            if(state == 'SUCCESS'){
                var lob = response.getReturnValue();
                //(lob);
                if(lob!=null && lob!=''){
                    
                    //var rowIndex = event.currentTarget.parentElement.parentElement.id ;
                    if(sclist.length>1){
                        
                        //alert(rowIndex);
                        component.set("v.lob",lob);
                        lobcomp[rowIndex].set("v.value",lob);
                    }
                    else{
                        lobcomp.set("v.value",lob);
                    }
                }
                else{
                    
                    if(sclist.length==1){
                        lobcomp.set("v.value",'');
                    }
                    else{
                        lobcomp[rowIndex].set("v.value",'');
                    }
                }
            }
        });
        
        $A.enqueueAction(action); 
        
        
        
    },
    /*,
    onSingleSelectChange: function(component,event,helper) {
         var selectCmp = component.find("role");
         //var resultCmp = cmp.find("singleResult");
         selectCmp.set("v.value", selectCmp.get("v.value"));
	 }*/
    
    
    delRow : function(component, event, helper){
        component.set("v.flag",false) ;
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        //component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
        //(sclist.length>0)
        var scdelId=event.getSource().get("v.value");
        //alert(scdelId);
        var action = component.get("c.del");
        action.setParams({
            "scId" : scdelId
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log("state "+ state); 
            // console.log("***"+JSON.stringify(response.getReturnValue()));
            //alert(response.getReturnValue());
            if(state == 'SUCCESS'){
                var val = response.getReturnValue();
                //alert(val);
                //component.set("v.salesList",);
                
                var a = component.get('c.doInit');
                $A.enqueueAction(a);
                
            }
            else{
                var errors = response.getError();
                //alert(JSON.stringify(errors));
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    if(errors[0].pageErrors.length>0){
                        var message = errors[0].pageErrors[0].message;
                        //alert(message);
                        component.set("v.errors",message); 
                        component.set("v.flag",true) ;                 
                    }
                }
            }
        });
        
        $A.enqueueAction(action); 
        
        //list.remove(rowid);
        
    },
    /* DelWithOutOppTeam : function(component,event,helper){
        var scdelId=event.getSource().get("v.value");
        alert(scdelId);
    },
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
    
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.SCList");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.salesList", AllRowsList);
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },*/
    selectAll:function(component, event, helper) {
        var slctCheck = event.getSource().get("v.value");
        var slist = component.get("v.SCList");
        
        // var getCheckAllId = component.find("cboxRow");
        //getCheckAllId[0].set("v.value",true);
        //alert(getCheckAllId);
        // alert(getCheckAllId.length);
        if(slist!=null){
            if(slctCheck == true) {
                for (var i = 0; i < slist.length; i++) {
                    slist[i].isChecked = true;
                    
                }
            } else {
                for (var i = 0; i < slist.length; i++) {
                    slist[i].isChecked = false; 
                }
            }
            component.set("v.SCList",slist);
        }
    },
    changeSelectAll:function(component,event, helper){
        var slctCheckRow = event.getSource().get("v.value");
        var getCheckAllId = component.find("cbox");
        if(slctCheckRow == false) {
            component.find("cbox").set("v.value", false);
        }
    },
    deleteSlctd : function(component,event,helper) {
        component.set("v.Spinner",true);
        var displayedList = component.get("v.SCList");
        var salesCredits = [];
        var isDatabaseDelete = false;
        //alert(JSON.stringify(displayedList));
        
        if(displayedList != null){
            for(var loopCounter=0; loopCounter<displayedList.length; loopCounter++){
                
                if(displayedList[loopCounter].isChecked && displayedList[loopCounter].scl.Id != null){
                    
                    isDatabaseDelete = true;
                    salesCredits.push(displayedList[loopCounter]);
                }
                else if(displayedList[loopCounter].isChecked && displayedList[loopCounter].scl.Id == null){
                    
                } /*else if(displayedList[loopCounter].isChecked== false && displayedList[loopCounter].scl.Id == null ){
                    //displayedList[loopCounter].scl.EmpName__r.Id=null;
                    //displayedList[loopCounter].scl.EmpName__r.Name='';
                    //salesCredits.push(displayedList[loopCounter]);
                }*/else{
                    salesCredits.push(displayedList[loopCounter]);
                }
            }
            if(isDatabaseDelete === true){
                // alert(JSON.stringify(salesCredits));
                
                var action = component.get("c.delSlctRec");
                action.setParams({
                    "slctRec" : JSON.stringify(salesCredits)
                }); 
                action.setCallback(this, function(response) {
                    
                    var state = response.getState();
                    
                    if(state == 'SUCCESS'){
                        component.set("v.Spinner",false);
                        var resultingList = response.getReturnValue();
                        
                        component.set("v.SCList", resultingList);
                        var sum=0;
                        for(var i=0;i<resultingList.length;i++){
                            sum=sum+resultingList[i].scl.Percent_Allocation__c;
                        } 
                        component.set("v.total",sum);
                        component.find("cbox").set("v.value",false);
                        //alert(sum);
                        /*if(sum>200){
                            component.set("v.errors",'Sales Credit must not be more than 200%. Please amend the values.'); 
                            component.set("v.flag",true) ; 
                            component.find("saveButton").set("v.disabled",true);
                        }
                        else{
                            component.set("v.errors",''); 
                            component.set("v.flag",false) ;  
                            
                            component.find("saveButton").set("v.disabled",false);
                        }*/
                    }
                    
                    else{
                        component.set("v.Spinner",false);
                        var errors = response.getError();
                        //alert(JSON.stringify(errors));
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            if(errors[0].pageErrors.length>0){
                                var message = errors[0].pageErrors[0].message;
                                component.set("v.errors",message); 
                                component.set("v.flag",true) ;                 
                            }
                        }
                    }
                });
                
                $A.enqueueAction(action);
                
            }else{
                component.find("cbox").set("v.value",false);
                component.set("v.Spinner",false);
                component.set("v.SCList", salesCredits);
                var sum=0;
                for(var i=0;i<salesCredits.length;i++){
                    sum=sum+salesCredits[i].scl.Percent_Allocation__c;
                } 
                component.set("v.total",sum);
                //alert(sum);
                /*if(sum>200){
                    component.set("v.errors",'Sales Credit must not be more than 200%. Please amend the values.'); 
                    component.set("v.flag",true) ; 
                    component.find("saveButton").set("v.disabled",true);
                }else{
                    component.set("v.errors",''); 
                    component.set("v.flag",false) ;  
                   
                    component.find("saveButton").set("v.disabled",false);
                }*/
            }
        } 
    },
    AddRow:function(component,event,helper){
        helper.createObjectData(component,event);
    }
})