// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	doInit : function(component, event, helper) {
        var articleSourcePromise;
        var isNewsRecordType =

        $('.targetButton').attr('disabled','disabled');
        var url = window.location.href;
        var splitUrl = url.split("/s");
        component.set("v.sfInstanceUrl",splitUrl[0]);
		
        var spinnerSubmit = component.find("spinnerSubmit");
        $A.util.addClass(spinnerSubmit, 'hide');

        helper.getTopics(component);
        helper.getAuthors(component);
        helper.get_SitePrefix(component);
        helper.get_SessionId(component);

        // get the article source picklist options
        articleSourcePromise = helper.getDefaultArticleSource(component);

        // Check if we are in edit mode or not
        // ---------------------------------
		if(component.get('v.isEdit') === true){
        	// we need to make sure we have the article source pick list first
            $.when(articleSourcePromise).then(function(){
                helper.getNewsRecord(component, event, helper);
            }, function(){
                console.error('NewCreateController:doInit: Failed to get article source picklist');
            });
        } else {
		    // get the current user and use as author
		    if (!component.get("v.newsObj.Author__c")) {
				helper.setDefaultAuthor(component, event, helper);
			}
		}

        /*020616 - Safari fixes for input type file*/
        $(function(){
            var nAgt = navigator.userAgent;
            if (nAgt.indexOf("Safari") !== -1)
            {
                if (nAgt.indexOf("Version") !== -1)
                {
                    $('.attachEdit').addClass("attachEditSafari");
                }
            }
            else if (nAgt.indexOf("Firefox") !== -1) {
               $('.attachEdit').addClass("attachEditMozilla");
            }
            else if (nAgt.match("CriOS") !== -1) {
               $('.attachEdit').addClass("attachEditMozilla");
            }
        });
        /*020616 - Safari fixes for input type file*/
	},

    notifyFileSelected : function(component) {

        var fileInput = component.find("image").getElement();
        if(fileInput.files.length > 0 ){
            component.set('v.isFileCooosen',true);
        }else{
            component.set('v.isFileCooosen',false);
            component.set("v.attachmentName",'No File Chosen');
        }
    },
    showAuthorDropdown: function(component, event, helper) {

        helper.showHideAuthors(component);
	},
    submitNews : function(component, event, helper) {

        var spinnerSubmit = component.find("spinnerSubmit");
        $A.util.removeClass(spinnerSubmit,'hide');

        component.set("v.strError",null);
        component.set("v.isBrowseError",false);

        var newsObj = component.get("v.newsObj");
        var fileInput = component.find("image").getElement();
        var pbdt = component.find("publishDate").get("v.value");
        var ardt = component.find("archiveDate").get("v.value");

        var isError = false;

        var inputTitleCmp = component.find("newsName");

        if(!inputTitleCmp.get("v.value") || inputTitleCmp.get("v.value").trim() === "")
        {
        	inputTitleCmp.set("v.errors", [{message:"Please fill out the Title field."}]);
            isError = true;
        }
        else
        {
            if( inputTitleCmp.get("v.value").length > 80)
            {
                inputTitleCmp.set("v.errors", [{message:"Event Title cannot exceed 80 characters. Please enter a title less than 80 characters."}]);
                isError = true;
            }
            else
            {
                inputTitleCmp.set("v.errors",null);
            }
        }

        var fullArticleCmp = component.find("newsDetail");
	    if (!fullArticleCmp.get("v.value") || fullArticleCmp.get("v.value").trim() === "") {
		    component.set('v.detailValid', false);
		    isError = true;
	    }
	    else {
		    component.set('v.detailValid', true);
	    }

	    // make sure summary is not empty
        var summaryCmp = component.find("newsSummary");
        if(!summaryCmp.get("v.value") || summaryCmp.get("v.value").trim() === "")
        {
            summaryCmp.set("v.errors", [{message:"Please fill out the Summary field."}]);
            isError = true;
        }
        else
        {
            summaryCmp.set("v.errors",null);
        }

        var filterByTopicCmp = component.find("filterByTopic");

        if(!filterByTopicCmp.get("v.value") || filterByTopicCmp.get("v.value").trim() === "")
        {
        	filterByTopicCmp.set("v.errors", [{message:"Please Select Topics."}]);
            isError = true;
        }
        else
        {
            filterByTopicCmp.set("v.errors",null);
        }

		var publishDateCmp = component.find("publishDate");

        if(!publishDateCmp.get("v.value") || publishDateCmp.get("v.value").trim() === "")
        {
        	publishDateCmp.set("v.errors", [{message:"Please Select Publication Date."}]);
            isError = true;
        }
        else
        {
            publishDateCmp.set("v.errors",null);
            if(ardt !== null && ardt && moment(pbdt).isAfter(moment(ardt)))
            {
                publishDateCmp.set("v.errors", [{message:"Publication date must be before archive date."}]);
                isError = true;
            }
            else
            {
                publishDateCmp.set("v.errors",null);
            }
        }

        if(fileInput.files.length > 0 && fileInput.files[0].type.indexOf("image") === -1)
        {
            isError = true;
            component.set("v.isBrowseError",true);
            component.set("v.strError","Error : Selected file must be an image.");
        }else if(fileInput.files.length > 0 && fileInput.files[0].size > 25000000){
            isError = true;
            component.set("v.isBrowseError",true);
            component.set("v.strError","Error : Image size must be less than 25MB.");
        }else{
             if(component.get("v.isEdit") && component.get("v.attachmentName") === 'No File Chosen' && component.get("v.isAttachment")){
             	component.set('v.isFileDelete',true);
             }
             component.set("v.isBrowseError",false);
             component.set("v.strError",null);
        }

        if(isError === true){
            $A.util.addClass(spinnerSubmit,"hide");
            helper.debug(component,"Error Occured",null);
        }else{
            if(fileInput.files.length > 0){
                helper.debug(component,"created with attachment called",null);
                helper.submitNews(component, newsObj);
            }
            else
            {
                helper.debug(component,"created without attachment called ",null);
                helper.submitNews(component, newsObj);
            }
        }
    },
	setSubmitButtonState: function(component, event, helper) {
    	helper.setSubmitButtonStateCheck(component, event);
    }

});