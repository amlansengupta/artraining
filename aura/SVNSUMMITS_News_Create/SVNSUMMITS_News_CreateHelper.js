// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getTopics : function(component)   {
        var action = component.get("c.getTopics");

        action.setCallback(this, function(response) {

            var values = response.getReturnValue();
            var valuesTemp = [];
            for (var value in values){
                if(values.hasOwnProperty(value)){
                    valuesTemp.push({
                        key: value,
                        value: values[value]
                    });
                }
            }

            component.set("v.topicValues", valuesTemp);
        });
        $A.enqueueAction(action);
    },

    getAuthors : function(component) {
        var action = component.get("c.getUsers");

        action.setCallback(this, function(response) {

            var values = response.getReturnValue();
            var valuesTemp = [];
            for (var value in values){
                if(values.hasOwnProperty(value)){
                    valuesTemp.push({
                        key: value,
                        value: values[value]
                    });
                }
            }
            component.set("v.authorValues", valuesTemp);
        });
        $A.enqueueAction(action);
    },

    setDefaultAuthor: function(component) {
        var action = component.get("c.getCurrentUser");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.newsObj.Author__c", response.getReturnValue().Id);
            }
        });
        $A.enqueueAction(action);
    },

    getDefaultArticleSource: function(component){
        var dfd = jQuery.Deferred();

        var action = component.get("c.getArticleSourceList");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.articleSourceList", response.getReturnValue());
                dfd.resolve();
            } else {
                dfd.reject();
            }
        });

        $A.enqueueAction(action);

        return dfd;
    },

    initializeDropdown: function(component) {
        try {

            var createDropdown = function() {
                $(".topic")
                    .addClass("ui fluid search")
                    .dropdown({
                        placeholder: "Select Topics"
                    });

                $(".author")
                    .addClass("ui fluid search")
                    .dropdown({
                        placeholder: "Select Author"
                    });


                var topics = component.get("v.selectedTopics");

                if(topics !== ''){
                    var topicsSelected = JSON.parse(topics);
                    var tp = [];
                    for(var t=0; t < topicsSelected.length; t++)
                    {
                        tp.push(topicsSelected[t].TopicId);
                    }
                    window.setTimeout(
                        $A.getCallback(function() {
                            $(".topic").dropdown('set selected', tp);
                        }), 1000
                    );

                }

                var author = component.get("v.newsObj.Author__c");
                if(author !== ''){

                    window.setTimeout(
                        $A.getCallback(function() {
                            $(".author").dropdown('set selected', author);
                        }), 1000
                    );

                }
            };

            // why are we doing it this way? Check out the description
            // in the initJqueryPlugins function in this file
            if (component.isValid() && window.$ && window.$.fn.dropdown) {
                createDropdown();
            } else {
                this.initJqueryPlugins();
                createDropdown();
            }

        } catch(e){
            this.debug(component,null,e);
        }
    },

    showHideAuthors: function(component){
        var ischeck = component.get("v.newsObj.Show_Author__c");
        var cmpTarget = component.find('authorSelection');
        if(ischeck === true){
            $A.util.removeClass(cmpTarget, 'hideDropdown');
            $A.util.addClass(cmpTarget, 'showDropdown');
        }else{
            $A.util.removeClass(cmpTarget, 'showDropdown');
            $A.util.addClass(cmpTarget, 'hideDropdown');
        }
    },

    // Fetching record detail for edit view as we are using create component for edit news page
    getNewsRecord : function(component, event, helper) {
        this.debug(component,"get News Record for detail called from news create ",null);

        var action = component.get("c.getNewsRecord");
        action.setParams({
            newsRecordId : component.get("v.sObjectId"),
        });

        component.set('v.attachmentName','No File Chosen');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var newsListWrapper  = response.getReturnValue();
                var recordType = '';

                // set the record type
                if(newsListWrapper.newsList !== undefined && newsListWrapper.newsList[0] !== undefined && newsListWrapper.newsList[0].RecordType !== undefined){
                    recordType = newsListWrapper.newsList[0].RecordType.DeveloperName;
                    component.set("v.createType", recordType);
                }

                if(newsListWrapper.newsList[0].Attachments && newsListWrapper.newsList[0].Attachments.length > 0)
                {
                    component.set('v.isAttachment', true);
                    component.set('v.attachmentName', newsListWrapper.newsList[0].Attachments[0].Name);
                }

                component.set("v.newsObj", newsListWrapper.newsList[0]);

                this.showHideAuthors(component,event,helper);

                $("document").ready(function(){
                    $("#upload").change(function() {
                        var a = document.getElementById('upload');
                        if(a.value === "")
                        {
                            fileLabel.innerHTML = "No file Chosen";
                        }
                        else
                        {
                            var theSplit = a.value.split('\\');
                            fileLabel.innerHTML = theSplit[theSplit.length-1];
                            jQuery('<div/>',
                                   {
                                       class: 'myCls',
                                       text: theSplit[theSplit.length-1]
                                   }).appendTo('body');
                            $('.myCls').html('');
                        }
                    });
                });

                // show the form
                $A.util.removeClass(component, 'slds-hide');

            }
        });
        $A.enqueueAction(action);
    },

    deleteAttachment : function(component){
        var action = component.get("c.deleteAttachment");
        action.setParams({
            newsRecordId : component.get("v.sObjectId"),
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                this.debug(component,"Attachment Deleted",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    submitNews : function(component, newsObj) {

        // create type should be the RecordTypes DeveloperName
        var recordType = component.get("v.createType");

        // are we editing news?
        var isEdit = component.get("v.isEdit");

        var strfilterByTopic = component.find("filterByTopic").get("v.value");
        var fileInput = component.find("image").getElement();
        var pathToDetail = component.get("v.pathToDetail");
        var spinnerSubmit = component.find("spinnerSubmit");

        if(component.get("v.isFileCooosen") || component.get("v.isFileDelete")){
            this.deleteAttachment(component);
        }

        // if we are creating a blog we want to force the show author to true if not set it to false;
        if(recordType === 'Blogs'){
            component.set("v.newsObj.Show_Author__c", true);
        } else {
            component.set("v.newsObj.Show_Author__c", false);
        }

        var action = component.get("c.saveNews");

        action.setParams({
            "newsObj": newsObj,
            "strfilterByTopic" : strfilterByTopic,
            "recordType": recordType,
            "isEdit": isEdit
        });

        var self = this;

        action.setCallback(this, function(response) {

            var state = response.getState();
            this.debug(component,"response : ",response.getState());
            if (component.isValid() && state === "SUCCESS") {
                var resId = response.getReturnValue().Id;
                this.debug(component,"response Id : ",resId);

                if(fileInput.files.length > 0 && resId !== null && resId.length > 0 )
                {
                    self.uploadImage(component,resId,fileInput.files);
                }
                else if(resId !== null && resId.length > 0 )
                {
                    $A.util.addClass(spinnerSubmit, 'hideDropdown');
                    self.goToURL(pathToDetail + resId);
                }
            }
            if (state === "ERROR") {
                var errors = response.getError();
                
                if (errors[0] && errors[0].fieldErrors) {

                    if(errors[0].fieldErrors.EntityId[0].message.length > 0){
                        component.set("v.strError",errors[0].fieldErrors.EntityId[0].message);
                    }

                }
                if (errors[0] && errors[0].pageErrors) {

                    this.debug(component,"Page Error : ",errors[0].pageErrors[0].message);
                    component.set("v.strError",errors[0].pageErrors[0].message);
                }
            }
        });
        $A.enqueueAction(action);
    },

    uploadImage : function(component, resId, files) {

        var sessionId = component.get("v.sessionId");
        var pathToDetail = component.get("v.pathToDetail");
        var sfInstanceUrl = component.get("v.sfInstanceUrl");
        var client = new forcetk.Client();
        client.setSessionToken(sessionId ,'v39.0',sfInstanceUrl);
        client.proxyUrl = null;
        client.instanceUrl = sfInstanceUrl;
        var spinnerSubmit = component.find("spinnerSubmit");

        var file = files[0];
        var self = this;

        client.createBlob('Attachment', {
            'ParentId': resId,
            'Name': file.name,
            'ContentType': file.type,
        }, file.name, 'Body', file , function(response){
            if(response.id !== null){
                $A.util.addClass(spinnerSubmit,'hide');
                self.goToURL(pathToDetail + resId);
            }else{
                $A.util.addClass(spinnerSubmit,'hide');
                component.set("v.strAttachmentError",'Error : '+response.errors[0].message);
            }
        }, function(request, status, response){
            $A.util.addClass(spinnerSubmit,'hide');
            var res = JSON.parse(response);
            component.set("v.strAttachmentError",'Error : ' +res[0].message);
        });
    },

    goToURL: function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
        });
        urlEvent.fire();
    },

    get_SessionId : function(component) {
        var action = component.get("c.getSessionId");
        action.setCallback(this, function(actionResult) {
            component.set("v.sessionId", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    get_SitePrefix : function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            this.debug(component,"sitePath",sitePath);
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s",""));
        });
        $A.enqueueAction(action);
    },

    debug: function(component, msg, variable) {
        console.log('IN DEBUG MODE');
        var debugMode = component.get("v.debugMode");
        if(debugMode) {
            if(msg) {
                console.log(msg);
            }
            if(variable) {
                console.log(variable);
            }
        }
    },

    setSubmitButtonStateCheck: function(component, event) {
        component.set("v.disableButton",false);
        var toggleText = component.find("FillHeader");
        $A.util.removeClass(toggleText, 'show');
        $A.util.addClass(toggleText, 'hide');
        $('#FillHeader').hide(); //Working with Lightning Locker 

        var name = component.get('v.newsObj.Name');
        var details = component.get('v.newsObj.Details__c');
        var summary = component.get('v.newsObj.Summary__c');
        var dtTime = component.get('v.newsObj.Publish_DateTime__c');
        var topics = component.find('filterByTopic').get('v.value');
        var time = component.find('publishDate').get('v.value');

        //undefined can be overridden. Safer to use 'in' keyword
        if((name == '' || name in window) ||
           	(details == '' || details in window) ||
            (summary == '' || summary in window) ||
           	(topics == '' || topics in window) || 
            (time == '' || time in window))
        {
            component.set("v.disableButton",true);
            $('#FillHeader').show(); //Working with Lightning Locker 
            $A.util.addClass(toggleText, 'show');
        	$A.util.removeClass(toggleText, 'hide');
        }
		
        $A.util.toggleClass(toggleText, "toggle");        
    },

    calenderScroll : function() {
        $('#pbDt').click(function(){
            if($(window).scrollTop() > 700)
                $(window).scrollTop(900);
        });

        $('#arDt').click(function(){
            if($(window).scrollTop() > 700)
                $(window).scrollTop(1010);
        });
    },

    /**
     * Why do you need to embed this?? Well because of how lightning components require you to
     * include third party libraries into your components it provides the potential that even though
     * your component loads the scripts it needs, it can't be assumed they will actual be there when
     * you need them. Say for example I load in my component jQuery and Slick.JS which extends jQuery.
     * Then someone else component anywhere else in the community loads another jQuery that now whips
     * out our components vesion of jQuery along with the extended slick.js. When locker service in
     * salesforce is turned on this won't be a problem because each component is in their own sandbox.
     */
    initJqueryPlugins: function(){
        /*!
         * # Semantic UI 2.1.7 - Dropdown
         * http://github.com/semantic-org/semantic-ui/
         *
         *
         * Copyright 2015 Contributors
         * Released under the MIT license
         * http://opensource.org/licenses/MIT
         *
         */
        !function(a,b,c,d){"use strict";a.fn.dropdown=function(e){var o,f=a(this),g=a(c),h=f.selector||"",i="ontouchstart"in c.documentElement,j=(new Date).getTime(),k=[],l=arguments[0],m="string"==typeof l,n=[].slice.call(arguments,1);return f.each(function(p){var R,S,T,U,V,W,X,q=a.isPlainObject(e)?a.extend(!0,{},a.fn.dropdown.settings,e):a.extend({},a.fn.dropdown.settings),r=q.className,s=q.message,t=q.fields,u=q.keys,v=q.metadata,w=q.namespace,x=q.regExp,y=q.selector,z=q.error,A=q.templates,B="."+w,C="module-"+w,D=a(this),E=a(q.context),F=D.find(y.text),G=D.find(y.search),H=D.find(y.input),I=D.find(y.icon),J=D.prev().find(y.text).length>0?D.prev().find(y.text):D.prev(),K=D.children(y.menu),L=K.find(y.item),M=!1,N=!1,O=!1,P=this,Q=D.data(C);X={initialize:function(){X.debug("Initializing dropdown",q),X.is.alreadySetup()?X.setup.reference():(X.setup.layout(),X.refreshData(),X.save.defaults(),X.restore.selected(),X.create.id(),X.bind.events(),X.observeChanges(),X.instantiate())},instantiate:function(){X.verbose("Storing instance of dropdown",X),Q=X,D.data(C,X)},destroy:function(){X.verbose("Destroying previous dropdown",D),X.remove.tabbable(),D.off(B).removeData(C),K.off(B),g.off(T),V&&V.disconnect(),W&&W.disconnect()},observeChanges:function(){"MutationObserver"in b&&(V=new MutationObserver(function(a){X.debug("<select> modified, recreating menu"),X.setup.select()}),W=new MutationObserver(function(a){X.debug("Menu modified, updating selector cache"),X.refresh()}),X.has.input()&&V.observe(H[0],{childList:!0,subtree:!0}),X.has.menu()&&W.observe(K[0],{childList:!0,subtree:!0}),X.debug("Setting up mutation observer",V,W))},create:{id:function(){U=(Math.random().toString(16)+"000000000").substr(2,8),T="."+U,X.verbose("Creating unique id for element",U)},userChoice:function(b){var c,e,g;return!!(b=b||X.get.userValues())&&(b=a.isArray(b)?b:[b],a.each(b,function(b,f){!1===X.get.item(f)&&(g=q.templates.addition(X.add.variables(s.addResult,f)),e=a("<div />").html(g).attr("data-"+v.value,f).attr("data-"+v.text,f).addClass(r.addition).addClass(r.item),c=c===d?e:c.add(e),X.verbose("Creating user choices for value",f,e))}),c)},userLabels:function(b){var c=X.get.userValues();c&&(X.debug("Adding user labels",c),a.each(c,function(a,b){X.verbose("Adding custom user value"),X.add.label(b,b)}))},menu:function(){K=a("<div />").addClass(r.menu).appendTo(D)}},search:function(a){a=a!==d?a:X.get.query(),X.verbose("Searching for query",a),X.filter(a)},select:{firstUnfiltered:function(){X.verbose("Selecting first non-filtered element"),X.remove.selectedItem(),L.not(y.unselectable).eq(0).addClass(r.selected)},nextAvailable:function(a){a=a.eq(0);var b=a.nextAll(y.item).not(y.unselectable).eq(0),c=a.prevAll(y.item).not(y.unselectable).eq(0);b.length>0?(X.verbose("Moving selection to",b),b.addClass(r.selected)):(X.verbose("Moving selection to",c),c.addClass(r.selected))}},setup:{api:function(){var a={debug:q.debug,on:!1};X.verbose("First request, initializing API"),D.api(a)},layout:function(){D.is("select")&&(X.setup.select(),X.setup.returnedObject()),X.has.menu()||X.create.menu(),X.is.search()&&!X.has.search()&&(X.verbose("Adding search input"),G=a("<input />").addClass(r.search).prop("autocomplete","off").insertBefore(F)),q.allowTab&&X.set.tabbable()},select:function(){var b=X.get.selectValues();X.debug("Dropdown initialized on a select",b),D.is("select")&&(H=D),H.parent(y.dropdown).length>0?(X.debug("UI dropdown already exists. Creating dropdown menu only"),D=H.closest(y.dropdown),X.has.menu()||X.create.menu(),K=D.children(y.menu),X.setup.menu(b)):(X.debug("Creating entire dropdown from select"),D=a("<div />").attr("class",H.attr("class")).addClass(r.selection).addClass(r.dropdown).html(A.dropdown(b)).insertBefore(H),H.hasClass(r.multiple)&&!1===H.prop("multiple")&&(X.error(z.missingMultiple),H.prop("multiple",!0)),H.is("[multiple]")&&X.set.multiple(),H.prop("disabled")&&(X.debug("Disabling dropdown"),D.addClass(r.disabled)),H.removeAttr("class").detach().prependTo(D)),X.refresh()},menu:function(a){K.html(A.menu(a,t)),L=K.find(y.item)},reference:function(){X.debug("Dropdown behavior was called on select, replacing with closest dropdown"),D=D.parent(y.dropdown),X.refresh(),X.setup.returnedObject(),m&&(Q=X,X.invoke(l))},returnedObject:function(){var a=f.slice(0,p),b=f.slice(p+1);f=a.add(D).add(b)}},refresh:function(){X.refreshSelectors(),X.refreshData()},refreshSelectors:function(){X.verbose("Refreshing selector cache"),F=D.find(y.text),G=D.find(y.search),H=D.find(y.input),I=D.find(y.icon),J=D.prev().find(y.text).length>0?D.prev().find(y.text):D.prev(),K=D.children(y.menu),L=K.find(y.item)},refreshData:function(){X.verbose("Refreshing cached metadata"),L.removeData(v.text).removeData(v.value),D.removeData(v.defaultText).removeData(v.defaultValue).removeData(v.placeholderText)},toggle:function(){X.verbose("Toggling menu visibility"),X.is.active()?X.hide():X.show()},show:function(b){if(b=a.isFunction(b)?b:function(){},X.can.show()&&!X.is.active()){if(X.debug("Showing dropdown"),X.is.multiple()&&!X.has.search()&&X.is.allFiltered())return!0;!X.has.message()||X.has.maxSelections()||X.has.allResultsFiltered()||X.remove.message(),!1!==q.onShow.call(P)&&X.animate.show(function(){X.can.click()&&X.bind.intent(),X.set.visible(),b.call(P)})}},hide:function(b){b=a.isFunction(b)?b:function(){},X.is.active()&&(X.debug("Hiding dropdown"),!1!==q.onHide.call(P)&&X.animate.hide(function(){X.remove.visible(),b.call(P)}))},hideOthers:function(){X.verbose("Finding other dropdowns to hide"),f.not(D).has(y.menu+"."+r.visible).dropdown("hide")},hideMenu:function(){X.verbose("Hiding menu  instantaneously"),X.remove.active(),X.remove.visible(),K.transition("hide")},hideSubMenus:function(){var a=K.children(y.item).find(y.menu);X.verbose("Hiding sub menus",a),a.transition("hide")},bind:{events:function(){i&&X.bind.touchEvents(),X.bind.keyboardEvents(),X.bind.inputEvents(),X.bind.mouseEvents()},touchEvents:function(){X.debug("Touch device detected binding additional touch events"),X.is.searchSelection()||X.is.single()&&D.on("touchstart"+B,X.event.test.toggle),K.on("touchstart"+B,y.item,X.event.item.mouseenter)},keyboardEvents:function(){X.verbose("Binding keyboard events"),D.on("keydown"+B,X.event.keydown),X.has.search()&&D.on(X.get.inputEvent()+B,y.search,X.event.input),X.is.multiple()&&g.on("keydown"+T,X.event.document.keydown)},inputEvents:function(){X.verbose("Binding input change events"),D.on("change"+B,y.input,X.event.change)},mouseEvents:function(){X.verbose("Binding mouse events"),X.is.multiple()&&D.on("click"+B,y.label,X.event.label.click).on("click"+B,y.remove,X.event.remove.click),X.is.searchSelection()?(D.on("mousedown"+B,y.menu,X.event.menu.mousedown).on("mouseup"+B,y.menu,X.event.menu.mouseup).on("click"+B,y.icon,X.event.icon.click).on("click"+B,y.search,X.show).on("focus"+B,y.search,X.event.search.focus).on("blur"+B,y.search,X.event.search.blur).on("click"+B,y.text,X.event.text.focus),X.is.multiple()&&D.on("click"+B,X.event.click)):("click"==q.on?D.on("click"+B,y.icon,X.event.icon.click).on("click"+B,X.event.test.toggle):"hover"==q.on?D.on("mouseenter"+B,X.delay.show).on("mouseleave"+B,X.delay.hide):D.on(q.on+B,X.toggle),D.on("mousedown"+B,X.event.mousedown).on("mouseup"+B,X.event.mouseup).on("focus"+B,X.event.focus).on("blur"+B,X.event.blur)),K.on("mouseenter"+B,y.item,X.event.item.mouseenter).on("mouseleave"+B,y.item,X.event.item.mouseleave).on("click"+B,y.item,X.event.item.click)},intent:function(){X.verbose("Binding hide intent event to document"),i&&g.on("touchstart"+T,X.event.test.touch).on("touchmove"+T,X.event.test.touch),g.on("click"+T,X.event.test.hide)}},unbind:{intent:function(){X.verbose("Removing hide intent event from document"),i&&g.off("touchstart"+T).off("touchmove"+T),g.off("click"+T)}},filter:function(a){var b=a!==d?a:X.get.query(),c=function(){X.is.multiple()&&X.filterActive(),X.select.firstUnfiltered(),X.has.allResultsFiltered()?q.onNoResults.call(P,b)?q.allowAdditions||(X.verbose("All items filtered, showing message",b),X.add.message(s.noResults)):(X.verbose("All items filtered, hiding dropdown",b),X.hideMenu()):X.remove.message(),q.allowAdditions&&X.add.userSuggestion(a),X.is.searchSelection()&&X.can.show()&&X.is.focusedOnSearch()&&X.show()};q.useLabels&&X.has.maxSelections()||(q.apiSettings?X.can.useAPI()?X.queryRemote(b,function(){c()}):X.error(z.noAPI):(X.filterItems(b),c()))},queryRemote:function(b,c){var d={errorDuration:!1,throttle:q.throttle,urlData:{query:b},onError:function(){X.add.message(s.serverError),c()},onFailure:function(){X.add.message(s.serverError),c()},onSuccess:function(a){X.remove.message(),X.setup.menu({values:a[t.remoteValues]}),c()}};D.api("get request")||X.setup.api(),d=a.extend(!0,{},d,q.apiSettings),D.api("setting",d).api("query")},filterItems:function(b){var c=b!==d?b:X.get.query(),e=null,f=X.escape.regExp(c),g=new RegExp("^"+f,"igm");X.has.query()&&(e=[],X.verbose("Searching for matching values",c),L.each(function(){var d,f,b=a(this);if("both"==q.match||"text"==q.match){if(d=String(X.get.choiceText(b,!1)),-1!==d.search(g))return e.push(this),!0;if(q.fullTextSearch&&X.fuzzySearch(c,d))return e.push(this),!0}if("both"==q.match||"value"==q.match){if(f=String(X.get.choiceValue(b,d)),-1!==f.search(g))return e.push(this),!0;if(q.fullTextSearch&&X.fuzzySearch(c,f))return e.push(this),!0}})),X.debug("Showing only matched items",c),X.remove.filteredItem(),e&&L.not(e).addClass(r.filtered)},fuzzySearch:function(a,b){var c=b.length,d=a.length;if(a=a.toLowerCase(),b=b.toLowerCase(),d>c)return!1;if(d===c)return a===b;a:for(var e=0,f=0;e<d;e++){for(var g=a.charCodeAt(e);f<c;)if(b.charCodeAt(f++)===g)continue a;return!1}return!0},filterActive:function(){q.useLabels&&L.filter("."+r.active).addClass(r.filtered)},focusSearch:function(){X.is.search()&&!X.is.focusedOnSearch()&&G[0].focus()},forceSelection:function(){var a=L.not(r.filtered).filter("."+r.selected).eq(0),b=L.not(r.filtered).filter("."+r.active).eq(0),c=a.length>0?a:b,d=c.size()>0;if(X.has.query()){if(d)return X.debug("Forcing partial selection to selected item",c),void X.event.item.click.call(c);X.remove.searchTerm()}X.hide()},event:{change:function(){O||(X.debug("Input changed, updating selection"),X.set.selected())},focus:function(){q.showOnFocus&&!M&&X.is.hidden()&&!S&&X.show()},click:function(b){a(b.target).is(D)&&!X.is.focusedOnSearch()&&X.focusSearch()},blur:function(a){S=c.activeElement===this,M||S||(X.remove.activeLabel(),X.hide())},mousedown:function(){M=!0},mouseup:function(){M=!1},search:{focus:function(){M=!0,X.is.multiple()&&X.remove.activeLabel(),q.showOnFocus&&(X.search(),X.show())},blur:function(a){S=c.activeElement===this,N||S?S&&q.forceSelection&&X.forceSelection():X.is.multiple()?(X.remove.activeLabel(),X.hide()):q.forceSelection?X.forceSelection():X.hide()}},icon:{click:function(a){X.toggle(),a.stopPropagation()}},text:{focus:function(a){M=!0,X.focusSearch()}},input:function(a){(X.is.multiple()||X.is.searchSelection())&&X.set.filtered(),clearTimeout(X.timer),X.timer=setTimeout(X.search,q.delay.search)},label:{click:function(b){var c=a(this),d=D.find(y.label),e=d.filter("."+r.active),f=c.nextAll("."+r.active),g=c.prevAll("."+r.active),h=f.length>0?c.nextUntil(f).add(e).add(c):c.prevUntil(g).add(e).add(c);b.shiftKey?(e.removeClass(r.active),h.addClass(r.active)):b.ctrlKey?c.toggleClass(r.active):(e.removeClass(r.active),c.addClass(r.active)),q.onLabelSelect.apply(this,d.filter("."+r.active))}},remove:{click:function(){var b=a(this).parent();b.hasClass(r.active)?X.remove.activeLabels():X.remove.activeLabels(b)}},test:{toggle:function(a){var b=X.is.multiple()?X.show:X.toggle;X.determine.eventOnElement(a,b)&&a.preventDefault()},touch:function(a){X.determine.eventOnElement(a,function(){"touchstart"==a.type?X.timer=setTimeout(function(){X.hide()},q.delay.touch):"touchmove"==a.type&&clearTimeout(X.timer)}),a.stopPropagation()},hide:function(a){X.determine.eventInModule(a,X.hide)}},menu:{mousedown:function(){N=!0},mouseup:function(){N=!1}},item:{mouseenter:function(b){var c=a(this).children(y.menu),d=a(this).siblings(y.item).children(y.menu);c.length>0&&(clearTimeout(X.itemTimer),X.itemTimer=setTimeout(function(){X.verbose("Showing sub-menu",c),a.each(d,function(){X.animate.hide(!1,a(this))}),X.animate.show(!1,c)},q.delay.show),b.preventDefault())},mouseleave:function(b){var c=a(this).children(y.menu);c.length>0&&(clearTimeout(X.itemTimer),X.itemTimer=setTimeout(function(){X.verbose("Hiding sub-menu",c),X.animate.hide(!1,c)},q.delay.hide))},touchend:function(){},click:function(b){var c=a(this),d=a(b?b.target:""),e=c.find(y.menu),f=X.get.choiceText(c),g=X.get.choiceValue(c,f),h=e.length>0;e.find(d).length>0||h&&!q.allowCategorySelection||(q.useLabels||(X.remove.filteredItem(),X.remove.searchTerm(),X.set.scrollPosition(c)),X.determine.selectAction.call(this,f,g))}},document:{keydown:function(a){var b=a.which;if(X.is.inObject(b,u)){var d=D.find(y.label),e=d.filter("."+r.active),g=(e.data(v.value),d.index(e)),h=d.length,i=e.length>0,j=e.length>1,k=0===g,l=g+1==h,m=X.is.searchSelection(),n=X.is.focusedOnSearch(),o=X.is.focused(),p=n&&0===X.get.caretPosition();if(m&&!i&&!n)return;b==u.leftArrow?!o&&!p||i?i&&(a.shiftKey?X.verbose("Adding previous label to selection"):(X.verbose("Selecting previous label"),d.removeClass(r.active)),k&&!j?e.addClass(r.active):e.prev(y.siblingLabel).addClass(r.active).end(),a.preventDefault()):(X.verbose("Selecting previous label"),d.last().addClass(r.active)):b==u.rightArrow?(o&&!i&&d.first().addClass(r.active),i&&(a.shiftKey?X.verbose("Adding next label to selection"):(X.verbose("Selecting next label"),d.removeClass(r.active)),l?m?n?d.removeClass(r.active):X.focusSearch():j?e.next(y.siblingLabel).addClass(r.active):e.addClass(r.active):e.next(y.siblingLabel).addClass(r.active),a.preventDefault())):b==u.deleteKey||b==u.backspace?i?(X.verbose("Removing active labels"),l&&m&&!n&&X.focusSearch(),e.last().next(y.siblingLabel).addClass(r.active),X.remove.activeLabels(e),a.preventDefault()):p&&!i&&b==u.backspace&&(X.verbose("Removing last label on input backspace"),e=d.last().addClass(r.active),X.remove.activeLabels(e)):e.removeClass(r.active)}}},keydown:function(a){var b=a.which;if(X.is.inObject(b,u)){var o,d=L.not(y.unselectable).filter("."+r.selected).eq(0),e=K.children("."+r.active).eq(0),f=d.length>0?d:e,g=f.length>0?f.siblings(":not(."+r.filtered+")").andSelf():K.children(":not(."+r.filtered+")"),h=f.children(y.menu),i=f.closest(y.menu),j=i.hasClass(r.visible)||i.hasClass(r.animating)||i.parent(y.menu).length>0,k=h.length>0,l=f.length>0,m=f.not(y.unselectable).length>0,n=b==u.delimiter&&q.allowAdditions&&X.is.multiple();if(X.is.visible()){if((b==u.enter||n)&&(b==u.enter&&l&&k&&!q.allowCategorySelection?(X.verbose("Pressed enter on unselectable category, opening sub menu"),b=u.rightArrow):m&&(X.verbose("Selecting item from keyboard shortcut",f),X.event.item.click.call(f,a),X.is.searchSelection()&&X.remove.searchTerm()),a.preventDefault()),b==u.leftArrow&&i[0]!==K[0]&&(X.verbose("Left key pressed, closing sub-menu"),X.animate.hide(!1,i),f.removeClass(r.selected),i.closest(y.item).addClass(r.selected),a.preventDefault()),b==u.rightArrow&&k&&(X.verbose("Right key pressed, opening sub-menu"),X.animate.show(!1,h),f.removeClass(r.selected),h.find(y.item).eq(0).addClass(r.selected),a.preventDefault()),b==u.upArrow){if(o=l&&j?f.prevAll(y.item+":not("+y.unselectable+")").eq(0):L.eq(0),g.index(o)<0)return X.verbose("Up key pressed but reached top of current menu"),void a.preventDefault();X.verbose("Up key pressed, changing active item"),f.removeClass(r.selected),o.addClass(r.selected),X.set.scrollPosition(o),a.preventDefault()}if(b==u.downArrow){if(o=l&&j?o=f.nextAll(y.item+":not("+y.unselectable+")").eq(0):L.eq(0),0===o.length)return X.verbose("Down key pressed but reached bottom of current menu"),void a.preventDefault();X.verbose("Down key pressed, changing active item"),L.removeClass(r.selected),o.addClass(r.selected),X.set.scrollPosition(o),a.preventDefault()}b==u.pageUp&&(X.scrollPage("up"),a.preventDefault()),b==u.pageDown&&(X.scrollPage("down"),a.preventDefault()),b==u.escape&&(X.verbose("Escape key pressed, closing dropdown"),X.hide())}else n&&a.preventDefault(),b==u.downArrow&&(X.verbose("Down key pressed, showing dropdown"),X.show(),a.preventDefault())}else X.is.selection()&&!X.is.search()&&X.set.selectedLetter(String.fromCharCode(b))}},trigger:{change:function(){var a=c.createEvent("HTMLEvents"),b=H[0];b&&(X.verbose("Triggering native change event"),a.initEvent("change",!0,!1),b.dispatchEvent(a))}},determine:{selectAction:function(b,c){X.verbose("Determining action",q.action),a.isFunction(X.action[q.action])?(X.verbose("Triggering preset action",q.action,b,c),X.action[q.action].call(this,b,c)):a.isFunction(q.action)?(X.verbose("Triggering user action",q.action,b,c),q.action.call(this,b,c)):X.error(z.action,q.action)},eventInModule:function(b,d){var e=a(b.target),f=e.closest(c.documentElement).length>0,g=e.closest(D).length>0;return d=a.isFunction(d)?d:function(){},f&&!g?(X.verbose("Triggering event",d),d(),!0):(X.verbose("Event occurred in dropdown, canceling callback"),!1)},eventOnElement:function(b,c){var d=a(b.target),e=d.closest(y.siblingLabel),f=0===D.find(e).length,g=0===d.closest(K).length;return c=a.isFunction(c)?c:function(){},f&&g?(X.verbose("Triggering event",c),c(),!0):(X.verbose("Event occurred in dropdown menu, canceling callback"),!1)}},action:{nothing:function(){},activate:function(b,c){if(c=c!==d?c:b,X.can.activate(a(this))){if(X.set.selected(c,a(this)),X.is.multiple()&&!X.is.allFiltered())return;X.hideAndClear()}},select:function(a,b){X.action.activate.call(this)},combo:function(b,c){c=c!==d?c:b,X.set.selected(c,a(this)),X.hideAndClear()},hide:function(a,b){X.set.value(b),X.hideAndClear()}},get:{id:function(){return U},defaultText:function(){return D.data(v.defaultText)},defaultValue:function(){return D.data(v.defaultValue)},placeholderText:function(){return D.data(v.placeholderText)||""},text:function(){return F.text()},query:function(){return a.trim(G.val())},searchWidth:function(a){return a*q.glyphWidth+"em"},selectionCount:function(){var b=X.get.values();return X.is.multiple()?a.isArray(b)?b.length:0:""!==X.get.value()?1:0},transition:function(a){return"auto"==q.transition?X.is.upward(a)?"slide up":"slide down":q.transition},userValues:function(){var b=X.get.values();return!!b&&(b=a.isArray(b)?b:[b],a.grep(b,function(a){return!1===X.get.item(a)}))},uniqueArray:function(b){return a.grep(b,function(c,d){return a.inArray(c,b)===d})},caretPosition:function(){var b,d,a=G.get(0);return"selectionStart"in a?a.selectionStart:c.selection?(a.focus(),b=c.selection.createRange(),d=b.text.length,b.moveStart("character",-a.value.length),b.text.length-d):void 0},value:function(){var b=H.length>0?H.val():D.data(v.value);return a.isArray(b)&&1===b.length&&""===b[0]?"":b},values:function(){var a=X.get.value();return""===a?"":!X.has.selectInput()&&X.is.multiple()?"string"==typeof a?a.split(q.delimiter):"":a},remoteValues:function(){var b=X.get.values(),c=!1;return b&&("string"==typeof b&&(b=[b]),c={},a.each(b,function(a,b){var d=X.read.remoteData(b);X.verbose("Restoring value from session data",d,b),c[b]=d||b})),c},choiceText:function(b,c){if(c=c!==d?c:q.preserveHTML,b)return b.find(y.menu).length>0&&(X.verbose("Retreiving text of element with sub-menu"),b=b.clone(),b.find(y.menu).remove(),b.find(y.menuIcon).remove()),b.data(v.text)!==d?b.data(v.text):c?a.trim(b.html()):a.trim(b.text())},choiceValue:function(b,c){return c=c||X.get.choiceText(b),!!b&&(b.data(v.value)!==d?String(b.data(v.value)):"string"==typeof c?a.trim(c.toLowerCase()):String(c))},inputEvent:function(){var a=G[0];return!!a&&(a.oninput!==d?"input":a.onpropertychange!==d?"propertychange":"keyup")},selectValues:function(){var b={};return b.values=[],D.find("option").each(function(){var c=a(this),e=c.html(),f=c.attr("disabled"),g=c.attr("value")!==d?c.attr("value"):e;"auto"===q.placeholder&&""===g?b.placeholder=e:b.values.push({name:e,value:g,disabled:f})}),q.placeholder&&"auto"!==q.placeholder&&(X.debug("Setting placeholder value to",q.placeholder),b.placeholder=q.placeholder),q.sortSelect?(b.values.sort(function(a,b){return a.name>b.name?1:-1}),X.debug("Retrieved and sorted values from select",b)):X.debug("Retreived values from select",b),b},activeItem:function(){return L.filter("."+r.active)},selectedItem:function(){var a=L.not(y.unselectable).filter("."+r.selected);return a.length>0?a:L.eq(0)},itemWithAdditions:function(a){var b=X.get.item(a),c=X.create.userChoice(a);return c&&c.length>0&&(b=b.length>0?b.add(c):c),b},item:function(b,c){var f,g,e=!1;return b=b!==d?b:X.get.values()!==d?X.get.values():X.get.text(),f=g?b.length>0:b!==d&&null!==b,g=X.is.multiple()&&a.isArray(b),c=""===b||0===b||(c||!1),f&&L.each(function(){var f=a(this),h=X.get.choiceText(f),i=X.get.choiceValue(f,h);if(null!==i&&i!==d)if(g)-1===a.inArray(String(i),b)&&-1===a.inArray(h,b)||(e=e?e.add(f):f);else if(c){if(X.verbose("Ambiguous dropdown value using strict type check",f,b),i===b||h===b)return e=f,!0}else if(String(i)==String(b)||h==b)return X.verbose("Found select item by value",i,b),e=f,!0}),e}},check:{maxSelections:function(a){return!q.maxSelections||(a=a!==d?a:X.get.selectionCount(),a>=q.maxSelections?(X.debug("Maximum selection count reached"),q.useLabels&&(L.addClass(r.filtered),X.add.message(s.maxSelections)),!0):(X.verbose("No longer at maximum selection count"),X.remove.message(),X.remove.filteredItem(),X.is.searchSelection()&&X.filterItems(),!1))}},restore:{defaults:function(){X.clear(),X.restore.defaultText(),X.restore.defaultValue()},defaultText:function(){var a=X.get.defaultText();a===X.get.placeholderText?(X.debug("Restoring default placeholder text",a),X.set.placeholderText(a)):(X.debug("Restoring default text",a),X.set.text(a))},defaultValue:function(){var a=X.get.defaultValue();a!==d&&(X.debug("Restoring default value",a),""!==a?(X.set.value(a),X.set.selected()):(X.remove.activeItem(),X.remove.selectedItem()))},labels:function(){q.allowAdditions&&(q.useLabels||(X.error(z.labels),q.useLabels=!0),X.debug("Restoring selected values"),X.create.userLabels()),X.check.maxSelections()},selected:function(){X.restore.values(),X.is.multiple()?(X.debug("Restoring previously selected values and labels"),X.restore.labels()):X.debug("Restoring previously selected values")},values:function(){X.set.initialLoad(),q.apiSettings?q.saveRemoteData?X.restore.remoteValues():X.clearValue():X.set.selected(),X.remove.initialLoad()},remoteValues:function(){var b=X.get.remoteValues();X.debug("Recreating selected from session data",b),b&&(X.is.single()?a.each(b,function(a,b){X.set.text(b)}):a.each(b,function(a,b){X.add.label(a,b)}))}},read:{remoteData:function(a){var c;return b.Storage===d?void X.error(z.noStorage):(c=sessionStorage.getItem(a))!==d&&c}},save:{defaults:function(){X.save.defaultText(),X.save.placeholderText(),X.save.defaultValue()},defaultValue:function(){var a=X.get.value();X.verbose("Saving default value as",a),D.data(v.defaultValue,a)},defaultText:function(){var a=X.get.text();X.verbose("Saving default text as",a),D.data(v.defaultText,a)},placeholderText:function(){var a;!1!==q.placeholder&&F.hasClass(r.placeholder)&&(a=X.get.text(),X.verbose("Saving placeholder text as",a),D.data(v.placeholderText,a))},remoteData:function(a,c){if(b.Storage===d)return void X.error(z.noStorage);X.verbose("Saving remote data to session storage",c,a),sessionStorage.setItem(c,a)}},clear:function(){X.is.multiple()?X.remove.labels():(X.remove.activeItem(),X.remove.selectedItem()),X.set.placeholderText(),X.clearValue()},clearValue:function(){X.set.value("")},scrollPage:function(a,b){var l,m,n,c=b||X.get.selectedItem(),d=c.closest(y.menu),e=d.outerHeight(),f=d.scrollTop(),g=L.eq(0).outerHeight(),h=Math.floor(e/g),j=(d.prop("scrollHeight"),"up"==a?f-g*h:f+g*h),k=L.not(y.unselectable);n="up"==a?k.index(c)-h:k.index(c)+h,l="up"==a?n>=0:n<k.length,m=l?k.eq(n):"up"==a?k.first():k.last(),m.length>0&&(X.debug("Scrolling page",a,m),c.removeClass(r.selected),m.addClass(r.selected),d.scrollTop(j))},set:{filtered:function(){var a=X.is.multiple(),b=X.is.searchSelection(),c=a&&b,d=b?X.get.query():"",e="string"==typeof d&&d.length>0,f=X.get.searchWidth(d.length),g=""!==d;a&&e&&(X.verbose("Adjusting input width",f,q.glyphWidth),G.css("width",f)),e||c&&g?(X.verbose("Hiding placeholder text"),F.addClass(r.filtered)):(!a||c&&!g)&&(X.verbose("Showing placeholder text"),F.removeClass(r.filtered))},loading:function(){D.addClass(r.loading)},placeholderText:function(a){a=a||X.get.placeholderText(),X.debug("Setting placeholder text",a),X.set.text(a),F.addClass(r.placeholder)},tabbable:function(){X.has.search()?(X.debug("Added tabindex to searchable dropdown"),G.val("").attr("tabindex",0),K.attr("tabindex",-1)):(X.debug("Added tabindex to dropdown"),D.attr("tabindex")===d&&(D.attr("tabindex",0),K.attr("tabindex",-1)))},initialLoad:function(){X.verbose("Setting initial load"),R=!0},activeItem:function(a){q.allowAdditions&&a.filter(y.addition).length>0?a.addClass(r.filtered):a.addClass(r.active)},scrollPosition:function(a,b){var e,f,g,i,j,k,l,m,n,c=5;a=a||X.get.selectedItem(),e=a.closest(y.menu),f=a&&a.length>0,b=b!==d&&b,a&&e.length>0&&f&&(i=a.position().top,e.addClass(r.loading),k=e.scrollTop(),j=e.offset().top,i=a.offset().top,g=k-j+i,b||(l=e.height(),n=k+l<g+c,m=g-c<k),X.debug("Scrolling to active item",g),(b||m||n)&&e.scrollTop(g),e.removeClass(r.loading))},text:function(a){"select"!==q.action&&("combo"==q.action?(X.debug("Changing combo button text",a,J),q.preserveHTML?J.html(a):J.text(a)):(a!==X.get.placeholderText()&&F.removeClass(r.placeholder),X.debug("Changing text",a,F),F.removeClass(r.filtered),q.preserveHTML?F.html(a):F.text(a)))},selectedLetter:function(b){var f,c=L.filter("."+r.selected),d=c.length>0&&X.has.firstLetter(c,b),e=!1;d&&(f=c.nextAll(L).eq(0),X.has.firstLetter(f,b)&&(e=f)),e||L.each(function(){if(X.has.firstLetter(a(this),b))return e=a(this),!1}),e&&(X.verbose("Scrolling to next value with letter",b),X.set.scrollPosition(e),c.removeClass(r.selected),e.addClass(r.selected))},direction:function(a){"auto"==q.direction?X.is.onScreen(a)?X.remove.upward(a):X.set.upward(a):"upward"==q.direction&&X.set.upward(a)},upward:function(a){(a||D).addClass(r.upward)},value:function(a,b,c){var e=H.length>0,g=(X.has.value(a),X.get.values()),h=a!==d?String(a):a;if(e){if(h==g&&(X.verbose("Skipping value update already same value",a,g),!X.is.initialLoad()))return;X.is.single()&&X.has.selectInput()&&X.can.extendSelect()&&(X.debug("Adding user option",a),X.add.optionValue(a)),X.debug("Updating input value",a,g),O=!0,H.val(a),!1===q.fireOnInit&&X.is.initialLoad()?X.debug("Input native change event ignored on initial load"):X.trigger.change(),O=!1}else X.verbose("Storing value in metadata",a,H),a!==g&&D.data(v.value,h);!1===q.fireOnInit&&X.is.initialLoad()?X.verbose("No callback on initial load",q.onChange):q.onChange.call(P,a,b,c)},active:function(){D.addClass(r.active)},multiple:function(){D.addClass(r.multiple)},visible:function(){D.addClass(r.visible)},exactly:function(a,b){X.debug("Setting selected to exact values"),X.clear(),X.set.selected(a,b)},selected:function(b,c){var d=X.is.multiple();(c=q.allowAdditions?c||X.get.itemWithAdditions(b):c||X.get.item(b))&&(X.debug("Setting selected menu item to",c),X.is.single()?(X.remove.activeItem(),X.remove.selectedItem()):q.useLabels&&X.remove.selectedItem(),c.each(function(){var b=a(this),e=X.get.choiceText(b),f=X.get.choiceValue(b,e),g=b.hasClass(r.filtered),h=b.hasClass(r.active),i=b.hasClass(r.addition),j=d&&1==c.length;d?!h||i?(q.apiSettings&&q.saveRemoteData&&X.save.remoteData(e,f),q.useLabels?(X.add.value(f,e,b),X.add.label(f,e,j),X.set.activeItem(b),X.filterActive(),X.select.nextAvailable(c)):(X.add.value(f,e,b),X.set.text(X.add.variables(s.count)),X.set.activeItem(b))):g||(X.debug("Selected active value, removing label"),X.remove.selected(f)):(q.apiSettings&&q.saveRemoteData&&X.save.remoteData(e,f),X.set.text(e),X.set.value(f,e,b),b.addClass(r.active).addClass(r.selected))}))}},add:{label:function(b,c,d){var f,e=X.is.searchSelection()?G:F;if(f=a("<a />").addClass(r.label).attr("data-value",b).html(A.label(b,c)),f=q.onLabelCreate.call(f,b,c),X.has.label(b))return void X.debug("Label already exists, skipping",b);q.label.variation&&f.addClass(q.label.variation),!0===d?(X.debug("Animating in label",f),f.addClass(r.hidden).insertBefore(e).transition(q.label.transition,q.label.duration)):(X.debug("Adding selection label",f),f.insertBefore(e))},message:function(b){var c=K.children(y.message),d=q.templates.message(X.add.variables(b));c.length>0?c.html(d):c=a("<div/>").html(d).addClass(r.message).appendTo(K)},optionValue:function(b){H.find('option[value="'+b+'"]').length>0||(V&&(V.disconnect(),X.verbose("Temporarily disconnecting mutation observer",b)),X.is.single()&&(X.verbose("Removing previous user addition"),H.find("option."+r.addition).remove()),a("<option/>").prop("value",b).addClass(r.addition).html(b).appendTo(H),X.verbose("Adding user addition as an <option>",b),V&&V.observe(H[0],{childList:!0,subtree:!0}))},userSuggestion:function(a){var f,b=K.children(y.addition),c=X.get.item(a),d=c&&c.not(y.addition).length,e=b.length>0;if(!q.useLabels||!X.has.maxSelections()){if(""===a||d)return void b.remove();L.removeClass(r.selected),e?(f=q.templates.addition(X.add.variables(s.addResult,a)),b.html(f).attr("data-"+v.value,a).attr("data-"+v.text,a).removeClass(r.filtered).addClass(r.selected),X.verbose("Replacing user suggestion with new value",b)):(b=X.create.userChoice(a),b.prependTo(K).addClass(r.selected),X.verbose("Adding item choice to menu corresponding with user choice addition",b))}},variables:function(a,b){var g,h,c=-1!==a.search("{count}"),d=-1!==a.search("{maxCount}"),e=-1!==a.search("{term}");return X.verbose("Adding templated variables to message",a),c&&(g=X.get.selectionCount(),a=a.replace("{count}",g)),d&&(g=X.get.selectionCount(),a=a.replace("{maxCount}",q.maxSelections)),e&&(h=b||X.get.query(),a=a.replace("{term}",h)),a},value:function(b,c,d){var f,e=X.get.values();if(""===b)return void X.debug("Cannot select blank values from multiselect");a.isArray(e)?(f=e.concat([b]),f=X.get.uniqueArray(f)):f=[b],X.has.selectInput()?X.can.extendSelect()&&(X.debug("Adding value to select",b,f,H),X.add.optionValue(b)):(f=f.join(q.delimiter),X.debug("Setting hidden input to delimited value",f,H)),!1===q.fireOnInit&&X.is.initialLoad()?X.verbose("Skipping onadd callback on initial load",q.onAdd):q.onAdd.call(P,b,c,d),X.set.value(f,b,c,d),X.check.maxSelections()}},remove:{active:function(){D.removeClass(r.active)},activeLabel:function(){D.find(y.label).removeClass(r.active)},loading:function(){D.removeClass(r.loading)},initialLoad:function(){R=!1},upward:function(a){(a||D).removeClass(r.upward)},visible:function(){D.removeClass(r.visible)},activeItem:function(){L.removeClass(r.active)},filteredItem:function(){q.useLabels&&X.has.maxSelections()||(q.useLabels&&X.is.multiple()?L.not("."+r.active).removeClass(r.filtered):L.removeClass(r.filtered))},optionValue:function(a){var b=H.find('option[value="'+a+'"]');b.length>0&&b.hasClass(r.addition)&&(V&&(V.disconnect(),X.verbose("Temporarily disconnecting mutation observer",a)),b.remove(),X.verbose("Removing user addition as an <option>",a),V&&V.observe(H[0],{childList:!0,subtree:!0}))},message:function(){K.children(y.message).remove()},searchTerm:function(){X.verbose("Cleared search term"),G.val(""),X.set.filtered()},selected:function(b,c){if(!(c=q.allowAdditions?c||X.get.itemWithAdditions(b):c||X.get.item(b)))return!1;c.each(function(){var b=a(this),c=X.get.choiceText(b),d=X.get.choiceValue(b,c);X.is.multiple()?q.useLabels?(X.remove.value(d,c,b),X.remove.label(d)):(X.remove.value(d,c,b),0===X.get.selectionCount()?X.set.placeholderText():X.set.text(X.add.variables(s.count))):X.remove.value(d,c,b),b.removeClass(r.filtered).removeClass(r.active),q.useLabels&&b.removeClass(r.selected)})},selectedItem:function(){L.removeClass(r.selected)},value:function(a,b,c){var e,d=X.get.values();X.has.selectInput()?(X.verbose("Input is <select> removing selected option",a),e=X.remove.arrayValue(a,d),X.remove.optionValue(a)):(X.verbose("Removing from delimited values",a),e=X.remove.arrayValue(a,d),e=e.join(q.delimiter)),!1===q.fireOnInit&&X.is.initialLoad()?X.verbose("No callback on initial load",q.onRemove):q.onRemove.call(P,a,b,c),X.set.value(e,b,c),X.check.maxSelections()},arrayValue:function(b,c){return a.isArray(c)||(c=[c]),c=a.grep(c,function(a){return b!=a}),X.verbose("Removed value from delimited string",b,c),c},label:function(a,b){var c=D.find(y.label),d=c.filter('[data-value="'+a+'"]');X.verbose("Removing label",d),d.remove()},activeLabels:function(a){a=a||D.find(y.label).filter("."+r.active),X.verbose("Removing active label selections",a),X.remove.labels(a)},labels:function(b){b=b||D.find(y.label),X.verbose("Removing labels",b),b.each(function(){var b=a(this),c=b.data(v.value),e=c!==d?String(c):c,f=X.is.userValue(e);if(!1===q.onLabelRemove.call(b,c))return void X.debug("Label remove callback cancelled removal");f?(X.remove.value(e),X.remove.label(e)):X.remove.selected(e)})},tabbable:function(){X.has.search()?(X.debug("Searchable dropdown initialized"),G.removeAttr("tabindex"),K.removeAttr("tabindex")):(X.debug("Simple selection dropdown initialized"),D.removeAttr("tabindex"),K.removeAttr("tabindex"))}},has:{search:function(){return G.length>0},selectInput:function(){return H.is("select")},firstLetter:function(a,b){var c,d;return!(!a||0===a.length||"string"!=typeof b)&&(c=X.get.choiceText(a,!1),b=b.toLowerCase(),d=String(c).charAt(0).toLowerCase(),b==d)},input:function(){return H.length>0},items:function(){return L.length>0},menu:function(){return K.length>0},message:function(){return 0!==K.children(y.message).length},label:function(a){return D.find(y.label).filter('[data-value="'+a+'"]').length>0},maxSelections:function(){return q.maxSelections&&X.get.selectionCount()>=q.maxSelections},allResultsFiltered:function(){return L.filter(y.unselectable).length===L.length},query:function(){return""!==X.get.query()},value:function(b){var c=X.get.values();return!!(a.isArray(c)?c&&-1!==a.inArray(b,c):c==b)}},is:{active:function(){return D.hasClass(r.active)},alreadySetup:function(){return D.is("select")&&D.parent(y.dropdown).length>0&&0===D.prev().length},animating:function(a){return a?a.transition&&a.transition("is animating"):K.transition&&K.transition("is animating")},disabled:function(){return D.hasClass(r.disabled)},focused:function(){return c.activeElement===D[0]},focusedOnSearch:function(){return c.activeElement===G[0]},allFiltered:function(){return(X.is.multiple()||X.has.search())&&!X.has.message()&&X.has.allResultsFiltered()},hidden:function(a){return!X.is.visible(a)},initialLoad:function(){return R},onScreen:function(a){var e,b=a||K,c=!0,d={};return b.addClass(r.loading),e={context:{scrollTop:E.scrollTop(),height:E.outerHeight()},menu:{offset:b.offset(),height:b.outerHeight()}},d={above:e.context.scrollTop<=e.menu.offset.top-e.menu.height,below:e.context.scrollTop+e.context.height>=e.menu.offset.top+e.menu.height},d.below?(X.verbose("Dropdown can fit in context downward",d),c=!0):d.below||d.above?(X.verbose("Dropdown cannot fit below, opening upward",d),c=!1):(X.verbose("Dropdown cannot fit in either direction, favoring downward",d),c=!0),b.removeClass(r.loading),c},inObject:function(b,c){var d=!1;return a.each(c,function(a,c){if(c==b)return d=!0,!0}),d},multiple:function(){return D.hasClass(r.multiple)},single:function(){return!X.is.multiple()},selectMutation:function(b){var c=!1;return a.each(b,function(b,d){if(d.target&&a(d.target).is("select"))return c=!0,!0}),c},search:function(){return D.hasClass(r.search)},searchSelection:function(){return X.has.search()&&1===G.parent(y.dropdown).length},selection:function(){return D.hasClass(r.selection)},userValue:function(b){return-1!==a.inArray(b,X.get.userValues())},upward:function(a){return(a||D).hasClass(r.upward)},visible:function(a){return a?a.hasClass(r.visible):K.hasClass(r.visible)}},can:{activate:function(a){return!!q.useLabels||(!X.has.maxSelections()||!(!X.has.maxSelections()||!a.hasClass(r.active)))},click:function(){return i||"click"==q.on},extendSelect:function(){return q.allowAdditions||q.apiSettings},show:function(){return!X.is.disabled()&&(X.has.items()||X.has.message())},useAPI:function(){return a.fn.api!==d}},animate:{show:function(b,c){var g,e=c||K,f=c?function(){}:function(){X.hideSubMenus(),X.hideOthers(),X.set.active()};b=a.isFunction(b)?b:function(){},X.verbose("Doing menu show animation",e),X.set.direction(c),g=X.get.transition(c),X.is.selection()&&X.set.scrollPosition(X.get.selectedItem(),!0),(X.is.hidden(e)||X.is.animating(e))&&("none"==g?(f(),e.transition("show"),b.call(P)):a.fn.transition!==d&&D.transition("is supported")?e.transition({animation:g+" in",debug:q.debug,verbose:q.verbose,duration:q.duration,queue:!0,onStart:f,onComplete:function(){b.call(P)}}):X.error(z.noTransition,g))},hide:function(b,c){var e=c||K,g=(c?q.duration:q.duration,c?function(){}:function(){X.can.click()&&X.unbind.intent(),X.remove.active()}),h=X.get.transition(c);b=a.isFunction(b)?b:function(){},(X.is.visible(e)||X.is.animating(e))&&(X.verbose("Doing menu hide animation",e),"none"==h?(g(),e.transition("hide"),b.call(P)):a.fn.transition!==d&&D.transition("is supported")?e.transition({animation:h+" out",duration:q.duration,debug:q.debug,verbose:q.verbose,queue:!0,onStart:g,onComplete:function(){"auto"==q.direction&&X.remove.upward(c),b.call(P)}}):X.error(z.transition))}},hideAndClear:function(){X.remove.searchTerm(),X.has.maxSelections()||(X.has.search()?X.hide(function(){X.remove.filteredItem()}):X.hide())},delay:{show:function(){X.verbose("Delaying show event to ensure user intent"),clearTimeout(X.timer),X.timer=setTimeout(X.show,q.delay.show)},hide:function(){X.verbose("Delaying hide event to ensure user intent"),clearTimeout(X.timer),X.timer=setTimeout(X.hide,q.delay.hide)}},escape:{regExp:function(a){return a=String(a),a.replace(x.escape,"\\$&")}},setting:function(b,c){if(X.debug("Changing setting",b,c),a.isPlainObject(b))a.extend(!0,q,b);else{if(c===d)return q[b];q[b]=c}},internal:function(b,c){if(a.isPlainObject(b))a.extend(!0,X,b);else{if(c===d)return X[b];X[b]=c}},debug:function(){q.debug&&(q.performance?X.performance.log(arguments):(X.debug=Function.prototype.bind.call(console.info,console,q.name+":"),X.debug.apply(console,arguments)))},verbose:function(){q.verbose&&q.debug&&(q.performance?X.performance.log(arguments):(X.verbose=Function.prototype.bind.call(console.info,console,q.name+":"),X.verbose.apply(console,arguments)))},error:function(){X.error=Function.prototype.bind.call(console.error,console,q.name+":"),X.error.apply(console,arguments)},performance:{log:function(a){var b,c,d;q.performance&&(b=(new Date).getTime(),d=j||b,c=b-d,j=b,k.push({Name:a[0],Arguments:[].slice.call(a,1)||"",Element:P,"Execution Time":c})),clearTimeout(X.performance.timer),X.performance.timer=setTimeout(X.performance.display,500)},display:function(){var b=q.name+":",c=0;j=!1,clearTimeout(X.performance.timer),a.each(k,function(a,b){c+=b["Execution Time"]}),b+=" "+c+"ms",h&&(b+=" '"+h+"'"),(console.group!==d||console.table!==d)&&k.length>0&&(console.groupCollapsed(b),console.table?console.table(k):a.each(k,function(a,b){console.log(b.Name+": "+b["Execution Time"]+"ms")}),console.groupEnd()),k=[]}},invoke:function(b,c,e){var g,h,i,f=Q;return c=c||n,e=P||e,"string"==typeof b&&f!==d&&(b=b.split(/[\. ]/),g=b.length-1,a.each(b,function(c,e){var i=c!=g?e+b[c+1].charAt(0).toUpperCase()+b[c+1].slice(1):b;if(a.isPlainObject(f[i])&&c!=g)f=f[i];else{if(f[i]!==d)return h=f[i],!1;if(!a.isPlainObject(f[e])||c==g)return f[e]!==d?(h=f[e],!1):(X.error(z.method,b),!1);f=f[e]}})),a.isFunction(h)?i=h.apply(e,c):h!==d&&(i=h),a.isArray(o)?o.push(i):o!==d?o=[o,i]:i!==d&&(o=i),h}},m?(Q===d&&X.initialize(),X.invoke(l)):(Q!==d&&Q.invoke("destroy"),X.initialize())}),o!==d?o:f},a.fn.dropdown.settings={debug:!1,verbose:!1,performance:!0,on:"click",action:"activate",apiSettings:!1,saveRemoteData:!0,throttle:200,context:b,direction:"auto",keepOnScreen:!0,match:"both",fullTextSearch:!1,placeholder:"auto",preserveHTML:!0,sortSelect:!1,forceSelection:!0,allowAdditions:!1,maxSelections:!1,useLabels:!0,delimiter:",",showOnFocus:!0,allowTab:!0,allowCategorySelection:!1,fireOnInit:!1,transition:"auto",duration:200,glyphWidth:1.0714,label:{transition:"scale",duration:200,variation:!1},delay:{hide:300,show:200,search:20,touch:50},onChange:function(a,b,c){},onAdd:function(a,b,c){},onRemove:function(a,b,c){},onLabelSelect:function(a){},onLabelCreate:function(b,c){return a(this)},onLabelRemove:function(a){return!0},onNoResults:function(a){return!0},onShow:function(){},onHide:function(){},name:"Dropdown",namespace:"dropdown",message:{addResult:"Add <b>{term}</b>",count:"{count} selected",maxSelections:"Max {maxCount} selections",noResults:"No results found.",serverError:"There was an error contacting the server"},error:{action:"You called a dropdown action that was not defined",alreadySetup:"Once a select has been initialized behaviors must be called on the created ui dropdown",labels:"Allowing user additions currently requires the use of labels.",missingMultiple:"<select> requires multiple property to be set to correctly preserve multiple values",method:"The method you called is not defined.",noAPI:"The API module is required to load resources remotely",noStorage:"Saving remote data requires session storage",noTransition:"This module requires ui transitions <https://github.com/Semantic-Org/UI-Transition>"},regExp:{escape:/[-[\]{}()*+?.,\\^$|#\s]/g},metadata:{defaultText:"defaultText",defaultValue:"defaultValue",placeholderText:"placeholder",text:"text",value:"value"},fields:{remoteValues:"results",values:"values",name:"name",value:"value"},keys:{backspace:8,delimiter:188,deleteKey:46,enter:13,escape:27,pageUp:33,pageDown:34,leftArrow:37,upArrow:38,rightArrow:39,downArrow:40},selector:{addition:".addition",dropdown:".ui.dropdown",icon:"> .dropdown.icon",input:'> input[type="hidden"], > select',item:".item",label:"> .label",remove:"> .label > .delete.icon",siblingLabel:".label",menu:".menu",message:".message",menuIcon:".dropdown.icon",search:"input.search, .menu > .search > input",text:"> .text:not(.icon)",unselectable:".disabled, .filtered"},className:{active:"active",addition:"addition",animating:"animating",disabled:"disabled",dropdown:"ui dropdown",filtered:"filtered",hidden:"hidden transition",item:"item",label:"ui label",loading:"loading",menu:"menu",message:"message",multiple:"multiple",placeholder:"default",search:"search",selected:"selected",selection:"selection",upward:"upward",visible:"visible"}},a.fn.dropdown.settings.templates={dropdown:function(b){var c=b.placeholder||!1,e=(b.values,"");return e+='<i class="dropdown icon"></i>',b.placeholder?e+='<div class="default text">'+c+"</div>":e+='<div class="text"></div>',e+='<div class="menu">',a.each(b.values,function(a,b){e+=b.disabled?'<div class="disabled item" data-value="'+b.value+'">'+b.name+"</div>":'<div class="item" data-value="'+b.value+'">'+b.name+"</div>"}),e+="</div>"},menu:function(b,c){var d=b[c.values]||{},e="";return a.each(d,function(a,b){e+='<div class="item" data-value="'+b[c.value]+'">'+b[c.name]+"</div>"}),e},label:function(a,b){return b+'<i class="delete icon"></i>'},message:function(a){return a},addition:function(a){return a}}}(jQuery,window,document);

        /*!
         * # Semantic UI 2.1.7 - Transition
         * http://github.com/semantic-org/semantic-ui/
         *
         *
         * Copyright 2015 Contributors
         * Released under the MIT license
         * http://opensource.org/licenses/MIT
         *
         */
        !function(a,b,c,d){"use strict";a.fn.transition=function(){var n,e=a(this),f=e.selector||"",g=(new Date).getTime(),h=[],i=arguments,j=i[0],k=[].slice.call(arguments,1),l="string"==typeof j;b.requestAnimationFrame||b.mozRequestAnimationFrame||b.webkitRequestAnimationFrame||b.msRequestAnimationFrame;return e.each(function(b){var p,q,r,s,t,u,x,y,z,m=a(this),o=this;z={initialize:function(){p=z.get.settings.apply(o,i),s=p.className,r=p.error,t=p.metadata,y="."+p.namespace,x="module-"+p.namespace,q=m.data(x)||z,u=z.get.animationEndEvent(),l&&(l=z.invoke(j)),!1===l&&(z.verbose("Converted arguments into settings object",p),p.interval?z.delay(p.animate):z.animate(),z.instantiate())},instantiate:function(){z.verbose("Storing instance of module",z),q=z,m.data(x,q)},destroy:function(){z.verbose("Destroying previous module for",o),m.removeData(x)},refresh:function(){z.verbose("Refreshing display type on next animation"),delete z.displayType},forceRepaint:function(){z.verbose("Forcing element repaint");var a=m.parent(),b=m.next();0===b.length?m.detach().appendTo(a):m.detach().insertBefore(b)},repaint:function(){z.verbose("Repainting element");o.offsetWidth},delay:function(a){var f,g,c=z.get.animationDirection();c||(c=z.can.transition()?z.get.direction():"static"),a=a!==d?a:p.interval,f="auto"==p.reverse&&c==s.outward,g=f||1==p.reverse?(e.length-b)*p.interval:b*p.interval,z.debug("Delaying animation by",g),setTimeout(z.animate,g)},animate:function(a){if(p=a||p,!z.is.supported())return z.error(r.support),!1;if(z.debug("Preparing animation",p.animation),z.is.animating()){if(p.queue)return!p.allowRepeats&&z.has.direction()&&z.is.occurring()&&!0!==z.queuing?z.debug("Animation is currently occurring, preventing queueing same animation",p.animation):z.queue(p.animation),!1;if(!p.allowRepeats&&z.is.occurring())return z.debug("Animation is already occurring, will not execute repeated animation",p.animation),!1;z.debug("New animation started, completing previous early",p.animation),q.complete()}z.can.animate()?z.set.animating(p.animation):z.error(r.noAnimation,p.animation,o)},reset:function(){z.debug("Resetting animation to beginning conditions"),z.remove.animationCallbacks(),z.restore.conditions(),z.remove.animating()},queue:function(a){z.debug("Queueing animation of",a),z.queuing=!0,m.one(u+".queue"+y,function(){z.queuing=!1,z.repaint(),z.animate.apply(this,p)})},complete:function(a){z.debug("Animation complete",p.animation),z.remove.completeCallback(),z.remove.failSafe(),z.is.looping()||(z.is.outward()?(z.verbose("Animation is outward, hiding element"),z.restore.conditions(),z.hide()):z.is.inward()?(z.verbose("Animation is outward, showing element"),z.restore.conditions(),z.show()):(z.verbose("Static animation completed"),z.restore.conditions(),p.onComplete.call(o)))},force:{visible:function(){var a=m.attr("style"),b=z.get.userStyle(),c=z.get.displayType(),e=b+"display: "+c+" !important;",f=m.css("display"),g=a===d||""===a;f!==c?(z.verbose("Overriding default display to show element",c),m.attr("style",e)):g&&m.removeAttr("style")},hidden:function(){var a=m.attr("style"),b=m.css("display"),c=a===d||""===a;"none"===b||z.is.hidden()?c&&m.removeAttr("style"):(z.verbose("Overriding default display to hide element"),m.css("display","none"))}},has:{direction:function(b){var c=!1;return b=b||p.animation,"string"==typeof b&&(b=b.split(" "),a.each(b,function(a,b){b!==s.inward&&b!==s.outward||(c=!0)})),c},inlineDisplay:function(){var b=m.attr("style")||"";return a.isArray(b.match(/display.*?;/,""))}},set:{animating:function(a){var b;z.remove.completeCallback(),a=a||p.animation,b=z.get.animationClass(a),z.save.animation(b),z.force.visible(),z.remove.hidden(),z.remove.direction(),z.start.animation(b)},duration:function(a,b){b=b||p.duration,((b="number"==typeof b?b+"ms":b)||0===b)&&(z.verbose("Setting animation duration",b),m.css({"animation-duration":b}))},direction:function(a){a=a||z.get.direction(),a==s.inward?z.set.inward():z.set.outward()},looping:function(){z.debug("Transition set to loop"),m.addClass(s.looping)},hidden:function(){m.addClass(s.transition).addClass(s.hidden)},inward:function(){z.debug("Setting direction to inward"),m.removeClass(s.outward).addClass(s.inward)},outward:function(){z.debug("Setting direction to outward"),m.removeClass(s.inward).addClass(s.outward)},visible:function(){m.addClass(s.transition).addClass(s.visible)}},start:{animation:function(a){a=a||z.get.animationClass(),z.debug("Starting tween",a),m.addClass(a).one(u+".complete"+y,z.complete),p.useFailSafe&&z.add.failSafe(),z.set.duration(p.duration),p.onStart.call(o)}},save:{animation:function(a){z.cache||(z.cache={}),z.cache.animation=a},displayType:function(a){"none"!==a&&m.data(t.displayType,a)},transitionExists:function(b,c){a.fn.transition.exists[b]=c,z.verbose("Saving existence of transition",b,c)}},restore:{conditions:function(){var a=z.get.currentAnimation();a&&(m.removeClass(a),z.verbose("Removing animation class",z.cache)),z.remove.duration()}},add:{failSafe:function(){var a=z.get.duration();z.timer=setTimeout(function(){m.triggerHandler(u)},a+p.failSafeDelay),z.verbose("Adding fail safe timer",z.timer)}},remove:{animating:function(){m.removeClass(s.animating)},animationCallbacks:function(){z.remove.queueCallback(),z.remove.completeCallback()},queueCallback:function(){m.off(".queue"+y)},completeCallback:function(){m.off(".complete"+y)},display:function(){m.css("display","")},direction:function(){m.removeClass(s.inward).removeClass(s.outward)},duration:function(){m.css("animation-duration","")},failSafe:function(){z.verbose("Removing fail safe timer",z.timer),z.timer&&clearTimeout(z.timer)},hidden:function(){m.removeClass(s.hidden)},visible:function(){m.removeClass(s.visible)},looping:function(){z.debug("Transitions are no longer looping"),z.is.looping()&&(z.reset(),m.removeClass(s.looping))},transition:function(){m.removeClass(s.visible).removeClass(s.hidden)}},get:{settings:function(b,c,d){return"object"==typeof b?a.extend(!0,{},a.fn.transition.settings,b):"function"==typeof d?a.extend({},a.fn.transition.settings,{animation:b,onComplete:d,duration:c}):"string"==typeof c||"number"==typeof c?a.extend({},a.fn.transition.settings,{animation:b,duration:c}):"object"==typeof c?a.extend({},a.fn.transition.settings,c,{animation:b}):"function"==typeof c?a.extend({},a.fn.transition.settings,{animation:b,onComplete:c}):a.extend({},a.fn.transition.settings,{animation:b})},animationClass:function(a){var b=a||p.animation,c=z.can.transition()&&!z.has.direction()?z.get.direction()+" ":"";return s.animating+" "+s.transition+" "+c+b},currentAnimation:function(){return!(!z.cache||z.cache.animation===d)&&z.cache.animation},currentDirection:function(){return z.is.inward()?s.inward:s.outward},direction:function(){return z.is.hidden()||!z.is.visible()?s.inward:s.outward},animationDirection:function(b){var c;return b=b||p.animation,"string"==typeof b&&(b=b.split(" "),a.each(b,function(a,b){b===s.inward?c=s.inward:b===s.outward&&(c=s.outward)})),c||!1},duration:function(a){return a=a||p.duration,!1===a&&(a=m.css("animation-duration")||0),"string"==typeof a?a.indexOf("ms")>-1?parseFloat(a):1e3*parseFloat(a):a},displayType:function(){return p.displayType?p.displayType:(m.data(t.displayType)===d&&z.can.transition(!0),m.data(t.displayType))},userStyle:function(a){return a=a||m.attr("style")||"",a.replace(/display.*?;/,"")},transitionExists:function(b){return a.fn.transition.exists[b]},animationStartEvent:function(){var e,a=c.createElement("div"),b={animation:"animationstart",OAnimation:"oAnimationStart",MozAnimation:"mozAnimationStart",WebkitAnimation:"webkitAnimationStart"};for(e in b)if(a.style[e]!==d)return b[e];return!1},animationEndEvent:function(){var e,a=c.createElement("div"),b={animation:"animationend",OAnimation:"oAnimationEnd",MozAnimation:"mozAnimationEnd",WebkitAnimation:"webkitAnimationEnd"};for(e in b)if(a.style[e]!==d)return b[e];return!1}},can:{transition:function(b){var f,g,h,i,j,k,l,c=p.animation,e=z.get.transitionExists(c);if(e===d||b){if(z.verbose("Determining whether animation exists"),f=m.attr("class"),g=m.prop("tagName"),h=a("<"+g+" />").addClass(f).insertAfter(m),i=h.addClass(c).removeClass(s.inward).removeClass(s.outward).addClass(s.animating).addClass(s.transition).css("animationName"),j=h.addClass(s.inward).css("animationName"),l=h.attr("class",f).removeAttr("style").removeClass(s.hidden).removeClass(s.visible).show().css("display"),z.verbose("Determining final display state",l),z.save.displayType(l),h.remove(),i!=j)z.debug("Direction exists for animation",c),k=!0;else{if("none"==i||!i)return void z.debug("No animation defined in css",c);z.debug("Static animation found",c,l),k=!1}z.save.transitionExists(c,k)}return e!==d?e:k},animate:function(){return z.can.transition()!==d}},is:{animating:function(){return m.hasClass(s.animating)},inward:function(){return m.hasClass(s.inward)},outward:function(){return m.hasClass(s.outward)},looping:function(){return m.hasClass(s.looping)},occurring:function(a){return a=a||p.animation,a="."+a.replace(" ","."),m.filter(a).length>0},visible:function(){return m.is(":visible")},hidden:function(){return"hidden"===m.css("visibility")},supported:function(){return!1!==u}},hide:function(){z.verbose("Hiding element"),z.is.animating()&&z.reset(),o.blur(),z.remove.display(),z.remove.visible(),z.set.hidden(),z.force.hidden(),p.onHide.call(o),p.onComplete.call(o)},show:function(a){z.verbose("Showing element",a),z.remove.hidden(),z.set.visible(),z.force.visible(),p.onShow.call(o),p.onComplete.call(o)},toggle:function(){z.is.visible()?z.hide():z.show()},stop:function(){z.debug("Stopping current animation"),m.triggerHandler(u)},stopAll:function(){z.debug("Stopping all animation"),z.remove.queueCallback(),m.triggerHandler(u)},clear:{queue:function(){z.debug("Clearing animation queue"),z.remove.queueCallback()}},enable:function(){z.verbose("Starting animation"),m.removeClass(s.disabled)},disable:function(){z.debug("Stopping animation"),m.addClass(s.disabled)},setting:function(b,c){if(z.debug("Changing setting",b,c),a.isPlainObject(b))a.extend(!0,p,b);else{if(c===d)return p[b];p[b]=c}},internal:function(b,c){if(a.isPlainObject(b))a.extend(!0,z,b);else{if(c===d)return z[b];z[b]=c}},debug:function(){p.debug&&(p.performance?z.performance.log(arguments):(z.debug=Function.prototype.bind.call(console.info,console,p.name+":"),z.debug.apply(console,arguments)))},verbose:function(){p.verbose&&p.debug&&(p.performance?z.performance.log(arguments):(z.verbose=Function.prototype.bind.call(console.info,console,p.name+":"),z.verbose.apply(console,arguments)))},error:function(){z.error=Function.prototype.bind.call(console.error,console,p.name+":"),z.error.apply(console,arguments)},performance:{log:function(a){var b,c,d;p.performance&&(b=(new Date).getTime(),d=g||b,c=b-d,g=b,h.push({Name:a[0],Arguments:[].slice.call(a,1)||"",Element:o,"Execution Time":c})),clearTimeout(z.performance.timer),z.performance.timer=setTimeout(z.performance.display,500)},display:function(){var b=p.name+":",c=0;g=!1,clearTimeout(z.performance.timer),a.each(h,function(a,b){c+=b["Execution Time"]}),b+=" "+c+"ms",f&&(b+=" '"+f+"'"),e.length>1&&(b+=" ("+e.length+")"),(console.group!==d||console.table!==d)&&h.length>0&&(console.groupCollapsed(b),console.table?console.table(h):a.each(h,function(a,b){console.log(b.Name+": "+b["Execution Time"]+"ms")}),console.groupEnd()),h=[]}},invoke:function(b,c,e){var g,h,i,f=q;return c=c||k,e=o||e,"string"==typeof b&&f!==d&&(b=b.split(/[\. ]/),g=b.length-1,a.each(b,function(c,e){var i=c!=g?e+b[c+1].charAt(0).toUpperCase()+b[c+1].slice(1):b;if(a.isPlainObject(f[i])&&c!=g)f=f[i];else{if(f[i]!==d)return h=f[i],!1;if(!a.isPlainObject(f[e])||c==g)return f[e]!==d&&(h=f[e],!1);f=f[e]}})),a.isFunction(h)?i=h.apply(e,c):h!==d&&(i=h),a.isArray(n)?n.push(i):n!==d?n=[n,i]:i!==d&&(n=i),h!==d&&h}},z.initialize()}),n!==d?n:this},a.fn.transition.exists={},a.fn.transition.settings={name:"Transition",debug:!1,verbose:!1,performance:!0,namespace:"transition",interval:0,reverse:"auto",onStart:function(){},onComplete:function(){},onShow:function(){},onHide:function(){},useFailSafe:!0,failSafeDelay:100,allowRepeats:!1,displayType:!1,animation:"fade",duration:!1,queue:!0,metadata:{displayType:"display"},className:{animating:"animating",disabled:"disabled",hidden:"hidden",inward:"in",loading:"loading",looping:"looping",outward:"out",transition:"transition",visible:"visible"},error:{noAnimation:"Element is no longer attached to DOM. Unable to animate.",repeated:"That animation is already occurring, cancelling repeated animation",method:"The method you called is not defined",support:"This browser does not support CSS animations"}}}(jQuery,window,document);
    }

});