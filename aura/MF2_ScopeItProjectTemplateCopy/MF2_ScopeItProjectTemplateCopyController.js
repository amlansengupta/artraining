({
    //Request Id#17460:This method will get invoked once the related component gets loaded and requests server side controller class
    //to make a copy of the scopeIt Project Template record.
    doInit : function(component, event, helper) {
        
        var tempId=component.get("v.recordId");
        
        var action = component.get("c.CopyTemplateTaskEmployee");
        action.setParams({
            'tempId' : tempId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.loaded",true);
            if(state == "SUCCESS")
            {
                var smessage = response.getReturnValue();
                if(smessage.indexOf("Copy")!=-1)
                {
                    component.set("v.pageMessage",smessage);
                    component.set("v.toForward",false);
                    //alert(smessage);
                }
                else
                {
                    component.set("v.pageMessage","Record copied successfully.");
                    component.set("v.copiedId", smessage);
                        component.set("v.toForward",true);
                    //alert("Record copied successfully.");
                }
                //location.replace("/lightning/r/Temp_ScopeIt_Project__c/"+smessage+"/view");
            }
        });
        $A.enqueueAction(action);
        
    },
    forwardToCopiedRecord:function(component,event,helper){
        var scopeItProjectTempId = component.get("v.copiedId");
        var scopeItProjectTempUrl = $A.get("$Label.c.ScopeItProjectTempCopyURL");
        var replacements = [scopeItProjectTempId];
        for (var i = 0; i < replacements.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            scopeItProjectTempUrl = scopeItProjectTempUrl.replace(regexp, replacements[i]);
        }
        
        //location.replace("/lightning/r/Temp_ScopeIt_Project__c/"+component.get("v.copiedId")+"/view");
        location.replace(scopeItProjectTempUrl);
    }
})