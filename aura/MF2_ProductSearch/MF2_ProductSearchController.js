({
    doInit : function(component, event, helper){
        component.set("v.Spinner", true);
        
        //var indexOfEquals = window.location.search.indexOf('=');
        //var oppId = window.location.search.substr(indexOfEquals+1);
        
        //alert(component.get("v.oppId"));
        //alert("Oppid "+oppId);
        //component.set("v.oppId", oppId);
        //alert(component.get("v.oppId"));
     
        var oppId = component.get("v.oppId");
        
        var action = component.get("c.productFieldsInitialize");
        action.setParams({
            'oppId': oppId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert(state);
            if (state === "SUCCESS") {
                var prodFieldsWrap = response.getReturnValue();
                var mapLOBSubBuss = prodFieldsWrap.mapLOBSubBuss;
                component.set("v.mapLOBSubBuss",mapLOBSubBuss);
                
                var mapSubBussSolSeg = prodFieldsWrap.mapSubBussSolSeg;
                component.set("v.mapSubBussSolSeg",mapSubBussSolSeg);
                
                var mapSolSegCluster = prodFieldsWrap.mapSolSegCluster;
                component.set("v.mapSolSegCluster",mapSolSegCluster);
                
                var LOBs = [];
                var listLOB = [];
                //listLOB.push('--Select One--');
                //alert("storeresponse:" + storeResponse);
                //alert("map of LOB : "+JSON.stringify(prodFieldsWrap.mapLOBSubBuss));
                for(var key in mapLOBSubBuss){
                    //console.log('key : '+ key+ 'Map value: ', mapLOBSubBuss[key]);
                    listLOB.push(key);
                };
                //var lstLOB = prodFieldsWrap.mapLOBSubBuss.keys();
                //alert("lobs : "+JSON.stringify(listLOB));
                listLOB.sort();
                for(var i = 0; i<listLOB.length; i++ ){
                    var LOB = {
                        "label": listLOB[i],
                        "value": listLOB[i],
                    };
                    LOBs.push(LOB);
                }
                LOBs.unshift('');
                component.set("v.listLOB",LOBs);
                //component.set("v.listLOB",listLOB);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.Message", true);
                component.set("v.searchError",errors);
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(action);
        
    },
    
    onSelectChange : function(component, event, helper) {
        //var selected = component.find("records").get("v.value");
        helper.onSelectChangehelper(component,event);
        
    },
    
    holdLOB : function(component,event,helper){
        var holdLOBValue = component.get("v.searchLOB");
        if(holdLOBValue){
            component.set("v.checkLOB",false);
            var mapLOBSubBuss = component.get("v.mapLOBSubBuss");
            
            var SubBuss = [];
            var listSubBuss = [];
            var tempMap = mapLOBSubBuss[0];
            
            for(var key in tempMap){
                if(key == holdLOBValue){
                    listSubBuss = tempMap[key];
                    //listSubBuss.unshift('--Select One--');
                }
            }
            //comment out the below part to use lightning:select
            listSubBuss.sort();
            for(var i = 0; i<listSubBuss.length; i++ ){
                var SB = {
                    "label": listSubBuss[i],
                    "value": listSubBuss[i],
                };
                SubBuss.push(SB);
            }
            SubBuss.unshift(''); 
            component.set("v.listSubBuss",SubBuss);
            component.set("v.checkSubBussiness",true);
            component.set("v.checkSolSeg",true);
            component.set("v.searchSubBussiness",'');
            component.set("v.searchSolSeg",'');
            component.set("v.searchCluster",'');
        }
        else{
            component.set("v.checkLOB",true);
            component.set("v.checkSubBussiness",true);
            component.set("v.checkSolSeg",true);
            component.set("v.searchSubBussiness",'');
            component.set("v.searchSolSeg",'');
            component.set("v.searchCluster",'');
            
        }
    },
    holdSubBussiness : function(component,event,helper){
        var holdLOBValue = component.get("v.searchLOB");
        var holdSubBussiness = component.get("v.searchSubBussiness");
        var sbssKey = '';
        sbssKey = sbssKey.concat(holdLOBValue,'_',holdSubBussiness);
        //alert("sub buss : " +holdSubBussiness);
        if(holdSubBussiness){
            component.set("v.checkSubBussiness",false);
            var mapSubBussSolSeg = component.get("v.mapSubBussSolSeg");
            var SolSegs = [];
            var listSolSeg = [];
            var tempMap = mapSubBussSolSeg[0];
            
            for(var key in tempMap){
                if(key == sbssKey){
                    listSolSeg = tempMap[key];
                    //listSolSeg.unshift('--Select One--');
                }
            }
            for(var i = 0; i<listSolSeg.length; i++ ){
                var SS = {
                    "label": listSolSeg[i],
                    "value": listSolSeg[i],
                };
                SolSegs.push(SS);
            }
            SolSegs.sort();
            SolSegs.unshift('');
            component.set("v.listSolSeg",SolSegs);
            component.set("v.checkSolSeg",true);
            component.set("v.searchSolSeg",'');
            component.set("v.searchCluster",'');
        }
        else{
            component.set("v.checkSubBussiness",true);
            component.set("v.checkSolSeg",true);
            component.set("v.searchSolSeg",'');
            component.set("v.searchCluster",'');
        }
    },
    holdSolSeg : function(component,event,helper){
        var holdLOBValue = component.get("v.searchLOB");
        var holdSubBussiness = component.get("v.searchSubBussiness");
        var holdSolSeg = component.get("v.searchSolSeg");
        //alert("sol seg : "+holdSolSeg);
        var sscKey = '';
        sscKey = sscKey.concat(holdLOBValue,'_',holdSubBussiness,'_',holdSolSeg);
        if(holdSolSeg){
            component.set("v.checkSolSeg",false);
            var mapSolSegCluster = component.get("v.mapSolSegCluster");
            var Clusters = [];
            var listCluster = [];
            var tempMap = mapSolSegCluster[0];
            
            for(var key in tempMap){
                if(key == sscKey){
                    listCluster = tempMap[key];
                    //listCluster.unshift('--Select One--');
                }
            }
            listCluster.sort();
            for(var i = 0; i<listCluster.length; i++ ){
                var Cl = {
                    "label": listCluster[i],
                    "value": listCluster[i],
                };
                Clusters.push(Cl);
            }
            Clusters.unshift('');
            component.set("v.listCluster",Clusters);
            component.set("v.searchCluster",'');
        }
        else{
            component.set("v.checkSolSeg",true);
            component.set("v.searchCluster",'');
        }
    },
    holdCluster : function(component,event,helper){
        var holdCluster = component.get("v.searchCluster");
        //alert("Cluster : "+holdCluster);
        if(holdCluster){
            component.set("v.checkCluster",false);
        }
        else{
            component.set("v.checkCluster",true);
            
        }
    },
    
    
    
    Search: function(component, event, helper) {
       	
        var searchKeyFld = component.find("searchId");
        var srcValue = searchKeyFld.get("v.value");
        var pscCode = component.get("v.searchPSC");
        var lob = component.get("v.searchLOB");
        var subBuss = component.get("v.searchSubBussiness");
        var solSeg = component.get("v.searchSolSeg");
        var cluster = component.get("v.searchCluster");
        if((srcValue == '' || srcValue == null) 
            && (pscCode == '' || pscCode == null) 
            && (lob == '' || lob == null || lob == '--Select One--')
            && (subBuss == '' || subBuss == null || subBuss == '--Select One--') 
            && (solSeg == '' || solSeg == null || solSeg == '--Select One--')
            && (cluster == '' || cluster == null || cluster == '--Select One--')
        ){
            component.set("v.Message", true);
            component.set("v.searchError", "Enter either the search name or PSC code or select values for the Product attributes.");
        } else {
            //searchKeyFld.set("v.errors", null);
            // call helper method
            component.set("v.Message", false);
            component.set("v.searchError", "");
            helper.SearchHelper(component, event);
        }
    },
    
    favourites : function(component,event,helper){
        component.set("v.Message", false);
        component.set("v.searchError", "");
        helper.favouritesHelper(component, event);        
    },
    
    formPress: function(component,event,helper){
        if (event.keyCode === 13) {
            //var oppId = window.location.search.substr(7);
            //alert("Oppid "+oppId);
            //component.set("v.oppId", oppId);
            
            var searchKeyFld = component.find("searchId");
            var srcValue = searchKeyFld.get("v.value");
            var pscCode = component.get("v.searchPSC");
            var lob = component.get("v.searchLOB");
            var subBuss = component.get("v.searchSubBussiness");
            var solSeg = component.get("v.searchSolSeg");
            var cluster = component.get("v.searchCluster");
            
            if((srcValue == '' || srcValue == null) 
               && (pscCode == '' || pscCode == null) 
               && (lob == '' || lob == null || lob == '--Select One--' 
                   || subBuss == '' || subBuss == null || subBuss == '--Select One--' 
                   || solSeg == '' || solSeg == null || solSeg == '--Select One--' 
                   || cluster == '' || cluster == null || cluster == '--Select One--')
              ){
                component.set("v.Message", true);
                component.set("v.searchError", "Enter either the search name or PSC code or select values for the Product attributes.");
            } else {
                //searchKeyFld.set("v.errors", null);
                // call helper method
                component.set("v.Message", false);
                component.set("v.searchError", "");
                helper.SearchHelper(component, event);
            }
        }
    },
    
    first : function(component, event, helper){
        
        helper.firsthelper(component,event);
    },
    
    last : function(component, event, helper){
        
        helper.lasthelper(component,event);
    },
    
    next : function(component, event, helper){
        helper.nexthelper(component,event);
        
    },
    
    previous : function(component, event, helper){
        helper.previoushelper(component,event);
        
    },
    
    firesearchAccExpandEvent : function(component, event){
        var pbeID = event.target.id;
        var oppId = component.get("v.oppId");
        //var indexOfEquals = oppId.indexOf('=');
    	//oppId = oppId.substr(indexOfEquals+1); 
        var pbeList = component.get("v.searchResult");
        var start = parseInt(component.get("v.start"));
        var end = parseInt(component.get("v.end"));
        var prevExpandedButton = component.get("v.expandedRecordButton");
        var openedfromVF = component.get("v.openedfromVF");
        
        if(document.getElementById(prevExpandedButton) != null){
            document.getElementById(prevExpandedButton).className = "slds-button slds-button_neutral";
            document.getElementById(prevExpandedButton).innerHTML = "Select";
        }
       	
        var searchProductEvent = $A.get("e.c:MF2_ProductSearchExpandEvent");
        searchProductEvent.setParams({
            "pbeID" : pbeID,
            "oppId" : oppId, 
            "searchResultPBE" : pbeList,
            "start" : start,
            "end" : end,
            "isExpanded" : true,
            "openedfromVF" : openedfromVF,
        })
        searchProductEvent.fire();
        
        document.getElementById(pbeID).className = "slds-button slds-button_success";
        document.getElementById(pbeID).innerHTML = "Selected";
        component.set("v.expandedRecordButton",pbeID);
    },
    
    cancelProductExpand : function (component, event){
      var isExpanded = event.getParam("isExpanded");
      var eventSource = event.getParam("eventSource");
      if(!isExpanded){
            document.getElementById(component.get("v.expandedRecordButton")).className = "slds-button slds-button_neutral";
            document.getElementById(component.get("v.expandedRecordButton")).innerHTML = "Select";
          	component.set("v.expandedRecordButton",'');
          if(eventSource === 'savemore'){
              component.set("v.searchResult", null);
              component.set("v.paginationList", null);
              component.set("v.searchKeyword", null);
              component.set("v.searchPSC", null);
              component.set("v.searchError", null);
              component.set("v.searchLOB", null);
              component.set("v.searchSubBussiness", null);
              component.set("v.searchSolSeg", null);
              component.set("v.searchCluster", null);
              component.set("v.checkLOB", true);
              component.set("v.checkSubBussiness", true);
              component.set("v.checkSolSeg", true);
              component.set("v.checkCluster", true);
              component.set("v.checkSubBussiness", true);
              component.set("v.Message", false);
              component.set("v.numberOfRecord", 0);
              component.set("v.totalSize", 0);
              component.set("v.start", false);
              component.set("v.end", false);
              component.set("v.temp", false);
              component.set("v.expandedRecordButton", '');
        	  component.set("v.isExpanded", false);
              document.body.scrollTop = 0; // For Safari
  			  document.documentElement.scrollTop = 0 // For Chrome, IE, Mozilla
          }
      }
    },
    
    ClearSearch : function (component, event){
        component.set("v.searchResult", null);
        component.set("v.paginationList", null);
        component.set("v.searchKeyword", null);
        component.set("v.searchPSC", null);
        component.set("v.searchError", null);
        component.set("v.searchLOB", null);
        component.set("v.searchSubBussiness", null);
        component.set("v.searchSolSeg", null);
        component.set("v.searchCluster", null);
        component.set("v.checkLOB", true);
        component.set("v.checkSubBussiness", true);
        component.set("v.checkSolSeg", true);
        component.set("v.checkCluster", true);
        component.set("v.checkSubBussiness", true);
        component.set("v.Message", false);
        component.set("v.numberOfRecord", 0);
        component.set("v.totalSize", 0);
        component.set("v.start", false);
        component.set("v.end", false);
        component.set("v.temp", false);
        if(component.get("v.expandedRecordButton") !== null && component.get("v.expandedRecordButton") !== ''){
            //document.getElementById(component.get("v.expandedRecordButton")).className = "slds-button slds-button_neutral";
            //document.getElementById(component.get("v.expandedRecordButton")).innerHTML = "Select";
            component.set("v.expandedRecordButton", '');
            component.set("v.isExpanded", false);
            
            var oppId = component.get("v.oppId");
            var indexOfEquals = oppId.indexOf('=');
            oppId = oppId.substr(indexOfEquals+1);
            var searchProductEvent = $A.get("e.c:MF2_ProductSearchExpandEvent");
            searchProductEvent.setParams({
                "pbeID" : '',
                "oppId" : oppId, 
                "searchResultPBE" : null,
                "start" : 0,
                "end" : 0,
                "isExpanded" : false,
            })
            searchProductEvent.fire();
        }
    },
    
    activeProductsReport : function(component, event){
      window.open('/00OE00000022izz');  
    },
    
    handleSRChange : function(component, event){
        //alert(event.getParam("oldValue"));
        //alert(event.getParam("value"));
        if(event.getParam("value") === null || event.getParam("value").length === 0){
            $A.util.removeClass(component.find("result"), 'slds-show'); 
            $A.util.addClass(component.find("result"), 'slds-hide');
        }
        else if(event.getParam("value") !== null || event.getParam("oldValue").length !== 0){
            $A.util.removeClass(component.find("result"), 'slds-hide'); 
            $A.util.addClass(component.find("result"), 'slds-show');
        }
    },
    
    backToOpp : function(component, event){
        var oppId = component.get("v.oppId");
        var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": oppId,
                "slideDevName": "detail"
            });
            navEvt.fire(); 
    },
    
})