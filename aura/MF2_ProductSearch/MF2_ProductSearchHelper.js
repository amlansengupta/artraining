({
    SearchHelper: function(component, event) {
    var oppId = component.get("v.oppId");
    //var indexOfEquals = oppId.indexOf('=');
    //oppId = oppId.substr(indexOfEquals+1);    
    //alert('2nd oppid : ' + oppId);
    var action = component.get("c.fetchPbEntry");
    action.setParams({
        'searchKeyWord': component.get("v.searchKeyword"),
        'oppID' : oppId,
        'pscCode' : component.get("v.searchPSC"),
        'lob' : component.get("v.searchLOB"),
        'subBuss' : component.get("v.searchSubBussiness"),
        'solSeg' : component.get("v.searchSolSeg"),
        'cluster' : component.get("v.searchCluster")
    });
    action.setCallback(this, function(response) {
        var state = response.getState();
        //alert(state);
        if (state === "SUCCESS") {
            var storeResponse = response.getReturnValue();
            var totalSize = storeResponse.length;
            //alert("storeresponse:" + storeResponse);
            // if storeResponse size is 0 ,display no record found message on screen.
            if (storeResponse.length == 0) {
                component.set("v.Message", true);
                component.set("v.searchError", 'No Records Found.');
                component.set("v.paginationList", null);
            } else {
                component.set("v.Message", false);
                //alert('Accordion '+ component.get("v.activeSections"));
                //var activesecions = ['A','B'];
                //component.set("v.activeSections",activesecions); 
                /*if(! component.get("v.activeSections").includes('B')){
                	component.get("v.activeSections").push('B');
                }
                alert('Accordion next '+ component.get("v.activeSections"));*/
                // set numberOfRecord attribute value with length of return value from server
                var pageSize = component.get("v.pageSize");
                component.set("v.numberOfRecord", storeResponse.length);
                // set searchResult list with return value from server.
                component.set("v.searchResult", storeResponse);
                component.set("v.totalSize", totalSize);
                
                //alert(pageSize);
                //alert(searchResult[0]);
                component.set("v.temp", pageSize);
                
                var paginationList = [];
                var searchresult = component.get("v.searchResult");
                if(totalSize > parseInt(pageSize)){
                    for(var i=0; i< parseInt(pageSize); i++){
                        paginationList.push(searchresult[i]);
                    }
                }    
                else{
                    paginationList = searchresult.slice();
                }
                component.set("v.start",0);
                component.set("v.end",paginationList.length-1);
                
                component.set("v.paginationList", paginationList);
                //document.getElementById("result").style.display = "block" ;
                //document.getElementById("numberOfRecords").style.display = "block" ;
                //document.getElementById("paginationButtons").style.display = "block" ;
                
            }
        }
        else if (state === "ERROR") {
                var errors = response.getError();
                component.set("v.Message", true);
                component.set("v.searchError",errors);
            }
        
    });
    $A.enqueueAction(action);
    },
    
    favouritesHelper : function(component, event){
        
        var oppId = component.get("v.oppId");
        //alert(oppId);
        //var indexOfEquals = oppId.indexOf('=');
    	//oppId = oppId.substr(indexOfEquals+1); 
        var action = component.get("c.fetchFavourites");
        //alert(component.get("v.activeSessionId"));
        action.setParams({
            'oppID' : oppId,
            'activeSessionId' : component.get("v.activeSessionId")
        });
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var storeResponse = response.getReturnValue();
            var totalSize = storeResponse.length;
            //alert("storeresponse:" + storeResponse);
            // if storeResponse size is 0 ,display no record found message on screen.
            if (storeResponse.length == 0) {
                component.set("v.Message", true);
                component.set("v.searchError",'No favourites found!');
            } else {
                component.set("v.Message", false);
                
                // set numberOfRecord attribute value with length of return value from server
                //var searchresult = [];
                //searchresult.push(storeResponse.get(index),storeResponse);
                var pageSize = component.get("v.pageSize");
                component.set("v.numberOfRecord", storeResponse.length);
                // set searchResult list with return value from server.
                component.set("v.searchResult", storeResponse);
                component.set("v.totalSize", totalSize);
                
                //alert(pageSize);
                //alert(searchResult[0]);
                component.set("v.temp", pageSize);
                
                var paginationList = [];
                var searchresult = component.get("v.searchResult");
                if(totalSize > parseInt(pageSize)){
                    for(var i=0; i< parseInt(pageSize); i++){
                        paginationList.push(searchresult[i]);
                    }
                }    
                else{
                    paginationList = searchresult.slice();
                }
                component.set("v.start",0);
                component.set("v.end",paginationList.length-1);
                
                component.set("v.paginationList", paginationList);
                //document.getElementById("result").style.display = "block" ;
                //document.getElementById("numberOfRecords").style.display = "block" ;
                //document.getElementById("paginationButtons").style.display = "block" ;
                
            }
        }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(JSON.stringify(errors));
                component.set("v.Message", true);
                component.set("v.searchError",errors);
            }
        
    });
    $A.enqueueAction(action);
    },
  
  onSelectChangehelper : function(component, event) {
      var selected = component.find("records").get("v.value");
      var pbeList = component.get("v.searchResult");
      var totalSize = component.get("v.totalSize");
      var start = component.get("v.start");
      var end = component.get("v.end");
      var prevPagesize = component.get("v.pageSize");
      component.set("v.pageSize",selected);
      
      if(pbeList.length >0){ 
          var paginationList = [];
          
          if(totalSize > parseInt(selected)){
              for(var i=0; i< selected; i++)
              {
                  paginationList.push(pbeList[i]);
              }
          }
          else{
              paginationList = pbeList.slice();
          }
          component.set("v.paginationList", paginationList);
          //searchAccExpandController.searchAccExpand(component,event,helper);
          start = paginationList[0].indexPB;
          end = paginationList[paginationList.length-1].indexPB;
          component.set("v.start",start);
          component.set("v.end",end);
      }
  },
  
  firsthelper : function(component, event){
      var pbeList = component.get("v.searchResult");
      var pageSize = component.get("v.pageSize");
      //var totalSize = component.get("v.totalSize");
      var temp = component.get("v.temp");
      var tempupdated = parseInt(pageSize);
      component.set("v.temp",tempupdated);
      
      var paginationList = [];
      for(var i=0; i< parseInt(pageSize); i++)
      {
          paginationList.push(pbeList[i]);
      }
      component.set("v.paginationList", paginationList);
      component.set("v.start",0);
      component.set("v.end",parseInt(pageSize)-1);
  },
  
  lasthelper : function(component, event){
      var pbeList = component.get("v.searchResult");
      var pageSize = component.get("v.pageSize");
      var totalSize = component.get("v.totalSize");
      var paginationList = [];
      var temp = component.get("v.temp");
      var tempupdated = parseInt(temp) + parseInt(totalSize);
      component.set("v.temp",tempupdated);
      
      for(var i=parseInt(totalSize)-parseInt(pageSize); i< parseInt(totalSize); i++){
          paginationList.push(pbeList[i]);
      }
      component.set("v.paginationList", paginationList);
      component.set("v.start",parseInt(totalSize)-paginationList.length);
      component.set("v.end",parseInt(totalSize)-1);
      
  },
  
  nexthelper : function(component, event){
      
      var pbeList = component.get("v.searchResult");
      var end = component.get("v.end");
      var start = component.get("v.start");
      var pageSize = component.get("v.pageSize");
      var totalSize = component.get("v.totalSize");
      var paginationList = [];
      var temp = component.get("v.temp");
      var tempupdated = parseInt(temp) + parseInt(pageSize);
      component.set("v.temp",tempupdated);
      
      var counter = 0;
      //alert("End " + end + " " + "PageSize "+ pageSize);
      for(var i=parseInt(end)+1; i < parseInt(end)+ parseInt(pageSize)+1; i++){
          if(pbeList.length > end){
              if(pbeList[i] != null){
                  paginationList.push(pbeList[i]);
                  counter ++ ;
              }
          }
      }
      //alert("paginationList" + paginationList.length);
      
      start = paginationList[0].indexPB;
      end = paginationList[paginationList.length-1].indexPB;
      /*var tempend = parseInt(end) + counter;
      if(tempend > parseInt(totalSize-1)){
          end = parseInt(totalSize - 1);
      }
      else{
          end = tempend;
      }*/
      
      component.set("v.start",start);
      component.set("v.end",end);
      component.set("v.paginationList", paginationList);
      //alert(tempupdated+ ' ' + pageSize+ ' ' + totalSize);
  },
  
  previoushelper : function(component, event){
      var pbeList = component.get("v.searchResult");
      var end = component.get("v.end");
      var start = component.get("v.start");
      var pageSize = component.get("v.pageSize");
      var temp = component.get("v.temp");
      var tempupdated = parseInt(temp) - parseInt(pageSize);
      component.set("v.temp",tempupdated);
      
      var paginationList = [];
      //var counter = 0;
      
      for(var i = parseInt(start)-parseInt(pageSize); i < parseInt(start) ; i++){
          if(i > -1)  {
              if(pbeList[i] != null){
                  paginationList.push(pbeList[i]);
                  //counter ++;
              }
          }
          
      } 
      
      start = paginationList[0].indexPB;
      end = paginationList[paginationList.length-1].indexPB;
      component.set("v.start",start);
      component.set("v.end",end);
      component.set("v.paginationList", paginationList);
  },
  
})