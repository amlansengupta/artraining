// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
		doInit : function(component, event, helper) {
				helper.setIsFollowing(component);
		},

		toggleFollow: function(component, event, helper) {
				if (component.get("v.isFollowing")) {
						helper.unfollow(component);
				} else {
						helper.follow(component);
				}
		}
})