// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    // Method called on scripts load
	doInit : function(component, event, helper) {
	    console.log("doing init for groups_list");
        helper.getSitePrefix(component);
		helper.getGroups(component, event);
        helper.isNicknameDisplayEnabled(component);
	},
    // Method called for pagination and next page
    getNextPage : function(component, event, helper) {
        component.set("v.groupsListWrapper.groupsList",null);
        helper.getNextPage(component);
    },
	// Method called for pagination and previous page
    getPreviousPage : function(component, event, helper) {
        component.set("v.groupsListWrapper.groupsList",null);
        helper.getPreviousPage(component);
    },
    // Method to fetch and sort groups
    setSortBy :  function(component, event, helper) {
        var sortBy = event.getParam("sortBy");
		component.set("v.sortBy", sortBy);
        helper.getGroups(component, event);
    },
    // Method to set group filters
    setGroupsFilters :  function(component, event, helper) {
        var searchString = event.getParam("searchString");
        var searchMyGroups = event.getParam("searchMyGroups");
        component.set("v.searchString", searchString);
        component.set("v.searchMyGroups", searchMyGroups);
        helper.getGroups(component, event);
        
    }
})