// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    	// method to fetch groups
 		getGroups : function(component, event)   {
        
        this.debug(component,"======sort by",component.get("v.sortBy"));
        this.debug(component,"======my groupppp",component.get("v.searchMyGroups"));
        this.debug(component,"======search",component.get("v.searchString"));
        
    	var action = component.get("c.getGroups");
        // set parameters to method called from apex class
        action.setParams({
            numberOfGroups : component.get("v.numberOfGroups"),
            sortBy  : component.get("v.sortBy"),
            searchMyGroups : component.get("v.searchMyGroups"),
            searchString : component.get("v.searchString"),
        });
		
        action.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
            
                var groupsListWrapper = response.getReturnValue();
                console.dir(groupsListWrapper);
                if(groupsListWrapper.errorMsg !== '' && groupsListWrapper.errorMsg !== null){
                    component.set("v.strError",'Unexpected Error Occured, Please contact your Administrator.');
                }
                
                var appEvent = $A.get("e.c:SVNSUMMITS_Groups_Header_Event");
                appEvent.setParams({
                    "totalResults" : groupsListWrapper.totalResults,
                });
                appEvent.fire();
                
                this.debug(component,'====List Success',groupsListWrapper.totalResults);
                // check for description field having limit of 100 characters with limit  
                // of 97 characters followed by (...)
                for(var i=0;i< groupsListWrapper.groupsList.length;i++){
                    groupsListWrapper.groupsList[i].strTime = moment.utc(groupsListWrapper.groupsList[i].LastFeedModifiedDate).fromNow();
                	/*if(groupsListWrapper.groupsList[i].Description != null && groupsListWrapper.groupsList[i].Description.length > 97){
                        groupsListWrapper.groupsList[i].Description = groupsListWrapper.groupsList[i].Description.substring(0,97)+'...';
                    }*/
                }
                component.set("v.groupsListWrapper", groupsListWrapper);
            }
            
        });
        $A.enqueueAction(action);
                    
    },
    
    // Pagination - fetching records for next page 
    getNextPage: function(component) {
        
        this.debug(component,"Next Clicked...",null);
        var action = component.get("c.nextPage");
        //set parameters to method called from apex class
        action.setParams({
            numberOfGroups : component.get("v.numberOfGroups"),
            pageNumber : component.get("v.groupsListWrapper").pageNumber,
            sortBy  : component.get("v.sortBy"),
            searchMyGroups : component.get("v.searchMyGroups"),
            searchString : component.get("v.searchString"),
        });

        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var groupsListWrapper = actionResult.getReturnValue();
                
                // check for description field having limit of 100 characters with limit  
                // of 97 characters followed by (...)
                for(var i=0;i< groupsListWrapper.groupsList.length;i++){
                    groupsListWrapper.groupsList[i].strTime = moment.utc(groupsListWrapper.groupsList[i].LastFeedModifiedDate).fromNow();
                    if(groupsListWrapper.groupsList[i].Description != null && groupsListWrapper.groupsList[i].Description.length > 97){
                        groupsListWrapper.groupsList[i].Description = groupsListWrapper.groupsList[i].Description.substring(0,97)+'...';
                    }
                }
                component.set("v.groupsListWrapper", groupsListWrapper);
                var pageNumberComp = this.component.find("pageNumber");
                //pageNumberComp.set("v.value",groupsListWrapper.pageNumber);
            }
        });

        $A.enqueueAction(action);
    },
	// Pagination - fetching records for previous page 
    getPreviousPage: function(component) {
        
        this.debug(component,"Previous Clicked...",null);
        var action = component.get("c.previousPage");
        
        //set parameters to method called from apex class
        action.setParams({
            numberOfGroups : component.get("v.numberOfGroups"),
            pageNumber : component.get("v.groupsListWrapper").pageNumber,
            sortBy  : component.get("v.sortBy"),
            searchMyGroups : component.get("v.searchMyGroups"),
            searchString : component.get("v.searchString"),
        });

        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === "SUCCESS") {
                var groupsListWrapper = actionResult.getReturnValue();
                // check for description field having limit of 100 characters with limit  
                // of 97 characters followed by (...)
                for(var i=0;i< groupsListWrapper.groupsList.length;i++){
                    groupsListWrapper.groupsList[i].strTime = moment.utc(groupsListWrapper.groupsList[i].LastFeedModifiedDate).fromNow();
                    if(groupsListWrapper.groupsList[i].Description != null && groupsListWrapper.groupsList[i].Description.length > 97){
                        groupsListWrapper.groupsList[i].Description = groupsListWrapper.groupsList[i].Description.substring(0,97)+'...';
                    }
                }
                
                component.set("v.groupsListWrapper", groupsListWrapper);
                var pageNumberComp = this.component.find("pageNumber");
                pageNumberComp.set("v.value",groupsListWrapper.pageNumber);
            }

        });

        $A.enqueueAction(action);
    },
    // method to fetch site prefix
    getSitePrefix : function(component) {
    	var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s",""));
		});
        $A.enqueueAction(action);
    },
    //check with isNicknameDisplayEnabled
    isNicknameDisplayEnabled : function(component) {
        var action = component.get("c.isNicknameDisplayEnabled");
        action.setCallback(this, function(actionResult) {
            component.set("v.isNicknameDisplayEnabled", actionResult.getReturnValue());  
            this.debug(component,"Nick Name for Community Boolean : ",component.get("v.isNicknameDisplayEnabled"));
        });
        $A.enqueueAction(action);
    },
    debug: function(component, msg, variable) {

        var debugMode = component.get("v.debugMode");

        if(debugMode)
        {
            if(msg)
            {
            	console.log(msg);
            }

            if(variable)
            {
            	console.log(variable);
            }
        }

    },
    
})