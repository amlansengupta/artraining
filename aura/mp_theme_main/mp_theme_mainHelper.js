/**
 * Created by emaleesoddy on 3/17/17.
 */
({
    getUserInfo: function(component) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(a) {
            component.set("v.currentUser", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    getAccountInfo: function(component) {
        var action = component.get("c.getAccountInfo");
        action.setCallback(this, function(a) {
            component.set("v.accountInfo", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    toggleSearch: function(component) {
        var search = component.find("mpSearch");
        $A.util.toggleClass(search, "open");
    },
    hideSearch: function(component) {
        var search = component.find("mpSearch");
        $A.util.removeClass(search, "open");
    },
    handleShowModal: function (cmp) {

    }
})