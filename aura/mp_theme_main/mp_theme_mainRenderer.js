({
    render : function(cmp, helper){
        var ret = this.superRender();
        console.log('render');
        return ret;
    },
    rerender : function(cmp, helper){
        this.superRerender();
        console.log('rerender');
    },
    afterRender: function (component, helper) {
        this.superAfterRender();
        var searchContainer = document.getElementById("mpSearch");
        var searchButton = searchContainer.getElementsByClassName("search-button")[0];
        var searchField = searchContainer.getElementsByClassName("search-field")[0];
        searchButton.onclick = function(){
            helper.hideSearch(component);
        }
        searchField.onkeypress = function(){
            if (event.which == 13 || event.keyCode == 13) {
                helper.hideSearch(component);
            }
        }
    },

})