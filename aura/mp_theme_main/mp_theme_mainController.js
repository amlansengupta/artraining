/**
 * Created by emaleesoddy on 3/17/17.
 */
({
    doInit: function(component, event, helper) {
        helper.getUserInfo(component);
        helper.getAccountInfo(component);
    },
    toggleSearch: function(component, event, helper) {
        helper.toggleSearch(component);
    },
    hideSearch: function(component, event, helper) {
        helper.hideSearch(component);
    },
    openModalWindow: function (component) {
       component.set('v.openModal', true);
        //cmp.get("c:Peak_Onboarding");
    }
})