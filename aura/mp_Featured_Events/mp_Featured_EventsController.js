/**
 * Created by abdoundure on 8/20/18.
 */
({
    doInit: function (component, event, helper) {
        helper.fetchFeaturedFourEvents(component, event);
        helper.get_SitePrefix(component);
    }
})