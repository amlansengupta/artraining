({
    
    //Request Id#17446:This method will get invoked once the related component gets loaded and requests server side controller class
    //to make a copy of the scope modelling record.
    doInit : function(component, event, helper) {
        
        var ModId = component.get("v.recordId");
        var action = component.get("c.CopyModellingTaskEmployee");
        action.setParams({
            'ModId' : ModId
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            component.set("v.loaded",true);
            if(state == "SUCCESS")
            {
                var smessage = response.getReturnValue();
                
                if(smessage!=null){
                    if(smessage.indexOf("Copy")!=-1)
                    {
                        component.set("v.pageMessage",smessage);
                        component.set("v.toForward",false);
                        //alert(smessage);
                    }
                    else
                    {
                        component.set("v.pageMessage","Record copied successfully.");
                        component.set("v.copiedId", smessage);
                        component.set("v.toForward",true);
                        //alert('Record copied successfully.');
                    }
                }
                //location.replace("/lightning/r/Scope_Modeling__c/"+smessage+"/view");
            }
        });
        $A.enqueueAction(action);
        
    },
    forwardToCopiedRecord:function(component,event,helper){
        var scopeModelingId = component.get("v.copiedId");
        var scopeModelingUrl = $A.get("$Label.c.ScopeModelingCopyURL");
        var replacements = [scopeModelingId];
        for (var i = 0; i < replacements.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            scopeModelingUrl = scopeModelingUrl.replace(regexp, replacements[i]);
        }
        
        //location.replace("/lightning/r/Scope_Modeling__c/"+component.get("v.copiedId")+"/view");
        location.replace(scopeModelingUrl);
    }
})