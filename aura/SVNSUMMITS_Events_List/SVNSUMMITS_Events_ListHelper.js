// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getEventsHelper: function(cmp, event) {
        var self = this;
        var action;
        var filterByFollowingTopic = cmp.get("v.filterByFollowingTopic");

        // check if we are filtering by topics I'm following. This comes
        // from the news filter component when the user checks the select
        // box in the UI.
        if(filterByFollowingTopic === true) {
            action = cmp.get("c.getEventsFilteredByFollowing");
        } else {
            action = cmp.get("c.getEvents");
        }

        // if we have there is a search string set the filterOn  value
        var searchTerm = cmp.get("v.searchstr");
        if(searchTerm !== undefined && searchTerm !== ''){
            cmp.set("v.filterOn", "Search Term");
        } else {
            cmp.set("v.filterOn", "");
        }

        action.setParams({
            eventListFlag: cmp.get("v.displayMode") === 'Compact' ? false : true,
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            strfilterType: cmp.get("v.filterType"),
            strRecordId: cmp.get("v.topicValue"),
            networkId: '',
            sortBy: cmp.get("v.sortBy"),
            filterByTopic: cmp.get("v.filterByTopic"),
            topicName: cmp.get("v.filterByTopic"),
            filterBySearchTerm: '',
            searchTerm: searchTerm,
            filterOn: cmp.get("v.filterOn"),
            fromDate: cmp.get("v.fromDate"),
            toDate: cmp.get("v.toDate"),
            listViewMode: cmp.get("v.listViewMode"),
            sortByLabel: cmp.get("v.sortByLabel")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            self.debug(cmp, "Event_ListHelper.js - getEventsHelper() callback response state: ", state);
            var sortLabel = cmp.get('v.sortByLabel');
            if (cmp.isValid() && state === "SUCCESS") {
                self.debug(cmp, "Return value:", response.getReturnValue());

                var eventsListWrapper = response.getReturnValue();

                //updated code to hide recommended component if no records found
                if (cmp.get("v.displayMode") === 'Compact') {
                    if (eventsListWrapper.objEventList.length === 0) {
                        $('.CCEVENTSLCSVNSUMMITS_Events_List').hide();
                    }
                }
                cmp.set("v.totalEvents", eventsListWrapper.totalResults);

                var appEvent = $A.get("e.c:SVNSUMMITS_Events_Header_Event");
                appEvent.setParams({
                    "sortByLabel": sortLabel,
                    "totalResults": eventsListWrapper.totalResults

                });
                appEvent.fire();

				cmp.set("v.wrappedEvents", self.updateEventsWrapper(cmp, eventsListWrapper));
            }
            else if (state === "ERROR") {
                console.log("problem in getEventsHelper while calling getEvents");
                console.log(response.getError());
            }
        });

        $A.enqueueAction(action);
    },

	updateEventsWrapper : function (cmp, eventsListWrapper) {
        console.log('updateEventsWrapper:::', eventsListWrapper);
        
		eventsListWrapper.strTimeZone = this.getTimeZone(eventsListWrapper);

	    for (var i = 0; i < eventsListWrapper.objEventList.length; i++) {
	        eventsListWrapper.objEventList[i].showTo = false;
	        eventsListWrapper.objEventList[i].showEndDate = false;

	        var startDate, startTime;
	        var endDate, endTime;
			var localeStartDate, localeEndDate;
			var startDay, startMonth, startYear;
			var endDay, endMonth, endYear;

	        if (eventsListWrapper.objEventList[i].Start_DateTime__c !== null) {
	            localeStartDate = moment.tz(eventsListWrapper.objEventList[i].Start_DateTime__c,
	                                        eventsListWrapper.strTimeZone)
	                                    .format('YYYY-MM-DD HH:mm:ss');
	            eventsListWrapper.objEventList[i].localeStartDate = localeStartDate;

	            startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
	            startTime = moment(startDate).toDate();
	            eventsListWrapper.objEventList[i].strMinute = moment(startTime);
	            var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	            eventsListWrapper.objEventList[i].strDay = moment(startTime).format('ddd');
	        }

	        if (eventsListWrapper.objEventList[i].End_DateTime__c !== null) {
	            localeEndDate = moment.tz(eventsListWrapper.objEventList[i].End_DateTime__c,
	                                      eventsListWrapper.strTimeZone)
	                                  .format('YYYY-MM-DD HH:mm:ss');
				eventsListWrapper.objEventList[i].localeEndDate = localeEndDate;

	            endDate = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
	            endTime = moment(endDate).toDate();
	            eventsListWrapper.objEventList[i].endMinute = moment(endTime);
	        }

	        if (startTime !== null && endTime !== null) {
	            var diffDays = Math.round(Math.abs((endTime.getTime() - startTime.getTime()) / (24 * 60 * 60 * 1000)));
	            eventsListWrapper.objEventList[i].daysOfMultiDaysEvent = diffDays;
	            eventsListWrapper.objEventList[i].showTo = true;
	        }

	        startDay   = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("DD");
	        startMonth = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("MMM");
	        startYear  = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("YYYY");

	        endDay     = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("DD");
	        endMonth   = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("MMM");
	        endYear    = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format("YYYY");

	        eventsListWrapper.objEventList[i].strMonth = startMonth;
	        eventsListWrapper.objEventList[i].intDate  = startDay;
            eventsListWrapper.objEventList[i].strYear  = startYear;

			if (startDay !== endDay || startMonth !== endMonth || startYear !== endYear) {
				eventsListWrapper.objEventList[i].showEndDate = true;
			    eventsListWrapper.objEventList[i].endDay = endDay;
			    eventsListWrapper.objEventList[i].endMonth = endMonth;
			    eventsListWrapper.objEventList[i].endYear = endYear;
            }

	        if (cmp.get("v.titletext") === 'Recommended For You' ? true : false) {
	            if (eventsListWrapper.objEventList[i].Name.length > 80) {
	                eventsListWrapper.objEventList[i].Name = eventsListWrapper.objEventList[i].Name.substring(0, 77);
	                eventsListWrapper.objEventList[i].Name = eventsListWrapper.objEventList[i].Name + '...';
	            }
	        } else {
	            if (eventsListWrapper.objEventList[i].Name.length > 80) {
	                eventsListWrapper.objEventList[i].Name = eventsListWrapper.objEventList[i].Name.substring(0, 77);
	                eventsListWrapper.objEventList[i].Name = eventsListWrapper.objEventList[i].Name + '...';
	            }
	        }

	        eventsListWrapper.objEventList[i].topics1 = [];
	        eventsListWrapper.objEventList[i].topics1.push(eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);
	        eventsListWrapper.objEventList[i].topics = [];

	        /* Logic for topics will be displayed till 27 characters only */
	        if (eventsListWrapper.objEventList[i].topics1 !== undefined) {
	            for (var j = 0; j < eventsListWrapper.objEventList[i].topics1.length; j++) {
	                var eventsname = '';
	                if (eventsListWrapper.objEventList[i].topics1[j] !== undefined) {
	                    for (var jj = 0; jj < eventsListWrapper.objEventList[i].topics1[j].length; jj++) {
	                        eventsname += eventsListWrapper.objEventList[i].topics1[j][jj].Topic.Name;
	                        if (eventsname.length <= 27 && eventsListWrapper.objEventList[i].topics !== undefined) {
	                            eventsListWrapper.objEventList[i].topics.push(eventsListWrapper.objEventList[i].topics1[j][jj]);
	                        }
	                    }
	                }
	            }
	        }
	    }

	    return eventsListWrapper;
    },

    getNextPage: function(cmp, event) {
        var action = cmp.get("c.nextPage");
        var self   = this;

        action.setParams({
            eventListFlag: cmp.get("v.displayMode") == 'Compact' ? false : true,
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            pageNumber: cmp.get("v.wrappedEvents").pageNumber,
            strfilterType: cmp.get("v.filterType"),
            strRecordId: cmp.get("v.topicValue"),
            networkId: '',
            sortBy: cmp.get("v.sortBy"),
            filterByTopic: cmp.get("v.filterByTopic"),
            topicName: cmp.get("v.filterByTopic"),
            filterBySearchTerm: cmp.get("v.filterOn"),
            searchTerm: cmp.get("v.searchstr"),
            filterOn: cmp.get("v.filterOn"),
            fromDate: cmp.get("v.fromDate"),
            toDate: cmp.get("v.toDate"),
            listViewMode: cmp.get("v.listViewMode")
        });

        action.setCallback(this, function(actionResult) {
            var eventsListWrapper = actionResult.getReturnValue();
			cmp.set("v.wrappedEvents", self.updateEventsWrapper(cmp, eventsListWrapper));

            var pageNumberComp = self.cmp.find("pageNumber");
            pageNumberComp.set("v.value", eventsListWrapper.pageNumber);
        });

        $A.enqueueAction(action);
    },

    getPreviousPage: function(cmp, event) {
        var action = cmp.get("c.previousPage");
        var self   = this;

        action.setParams({
            eventListFlag: cmp.get("v.displayMode") == 'Compact' ? false : true,
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            pageNumber: cmp.get("v.wrappedEvents").pageNumber,
            strfilterType: cmp.get("v.filterType"),
            strRecordId: cmp.get("v.topicValue"),
            networkId: '',
            sortBy: cmp.get("v.sortBy"),
            filterByTopic: cmp.get("v.filterByTopic"),
            topicName: cmp.get("v.filterByTopic"),
            filterBySearchTerm: '',
            searchTerm: cmp.get("v.searchstr"),
            filterOn: cmp.get("v.filterOn"),
            fromDate: cmp.get("v.fromDate"),
            toDate: cmp.get("v.toDate"),
            listViewMode: cmp.get("v.listViewMode")
        });

        action.setCallback(this, function(actionResult) {
            var eventsListWrapper = actionResult.getReturnValue();
			cmp.set("v.wrappedEvents", self.updateEventsWrapper(cmp, eventsListWrapper));

            var pageNumberComp = self.cmp.find("pageNumber");
            pageNumberComp.set("v.value", eventsListWrapper.pageNumber);
        });

        $A.enqueueAction(action);
    },

    get_SitePrefix: function(cmp) {
        var action = cmp.get("c.getSitePrefix");

        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            cmp.set("v.sitePath", sitePath);
            cmp.set("v.sitePrefix", sitePath.replace("/s", ""));

        });
        $A.enqueueAction(action);
    },

    sortByShowHide: function(component) {
        $(window).click(function() {
            $('#dropDwnBtn_menu').hide();
        });

        $('#dropDwnBtn').click(function(even) {
            even.stopPropagation();
        });

        $('#dropDwnBtn').click(function() {
            $('#dropDwnBtn_menu').toggle();
        });

        $('#dropDwnBtn_menu').click(function() {
            if ($('#dropDwnBtn_menu').show()) {
                $('#dropDwnBtn_menu').hide();
            } else {
                $('#dropDwnBtn_menu').show();
            }
        });
    },

	// adjust for guest user
    getTimeZone : function(eventsListWrapper) {
        return !eventsListWrapper.strTimeZone || eventsListWrapper.strTimeZone === 'GMT'
            ? moment.tz.guess()
			: eventsListWrapper.strTimeZone;
    },
    
    debug: function(cmp, msg, variable) {
        var debugMode = cmp.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    }
})