// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    //Initialize method for list view component
    doInit : function(component, event, helper) {
        helper.debug(component,'List Components called..',null);

        var appEvent = $A.get("e.c:SVNSUMMITS_Events_DisplayMode_Event");
        appEvent.setParams({
            "listViewMode" : component.get("v.listViewMode")
        });
        appEvent.fire();

        var url = window.location.href;
        component.set("v.currentURL",encodeURIComponent(url));

        var topicValue = url.split("/").pop();

        topicValue = decodeURIComponent(topicValue);
        topicValue = topicValue.replace(/#/g , "");

        var filterOn = component.get("v.filterOn");
        if (filterOn === "Search Term") {
            component.set("v.searchstr",topicValue);
        } else if (filterOn === "Topic Value") {
            component.set("v.filterByTopic",topicValue);
        } else {
            component.set("v.topicValue",component.get("v.recordId"));
        }

        helper.getEventsHelper(component);
        helper.get_SitePrefix(component);
        helper.sortByShowHide(component);
    },

    //Get Next method for next page and pagination
    getNextPage : function(component, event, helper) {
        component.set("v.wrappedEvents.objEventList",null);
        helper.getNextPage(component, event);
    },

    //Get Previous method for Previous page and pagination
    getPreviousPage : function(component, event, helper) {
        component.set("v.wrappedEvents.objEventList",null);
        helper.getPreviousPage(component, event);
    },

    //Get Previous method for Previous page and pagination
    setDates :  function(component, event, helper) {
        var fromDate = event.getParam("fromDate");
        var toDate = event.getParam("toDate");
        component.set("v.fromDate", fromDate);
        component.set("v.toDate", toDate);
        helper.getEventsHelper(component);
    },

    // Set topics method
    setTopic :  function(component, event, helper) {
        var filterByTopic = event.getParam("filterByTopic");
        component.set("v.filterByTopic", filterByTopic);
        helper.getEventsHelper(component);
    },

    setDisplayMode: function(component, event, helper){
        var listViewMode = event.getParam("listViewMode");
        component.set("v.listViewMode", listViewMode);
        helper.getEventsHelper(component);
    },

    setFilterByFollowingTopic: function(component, event, helper){
        var filterByFollowingTopic = event.getParam("filterByFollowingTopic");
        component.set("v.filterByFollowingTopic", filterByFollowingTopic);
        helper.getEventsHelper(component, event);
    },

    setSearchFilter: function(component, event, helper){
        var searchText = event.getParam("searchText");
        component.set("v.searchstr", searchText);
        helper.getEventsHelper(component, event);
    },

    // Sort By lists method
    sortBy : function(component, event, helper){
        var sortBy = event.getParam("sortBy");
        var sortByLabel = event.getParam('sortByLabel');
        var listViewMode = event.getParam("listViewMode");

        component.set("v.listViewMode", listViewMode);
        //console.log('listViewMode before event = ' + component.get('v.listViewMode'));
        //if(sortBy != component.get("v.sortBy")){
        component.set("v.sortBy", sortBy);
        component.set("v.sortByLabel", sortByLabel);
        var appEvent = $A.get("e.c:SVNSUMMITS_Events_DisplayMode_Event");
        appEvent.setParams({
            "listViewMode" : listViewMode
        });
        appEvent.fire();
        //console.log('listViewMode = ' + component.get('v.listViewMode'));
        helper.getEventsHelper(component);
        //}
    },

    // Sort By Upcoming values
    sortByUpcoming : function(component, event, helper) {
        if(component.get("v.sortBy") !== 'Upcoming'){
            component.set("v.sortBy",component.find("Upcoming").get("v.value"));
            helper.getEventsHelper(component, event);

            var cmpTarget = component.find('upcommingImg');
            $A.util.removeClass(cmpTarget, 'hideImg');
            $A.util.addClass(cmpTarget, 'showImg');

            var cmpTarget1 = component.find('topAttendanceImg');
            $A.util.removeClass(cmpTarget1, 'showImg');
            $A.util.addClass(cmpTarget1, 'hideImg');
        }

    },
    
    //Sort by top attendees
    sortByTopAttendees : function(component, event, helper) {
        if(component.get("v.sortBy") !== 'TopAttendees'){
            component.set("v.sortBy",component.find("TopAttendees").get("v.value"));
            helper.getEventsHelper(component, event);

            var cmpTarget = component.find('topAttendanceImg');
            $A.util.removeClass(cmpTarget, 'hideImg');
            $A.util.addClass(cmpTarget, 'showImg');

            var cmpTarget1 = component.find('upcommingImg');
            $A.util.removeClass(cmpTarget1, 'showImg');
            $A.util.addClass(cmpTarget1, 'hideImg');
        }
    },
})