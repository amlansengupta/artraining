({
	getSitePrefix: function(component) {
    	var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            console.log("featured user sitePath",sitePath);
            component.set("v.sitePath", sitePath);
            // component.set("v.sitePrefix", sitePath.replace("/s",""));
		});
        $A.enqueueAction(action);
    },
    getUser: function(component) {
        // Create the action
        if (component.get("v.userId") != '') {
            var action = component.get("c.getUserInformation");
            action.setParams({
                "userId": component.get("v.userId")
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    console.log(response.getReturnValue());
                    component.set("v.user", response.getReturnValue());
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });

            // Send action off to be executed
            $A.enqueueAction(action);
        }
    }
})