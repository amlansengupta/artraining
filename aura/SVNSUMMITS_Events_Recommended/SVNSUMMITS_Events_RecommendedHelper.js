// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getEventList : function(cmp) {
        var self = this;

        // Load all contact data
        var titletext = cmp.get("v.titletext");
        var numberofresults = cmp.get("v.numberofresults");
        var Contactdetailpage = cmp.get("v.eventDetailURL");
        var searchstr = cmp.get("v.searchstr");
        var filterOn = cmp.get("v.filterOn");
        var debugMode = cmp.get("v.debugMode");
        var displayMode = cmp.get("v.displayMode");
        var sortBy = cmp.get("v.sortBy");

        cmp.set("v.titletext", titletext);
        cmp.set("v.eventDetailURL",Contactdetailpage);
        cmp.set("v.searchstr", searchstr);
        cmp.set("v.displayMode", displayMode);
        cmp.set("v.filterOn",filterOn);

        var action = cmp.get("c.getEvents");
        action.setParams({
            eventListFlag: cmp.get("v.titletext") !== 'Recommended For You',
            numberofresults: cmp.get("v.numberofresults"),
            listSize: cmp.get("v.listSize"),
            strfilterType: cmp.get("v.filterType"),
            strRecordId : cmp.get("v.topicValue"),
            networkId : '',
            sortBy  : cmp.get("v.sortBy"),
            filterByTopic : cmp.get("v.filterByTopic"),
            topicName : '',
            filterBySearchTerm : '',
            searchTerm : cmp.get("v.searchstr"),
            filterOn : cmp.get("v.filterOn"),
            fromDate : cmp.get("v.fromDate"),
            toDate : cmp.get("v.toDate")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            this.debug(cmp,"State of the action --->>", state);

            if (cmp.isValid() && state === "SUCCESS") {
                this.debug(cmp,"RES --->>", response.getReturnValue());
                var eventsListWrapper  = response.getReturnValue();

                eventsListWrapper.strTimeZone = self.getTimeZone(eventsListWrapper);

                for (var i=0; i < eventsListWrapper.objEventList.length; i++){

                    //var endDate =  eventsListWrapper.objEventList[i].End_DateTime__c.getTime();
                    var startDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                    var startTime = moment(startDate).toDate();

                    var endDate = moment(eventsListWrapper.objEventList[i].End_DateTime__c).format('YYYY-MM-DD HH:mm:ss');
                    var endTime = moment(endDate).toDate();

                    var diffDays = Math.round(Math.abs((endTime.getTime() - startTime.getTime())/(24*60*60*1000)));
                    eventsListWrapper.objEventList[i].daysOfMultiDaysEvent = diffDays;

                    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
                    eventsListWrapper.objEventList[i].strDay = days[startTime.getDay()];

                    eventsListWrapper.objEventList[i].strMonth = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("MMM");
                    eventsListWrapper.objEventList[i].intDate = moment(eventsListWrapper.objEventList[i].Start_DateTime__c).format("DD");
                    eventsListWrapper.objEventList[i].strMinute = moment(startTime).format('HH:mm a');

                    if(eventsListWrapper.objEventList[i].Name.length > 15){
                        eventsListWrapper.objEventList[i].Name = eventsListWrapper.objEventList[i].Name.substring(0,15);
                    }

                    eventsListWrapper.objEventList[i].topics = [];
                    eventsListWrapper.objEventList[i].topics.push(eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);
                    this.debug(cmp, '>>>>topics>>>>',eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);
                }

                cmp.set("v.wrappedEvents", eventsListWrapper);

            }
        });
        $A.enqueueAction(action);
    },

	// adjust for guest user
    getTimeZone : function(eventsListWrapper) {
        return !eventsListWrapper.strTimeZone || eventsListWrapper.strTimeZone === 'GMT'
            ? moment.tz.guess()
			: eventsListWrapper.strTimeZone;
    },

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if(debugMode){
            if(msg){
                console.log(msg);
            }
            if(variable){
                console.log(variable);
            }
        }
    }
})