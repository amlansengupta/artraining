// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getSitePrefix : function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s",""));
        });
        $A.enqueueAction(action);

    },

    // Method used for get the records on the basis of sorting options.
	sortMyMembers : function(component)   {
		
        var cmpTarget = component.find('checkImg');

        var appEvent = $A.get("e.c:SVNSUMMITS_Members_Filters_Event");
		
        if(cmpTarget.elements[0].className === 'showImg tickSymbol' || cmpTarget.elements[0].className === 'tickSymbol showImg'){
        
            $A.util.removeClass(cmpTarget, 'showImg');
        	$A.util.addClass(cmpTarget, 'hideImg');

            component.set("v.isSortBySelected",false);

            appEvent.setParams({
                "searchMyMembers" : ''
            });

            if(component.get("v.isSearchText") === true){
                appEvent.setParams({
                    "searchString" : component.get("v.searchString"),
                });
            }

        }else{

            $A.util.removeClass(cmpTarget, 'hideImg');
        	$A.util.addClass(cmpTarget, 'showImg');

            component.set("v.isSortBySelected",true);

            appEvent.setParams({
                "searchMyMembers" : component.find("myMmbrs").get("v.value"),
            });

            if(component.get("v.isSearchText") === true){
                appEvent.setParams({
                    "searchString" : component.get("v.searchString"),
                });
            }
        }
        appEvent.fire();
	},
    // Method used for fetch the records on the basis of input search string.
    searchMembers: function(component) {
        
        var searchString = document.getElementById("searchTextbox").value;
        
        component.set("v.isSearchText",true);
        
        var appEvent = $A.get("e.c:SVNSUMMITS_Members_Filters_Event");
        
        component.set("v.searchString",searchString);
        
        appEvent.setParams({
            "searchString" : searchString,
        });
        
        if(component.get("v.isSortBySelected") === true){
            appEvent.setParams({
                "searchMyMembers" : component.find("myMmbrs").get("v.value"),
            });
        }
        appEvent.fire();
    }    
})