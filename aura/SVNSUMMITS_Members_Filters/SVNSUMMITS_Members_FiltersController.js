// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit: function(component, event, helper) {
        helper.getSitePrefix(component);
    },

    // Method used for get the records on the basis of sorting options.
    sortByMyMembers : function(component, event, helper) {
		
        helper.sortMyMembers(component);
        
    },
	// Method used for fetch the records on the basis of input search string.
    getSearchString : function(component, event, helper) {
    	
        helper.searchMembers(component);
        
    }
})