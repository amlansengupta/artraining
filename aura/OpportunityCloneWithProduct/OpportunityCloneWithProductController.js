({
    doInit : function(component, event, helper) {
        if($A.get("$Browser.isPhone")){
            alert('This feature is not available for mobile experience');
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.recordId"),
                "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else{
            component.set("v.NoActive",false);
            component.set("v.AtleastOneActiveFlag",false);
            component.set("v.flag",false);
            //component.set("v.mandatoryTypeerr",'');
            var recId =component.get("v.recordId"); 
            //var recId = "0064D000004zsv9QAA";
            var action = component.get("c.getProdStatusCount"); 
            action.setParams({
                "opp" : recId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var prodDetail = response.getReturnValue();
                    var ProdCount = JSON.parse(prodDetail)[0];
                    
                    if((ProdCount.inactivecount==0||ProdCount.inactivecount>0) && ProdCount.activecount==0){                    
                        component.set("v.isProductFetched",false);
                        component.set("v.NoActive",true);
                        /* alert('There are no active products associated with this opportunity. The cloning cannot be completed.');                    
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();*/
                }else if(ProdCount.inactivecount>0 && ProdCount.activecount>0 ){                  
                    //alert('One or more products associated with this opportunity are no longer active. Only active products have been copied to the cloned record.');            
                    component.set("v.AtleastOneActiveFlag",true);
                    /*component.set("v.isProductFetched",true);
                    //12638 replacing step with stage
                    //helper.fetchStepOptions(component,recId,false);
                    helper.fetchCloseStageReasons(component,recId);
                    helper.fetchOpportunityRecord(component,recId,"True","False");
                    //12638 replacing step with stage with expansion flag false
                    //alert('hi');                    
                    helper.fetchStageOptions(component,recId,false);
                    helper.fetchOfficeOptions(component,recId);
                    helper.fetchTypeOptions(component,recId);*/
                }else if(ProdCount.inactivecount==0 && ProdCount.activecount>0){
                    component.set("v.isProductFetched",true);
                    //12638 replacing step with stage
                    //helper.fetchStepOptions(component,recId,false);
                    helper.fetchCloseStageReasons(component,recId);
                    helper.fetchOpportunityRecord(component,recId,true,false);
                    //12638 replacing step with stage with expansion flag false
                    //alert('hi'); 
                    helper.fetchStageOptions(component,recId,false);
                    helper.fetchOfficeOptions(component,recId);
                    helper.fetchTypeOptions(component,recId);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
            $A.enqueueAction(action);
        }
    },
    onCloneSave : function(component, event, helper){
        component.set("v.flag",false);
        component.set("v.Errormsg",'');
        component.set("v.Errormsg1",'');
        component.set("v.Errormsg2",'');
        component.set("v.Errormsg3",'');
        component.set("v.Errormsg4",'');
        event.preventDefault();
        var eventFields = event.getParam("fields");
        var Type = component.get("v.selectedType");
        //var Type = eventFields["Type"];
        var prob = component.find("prob").get("v.value");
        var opp = component.get("v.Opp");
        var off= component.get("v.selectedOffice");
        //alert(off);
        var listerr =  [];
        var flagErr =false;
        var Account = eventFields["AccountId"];
        var Buyer = eventFields["Buyer__c"];
        //alert(opp.AccountId);
        //alert(opp.OwnerId);
        //alert(opp.Buyer__c);
        if(Type==null || Type==''||Type=='None' ){
            $A.util.addClass(component.find("fteError"),"show");
            component.set("v.Errormsg1","Opportunity Type  is Mandatory");
            flagErr=true;
            
        }
        else{
            flagErr=false;
        }
        
        if(opp.AccountId ==''||opp.AccountId=='undefined'||opp.AccountId==null){
            $A.util.addClass(component.find("AccountError"),"show");
            component.set("v.Errormsg2","Account is Mandatory");
            flagErr=true;
            //component.set("v.Errormsgs",listerr);
            // component.set("v.flag",true);
        }
        if(opp.OwnerId ==''||opp.OwnerId=='undefined'||opp.OwnerId==null||(JSON.stringify(opp.OwnerId) === JSON.stringify({}))){
            //listerr.push("Owner is mandatory");
            flagErr = true;
            //component.set("v.Errormsgs",listerr);
            // component.set("v.flag",true);
            $A.util.addClass(component.find("OwnerError"),"show");
            component.set("v.Errormsg3","Owner is Mandatory");
        }
        if(off ==''||off =='undefined'||off == null){
            //alert('hi');
            //listerr.push("Owner is mandatory");
            flagErr = true;
            var v =component.find("officeError");
            //alert(v);
            //component.set("v.Errormsgs",listerr);
            // component.set("v.flag",true);
            $A.util.addClass(component.find("officeError"),"show");
            component.set("v.Errormsg4","Opportunity Office is Mandatory");
        }
        
        
        /*else if(opp.Buyer__c==''){
           opp.Buyer__c = null;
        }*/                                           
        if(flagErr == false){
            
            $A.util.removeClass(component.find("fteError"),"None");
            $A.util.removeClass(component.find("AccountError"),"None");
            $A.util.removeClass(component.find("OwnerError"),"None");
            $A.util.removeClass(component.find("OfficeError"),"None");
            var buyerId;
            var ownerid;
            var accId;
            if(opp.Buyer__c =='' || opp.Buyer__c == null || opp.Buyer__c =='undefined'){
                buyerId=null;
            }
            else{
                if($A.get("$Browser.isPhone")){
                    buyerId =opp.Buyer__c.Id;
                }
                else
                    buyerId =opp.Buyer__c;
            }
            if($A.get("$Browser.isPhone")){
                ownerid = opp.OwnerId.Id;
                accId = opp.AccountId.Id;
                
            }
            else{
                accId= opp.AccountId;
                ownerid = opp.OwnerId;
            }
            // alert(opp.OwnerId );
            //alert(JSON.stringify(opp.OwnerId));   
            //12638 replacing step with stage from fieldset
            var fieldSet = {"Name":opp.Name,//"Step__c":component.get("v.selectedStepValue"),
                            "AccountId":accId,"StageName":component.get("v.selectedStage"),
                            "CloseDate":eventFields["CloseDate"],"Type":component.get("v.selectedType"),
                            "Probability":prob,"Description":opp.Description,
                            "Next_Action__c":eventFields["Next_Action__c"],"Buyer__c":buyerId,
                            "Opportunity_Office__c":component.get("v.selectedOffice"),
                            "Closed_Stage_Reason_Detail__c":opp.Closed_Stage_Reason_Detail__c,
                            //"Opportunity_Country__c":eventFields["Opportunity_Country__c"],
                            "OwnerId":ownerid,
                            "RecordTypeId":component.get("v.opportunity").RecordTypeId
        					};
            
            console.log('Field to Save ' + JSON.stringify(fieldSet));
            console.log(fieldSet);
            //event.preventDefault();
            var action = component.get("c.saveClonedOpportunity");
            var opts = [];
            action.setParams({
                "clonedCopy" : JSON.stringify(fieldSet),
                "origOpportunity" : component.get("v.opportunity"),
                "isExpansion" : false
            });
            component.set("v.Spinner",true);
            action.setCallback(this, function(response){
                var state = response.getState();
                // alert(state);
                if(state == "SUCCESS"){
                    component.set("v.Spinner",false);
                    var result = response.getReturnValue();
                    //alert(result.clonedId);
                    if(result.clonedId!=null){
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": result.clonedId,
                            "slideDevName": "detail"
                        });                
                        navEvt.fire();
                    }
                    else{
                        //alert(result.errorToShow);
                        //listerr.push(result.errorToShow);
                        component.set("v.Errormsg",result.errorToShow);
                        component.set("v.flag",true); 
                    }
                    
                }else if (state === "ERROR") {
                    component.set("v.Spinner",false);
                    var errors = response.getError();
                    //alert(JSOn.stringify(errors));
                    if (errors) {
                        
                        if(errors[0] && errors[0].message){
                            //listerr.push(errors[0].message);
                            component.set("v.Errormsg",errors[0].message);
                            component.set("v.flag",true);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    closeWindow:function(component,event,helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "detail"
        });
        
        navEvt.fire();
        
    },
    makeisProductFetchedtrue:function(component,event,helper){
        component.set("v.isProductFetched",true);
        //12638 replacing step with stage
        //helper.fetchStepOptions(component,recId,false);
        helper.fetchCloseStageReasons(component,recId);
        helper.fetchOpportunityRecord(component,recId,true,false);
        //12638 replacing step with stage with expansion flag false
        //alert('hi');                    
        helper.fetchStageOptions(component,recId,false);
        helper.fetchOfficeOptions(component,recId);
        helper.fetchTypeOptions(component,recId); 
    }
})