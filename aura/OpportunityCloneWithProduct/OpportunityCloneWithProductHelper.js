({
	fetchOpportunityRecord : function(component, recordId, isCloned, isExpansion){
        
        var action = component.get("c.getOpportunityRecordDetails"); 
        action.setParams({
            "oppid" : recordId,
            "isCloned" : isCloned,
            "isexpansion" : isExpansion
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var oppDetail = response.getReturnValue();
                console.log(JSON.stringify(oppDetail));
                console.log(oppDetail.Opportunity_Office__c);
                //alert(oppDetail.Name);
//alert(oppDetail.OwnerId);
                component.set("v.Opp", oppDetail);
                component.set("v.ownerFlag",true);
                 component.set("v.opportunity", oppDetail);
                window.setTimeout(
                $A.getCallback( function() {
                    //12638 removing set from UI to Controller
                    //component.find("step_default").set("v.value", oppDetail.Step__c);
                    component.find("stage_default").set("v.value", oppDetail.StageName);
                    component.find("office_default").set("v.value", oppDetail.Opportunity_Office__c);
                    component.find("type_default").set("v.value", oppDetail.Type);
                }));
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    /* 12638 Commenting step options method*/
    /*fetchStepOptions : function(component, recordId, isExpansion){
        var action = component.get("c.getstepVal");
        action.setParams({
            "isExpansion" : isExpansion
        });
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var stepValues = response.getReturnValue();
                console.log(JSON.stringify(stepValues));
                for (var loopCounter = 0; loopCounter < stepValues.length; loopCounter++) {
                    opts.push(stepValues[loopCounter]);
                }
                component.set("v.stepOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },*/
    /****12638 replacing step with stage picklist values with expansion flag ****/
    fetchStageOptions : function(component, recordId, isExpansion){
        //alert(isExpansion);
        var action = component.get("c.getStages");
        action.setParams({
            "isExpansion" : isExpansion
        });
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if(state == "SUCCESS"){
                var stepValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < stepValues.length; loopCounter++) {
                    //alert(stepValues[loopCounter]);
                    opts.push(stepValues[loopCounter]);
                }
                component.set("v.stageOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    fetchOfficeOptions : function(component,recordId){
      var action = component.get("c.getOffice");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var reasonValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < reasonValues.length; loopCounter++) {
                    opts.push(reasonValues[loopCounter]);
                }
                component.set("v.officeOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);   
    },
    fetchTypeOptions : function(component,recordId){
       var action = component.get("c.getType");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var reasonValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < reasonValues.length; loopCounter++) {
                    opts.push(reasonValues[loopCounter]);
                }
                component.set("v.typeOptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action); 
    },
    fetchCloseStageReasons : function(component, recordId){
        var action = component.get("c.getcloseStageReasonVal");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var reasonValues = response.getReturnValue();
                for (var loopCounter = 0; loopCounter < reasonValues.length; loopCounter++) {
                    opts.push(reasonValues[loopCounter]);
                }
                component.set("v.closeStageReasons", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
   fetchChargeCodeExceptions : function(component, recordId){
        var action = component.get("c.getChargeCodeExepVal");
        var opts = [];
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    /*opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });*/
                    opts.push(allValues[i]);
                }
                component.set("v.chargeCodeExceptions", opts);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})