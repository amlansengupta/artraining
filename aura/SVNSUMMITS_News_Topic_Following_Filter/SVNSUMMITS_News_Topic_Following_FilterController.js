/**
 * @File Name     :
 * @Created by    : 7Summits
 * @Description   :
 **/
({

    onChange: function(cmp, evt, helper){
        var checkbox = evt.getSource();
        var checked = checkbox.get("v.checked");

        var appEvent = $A.get("e.c:SVNSUMMITS_News_Topic_Following_Event");
        appEvent.setParams({
            "filterByFollowingTopic": checked
        });

        appEvent.fire();
    }

})