({
    doInit : function(component, event, helper) {
        var action = component.get("c.getOpportunity");
        action.setParams({
            "oppId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var oppObj = response.getReturnValue();
                if(oppObj.Opp_Number_of_LOBs__c == 0) {
                    alert('No product to be updated in WEBCAS');
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": oppObj.Id,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }
                else {
                    component.set("v.isProductFetched",true);
                    if (oppObj.Opp_Number_of_LOBs__c == 1) {
                        helper.pullEmailDetails(component, oppObj.Id);
                    }
                    else if(oppObj.Opp_Number_of_LOBs__c > 1) {
                        helper.pullEmailDetails(component, oppObj.Id);
                    }
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    handleUploadFinished : function(component, event){
        var prevUploads = component.get("v.uploadedFiles");
        var totalAttachments = prevUploads.concat(event.getParam("files"));
        component.set("v.uploadedFiles", totalAttachments);
    },
    sendMail : function(component, event, helper){
        var toRecipients = component.get("v.emailTo");
        var ccRecipients = component.get("v.emailCc");
        var bccRecipients = component.get("v.emailBcc");
        var uploadedFiles = component.get("v.uploadedFiles");
        var mailSubject = component.get("v.subject");
        var emailBody = component.get("v.mailTemplate");
        var fieldSet = {"To":component.get("v.emailTo"),
                        "Cc":component.get("v.emailCc"),
                        "Bcc":component.get("v.emailBcc"),
                        "Attachments":component.get("v.uploadedFiles"),
                        "Subject":component.get("v.subject"),
                        "Body":component.get("v.mailTemplate")};
        var action = component.get("c.sendEmail");
        var opts = [];
        console.log("eamilcontent : "+JSON.stringify(fieldSet));
        action.setParams({
            "emailContent" : JSON.stringify(fieldSet)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "detail"
                });
                navEvt.fire();
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    deleteAttachment : function(component, event, helper){
        var selectedAttachment = event.target.id;
        var uploadedAttachments = component.get("v.uploadedFiles");
        var uploadedDocId = '';
        for(var loopCounter = 0; loopCounter < uploadedAttachments.length; loopCounter++){
            if(uploadedAttachments[loopCounter].name == selectedAttachment){
                uploadedDocId = uploadedAttachments[loopCounter].documentId;
                uploadedAttachments.splice(loopCounter, 1);
                break;
            }
        }
        if(uploadedDocId != ""){
            var action = component.get("c.deleteUploadedDocument");
            action.setParams({
                "docId" : uploadedDocId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.uploadedFiles", uploadedAttachments);
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    }
})