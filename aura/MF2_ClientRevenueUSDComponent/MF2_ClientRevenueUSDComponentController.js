({
    doInit : function(component, event, helper){
        var action = component.get("c.fetchRevenueDetails");
        action.setParams({
            "accId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnedVal = response.getReturnValue();
                console.log(JSON.stringify(returnedVal));
                component.set("v.account",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    EditCompOpen:function(component,event,helper){
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-show");
        $A.util.addClass(accDisp,"slds-hide");
        $A.util.removeClass(accEdit,"slds-hide"); 
        $A.util.addClass(accEdit,"slds-show");
    },
    
    closeModal:function(component,event,helper){    
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-hide");
        $A.util.addClass(accDisp,"slds-show");
        $A.util.removeClass(accEdit,"slds-show"); 
        $A.util.addClass(accEdit,"slds-hide");
    },
    
    handleOnload : function(component, event, helper) {
        var recUi = event.getParam("recordUi");
    },
    
    handleOnSubmit : function(component, event, helper) {
        event.preventDefault();  // stop the form from submitting
        var eventFields = event.getParam("fields");
        component.find('form').submit(eventFields);
    },
    handleError: function(component, event) {
        var errors = event.getParams();
        console.log("response", JSON.stringify(errors));
    },
    handleOnSuccess : function(component, event, helper) {
        var payload = event.getParams().response;
		console.log(payload.id);
        
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-hide");
        $A.util.addClass(accDisp,"slds-show");
        $A.util.removeClass(accEdit,"slds-show"); 
        $A.util.addClass(accEdit,"slds-hide");
	},
    
    dateChange: function(component, event, helper){
        var dateField = component.find("DateasOf");
        var date = dateField.get("v.value");
        component.set("v.simpleRecord.Client_Type__c", date);
    },
    
     handleSaveRecord: function(component, event, helper) {
        component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
            // NOTE: If you want a specific behavior(an action or UI behavior) when this action is successful
            // then handle that in a callback (generic logic when record is changed should be handled in recordUpdated event handler)
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                // handle component related logic in event handler
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    }

    
})