// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    setSitePrefix : function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
                var sitePath = actionResult.getReturnValue();
                component.set("v.sitePrefix", sitePath.replace("/s",""));
        });
        $A.enqueueAction(action);
    },

    sortByUpcoming : function(component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");
        //var selectLabel = document.getElementById('label-holder').getAttribute('data-Upcoming');

        //console.log("selectLabel = " + component.get("v.sortByLabel"));
        //console.log("upcoming event - eventSortFilter");
        //console.log("listViewMode = " + component.get("v.listViewMode"));
        appEvent.setParams({
            "sortBy" : "Upcoming",
            "sortByLabel" : component.get("v.sortByUpcoming"),
            "listViewMode" : component.get('v.listViewMode'),
        });
        appEvent.fire();
    },

    sortByTopAttendees : function(component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");
        //console.log("attendees event - eventSortFilter");
        //console.log("selectLabel = " + component.get("v.sortByLabel"));
        //console.log("listViewMode = " + component.get("v.listViewMode"));
        appEvent.setParams({
            "sortBy" : "TopAttendees",
            "sortByLabel" : component.get("v.sortByAttendance"),
            "listViewMode" : component.get('v.listViewMode'),
        });
        appEvent.fire();
    },

    setSortOption: function(component, event, helper) {

        if (component.get('v.sortBy') === 'Upcoming') {
            //var selectLabel = component.find('SortSelect').get("v.value");
            //var thisSelect = component.find('SortSelect');
            //var selectLabel = $("#label-holder").attr("data-Upcoming");
            //var thisParent = thisSelect.parentElement;
            //var selectLabel = document.getElementById('label-holder').getAttribute('data-Upcoming');
            //var thisSelected = thisParent.getAttribute('data-Upcoming');
            //console.log("selectLabel = " + selectLabel );
            //component.set('v.sortByLabel', selectLabel);

            this.sortByUpcoming(component);
        } else if (component.get('v.sortBy') === 'TopAttendees') {
            //var selsectLabel = component.find('SortSelect').get("v.value");
            //var selectLabel = $("#label-holder").attr("data-TopAttendees");
            //component.set('v.sortByLabel', selectLabel);
            //console.log("selectLabel = " + selectLabel );

            this.sortByTopAttendees(component);
        }
    }
})