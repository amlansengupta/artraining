// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({

	doInit: function(component, event, helper) {
        helper.setSitePrefix(component);
        helper.setSortOption(component, event, helper);
	},

    setSortOption: function(component, event, helper) {
        helper.setSortOption(component, event, helper);
    }

})