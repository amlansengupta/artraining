// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit : function(component, event, helper) {
        var totalResults = event.getParam("totalResults");
        var sortByLabel = event.getParam("sortByLabel");

        if(totalResults !== undefined){
            component.set("v.numberOfResults", totalResults);
        }

        if(sortByLabel !== undefined){
            component.set("v.sortByLabel", sortByLabel);
        }

    },

    setParameters : function(component, event, helper) {
        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");
        appEvent.setParams({
            "sortBy" :  component.get("v.sortBy"),
            "listViewMode" : component.get("v.defaultView"),
            "sortByLabel" : component.get("v.sortByLabel"),
        });
        // console.dir(appEvent);
        appEvent.fire();

        if (component.get("v.sortBy") === 'Upcoming') {
            //component.set("v.sortByLabel", $A.get('$Label.c.ss_label_Upcoming'));
            var cmpTarget = component.find('upcommingImg');
            if ($A.util.hasClass(cmpTarget, 'hideImg')) {
                $A.util.removeClass(cmpTarget, 'hideImg');
                $A.util.addClass(cmpTarget, 'showImg');
            }

            var cmpTarget1 = component.find('topAttendanceImg');
            if ($A.util.hasClass(cmpTarget1, 'showImg')) {
                $A.util.removeClass(cmpTarget1, 'showImg');
                $A.util.addClass(cmpTarget1, 'hideImg');
            }
        } else {
            var cmpTarget2 = component.find('topAttendanceImg');
            $A.util.removeClass(cmpTarget2, 'hideImg');
            $A.util.addClass(cmpTarget2, 'showImg');

            var cmpTarget3 = component.find('upcommingImg');
            $A.util.removeClass(cmpTarget3, 'showImg');
            $A.util.addClass(cmpTarget3, 'hideImg');
        }
    },

    setSitePrefix : function(component, event, helper) {
        helper.getSitePrefix(component);
        helper.debug(component,"Method called",null);
    },

    //Changed due to resolve the java script Error of Renderer/afterRender
    afterScriptsLoaded: function(component, event, helper) {
        svg4everybody();
    },

    sortByUpcoming : function(component, event, helper) {
        if (component.get('v.currentOption') === 'Upcoming') return;

        component.set('v.currentOption', 'Upcoming');
        //component.set('v.sortBy',component.find("Upcoming").get("v.value"));
        component.set('v.sortBy',component.find("Upcoming").get("v.text"));
        //var sortLabel = component.find("Upcoming").get("v.value");
        component.set('v.sortByLabel', $A.get('$Label.c.ss_label_Upcoming'));

        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");
        appEvent.setParams({
            //"sortBy" : component.find("Upcoming").get("v.value"),
            "sortBy" : component.find("Upcoming").get("v.text"),
            "listViewMode" : component.get("v.listViewMode"),
            "sortByLabel" : component.get("v.sortByUpcoming")
        });
        appEvent.fire();

        var cmpTarget = component.find('upcommingImg');
        if ($A.util.hasClass(cmpTarget, 'hideImg')) {
            $A.util.removeClass(cmpTarget, 'hideImg');
            $A.util.addClass(cmpTarget, 'showImg');

        }

        var cmpTarget1 = component.find('topAttendanceImg');
        if ($A.util.hasClass(cmpTarget1, 'showImg')) {
            $A.util.removeClass(cmpTarget1, 'showImg');
            $A.util.addClass(cmpTarget1, 'hideImg');
        }
    },

    sortByTopAttendees : function(component, event, helper) {

        if (component.get('v.currentOption') == 'TopAttendees') return ;

        component.set('v.currentOption', 'TopAttendees');
        component.set('v.sortBy',component.find("TopAttendees").get("v.text"));
        //component.set('v.sortByLabel', component.get('$Label.c.ss_label_TopAttendees'));

        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");

        appEvent.setParams({
            //"sortBy" : component.find("TopAttendees").get("v.value"),
            "sortBy" : component.find("TopAttendees").get("v.text"),
            "listViewMode" : component.get("v.listViewMode"),
            "sortByLabel" : component.get('v.sortByAttendance'),
        });
        appEvent.fire();

        var cmpTarget = component.find('topAttendanceImg');
        $A.util.removeClass(cmpTarget, 'hideImg');
        $A.util.addClass(cmpTarget, 'showImg');

        var cmpTarget1 = component.find('upcommingImg');
        $A.util.removeClass(cmpTarget1, 'showImg');
        $A.util.addClass(cmpTarget1, 'hideImg');

        if ($A.util.hasClass(cmpTarget, 'hideImg')) {
            $A.util.removeClass(cmpTarget, 'hideImg');
            $A.util.addClass(cmpTarget, 'showImg');
            $A.util.toggleClass(cmpTarget, "hideImg");
        }

        cmpTarget1 = component.find('upcommingImg');
        if ($A.util.hasClass(cmpTarget1, 'showImg')) {
            $A.util.removeClass(cmpTarget1, 'showImg');
            $A.util.addClass(cmpTarget1, 'hideImg');
        }
    },

    setListView : function(component, event, helper) {
        var cmpTargetSortBy = component.find('sortDiv');
        $A.util.removeClass(cmpTargetSortBy, 'hideImg');
        $A.util.addClass(cmpTargetSortBy, 'showImg');

        $('.CCEVENTSLCSVNSUMMITS_Events_Date_Filter').show();
        $A.util.removeClass(component.find('calendarBTN'), 'btnActive');
        $A.util.addClass(component.find('listBTN'), 'btnActive');

        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");

        appEvent.setParams({
            "sortBy" : component.get("v.sortBy"),
            "listViewMode" : component.find("List").get("v.value"),
        });

        appEvent.fire();
    },

    setCalendarView : function(component, event, helper) {
        var cmpTarget1 = component.find('sortDiv');
        $A.util.removeClass(cmpTarget1, 'showImg');
        $A.util.addClass(cmpTarget1, 'hideImg');

        $('.CCEVENTSLCSVNSUMMITS_Events_Date_Filter').hide();

        $A.util.addClass(component.find('calendarBTN'), 'btnActive');
        $A.util.removeClass(component.find('listBTN'), 'btnActive');

        var appEvent = $A.get("e.c:SVNSUMMITS_Events_SortBy_Filter_Event");

        appEvent.setParams({
            "sortBy" : component.get("v.sortBy"),
            "listViewMode" : component.find("Calendar").get("v.value"),
        });
        appEvent.fire();

    },

    setDisplayMode : function(component, event, helper) {
        var listViewMode = event.getParam("listViewMode");
        component.set("v.listViewMode", listViewMode);
    }
})