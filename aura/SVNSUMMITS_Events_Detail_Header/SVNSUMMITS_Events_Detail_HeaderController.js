// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    fetchUrl : function(component, event, helper) {
        helper.debug(component,"Header Called",null);

        var eventRecordId = component.get("v.recordId");

        var action = component.get("c.getEventRecord");
        action.setParams({
            eventRecordId: eventRecordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var eventsListWrapper = response.getReturnValue();


                for (var i = 0; i < eventsListWrapper.objEventList.length; i++) {
                    eventsListWrapper.objEventList[i].topics = [];
                    eventsListWrapper.objEventList[i].topics.push(eventsListWrapper.eventsToTopicsMap[eventsListWrapper.objEventList[i].Id]);
                }

                component.set("v.wrappedEventsObj", eventsListWrapper);
            }

        });
        $A.enqueueAction(action);
        var action1 = component.get("c.getSitePrefix");
        action1.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
        });
        $A.enqueueAction(action1);
    }
})