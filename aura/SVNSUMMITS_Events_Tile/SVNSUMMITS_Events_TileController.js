// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    setSitePrefix : function(component, event, helper) {
        helper.get_SitePrefix(component);
    },

    doInitialization : function(component, event, helper){
        return;
        var listSize = component.get("v.listSize");

        window.setTimeout(
            $A.getCallback(function() {
                var totalEvents= component.get("v.totalEvents");
                if (totalEvents > 0) {
                    if (totalEvents % 2 === 0) {

                    } else {
                        component.set("v.isOdd",true);
                    }
                }
            }), 5000
        );

        // List view logic for search and topic pages
        if (listSize > 0){
            if (listSize % 3 === 0){

            } else {
                component.set("v.isTotalOdd",true);

                if (listSize % 3 === 1) {
                    component.set("v.isAddCol",true);
                }
                else if(listSize % 3 === 2) {
                    component.set("v.isAddCol",false);
                }
            }
        }

        //List view addition div logic
        if (listSize > 0) {
            if (listSize % 2 === 0) {

            } else {
                component.set("v.isOdd",true);
            }
        }
    },

	goToRecord : function(component, event, helper){
        $A.get("e.force:navigateToSObject")
		  .setParams({
		      "recordId": $(event.currentTarget).data("id"),
	          "slideDevName": "related"})
          .fire();
	},

    gotoURL : function (component, event, helper) {
        var isChrome = !!window.chrome && !!window.chrome.webstore;
		var isFirefox = typeof InstallTrigger !== 'undefined';

        if ($A.get("$Browser.isAndroid") || isChrome || isFirefox) {
            location.reload();
        }
    }
})