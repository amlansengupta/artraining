({
	doInit : function(component, event, helper) {
		var action = component.get("c.getGroupKeyContact");
		action.setParams({
			groupId : component.get("v.recordId"),
			roleName: component.get("v.roleName")
		});
		action.setCallback(this, function(r){
			component.set("v.contact", r.getReturnValue());
		})
		$A.enqueueAction(action);
	},

	goToProfile : function(component, event, helper){
		var profileId = event.currentTarget.dataset.id;

    var navEvt = $A.get("e.force:navigateToSObject");
    navEvt.setParams({
      "recordId": profileId,
      "slideDevName": "detail"
    });
    navEvt.fire();
	}
})