({
    doInit: function(component, event, helper){
        if(component.get("v.objectAPIName").toLowerCase() === 'account'){
            component.set("v.objectAcc", true);
        } 
        
       if(component.get("v.oRecord").Email_Address__c != null && component.get("v.oRecord").Email_Address__c != ''){
           component.set("v.emailAddressPresent", true); 
            
        }
        
        if(component.get("v.oRecord").OwnerId == '' 
           || component.get("v.oRecord").OwnerId == null||component.get("v.oRecord").OwnerId == 'undefined'){
            component.set("v.OwnerPresent", false);
        }
        else{
            component.set("v.OwnerPresent", true);
        }
    },
    
    selectRecord : function(component, event, helper){      
          var getSelectRecord = component.get("v.oRecord");
          var Event = component.getEvent("oSelectedRecordEvent");
          Event.setParams({"recordByEvent" : getSelectRecord });  
          Event.fire();
    },
    
    openPop : function(component, event, helper) {
        var pop = component.find('pop');
        $A.util.addClass(pop, 'slds-show');
        $A.util.removeClass(pop, 'slds-hide');
        
    },
    
    closePop : function(component, event, helper) {
        var pop = component.find('pop');
        $A.util.addClass(pop, 'slds-hide');
        $A.util.removeClass(pop, 'slds-show');
        
    },
})