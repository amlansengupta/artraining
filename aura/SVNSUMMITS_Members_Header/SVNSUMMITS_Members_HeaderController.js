// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	setNoOfMembers : function(component, event, helper) {
        
        var numberOfMembers = event.getParam("totalResults");
        component.set("v.numberOfMembers",numberOfMembers);
        helper.getSitePrefix(component);
	},

    selectSortBy : function(component, event, helper) {
		var appEvent = $A.get("e.c:SVNSUMMITS_Members_SortBy_Event");
        appEvent.setParams({
            "sortBy" : component.find("headerSort").get("v.value"),
        });
        appEvent.fire();
    },
    
    afterScriptsLoaded: function(component) {
    	svg4everybody();
        
        $(function()
          {
              $('.dropdown')
              .dropdown({
                  maxSelections: 3
              })
              ;
              $('.dropdown')
              .dropdown({
                  useLabels: false,
                  maxSelections: 3
              });
          });
	},

})