// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    getSitePrefix : function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s",""));
        });
        $A.enqueueAction(action);
        
    },
    
    initializeDropdown: function(component) {
        try {
            $(".sortingOptions")
            .addClass("ui selection dropdown")
            .dropdown({
                placeholder: "Sort by Last Name"
            });
        } catch(e) {
            this.debug(component,null,e);
        }
    },
    
    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if(debugMode) {
            if(msg) {
                console.log(msg);
            }
            if(variable) {
                console.log(variable);
            }
        }
    }
})