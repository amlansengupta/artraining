// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    afterRender : function(component, helper) {
    
        var ren = this.superAfterRender();
        helper.initializeDropdown(component);
        return ren;
        
	},
    
    rerender: function(component, helper) {
    	// By default, after the component finished loading data/handling events,
    	// it will call this render function this.superRender() will call the
    	// render function in the parent component.
    	var ret = this.superRerender();
    	//Getting error in page
    	//rerender threw an error in 'markup://c:SVNSUMMITS_Members_Header' 
    	//[ReferenceError: svg4everybody is not defined] /ccmember/s/sfsites/auraFW/javascript/dpJ3LUIaUXt-CtvrXqBcnQ/aura_prod.js:1

        //svg4everybody();
        return ret;
    }   
})