// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit: function (component, event, helper) {
        helper.getTopics(component);
    },

    setTopic: function (component, event, helper) {
        if (component.find("filterByTopic").get("v.value") === '') {
            var cmpTarget = component.find('clearTopicButton');
            $A.util.removeClass(cmpTarget, 'toggleClass1');
            $A.util.addClass(cmpTarget, 'toggleClass');
        }

        helper.sendFilterEvent(component);
    },

    clearTopic: function (component, event, helper) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Topic_Filter_Event");
        appEvent.setParams({
            "filterByTopic": null,
        });
        appEvent.fire();

        $(".topic").each(function () {
            $(this).find('a').remove();
        });

        helper.getTopics(component);
        //$('.slds-input-select-multi').dropdown('clear');
        var cmpTarget = component.find('clearTopicButton');
        $A.util.removeClass(cmpTarget, 'toggleClass1');
        $A.util.addClass(cmpTarget, 'toggleClass');
    },

    selectTopic: function (component, event, helper) {
        var cmpTarget = component.find('clearTopicButton');
        $A.util.removeClass(cmpTarget, 'toggleClass');
        $A.util.addClass(cmpTarget, 'toggleClass1');

        if (!component.get('v.showButton')) {
            helper.sendFilterEvent(component);
        }
    }
})