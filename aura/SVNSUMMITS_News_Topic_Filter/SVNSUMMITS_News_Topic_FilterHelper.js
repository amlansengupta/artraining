// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    sendFilterEvent: function (component) {
        var appEvent = $A.get("e.c:SVNSUMMITS_News_Topic_Filter_Event");
        appEvent.setParams({
            "filterByTopic": component.find("filterByTopic").get("v.value"),
        });
        appEvent.fire();
    },

    getTopics: function (component) {
        //component.set("v.semanticClass",'ui fluid search');
        this.debug(component, '====Fetch topics====', null);
        var action = component.get("c.getTopics");

        action.setCallback(this, function (response) {

            var values = response.getReturnValue();
            var valuesTemp = [];
            for (var value in values) {
                if (values.hasOwnProperty(value)) {
                    valuesTemp.push({
                        key: value,
                        value: values[value]
                    });
                }
            }
            component.set("v.values", valuesTemp);
        });
        $A.enqueueAction(action);
    },

    initializeTopicDropdown: function (component) {
        // console.log('&&&&&&&&&&',component.get('v.semanticClass'));
        try {
            $(".topic")
                .addClass("ui fluid search")
                .dropdown({
                    placeholder: "Filter by Topic"
                });

        } catch (e) {
            this.debug(component, null, e);
        }
    },

    debug: function (component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }
    }
})