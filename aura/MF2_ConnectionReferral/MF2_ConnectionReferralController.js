({
    
    doInit : function(component, event, helper) {
        var oppId = component.get("v.recordId");
        var action = component.get("c.recordTypeCheck");
        action.setParams({
            'oppId' : oppId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //opp = response.getReturnValue();
                //alert("opp "+ opp);
                // alert(JSON.stringify(response.getReturnValue()));
                component.set("v.flag", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
    },
    onSubmit : function(component, event, helper){
        event.preventDefault();
        var oppId = component.get("v.recordId");
        var eventFields = event.getParam("fields");
        var fieldSet = {"Sibling_Company__c":(eventFields["Sibling_Company__c"]!=''?eventFields["Sibling_Company__c"]:null),
                            //"AccountId":eventFields["AccountId"],
                        "Business_LOB__c":(eventFields["Business_LOB__c"]!=''?eventFields["Business_LOB__c"]:null),
                        "Sibling_Contact_Name__c":(eventFields["Sibling_Contact_Name__c"]!=''?eventFields["Sibling_Contact_Name__c"]:null),
                        "Potential_Revenue__c":(eventFields["Potential_Revenue__c"]!=''?eventFields["Potential_Revenue__c"]:null),
                        "Cross_Sell_ID__c":(eventFields["Cross_Sell_ID__c"]!=''?eventFields["Cross_Sell_ID__c"]:null),
                           };
        console.log(JSON.stringify(fieldSet));
            
            var action = component.get("c.saveOpp");
            action.setParams({
                "fieldSet" : JSON.stringify(fieldSet),
                "oppID" : oppId
            });
        action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    console.log('saved');
                    location.replace("/lightning/r/Opportunity/"+oppId+"/view");
                }
            	else
                {
                    var errors = response.getError();
                    var toastParams = {
             		title: "Error",
             		message: errors[0].message, // Default error message
             		type: "error"
            		};
                    
                    var toastEvent = $A.get("e.force:showToast");
           			toastEvent.setParams(toastParams);
           			toastEvent.fire(); 

                    console.log('failed');
                }
    		});
        $A.enqueueAction(action);
   },
    
    onSuccess : function(component, event, helper){
        event.preventDefault();
        var oppId = component.get("v.recordId");
        location.replace("/lightning/r/Opportunity/"+oppId+"/view");
      }, 
    
    handleOnError: function(component, event) {
        var errors = event.getParams();
        console.log("response", JSON.stringify(errors));
    }
    
})