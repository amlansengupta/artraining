({
    init: function (cmp, evt, helper) {
        console.log('in first init');
        helper.init(cmp, evt, helper);
        helper.getUser(cmp, evt, helper);

    },
    openWelcome: function (cmp, evt, helper) {
        cmp.set('v.page', '1');
        cmp.set('v.displayOnboarding', true);

    },
    openProfile: function (cmp, evt, helper) {
        cmp.set('v.page', '2');
        cmp.set('v.displayOnboarding', true);

    },

    openTopic: function (cmp, evt, helper) {
        cmp.set('v.page', '3');
        cmp.set('v.displayOnboarding', true);

    },
    openGroup: function (cmp, evt, helper) {
        cmp.set('v.page', '4');
        cmp.set('v.displayOnboarding', true);

    },
    handlePageChange : function(component, event, helper) {
        helper.handlePageChange(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        component.set("v.displayOnboarding", false);
    }
})