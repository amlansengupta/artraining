({
    init : function(cmp, evt, helper) {
        var action = cmp.get("c.getProgress");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var slide = response.getReturnValue();
                console.log('Completed: ', slide);
                cmp.set("v.completedSteps", response.getReturnValue());
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getUser : function(cmp, evt, helper) {
        var action = cmp.get("c.getUserRecord");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var user = response.getReturnValue();
                user.shortId = user.Id.substring(0,15);
                cmp.set("v.profileUser", response.getReturnValue());
                cmp.set("v.user", response.getReturnValue());
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    handlePageChange : function(component, event, helper) {
        var slideComplete=event.getParam("slide");
        var pageNumber=event.getParam("message");
        var action = component.get("c.completeSlide");
        // if "message" equals "close", the user has just completed the entire modal, so the 'Complete_Modal__c' field should be checked and the modal closed
        if(pageNumber=='Close'){
            console.log('in close');
            action.setParams({
                "slide": slideComplete
            });
            console.log(action);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    console.log('in success');
                    component.set("v.page", pageNumber);
                } else {
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
            this.closeModal(component, event, helper);
            // if "slide' is not blank, the user just completed a slide, and the database needs to be called to check the corresponding field on the User record
        } else if(slideComplete != ''){
            action.setParams({
                "slide": slideComplete
            });
            console.log(action);
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    console.log('in success');
                    component.set("v.page", pageNumber);
                } else {
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);

            // if "slide" is blank, the user is going back to a previous screen and therefore the database doesn't need to be called to check a slide complete field on the User record.
        } else {
            component.set("v.page", pageNumber);
        }
    }

})