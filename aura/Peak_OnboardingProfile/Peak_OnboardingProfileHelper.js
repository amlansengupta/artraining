({
    getListVal: function (component) {
        var action = component.get("c.getPicklistOptions");
        action.setParams({
           "pickListField": "user.Functional_Business_Area__c"
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var values = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.businessAreaList", values);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getIndustryVal: function (component) {
        var action = component.get("c.getPicklistOptions");
        action.setParams({
            "pickListField": "user.Industry__c"
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var values = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.industryList", values);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getRegionVal: function (component) {
        var action = component.get("c.getPicklistOptions");
        action.setParams({
           // "pickListField": "user.MP_Region__c"
           "pickListField": "user.Primary_Region__c"
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var values = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.regionList", values);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    updateName : function(component, event, helper) {
        var action = component.get("c.updateUserNames");
        action.setParams({
            "currentUser": component.get("v.user")
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            var currentUser = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                console.log('currentUser = ', currentUser);
                component.set("v.user", currentUser);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    updateUserInfo : function(component, event, helper) {
        var action = component.get("c.upsertUserProfile");
        action.setParams({

            "iIndustry": component.get("v.user.Industry__c"),
            "iJobTitle": component.get("v.user.Job_Title__c"),
            "iFunctionalArea": component.get("v.user.Functional_Business_Area__c"),
            "iRegion": component.get("v.user.Primary_Region__c")
        });
        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            var currentUser = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                console.log('currentUser ///= ', currentUser);
                //component.set("v.user", currentUser);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

    },
    goToNext : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");

        compEvent.setParams({"message" : "3", "slide" : "Profile"});
        compEvent.fire();

    },
    goBack : function(component, event, helper) {
        var compEvent = component.getEvent("pageChange");
        compEvent.setParams({"message" : "1", "slide" : ""});
        compEvent.fire();
    }
})