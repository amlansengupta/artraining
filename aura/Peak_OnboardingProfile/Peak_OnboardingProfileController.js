({
    init : function(component, event, helper) {
        helper.getListVal(component, event, helper);
        helper.getIndustryVal(component, event, helper);
        helper.getRegionVal(component, event, helper);
    },
    onBusinessChange: function (component) {
        var selected = component.find("business").get("v.value");
        component.set("v.businessArea", selected);
    },
    onIndustryChange: function (component) {
        var selected = component.find("industry").get("v.value");
        component.set("v.industry", selected);
    },
    onRegionChange: function (component) {
        var selected = component.find("region").get("v.value");
        component.set("v.region", selected);
    },
    goToNext : function(component, event, helper) {
        helper.updateName(component, event, helper);
        helper.updateUserInfo(component, event, helper);        
        helper.goToNext(component, event, helper);

    },
    goBack : function(component, event, helper) {
        helper.goBack(component, event, helper);
    }
})