/**
 * Created by 7Summits on 12/7/17.
 */
({
    init: function(cmp, evt, helper){
        var appendNameToHeadline = cmp.get('v.appendNameToHeadline');

        // if true, get the users first name and add it to
        // the end of the headline string.
        if(appendNameToHeadline === true){

            var action = cmp.get('c.getCurrentUser');

            action.setCallback(this, function(response) {
                var state = response.getState();

                // check if the request was successful or not
                if (cmp.isValid() && state === 'SUCCESS') {
                    cmp.set('v.currentUser', response.getReturnValue());
                }
            });

            $A.enqueueAction(action);

        }

    }

})