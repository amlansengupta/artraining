<aura:application access="GLOBAL" extends="ltng:outApp" >
    <aura:dependency resource="c:MF2_ProductSearch"/>
    <aura:dependency resource="markup://force:showToast" type="EVENT"/>
    <aura:dependency resource="markup://force:navigateToSObject" type="EVENT"/>
    <aura:dependency resource="markup://lightning:notificationsLibrary" type="EVENT"/>
    <aura:dependency resource="markup://lightning:helpText" type="COMPONENT"/>
</aura:application>