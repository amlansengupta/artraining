({
    //Function for opening the link in new window.
    redirectToUrl : function(component, event, helper){
        var oppId = component.get("v.recordId");
        var selectedItem = event.currentTarget;
        var clickedLink= selectedItem.dataset.variablename;
        var clickedType = selectedItem.name;
        
        var replacements = [];
        replacements.push(oppId);
        
        if(clickedType=='WebCAS project code')
        { 
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/DelegateCodeSetUpLauncher?id="+oppId
            });
            urlEvent.fire(); 
        }
        else if(clickedType=='Sales Credit')
        {
            var actionAPI = component.find("quickActionAPI");
            var args = { actionName :"Opportunity.Edit_Sales_Credit" };
            actionAPI.invokeAction(args);   
        }
        else if(clickedType=='Product'){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/MF2_ProductSearchContainerPage?id="+oppId
            });
            urlEvent.fire();     
        }
        else
        {
            for (var i = 0; i < replacements.length; i++) {
                var regexp = new RegExp('\\{'+i+'\\}', 'gi');
                clickedLink = clickedLink.replace(regexp, replacements[i]);
            }
            location.replace(clickedLink);
        }
    },
    
    doInit : function(component, event, helper) {
        var oppId = component.get("v.recordId");
        
        var action = component.get("c.validateOpportunity");
        action.setParams({
            recordId : oppId
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                //var alertMessagesList = response.getReturnValue();
                component.set("v.alertMessages",response.getReturnValue());
                
            } else if (response.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);
        
    }
})