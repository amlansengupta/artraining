// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	doInit : function(component, event, helper) 
    {
        // helper.debug(component,'Topic Url fetched..',null);
        var shouldLinkTopic = component.get('v.shouldLinkTopic');
        if(shouldLinkTopic === true){
        	helper.FetchTopicUrl(component);
		}
	}
})