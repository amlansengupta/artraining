/**
 * Created by abdoundure on 9/10/18.
 */
({
    getExperts: function (component, event, helper) {
        var expertList = component.get("v.recordId");
        var action = component.get("c.getMercerExpertsByTopic");
       action.setParams({
            "topicId": expertList
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var expertList = response.getReturnValue();
                console.log('test experts: ', expertList);
                component.set("v.experts", expertList);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getMetaExperts: function (component) {
        var metaExpert = component.get('v.metaExpertId');
        var action = component.get("c.getMercerExperts");
        action.setParams({
            "UserId": metaExpert
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var expertList = response.getReturnValue();
                //console.log('Meta experts: ', expertList);
                component.set("v.metaExpert", expertList);
                component.set('v.metaExpertJoined', expertList.Follow__c);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getFollowExpert: function (component, event, helper) {
        //var action = component.get("c.followMercerExperts");
        var action = component.get("c.followMercerExpert");
        
        action.setParams({
            "expertId": component.get("v.metaExpertId"),
            "following": component.get("v.metaExpertJoined")
        });

        console.log(action);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }

})