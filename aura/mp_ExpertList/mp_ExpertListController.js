/**
 * Created by abdoundure on 9/10/18.
 */
({
    doInit:function (component, event, helper) {
        helper.getExperts(component, event, helper);
        helper.getMetaExperts(component, event, helper);
        //helper.getFollowExpert(component, event, helper);
    },
    userUnFollowExpert: function (component, event, helper) {
        console.log('called');
        component.set("v.metaExpertJoined", false);
        helper.getFollowExpert(component, event, helper);
    },
    userFollowExpert: function (component, event, helper) {
        var following = component.get("v.metaExpertJoined");
 
       if (following){
            component.set("v.metaExpertJoined", false);
        } else {
            component.set("v.metaExpertJoined", true);
            }
        helper.getFollowExpert(component, event, helper);


    }
})