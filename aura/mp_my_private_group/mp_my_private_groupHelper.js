/**
 * Created by emaleesoddy on 4/13/17.
 */
({
    getPrivateGroupInfo: function(component) {
        var action = component.get("c.getPrivateGroup");
        var self = this;

        action.setCallback(this, function(a) {
            var results = a.getReturnValue().results[0];
            if (results) {
                console.log('private group items: ',results);
                component.set("v.privateGroup", results);

            if (results["Id"] != '') {
                component.set("v.privateGroupID", results["Id"]);
            }
            }
            self.getKeyContact(component);
        });
        $A.enqueueAction(action);
    },
    getKeyContact: function(component) {
        if (component.get("v.showGCM") != false && component.get("v.privateGroupID") != '') {
            var action = component.get("c.getGroupKeyContact");
            action.setParams({
                groupId : component.get("v.privateGroupID"),
                roleName: component.get("v.roleName")
            });
            action.setCallback(this, function(r){
                component.set("v.GCM", r.getReturnValue());
            });
            $A.enqueueAction(action);
        }
    }
})