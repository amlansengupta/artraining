/**
 * Created by emaleesoddy on 4/13/17.
 */
({
    doInit: function (component, event, helper) {
        helper.getPrivateGroupInfo(component);
    },
    goToProfile : function(component, event, helper){
        var profileId = event.currentTarget.dataset.id;

        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": profileId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    goToGroup : function(component, event, helper){
        var groupId = event.currentTarget.dataset.id;

        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": groupId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    }
})