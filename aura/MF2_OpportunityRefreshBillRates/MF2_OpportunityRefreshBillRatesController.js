({
    //Request Id#17444: This method will get invoked once the related component gets loaded and requests server side controller class
    //to refresh the Bill rates.
    doInit : function(component, event, helper){
        var oppId=component.get("v.recordId");
        var action = component.get("c.refreshBillRates");
        action.setParams({
            'oppId' : oppId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.loaded",true);
            if(state == "SUCCESS"){
                var stageError =  response.getReturnValue();
                if(stageError){
                    component.set("v.pageMessage","This button is only enabled for open opportunities.");
                    component.set("v.toForward",false);
                }
                else{
                    component.set("v.pageMessage","Bill Rates refreshed successfully.");
                    //location.replace(self.location.href);
                    component.set("v.toForward",true);
                }
            }
            else if (state === "ERROR"){
                component.set("v.pageMessage","Could not refresh Bill Rates! Please try after sometime.");
                //location.replace(self.location.href);
                component.set("v.toForward",false);
            }
        });
        $A.enqueueAction(action);
    },
    forwardToCopiedRecord:function(component,event,helper){
        var oppRecordId = component.get("v.recordId");
        var oppUrl = $A.get("$Label.c.OppRefreshBillRateURL");
        var replacements = [oppRecordId];
        for (var i = 0; i < replacements.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            oppUrl = oppUrl.replace(regexp, replacements[i]);
        }
        
        //location.replace("/lightning/r/Opportunity/"+component.get("v.recordId")+"/view");
        location.replace(oppUrl);
    }
    
})