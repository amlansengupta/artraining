({
	doInit : function(component, event, helper) {
		var oppId = component.get("v.recordId");
        var action = component.get("c.scopeRecordTypeCheck");
        action.setParams({
            'oppId' : oppId,
    	});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //opp = response.getReturnValue();
                //alert("opp "+ opp);
                component.set("v.flag", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
	},
    printScopeitDetailsComp : function(component,event,helper){
        var oppId = component.get("v.recordId");
    	window.open("/apex/Mercer_PrintScopeIt?id="+oppId);
	},
    refreshBillRatesComp : function(component,event,helper){
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Event '+evt);
        var oppId = component.get("v.recordId");
        evt.setParams({
            componentDef  : "c:MF2_OpportunityRefreshBillRates" ,
            componentAttributes : {
                recordId : oppId
            }
        

        });
      
        evt.fire();
    }
})