// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    // Method to initailize topics for dropdown
    initializeDropdown: function(component) {
        try {
            $(".topic")
                .addClass("ui fluid search")
                .dropdown({
                    placeholder: "Select Topics"
                });
            var topics = component.get('v.selectedTopics');
            if (topics !== '') {
                topics = JSON.parse(topics);
                var tp = [];
                for (var t = 0; t < topics.length; t++) {
                    tp.push(topics[t].TopicId);
                }
                window.setTimeout(
                    $A.getCallback(function() {
                        $(".topic").dropdown('set selected', tp);
                    }), 2000
                );

            }
        } catch (e) {
            this.debug(component, null, e);
        }
    },

    // Method to fetch the topics
    getTopics: function(component) {
        var action = component.get("c.getTopics");
        action.setCallback(this, function(response) {
            var values = response.getReturnValue();
            var valuesTemp = [];
            for (var value in values) {
                if (values.hasOwnProperty(value)) {
                    valuesTemp.push({
                        key: value,
                        value: values[value]
                    });
                }
            }
            component.set("v.topicValues", valuesTemp);

        });
        $A.enqueueAction(action);
    },

    // Method to submit and create event
    submitEventHelper: function(component, eventObj, allDayEventStartDate, allDayEventEndDate) {
        var strfilterByTopic = component.find("filterByTopic").get("v.value");
        var fileInput = component.find("image").getElement();
        var pathToDetail = component.get("v.pathToDetail");

        if (component.get("v.isFileCooosen") || component.get("v.isFileDelete")) {
            this.deleteAttachment(component);
        }

        var action = component.get("c.saveEvents"); //added to test changed the method to 2

        action.setParams({
            "eventObj": eventObj,
            "strfilterByTopic": strfilterByTopic,
            "allDayEventStartDate": allDayEventStartDate, // added to test ,extra parameter
            "allDayEventEndDate": allDayEventEndDate,
        });

        var self = this;

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var resId = response.getReturnValue().Id;
                console.log('&&&&&', resId);

                if (fileInput.files.length > 0 && resId !== null && resId.length > 0) {
                    self.uploadImage(component, resId, fileInput.files);
                } else if (resId !== null && resId.length > 0) {
                    $A.util.addClass(spinnerSubmit, "hide");
                    self.goToURL(pathToDetail + resId);
                }
            }
            if (state === "ERROR") {
                var errors = response.getError();
                
                if (errors[0] && errors[0].pageErrors) {
                    var errorMsg = errors[0].pageErrors[0].message;
                    if (errorMsg.includes('Required fields are missing: [Start DateTime]')) {
                        component.find("eventStartDate").set("v.errors", [{
                            message: errorMsg
                        }]);
                    } else {
                        component.set("v.strError", errors[0].pageErrors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },

    uploadImage: function(component, resId, files) {
        var sessionId = component.get("v.sessionId");
        var pathToDetail = component.get("v.pathToDetail");
        var sfInstanceUrl = component.get("v.sfInstanceUrl");
        var client = new forcetk.Client();
        client.setSessionToken(sessionId, 'v36.0', sfInstanceUrl);
        client.proxyUrl = null;
        client.instanceUrl = sfInstanceUrl;

        var file = files[0];
        var self = this;
        var spinner = component.find("spinnerSubmit");
        client.createBlob('Attachment', {
            'ParentId': resId,
            'Name': file.name,
            'ContentType': file.type,
        }, file.name, 'Body', file, function(response) {
            if (response.id !== null) {
                $A.util.addClass(spinner, "hide");
                self.goToURL(pathToDetail + resId);
            } else {
                $A.util.addClass(spinner, "hide");
                component.set("v.strAttachmentError", 'Error : ' + response.errors[0].message);
            }
        }, function(request, status, response) {
            $A.util.addClass(spinner, "hide");
            var res = JSON.parse(response);
            component.set("v.strAttachmentError", 'Error : ' + res[0].message);

        });
    },

    //goToURL: function(url, isredirect) {
    goToURL: function(url) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
        });
        urlEvent.fire();
    },

    // Fetching Site Path
    get_SitePrefix: function(component) {
        var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            component.set("v.sitePrefix", sitePath.replace("/s", ""));

        });
        $A.enqueueAction(action);
    },

    //Check for Button before creating an event
    toggleSubmitButton: function(component) {

            component.set("v.disableButton", false);
            var errorButton = document.getElementById('FillHeader');
            errorButton.style.display = "none";
            //$('#FillHeader').hide();

            var stDt;
            var stTme;
            var errMsg = "";
            if ($(".dateTime-inputDate input").attr("class") == 'form-control requiredField stdate input') {
                stDt = $(".dateTime-inputDate input").val();
                errMsg += "Start Date = " + stDt +" ";
            }
            if ($(".dateTime-inputTime input").attr("class") == 'form-control requiredField stdate input') {
                stTme = $(".dateTime-inputTime input").val();
                errMsg += "Start Time = " + stTme +" ";
            }
            //this.debug(component, 'start date and time', stDt + stTme);

            var valError = false;



            if (component.get('v.eventObj.Name') === undefined ||
                    component.get('v.eventObj.Name') === '') {
                valError = true;
                errMsg += "Name not valid. ";

            }
            if (!component.find('allDayEvent').get("v.value") &&
                    (stDt === undefined || stDt.trim() === '' || stTme === undefined || stTme.trim() === '')){
                valError = true;
                errMsg += "Not all day but date/time not set. ";

                //console.log("Not all day but date/time not set");
            }

            if (component.find('allDayEvent').get("v.value") &&
                    (component.get('v.allDayEventStartDate') === undefined ||
                        component.get('v.allDayEventStartDate') === '')) {
                valError = true;
                errMsg += "all day but Date not set. ";
                //console.log("all day but Date not set");
            }

            if (component.get('v.eventObj.Location_Name__c') === undefined ||
                    component.get('v.eventObj.Location_Name__c') === '') {
                valError = true;
                errMsg += "Location not set. ";
                //console.log("Location not set");
            }
            /*if (component.get('v.eventObj.Details__c') === undefined ||
                    component.get('v.eventObj.Details__c') === '') {
                valError = true;
                errMsg += "Detail not set. ";

            }*/
            if (component.find('filterByTopic').get('v.value') === undefined ||
                    component.find('filterByTopic').get('v.value') === '') {
                valError = true;
                errMsg += "Topic not set. ";

            }

            if (valError) {
                component.set("v.disableButton", true);
                errorButton.style.display = "block";
                console.log(errMsg);
                //console.log("v.eventObj = " + component.get('v.eventObj.Details__c'));
                //console.log('eventDetail = ' + component.find('eventDetail').get('v.value')) ;
                //$('#FillHeader').show();
            }

    },

    get_SessionId: function(component) {
        var action = component.get("c.getSessionId");
        action.setCallback(this, function(actionResult) {
            component.set("v.sessionId", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    enablePricing: function(component, event, helper) {

        var cmpTarget = component.find('eventTicketPriceBlock');
        if (component.find('enablePricing').get("v.value") === true) {
            $A.util.addClass(cmpTarget, 'showDiv');
            $A.util.removeClass(cmpTarget, 'hideDiv');
        } else {
            $A.util.addClass(cmpTarget, 'hideDiv');
            $A.util.removeClass(cmpTarget, 'showDiv');
        }
    },

    enableThreshold: function(component, event, helper) {
        var newEvent  = component.get("v.eventObj");
        var cmpTarget = component.find('eventThresholdCountBlock');

        if (component.find('enableRSVP').get("v.value") === true) {
            $A.util.addClass(cmpTarget, 'showDiv');
            $A.util.removeClass(cmpTarget, 'hideDiv');
            newEvent.RSVP_Count_Threshold__c = 10;
        } else {
            $A.util.addClass(cmpTarget, 'hideDiv');
            $A.util.removeClass(cmpTarget, 'showDiv');
            newEvent.RSVP_Count_Threshold__c = 0;
        }

        component.set("v.eventObj", newEvent);
    },

    getEventsRecord: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getEventRecord");

        action.setParams({
            eventRecordId: component.get("v.sObjectId"),
        });
        component.set('v.attachmentName', 'No File Chosen');

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (component.isValid() && state === "SUCCESS") {
                var eventsListWrapper = response.getReturnValue();

                component.set('v.attachments', eventsListWrapper.objEventList[0].Attachments);
                component.set("v.eventObj", eventsListWrapper.objEventList[0]);
                component.set('v.richTextDetail', eventsListWrapper.objEventList[0].Details__c);

                if (moment().isAfter(eventsListWrapper.objEventList[0].Start_DateTime__c, 'day')) {
                    if (!eventsListWrapper.objEventList[0].All_Day_Event__c) {
                        // not clear why this is messing with UTC?
                        //component.set('v.eventObj.Start_DateTime__c', moment().utc().format('YYYY-MM-DDThh:mm:ss') + 'Z');
                    } else {
                        var today = new Date();
                        component.set('v.allDayEventStartDate', moment().format('YYYY-MM-DD'));
                    }
                }
                component.set('v.allDayEventStartDate', eventsListWrapper.objEventList[0].Start_DateTime__c);
                component.set('v.allDayEventEndDate', eventsListWrapper.objEventList[0].End_DateTime__c);


                if (eventsListWrapper.objEventList[0].Attachments && eventsListWrapper.objEventList[0].Attachments.length > 0) {
                    component.set('v.isAttachment', true);
                    component.set('v.attachmentName', eventsListWrapper.objEventList[0].Attachments[0].Name);
                }

                self.enablePricing(component, event, helper);
                self.enableThreshold(component, event, helper);

                $("document").ready(function() {
                    $("#upload").change(function() {
                        //alert('changed!');
                        var a = document.getElementById('upload');
                        if (a.value === "") {
                            fileLabel.innerHTML = "No file Chosen";
                        } else {
                            var theSplit = a.value.split('\\');
                            fileLabel.innerHTML = theSplit[theSplit.length - 1];
                            jQuery('<div/>', {
                                class: 'myCls',
                                text: theSplit[theSplit.length - 1]
                            }).appendTo('body');
                            $('.myCls').html('');
                        }
                    });
                });

            }
        });
        $A.enqueueAction(action);
    },

    deleteAttachment: function(component) {

        var action = component.get("c.deleteAttachment");

        action.setParams({
            eventRecordId: component.get("v.sObjectId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var attachments = response.getReturnValue();
                component.set("v.attachments", attachments);
            }
        });
        $A.enqueueAction(action);
    },

    isInteger: function (value)
    { return typeof value === 'number' && isFinite(value) && Math.floor(value) === value; },

    debug: function(component, msg, variable) {
        var debugMode = component.get("v.debugMode");
        if (debugMode) {
            if (msg) {
                console.log(msg);
            }
            if (variable) {
                console.log(variable);
            }
        }

    },

})