// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
    doInit : function(component, event, helper) {
        var url = window.location.href;
        var splitUrl = url.split("/s");
        component.set("v.sfInstanceUrl", splitUrl[0]);

        helper.get_SitePrefix(component);
        helper.get_SessionId(component);

        helper.getTopics(component);

        if (component.get('v.isEdit') === true) {
            helper.getEventsRecord(component, event, helper);
        }

        component.set('v.disableDateTime', false);
        component.set("v.disableButton", true);

        var cmpSpinner = component.find("spinnerSubmit");
        $A.util.addClass(cmpSpinner, "hide");

        if (component.get("v.eventObj.RSVP_Count_Threshold__c") === ''){
            component.set("v.eventObj.RSVP_Count_Threshold__c", '10');
        }
    },

    initializeUI: function(component, event, helper) {
        helper.initializeDropdown(component);
        $('#stDate , #endDate').click(function() {
            if($(window).scrollTop() > 700) {
                $(window).scrollTop(250);
            }
        });

        /*020616 - Safari and Mozilla fixes for input type file*/
        $(function(){
            var verOffset;
            var nAgt = navigator.userAgent;
            if ((verOffset = nAgt.indexOf("Safari")) !== -1) {
                if ((verOffset = nAgt.indexOf("Version")) !== -1) {
                    $('.attachEdit').addClass('attachEditSafari');
                }
            }
            else if ((verOffset = nAgt.indexOf("Firefox")) !== -1) {
                $('.attachEdit').addClass('attachEditMozilla');
            }
        });
        /*020616 - Safari fixes for input type file*/
    },

    triggerButtonCheck: function(component,event,helper) {
        helper.debug(component, "trigger button check");
        component.set('v.disableDateTime', component.find('allDayEvent').get("v.value"));
        helper.debug(component,"check all day ",component.find('allDayEvent').get("v.value"));
        helper.toggleSubmitButton(component);
        //helper.debug(component, "just ran validation test");
    },

    enableThreshold : function(component, event, helper) {
        helper.enableThreshold(component, event, helper);
    },

    //Check for enable pricing section on create event page
    enablePricing : function(component, event, helper) {
        helper.enablePricing(component, event, helper);
    },

    //Check for disable dates on create event page
    disableDates : function(component, event, helper) {
        if (component.find('allDayEvent').get("v.value") === true) {
            component.find("eventStartDate").set("v.errors", null);
            component.find("eventEndDate").set("v.errors", null);
        }
    },

    //Check for validations messages on create event page
    submitEvent : function(component, event, helper) {
        try {
            var cont = (component.find("allDayEvent").get("v.value")) ? 'day' : 'minute';
            var spinnerSubmit = component.find("spinnerSubmit");
            $A.util.removeClass(spinnerSubmit,"hide");

            component.set("v.strError",null);
            component.set("v.strAttachmentError",null);
            component.set("v.isBrowseError",false);
            if (component.find('allDayEvent').get("v.value") === true) {
                $('.dateTime-inputTime').hide();
            } else {
                $('.dateTime-inputTime').show();
            }

            var eventObj = component.get("v.eventObj");
            var fileInput = component.find("image").getElement();
            var allDayEventStartDate;
            var allDayEventEndDate;
            if (component.find('allDayEvent').get("v.value")) {
                allDayEventStartDate = component.get("v.allDayEventStartDate");  // added to test
                allDayEventEndDate = component.get("v.allDayEventEndDate");
            } else {
                allDayEventStartDate = component.get("v.eventObj.Start_DateTime__c");
                allDayEventEndDate = component.get("v.eventObj.End_DateTime__c");
            }

            var isError = false;
            // Validation message for location if empty or spaces check
            var locationCMP = component.find("eventLocationName");
            if (!locationCMP.get("v.value") || locationCMP.get("v.value").trim() === "") {
                locationCMP.set("v.errors", [{message:"Please fill out the Event Location."}]);
                isError = true;
            } else {
                if (locationCMP.get("v.value").length > 255) {
                    locationCMP.set("v.errors", [{message:"Event Location cannot exceed 255 characters. Please enter a location less than 255 characters."}]);
                    isError = true;
                } else {
                    locationCMP.set("v.errors",null);
                }
            }

            var eventAddressCMP = component.find("eventAddress");
            if (!eventAddressCMP.get("v.value") || eventAddressCMP.get("v.value").trim() === "") {
                eventAddressCMP.set("v.errors",null);
            } else {
                if(eventAddressCMP.get("v.value").length > 255){
                    eventAddressCMP.set("v.errors", [{message:"Event Address cannot exceed 255 characters. Please enter address less than 255 characters."}]);
                    isError = true;
                } else {
                    eventAddressCMP.set("v.errors",null);
                }
            }

            // Validation for description field for null check
            var eventDetailCmp =component.find("eventDetail");
            if (!eventDetailCmp.get("v.value") || eventDetailCmp.get("v.value").trim().length <= 0) {
                eventDetailCmp.set("v.errors", [{message:"Please fill out the Event Description."}]);
                isError = true;
            } else {
                eventDetailCmp.set("v.errors",null);
            }

            // Validation message for Event title if empty and spaces check and length if more than 80 characters
            var eventNameCmp = component.find("eventName");
            if (!eventNameCmp.get("v.value") || eventNameCmp.get("v.value").trim() === "") {
                eventNameCmp.set("v.errors", [{message:"Please fill out the Event Name."}]);
                isError = true;
            } else {
                if (eventNameCmp.get("v.value").length > 80) {
                    eventNameCmp.set("v.errors", [{message:"Event Title cannot exceed 80 characters. Please enter a title less than 80 characters."}]);
                    isError = true;
                } else {
                    eventNameCmp.set("v.errors",null);
                }
            }

            // Validation message for Date validations
            var eventStartDateCmp = component.find("eventStartDate");
            var eventStartDateCmp1 = component.find("eventStartDate_dateOnly");
            var frmDtStr;
            var toDtStr ;
            var startDate;
            var endDate;

            try {
                eventStartDateCmp.set("v.errors", null);
                component.find("eventEndDateTime").set("v.errors", null);
            } catch(e) {
                //console.log(e);
            }

            if (component.find("allDayEvent").get("v.value") !== true) {
                if (!eventStartDateCmp.get("v.value") || eventStartDateCmp.get("v.value").trim() === "") {
                    eventStartDateCmp.set("v.errors", [{message:"Please Select Event Start Date."}]);
                    isError = true;
                } else {

                    eventStartDateCmp.set("v.errors", null);
                    startDate = moment.utc(component.find("eventStartDate").get("v.value")).format('YYYY-MM-DD HH:mm:ss');
                    frmDtStr = moment.utc(startDate).toDate();
                }

                if (component.find("eventEndDateTime").get("v.value") !== null) {
                    endDate = moment(component.find("eventEndDateTime").get("v.value")).format('YYYY-MM-DD HH:mm:ss');
                    toDtStr = moment(endDate).toDate();
                }

                if (moment().isAfter(startDate, 'day')) {
                    component.find("eventStartDate").set("v.errors", [{message:"Start Date must be greater than or equal to today."}]);
                    component.find("eventEndDateTime").set("v.errors", null);
                    isError = true;
                } else if (toDtStr !== '' && toDtStr && moment(toDtStr).isBefore(frmDtStr, 'day')) {
                    eventStartDateCmp.set("v.errors", null);
                    component.find("eventEndDateTime").set("v.errors", [{message:"To Date Can not be less than From Date."}]);
                    isError = true;
                } else {
                    eventStartDateCmp.set("v.errors", null);
                    component.find("eventEndDateTime").set("v.errors",null);
                }
            } else {
                if (!eventStartDateCmp1.get("v.value") || eventStartDateCmp1.get("v.value").trim() === "") {
                    eventStartDateCmp1.set("v.errors", [{message:"Please Select Event Start Date."}]);
                    isError = true;
                } else {
                    eventStartDateCmp1.set("v.errors", null);
                    startDate = moment(component.find("eventStartDate_dateOnly").get("v.value")).format('YYYY-MM-DD HH:mm:ss');
                    frmDtStr = moment(startDate).toDate();
                }

                if (component.find("eventEndDate").get("v.value") !== null) {
                    endDate = moment(component.find("eventEndDate").get("v.value")).format('YYYY-MM-DD HH:mm:ss');
                    component.find("eventEndDate").set("v.errors",null);
                    toDtStr = moment(endDate).toDate();
                }

                if (moment().isAfter(moment(startDate), 'day')) {
                    component.find("eventStartDate_dateOnly").set("v.errors", [{message:"Start Date must be greater than or equal to today."}]);
                    isError = true;
                } else if(toDtStr !== '' && toDtStr && moment(toDtStr).isBefore(frmDtStr, 'day')) {
                    eventStartDateCmp1.set("v.errors", null);
                    component.find("eventEndDate").set("v.errors", [{message:"To Date Can not be less than From Date."}]);
                    isError = true;
                } else {
                    component.find("eventStartDate_dateOnly").set("v.errors", null);
                    component.find("eventEndDate").set("v.errors",null);
                }
            }

            // Validation message for attachments
            if (fileInput.files.length > 0 && fileInput.files[0].type.indexOf("image") === -1) {
                isError = true;
                component.set("v.isBrowseError",true);
                component.set("v.strError","Selected file must be an image.");
            } else if(fileInput.files.length > 0 && fileInput.files[0].size > 25000000) {
                isError = true;
                component.set("v.isBrowseError",true);
                component.set("v.strError","Error : Image size must be less than 25MB.");
            } else {
                if (component.get("v.isEdit") &&
                    component.get("v.attachmentName") === 'No File Chosen' &&
                    component.get("v.isAttachment")) {
                        component.set('v.isFileDelete',true);
                }
                component.set("v.isBrowseError",false);
                component.set("v.strError",null);
            }

            // Validation for topics to be selected
            var filterByTopicCmp = component.find("filterByTopic");

            if (!filterByTopicCmp.get("v.value") || filterByTopicCmp.get("v.value").trim() === "") {
                filterByTopicCmp.set("v.errors", [{message:"Please Select Topics."}]);
                isError = true;
            } else {
                filterByTopicCmp.set("v.errors",null);
            }

            //Updated on 24-5 code to check length of payment url field
            var paymentUrlCmp = component.find("eventExternalPaymentURL");
            if (!paymentUrlCmp.get("v.value") || paymentUrlCmp.get("v.value").trim() === "") {
                paymentUrlCmp.set("v.errors",null);
            } else {
                if (paymentUrlCmp.get("v.value").length > 255) {
                    isError = true;
                    paymentUrlCmp.set("v.errors", [{message:"Payment URL cannot exceed 255 characters. Please enter a URL less than 255 characters."}]);
                } else {
                    paymentUrlCmp.set("v.errors",null);
                }
            }

            if (component.find('enableRSVP').get("v.value") === true) {
                var eventThresholdCountCmp = component.find('eventThresholdCount');
                var eventThresholdCountValue = eventThresholdCountCmp.get("v.value");
                //if (!eventThresholdCountValue || (!Number.isInteger(eventThresholdCountValue) && eventThresholdCountValue.trim() === "")eventThresholdCountValue) {
                if (!eventThresholdCountValue || (!helper.isInteger(eventThresholdCountValue) && eventThresholdCountValue.trim() === "")) {
                    eventThresholdCountCmp.set("v.errors",[{message:"Please Fill out RSVP counter."}]);
                    isError = true;
                } else {
                    eventThresholdCountCmp.set("v.errors",null);
                }
            }

            if (isError === true) {
                $A.util.addClass(spinnerSubmit,"hide");
            } else {
                if (fileInput.files.length > 0) {
                    helper.submitEventHelper(component, eventObj,allDayEventStartDate,allDayEventEndDate);
                } else {
                    helper.submitEventHelper(component, eventObj,allDayEventStartDate,allDayEventEndDate);
                }
            }
        } catch(e) {
            //console.log('Exceptin e', e);
        }
    },

    notifyFileSelected : function(component, event, helper) {
        var fileInput = component.find("image").getElement();
        if (fileInput.files.length > 0 ) {
            component.set('v.isFileCooosen',true);
        } else {
            component.set('v.isFileCooosen',false);
            component.set("v.attachmentName",'No File Chosen');
        }
    },
})