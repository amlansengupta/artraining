({
    doInit : function(component, event, helper){
        var action = component.get("c.isRunningUserInParentHierarchy");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnedVal = response.getReturnValue();
                
                component.set("v.isParent",response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    EditCompOpen:function(component,event,helper){
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-show");
        $A.util.addClass(accDisp,"slds-hide");
        $A.util.removeClass(accEdit,"slds-hide"); 
        $A.util.addClass(accEdit,"slds-show");
    },
    
    closeModal:function(component,event,helper){    
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-hide");
        $A.util.addClass(accDisp,"slds-show");
        $A.util.removeClass(accEdit,"slds-show"); 
        $A.util.addClass(accEdit,"slds-hide");
    },
    
    handleOnload : function(component, event, helper) {
        var recUi = event.getParam("recordUi");
        console.assert(null === recUi.record.id);
    },
    
    handleOnSubmit : function(component, event, helper) {
        event.preventDefault();  // stop the form from submitting
        var eventFields = event.getParam("fields");
        component.find('form').submit(eventFields);
    },
    
    handleOnSuccess : function(component, event, helper) {
        var payload = event.getParams().response;
		console.log(payload.id);
        
        var accEdit = component.find('accountEdit');
        var accDisp = component.find('accountDisplay');
        $A.util.removeClass(accDisp,"slds-hide");
        $A.util.addClass(accDisp,"slds-show");
        $A.util.removeClass(accEdit,"slds-show"); 
        $A.util.addClass(accEdit,"slds-hide");
	},
   
    
})