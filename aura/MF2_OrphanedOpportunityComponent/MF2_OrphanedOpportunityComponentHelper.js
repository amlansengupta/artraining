({
	loadRecordCount : function(component, event, developerName, targetElementId, appendingElement){
        //var selectedItem = event.srcElement.id;
        //event.preventDefault();
        var action = component.get("c.fetchRecordCount");
        action.setParams({
            "developerName" : developerName
        });
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS'){
                document.getElementById(targetElementId).appendChild(appendingElement);
                document.getElementById(developerName).innerHTML = response.getReturnValue()+" "+document.getElementById(developerName).innerHTML;
                
            }else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
    
})