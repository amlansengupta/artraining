({
    doInit : function(component, event, helper) {
        var action = component.get("c.fetchDailyAlertDetails");
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS'){
                //var orphanOpp=response.getReturnValue();
                //component.set("v.totalRecords",orphanOpp.checkRecordNo);
                console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.dailyAlerts",response.getReturnValue());
                //var dailyAlerts = response.getReturnValue();
                
            }else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    createDailySyncPanel : function(component, event, helper){
        var dailyAlerts = component.get("v.dailyAlerts");
    	for(var loopCounter = 0;(dailyAlerts && (loopCounter < dailyAlerts.length)); loopCounter++){
                    var alert = dailyAlerts[loopCounter];
                    var anchorTag = document.createElement("a");
                    anchorTag.href = alert.Link__c;
                    if(alert.Display_In_New_Window__c === true){
                        anchorTag.target = "_blank";
                    }else{
                        anchorTag.target = "_self";
                    }
                    anchorTag.innerHTML = alert.UI_Label__c;
                    anchorTag.id = alert.DeveloperName;
                    
                    var tdElement = document.createElement("td");
                    tdElement.setAttribute("style", "padding-bottom:7px;");
                    tdElement.appendChild(anchorTag);
                    
                    var trElement = document.createElement("tr");
                    trElement.setAttribute("style", "text-align:left;");
                    trElement.appendChild(tdElement);
                    
                   
                    
                    helper.loadRecordCount(component, event, alert.DeveloperName, "targetContext", trElement);
                }
	}
    
    
})