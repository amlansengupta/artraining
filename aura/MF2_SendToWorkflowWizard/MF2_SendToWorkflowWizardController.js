({
    doInit : function(component, event, helper){
        var accID = component.get("v.recordId");
        var URL = parent.location.href;
        //alert(URL);
        var storeResponse;
        
        var action = component.get("c.fetchAccount");
        action.setParams({
            'accID' : accID,
            'URL' : URL
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.loaded",true);
            if (state === "SUCCESS") {
                storeResponse = response.getReturnValue();
                
                if(storeResponse.isURL){
                    component.set("v.isSuccess", true);
                    //alert('is success : ' + component.get("v.isSuccess"));
                    component.set("v.toForward",true);
                    component.set("v.returnstatement",storeResponse.returnStatement);
                    
                }
                else if(!storeResponse.isURL){
                    component.set("v.isError", true);
                    //alert('is error : ' + component.get("v.isError"));
                    component.set("v.pageMessage",storeResponse.returnStatement);
                    //alert(component.get("v.pageMessage"));
                }
            }
            else{
                var Error = response.getError();
                component.set("v.isError", true);
                component.set("v.pageMessage",Error);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    forwardToCopiedRecord:function(component,event,helper){
       location.replace('/' + component.get("v.recordId")); 
    },
})