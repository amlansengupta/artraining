({
	doInit : function(component, event, helper) {
		var accId = component.get("v.recordId");
        var action = component.get("c.recordTypeCheckAcc");
        action.setParams({
            'accId' : accId
    	});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.flag", response.getReturnValue());
            }});
        
        $A.enqueueAction(action);
	}
})