// Copyright ©2016-2017 7Summits Inc. All rights reserved.
({
	getNewsRecord : function(component) {
		
        this.debug(component,'Posted By called...',null);

        var action = component.get("c.getNewsRecord");
        action.setParams({
            newsRecordId : component.get("v.recordId"),
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                this.debug(component,'Success...');
                console.log(response.getReturnValue());
                var newsListWrapper  = response.getReturnValue();
                if(newsListWrapper.netMem !== null){
                	component.set("v.strDate",moment.utc(newsListWrapper.netMem.CreatedDate).format('LL'));
                }
                component.set("v.newsListWrapper", newsListWrapper);
            }
            else{
                this.debug(component,'Action failed...',response);
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.isNicknameDisplayEnabled");
        
        action1.setCallback(this, function(actionResult) {
            this.debug(component,'Success - isNicknameDisplayEnabled...');
            console.log(actionResult.getReturnValue());
            component.set("v.isNicknameDisplayEnabled", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action1);
        
    },
    
    get_SitePrefix : function(component) {
    	var action = component.get("c.getSitePrefix");
        action.setCallback(this, function(actionResult) {
            var sitePath = actionResult.getReturnValue();
            component.set("v.sitePath", sitePath);
            //component.set("v.sitePrefix", sitePath.replace("/s",""));
		});
        $A.enqueueAction(action);
    },
    
    debug: function(component, msg, variable) {

        var debugMode = component.get("v.debugMode");
        if(debugMode){
            if(msg){
            	console.log(msg);
            }
            if(variable){
            	console.log(variable);
            }
        }
    }
    
})