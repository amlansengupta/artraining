({
    doInit : function(component, event, helper){
        var action = component.get("c.isRunningUserInParentHierarchy");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnedVal = response.getReturnValue();
                
                component.set("v.isParent",response.getReturnValue());
                console.log("Got not error in 1st");
            }else if (state === "ERROR") {
                
                var errors = response.getError();                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);                        
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
        var accId = component.get("v.recordId");
        var action1 = component.get("c.recordTypeCheckAcc");
        action1.setParams({
            'accId' : accId
    	});
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.flag", response.getReturnValue());
                console.log("Got not error in 2'nd");
            }else if (state === "ERROR") {
                
                var errors = response.getError();                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);                        
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action1);
        
        var action2 = component.get("c.isRenderSections");
        
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.kycFlag", response.getReturnValue());
                console.log("Got not error in 3'rd");
            }else if (state === "ERROR") {
                
                var errors = response.getError();                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);                        
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action2);
    },
    editRecord : function(component, event, helper){
        event.preventDefault();
        helper.showHide(component);
    },
    
    /*handleSubmit : function(component, event, helper) {
        event.preventDefault();
        
        var eventFields = event.getParam("fields");
        
        
        alert(JSON.stringify(eventFields));
        component.find('editForm').submit(eventFields);
    },*/
    
    handleSuccess : function(component, event, helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({"title" : "Success!","message":"The Account's information is updated","type":"success"});
        toastEvent.fire();
        helper.showHide(component);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "detail"
        });
        navEvt.fire();
        
    },
    
    handleCancel : function(component, event, helper){
        helper.showHide(component);
        event.preventDefault();
    } 
})